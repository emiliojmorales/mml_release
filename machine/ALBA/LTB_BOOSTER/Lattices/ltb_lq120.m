function ltb
%ALBA_LTB lattice definition file
% Created by G. Benedetti 18/08/08
%

global FAMLIST THERING GLOBVAL

GLOBVAL.E0 = 0.1e+9;
GLOBVAL.LatticeFile = mfilename;
FAMLIST = cell(0);

disp(['**   Loading ALBA LTB lattice ', mfilename]);

AP = aperture('AP', [-0.0155, 0.0155, -0.0155, 0.0155],'AperturePass');

% Cavity
L0 = 13.6723;	% design length  [m]
C0 = 299792458; 	% speed of light [m/s]

D101  =    drift('D101', 0.300, 'DriftPass');
D102  =    drift('D102', 0.150, 'DriftPass');
D103  =    drift('D103', 0.330, 'DriftPass');
D104  =    drift('D104', 0.180, 'DriftPass');
D105  =    drift('D105', 0.180, 'DriftPass');
D106  =    drift('D106', 0.790, 'DriftPass');
D201  =    drift('D201', 0.600, 'DriftPass');
D202  =    drift('D202', 0.150, 'DriftPass');
D203  =    drift('D203', 1.140, 'DriftPass');
D204  =    drift('D204', 0.180, 'DriftPass');
D205  =    drift('D205', 0.180, 'DriftPass');
D206  =    drift('D206', 1.348, 'DriftPass');
D207  =    drift('D207', 1.000, 'DriftPass');
D208  =    drift('D208', 0.592, 'DriftPass');
D301  =    drift('D301', 0.300, 'DriftPass');
D302  =    drift('D302', 0.150, 'DriftPass');
D303  =    drift('D303', 1.940, 'DriftPass');
D304  =    drift('D304', 0.180, 'DriftPass');
D305  =    drift('D305', 0.180, 'DriftPass');
D306  =    drift('D306', 0.165, 'DriftPass');
D307  =    drift('D307', 0.150, 'DriftPass');
D308  =    drift('D308', 1.806, 'DriftPass');

QT0101 =  quadrupole('Q', 0.12,  -8.101318, 'StrMPoleSymplectic4Pass');
QT0102 =  quadrupole('Q', 0.12,  -2.983493, 'StrMPoleSymplectic4Pass');
QT0103 =  quadrupole('Q', 0.12,   8.502664, 'StrMPoleSymplectic4Pass');
QT0104 =  quadrupole('Q', 0.12,  -0.716943, 'StrMPoleSymplectic4Pass');
QT0105 =  quadrupole('Q', 0.12, -10.865343, 'StrMPoleSymplectic4Pass');
QT0106 =  quadrupole('Q', 0.12,  10.355130, 'StrMPoleSymplectic4Pass');
QT0107 =  quadrupole('Q', 0.12,   8.510178, 'StrMPoleSymplectic4Pass');
QT0108 =  quadrupole('Q', 0.12, -14.098167, 'StrMPoleSymplectic4Pass');
QT0109 =  quadrupole('Q', 0.12,   7.033721, 'StrMPoleSymplectic4Pass');

BM01 = sbend('BEND', 0.3012, 8.75*pi/180, 8.75/2*pi/180, 8.75/2*pi/180,...
             0.0, 'BndMPoleSymplectic4Pass');
BM02 = sbend('BM02', 0.3001, 4.50*pi/180, 4.50/2*pi/180, 4.50/2*pi/180,...
             0.0, 'BndMPoleSymplectic4Pass');
%%SEPINJ   : sbend, L = 0.6342, Angle = 12.60*PI/180,
%%E1 = 14.50*PI/180, E2 = -1.90*PI/180;

CORH = corrector('HCM', 0.0, [0 0],'CorrectorPass');
CORV = corrector('VCM', 0.0, [0 0],'CorrectorPass');
BPM = marker('BPM','IdentityPass');
IP = marker('IP','IdentityPass');

% Begin Lattice
%----- Linac to Booster Transfer Line: LTB --------------------------!;
LTB = [BPM, D101, CORH, D102, CORV, D103, QT0101, D104, QT0102, D105,...
       QT0103, D106, BPM, BM01, D201, CORH, D202, CORV, D203, QT0104,...
       D204, QT0105, D205, QT0106, D206, D207, D208, BPM, BM02,...
       D301, CORH, D302, CORV, D303, QT0107, D304, QT0108, D305,...
       QT0109, D306, CORH, D307, CORV, D308, BPM, CORH, CORV, IP];

% INIT_LTB : Beta0,
%                 betx = 5.00, alfx = -1.00,
%                 bety = 5.00, alfy = -1.00,
%                 dx   = 0.00, dpx  = 0.00;

buildlat(LTB);

% set all magnets to the same energy
THERING = setcellstruct(THERING,'Energy',1:length(THERING),GLOBVAL.E0);

%evalin('caller','global THERING FAMLIST GLOBVAL');

% atsummary;

if nargout
    varargout{1} = THERING;
end

% Compute total length and RF frequency
L0_tot=0;
for i=1:length(THERING)
    L0_tot=L0_tot+THERING{i}.Length;
end
fprintf('   Model LTB length is %.6f meters\n', L0_tot)
%fprintf('   Model RF frequency is %.6f MHz\n', HarmNumber*C0/L0_tot/1e6)

clear global FAMLIST
%clear global GLOBVAL when GWig... has been changed.

%evalin('caller','global THERING FAMLIST GLOBVAL');
