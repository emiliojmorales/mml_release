function ltb_boosterinit(OperationalMode)
% Initialize parameters for ALBA LTB control in MATLAB

% Created by G.Benedetti - 18 August 2008

if nargin < 1
    OperationalMode = 1;
end

%==============================
% load AcceleratorData structure
%==============================

Mode             = 'Simulator';
setad([]);       %clear AcceleratorData memory

%%%%%%%%%%%%%%%%%%%%
% ACCELERATOR OBJECT
%%%%%%%%%%%%%%%%%%%%

setao([]);   %clear previous AcceleratorObjects

%=============================================
%% BPMx data: status field designates if BPM in use
%=============================================
iFam = 'BPMx';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'BPM';
AO.(iFam).MemberOf                 = {'PlotFamily'; 'HBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';

bpm={
     1,	'LI/DI/BPM-01', 1, [1  1], 'BPMX-00'
     2,	'LT/DI/BPM-01', 1, [1  2], 'BPMX-01'
     3,	'LT/DI/BPM-02', 1, [1  3], 'BPMX-02'
     4,	'LT/DI/BPM-03', 1, [1  4], 'BPMX-03'
     1,	'BO01/DI/BPM-01', 1, [1  1], '01BPM01'
     2,	'BO01/DI/BPM-02', 0, [1  2], '01BPM02'
     3,	'BO01/DI/BPM-03', 1, [1  3], '01BPM03'
     4,	'BO01/DI/BPM-04', 0, [1  4], '01BPM04'
     5,	'BO01/DI/BPM-05', 1, [1  5], '01BPM05'
     6,	'BO01/DI/BPM-06', 1, [1  6], '01BPM06'
     7,	'BO01/DI/BPM-07', 1, [1  7], '01BPM07'
     8,	'BO01/DI/BPM-08', 0, [1  8], '01BPM08'
     9,	'BO01/DI/BPM-09', 1, [1  9], '01BPM09'
    10,	'BO01/DI/BPM-10', 0, [1 10], '01BPM10'
    11,	'BO01/DI/BPM-11', 1, [1 11], '01BPM11'
    12,	'BO02/DI/BPM-01', 1, [2  1], '02BPM01'
    13,	'BO02/DI/BPM-02', 1, [2  2], '02BPM02'
    14,	'BO02/DI/BPM-03', 1, [2  3], '02BPM03'
    15,	'BO02/DI/BPM-04', 0, [2  4], '02BPM04'
    16,	'BO02/DI/BPM-05', 1, [2  5], '02BPM05'
    17,	'BO02/DI/BPM-06', 0, [2  6], '02BPM06'
    18,	'BO02/DI/BPM-07', 1, [2  7], '02BPM07'
    19,	'BO02/DI/BPM-08', 0, [2  8], '02BPM08'
    20,	'BO02/DI/BPM-09', 1, [2  9], '02BPM09'
 %   20,	'BO02/DI/BPM-09', 0, [2  9], '02BPM09' % 22 January 2010. Temporal thing.
    21,	'BO02/DI/BPM-10', 0, [2 10], '02BPM10'
    22,	'BO02/DI/BPM-11', 1, [2 11], '02BPM11'
 %   22,	'BO02/DI/BPM-11', 0, [2 11], '02BPM11' % 22 January 2010. Temporal thing.
    23,	'BO03/DI/BPM-01', 1, [3  1], '03BPM01'
    24,	'BO03/DI/BPM-02', 0, [3  2], '03BPM02'
    25,	'BO03/DI/BPM-03', 1, [3  3], '03BPM03'
    26,	'BO03/DI/BPM-04', 0, [3  4], '03BPM04'
    27,	'BO03/DI/BPM-05', 1, [3  5], '03BPM05'
    28,	'BO03/DI/BPM-06', 1, [3  6], '03BPM06'
    29,	'BO03/DI/BPM-07', 1, [3  7], '03BPM07'
    30,	'BO03/DI/BPM-08', 0, [3  8], '03BPM08'
    31,	'BO03/DI/BPM-09', 1, [3  9], '03BPM09'
%    31,	'BO03/DI/BPM-09', 0, [3  9], '03BPM09' %23 Jan 2010. Putting it
    32,	'BO03/DI/BPM-10', 0, [3 10], '03BPM10'
    33,	'BO03/DI/BPM-11', 1, [3 11], '03BPM11'
    34,	'BO04/DI/BPM-01', 1, [4  1], '04BPM01'
    35,	'BO04/DI/BPM-02', 0, [4  2], '04BPM02'
    36,	'BO04/DI/BPM-03', 1, [4  3], '04BPM03'
    37,	'BO04/DI/BPM-04', 1, [4  4], '04BPM04'
    38,	'BO04/DI/BPM-05', 0, [4  5], '04BPM05'
    39,	'BO04/DI/BPM-06', 1, [4  6], '04BPM06'
    40,	'BO04/DI/BPM-07', 1, [4  7], '04BPM07'
    41,	'BO04/DI/BPM-08', 1, [4  8], '04BPM08'
    42,	'BO04/DI/BPM-09', 0, [4  9], '04BPM09'
    43,	'BO04/DI/BPM-10', 0, [4 10], '04BPM10'
    44,	'BO04/DI/BPM-11', 1, [4 11], '04BPM11'
    };

% Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/XposSA');
    AO.(iFam).Status(ii,:)             = bpm{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';
% AO.(iFam).Monitor.Handles = NaN * ones(size(AO.(iFam).DeviceList,1),1);

%=============================================
%% BPMy data: status field designates if BPM in use
%=============================================

iFam = 'BPMy';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'BPM';
AO.(iFam).MemberOf                 = {'PlotFamily'; 'VBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';

bpm={
     1,	'LI/DI/BPM-01', 1, [1  1], 'BPMY-00'
     2,	'LT/DI/BPM-01', 1, [1  2], 'BPMY-01'
     3,	'LT/DI/BPM-02', 1, [1  3], 'BPMY-02'
     4,	'LT/DI/BPM-03', 1, [1  4], 'BPMY-03'
     1,	'BO01/DI/BPM-01', 1, [1  1], '01BPM01'
     2,	'BO01/DI/BPM-02', 0, [1  2], '01BPM02'
     3,	'BO01/DI/BPM-03', 1, [1  3], '01BPM03'
     4,	'BO01/DI/BPM-04', 0, [1  4], '01BPM04'
     5,	'BO01/DI/BPM-05', 1, [1  5], '01BPM05'
     6,	'BO01/DI/BPM-06', 1, [1  6], '01BPM06'
     7,	'BO01/DI/BPM-07', 1, [1  7], '01BPM07'
     8,	'BO01/DI/BPM-08', 0, [1  8], '01BPM08'
     9,	'BO01/DI/BPM-09', 1, [1  9], '01BPM09'
    10,	'BO01/DI/BPM-10', 0, [1 10], '01BPM10'
    11,	'BO01/DI/BPM-11', 1, [1 11], '01BPM11'
    12,	'BO02/DI/BPM-01', 1, [2  1], '02BPM01'
    13,	'BO02/DI/BPM-02', 1, [2  2], '02BPM02'
    14,	'BO02/DI/BPM-03', 1, [2  3], '02BPM03'
    15,	'BO02/DI/BPM-04', 0, [2  4], '02BPM04'
    16,	'BO02/DI/BPM-05', 1, [2  5], '02BPM05'
    17,	'BO02/DI/BPM-06', 0, [2  6], '02BPM06'
    18,	'BO02/DI/BPM-07', 1, [2  7], '02BPM07'
    19,	'BO02/DI/BPM-08', 0, [2  8], '02BPM08'
    20,	'BO02/DI/BPM-09', 1, [2  9], '02BPM09'
 %   20,	'BO02/DI/BPM-09', 0, [2  9], '02BPM09' % 22 January 2010. Temporal thing.
    21,	'BO02/DI/BPM-10', 0, [2 10], '02BPM10'
   22,	'BO02/DI/BPM-11', 1, [2 11], '02BPM11'
 %   22,	'BO02/DI/BPM-11', 0, [2 11], '02BPM11' % 22 January 2010. Temporal thing.
    23,	'BO03/DI/BPM-01', 1, [3  1], '03BPM01'
    24,	'BO03/DI/BPM-02', 0, [3  2], '03BPM02'
    25,	'BO03/DI/BPM-03', 1, [3  3], '03BPM03'
    26,	'BO03/DI/BPM-04', 0, [3  4], '03BPM04'
    27,	'BO03/DI/BPM-05', 1, [3  5], '03BPM05'
    28,	'BO03/DI/BPM-06', 1, [3  6], '03BPM06'
    29,	'BO03/DI/BPM-07', 1, [3  7], '03BPM07'
    30,	'BO03/DI/BPM-08', 0, [3  8], '03BPM08'
    %31,	'BO03/DI/BPM-09', 0, [3  9], '03BPM09' %putting it back
    31,	'BO03/DI/BPM-09', 1, [3  9], '03BPM09'
    32,	'BO03/DI/BPM-10', 0, [3 10], '03BPM10'
    33,	'BO03/DI/BPM-11', 1, [3 11], '03BPM11'
    34,	'BO04/DI/BPM-01', 1, [4  1], '04BPM01'
    35,	'BO04/DI/BPM-02', 0, [4  2], '04BPM02'
    36,	'BO04/DI/BPM-03', 1, [4  3], '04BPM03'
    37,	'BO04/DI/BPM-04', 1, [4  4], '04BPM04'
    38,	'BO04/DI/BPM-05', 0, [4  5], '04BPM05'
    39,	'BO04/DI/BPM-06', 1, [4  6], '04BPM06'
    40,	'BO04/DI/BPM-07', 1, [4  7], '04BPM07'
    41,	'BO04/DI/BPM-08', 1, [4  8], '04BPM08'
    42,	'BO04/DI/BPM-09', 0, [4  9], '04BPM09'
    43,	'BO04/DI/BPM-10', 0, [4 10], '04BPM10'
    44,	'BO04/DI/BPM-11', 1, [4 11], '04BPM11'
    };

%Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/YposSA');
    AO.(iFam).Status(ii,:)             = bpm{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';
% AO.(iFam).Monitor.Handles = NaN * ones(size(AO.(iFam).DeviceList,1),1);

%===========================================================
%% HCM
%===========================================================

iFam ='HCM';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'COR';
AO.(iFam).MemberOf                 = {'COR'; 'MCOR'; 'HCM'; 'Magnet'; 'CORH'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;

cor={
     1,	'LT01/PC/CORH-01', 1, [1  1], 'CORH-01'
     2,	'LT01/PC/CORH-02', 1, [1  2], 'CORH-02'
     3,	'LT01/PC/CORH-03', 1, [1  3], 'CORH-03'
     4,	'LT01/PC/CORH-04', 1, [1  4], 'CORH-04'
%     5,	'LT01/PC/CORH-05', 1, [1  5], 'CORH-05' % there is only 5
%     correctors
1,	'BO01/PC/CORH-01', 1, [1  1], '01CORH01'
     2,	'BO01/PC/CORH-02', 1, [1  2], '01CORH02'
     3,	'BO01/PC/CORH-03', 1, [1  3], '01CORH03'
     4,	'BO01/PC/CORH-04', 1, [1  4], '01CORH04'
     5,	'BO01/PC/CORH-05', 1, [1  5], '01CORH05'
     6,	'BO01/PC/CORH-06', 1, [1  6], '01CORH06'
     7,	'BO01/PC/CORH-07', 1, [1  7], '01CORH07'
     8,	'BO01/PC/CORH-08', 1, [1  8], '01CORH08'
     9,	'BO01/PC/CORH-09', 1, [1  9], '01CORH09'
    10,	'BO01/PC/CORH-10', 1, [1 10], '01CORH10'
    11,	'BO01/PC/CORH-11', 1, [1 11], '01CORH11'
    12,	'BO02/PC/CORH-01', 1, [2  1], '02CORH01'
    13,	'BO02/PC/CORH-02', 1, [2  2], '02CORH02'
    14,	'BO02/PC/CORH-03', 1, [2  3], '02CORH03'
    15,	'BO02/PC/CORH-04', 1, [2  4], '02CORH04'
    16,	'BO02/PC/CORH-05', 1, [2  5], '02CORH05'
    17,	'BO02/PC/CORH-06', 1, [2  6], '02CORH06'
    18,	'BO02/PC/CORH-07', 1, [2  7], '02CORH07'
    19,	'BO02/PC/CORH-08', 1, [2  8], '02CORH08'
    20,	'BO02/PC/CORH-09', 1, [2  9], '02CORH09'
    21,	'BO02/PC/CORH-10', 1, [2 10], '02CORH10'
    22,	'BO02/PC/CORH-11', 1, [2 11], '02CORH11'
    23,	'BO03/PC/CORH-01', 1, [3  1], '03CORH01'
    24,	'BO03/PC/CORH-02', 1, [3  2], '03CORH02'
    25,	'BO03/PC/CORH-03', 1, [3  3], '03CORH03'
    26,	'BO03/PC/CORH-04', 1, [3  4], '03CORH04'
    27,	'BO03/PC/CORH-05', 1, [3  5], '03CORH05'
    28,	'BO03/PC/CORH-06', 1, [3  6], '03CORH06'
    29,	'BO03/PC/CORH-07', 1, [3  7], '03CORH07'
    30,	'BO03/PC/CORH-08', 1, [3  8], '03CORH08'
    31,	'BO03/PC/CORH-09', 1, [3  9], '03CORH09'
    32,	'BO03/PC/CORH-10', 1, [3 10], '03CORH10'
    33,	'BO03/PC/CORH-11', 1, [3 11], '03CORH11'
    34,	'BO04/PC/CORH-01', 1, [4  1], '04CORH01'
    35,	'BO04/PC/CORH-02', 1, [4  2], '04CORH02'
    36,	'BO04/PC/CORH-03', 1, [4  3], '04CORH03'
    37,	'BO04/PC/CORH-04', 1, [4  4], '04CORH04'
    38,	'BO04/PC/CORH-05', 1, [4  5], '04CORH05'
    39,	'BO04/PC/CORH-06', 1, [4  6], '04CORH06'
    40,	'BO04/PC/CORH-07', 1, [4  7], '04CORH07'
    41,	'BO04/PC/CORH-08', 1, [4  8], '04CORH08'
    42,	'BO04/PC/CORH-09', 1, [4  9], '04CORH09'
    43,	'BO04/PC/CORH-10', 1, [4 10], '04CORH10'
    44,	'BO04/PC/CORH-11', 1, [4 11], '04CORH11'
     };

%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients('HCM');

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-2 2]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-4;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%% VCM

iFam ='VCM';

AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'COR';
AO.(iFam).MemberOf                 = {'COR'; 'MCOR'; 'VCM'; 'Magnet'; 'CORV'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;

cor={
     1,	'LT01/PC/CORV-01', 1, [1  1], 'CORV-01'
     2,	'LT01/PC/CORV-02', 1, [1  2], 'CORV-02'
     3,	'LT01/PC/CORV-03', 1, [1  3], 'CORV-03'
     4,	'LT01/PC/CORV-04', 1, [1  4], 'CORV-04'
%     5,	'LT01/PC/CORV-05', 1, [1  5], 'CORV-05'
 1,	'BO01/PC/CORV-01', 1, [1  1], '01CORV01'
     2,	'BO01/PC/CORV-03', 1, [1  2], '01CORV03'
     3,	'BO01/PC/CORV-05', 1, [1  3], '01CORV05'
     4,	'BO01/PC/CORV-06', 1, [1  4], '01CORV06'
     5,	'BO01/PC/CORV-07', 1, [1  5], '01CORV07'
     6,	'BO01/PC/CORV-09', 1, [1  6], '01CORV09'
     7,	'BO01/PC/CORV-11', 1, [1  7], '01CORV11'
     8,	'BO02/PC/CORV-01', 1, [2  1], '02CORV01'
     9,	'BO02/PC/CORV-03', 1, [2  2], '02CORV03'
    10,	'BO02/PC/CORV-05', 1, [2  3], '02CORV05'
    11,	'BO02/PC/CORV-06', 1, [2  4], '02CORV06'
    12,	'BO02/PC/CORV-07', 1, [2  5], '02CORV07'
    13,	'BO02/PC/CORV-09', 1, [2  6], '02CORV09'
    14,	'BO02/PC/CORV-11', 1, [2  7], '02CORV11'
    15,	'BO03/PC/CORV-01', 1, [3  1], '03CORV01'
    16,	'BO03/PC/CORV-03', 1, [3  2], '03CORV03'
    17,	'BO03/PC/CORV-05', 1, [3  3], '03CORV05'
    18,	'BO03/PC/CORV-06', 1, [3  4], '03CORV06'
    19,	'BO03/PC/CORV-07', 1, [3  5], '03CORV07'
    20,	'BO03/PC/CORV-09', 1, [3  6], '03CORV09'
    21,	'BO03/PC/CORV-11', 1, [3  7], '03CORV11'
    22,	'BO04/PC/CORV-01', 1, [4  1], '04CORV01'
    23,	'BO04/PC/CORV-03', 1, [4  2], '04CORV03'
    24,	'BO04/PC/CORV-05', 1, [4  3], '04CORV05'
    25,	'BO04/PC/CORV-06', 1, [4  4], '04CORV06'
    26,	'BO04/PC/CORV-07', 1, [4  5], '04CORV07'
    27,	'BO04/PC/CORV-09', 1, [4  6], '04CORV09'
    28,	'BO04/PC/CORV-11', 1, [4  7], '04CORV11'
     };

% Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients('VCM');

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-2 2]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-4;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%=============================
%        MAIN MAGNETS
%=============================

%===========
% Dipole data
%===========
%% *** BEND-01 ***
iFam = 'BEND';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).FamilyType                 = 'BEND';
AO.(iFam).MemberOf                   = {'BEND'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('BEND');
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn = @bend2gev;
AO.(iFam).Setpoint.Physics2HWFcn = @gev2bend;
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';

bend={
     1,	'LT01/PC/BEND-01', 1, [1  1], 'BEND-01'
     };

for ii=1:size(bend,1)
    AO.(iFam).ElementList(ii,:)        = bend{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bend(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bend(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = bend{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bend{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bend{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(bend(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 200]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.5;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%% *** BEND-02 ***
iFam = 'BM02';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).FamilyType                 = 'BEND';
AO.(iFam).MemberOf                   = {'BEND'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                    = HW2PhysicsParams;

% HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
% Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn = @bend2gev;
AO.(iFam).Setpoint.Physics2HWFcn = @gev2bend;
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';

bend={
     1,	'LT01/PC/BEND-02', 1, [1  1], 'BEND-02'
     };

for ii=1:size(bend,1)
    AO.(iFam).ElementList(ii,:)        = bend{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bend(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bend(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = bend{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bend{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bend{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(bend(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 15]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.5;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


% AO.(iFam).DeviceName(:,:) = {'LT01/PC/BEND-01';'LT01/PC/BEND-02'};
% AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/current');
% 
% AO.(iFam).DeviceList(:,:) = [1 1];
% AO.(iFam).ElementList(:,:)= 1;
% AO.(iFam).Status          = 1;
% 
% val = 1;
% AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
% AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
% AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
% AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
% AO.(iFam).Monitor.Range(:,:) = [0 200]; % 148 A for 0.170 T @ 0.1 GeV
% 
% AO.(iFam).Setpoint = AO.(iFam).Monitor;
% AO.(iFam).Desired  = AO.(iFam).Monitor;
% AO.(iFam).Setpoint.MemberOf  = {'PlotFamily'};
% AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/currentPM');
% 
% AO.(iFam).Setpoint.Tolerance(:,:) = 0.05;
% AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.05;

% %% *** BM02 ***
% iFam = 'BM02';
% AO.(iFam).FamilyName                 = iFam;
% AO.(iFam).MemberOf                   = {'MachineConfig'; 'BEND'; 'Magnet';};
% HW2PhysicsParams                    = magnetcoefficients('BM02');
% Physics2HWParams                    = HW2PhysicsParams;
% 
% AO.(iFam).Monitor.Mode               = Mode;
% AO.(iFam).Monitor.DataType           = 'Scalar';
% AO.(iFam).Monitor.Units              = 'Hardware';
% AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
% AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
% AO.(iFam).Monitor.HWUnits            = 'A';
% AO.(iFam).Monitor.PhysicsUnits       = 'rad';
% 
% AO.(iFam).DeviceName(:,:) = {'LT01/PC/BEND-02'};
% AO.(iFam).Monitor.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName(:,:),'/current');
% 
% AO.(iFam).DeviceList(:,:) = [1 1];
% AO.(iFam).ElementList(:,:)= 1;
% AO.(iFam).Status          = 1;
% 
% val = 1;
% AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
% AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
% AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
% AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
% AO.(iFam).Monitor.Range(:,:) = [0 200]; % 76 A for 0.087 T @ 0.1 GeV
% 
% AO.(iFam).Setpoint = AO.(iFam).Monitor;
% AO.(iFam).Desired  = AO.(iFam).Monitor;
% AO.(iFam).Setpoint.MemberOf  = {'PlotFamily'};
% AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/currentPM');
% 
% AO.(iFam).Setpoint.Tolerance(:,:) = 0.05;
% AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.05;

%============
% QUADRUPOLES
%============
%% *** QUADS ***
iFam = 'Q';
AO.(iFam).FamilyType               = 'QUAD';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet';};

HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
     1,	'LT01/PC/Q-01', 1, [1 1], 'Q-01'
     2,	'LT01/PC/Q-02', 1, [1 2], 'Q-02'
     3,	'LT01/PC/Q-03', 1, [1 3], 'Q-03'
     4,	'LT01/PC/Q-04', 1, [1 4], 'Q-04'
     5,	'LT01/PC/Q-05', 1, [1 5], 'Q-05'
     6,	'LT01/PC/Q-06', 1, [1 6], 'Q-06'
     7,	'LT01/PC/Q-07', 1, [1 7], 'Q-07'
     8,	'LT01/PC/Q-08', 1, [1 8], 'Q-08'
     9,	'LT01/PC/Q-09', 1, [1 9], 'Q-09'
     };
 
for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
%     AO.(iFam).Monitor.Range(ii,:)        = [-15 15]; 
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-15 15]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 0.0005;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.2;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%%%
%% ** Injection Septum
%%%

ifam = 'SEINJ'; 
AO.(ifam).FamilyName           = ifam;
AO.(ifam).FamilyType           = 'SEPTUM';
AO.(ifam).MemberOf             = {'Injection';'Septum'};

AO.(ifam).Monitor.MemberOf     = {'PlotFamily';};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

AO.(ifam).Status               = 1;
AO.(ifam).DeviceName           = {'BO/PC/SEINJ'};
AO.(ifam).CommonNames          = 'SEINJ';
AO.(ifam).ElementList          = 1;
AO.(ifam).DeviceList           = [1 1];
AO.(ifam).Monitor.TangoNames     = strcat(AO.(ifam).DeviceName, '/Voltage');
AO.(ifam).Monitor.HW2PhysicsFcn  = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn  = @k2amp;

HW2PhysicsParams = magnetcoefficients(ifam);
Physics2HWParams = HW2PhysicsParams;

AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'V';
AO.(ifam).Monitor.PhysicsUnits     = 'mrad';

AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Setpoint.Range       = [0 120];
AO.(ifam).Setpoint.Tolerance   = 1.e-2;

%%%%%%%%%%%%%%%%%%
%%% Diagnostics
%%%%%%%%%%%%%%%%%%
%% Charge Monitor - Moniteur de charge
ifam = 'BCM';

AO.(ifam).FamilyName             = 'BCM';
AO.(ifam).MemberOf               = {'Diag'; 'MC'; 'Archivable'; 'BCM'};
AO.(ifam).Mode                   = Mode;
AO.(ifam).DeviceName             = {'LT01/DI/BCM-01'; 'LT01/DI/BCM-02'};
AO.(ifam).CommonNames            = ['BCM-01';'BCM-02';];
AO.(ifam).DeviceList(:,:)        = [1 1; 1 2];
AO.(ifam).ElementList            = [1 2]';
AO.(ifam).Status                 = [1 1]';
AO.(ifam).Monitor.TangoNames     = [strcat(AO.(ifam).DeviceName(1,:), '/qIct1'); ...
                                    strcat(AO.(ifam).DeviceName(2,:), '/qIct2')];
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = [NaN; NaN]';
AO.(ifam).Monitor.DataType       = 'Vector';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'nC';
AO.(ifam).Monitor.PhysicsUnits   = 'nC';
AO.(ifam).Monitor.HW2PhysicsParams = 1.0;
AO.(ifam).Monitor.Physics2HWParams = 1.0;



%=============================================
%% BPMx data: status field designates if BPM in use
%=============================================
iFam = 'BPMx';
AO.(iFam).FamilyName               = iFam;
% AO.(iFam).FamilyType               = 'BPM';
AO.(iFam).MemberOf                 = {'PlotFamily'; 'HBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';

bpm={
     1,	'BO01/DI/BPM-01', 1, [1  1], '01BPM01'
     2,	'BO01/DI/BPM-02', 0, [1  2], '01BPM02'
     3,	'BO01/DI/BPM-03', 1, [1  3], '01BPM03'
     4,	'BO01/DI/BPM-04', 0, [1  4], '01BPM04'
     5,	'BO01/DI/BPM-05', 1, [1  5], '01BPM05'
     6,	'BO01/DI/BPM-06', 1, [1  6], '01BPM06'
     7,	'BO01/DI/BPM-07', 1, [1  7], '01BPM07'
     8,	'BO01/DI/BPM-08', 0, [1  8], '01BPM08'
     9,	'BO01/DI/BPM-09', 1, [1  9], '01BPM09'
    10,	'BO01/DI/BPM-10', 0, [1 10], '01BPM10'
    11,	'BO01/DI/BPM-11', 1, [1 11], '01BPM11'
    12,	'BO02/DI/BPM-01', 1, [2  1], '02BPM01'
    13,	'BO02/DI/BPM-02', 1, [2  2], '02BPM02'
    14,	'BO02/DI/BPM-03', 1, [2  3], '02BPM03'
    15,	'BO02/DI/BPM-04', 0, [2  4], '02BPM04'
    16,	'BO02/DI/BPM-05', 1, [2  5], '02BPM05'
    17,	'BO02/DI/BPM-06', 0, [2  6], '02BPM06'
    18,	'BO02/DI/BPM-07', 1, [2  7], '02BPM07'
    19,	'BO02/DI/BPM-08', 0, [2  8], '02BPM08'
    20,	'BO02/DI/BPM-09', 1, [2  9], '02BPM09'
 %   20,	'BO02/DI/BPM-09', 0, [2  9], '02BPM09' % 22 January 2010. Temporal thing.
    21,	'BO02/DI/BPM-10', 0, [2 10], '02BPM10'
    22,	'BO02/DI/BPM-11', 1, [2 11], '02BPM11'
 %   22,	'BO02/DI/BPM-11', 0, [2 11], '02BPM11' % 22 January 2010. Temporal thing.
    23,	'BO03/DI/BPM-01', 1, [3  1], '03BPM01'
    24,	'BO03/DI/BPM-02', 0, [3  2], '03BPM02'
    25,	'BO03/DI/BPM-03', 1, [3  3], '03BPM03'
    26,	'BO03/DI/BPM-04', 0, [3  4], '03BPM04'
    27,	'BO03/DI/BPM-05', 1, [3  5], '03BPM05'
    28,	'BO03/DI/BPM-06', 1, [3  6], '03BPM06'
    29,	'BO03/DI/BPM-07', 1, [3  7], '03BPM07'
    30,	'BO03/DI/BPM-08', 0, [3  8], '03BPM08'
    31,	'BO03/DI/BPM-09', 1, [3  9], '03BPM09'
%    31,	'BO03/DI/BPM-09', 0, [3  9], '03BPM09' %23 Jan 2010. Putting it
    32,	'BO03/DI/BPM-10', 0, [3 10], '03BPM10'
    33,	'BO03/DI/BPM-11', 1, [3 11], '03BPM11'
    34,	'BO04/DI/BPM-01', 1, [4  1], '04BPM01'
    35,	'BO04/DI/BPM-02', 0, [4  2], '04BPM02'
    36,	'BO04/DI/BPM-03', 1, [4  3], '04BPM03'
    37,	'BO04/DI/BPM-04', 1, [4  4], '04BPM04'
    38,	'BO04/DI/BPM-05', 0, [4  5], '04BPM05'
    39,	'BO04/DI/BPM-06', 1, [4  6], '04BPM06'
    40,	'BO04/DI/BPM-07', 1, [4  7], '04BPM07'
    41,	'BO04/DI/BPM-08', 1, [4  8], '04BPM08'
    42,	'BO04/DI/BPM-09', 0, [4  9], '04BPM09'
    43,	'BO04/DI/BPM-10', 0, [4 10], '04BPM10'
    44,	'BO04/DI/BPM-11', 1, [4 11], '04BPM11'
    };

% Load fields from data block
% for ii=1:size(bpm,1)
%     AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
%     AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
%     AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/XposSA');
%     AO.(iFam).Status(ii,:)             = bpm{ii,3};  
%     AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
%     AO.(iFam).CommonNames(ii,:)        = bpm(ii,5);
%     AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
%     AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
% end
% 
% % Scalar channel method
% AO.(iFam).Monitor.DataType = 'Scalar';
% % AO.(iFam).Monitor.Handles = NaN * ones(size(AO.(iFam).DeviceList,1),1);

for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/XPosSA');
    AO.(iFam).Status(ii,:)             = bpm{ii,3};
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';



%=============================================
%% BPMy data: status field designates if BPM in use
%=============================================

iFam = 'BPMy';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'BPM';
AO.(iFam).MemberOf                 = {'PlotFamily'; 'VBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';

bpm={
     1,	'BO01/DI/BPM-01', 1, [1  1], '01BPM01'
     2,	'BO01/DI/BPM-02', 0, [1  2], '01BPM02'
     3,	'BO01/DI/BPM-03', 1, [1  3], '01BPM03'
     4,	'BO01/DI/BPM-04', 0, [1  4], '01BPM04'
     5,	'BO01/DI/BPM-05', 1, [1  5], '01BPM05'
     6,	'BO01/DI/BPM-06', 1, [1  6], '01BPM06'
     7,	'BO01/DI/BPM-07', 1, [1  7], '01BPM07'
     8,	'BO01/DI/BPM-08', 0, [1  8], '01BPM08'
     9,	'BO01/DI/BPM-09', 1, [1  9], '01BPM09'
    10,	'BO01/DI/BPM-10', 0, [1 10], '01BPM10'
    11,	'BO01/DI/BPM-11', 1, [1 11], '01BPM11'
    12,	'BO02/DI/BPM-01', 1, [2  1], '02BPM01'
    13,	'BO02/DI/BPM-02', 1, [2  2], '02BPM02'
    14,	'BO02/DI/BPM-03', 1, [2  3], '02BPM03'
    15,	'BO02/DI/BPM-04', 0, [2  4], '02BPM04'
    16,	'BO02/DI/BPM-05', 1, [2  5], '02BPM05'
    17,	'BO02/DI/BPM-06', 0, [2  6], '02BPM06'
    18,	'BO02/DI/BPM-07', 1, [2  7], '02BPM07'
    19,	'BO02/DI/BPM-08', 0, [2  8], '02BPM08'
    20,	'BO02/DI/BPM-09', 1, [2  9], '02BPM09'
 %   20,	'BO02/DI/BPM-09', 0, [2  9], '02BPM09' % 22 January 2010. Temporal thing.
    21,	'BO02/DI/BPM-10', 0, [2 10], '02BPM10'
   22,	'BO02/DI/BPM-11', 1, [2 11], '02BPM11'
 %   22,	'BO02/DI/BPM-11', 0, [2 11], '02BPM11' % 22 January 2010. Temporal thing.
    23,	'BO03/DI/BPM-01', 1, [3  1], '03BPM01'
    24,	'BO03/DI/BPM-02', 0, [3  2], '03BPM02'
    25,	'BO03/DI/BPM-03', 1, [3  3], '03BPM03'
    26,	'BO03/DI/BPM-04', 0, [3  4], '03BPM04'
    27,	'BO03/DI/BPM-05', 1, [3  5], '03BPM05'
    28,	'BO03/DI/BPM-06', 1, [3  6], '03BPM06'
    29,	'BO03/DI/BPM-07', 1, [3  7], '03BPM07'
    30,	'BO03/DI/BPM-08', 0, [3  8], '03BPM08'
    %31,	'BO03/DI/BPM-09', 0, [3  9], '03BPM09' %putting it back
    31,	'BO03/DI/BPM-09', 1, [3  9], '03BPM09'
    32,	'BO03/DI/BPM-10', 0, [3 10], '03BPM10'
    33,	'BO03/DI/BPM-11', 1, [3 11], '03BPM11'
    34,	'BO04/DI/BPM-01', 1, [4  1], '04BPM01'
    35,	'BO04/DI/BPM-02', 0, [4  2], '04BPM02'
    36,	'BO04/DI/BPM-03', 1, [4  3], '04BPM03'
    37,	'BO04/DI/BPM-04', 1, [4  4], '04BPM04'
    38,	'BO04/DI/BPM-05', 0, [4  5], '04BPM05'
    39,	'BO04/DI/BPM-06', 1, [4  6], '04BPM06'
    40,	'BO04/DI/BPM-07', 1, [4  7], '04BPM07'
    41,	'BO04/DI/BPM-08', 1, [4  8], '04BPM08'
    42,	'BO04/DI/BPM-09', 0, [4  9], '04BPM09'
    43,	'BO04/DI/BPM-10', 0, [4 10], '04BPM10'
    44,	'BO04/DI/BPM-11', 1, [4 11], '04BPM11'
    };

%Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/ZPosSA');
    AO.(iFam).Status(ii,:)             = bpm{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';
% AO.(iFam).Monitor.Handles = NaN * ones(size(AO.(iFam).DeviceList,1),1);

%===========================================================
%% HCM
%===========================================================

iFam ='HCM';
AO.(iFam).FamilyName               = iFam;
% AO.(iFam).FamilyType               = 'COR';
AO.(iFam).MemberOf                 = {'COR'; 'HCOR'; 'HCM'; 'Magnet'; 'CORH'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;

cor={
     1,	'BO01/PC/CORH-01', 1, [1  1], '01CORH01'
     2,	'BO01/PC/CORH-02', 1, [1  2], '01CORH02'
     3,	'BO01/PC/CORH-03', 1, [1  3], '01CORH03'
     4,	'BO01/PC/CORH-04', 1, [1  4], '01CORH04'
     5,	'BO01/PC/CORH-05', 1, [1  5], '01CORH05'
     6,	'BO01/PC/CORH-06', 1, [1  6], '01CORH06'
     7,	'BO01/PC/CORH-07', 1, [1  7], '01CORH07'
     8,	'BO01/PC/CORH-08', 1, [1  8], '01CORH08'
     9,	'BO01/PC/CORH-09', 1, [1  9], '01CORH09'
    10,	'BO01/PC/CORH-10', 1, [1 10], '01CORH10'
    11,	'BO01/PC/CORH-11', 1, [1 11], '01CORH11'
    12,	'BO02/PC/CORH-01', 1, [2  1], '02CORH01'
    13,	'BO02/PC/CORH-02', 1, [2  2], '02CORH02'
    14,	'BO02/PC/CORH-03', 1, [2  3], '02CORH03'
    15,	'BO02/PC/CORH-04', 1, [2  4], '02CORH04'
    16,	'BO02/PC/CORH-05', 1, [2  5], '02CORH05'
    17,	'BO02/PC/CORH-06', 1, [2  6], '02CORH06'
    18,	'BO02/PC/CORH-07', 1, [2  7], '02CORH07'
    19,	'BO02/PC/CORH-08', 1, [2  8], '02CORH08'
    20,	'BO02/PC/CORH-09', 1, [2  9], '02CORH09'
    21,	'BO02/PC/CORH-10', 1, [2 10], '02CORH10'
    22,	'BO02/PC/CORH-11', 1, [2 11], '02CORH11'
    23,	'BO03/PC/CORH-01', 1, [3  1], '03CORH01'
    24,	'BO03/PC/CORH-02', 1, [3  2], '03CORH02'
    25,	'BO03/PC/CORH-03', 1, [3  3], '03CORH03'
    26,	'BO03/PC/CORH-04', 1, [3  4], '03CORH04'
    27,	'BO03/PC/CORH-05', 1, [3  5], '03CORH05'
    28,	'BO03/PC/CORH-06', 1, [3  6], '03CORH06'
    29,	'BO03/PC/CORH-07', 1, [3  7], '03CORH07'
    30,	'BO03/PC/CORH-08', 1, [3  8], '03CORH08'
    31,	'BO03/PC/CORH-09', 1, [3  9], '03CORH09'
    32,	'BO03/PC/CORH-10', 1, [3 10], '03CORH10'
    33,	'BO03/PC/CORH-11', 1, [3 11], '03CORH11'
    34,	'BO04/PC/CORH-01', 1, [4  1], '04CORH01'
    35,	'BO04/PC/CORH-02', 1, [4  2], '04CORH02'
    36,	'BO04/PC/CORH-03', 1, [4  3], '04CORH03'
    37,	'BO04/PC/CORH-04', 1, [4  4], '04CORH04'
    38,	'BO04/PC/CORH-05', 1, [4  5], '04CORH05'
    39,	'BO04/PC/CORH-06', 1, [4  6], '04CORH06'
    40,	'BO04/PC/CORH-07', 1, [4  7], '04CORH07'
    41,	'BO04/PC/CORH-08', 1, [4  8], '04CORH08'
    42,	'BO04/PC/CORH-09', 1, [4  9], '04CORH09'
    43,	'BO04/PC/CORH-10', 1, [4 10], '04CORH10'
    44,	'BO04/PC/CORH-11', 1, [4 11], '04CORH11'
    };

%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients('HCM');

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-6 6]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-3;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.02;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
%     AO.(iFam).Monitor.Handles(ii,1)    = NaN;
%     AO.(iFam).Setpoint.Handles(ii,1)   = NaN;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%% VCM

iFam ='VCM';

AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'COR';
AO.(iFam).MemberOf                 = {'COR'; 'VCOR'; 'VCM'; 'Magnet'; 'CORV'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;

cor={
     1,	'BO01/PC/CORV-01', 1, [1  1], '01CORV01'
     2,	'BO01/PC/CORV-03', 1, [1  2], '01CORV03'
     3,	'BO01/PC/CORV-05', 1, [1  3], '01CORV05'
     4,	'BO01/PC/CORV-06', 1, [1  4], '01CORV06'
     5,	'BO01/PC/CORV-07', 1, [1  5], '01CORV07'
     6,	'BO01/PC/CORV-09', 1, [1  6], '01CORV09'
     7,	'BO01/PC/CORV-11', 1, [1  7], '01CORV11'
     8,	'BO02/PC/CORV-01', 1, [2  1], '02CORV01'
     9,	'BO02/PC/CORV-03', 1, [2  2], '02CORV03'
    10,	'BO02/PC/CORV-05', 1, [2  3], '02CORV05'
    11,	'BO02/PC/CORV-06', 1, [2  4], '02CORV06'
    12,	'BO02/PC/CORV-07', 1, [2  5], '02CORV07'
    13,	'BO02/PC/CORV-09', 1, [2  6], '02CORV09'
    14,	'BO02/PC/CORV-11', 1, [2  7], '02CORV11'
    15,	'BO03/PC/CORV-01', 1, [3  1], '03CORV01'
    16,	'BO03/PC/CORV-03', 1, [3  2], '03CORV03'
    17,	'BO03/PC/CORV-05', 1, [3  3], '03CORV05'
    18,	'BO03/PC/CORV-06', 1, [3  4], '03CORV06'
    19,	'BO03/PC/CORV-07', 1, [3  5], '03CORV07'
    20,	'BO03/PC/CORV-09', 1, [3  6], '03CORV09'
    21,	'BO03/PC/CORV-11', 1, [3  7], '03CORV11'
    22,	'BO04/PC/CORV-01', 1, [4  1], '04CORV01'
    23,	'BO04/PC/CORV-03', 1, [4  2], '04CORV03'
    24,	'BO04/PC/CORV-05', 1, [4  3], '04CORV05'
    25,	'BO04/PC/CORV-06', 1, [4  4], '04CORV06'
    26,	'BO04/PC/CORV-07', 1, [4  5], '04CORV07'
    27,	'BO04/PC/CORV-09', 1, [4  6], '04CORV09'
    28,	'BO04/PC/CORV-11', 1, [4  7], '04CORV11'
    };

% Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients('VCM');

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-6 6]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-3;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.02;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
%     AO.(iFam).Monitor.Handles(ii,1)    = NaN;
%     AO.(iFam).Setpoint.Handles(ii,1)   = NaN;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%=============================
%        MAIN MAGNETS
%=============================

%===========
% Dipole data
%===========
%% *** BEND ***
iFam = 'BEND';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'BEND'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('BEND');
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';

AO.(iFam).DeviceName(:,:) = {'bo/pc/bend-1'};
AO.(iFam).Monitor.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName(:,:),'/Current');

AO.(iFam).DeviceList(:,:) = [1 1];
AO.(iFam).ElementList(:,:)= 1;
AO.(iFam).Status          = 1;

val = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(iFam).Monitor.Range(:,:) = [0 700]; % 670 A for 1.4214 T @ 3 GeV

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
% AO.(iFam).Desired  = AO.(iFam).Monitor;
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/Current');

AO.(iFam).Setpoint.Tolerance(:,:) = 1.e-2;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.5;

%============
% QUADRUPOLES
%============
%% *** QH01 ***
iFam = 'QH01';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';};

HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).ElementList(:,:)        = 1;
AO.(iFam).DeviceName(:,:)         = {'BO/PC/QH01'};
AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/Current');
AO.(iFam).Status             = 1;
AO.(iFam).DeviceList(:,:)         = [1 1];
AO.(iFam).CommonNames        = iFam;

val = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(iFam).Monitor.Range(:,:) = [-180 180];

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/Current');

AO.(iFam).Setpoint.Tolerance(:,:)    = 1E-4;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.01;

%% *** QH02 ***
iFam = 'QH02';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';};

HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).ElementList(:,:)        = 1;
AO.(iFam).DeviceName(:,:)         = {'BO/PC/QH02'};
AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/Current');
AO.(iFam).Status             = 1;
AO.(iFam).DeviceList(:,:)         = [1 1];
AO.(iFam).CommonNames        = iFam;

val = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(iFam).Monitor.Range(:,:) = [-180 180];

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/Current');

AO.(iFam).Setpoint.Tolerance(:,:)    = 1E-4;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.01;

%% *** QV01 ***
iFam = 'QV01';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';};

HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).ElementList(:,:)        = 1;
AO.(iFam).DeviceName(:,:)         = {'BO/PC/QV01'};
AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/Current');
AO.(iFam).Status             = 1;
AO.(iFam).DeviceList(:,:)         = [1 1];
AO.(iFam).CommonNames        = iFam;

val = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(iFam).Monitor.Range(:,:) = [-180 180];

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/Current');

AO.(iFam).Setpoint.Tolerance(:,:)    = 1E-4;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.01;

%% *** QV02 ***
iFam = 'QV02';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';};

HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).ElementList(:,:)        = 1;
AO.(iFam).DeviceName(:,:)         = {'BO/PC/QV02'};
AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/Current');
AO.(iFam).Status             = 1;
AO.(iFam).DeviceList(:,:)         = [1 1];
AO.(iFam).CommonNames        = iFam;

val = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(iFam).Monitor.Range(:,:) = [-180 180];

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/Current');

AO.(iFam).Setpoint.Tolerance(:,:)    = 1E-4;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.01;

%===============
% Sextupole data
%===============
%% *** SH ***
iFam = 'SH';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector';};
%AO.(iFam).FamilyType                 = 'SEXT';
HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';

AO.(iFam).ElementList               = 1;
AO.(iFam).DeviceName    = 'BO/PC/SH';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');
AO.(iFam).Status          = 1;
AO.(iFam).DeviceList                = [1 1];
AO.(iFam).CommonNames   = iFam;
% AO.(iFam).Monitor.Handles = NaN;

val = 1.0; % scaling factor

AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint = AO.(iFam).Monitor;
% AO.(iFam).Desired  = AO.(iFam).Monitor;
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');

AO.(iFam).Monitor.Range(:,:) = [0 8]; 
AO.(iFam).Setpoint.Tolerance     = 1.e-3;
AO.(iFam).Setpoint.DeltaRespMat  = 0.1; % Hardware units (gets set later) 

% AO.(iFam).Setpoint.DeltaRespMat  = 2e7; % Physics units for a thin sextupole

%convert response matrix kicks to HWUnits (after AO is loaded to AppData)
% setao(AO);   %required to make physics2hw function
% AO.(iFam).Setpoint.DeltaRespMat=physics2hw(AO.(iFam).FamilyName,'Setpoint',AO.(iFam).Setpoint.DeltaRespMat,AO.(iFam).DeviceList);

%% *** SV ***
iFam = 'SV';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector'};
% AO.(iFam).FamilyType                 = 'SEXT';
HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';

AO.(iFam).ElementList               = 1;
AO.(iFam).DeviceName    = 'BO/PC/SV';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');
AO.(iFam).Status          = 1;
AO.(iFam).DeviceList                = [1 1];
AO.(iFam).CommonNames   = iFam;
% AO.(iFam).Monitor.Handles = NaN;

val = 1.0; % scaling factor

AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint = AO.(iFam).Monitor;
% AO.(iFam).Desired  = AO.(iFam).Monitor;
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');

AO.(iFam).Monitor.Range(:,:) = [0 8]; 
AO.(iFam).Setpoint.Tolerance     = 1.e-3;
AO.(iFam).Setpoint.DeltaRespMat  = 0.1; % Hardware units (gets set later) 

%====
%% DCCT
%====
AO.DCCT.FamilyName                     = 'DCCT';
AO.DCCT.MemberOf                       = {'DCCT'};
AO.DCCT.DeviceName             = {'BO03/di/DCCT-CDI0903'};
AO.DCCT.CommonNames                    = 'DCCT';
AO.DCCT.DeviceList                     = [1 1];
AO.DCCT.ElementList                    = 1;
AO.DCCT.Status                         = 1;


AO.DCCT.Monitor.TangoNames     = [strcat(AO.DCCT.DeviceName(1,:), '/AverageCurrent')];
AO.DCCT.Monitor.Mode                   = Mode;
AO.DCCT.Monitor.DataType               = 'Scalar';  
AO.DCCT.Monitor.Units                  = 'Hardware';
AO.DCCT.Monitor.HWUnits                = 'milli-ampere';           
AO.DCCT.Monitor.PhysicsUnits           = 'ampere';
AO.DCCT.Monitor.HW2PhysicsParams       = 1;          
AO.DCCT.Monitor.Physics2HWParams       = 1;

%============
%% RF System
%============
AO.RF.FamilyName                  = 'RF';
AO.RF.MemberOf                    = {'RF'};
AO.RF.ElementList                 = 1;
AO.RF.Status                      = 1;
AO.RF.DeviceList                  = [1 1];
AO.RF.DeviceName                  = 'SR09/rf/sgn-01';
AO.RF.CommonNames                 = 'RF';

AO.RF.Monitor.MemberOf           = {};
AO.RF.Monitor.Mode                = Mode;
AO.RF.Monitor.DataType            = 'Scalar';
AO.RF.Monitor.Units               = 'Hardware';
AO.RF.Monitor.HW2PhysicsParams    = 1;
AO.RF.Monitor.Physics2HWParams    = 1;
AO.RF.Monitor.HWUnits             = 'Hz';
AO.RF.Monitor.PhysicsUnits        = 'Hz';
% AO.RF.Monitor.HW2PhysicsParams    = 1e+6;
% AO.RF.Monitor.Physics2HWParams    = 1e-6;
% AO.RF.Monitor.HWUnits             = 'MHz';
% AO.RF.Monitor.PhysicsUnits        = 'Hz';
AO.RF.Monitor.TangoNames        = [strcat(AO.RF.DeviceName), '/Frequency'];

AO.RF.Setpoint.MemberOf           = {'MachineConfig'};
AO.RF.Setpoint.Mode               = Mode;
AO.RF.Setpoint.DataType           = 'Scalar';
AO.RF.Setpoint.Units              = 'Hardware';
AO.RF.Setpoint.HW2PhysicsParams   = 1;
AO.RF.Setpoint.Physics2HWParams   = 1;
AO.RF.Setpoint.HWUnits            = 'Hz';
AO.RF.Setpoint.PhysicsUnits       = 'Hz';
% AO.RF.Setpoint.HW2PhysicsParams   = 1e+6;
% AO.RF.Setpoint.Physics2HWParams   = 1e-6;
% AO.RF.Setpoint.HWUnits            = 'MHz';
% AO.RF.Setpoint.PhysicsUnits       = 'Hz';
AO.RF.Setpoint.TangoNames       =  [strcat(AO.RF.DeviceName), '/Frequency'];
AO.RF.Setpoint.Range              = [499E6 501E6];
AO.RF.Setpoint.Tolerance          = 1;

%====
%% TUNE
%====
AO.TUNE.FamilyName  = 'TUNE';
AO.TUNE.MemberOf    = {'Diagnostics'; 'TUNE'};
AO.TUNE.ElementList = [1 2]; % 3]';
AO.TUNE.Status      = [1 1]; % 0]';
AO.TUNE.DeviceList  = [ 1 1; 1 2]; %; 1 3];
AO.TUNE.CommonNames = ['xtune';'ytune']; %;'stune'];

AO.TUNE.DeviceName             = {'bo/DI/TuneBPM';'bo/DI/ZTune'}; %;'BO/DI/BPM-TUNEZ'};

AO.TUNE.Monitor.MemberOf         = {};
AO.TUNE.Monitor.Mode                   = Mode; 
AO.TUNE.Monitor.DataType               = 'Scalar';
%AO.TUNE.Monitor.TangoNames       = ['bo/DI/TuneBPM/Nu';'bo/DI/ZTune/Nu']; %;'BO/DI/BPM-TUNEZ/Nu'];
AO.TUNE.Monitor.TangoNames     = [strcat(AO.TUNE.DeviceName(1,:), '/Nu'); ...
                                    strcat(AO.TUNE.DeviceName(2,:), '/Nu')];

AO.TUNE.Monitor.Units                  = 'Hardware';
AO.TUNE.Monitor.HW2PhysicsParams       = 1;
AO.TUNE.Monitor.Physics2HWParams       = 1;
AO.TUNE.Monitor.HWUnits                = 'fractional tune';           
AO.TUNE.Monitor.PhysicsUnits           = 'fractional tune';


%%
% The operational mode sets the path, filenames, and other important parameters
% Run setoperationalmode after most of the AO is built so that the Units and Mode fields
% can be set in setoperationalmode
setao(AO);
setoperationalmode(OperationalMode);
AO = getao;

%======================================================================
%% Append Accelerator Toolbox information
%======================================================================
%======================================================================
% disp('** Initializing Accelerator Toolbox information');
% 
% 
% 
% ATindx = atindex(THERING);  %structure with fields containing indices
% 
% s = findspos(THERING,1:length(THERING)+1)';
% 
% %% Horizontal BPMs
% iFam = ('BPMx');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BPM(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% Vertical BPMs
% iFam = ('BPMy');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BPM(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);  
% 
% %% HORIZONTAL CORRECTORS
% iFam = ('HCM');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% VERTICAL CORRECTORS
% iFam = ('VCM');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% BENDING magnets
% for k = 1:2,
%     iFam = ['BM0' num2str(k)];
%     AO.(iFam).AT.ATType  = 'BEND';
%     AO.(iFam).AT.ATIndex = eval(['ATindx.' iFam '(:)']);
%     AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% end
% 
% % One group of all dipoles
% %AO.(iFam).Position   = reshape(s(AO.(iFam).AT.ATIndex),1,40);
% %AT.(iFam).AT.ATParamGroup = mkparamgroup(THERING,AT.(iFam).AT.ATIndex,'K2');
% 
% %% QUADRUPOLES
% iFam = ('Q');
% AO.(iFam).AT.ATType  = 'QUAD';
% AO.(iFam).AT.ATIndex = eval(['ATindx.' iFam '(:)']);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);

%%

AO.HCM.Setpoint.DeltaRespMat = physics2hw('HCM','Setpoint', 1e-4, AO.HCM.DeviceList);
AO.VCM.Setpoint.DeltaRespMat = physics2hw('VCM','Setpoint', 1e-4, AO.VCM.DeviceList);

% AO.Q.Setpoint.DeltaRespMat  = physics2hw('Q', 'Setpoint', AO.Q.Setpoint.DeltaRespMat,  AO.Q.DeviceList);

setao(AO);

% % reference values
% global RefOptics;
% %disp '    Reference optics, tunes and AO stored in RefOptics'
% RefOptics.AO=getao();
% RefOptics.twiss=gettwiss();
% RefOptics.tune= gettune();
