function updateatindex
%UPDATEATINDEX - Updates the AT indices in the MiddleLayer with the present AT lattice (THERING)

global THERING

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Append Accelerator Toolbox information %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Since changes in the AT model could change the AT indexes, etc,
% It's best to regenerate all the model indices whenever a model is loaded

% Sort by family first (findcells is linear and slow)
Indices = atindex(THERING);

AO = getao;

try
    AO.BPMx.AT.ATType = 'X';
    AO.BPMx.AT.ATIndex = Indices.BPM(:); % findcells(THERING,'FamName','BPM')';
    AO.BPMx.Position = findspos(THERING, AO.BPMx.AT.ATIndex)';

    AO.BPMy.AT.ATType = 'Y';
    AO.BPMy.AT.ATIndex = Indices.BPM(:); % findcells(THERING,'FamName','BPM')';
    AO.BPMy.Position = findspos(THERING, AO.BPMy.AT.ATIndex)';
catch
    fprintf('   BPM family not found in the model.\n');
end

try
    % Horizontal correctors
    AO.HCM.AT.ATType  = 'HCM';
    AO.HCM.AT.ATIndex = buildatindex(AO.HCM.FamilyName, Indices.HCM);
    AO.HCM.Position = findspos(THERING, AO.HCM.AT.ATIndex)';
    
    % Vertical correctors
    AO.VCM.AT.ATType = 'VCM';
    AO.VCM.AT.ATIndex = buildatindex(AO.VCM.FamilyName, Indices.VCM);
    AO.VCM.Position = findspos(THERING, AO.VCM.AT.ATIndex)';
catch
    fprintf('   COR family not found in the model.\n');
end

try
    AO.QH01.AT.ATType = 'QUAD';
%     ATIndex = [Indices.QH01(:)];
%     AO.QH01.AT.ATIndex  = sort(ATIndex);
    AO.QH01.AT.ATIndex = buildatindex(AO.QH01.FamilyName, Indices.QH01);
    AO.QH01.Position = findspos(THERING, AO.QH01.AT.ATIndex(:,1))';
catch
    fprintf('   QH01 family not found in the model.\n');
end

try
    AO.QH02.AT.ATType = 'QUAD';
%     ATIndex = [Indices.QH02(:)];
%     AO.QH02.AT.ATIndex  = sort(ATIndex);
    AO.QH02.AT.ATIndex = buildatindex(AO.QH02.FamilyName, Indices.QH02);
    AO.QH02.Position = findspos(THERING, AO.QH02.AT.ATIndex(:,1))';
catch
    fprintf('   QH02 family not found in the model.\n');
end

try
    AO.QV01.AT.ATType = 'QUAD';
%     ATIndex = [Indices.QV01(:)];
%     AO.QV01.AT.ATIndex  = sort(ATIndex);
    AO.QV01.AT.ATIndex = buildatindex(AO.QV01.FamilyName, Indices.QV01);
    AO.QV01.Position = findspos(THERING, AO.QV01.AT.ATIndex(:,1))';
catch
    fprintf('   QV01 family not found in the model.\n');
end

try
    AO.QV02.AT.ATType = 'QUAD';
%     ATIndex = [Indices.QV02(:)];
%     AO.QV02.AT.ATIndex  = sort(ATIndex);
    AO.QV02.AT.ATIndex = buildatindex(AO.QV02.FamilyName, Indices.QV02);
    AO.QV02.Position = findspos(THERING, AO.QV02.AT.ATIndex(:,1))';
catch
    fprintf('   QV02 family not found in the model.\n');
end

try
    AO.SH.AT.ATType = 'SEXT';
    AO.SH.AT.ATIndex = buildatindex(AO.SH.FamilyName, Indices.SH);
    AO.SH.Position = findspos(THERING, AO.SH.AT.ATIndex(:,1))';
catch
    fprintf('   SH family not found in the model.\n');
end

try
    AO.SV.AT.ATType = 'SEXT';
    AO.SV.AT.ATIndex = buildatindex(AO.SV.FamilyName, Indices.SV);
    AO.SV.Position = findspos(THERING, AO.SV.AT.ATIndex(:,1))';
catch
    fprintf('   SV family not found in the model.\n');
end

try
    AO.BEND.AT.ATType = 'BEND';
    AO.BEND.AT.ATIndex = buildatindex(AO.BEND.FamilyName, Indices.BEND);
    AO.BEND.Position = findspos(THERING, AO.BEND.AT.ATIndex(:,1))';
catch
    fprintf('   BEND family not found in the model.\n');
end

try
    % RF Cavity
    AO.RF.AT.ATType = 'RF Cavity';
    AO.RF.AT.ATIndex = findcells(THERING,'Frequency')';
    AO.RF.Position = findspos(THERING, AO.RF.AT.ATIndex(:,1))';
catch
    fprintf('   RF cavity not found in the model.\n');
end

setao(AO);
