function [C, Leff, MagnetType, A] = magnetcoefficients(MagnetCoreType, Amps, InputType)
%MAGNETCOEFFICIENTS - Retrieves coefficient for conversion between Physics and Hardware units
%[C, Leff, MagnetType, A] = magnetcoefficients(MagnetCoreType)
%
% INPUTS
% 1. MagnetCoreType - Family name or type of magnet
%
% OUTPUTS
% 1. C vector coefficients for the polynomial expansion of the magnet field
%    based on magnet measurements
% 2. Leff - Effective length ie, which is used in AT
% 3. MagnetType
% 4. A - vector coefficients for the polynomial expansion of the curviline
%        integral of the magnet field based on magnet measurements
%
% C and A are vector coefficients for the polynomial expansion of the magnet field
% based on magnet measurements.
%
% The amp2k and k2amp functions convert between the two types of units.
%   amp2k returns BLeff, B'Leff, or B"Leff scaled by Brho if A-coefficients are used.
%   amp2k returns B    , B'    , or B"     scaled by Brho if C-coefficients are used.
%
% The A coefficients are direct from magnet measurements with a DC term:
%   a8*I^8+a7*I^7+a6*I^6+a5*I^5+a4*I^4+a3*I^3+a2*I^2+a1*I+a0 = B*Leff or B'*Leff or B"*Leff
%   A = [a8 a7 a6 a5 a4 a3 a2 a1 a0]
%
% C coefficients have been scaled to field (AT units, except correctors) and includes a DC term:
%   c8 * I^8+ c7 * I^7+ c6 * I^6 + c5 * I^5 + c4 * I^4 + c3 * I^3 + c2 * I^2 + c1*I + c0 = B or B' or B"
%   C = A/Leff
%
% For dipole:      k = B / Brho      (for AT: KickAngle = BLeff / Brho)
% For quadrupole:  k = B'/ Brho
% For sextupole:   k = B"/ Brho / 2  (to be compatible with AT)
%                  (all coefficients all divided by 2 for sextupoles)
%
% MagnetCoreType is the magnet measurements name for the magnet core (string, string matrix, or cell)
%   For SOLEIL:   BEND
%                 Q1 - Q10 S1 - S10,
%                 QT, HCOR, VCOR, FHCOR, FVCOR
%
% Leff is the effective length of the magnet
%
% See Also amp2k, k2amp

%
% Written by M. Yoon 4/8/03
% Adapted By Laurent S. Nadolski
%

% NOTE: Make sure the sign on the 'C' coefficients is reversed where positive current generates negative K-values
% Or use Tango K value set to -1


if nargin < 1
    error('MagnetCoreType input required');
end

if nargin < 2
    Amps = 230;  % not sure!!!
end

if nargin < 3
    InputType = 'Amps';
end

% For a string matrix
if iscell(MagnetCoreType)
    for i = 1:size(MagnetCoreType,1)
        for j = 1:size(MagnetCoreType,2)
            [C{i,j}, Leff{i,j}, MagnetType{i,j}, A{i,j}] = magnetcoefficients(MagnetCoreType{i});
        end
    end
    return
end

% For a string matrix
if size(MagnetCoreType,1) > 1
    C=[]; Leff=[]; MagnetType=[]; A=[];
    for i = 1:size(MagnetCoreType,1)
        [C1, Leff1, MagnetType1, A1] = magnetcoefficients(MagnetCoreType(i,:));
        C(i,:) = C1;
        Leff(i,:) = Leff1;
        MagnetType = strvcat(MagnetType, MagnetType1);
        A(i,:) = A1;
    end
    return
end

switch upper(deblank(MagnetCoreType))

    case 'BEND'
        % B = 0.870 T for I = 670 A
        Leff = 2.0;
%          a7= 0.0;
%          a6=-0.0;
%          a5= 0.0;
%          a4=-0.0;
%          a3= 0.0;
%          a2= 0.0;
%          a1= 2.65187e-3;  %10*pi/180*getbrho(3)/670;  %0.870*Leff/670;
%          a0= 3.24e-3;
%          a7= 0.0;
%          a6=-0.0;
%          a5= 0.0;
%          a4=-0.0;
%          a3= 0.0;
%          a2= -5.7141e-8;
%          a1= 2.6915e-3;  %10*pi/180*getbrho(3)/670;  %0.870*Leff/670;
%          a0= 7.9981e-4;
% Calibration for I < 44 A. To be updated for higher currents.
%         a7= 0.0;
%         a6=-0.0;
%         a5= 0.0;
%         a4=-0.0;
%         a3= 0.0;
%         a2=-1.29120e-6;
%         a1= 2.76060e-3;  %10*pi/180*getbrho(3)/670;  %0.870*Leff/670;
%         a0= 0.0;
%          a7= 0.0;
%          a6= 0.0;
%          a5= 0.0;
%          a4= 0.0;
%          a3=-7.5680e-11;
%          a2= 1.6428e-8;
%          a1= 2.6747e-3;
%          a0= 1.2663e-3;
         a7= 0.0;
         a6= 0.0;
         a5= -8.5678e-16;
         a4= 1.2627e-12;
         a3=-7.3946e-10;
         a2= 1.8143e-07;
         a1= 2.6381e-03;
         a0= 9.7150e-04;

        A = [a7 a6 a5 a4 a3 a2 a1 a0];

        MagnetType = 'BEND';

    case 'QH01'
        % k=2.3214 %3.0952 for I = 180 A @ 3 GeV

        % Find the current from the given polynomial for B'Leff
        Leff=0.360;

%         a7=  0.0;
%         a6=  0.0;
%         a5=  0.0;
%         a4=  0.0;
%         a3=  0.0;
%         a2=  0.0;
%         a1=  4.66853e-2; % kL*Brho/I
%         a0=  1.15074e-2;
        a7=  0.0;
        a6=  7.4807e-14;
        a5= -4.3964e-11;
        a4=  9.8792e-9;
        a3= -1.0776e-6;
        a2=  5.8764e-5;
        a1=  4.5314e-2; % kL*Brho/I
        a0=  1.4996e-2;
        A = [a7 a6 a5 a4 a3 a2 a1 a0];

        MagnetType = 'QUAD';

    case 'QH02'
        % k=2.3373 %3.116 for I = 180 A @ 3 GeV

        % Find the current from the given polynomial for B'Leff
        Leff=0.360;

%         a7=  0.0;
%         a6=  0.0;
%         a5=  0.0;
%         a4=  0.0;
%         a3=  0.0;
%         a2=  0.0;
%         a1=  4.66745e-2; % kL*Brho/I
%         a0=  1.49321e-2;
        a7=  0.0;
        a6= -9.9491e-14;
        a5=  4.7459e-11;
        a4= -8.0217e-9;
        a3=  5.3676e-7;
        a2= -8.5245e-6;
        a1=  4.6458e-2; % kL*Brho/I
        a0=  1.3129e-2;

        A = [a7 a6 a5 a4 a3 a2 a1 a0];

        MagnetType = 'QUAD';

    case {'QV01','QV02'}
        % k=3.1025 for I = 180 A @ 3 GeV
        % Find the current from the given polynomial for B'Leff
        Leff=0.20;

%         a7=  0.0;
%         a6=  0.0;
%         a5=  0.0;
%         a4=  0.0;
%         a3=  0.0;
%         a2=  0.0;
%         a1= -2.58983e-2; %2.3269*Leff*getbrho(3)/180; % kL*Brho/I
%         a0= -0.89114e-2;
        a7=  0.0;
        a6= -4.5810e-14;
        a5=  2.1502e-11;
        a4= -3.5489e-9;
        a3=  2.2117e-7;
        a2= -1.9722e-6;
        a1= -2.5780e-2; %2.3269*Leff*getbrho(3)/180; % kL*Brho/I
        a0= -7.3334e-3;

        A = [a7 a6 a5 a4 a3 a2 a1 a0];

        MagnetType = 'QUAD';

%      case 'QH01'
%          % k=2.3214 %3.0952 for I = 180 A @ 3 GeV
%  
%          % Find the current from the given polynomial for B'Leff
%          Leff=0.360;
%  
%          a7=  0.0;
%          a6=  0.0;
%          a5=  0.0;
%          a4=  0.0;
%          a3=  0.0;
%          a2=  0.0;
%          a1=  4.6734e-2; % kL*Brho/I
%          a0=  0.0;
%  
%          A = [a7 a6 a5 a4 a3 a2 a1 a0];
%  
%          MagnetType = 'QUAD';
%  
%      case 'QH02'
%          % k=2.3373 %3.116 for I = 180 A @ 3 GeV
%  
%          % Find the current from the given polynomial for B'Leff
%          Leff=0.360;
%  
%  
%          a7=  0.0;
%          a6=  0.0;
%          a5=  0.0;
%          a4=  0.0;
%          a3=  0.0;
%          a2=  0.0;
%          a1=  4.6904e-2; % kL*Brho/I
%          a0=  0.0;
%  
%          A = [a7 a6 a5 a4 a3 a2 a1 a0];
%  
%          MagnetType = 'QUAD';
%          
%  
%      case {'QV01','QV02'}
%          % k=3.1025 for I = 180 A @ 3 GeV
%          % Find the current from the given polynomial for B'Leff
%          Leff=0.20;
%  
%          a7=  0.0;
%          a6=  0.0;
%          a5=  0.0;
%          a4=  0.0;
%          a3=  0.0;
%          a2=  0.0;
%          a1=  2.5971e-2; %2.3269*Leff*getbrho(3)/180; % kL*Brho/I
%          a0=  0.0;
%  
%          A = [a7 a6 a5 a4 a3 a2 a1 a0];
%  
%          MagnetType = 'QUAD';


    case {'SH', 'SV'}
        % SL=88 for I = 6.33 A
        % Find the current from the given polynomial for B''Leff
        Leff=0.200; 
        a7=  0.0;
        a6=  0.0;
        a5=  0.0;
        a4=  0.0;
        a3=  0.0;
        a2=  0.0;
        a1=  14.22; %SL/I with S=B"
        a0=  0.0;
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        MagnetType = 'SEXT';


    case {'HCM', 'VCM'}    % 100 mm horizontal corrector
        Leff = 0.100; % To be changed for ALBA if thick correctors
        a7= 0.0;
        a6= 0.0;
        a5= 0.0;
        a4= 0.0;
        a3= 0.0;
        a2= 0.0;
        a1= 1.245e-3; % BL/I
        a0= 0.0;
        A = [a7 a6 a5 a4 a3 a2 a1 a0];

        MagnetType = 'COR';


    otherwise
        error('MagnetCoreType %s is not unknown', MagnetCoreType);
        k = 0;
        MagnetType = '';
        return
end

% compute B-field = int(Bdl)/Leff
C = A / Leff;

MagnetType = upper(MagnetType);

% Power Series Denominator (Factoral) be AT compatible
if strcmpi(MagnetType,'SEXT')
    C = C / 2;
end
if strcmpi(MagnetType,'OCTO')
    C = C / 6;
end
