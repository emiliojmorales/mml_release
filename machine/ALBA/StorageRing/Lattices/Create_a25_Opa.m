function Create_a25_Opa
%ALBA-25 example lattice definition file
% Created 11/21/99
%

% new lenghts
Leff_QD1=0.229461036229344;
Leff_QF4=0.229925397373565;
Leff_QF1=0.290162591216293;
Leff_QF2=0.289076322974338;
Leff_QF3=0.290050455701024;
Leff_QD2=0.289137158793229;
Leff_QD3=0.289210179039298;
Leff_QF5=0.309585300051539;
Leff_QF8=0.308817938383968;
Leff_QF6=0.527653513646467;
Leff_QF7=0.528184007720045;


Leff_SD1=0.17086697576;
Leff_SD2=0.17041858771;
Leff_SD3=0.17000351367;
Leff_SD5=0.16991020052;
Leff_SF1=0.172958010486228;
Leff_SF4=0.171819968084414;
Leff_SD4=0.240773319561292;
Leff_SF2=0.240764887993104;
Leff_SF3=0.240698339722994;

Leff_QD1_0=0.23;
Leff_QF4_0=0.23;
Leff_QF1_0=0.29;
Leff_QF2_0=0.29;
Leff_QF3_0=0.29;
Leff_QD2_0=0.29;
Leff_QD3_0=0.29;
Leff_QF5_0=0.31;
Leff_QF8_0=0.31;
Leff_QF6_0=0.53;
Leff_QF7_0=0.53;


Leff_SD1_0=0.15;
Leff_SD2_0=0.15;
Leff_SD3_0=0.15;
Leff_SD5_0=0.15;
Leff_SF1_0=0.15;
Leff_SF4_0=0.15;
Leff_SD4_0=0.22;
Leff_SF2_0=0.22;
Leff_SF3_0=0.22;

D_BPM_1=(0.2-Leff_QD1/2);
D_BPM_2=(0.163-Leff_SD1/2);
D_BPM_3=(0.212-Leff_QF3/2);
D_BPM_4=(0.229-Leff_QF5/2);
D_BPM_5=(0.162-Leff_SD3/2);
D_BPM_6=((0.1968+0.1978+0.1968)/3-Leff_SD4/2);
D_BPM_7=((0.2168+0.2258+0.2168)/3-Leff_QD2/2);
D_BPM_8=((0.1795+0.171)/2-Leff_SD5/2);
D_BPM_9=((0.1655+0.1665)/2-Leff_SF4/2);



Kmean= -0.568120443069375;
phimean=0.103476208774844; %5.92875º
Leff_BEND=1.373885080906250;
Leff_BEND_0=1.383684;
 
global GLOBVAL

E_0 = .51099891e6;
GLOBVAL.E0 = 3e9-E_0;
GLOBVAL.LatticeFile = 'a25';


cd /home/zeus/ZeusApps/Datas/;
fid=fopen('a25-2010.opa','w');

fprintf(fid,'{----- C:\\OPA\\ALBA\\A-25-2010.OPA ----- Tu 24.1.2006 9:19 -------------------------}\n');

fprintf(fid,'Energy =   %f; NPeriod = 4;\n',GLOBVAL.E0/1e9);
fprintf(fid,'Meth   = 4; Nbend = 4; Nquad = 6;\n');

fprintf(fid,'    BetaX   =   11.1985095; AlphaX  =    0.0000000;\n');
fprintf(fid,'    EtaX    =    0.1461974; EtaXP   =   -0.0000000;\n');
fprintf(fid,'    BetaY   =    5.9249926; AlphaY  =   -0.0000000;\n');
fprintf(fid,'    EtaY    =    0.0000000; EtaYP   =    0.0000000;\n');
fprintf(fid,'    OrbitX  = 0.0000000000; OrbitXP = 0.0000000000;\n');
fprintf(fid,'    OrbitY  = 0.0000000000; OrbitYP = 0.0000000000;\n');
fprintf(fid,'    OrbitDPP= 0.0000000000;\n');

fprintf(fid,'{----- Table of elements (units: m, m^-2, deg, T; mm, mrad) ---------------- }\n');
fprintf(fid,'{      Conventions: Quadrupole: k>0 horizontally focusing                    }\n');
fprintf(fid,'{                   Sextupole : k=m*l, m:=Bpoletip/r^2/(B*rho)               }\n');

fprintf(fid,'L_BPM_1      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',D_BPM_1);
fprintf(fid,'L_BPM_2      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',D_BPM_2);
fprintf(fid,'L_BPM_3      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',D_BPM_3);
fprintf(fid,'L_BPM_4      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',D_BPM_4);
fprintf(fid,'L_BPM_5      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',D_BPM_5);
fprintf(fid,'L_BPM_6      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',D_BPM_6);
fprintf(fid,'L_BPM_7      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',D_BPM_7);
fprintf(fid,'L_BPM_8      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',D_BPM_8);
fprintf(fid,'L_BPM_9      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',D_BPM_9);


fprintf(fid,'L_IDS      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n', (3.985+(Leff_QD1_0-Leff_QD1)/2)/4);
fprintf(fid,'L_IDS_BPM      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n', (3.985+(Leff_QD1_0-Leff_QD1)/2)/4-D_BPM_1);
fprintf(fid,'D11      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.17+(Leff_QD1_0-Leff_QD1)/2+(Leff_QF1_0-Leff_QF1)/2);
fprintf(fid,'D12      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n', 0.150+(Leff_QF1_0-Leff_QF1)/2+(Leff_SF1_0-Leff_SF1)/2);
fprintf(fid,'D13      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n', 0.150+(Leff_QF2_0-Leff_QF2)/2+(Leff_SF1_0-Leff_SF1)/2);
fprintf(fid,'D14      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n', 0.375+(Leff_QF2_0-Leff_QF2)/2+(Leff_SD1_0-Leff_SD1)/2-D_BPM_2);
fprintf(fid,'D15      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.260+(Leff_SD1_0-Leff_SD1)/2+(Leff_BEND_0-Leff_BEND)/2);
fprintf(fid,'D21      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.26+(Leff_SD2_0-Leff_SD2)/2+(Leff_BEND_0-Leff_BEND)/2);
fprintf(fid,'D22       : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.15+(Leff_QF3_0-Leff_QF3)/2+(Leff_SD2_0-Leff_SD2)/2-D_BPM_3);
fprintf(fid,'D23       : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.375 +(Leff_QF4_0-Leff_QF4)/2+(Leff_QF3_0-Leff_QF3)/2);
fprintf(fid,'D24       : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.15 +(Leff_QF4_0-Leff_QF4)/2+(Leff_SF2_0-Leff_SF2)/2);
fprintf(fid,'D25      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.165 +(Leff_QF5_0-Leff_QF5)/2+(Leff_SF2_0-Leff_SF2)/2-D_BPM_4);
fprintf(fid,'D26      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.470 +(Leff_QF5_0-Leff_QF5)/2+(Leff_SD3_0-Leff_SD3)/2-D_BPM_5);
fprintf(fid,'D28      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.26+(Leff_SD3_0-Leff_SD3)/2+(Leff_BEND_0-Leff_BEND)/2);
fprintf(fid,'D31      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.260+(Leff_SD4_0-Leff_SD4)/2+(Leff_BEND_0-Leff_BEND)/2);
fprintf(fid,'D32      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.37+(Leff_QF6_0-Leff_QF6)/2+(Leff_SD4_0-Leff_SD4)/2-D_BPM_6);
fprintf(fid,'D33      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.175+(Leff_QF6_0-Leff_QF6)/2+(Leff_SF3_0-Leff_SF3)/2 );
fprintf(fid,'D34      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.15+(Leff_QD2_0-Leff_QD2)/2+(Leff_SF3_0-Leff_SF3)/2);

fprintf(fid,'D31_b      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.260+(Leff_SD4_0-Leff_SD4)/2+(Leff_BEND_0-Leff_BEND)/2);
fprintf(fid,'D32_b      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.37+(Leff_QF7_0-Leff_QF7)/2+(Leff_SD4_0-Leff_SD4)/2-D_BPM_6);
fprintf(fid,'D33_b      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.175+(Leff_QF7_0-Leff_QF7)/2+(Leff_SF3_0-Leff_SF3)/2 );
fprintf(fid,'D34_b      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.15+(Leff_QD3_0-Leff_QD3)/2+(Leff_SF3_0-Leff_SF3)/2);

fprintf(fid,'D41      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.26 +(Leff_SD5_0-Leff_SD5)/2+(Leff_BEND_0-Leff_BEND)/2);
fprintf(fid,'D42      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.54+(Leff_QF8_0-Leff_QF8)/2+(Leff_SD5_0-Leff_SD5)/2-D_BPM_8);
fprintf(fid,'D43      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',0.165+(Leff_QF8_0-Leff_QF8)/2+(Leff_SF4_0-Leff_SF4)/2-D_BPM_9);
fprintf(fid,'S_ID      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',1.30+(Leff_SF4_0-Leff_SF4)/2 );
fprintf(fid,'M_IDS      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n', (2.096767+(Leff_QD2_0-Leff_QD2)/2)/4);
fprintf(fid,'M_IDS_BPM      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n', (2.096767+(Leff_QD2_0-Leff_QD2)/2)/4-D_BPM_7);
fprintf(fid,'M_IDS_b      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n', (2.096767+(Leff_QD3_0-Leff_QD3)/2)/4);
fprintf(fid,'M_IDS_b_BPM      : Drift, L =%1.10f, Nslice =  2, Ax = 30.00, Ay = 30.00;\n',(2.096767+(Leff_QD3_0-Leff_QD3)/2)/4-D_BPM_7);
fprintf(fid,'DSD1     : Drift, L = %1.10f, Nslice =  1, Ax = 30.00, Ay = 30.00;\n',Leff_SD1/2);
fprintf(fid,'DSD2     : Drift, L = %1.10f, Nslice =  1, Ax = 30.00, Ay = 30.00;\n',Leff_SD2/2);
fprintf(fid,'DSD3     : Drift, L = %1.10f, Nslice =  1, Ax = 30.00, Ay = 30.00;\n',Leff_SD3/2);
fprintf(fid,'DSD4     : Drift, L = %1.10f, Nslice =  1, Ax = 30.00, Ay = 30.00;\n',Leff_SD4/2);
fprintf(fid,'DSD5     : Drift, L = %1.10f, Nslice =  1, Ax = 30.00, Ay = 30.00;\n',Leff_SD5/2);
fprintf(fid,'DSF1     : Drift, L = %1.10f, Nslice =  1, Ax = 30.00, Ay = 30.00;\n',Leff_SF1/2);
fprintf(fid,'DSF2     : Drift, L = %1.10f, Nslice =  1, Ax = 30.00, Ay = 30.00;\n',Leff_SF2/2);
fprintf(fid,'DSF3     : Drift, L = %1.10f, Nslice =  1, Ax = 30.00, Ay = 30.00;\n',Leff_SF3/2);
fprintf(fid,'DSF4     : Drift, L = %1.10f, Nslice =  1, Ax = 30.00, Ay = 30.00;\n',Leff_SF4/2);

kf1=2.0611638594;
kf2=1.4449533489;
kf3=2.1777478836;
kf4=0.8457235934;
kf5=1.8576871898;
kf6=2.0640030022;
kf7=2.0523861235;
kf8=2.0360116106;
kd1=-2.0143279541;
kd2=-1.9461962209;
kd3=-1.9456314837;


fprintf(fid,'QD1      : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n',  Leff_QD1, kd1);
fprintf(fid,'QD2      : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n',  Leff_QD2, kd2);
fprintf(fid,'QD3      : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n',  Leff_QD3, kd3);
fprintf(fid,'QF1      : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n',  Leff_QF1,  kf1);
fprintf(fid,'QF2      : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n',  Leff_QF2,  kf2);
fprintf(fid,'QF3       : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n',  Leff_QF3,  kf3);
fprintf(fid,'QF4       : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n',  Leff_QF4,  kf4);
fprintf(fid,'QF5       : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n',  Leff_QF5,  kf5);
fprintf(fid,'QF6       : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n',  Leff_QF6,  kf6);
fprintf(fid,'QF7       : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n', Leff_QF7,  kf7);
fprintf(fid,'QF8      : Quadrupole, L =%1.10f, K =  %1.10f,  Ax = 30.00, Ay = 30.00;\n',  Leff_QF8,  kf8);



sd1=1.748825163518;
sd2=7.699988942150;
sd3=4.354462199393;
sd4=6.381534889243;
sd5=3.884312620362;
sf1=0.784927560589;
sf2=7.316794669252;
sf3=6.781077557039;
sf4=3.725309547836;


fprintf(fid,'XD1: Sextupole, K = %1.10f, Ax = 50.00, Ay = 50.00;\n',-sd1);
fprintf(fid,'XD2: Sextupole, K = %1.10f, Ax = 50.00, Ay = 50.00;\n',-sd2);
fprintf(fid,'XD3: Sextupole, K = %1.10f, Ax = 50.00, Ay = 50.00;\n',-sd3);
fprintf(fid,'XD4: Sextupole, K = %1.10f, Ax = 50.00, Ay = 50.00;\n',-sd4);
fprintf(fid,'XD5 : Sextupole, K = %1.10f, Ax = 50.00, Ay = 50.00;\n',-sd5);
fprintf(fid,'XF1: Sextupole, K = %1.10f, Ax = 50.00, Ay = 50.00;\n',sf1);
fprintf(fid,'XF2: Sextupole, K = %1.10f, Ax = 50.00, Ay = 50.00;\n',sf2);
fprintf(fid,'XF3: Sextupole, K = %1.10f, Ax = 50.00, Ay = 50.00;\n',sf3);
fprintf(fid,'XF4: Sextupole, K = %1.10f, Ax = 50.00, Ay = 50.00;\n',sf4);
bangle = 2*pi/32;
gap=0.036;
fint=0.7;
sext_dip_0 =-0.221129858946660;
fprintf(fid,'XB: Sextupole, K = %1.10f, Ax = 50.00, Ay = 50.00;\n',sext_dip_0*Leff_BEND);
fprintf(fid,'B1D      : Bending, L = %1.10f, T =  %1.10f, K = %1.10f,T1 =%1.10f, T2 =%1.10f, Gap =%1.10f,k1in=%1.10f,k1in=%1.10f,K1 = 0.0000, K2 = 0.0000, Ax = 30.00, Ay = 30.00;\n',Leff_BEND/2,bangle/2*180/pi,Kmean,phimean*180/pi, 0,gap,fint,0);
fprintf(fid,'B1U      : Bending, L = %1.10f, T =  %1.10f, K = %1.10f,T1 =%1.10f, T2 =%1.10f, Gap =%1.10f,k1in=%1.10f,k1ex=%1.10f,K1 = 0.0000, K2 = 0.0000, Ax = 30.00, Ay = 30.00;\n',Leff_BEND/2,bangle/2*180/pi,Kmean,0,phimean*180/pi,gap,0,fint);

fprintf(fid,'{----- Table of segments ----------------------------------------------------}\n');

fprintf(fid,'SF1       :DSF1, XF1, DSF1;\n');
fprintf(fid,'SF2       :DSF2, XF2, DSF2;\n');
fprintf(fid,'SF3       :DSF3, XF3, DSF3;\n');
fprintf(fid,'SF4       :DSF4, XF4, DSF4;\n');
fprintf(fid,'SD1       :DSD1, XD1, DSD1;\n');
fprintf(fid,'SD2       :DSD2, XD2, DSD2;\n');
fprintf(fid,'SD3       :DSD3, XD3, DSD3;\n');
fprintf(fid,'SD4       :DSD4, XD4, DSD4;\n');
fprintf(fid,'SD5       :DSD5, XD5, DSD5;\n');
fprintf(fid,'L_ID:L_IDS,L_IDS,L_IDS,L_IDS_BPM;\n');
fprintf(fid,'M_ID:M_IDS,M_IDS,M_IDS,M_IDS;\n');
fprintf(fid,'M_ID_b:M_IDS_b,M_IDS_b,M_IDS_b,M_IDS_b_BPM;\n');
fprintf(fid,'BLOCK1    :L_ID,L_BPM_1,QD1,D11,QF1,D12,SF1,D13,QF2,D14,L_BPM_2,SD1,D15;\n');
fprintf(fid,'BE        :B1U, XB, B1D;\n');
fprintf(fid,'BLOCK2    :D21,SD2,D22,L_BPM_3,QF3,D23,QF4,D24,SF2,D25,L_BPM_4,QF5,D26,L_BPM_5,SD3,D28;\n');
fprintf(fid,'BLOCK31   :D31,SD4,L_BPM_6,D32,QF6,D33,SF3,D34,QD2,L_BPM_7,M_ID;\n');
fprintf(fid,'BLOCK32   :M_ID_b,L_BPM_7,QD3,D34_b,SF3,D33_b,QF7,D32_b,L_BPM_6,SD4,D31_b;\n');
fprintf(fid,'BLOCK4    :D41,SD5,L_BPM_8,D42,QF8,D43,L_BPM_9,SF4,S_ID;\n');
fprintf(fid,'CELDA     :BLOCK1, BE, BLOCK2, BE, BLOCK31, BLOCK32, BE, BLOCK4, -BLOCK4, \n');
fprintf(fid,'           BE, -BLOCK32, BLOCK32, BE, BLOCK4, -BLOCK4, BE, -BLOCK32, -BLOCK31, \n');
fprintf(fid,'           BE, -BLOCK2, BE, -BLOCK1;\n');
fprintf(fid,'MACHINE   :CELDA, CELDA, CELDA, CELDA;\n');

fprintf(fid,'{----- C:\\OPA\\ALBA\\A-25-2010.OPA ----- Tu 24.1.2006 9:19 -------------------------}\n');

fclose(fid);

end
