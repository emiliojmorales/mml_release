function ltb
%ALBA_LTB lattice definition file
% Created by G. Benedetti 18/08/08
% Modificated by Z.Marti 02/03/2010

global FAMLIST THERING GLOBVAL
E_0 = .51099891e6;
GLOBVAL.E0 = 0.11005e+9-E_0; % Modificated by Z.Marti 02/03/2010
GLOBVAL.LatticeFile = mfilename;
FAMLIST = cell(0);

disp(['**   Loading ALBA LTB lattice ', mfilename]);

AP = aperture('AP', [-0.017, 0.017, -0.017, 0.017],'AperturePass');

% Cavity
L0 = 13.6723;	% design length  [m]
C0 = 299792458; 	% speed of light [m/s]

%===============================
% Calibrated parameters  @ 02/03/2010, by  Z.Marti
%===============================

b1_phi1=6.212135996;
b1_phi2=6.03828097;
b1_leff=0.314389376;
b1_l0=0.3012;
b1_edge_mult_1=[0 0 -1.320743228 -63.09785603];
b1_edge_mult_2=[0 0 -1.299040184 -60.9021407];
b1_mult=[0 0.10544360 10.24222874 524.6884223];


b2_phi1=1.938005603;
b2_phi2=1.930498312;
b2_leff=0.305591613;
b2_l0=0.3001;
b2_edge_mult_1=[0 0 -1.132643928 -53.54871174];
b2_edge_mult_2=[0 0 -1.139494265 -58.36299078];
b2_mult=[0 0.01820843 7.890120193 250.3985465];

%===============================
DLI1   =    drift('DLI1' , 0.6955, 'DriftPass'); 
DLI2   =    drift('DLI2' , 0.2365, 'DriftPass'); 
DLI3   =    drift('DLI3' , 0.248, 'DriftPass'); %drift from last linac BPM and the linac valve interface (end ofthe linac)
D101  =    drift('D101', 0.300, 'DriftPass');
D102  =    drift('D102', 0.150, 'DriftPass');
D103  =    drift('D103', 0.327, 'DriftPass');
D104  =    drift('D104', 0.174, 'DriftPass');
D105  =    drift('D105', 0.174, 'DriftPass');
D106  =    drift('D106', 0.442, 'DriftPass');
D107  =    drift('D106', 0.130, 'DriftPass');
D108  =    drift('D106', 0.215-(b1_leff-b1_l0)/2, 'DriftPass');

D201  =    drift('D201', 0.600-(b1_leff-b1_l0)/2, 'DriftPass');
D202  =    drift('D202', 0.150, 'DriftPass');
D203h  =    drift('D203', 1.137/2, 'DriftPass');
D204  =    drift('D204', 0.174, 'DriftPass');
D205  =    drift('D205', 0.174, 'DriftPass');
D206h  =    drift('D206', 1.345/2, 'DriftPass');
D207h  =    drift('D207', 1.000/2, 'DriftPass');
D208  =    drift('D208', 0.255, 'DriftPass');
D209  =    drift('D209', 0.130, 'DriftPass');
D210  =    drift('D210', 0.207-(b2_leff-b2_l0)/2, 'DriftPass');

D301  =    drift('D301', 0.300-(b2_leff-b2_l0)/2, 'DriftPass');
D302  =    drift('D302', 0.150, 'DriftPass');
D303h  =    drift('D303', 1.937/2, 'DriftPass');
D304  =    drift('D304', 0.174, 'DriftPass');
D305  =    drift('D305', 0.174, 'DriftPass');
D306  =    drift('D306', 0.162, 'DriftPass');
D307  =    drift('D307', 0.150, 'DriftPass');
D308h  =    drift('D308', 1.341/2, 'DriftPass');
D309  =    drift('D309', 0.130, 'DriftPass');
D310  =    drift('D310', 0.335, 'DriftPass');


% QT0101=quadrupole('Q', 0.126,-1.6525750527,'StrMPoleSymplectic4Pass');
% QT0102=quadrupole('Q', 0.126,-0.8662383167,'StrMPoleSymplectic4Pass');
% QT0103=quadrupole('Q', 0.126,-5.6836434333,'StrMPoleSymplectic4Pass');
% QT0104=quadrupole('Q', 0.126,7.1174012165,'StrMPoleSymplectic4Pass');
% QT0105=quadrupole('Q', 0.126,-15.4443334494,'StrMPoleSymplectic4Pass');
% QT0106=quadrupole('Q', 0.126,9.0643411992,'StrMPoleSymplectic4Pass');
% QT0107=quadrupole('Q', 0.126,4.4923320783,'StrMPoleSymplectic4Pass');
% QT0108=quadrupole('Q', 0.126,-12.8698252022,'StrMPoleSymplectic4Pass');
% QT0109=quadrupole('Q', 0.126,10.1791958031,'StrMPoleSymplectic4Pass');


QT0101=quadrupole('Q', 0.126,-5.8250000000,'StrMPoleSymplectic4Pass');
QT0102=quadrupole('Q', 0.126,1.0323000000,'StrMPoleSymplectic4Pass');
QT0103=quadrupole('Q', 0.126,-4.7238000000,'StrMPoleSymplectic4Pass');
QT0104=quadrupole('Q', 0.126,7.8944000000,'StrMPoleSymplectic4Pass');
QT0105=quadrupole('Q', 0.126,-14.7187000000,'StrMPoleSymplectic4Pass');
QT0106=quadrupole('Q', 0.126,7.7392000000,'StrMPoleSymplectic4Pass');
QT0107=quadrupole('Q', 0.126,4.0326000000,'StrMPoleSymplectic4Pass');
QT0108=quadrupole('Q', 0.126,-10.9219000000,'StrMPoleSymplectic4Pass');
QT0109=quadrupole('Q', 0.126,9.3645000000,'StrMPoleSymplectic4Pass');


%settings for second triplet scan
QT0101=quadrupole('Q', 0.126,-0.2836637226,'StrMPoleSymplectic4Pass');
QT0102=quadrupole('Q', 0.126,1.1295009312,'StrMPoleSymplectic4Pass');
QT0103=quadrupole('Q', 0.126,-2.5486325648,'StrMPoleSymplectic4Pass');
QT0104=quadrupole('Q', 0.126,0,'StrMPoleSymplectic4Pass');
QT0105=quadrupole('Q', 0.126,0,'StrMPoleSymplectic4Pass');
QT0106=quadrupole('Q', 0.126,0,'StrMPoleSymplectic4Pass');
QT0107=quadrupole('Q', 0.126,0,'StrMPoleSymplectic4Pass');
QT0108=quadrupole('Q', 0.126,0,'StrMPoleSymplectic4Pass');
QT0109=quadrupole('Q', 0.126,0,'StrMPoleSymplectic4Pass');
%settings for trird triplet scan
QT0101=quadrupole('Q', 0.126,-9.2805609080,'StrMPoleSymplectic4Pass');
QT0102=quadrupole('Q', 0.126,-10.3766068827,'StrMPoleSymplectic4Pass');
QT0103=quadrupole('Q', 0.126,15.2796924309,'StrMPoleSymplectic4Pass');
QT0104=quadrupole('Q', 0.126,-11.6815071951,'StrMPoleSymplectic4Pass');
QT0105=quadrupole('Q', 0.126,12.0737637529,'StrMPoleSymplectic4Pass');
QT0106=quadrupole('Q', 0.126,-0.1161785181,'StrMPoleSymplectic4Pass');
QT0107=quadrupole('Q', 0.126,0,'StrMPoleSymplectic4Pass');
QT0108=quadrupole('Q', 0.126,0,'StrMPoleSymplectic4Pass');
QT0109=quadrupole('Q', 0.126,0,'StrMPoleSymplectic4Pass');


% DankFisik calibration
QT0101=quadrupole('Q', 0.126,-4.9662228034,'StrMPoleSymplectic4Pass');
QT0102=quadrupole('Q', 0.126,6.7508405118,'StrMPoleSymplectic4Pass');
QT0103=quadrupole('Q', 0.126,-6.9424494214,'StrMPoleSymplectic4Pass');
QT0104=quadrupole('Q', 0.126,8.0915070786,'StrMPoleSymplectic4Pass');
QT0105=quadrupole('Q', 0.126,-15.2931364815,'StrMPoleSymplectic4Pass');
QT0106=quadrupole('Q', 0.126,7.7340200627,'StrMPoleSymplectic4Pass');
QT0107=quadrupole('Q', 0.126,5.3522866591,'StrMPoleSymplectic4Pass');
QT0108=quadrupole('Q', 0.126,-11.5954269393,'StrMPoleSymplectic4Pass');
QT0109=quadrupole('Q', 0.126,8.7403013826,'StrMPoleSymplectic4Pass');

% beam based calibration
QT0101=quadrupole('Q', 0.126,-6.9387845937,'StrMPoleSymplectic4Pass');
QT0102=quadrupole('Q', 0.126,9.9312806526,'StrMPoleSymplectic4Pass');
QT0103=quadrupole('Q', 0.126,-9.0324969043,'StrMPoleSymplectic4Pass');
QT0104=quadrupole('Q', 0.126,7.4708636456,'StrMPoleSymplectic4Pass');
QT0105=quadrupole('Q', 0.126,-14.9649694528,'StrMPoleSymplectic4Pass');
QT0106=quadrupole('Q', 0.126,8.1088787313,'StrMPoleSymplectic4Pass');
QT0107=quadrupole('Q', 0.126,4.7242263442,'StrMPoleSymplectic4Pass');
QT0108=quadrupole('Q', 0.126,-11.1424436213,'StrMPoleSymplectic4Pass');
QT0109=quadrupole('Q', 0.126,8.9364590557,'StrMPoleSymplectic4Pass');

% Measure 11 March 2013 
QT0101=quadrupole('Q', 0.126,-3.7413669809,'StrMPoleSymplectic4Pass');
QT0102=quadrupole('Q', 0.126,0.1222337111,'StrMPoleSymplectic4Pass');
QT0103=quadrupole('Q', 0.126,-1.9284716314,'StrMPoleSymplectic4Pass');
QT0104=quadrupole('Q', 0.126,7.6575863217,'StrMPoleSymplectic4Pass');
QT0105=quadrupole('Q', 0.126,-14.8741412453,'StrMPoleSymplectic4Pass');
QT0106=quadrupole('Q', 0.126,7.8398784974,'StrMPoleSymplectic4Pass');
QT0107=quadrupole('Q', 0.126,5.4038660558,'StrMPoleSymplectic4Pass');
QT0108=quadrupole('Q', 0.126,-11.8896655044,'StrMPoleSymplectic4Pass');
QT0109=quadrupole('Q', 0.126,8.8805882706,'StrMPoleSymplectic4Pass');

% Measures 11 Març 2013 en_spread measurement @ 2nd screen ONLY
QT0101=quadrupole('Q', 0.126,-5.7223264636,'StrMPoleSymplectic4Pass');
QT0102=quadrupole('Q', 0.126,-2.7904876749,'StrMPoleSymplectic4Pass');
QT0103=quadrupole('Q', 0.126,5.1090255246,'StrMPoleSymplectic4Pass');
QT0104=quadrupole('Q', 0.126,-2.8081431096,'StrMPoleSymplectic4Pass');
QT0105=quadrupole('Q', 0.126,-9.5956253446,'StrMPoleSymplectic4Pass');
QT0106=quadrupole('Q', 0.126,11.6149000327,'StrMPoleSymplectic4Pass');
QT0107=quadrupole('Q', 0.126,7.0068224582,'StrMPoleSymplectic4Pass');
QT0108=quadrupole('Q', 0.126,0.1141375085,'StrMPoleSymplectic4Pass');
QT0109=quadrupole('Q', 0.126,-5.7814904631,'StrMPoleSymplectic4Pass');

% QT0101=[QT0101 QT0101];
% QT0102=[QT0102 QT0102];
% QT0103=[QT0103 QT0103];
% QT0104=[QT0104 QT0104];
% QT0105=[QT0105 QT0105];
% QT0106=[QT0106 QT0106];
% QT0107=[QT0107 QT0107];
% QT0108=[QT0108 QT0108];
% QT0109=[QT0109 QT0109];

% Low disp on the third screen
% QT0101=quadrupole('Q', 0.126,10.7752035985,'StrMPoleSymplectic4Pass');
% QT0102=quadrupole('Q', 0.126,-14.5818977061,'StrMPoleSymplectic4Pass');
% QT0103=quadrupole('Q', 0.126,3.7786485459,'StrMPoleSymplectic4Pass');
% QT0104=quadrupole('Q', 0.126,19.5777257764,'StrMPoleSymplectic4Pass');
% QT0105=quadrupole('Q', 0.126,-19.9999999999,'StrMPoleSymplectic4Pass');
% QT0106=quadrupole('Q', 0.126,-19.9999999999,'StrMPoleSymplectic4Pass');





EdgeB1_l=multipole('EdgeB1_l',0,[0 0 0 0],b1_edge_mult_1,'ThinMPolePass');
EdgeB1_r=multipole('EdgeB1_r',0,[0 0 0 0],b1_edge_mult_2,'ThinMPolePass');
EdgeB2_l=multipole('EdgeB2_l',0,[0 0 0 0],b2_edge_mult_1,'ThinMPolePass');
EdgeB2_r=multipole('EdgeB2_r',0,[0 0 0 0],b2_edge_mult_2,'ThinMPolePass');
BM01a = sbendC2('BEND', b1_leff/2,8.75*pi/180/2, b1_phi1*pi/180, 0,[],b1_mult, 'BndMPoleSymplectic4Pass');
BM02a = sbendC2('BM02', b2_leff/2,4.50*pi/180/2, b2_phi1*pi/180, 0,[],b2_mult, 'BndMPoleSymplectic4Pass');
BM01b = sbendC2('BEND', b1_leff/2,8.75*pi/180/2, 0, b1_phi2*pi/180,[],b1_mult, 'BndMPoleSymplectic4Pass');
BM02b = sbendC2('BM02', b2_leff/2,4.50*pi/180/2, 0, b2_phi2*pi/180,[],b2_mult, 'BndMPoleSymplectic4Pass');
BM01=[BM01a BM01b];
BM02=[BM02a BM02b];

SEINJ = sbend('SEINJ', 0.6342, 12.60*pi/180, 14.50*pi/180,-1.90*pi/180, 0.0, 'BndMPoleSymplectic4Pass');

% Booster elements
LeffQH01=0.360;
Leff_Kick=0.4;
%KICKER_BO   = corrector('KIINJ' ,Leff_Kick, [-1.90*pi/180 0],'CorrectorPass');
KICKER_BO = sbend('KIINJ', Leff_Kick, 1.90*pi/180, 1.90*pi/180, 0.0, 0.0, 'BndMPoleSymplectic4Pass');

DBO01  =    drift('DBO01', 0.38339+(0.5-Leff_Kick)/2, 'DriftPass');
DBO02  =    drift('DBO02', 0.4835+(0.5-Leff_Kick)/2-LeffQH01/2, 'DriftPass');
DBO03  =    drift('DBO03', 0.3700-LeffQH01/2, 'DriftPass');
QH01 = quadrupole('QH01',  LeffQH01,  1.4329, 'StrMPoleSymplectic4Pass');
% 
% ElemData=FAMLIST{SEINJ}.ElemData;
% ElemData.T2=[-0.021-4e-6 1.90*pi/180 0 0 0 0];
% FAMLIST{SEINJ}.ElemData=ElemData;


CORH = corrector('HCM', 0.0, [0 0],'CorrectorPass');
CORV = corrector('VCM', 0.0, [0 0],'CorrectorPass');
BPM = marker('BPM','IdentityPass');
FSOTR = marker('FSOTR','IdentityPass');
IP = marker('IP','IdentityPass');
IP2 = marker('IP2','IdentityPass');
MatchP = marker('MatchP','IdentityPass');

% Begin Lattice
%----- Linac to Booster Transfer Line: LTB --------------------------!;
LTB = [DLI1,FSOTR,DLI2, BPM, DLI3, D101, CORH, D102, CORV, D103, QT0101, D104, ...
       QT0102, D105, QT0103, D106, BPM, D107, FSOTR, D108, EdgeB1_l, BM01, EdgeB1_r, ...
       D201, CORH, D202, CORV, D203h,D203h, QT0104, D204, QT0105, D205, ...
       QT0106, D206h,D206h, D207h,D207h, D208, BPM, D209, FSOTR, D210, EdgeB2_l, BM02, EdgeB2_r, ...
       D301, CORH, D302, CORV, D303h, D303h, QT0107, D304, QT0108, D305, ...
       QT0109, D306, CORH, D307, CORV, D308h,D308h, BPM, D309, FSOTR, D310, SEINJ, IP,DBO01,KICKER_BO,IP2,DBO02,...
       MatchP QH01  DBO03 BPM ];

% INIT_LTB : Beta0,
%                 betx = 5.00, alfx = -1.00,
%                 bety = 5.00, alfy = -1.00,
%                 dx   = 0.00, dpx  = 0.00;

buildlat(LTB);

% set all magnets to the same energy
THERING = setcellstruct(THERING,'Energy',1:length(THERING),GLOBVAL.E0);

%evalin('caller','global THERING FAMLIST GLOBVAL');

% atsummary;

if nargout
    varargout{1} = THERING;
end

% Compute total length and RF frequency
L0_tot=0;
for i=1:length(THERING)
    L0_tot=L0_tot+THERING{i}.Length;
end
fprintf('   Model LTB length is %.6f meters\n', L0_tot)
%fprintf('   Model RF frequency is %.6f MHz\n', HarmNumber*C0/L0_tot/1e6)

clear global FAMLIST
%clear global GLOBVAL when GWig... has been changed.

%evalin('caller','global THERING FAMLIST GLOBVAL');

end


function z=sbendC2(fname,L,A,A1,A2,polynomA,polynomB,method)
%BEND('FAMILYNAME',  Length[m], BendingAngle[rad], EntranceAngle[rad],ExitAngle[rad], polynomA,polynomB, 'METHOD')
%	creates a new family in the FAMLIST - a structure with fields
%		FamName        	family name
%		Length         	length of the arc for an on-energy particle [m]
%		BendingAngle		total bending angle [rad]
%		EntranceAngle		[rad] (0 - for sector bends)
%		ExitAngle			[rad] (0 - for sector bends)
%		ByError				error in the dipole field relative to the design value 
%		polynomA			skew multipole body values
%		polynomB			normal multipole body values
%		PassMethod        name of the function to use for tracking
% returns assigned address in the FAMLIST that is uniquely identifies
% the family

MaxOrder=max([length(polynomA) length(polynomB)]-1);
polynomA_copy=zeros(1,MaxOrder+1);
polynomB_copy=zeros(1,MaxOrder+1);
polynomA_copy(1:length(polynomA))=polynomA;
polynomB_copy(1:length(polynomB))=polynomB;

ElemData.FamName = fname;  % add check for identical family names
ElemData.Length			= L;
ElemData.MaxOrder			= MaxOrder;
ElemData.NumIntSteps 	= 10;
ElemData.BendingAngle  	= A;
ElemData.EntranceAngle 	= A1;
ElemData.ExitAngle     	= A2;
ElemData.ByError     	= 0;
ElemData.K      			= polynomB_copy(2);

ElemData.R1 = diag(ones(6,1));
ElemData.R2 = diag(ones(6,1));
ElemData.T1 = zeros(1,6);
ElemData.T2 = zeros(1,6);

ElemData.PolynomA			= polynomA_copy;	 
ElemData.PolynomB			= polynomB_copy; 

ElemData.PassMethod 		= method;

global FAMLIST
z = length(FAMLIST)+1; % number of declare families including this one
FAMLIST{z}.FamName = fname;
FAMLIST{z}.NumKids = 0;
FAMLIST{z}.KidsList= [];
FAMLIST{z}.ElemData= ElemData;

end
