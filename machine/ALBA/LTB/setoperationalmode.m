function setoperationalmode(ModeNumber)
%SETOPERATIONALMODE - Switches between the various operational modes
%  setoperationalmode(ModeNumber)
%
%  INPUTS
%  1. ModeNumber = 1. 0.1 GeV, Injection
%                  2. ...
%
%  See also aoinit, updateatindex, ltbinit

%
% Written by Gabriele Benedetti 18/8/08

global THERING AD;

% Check if the AO exists
checkforao;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Accelerator Dependent Modes %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 1
    ModeNumber = [];
end
if isempty(ModeNumber)
    ModeCell = {'0.1 GeV, Booster Injection'};
    [ModeNumber, OKFlag] = listdlg('Name','ALBA','PromptString','Select the Operational Mode:', 'SelectionMode','single', 'ListString', ModeCell, 'ListSize', [450 200]);
    if OKFlag ~= 1
        fprintf('   Operational mode not changed\n');
        return
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Accelerator Data Structure %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
AD = getad;
AD.Machine = 'ALBA';            % Will already be defined if setpathmml was used
AD.MachineType = 'Transport';   % Will already be defined if setpathmml was used
AD.SubMachine  = 'LTB';   % Will already be defined if setpathmml was used
AD.OperationalMode = '';          % Gets filled in later


%%AD.HarmonicNumber = 416;

% Tune processor delay: delay required to wait
% to have a fresh tune measurement after changing
% a variable like the RF frequency.  Setpv will wait
% 2.2 * TuneDelay to be guaranteed a fresh data point.
AD.BPMDelay  = 0.25; % use [N, BPMDelay]=getbpmsaverages (AD.BPMDelay will disappear)
%%AD.TuneDelay = 0.1;

% The offset and golden orbits are stored at the end of this file


% SP-AM Error level
% AD.ErrorWarningLevel = 0 -> SP-AM errors are Matlab errors {Default}
%                       -1 -> SP-AM errors are Matlab warnings
%                       -2 -> SP-AM errors prompt a dialog box
%                       -3 -> SP-AM errors are ignored (ErrorFlag=-1 is returned)
AD.ErrorWarningLevel = 0;

%%%%%%%%%%%%%%%%%%%%%
% Operational Modes %
%%%%%%%%%%%%%%%%%%%%%

% Mode setup variables (mostly path and file names)
% AD.OperationalMode - String used in titles
% ModeName - String used for mode directory name off DataRoot/MachineName
% OpsFileExtension - string add to default file names

if ModeNumber == 1
  
    % User mode - High Tune
    AD.OperationalMode = '0.1 GeV, Booster Injection';
    
    ModeName = 'Injection';
    OpsFileExtension = '_inj';
    
    
    % Comisioning 4 SR
    energy=0.11005;
    d_energy=0.25/100;
    enx=(8.3)*1e-6;
    eny=(8.7)*1e-6;
    bx=16.45;
    by=12.41;
    ax=3.32;
    ay=2.66;
    % Measure 11 març 2013
    energy=0.1099;
    d_energy=0.07/100;
    enx=(10.3)*1e-6;
    eny=(8.9)*1e-6;
    bx=16.99;
    by=14.58;
    ax=3.11;
    ay=2.77;
    % Measure 6 abril 2013
    energy=0.10953;
    d_energy=0.07/100;
    enx=(10.6)*1e-6;
    eny=(11.9)*1e-6;
    bx=20.25;
    by=14.7;
    ax=3.59;
    ay=4.1;
    
    AD.Energy = energy; % Make sure this is the same as bend2gev at the production lattice!
    AD.TwissData.ClosedOrbit=[0 0 0 0]';
    AD.TwissData.dP=0;
    AD.TwissData.dL=0;
    AD.TwissData.ElemIndex=1;
    AD.TwissData.SPos=0;
    AD.TwissData.M44=eye(4);
    AD.TwissData.mu= 2*pi*[0 0];
    AD.TwissData.Dispersion=[0 0 0 0]';
    AD.TwissData.beta=[bx by];
    AD.TwissData.alpha=[ax ay];
    AD.TwissData.Emit_N_x=enx;
    AD.TwissData.Emit_N_y=eny;
    AD.TwissData.Ener_sread=d_energy;
    AD.TwissData.Energy=energy;


    % AT lattice
    AD.ATModel = 'ltb';
    eval(AD.ATModel);  %run model for compiler;

    %switch2online;
    switch2sim;    % Until the machine is ready ???
    switch2hw;
    %switch2physics;
    
% elseif ModeNumber == 2
%     % Model mode
%     AD.OperationalMode = '0.1 GeV, 11.42 / 7.38, Injection';
%     ModeName = 'Injection';
%     OpsFileExtension = '_Inj';
% 
%     % AT lattice
%     AD.ATModel = 'booster_inj';
%     eval(AD.ATModel);  %run model for compiler;
% 
%     % Golden TUNE is with the TUNE family (This could have been in aspphysdata)  % ???
%     AO = getao;
%     AO.TUNE.Monitor.Golden = [
%         0.4200
%         0.3800
%         NaN];
%     setao(AO);
% 
% 
%     % Golden chromaticity is in the AD (Physics units)
%     AD.Chromaticity.Golden = [1; 1];   % ???
% 
%     % Defaults RF for dispersion and chromaticity measurements (must be in Hardware units)
%     AD.DeltaRFDisp = 100e-6;
%     AD.DeltaRFChro = [-100 -50 0 50 150] * 1e-6;
% 
%     %switch2online;
%     switch2sim;    % Until the machine is ready ???
%     switch2hw;
%     %switch2physics;
else
    error('Operational mode unknown');
end

%Set the AD directory path
setad(AD);
MMLROOT = setmmldirectories(AD.Machine, AD.SubMachine, ModeName, OpsFileExtension);
AD = getad;

%MMLROOT = getmmlroot;

% Top Level Directories
AD.Directory.DataRoot       = fullfile(MMLROOT, 'measdata', AD.Machine, 'LTB', filesep);
AD.Directory.OpsData        = fullfile(MMLROOT, 'machine', AD.Machine, 'LTBOpsData', filesep);
AD.Directory.Lattice        = fullfile(MMLROOT, 'machine', AD.Machine, 'LTB', 'Lattices', filesep);
AD.Directory.Orbit          = fullfile(MMLROOT, 'machine', AD.Machine, 'LTB',  'orbit', filesep);

% Data Archive Directories
AD.Directory.BPMData        = fullfile(AD.Directory.DataRoot, 'BPM', filesep);
AD.Directory.DispData       = fullfile(AD.Directory.DataRoot, 'Dispersion', filesep);
AD.Directory.ConfigData     = fullfile(AD.Directory.DataRoot, 'MachineConfig', filesep);
AD.Directory.BumpData       = fullfile(AD.Directory.DataRoot, 'Bumps', filesep);
AD.Directory.Archiving      = fullfile(AD.Directory.DataRoot, 'ArchivingData', filesep);
AD.Directory.QUAD           = fullfile(AD.Directory.DataRoot, 'QUAD', filesep);

% AD.Directory.InterlockData  = fullfile(AD.Directory.DataRoot, 'Interlock/'];

%Response Matrix Directories
AD.Directory.BPMResponse    = fullfile(AD.Directory.DataRoot, 'Response', 'BPM', filesep);
AD.Directory.DispResponse   = fullfile(AD.Directory.DataRoot, 'Response', 'Disp', filesep);

%Default Data File Prefix
AD.Default.BPMArchiveFile      = 'BPM';                %file in AD.Directory.BPM               orbit data
AD.Default.DispArchiveFile     = 'Disp';               %file in AD.Directory.Dispersion       dispersion data
AD.Default.CNFArchiveFile      = 'CNF';                %file in AD.Directory.CNF               configuration data
AD.Default.QUADArchiveFile     = 'QuadBeta';           %file in AD.Directory.QUAD             betafunction for quadrupoles   

%Default Response Matrix File Prefix
AD.Default.BPMRespFile      = 'BPMRespMat';         %file in AD.Directory.BPMResponse       BPM response matrices
AD.Default.DispRespFile     = 'DispRespMat';        %file in AD.Directory.DispResponse      dispersion response matrices

%Operational Files
AD.OpsData.LatticeFile       = 'GoldenLattice';     %Golden Lattice File (setup for users)
AD.OpsData.PhysDataFile      = 'GoldenPhysData';
AD.OpsData.BPMGoldenFile     = 'GoldenBPMOrbit';
AD.OpsData.BPMOffsetFile     = 'OffsetBPMOrbit';
AD.OpsData.BPMSigmaFile      = 'BPMSigma';
AD.OpsData.DispFile          = 'GoldenDisp';

%Operational Response Files
AD.OpsData.BPMRespFile       = 'GoldenBPMResp';     
AD.OpsData.DispRespFile      = 'GoldenDispResp';
AD.OpsData.RespFiles         = {AD.OpsData.BPMRespFile,  ...
                                AD.OpsData.TuneRespFile, ...
                                AD.OpsData.ChroRespFile, ...
                                AD.OpsData.DispRespFile};

%Orbit Control and Feedback Files
AD.Restore.GlobalFeedback   = 'Restore.m';

% Circumference
AD.Circumference = findspos(THERING,length(THERING)+1);
setad(AD);

% Updates the AT indices in the MiddleLayer with the present AT lattice
updateatindex;

% Set the model energy
setenergymodel(AD.Energy);

% Momentum compaction factor
% MCF = getmcf('Model');
% if isnan(MCF)
%     AD.MCF = 0.00358;  
%     fprintf('   Model alpha calculation failed, middlelayer alpha set to  %f\n', AD.MCF);
% else
%     AD.MCF = MCF;
%     fprintf('   Middlelayer alpha set to %f (AT model).\n', AD.MCF);
% end
setad(AD);

% Add Gain & Offsets for magnet family
fprintf('   Setting magnet monitor gains based on the production lattice.\n');


% %%%%%%%%%%%%%%%%%%%%%%
% % Final mode changes %
% %%%%%%%%%%%%%%%%%%%%%%
% if any(ModeNumber == [1 2])
%     % User mode - Nominal lattice
% 
%     % Tune actuactors
%     MemberOf = getfamilydata('QH01','MemberOf');
%     setfamilydata({MemberOf{:} 'Tune Corrector'}','QH01','MemberOf');
%     MemberOf = getfamilydata('QV01','MemberOf');
%     setfamilydata({MemberOf{:} 'Tune Corrector'}','QV01','MemberOf');
%     MemberOf = getfamilydata('QV02','MemberOf');
%     setfamilydata({MemberOf{:} 'Tune Corrector'}','QV02','MemberOf');
%     
%     % Chromaticity actuators
%     MemberOf = getfamilydata('SV','MemberOf');
%     setfamilydata({MemberOf{:} 'Chromaticity Corrector'}','SV','MemberOf');
%     MemberOf = getfamilydata('SH','MemberOf');
%     setfamilydata({MemberOf{:} 'Chromaticity Corrector'}','SH','MemberOf');
% end

% fprintf('   lattice files have changed or if the AT lattice has changed.\n');
fprintf('   Middlelayer setup for operational mode: %s\n', AD.OperationalMode);

setad(orderfields(AD));

end


