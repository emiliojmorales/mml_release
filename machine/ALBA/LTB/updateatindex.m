function updateatindex
%UPDATEATINDEX - Updates the AT indices in the MiddleLayer with the present AT lattice (THERING)

global THERING

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Append Accelerator Toolbox information %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Since changes in the AT model could change the AT indexes, etc,
% It's best to regenerate all the model indices whenever a model is loaded

% Sort by family first (findcells is linear and slow)
% Written for ALBA by G. Benedetti 18/8/08
% Modified by Z. Marti 23/3/2010

Indices = atindex(THERING);

AO = getao;

try
    AO.BPMx.AT.ATType = 'X';
    AO.BPMx.AT.ATIndex = Indices.BPM(:); % findcells(THERING,'FamName','BPM')';
    AO.BPMx.Position = findspos(THERING, AO.BPMx.AT.ATIndex)';

    AO.BPMy.AT.ATType = 'Y';
    AO.BPMy.AT.ATIndex = Indices.BPM(:); % findcells(THERING,'FamName','BPM')';
    AO.BPMy.Position = findspos(THERING, AO.BPMy.AT.ATIndex)';
catch
    fprintf('   BPM family not found in the model.\n');
end

try
    % Horizontal correctors
    AO.HCM.AT.ATType  = 'HCM';
    AO.HCM.AT.ATIndex = buildatindex(AO.HCM.FamilyName, Indices.HCM);%Indices.HCM;  % Modified by Z. Marti 23/3/2010
    AO.HCM.Position = findspos(THERING, AO.HCM.AT.ATIndex)';
    
    % Vertical correctors
    AO.VCM.AT.ATType = 'VCM';
    AO.VCM.AT.ATIndex = buildatindex(AO.VCM.FamilyName, Indices.VCM);%Indices.VCM;  % Modified by Z. Marti 23/3/2010
    AO.VCM.Position = findspos(THERING, AO.VCM.AT.ATIndex)';
catch
    fprintf('   COR family not found in the model.\n');
end

try
    AO.Q.AT.ATType = 'QUAD';
%     ATIndex = [Indices.QH01(:)];
%     AO.QH01.AT.ATIndex  = sort(ATIndex);
    AO.Q.AT.ATIndex = buildatindex(AO.Q.FamilyName, Indices.Q);
    AO.Q.Position = findspos(THERING, AO.Q.AT.ATIndex)';
catch
    fprintf('   Q family not found in the model.\n');
end

try
    AO.BEND.AT.ATType = 'BEND';
    AO.BEND.AT.ATIndex = buildatindex(AO.BEND.FamilyName, Indices.BEND);
    AO.BEND.Position = findspos(THERING, AO.BEND.AT.ATIndex)';
catch
    fprintf('   BM01 family not found in the model.\n');
end

try
    AO.BM02.AT.ATType = 'SEPTUM';
    AO.BM02.AT.ATIndex = buildatindex(AO.BM02.FamilyName, Indices.BM02);
    AO.BM02.Position = findspos(THERING, AO.BM02.AT.ATIndex)';
catch
    fprintf('   BM02 family not found in the model.\n');
end

try
    AO.SEINJ.AT.ATType = 'SEPTUM';
    AO.SEINJ.AT.ATIndex = buildatindex(AO.SEINJ.FamilyName, Indices.SEINJ);
    AO.SEINJ.Position = findspos(THERING, AO.SEINJ.AT.ATIndex)';
catch
    fprintf('   SEINJ element not found in the model.\n');
end

try
    AO.KIINJ.AT.ATType = 'SEPTUM';
    AO.KIINJ.AT.ATIndex = buildatindex(AO.KIINJ.FamilyName, Indices.KIINJ);
    AO.KIINJ.Position = findspos(THERING, AO.KIINJ.AT.ATIndex)';
catch
    fprintf('   KIINJ element not found in the model.\n');
end

setao(AO);
