function ltbinit(OperationalMode)
% Initialize parameters for ALBA LTB control in MATLAB

% Created by G.Benedetti - 18 August 2008
% Modified by Z.Marti - 23/3/2010

if nargin < 1
    OperationalMode = 1;
end

%==============================
% load AcceleratorData structure
%==============================

Mode             = 'Simulator';
setad([]);       %clear AcceleratorData memory

%%%%%%%%%%%%%%%%%%%%
% ACCELERATOR OBJECT
%%%%%%%%%%%%%%%%%%%%

setao([]);   %clear previous AcceleratorObjects

%=============================================
%% BPMx data: status field designates if BPM in use
%=============================================
iFam = 'BPMx';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'BPM';
AO.(iFam).MemberOf                 = {'PlotFamily'; 'HBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';

% bpm={
%      1,	'LI/DI/BPM-01', 1, [1  1], 'BPMX-00'
%      2,	'LT/DI/BPM-01', 1, [1  2], 'BPMX-01'
%      3,	'LT/DI/BPM-02', 1, [1  3], 'BPMX-02'
%      4,	'LT/DI/BPM-03', 1, [1  4], 'BPMX-03'
%      5,	'BO01/DI/BPM-01', 1, [1  5], 'BPMX-04'
%     };
%==============================
% Modified by Z.Marti - 23/3/2010
bpm={
     1,	'LI01/DI/BPM-ACQ-01', 1, [1  1], 'BPMX-00'
     2,	'LT01/DI/BPM-ACQ-01', 1, [1  2], 'BPMX-01'
     3,	'LT01/DI/BPM-ACQ-02', 1, [1  3], 'BPMX-02'
     4,	'LT01/DI/BPM-ACQ-03', 1, [1  4], 'BPMX-03'
     5,	'BO01/DI/BPM-ACQ-01', 1, [1  5], 'BPMX-04'
    };
% Modified by Z.Marti - 23/3/2010
%==============================

% Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/XPosDDPeak'); % '/XposSA'); %Modified by Z.Marti - 23/3/2010 (provisional)
    AO.(iFam).Status(ii,:)             = bpm{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';
% AO.(iFam).Monitor.Handles = NaN * ones(size(AO.(iFam).DeviceList,1),1);

%=============================================
%% BPMy data: status field designates if BPM in use
%=============================================

iFam = 'BPMy';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'BPM';
AO.(iFam).MemberOf                 = {'PlotFamily'; 'VBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';




% bpm={
%      1,	'LI/DI/BPM-01', 1, [1  1], 'BPMY-00'
%      2,	'LT/DI/BPM-01', 1, [1  2], 'BPMY-01'
%      3,	'LT/DI/BPM-02', 1, [1  3], 'BPMY-02'
%      4,	'LT/DI/BPM-03', 1, [1  4], 'BPMY-03'
%      5,	'BO01/DI/BPM-01', 1, [1  5], 'BPMY-04'
%     };
%==============================
% Modified by Z.Marti - 23/3/2010
bpm={
     1,	'LI01/DI/BPM-ACQ-01', 1, [1  1], 'BPMY-00'
     2,	'LT01/DI/BPM-ACQ-01', 1, [1  2], 'BPMY-01'
     3,	'LT01/DI/BPM-ACQ-02', 1, [1  3], 'BPMY-02'
     4,	'LT01/DI/BPM-ACQ-03', 1, [1  4], 'BPMY-03'
     5,	'BO01/DI/BPM-ACQ-01', 1, [1  5], 'BPMY-04'
    };
% Modified by Z.Marti - 23/3/2010
%==============================




%Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/ZPosDDPeak');%'/ZposSA'); %Modified by Z.Marti - 23/3/2010 (provisional)
    AO.(iFam).Status(ii,:)             = bpm{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';
% AO.(iFam).Monitor.Handles = NaN * ones(size(AO.(iFam).DeviceList,1),1);

%===========================================================
%% HCM
%===========================================================

iFam ='HCM';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'COR';
AO.(iFam).MemberOf                 = {'COR'; 'MCOR'; 'HCM'; 'Magnet'; 'CORH'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;

cor={
     1,	'LT01/PC/CORH-01', 1, [1  1], 'CORH-01'
     2,	'LT01/PC/CORH-02', 1, [1  2], 'CORH-02'
     3,	'LT01/PC/CORH-03', 1, [1  3], 'CORH-03'
     4,	'LT01/PC/CORH-04', 1, [1  4], 'CORH-04'
%     5,	'LT01/PC/CORH-05', 1, [1  5], 'CORH-05' % there is only 5
%     correctors
     };

%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients('HCM');

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-2 2]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-4;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%% VCM

iFam ='VCM';

AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'COR';
AO.(iFam).MemberOf                 = {'COR'; 'MCOR'; 'VCM'; 'Magnet'; 'CORV'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;

cor={
     1,	'LT01/PC/CORV-01', 1, [1  1], 'CORV-01'
     2,	'LT01/PC/CORV-02', 1, [1  2], 'CORV-02'
     3,	'LT01/PC/CORV-03', 1, [1  3], 'CORV-03'
     4,	'LT01/PC/CORV-04', 1, [1  4], 'CORV-04'
%     5,	'LT01/PC/CORV-05', 1, [1  5], 'CORV-05'
     };

% Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients('VCM');

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-2 2]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-4;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%=============================
%        MAIN MAGNETS
%=============================

%===========
% Dipole data
%===========
%% *** BEND-01 ***
iFam = 'BEND';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).FamilyType                 = 'BEND';
AO.(iFam).MemberOf                   = {'BEND'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('BEND');
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn = @bend2gev;
AO.(iFam).Setpoint.Physics2HWFcn = @gev2bend;
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';

bend={
     1,	'LT01/PC/BEND-01', 1, [1  1], 'BEND-01'
     };

for ii=1:size(bend,1)
    AO.(iFam).ElementList(ii,:)        = bend{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bend(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bend(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = bend{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bend{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bend{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(bend(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 200]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-4;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.5;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%% *** BEND-02 ***
iFam = 'BM02';
AO.(iFam).FamilyName                 = iFam;
%AO.(iFam).FamilyType                 = 'BEND';
AO.(iFam).FamilyType                 = 'SEPTUM';
AO.(iFam).MemberOf                   = {'SEPTUM'; 'Magnet';};
%AO.(iFam).MemberOf                   = {'BEND'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                    = HW2PhysicsParams;

% HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
% Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
%AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
%AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;

AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
%

AO.(iFam).Setpoint.HW2PhysicsFcn      = @amp2k;
%AO.(iFam).Setpoint.Physics2HWFcn      = @k2amp;

AO.(iFam).Setpoint.HW2PhysicsFcn = @bend2gev;
AO.(iFam).Setpoint.Physics2HWFcn = @gev2bend;
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';

bend={
     1,	'LT01/PC/BEND-02', 1, [1  1], 'BEND-02'
     };

for ii=1:size(bend,1)
    AO.(iFam).ElementList(ii,:)        = bend{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bend(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bend(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = bend{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bend{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bend{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(bend(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 15]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-4;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.5;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


% AO.(iFam).DeviceName(:,:) = {'LT01/PC/BEND-01';'LT01/PC/BEND-02'};
% AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/current');
% 
% AO.(iFam).DeviceList(:,:) = [1 1];
% AO.(iFam).ElementList(:,:)= 1;
% AO.(iFam).Status          = 1;
% 
% val = 1;
% AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
% AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
% AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
% AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
% AO.(iFam).Monitor.Range(:,:) = [0 200]; % 148 A for 0.170 T @ 0.1 GeV
% 
% AO.(iFam).Setpoint = AO.(iFam).Monitor;
% AO.(iFam).Desired  = AO.(iFam).Monitor;
% AO.(iFam).Setpoint.MemberOf  = {'PlotFamily'};
% AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/currentPM');
% 
% AO.(iFam).Setpoint.Tolerance(:,:) = 0.05;
% AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.05;

% %% *** BM02 ***
% iFam = 'BM02';
% AO.(iFam).FamilyName                 = iFam;
% AO.(iFam).MemberOf                   = {'MachineConfig'; 'BEND'; 'Magnet';};
% HW2PhysicsParams                    = magnetcoefficients('BM02');
% Physics2HWParams                    = HW2PhysicsParams;
% 
% AO.(iFam).Monitor.Mode               = Mode;
% AO.(iFam).Monitor.DataType           = 'Scalar';
% AO.(iFam).Monitor.Units              = 'Hardware';
% AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
% AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
% AO.(iFam).Monitor.HWUnits            = 'A';
% AO.(iFam).Monitor.PhysicsUnits       = 'rad';
% 
% AO.(iFam).DeviceName(:,:) = {'LT01/PC/BEND-02'};
% AO.(iFam).Monitor.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName(:,:),'/current');
% 
% AO.(iFam).DeviceList(:,:) = [1 1];
% AO.(iFam).ElementList(:,:)= 1;
% AO.(iFam).Status          = 1;
% 
% val = 1;
% AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
% AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
% AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
% AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
% AO.(iFam).Monitor.Range(:,:) = [0 200]; % 76 A for 0.087 T @ 0.1 GeV
% 
% AO.(iFam).Setpoint = AO.(iFam).Monitor;
% AO.(iFam).Desired  = AO.(iFam).Monitor;
% AO.(iFam).Setpoint.MemberOf  = {'PlotFamily'};
% AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/currentPM');
% 
% AO.(iFam).Setpoint.Tolerance(:,:) = 0.05;
% AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.05;

%============
% QUADRUPOLES
%============
%% *** QUADS ***
iFam = 'Q';
AO.(iFam).FamilyType               = 'QUAD';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet';};

HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
     1,	'LT01/PC/Q-01', 1, [1 1], 'Q-01'
     2,	'LT01/PC/Q-02', 1, [1 2], 'Q-02'
     3,	'LT01/PC/Q-03', 1, [1 3], 'Q-03'
     4,	'LT01/PC/Q-04', 1, [1 4], 'Q-04'
     5,	'LT01/PC/Q-05', 1, [1 5], 'Q-05'
     6,	'LT01/PC/Q-06', 1, [1 6], 'Q-06'
     7,	'LT01/PC/Q-07', 1, [1 7], 'Q-07'
     8,	'LT01/PC/Q-08', 1, [1 8], 'Q-08'
     9,	'LT01/PC/Q-09', 1, [1 9], 'Q-09'
     };
 
for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams(ii,:);
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams(ii,:);
%     AO.(iFam).Monitor.Range(ii,:)        = [-15 15]; 
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-15 15]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 0.0001;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.2;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams(ii,:);
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams(ii,:);
end

AO.(iFam).Status = AO.(iFam).Status(:);

%%%
%% ** Injection Septum
%%%

ifam = 'SEINJ'; 
AO.(ifam).FamilyName           = ifam;
AO.(ifam).FamilyType           = 'SEPTUM';
%AO.(ifam).FamilyType           = 'BEND';
AO.(ifam).MemberOf             = {'Injection';'Septum';'Magnet';};

AO.(ifam).Monitor.MemberOf     = {'PlotFamily';};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

AO.(ifam).Status               = 1;
AO.(ifam).DeviceName           = {'BO/PC/SEINJ'};
AO.(ifam).CommonNames          = 'SEINJ';
AO.(ifam).ElementList          = 1;
AO.(ifam).DeviceList           = [1 1];
AO.(ifam).Monitor.TangoNames     = strcat(AO.(ifam).DeviceName, '/VoltageSetpoint');
AO.(ifam).Monitor.HW2PhysicsFcn  = @bend2gev;
AO.(ifam).Monitor.Physics2HWFcn  = @gev2bend;

HW2PhysicsParams = magnetcoefficients(ifam);
Physics2HWParams = HW2PhysicsParams;

AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'V';
AO.(ifam).Monitor.PhysicsUnits     = 'mrad';

AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Setpoint.MemberOf= {'MachineConfig'; 'PlotFamily';};
AO.(ifam).Setpoint.Range       = [0 120];
AO.(ifam).Setpoint.Tolerance   = 1.e-2;

%%%
%% ** Injection Kicker
%%%

ifam = 'KIINJ'; 
AO.(ifam).FamilyName           = ifam;
AO.(ifam).FamilyType           = 'KICKER';
AO.(ifam).MemberOf             = {'Injection';'Kicker';'Magnet';};

AO.(ifam).Monitor.MemberOf     = {'PlotFamily';};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

AO.(ifam).Status               = 1;
AO.(ifam).DeviceName           = {'BO/PC/KIINJ'};
AO.(ifam).CommonNames          = 'KIINJ';
AO.(ifam).ElementList          = 1;
AO.(ifam).DeviceList           = [1 1];
AO.(ifam).Monitor.TangoNames     = strcat(AO.(ifam).DeviceName, '/VoltageSetpoint');
AO.(ifam).Monitor.HW2PhysicsFcn  = @bend2gev;
AO.(ifam).Monitor.Physics2HWFcn  = @gev2bend;

HW2PhysicsParams = magnetcoefficients(ifam);
Physics2HWParams = HW2PhysicsParams;

AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'V';
AO.(ifam).Monitor.PhysicsUnits     = 'mrad';


AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Setpoint.MemberOf= {'MachineConfig'; 'PlotFamily'};
AO.(ifam).Setpoint.Range       = [0 25000];
AO.(ifam).Setpoint.Tolerance   = 1.e-2;

%%%%%%%%%%%%%%%%%%
%%% Diagnostics
%%%%%%%%%%%%%%%%%%
%% Charge Monitor - Moniteur de charge
ifam = 'BCM';

AO.(ifam).FamilyName             = 'BCM';
AO.(ifam).MemberOf               = {'Diag'; 'MC'; 'Archivable'; 'BCM'};
AO.(ifam).Mode                   = Mode;
AO.(ifam).DeviceName             = {'LT01/DI/BCM-01'; 'LT01/DI/BCM-02'};
AO.(ifam).CommonNames            = ['BCM-01';'BCM-02';];
AO.(ifam).DeviceList(:,:)        = [1 1; 1 2];
AO.(ifam).ElementList            = [1 2]';
AO.(ifam).Status                 = [1 1]';
AO.(ifam).Monitor.TangoNames     = [strcat(AO.(ifam).DeviceName(1,:), '/qIct1'); ...
                                    strcat(AO.(ifam).DeviceName(2,:), '/qIct2')];
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = [NaN; NaN]';
AO.(ifam).Monitor.DataType       = 'Vector';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'nC';
AO.(ifam).Monitor.PhysicsUnits   = 'nC';
AO.(ifam).Monitor.HW2PhysicsParams = 1.0;
AO.(ifam).Monitor.Physics2HWParams = 1.0;

%%
% The operational mode sets the path, filenames, and other important parameters
% Run setoperationalmode after most of the AO is built so that the Units and Mode fields
% can be set in setoperationalmode
setao(AO);
setoperationalmode(OperationalMode);
AO = getao;

%======================================================================
%% Append Accelerator Toolbox information
%======================================================================
%======================================================================
% disp('** Initializing Accelerator Toolbox information');
% 
% 
% 
% ATindx = atindex(THERING);  %structure with fields containing indices
% 
% s = findspos(THERING,1:length(THERING)+1)';
% 
% %% Horizontal BPMs
% iFam = ('BPMx');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BPM(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% Vertical BPMs
% iFam = ('BPMy');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BPM(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);  
% 
% %% HORIZONTAL CORRECTORS
% iFam = ('HCM');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% VERTICAL CORRECTORS
% iFam = ('VCM');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% BENDING magnets
% for k = 1:2,
%     iFam = ['BM0' num2str(k)];
%     AO.(iFam).AT.ATType  = 'BEND';
%     AO.(iFam).AT.ATIndex = eval(['ATindx.' iFam '(:)']);
%     AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% end
% 
% % One group of all dipoles
% %AO.(iFam).Position   = reshape(s(AO.(iFam).AT.ATIndex),1,40);
% %AT.(iFam).AT.ATParamGroup = mkparamgroup(THERING,AT.(iFam).AT.ATIndex,'K2');
% 
% %% QUADRUPOLES
% iFam = ('Q');
% AO.(iFam).AT.ATType  = 'QUAD';
% AO.(iFam).AT.ATIndex = eval(['ATindx.' iFam '(:)']);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);

%%

AO.HCM.Setpoint.DeltaRespMat = physics2hw('HCM','Setpoint', 1e-4, AO.HCM.DeviceList);
AO.VCM.Setpoint.DeltaRespMat = physics2hw('VCM','Setpoint', 1e-4, AO.VCM.DeviceList);

% AO.Q.Setpoint.DeltaRespMat  = physics2hw('Q', 'Setpoint', AO.Q.Setpoint.DeltaRespMat,  AO.Q.DeviceList);

setao(AO);

% % reference values
% global RefOptics;
% %disp '    Reference optics, tunes and AO stored in RefOptics'
% RefOptics.AO=getao();
% RefOptics.twiss=gettwiss();
% RefOptics.tune= gettune();
