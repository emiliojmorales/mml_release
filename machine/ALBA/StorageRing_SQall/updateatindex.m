function updateatindex
%UPDATEATINDEX - Updates the AT indices in the MiddleLayer with the present AT lattice (THERING)


global THERING


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Append Accelerator Toolbox information %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Since changes in the AT model could change the AT indexes, etc,
% It's best to regenerate all the model indices whenever a model is loaded

% Sort by family first (findcells is linear and slow)
Indices = atindex(THERING);

AO = getao;


try
    AO.BPMx.AT.ATType = 'X';
    AO.BPMx.AT.ATIndex = Indices.BPM(:); % findcells(THERING,'FamName','BPM')';
    AO.BPMx.Position = findspos(THERING, AO.BPMx.AT.ATIndex)';

    AO.BPMy.AT.ATType = 'Y';
    AO.BPMy.AT.ATIndex = Indices.BPM(:); % findcells(THERING,'FamName','BPM')';
    AO.BPMy.Position = findspos(THERING, AO.BPMy.AT.ATIndex)';
catch
    fprintf('   BPM family not found in the model.\n');
end

try
    % Horizontal correctors
    AO.HCM.AT.ATType = 'HCM';
    AO.HCM.AT.ATIndex = buildatindex(AO.HCM.FamilyName, Indices.COR);
    AO.HCM.Position = findspos(THERING, AO.HCM.AT.ATIndex)';

    % Vertical correctors
    AO.VCM.AT.ATType = 'VCM';
    AO.VCM.AT.ATIndex = AO.HCM.AT.ATIndex;
    AO.VCM.Position = findspos(THERING, AO.VCM.AT.ATIndex)';
catch
    fprintf('   COR family not found in the model.\n');
end


try
    AO.QH01.AT.ATType = 'QUAD';
    ATIndex = [Indices.QH01(:)];
    AO.QH01.AT.ATIndex  = sort(ATIndex);
    %AO.QH01.AT.ATIndex = buildatindex(AO.QH01.FamilyName, Indices.QH01);
    AO.QH01.Position = findspos(THERING, AO.QH01.AT.ATIndex(:,1))';
catch
    fprintf('   QH01 family not found in the model.\n');
end
try
    AO.QH02.AT.ATType = 'QUAD';
    ATIndex = [Indices.QH02(:)];
    AO.QH02.AT.ATIndex  = sort(ATIndex);
    %AO.QH02.AT.ATIndex = buildatindex(AO.QH02.FamilyName, Indices.QH02);
    AO.QH02.Position = findspos(THERING, AO.QH02.AT.ATIndex(:,1))';
catch
    fprintf('   QH02 family not found in the model.\n');
end
try
    AO.QH03.AT.ATType = 'QUAD';
    ATIndex = [Indices.QH03(:)];
    AO.QH03.AT.ATIndex  = sort(ATIndex);
    %AO.QH03.AT.ATIndex = buildatindex(AO.QH03.FamilyName, Indices.QH03);
    AO.QH03.Position = findspos(THERING, AO.QH03.AT.ATIndex(:,1))';
catch
    fprintf('   QH03 family not found in the model.\n');
end
try
    AO.QH04.AT.ATType = 'QUAD';
    ATIndex = [Indices.QH04(:)];
    AO.QH04.AT.ATIndex  = sort(ATIndex);
    %AO.QH04.AT.ATIndex = buildatindex(AO.QH04.FamilyName, Indices.QH04);
    AO.QH04.Position = findspos(THERING, AO.QH04.AT.ATIndex(:,1))';
catch
    fprintf('   QH04 family not found in the model.\n');
end
try
    AO.QH05.AT.ATType = 'QUAD';
    ATIndex = [Indices.QH05(:)];
    AO.QH05.AT.ATIndex  = sort(ATIndex);
    %AO.QH05.AT.ATIndex = buildatindex(AO.QH05.FamilyName, Indices.QH05);
    AO.QH05.Position = findspos(THERING, AO.QH05.AT.ATIndex(:,1))';
catch
    fprintf('   QH05 family not found in the model.\n');
end
try
    AO.QH06.AT.ATType = 'QUAD';
    ATIndex = [Indices.QH06(:)];
    AO.QH06.AT.ATIndex  = sort(ATIndex);
    %AO.QH06.AT.ATIndex = buildatindex(AO.QH06.FamilyName, Indices.QH06);
    AO.QH06.Position = findspos(THERING, AO.QH06.AT.ATIndex(:,1))';
catch
    fprintf('   QH06 family not found in the model.\n');
end
try
    AO.QH07.AT.ATType = 'QUAD';
    ATIndex = [Indices.QH07(:)];
    AO.QH07.AT.ATIndex  = sort(ATIndex);
    %AO.QH07.AT.ATIndex = buildatindex(AO.QH07.FamilyName, Indices.QH07);
    AO.QH07.Position = findspos(THERING, AO.QH07.AT.ATIndex(:,1))';
catch
    fprintf('   QH07 family not found in the model.\n');
end


try
    AO.QH08.AT.ATType = 'QUAD';
    ATIndex = [Indices.QH08(:)];
    AO.QH08.AT.ATIndex  = sort(ATIndex);
    %AO.QH08.AT.ATIndex = buildatindex(AO.QH08.FamilyName, Indices.QH08);
    AO.QH08.Position = findspos(THERING, AO.QH08.AT.ATIndex(:,1))';
catch
    fprintf('   QH08 family not found in the model.\n');
end

try
    AO.QH09.AT.ATType = 'QUAD';
    ATIndex = [Indices.QH09(:)];
    AO.QH09.AT.ATIndex  = sort(ATIndex);
    %AO.QH09.AT.ATIndex = buildatindex(AO.QH09.FamilyName, Indices.QH09);
    AO.QH09.Position = findspos(THERING, AO.QH09.AT.ATIndex(:,1))';
catch
    fprintf('   QH09 family not found in the model.\n');
end

try
    AO.QH10.AT.ATType = 'QUAD';
    ATIndex = [Indices.QH10(:)];
    AO.QH10.AT.ATIndex  = sort(ATIndex);
    %AO.QH10.AT.ATIndex = buildatindex(AO.QH10.FamilyName, Indices.QH10);
    AO.QH10.Position = findspos(THERING, AO.QH10.AT.ATIndex(:,1))';
catch
    fprintf('   QH10 family not found in the model.\n');
end


try
    AO.QV01.AT.ATType = 'QUAD';
    ATIndex = [Indices.QV01(:)];
    AO.QV01.AT.ATIndex  = sort(ATIndex);
    %AO.QV01.AT.ATIndex = buildatindex(AO.QV01.FamilyName, Indices.QV01);
    AO.QV01.Position = findspos(THERING, AO.QV01.AT.ATIndex(:,1))';
catch
    fprintf('   QV01 family not found in the model.\n');
end
try
    AO.QV02.AT.ATType = 'QUAD';
    ATIndex = [Indices.QV02(:)];
    AO.QV02.AT.ATIndex  = sort(ATIndex);
    %AO.QV02.AT.ATIndex = buildatindex(AO.QV02.FamilyName, Indices.QV02);
    AO.QV02.Position = findspos(THERING, AO.QV02.AT.ATIndex(:,1))';
catch
    fprintf('   QV02 family not found in the model.\n');
end
try
    AO.QV03.AT.ATType = 'QUAD';
    ATIndex = [Indices.QV03(:)];
    AO.QV03.AT.ATIndex  = sort(ATIndex);
    %AO.QV03.AT.ATIndex = buildatindex(AO.QV03.FamilyName, Indices.QV03);
    AO.QV03.Position = findspos(THERING, AO.QV03.AT.ATIndex(:,1))';
catch
    fprintf('   QV03 family not found in the model.\n');
end

try
    AO.QV04.AT.ATType = 'QUAD';
    ATIndex = [Indices.QV04(:)];
    AO.QV04.AT.ATIndex  = sort(ATIndex);
    %AO.QV04.AT.ATIndex = buildatindex(AO.QV04.FamilyName, Indices.QV03);
    AO.QV04.Position = findspos(THERING, AO.QV04.AT.ATIndex(:,1))';
catch
    fprintf('   QV04 family not found in the model.\n');
end







try
    AO.FQH01.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQH01(:)];
    AO.FQH01.AT.ATIndex  = sort(ATIndex);
    %AO.FQH01.AT.ATIndex = buildatindex(AO.FQH01.FamilyName, Indices.FQH01);
    AO.FQH01.Position = findspos(THERING, AO.FQH01.AT.ATIndex(:,1))';
catch
    fprintf('   FQH01 family not found in the model.\n');
end
try
    AO.FQH02.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQH02(:)];
    AO.FQH02.AT.ATIndex  = sort(ATIndex);
    %AO.FQH02.AT.ATIndex = buildatindex(AO.FQH02.FamilyName, Indices.FQH02);
    AO.FQH02.Position = findspos(THERING, AO.FQH02.AT.ATIndex(:,1))';
catch
    fprintf('   FQH02 family not found in the model.\n');
end
try
    AO.FQH03.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQH03(:)];
    AO.FQH03.AT.ATIndex  = sort(ATIndex);
    %AO.FQH03.AT.ATIndex = buildatindex(AO.FQH03.FamilyName, Indices.FQH03);
    AO.FQH03.Position = findspos(THERING, AO.FQH03.AT.ATIndex(:,1))';
catch
    fprintf('   FQH03 family not found in the model.\n');
end
try
    AO.FQH04.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQH04(:)];
    AO.FQH04.AT.ATIndex  = sort(ATIndex);
    %AO.FQH04.AT.ATIndex = buildatindex(AO.FQH04.FamilyName, Indices.FQH04);
    AO.FQH04.Position = findspos(THERING, AO.FQH04.AT.ATIndex(:,1))';
catch
    fprintf('   FQH04 family not found in the model.\n');
end
try
    AO.FQH05.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQH05(:)];
    AO.FQH05.AT.ATIndex  = sort(ATIndex);
    %AO.FQH05.AT.ATIndex = buildatindex(AO.FQH05.FamilyName, Indices.FQH05);
    AO.FQH05.Position = findspos(THERING, AO.FQH05.AT.ATIndex(:,1))';
catch
    fprintf('   FQH05 family not found in the model.\n');
end
try
    AO.FQH06.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQH06(:)];
    AO.FQH06.AT.ATIndex  = sort(ATIndex);
    %AO.FQH06.AT.ATIndex = buildatindex(AO.FQH06.FamilyName, Indices.FQH06);
    AO.FQH06.Position = findspos(THERING, AO.FQH06.AT.ATIndex(:,1))';
catch
    fprintf('   FQH06 family not found in the model.\n');
end
try
    AO.FQH07.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQH07(:)];
    AO.FQH07.AT.ATIndex  = sort(ATIndex);
    %AO.FQH07.AT.ATIndex = buildatindex(AO.FQH07.FamilyName, Indices.FQH07);
    AO.FQH07.Position = findspos(THERING, AO.FQH07.AT.ATIndex(:,1))';
catch
    fprintf('   FQH07 family not found in the model.\n');
end


try
    AO.FQH08.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQH08(:)];
    AO.FQH08.AT.ATIndex  = sort(ATIndex);
    %AO.FQH08.AT.ATIndex = buildatindex(AO.FQH08.FamilyName, Indices.FQH08);
    AO.FQH08.Position = findspos(THERING, AO.FQH08.AT.ATIndex(:,1))';
catch
    fprintf('   FQH08 family not found in the model.\n');
end

try
    AO.FQH09.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQH09(:)];
    AO.FQH09.AT.ATIndex  = sort(ATIndex);
    %AO.FQH09.AT.ATIndex = buildatindex(AO.FQH09.FamilyName, Indices.FQH09);
    AO.FQH09.Position = findspos(THERING, AO.FQH09.AT.ATIndex(:,1))';
catch
    fprintf('   FQH09 family not found in the model.\n');
end

try
    AO.FQH10.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQH10(:)];
    AO.FQH10.AT.ATIndex  = sort(ATIndex);
    %AO.FQH10.AT.ATIndex = buildatindex(AO.FQH10.FamilyName, Indices.FQH10);
    AO.FQH10.Position = findspos(THERING, AO.FQH10.AT.ATIndex(:,1))';
catch
    fprintf('   FQH10 family not found in the model.\n');
end


try
    AO.FQV01.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQV01(:)];
    AO.FQV01.AT.ATIndex  = sort(ATIndex);
    %AO.FQV01.AT.ATIndex = buildatindex(AO.FQV01.FamilyName, Indices.FQV01);
    AO.FQV01.Position = findspos(THERING, AO.FQV01.AT.ATIndex(:,1))';
catch
    fprintf('   FQV01 family not found in the model.\n');
end
try
    AO.FQV02.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQV02(:)];
    AO.FQV02.AT.ATIndex  = sort(ATIndex);
    %AO.FQV02.AT.ATIndex = buildatindex(AO.FQV02.FamilyName, Indices.FQV02);
    AO.FQV02.Position = findspos(THERING, AO.FQV02.AT.ATIndex(:,1))';
catch
    fprintf('   FQV02 family not found in the model.\n');
end
try
    AO.FQV03.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQV03(:)];
    AO.FQV03.AT.ATIndex  = sort(ATIndex);
    %AO.FQV03.AT.ATIndex = buildatindex(AO.FQV03.FamilyName, Indices.FQV03);
    AO.FQV03.Position = findspos(THERING, AO.FQV03.AT.ATIndex(:,1))';
catch
    fprintf('   FQV03 family not found in the model.\n');
end

try
    AO.FQV04.AT.ATType = 'QUAD';
    ATIndex = [Indices.FQV04(:)];
    AO.FQV04.AT.ATIndex  = sort(ATIndex);
    %AO.FQV04.AT.ATIndex = buildatindex(AO.FQV04.FamilyName, Indices.FQV03);
    AO.FQV04.Position = findspos(THERING, AO.FQV04.AT.ATIndex(:,1))';
catch
    fprintf('   FQV04 family not found in the model.\n');
end





try
    AO.SF1.AT.ATType = 'SEXT';
    AO.SF1.AT.ATIndex = buildatindex(AO.SF1.FamilyName, Indices.SF1);
    AO.SF1.Position = findspos(THERING, AO.SF1.AT.ATIndex(:,1))';
catch
    fprintf('   SF1 family not found in the model.\n');
end
try
    AO.SF2.AT.ATType = 'SEXT';
    AO.SF2.AT.ATIndex = buildatindex(AO.SF2.FamilyName, Indices.SF2);
    AO.SF2.Position = findspos(THERING, AO.SF2.AT.ATIndex(:,1))';
catch
    fprintf('   SF2 family not found in the model.\n');
end
try
    AO.SF3.AT.ATType = 'SEXT';
    AO.SF3.AT.ATIndex = buildatindex(AO.SF3.FamilyName, Indices.SF3);
    AO.SF3.Position = findspos(THERING, AO.SF3.AT.ATIndex(:,1))';
catch
    fprintf('   SF3 family not found in the model.\n');
end
try
    AO.SF4.AT.ATType = 'SEXT';
    AO.SF4.AT.ATIndex = buildatindex(AO.SF4.FamilyName, Indices.SF4);
    AO.SF4.Position = findspos(THERING, AO.SF4.AT.ATIndex(:,1))';
catch
    fprintf('   SF4 family not found in the model.\n');
end


try
    AO.SD1.AT.ATType = 'SEXT';
    AO.SD1.AT.ATIndex = buildatindex(AO.SD1.FamilyName, Indices.SD1);
    AO.SD1.Position = findspos(THERING, AO.SD1.AT.ATIndex(:,1))';
catch
    fprintf('   SD1 family not found in the model.\n');
end
try
    AO.SD2.AT.ATType = 'SEXT';
    AO.SD2.AT.ATIndex = buildatindex(AO.SD2.FamilyName, Indices.SD2);
    AO.SD2.Position = findspos(THERING, AO.SD2.AT.ATIndex(:,1))';
catch
    fprintf('   SD2 family not found in the model.\n');
end
try
    AO.SD3.AT.ATType = 'SEXT';
    AO.SD3.AT.ATIndex = buildatindex(AO.SD3.FamilyName, Indices.SD3);
    AO.SD3.Position = findspos(THERING, AO.SD3.AT.ATIndex(:,1))';
catch
    fprintf('   SD3 family not found in the model.\n');
end
try
    AO.SD4.AT.ATType = 'SEXT';
    AO.SD4.AT.ATIndex = buildatindex(AO.SD4.FamilyName, Indices.SD4);
    AO.SD4.Position = findspos(THERING, AO.SD4.AT.ATIndex(:,1))';
catch
    fprintf('   SD4 family not found in the model.\n');
end
try
    AO.SD5.AT.ATType = 'SEXT';
    AO.SD5.AT.ATIndex = buildatindex(AO.SD5.FamilyName, Indices.SD5);
    AO.SD5.Position = findspos(THERING, AO.SD5.AT.ATIndex(:,1))';
catch
    fprintf('   SD5 family not found in the model.\n');
end


try
    AO.BEND.AT.ATType = 'BEND';
    AO.BEND.AT.ATIndex = buildatindex(AO.BEND.FamilyName, Indices.BEND);
    AO.BEND.Position = findspos(THERING, AO.BEND.AT.ATIndex(:,1))';
catch
    fprintf('   BEND family not found in the model.\n');
end

try
    %RF Cavity
    AO.RF.AT.ATType = 'RF Cavity';
    AO.RF.AT.ATIndex = findcells(THERING,'Frequency')';
    AO.RF.Position = findspos(THERING, AO.RF.AT.ATIndex(:,1))';
catch
    fprintf('   RF cavity not found in the model.\n');
end

try 
   AO.QS1.AT.ATType='SkewQuad';
   ATIndex = [Indices.QS1(:)];
   AO.QS1.AT.ATIndex  = sort(ATIndex);
   %AO.QS2.AT.ATIndex = buildatindex(AO.QS2.FamilyName, Indices.QS2);
   AO.QS1.Position = findspos(THERING, AO.QS1.AT.ATIndex)';
catch
    fprintf('  QS1 family not found in the model.\n');
end

try 
   AO.QS2.AT.ATType='SkewQuad';
   ATIndex = [Indices.QS2(:)];
   AO.QS2.AT.ATIndex  = sort(ATIndex);
   %AO.QS2.AT.ATIndex = buildatindex(AO.QS2.FamilyName, Indices.QS2);
   AO.QS2.Position = findspos(THERING, AO.QS2.AT.ATIndex)';
catch
    fprintf('  QS2 family not found in the model.\n');
end

try 
   AO.QS3.AT.ATType='SkewQuad';
   ATIndex = [Indices.QS3(:)];
   AO.QS3.AT.ATIndex  = sort(ATIndex);
   %AO.QS3.AT.ATIndex = buildatindex(AO.QS3.FamilyName, Indices.QS3);
   AO.QS3.Position = findspos(THERING, AO.QS3.AT.ATIndex)';
catch
    fprintf('  QS3 family not found in the model.\n');
end


try 
   AO.QS4.AT.ATType='SkewQuad';
   ATIndex = [Indices.QS4(:)];
   AO.QS4.AT.ATIndex  = sort(ATIndex);
   %AO.QS4.AT.ATIndex = buildatindex(AO.QS4.FamilyName, Indices.QS4);
   AO.QS4.Position = findspos(THERING, AO.QS4.AT.ATIndex)';
catch
    fprintf('  QS4 family not found in the model.\n');
end


try 
   AO.QS5.AT.ATType='SkewQuad';
   ATIndex = [Indices.QS5(:)];
   AO.QS5.AT.ATIndex  = sort(ATIndex);
   %AO.QS5.AT.ATIndex = buildatindex(AO.QS5.FamilyName, Indices.QS5);
   AO.QS5.Position = findspos(THERING, AO.QS1.AT.ATIndex)';
catch
    fprintf('  QS5 family not found in the model.\n');
end


try 
   AO.QS6.AT.ATType='SkewQuad';
   ATIndex = [Indices.QS6(:)];
   AO.QS6.AT.ATIndex  = sort(ATIndex);
   %AO.QS6.AT.ATIndex = buildatindex(AO.QS6.FamilyName, Indices.QS6);
   AO.QS6.Position = findspos(THERING, AO.QS6.AT.ATIndex)';
catch
    fprintf('  QS6 family not found in the model.\n');
end


try 
   AO.QS7.AT.ATType='SkewQuad';
   ATIndex = [Indices.QS7(:)];
   AO.QS7.AT.ATIndex  = sort(ATIndex);
   %AO.QS7.AT.ATIndex = buildatindex(AO.QS7.FamilyName, Indices.QS7);
   AO.QS7.Position = findspos(THERING, AO.QS7.AT.ATIndex)';
catch
    fprintf('  QS7 family not found in the model.\n');
end

try 
   AO.QS8.AT.ATType='SkewQuad';
   ATIndex = [Indices.QS8(:)];
   AO.QS8.AT.ATIndex  = sort(ATIndex);
   %AO.QS8.AT.ATIndex = buildatindex(AO.QS8.FamilyName, Indices.QS8);
   AO.QS8.Position = findspos(THERING, AO.QS8.AT.ATIndex)';
catch
    fprintf('  QS8 family not found in the model.\n');
end

try 
   AO.QS9.AT.ATType='SkewQuad';
   ATIndex = [Indices.QS9(:)];
   AO.QS9.AT.ATIndex  = sort(ATIndex);
   %AO.QV02.AT.ATIndex = buildatindex(AO.QS9.FamilyName, Indices.QS9);
   AO.QS9.Position = findspos(THERING, AO.QS9.AT.ATIndex)';
catch
    fprintf('  QS9 family not found in the model.\n');
end

try 
   AO.IK.AT.ATType='Kicker';
   ATIndex = [Indices.IK(:)];
   AO.IK.AT.ATIndex  = sort(ATIndex);
   %AO.QV02.AT.ATIndex = buildatindex(AO.QV02.FamilyName, Indices.QV02);
   AO.IK.Position = findspos(THERING, AO.IK.AT.ATIndex)';
catch
    fprintf('  IK family not found in the model.\n');
end

setao(AO);



% Set TwissData at the start of the storage ring
try   
    % BTS twiss parameters at the input 
    TwissData.alpha = [0 0]';
    TwissData.beta  = [11.1966 5.9268]';
    TwissData.mu    = [0 0]';
    TwissData.ClosedOrbit = [0 0 0 0]';
    TwissData.dP = 0;
    TwissData.dL = 0;
    TwissData.Dispersion  = [0.1459 0 0 0]';
    
    setpvmodel('TwissData', '', TwissData);  % Same as, THERING{1}.TwissData = TwissData;
catch
     warning('Setting the twiss data parameters in the MML failed.');
end
