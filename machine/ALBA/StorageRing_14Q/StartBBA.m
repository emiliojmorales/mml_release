function BPMData = StartBBA(varargin)
% launches a BBA for the BBA.
% BPMData = StartBBA
% BPMData = StartBBA(plane)
%   (plane = 1 -> horizontal, plane = 2 ->  vertical)
%   for all the BPMs
% BPMData = StartBBA(plane, 'Select')
%   (plane = 1 -> horizontal, plane = 2 ->  vertical)
%   Showing a selector for the BPMs
% BPMData = StartBBA(plane, list)
%   (plane = 1 -> horizontal, plane = 2 ->  vertical)
%   For the BPMs in the list
%   eg BPMData = StartBBA(1, 1:10)
% The data are save in a directory in the QMS folder

if length(varargin)==0,
    plane = 1;
    BPMFamily = 'BPMx';
    BPMDevList = getlist(BPMFamily); % only the ones with good status
elseif length(varargin)==1,
    plane = varargin{1};
    BPMFamily = 'BPMy';
    BPMDevList = getlist(BPMFamily); % only the ones with good status
elseif length(varargin)==2,
    plane = varargin{1};
    list=varargin{2};
    if isstr(list),
        if plane ==1,
            BPMFamily = 'BPMx';
            BPMDevList = editlist(family2dev('BPMx'), 'BPMx');
        else
            BPMFamily = 'BPMy';
            BPMDevList = editlist(family2dev('BPMy'), 'BPMy');
        end
    else
        if plane ==1,
            BPMFamily = 'BPMx';
            BPMDevList = getlist(BPMFamily);
            BPMDevList = BPMDevList(list, :);
        else
            BPMFamily = 'BPMy';
            BPMDevList = getlist(BPMFamily);
            BPMDevList = BPMDevList(list, :);
        end
        
    end
end
current=4;
outlimit=100;
Plane=plane;

%%%%%%%%%%%%%%%%%%%%%
% Make the BPM list %
%%%%%%%%%%%%%%%%%%%%%

% for i = 1:4
%     RemoveDeviceList(i,:) = [i 3];
% end
% i = findrowindex(RemoveDeviceList, BPMDevList);
% BPMDevList(i,:) = [];



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Clean out the data directory %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[QUADFamily, QUADDev] = bpm2quad(BPMFamily, BPMDevList(1,:));
q = quadcenterinit(QUADFamily, QUADDev, Plane);

DirStart = pwd;
NewDir = 'old';
cd(q.DataDirectory);
if ~exist(NewDir,'dir')
    mkdir(NewDir);
end
try
    movefile('*.mat', NewDir);
    movefile('quadcenter.log', NewDir);
catch
end
cd(DirStart);



%%%%%%%%%%%%%%%%%%%%%%%%
% Loop on all the BPMs %
%%%%%%%%%%%%%%%%%%%%%%%%
t0 = gettime;
for i = 1:size(BPMDevList,1)
    
    
    
    [QUADFamily, QUADDev, DelSpos, PhaseAdvanceX, PhaseAdvanceY] = bpm2quad(BPMFamily, BPMDevList(i,:));
    fprintf('   %d. BPM(%2d,%d)  %s(%2d,%d)  BPM-to-Quad Distance=%f meters\n', i, BPMDevList(i,:), QUADFamily, QUADDev, DelSpos);
    QMS=quadcenterinit(QUADFamily,QUADDev,Plane);
    QMS.OutlierFactor=outlimit;
    QMS.ExtraDelay=0;
    
    
    try
        [QMS1 QMS2] = quadcenter(QMS,2);
    catch
        fprintf('       Error occurred in BPM(%2d,%d).  Moving to the next BPM.\n', BPMDevList);
    end
    
    DCCT = getdcct;
    if DCCT < current
        % Redo magnet if the beam dumped
        sound(cos(1:10000));
        beep
        beep
        beep
        fprintf('   Current to low.  Refill and hit return.\n');
        pause;
        fprintf(' \n');
        [QMS1 QMS2] = quadcenter(QMS,2);
    elseif DCCT < current & i<size(BPMDevList,1)-1
        sound(cos(1:10000));
        beep
        beep
        beep
        fprintf('   Current to low.  Refill and hit return.\n');
        pause;
        fprintf(' \n');
    end
    BPMData(i)=QMS1;
    
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Move data to new directory by date %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DirStart = pwd;
NewDir = sprintf('%4d-%02d-%02d_%02d-%02d-%02.0f', clock);
cd(QMS1.DataDirectory);
if ~exist(NewDir,'dir')
    try
        mkdir(NewDir);
    catch
    end
end
try
    movefile('*.mat', NewDir);
    fprintf('   Data moved to %s\n', [QMS1.DataDirectory '/' NewDir]);
    try
        movefile('quadcenter.log', NewDir);
    catch
        fprintf('   Error occurred when moving log files to %s\n', [QMS1.DataDirectory NewDir]);
    end
catch
    fprintf('   Error occurred when moving data files to %s\n', [QMS1.DataDirectory NewDir]);
end
cd(NewDir)
save BPMData BPMData
cd(DirStart);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Print time and wake-up call %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('   Data collection time %f minutes\n', (gettime-t0)/60);
sound(cos(1:10000));
sound(cos(1:10000));