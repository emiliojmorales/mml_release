function soleilinit(OperationalMode)
%SOLEILINIT - Initializes params for SOLEIL control in MATLAB
%
% Written by Laurent S. Nadolski, Synchrotron SOLEIL
%
%==========================
% Accelerator Family Fields
%==========================
% FamilyName            BPMx, HCOR, etc
% CommonNames           Shortcut name for each element
% DeviceList            [Sector, Number]
% ElementList           number in list
% Position              m, if thick, it is not the magnet center
%
% MONITOR FIELD
% Mode                  online/manual/special/simulator
% TangoNames            Device Tango Names
% Units                 Physics or HW
% HW2PhysicsFcn         function handle used to convert from hardware to physics units ==> inline will not compile, see below
% HW2PhysicsParams      params used for conversion function
% Physics2HWFcn         function handle used to convert from physics to hardware units
% Physics2HWParams      params used for conversion function
% HWUnits               units for Hardware 'A';
% PhysicsUnits          units for physics 'Rad';
% Handles               monitor handle
%
% SETPOINT FIELDS
% Mode                  online/manual/special/simulator
% TangoNames            Devices tango names
% Units                 hardware or physics
% HW2PhysicsFcn         function handle used to convert from hardware to physics units
% HW2PhysicsParams      params used for conversion function
% Physics2HWFcn         function handle used to convert from physics to hardware units
% Physics2HWParams      params used for conversion function
% HWUnits               units for Hardware 'A';
% PhysicsUnits          units for physics 'Rad';
% Range                 minsetpoint, maxsetpoint;
% Tolerance             setpoint-monitor
% Handles               setpoint handle
%
%=============================================
% Accelerator Toolbox Simulation Fields
%=============================================
% ATType                Quad, Sext, etc
% ATIndex               index in THERING
% ATParamGroup      param group
%
%============
% Family List
%============
%    BPMx
%    BPMz
%    HCOR
%    VCOR
%    BEND
%    Q1 to Q10
%    S1 to S10
%    RF
%    TUNE
%    DCCT
%    Machine Params
%
% NOTES
%   All sextupoles have H and V corrector and skew quadrupole windings
%
%  See Also setpathsoleil, setpathmml, aoinit, setoperationalmode, updateatindex

%
%
% TODO, Deltakick for BPM orbit response matrix  Warning optics dependent cf. Low alpha lattice
%       to be put into setoperationalmode

% DO NOT REMOVE LSN
%suppress optimization message
%#ok<*ASGLU>

% CONTROL ROOM
% Check for nanoscopium
% Check for attribute names
% Check for range value of sextupoles

% If controlromm user is operator and online mode

[statuss WHO] = system('whoami');
% system gives back an visible character: carriage return!
% so comparison on the number of caracters
if strncmp(WHO, 'operateur',9),
    ControlRoomFlag = 1;
    Mode = 'Online';
else
    ControlRoomFlag = 0;
    Mode = 'Simulator';
end

%% Default operation mode (see setoperationalmode)
if nargin < 1
    %OperationalMode = 9; % with S11 betax=10m
    %OperationalMode = 19; % without S11
    OperationalMode = 16; % betax=5m 
end

% Define some global variables

h = waitbar(0,'soleilinit initialization, please wait');

%==============================
%% load AcceleratorData structure
%==============================

setad([]);       %clear AcceleratorData memory
AD.SubMachine = 'StorageRing';   % Will already be defined if setpathmml was used
AD.Energy        = 2.7391; % Energy in GeV needed for magnet calibration. Do not remove!

setad(AD);

%%%%%%%%%%%%%%%%%%%%
% ACCELERATOR OBJECT
%%%%%%%%%%%%%%%%%%%%

if ~isempty(getappdata(0, 'AcceleratorObjects'))
    AO = getao;
    % Check if online and AO is from Storagering
    if ControlRoomFlag && isfield(AO, 'Q10') 
        local_tango_kill_allgroup(AO); % kill all TANGO group
    end
end
setao([]); AO =[];   %clear previous AcceleratorObjects
waitbar(0.05,h);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BPM
% status field designates if BPM in use
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BPMx Horizontal plane
ifam = 'BPMx';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'BPM';
AO.(ifam).MemberOf                 = {'BPM'; 'HBPM'; 'PlotFamily'; 'Archivable'};
AO.(ifam).Monitor.Mode             = Mode;
%AO.(ifam).Monitor.Mode             = 'Special';
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'mm';
AO.(ifam).Monitor.PhysicsUnits     = 'm';
AO.(ifam).Monitor.SpecialFunctionGet = 'gethbpmgroup';
%AO.(ifam).Monitor.SpecialFunctionGet = 'gethbpmaverage';
%AO.(ifam).Monitor.SpecialFunctionGet = 'gethturdevnumberyturn';

% ElemList devlist tangoname status common
varlist = {
    1 [ 1  2] 'ANS-C01/DG/BPM.2'  1 'BPMx001'
    2 [ 1  3] 'ANS-C01/DG/BPM.3'  1 'BPMx002'
    3 [ 1  4] 'ANS-C01/DG/BPM.4'  1 'BPMx003'
    4 [ 1  5] 'ANS-C01/DG/BPM.5'  1 'BPMx004'
    5 [ 1  6] 'ANS-C01/DG/BPM.6'  1 'BPMx005'
    6 [ 1  7] 'ANS-C01/DG/BPM.7'  1 'BPMx006'
    7 [ 2  1] 'ANS-C02/DG/BPM.1'  1 'BPMx007'
    8 [ 2  2] 'ANS-C02/DG/BPM.2'  1 'BPMx008'
    9 [ 2  3] 'ANS-C02/DG/BPM.3'  1 'BPMx009'
    10 [ 2  4] 'ANS-C02/DG/BPM.4'  1 'BPMx010'
    11 [ 2  5] 'ANS-C02/DG/BPM.5'  1 'BPMx011'
    12 [ 2  6] 'ANS-C02/DG/BPM.6'  1 'BPMx012'
    13 [ 2  7] 'ANS-C02/DG/BPM.7'  1 'BPMx013'
    14 [ 2  8] 'ANS-C02/DG/BPM.8'  1 'BPMx014'
    15 [ 3  1] 'ANS-C03/DG/BPM.1'  1 'BPMx015'
    16 [ 3  2] 'ANS-C03/DG/BPM.2'  1 'BPMx016'
    17 [ 3  3] 'ANS-C03/DG/BPM.3'  1 'BPMx017'
    18 [ 3  4] 'ANS-C03/DG/BPM.4'  1 'BPMx018'
    19 [ 3  5] 'ANS-C03/DG/BPM.5'  1 'BPMx019'
    20 [ 3  6] 'ANS-C03/DG/BPM.6'  1 'BPMx020'
    21 [ 3  7] 'ANS-C03/DG/BPM.7'  1 'BPMx021'
    22 [ 3  8] 'ANS-C03/DG/BPM.8'  1 'BPMx022'
    23 [ 4  1] 'ANS-C04/DG/BPM.1'  1 'BPMx023'
    24 [ 4  2] 'ANS-C04/DG/BPM.2'  1 'BPMx024'
    25 [ 4  3] 'ANS-C04/DG/BPM.3'  1 'BPMx025'
    26 [ 4  4] 'ANS-C04/DG/BPM.4'  1 'BPMx026'
    27 [ 4  5] 'ANS-C04/DG/BPM.5'  1 'BPMx027'
    28 [ 4  6] 'ANS-C04/DG/BPM.6'  1 'BPMx028'
    29 [ 4  7] 'ANS-C04/DG/BPM.7'  1 'BPMx029'
    30 [ 5  1] 'ANS-C05/DG/BPM.1'  1 'BPMx030'
    31 [ 5  2] 'ANS-C05/DG/BPM.2'  1 'BPMx031'
    32 [ 5  3] 'ANS-C05/DG/BPM.3'  1 'BPMx032'
    33 [ 5  4] 'ANS-C05/DG/BPM.4'  1 'BPMx033'
    34 [ 5  5] 'ANS-C05/DG/BPM.5'  1 'BPMx034'
    35 [ 5  6] 'ANS-C05/DG/BPM.6'  1 'BPMx035'
    36 [ 5  7] 'ANS-C05/DG/BPM.7'  1 'BPMx036'
    37 [ 6  1] 'ANS-C06/DG/BPM.1'  1 'BPMx037'
    38 [ 6  2] 'ANS-C06/DG/BPM.2'  1 'BPMx038'
    39 [ 6  3] 'ANS-C06/DG/BPM.3'  1 'BPMx039'
    40 [ 6  4] 'ANS-C06/DG/BPM.4'  1 'BPMx040'
    41 [ 6  5] 'ANS-C06/DG/BPM.5'  1 'BPMx041'
    42 [ 6  6] 'ANS-C06/DG/BPM.6'  1 'BPMx042'
    43 [ 6  7] 'ANS-C06/DG/BPM.7'  1 'BPMx043'
    44 [ 6  8] 'ANS-C06/DG/BPM.8'  1 'BPMx044'
    45 [ 7  1] 'ANS-C07/DG/BPM.1'  1 'BPMx045'
    46 [ 7  2] 'ANS-C07/DG/BPM.2'  1 'BPMx046'
    47 [ 7  3] 'ANS-C07/DG/BPM.3'  1 'BPMx047'
    48 [ 7  4] 'ANS-C07/DG/BPM.4'  1 'BPMx048'
    49 [ 7  5] 'ANS-C07/DG/BPM.5'  1 'BPMx049'
    50 [ 7  6] 'ANS-C07/DG/BPM.6'  1 'BPMx050'
    51 [ 7  7] 'ANS-C07/DG/BPM.7'  1 'BPMx051'
    52 [ 7  8] 'ANS-C07/DG/BPM.8'  1 'BPMx052'
    53 [ 8  1] 'ANS-C08/DG/BPM.1'  1 'BPMx053'
    54 [ 8  2] 'ANS-C08/DG/BPM.2'  1 'BPMx054'
    55 [ 8  3] 'ANS-C08/DG/BPM.3'  1 'BPMx055'
    56 [ 8  4] 'ANS-C08/DG/BPM.4'  1 'BPMx056'
    57 [ 8  5] 'ANS-C08/DG/BPM.5'  1 'BPMx057'
    58 [ 8  6] 'ANS-C08/DG/BPM.6'  1 'BPMx058'
    59 [ 8  7] 'ANS-C08/DG/BPM.7'  1 'BPMx059'
    60 [ 9  1] 'ANS-C09/DG/BPM.1'  1 'BPMx060'
    61 [ 9  2] 'ANS-C09/DG/BPM.2'  1 'BPMx061'
    62 [ 9  3] 'ANS-C09/DG/BPM.3'  1 'BPMx062'
    63 [ 9  4] 'ANS-C09/DG/BPM.4'  1 'BPMx063'
    64 [ 9  5] 'ANS-C09/DG/BPM.5'  1 'BPMx064'
    65 [ 9  6] 'ANS-C09/DG/BPM.6'  1 'BPMx065'
    66 [ 9  7] 'ANS-C09/DG/BPM.7'  1 'BPMx066'
    67 [10  1] 'ANS-C10/DG/BPM.1'  1 'BPMx067'
    68 [10  2] 'ANS-C10/DG/BPM.2'  1 'BPMx068'
    69 [10  3] 'ANS-C10/DG/BPM.3'  1 'BPMx069'
    70 [10  4] 'ANS-C10/DG/BPM.4'  1 'BPMx070'
    71 [10  5] 'ANS-C10/DG/BPM.5'  1 'BPMx071'
    72 [10  6] 'ANS-C10/DG/BPM.6'  1 'BPMx072'
    73 [10  7] 'ANS-C10/DG/BPM.7'  1 'BPMx073'
    74 [10  8] 'ANS-C10/DG/BPM.8'  1 'BPMx074'
    75 [11  1] 'ANS-C11/DG/BPM.1'  1 'BPMx075'
    76 [11  2] 'ANS-C11/DG/BPM.2'  1 'BPMx076'
    77 [11  3] 'ANS-C11/DG/BPM.3'  1 'BPMx077'
    78 [11  4] 'ANS-C11/DG/BPM.4'  1 'BPMx078'
    79 [11  5] 'ANS-C11/DG/BPM.5'  1 'BPMx079'
    80 [11  6] 'ANS-C11/DG/BPM.6'  1 'BPMx080'
    81 [11  7] 'ANS-C11/DG/BPM.7'  1 'BPMx081'
    82 [11  8] 'ANS-C11/DG/BPM.8'  1 'BPMx082'
    83 [12  1] 'ANS-C12/DG/BPM.1'  1 'BPMx083'
    84 [12  2] 'ANS-C12/DG/BPM.2'  1 'BPMx084'
    85 [12  3] 'ANS-C12/DG/BPM.3'  1 'BPMx085'
    86 [12  4] 'ANS-C12/DG/BPM.4'  1 'BPMx086'
    87 [12  5] 'ANS-C12/DG/BPM.5'  1 'BPMx087'
    88 [12  6] 'ANS-C12/DG/BPM.6'  1 'BPMx088'
    89 [12  7] 'ANS-C12/DG/BPM.7'  1 'BPMx089'
    90 [13  1] 'ANS-C13/DG/BPM.1'  1 'BPMx090'
    91 [13  2] 'ANS-C13/DG/BPM.2'  1 'BPMx091'
    92 [13  3] 'ANS-C13/DG/BPM.3'  1 'BPMx092'
    93 [13  4] 'ANS-C13/DG/BPM.4'  1 'BPMx093'
    94 [13  5] 'ANS-C13/DG/BPM.5'  1 'BPMx094'
    95 [13  6] 'ANS-C13/DG/BPM.6'  1 'BPMx095'
    96 [13  7] 'ANS-C13/DG/BPM.7'  1 'BPMx096'
    97 [14  1] 'ANS-C14/DG/BPM.1'  1 'BPMx097'
    98 [14  2] 'ANS-C14/DG/BPM.2'  1 'BPMx098'
    99 [14  3] 'ANS-C14/DG/BPM.3'  1 'BPMx099'
    100 [14  4] 'ANS-C14/DG/BPM.4'  1 'BPMx100'
    101 [14  5] 'ANS-C14/DG/BPM.5'  1 'BPMx101'
    102 [14  6] 'ANS-C14/DG/BPM.6'  1 'BPMx102'
    103 [14  7] 'ANS-C14/DG/BPM.7'  1 'BPMx103'
    104 [14  8] 'ANS-C14/DG/BPM.8'  1 'BPMx104'
    105 [15  1] 'ANS-C15/DG/BPM.1'  1 'BPMx105'
    106 [15  2] 'ANS-C15/DG/BPM.2'  1 'BPMx106'
    107 [15  3] 'ANS-C15/DG/BPM.3'  1 'BPMx107'
    108 [15  4] 'ANS-C15/DG/BPM.4'  1 'BPMx108'
    109 [15  5] 'ANS-C15/DG/BPM.5'  1 'BPMx109'
    110 [15  6] 'ANS-C15/DG/BPM.6'  1 'BPMx110'
    111 [15  7] 'ANS-C15/DG/BPM.7'  1 'BPMx111'
    112 [15  8] 'ANS-C15/DG/BPM.8'  1 'BPMx112'
    113 [16  1] 'ANS-C16/DG/BPM.1'  1 'BPMx113'
    114 [16  2] 'ANS-C16/DG/BPM.2'  1 'BPMx114'
    115 [16  3] 'ANS-C16/DG/BPM.3'  1 'BPMx115'
    116 [16  4] 'ANS-C16/DG/BPM.4'  1 'BPMx116'
    117 [16  5] 'ANS-C16/DG/BPM.5'  1 'BPMx117'
    118 [16  6] 'ANS-C16/DG/BPM.6'  1 'BPMx118'
    119 [16  7] 'ANS-C16/DG/BPM.7'  1 'BPMx119'
    120 [ 1  1] 'ANS-C01/DG/BPM.1'  1 'BPMx120'
    };

devnumber = length(varlist);

% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Monitor.HW2PhysicsParams(:,:) = 1e-3*ones(devnumber,1);
AO.(ifam).Monitor.Physics2HWParams(:,:) = 1e3*ones(devnumber,1);
AO.(ifam).Monitor.Handles(:,1)          = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType              = 'Scalar';

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames{k}  = strcat(AO.(ifam).DeviceName{k}, '/XPosSA');
end

% Group
if ControlRoomFlag
        AO.(ifam).GroupId = tango_group_create2('BPMx');
        tango_group_add(AO.(ifam).GroupId,AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = NaN;
end

AO.(ifam).History = AO.(ifam).Monitor;
AO.(ifam).History = rmfield(AO.(ifam).History,'SpecialFunctionGet');

dev = AO.(ifam).DeviceName;
AO.(ifam).History.TangoNames(:,:)       = strcat(dev, '/XPosSAHistory');
AO.(ifam).History.MemberOf = {'Plotfamily'};

AO.(ifam).Va = AO.(ifam).History;
AO.(ifam).Va.TangoNames(:,:)       = strcat(dev, '/VaSA');
AO.(ifam).Va.MemberOf = {'Plotfamily'};

AO.(ifam).Vb = AO.(ifam).History;
AO.(ifam).Vb.TangoNames(:,:)       = strcat(dev, '/VbSA');
AO.(ifam).Vb.MemberOf = {'Plotfamily'};

AO.(ifam).Vc = AO.(ifam).History;
AO.(ifam).Vc.TangoNames(:,:)       = strcat(dev, '/VcSA');
AO.(ifam).Vc.MemberOf = {'Plotfamily'};

AO.(ifam).Vd = AO.(ifam).History;
AO.(ifam).Vd.TangoNames(:,:)       = strcat(dev, '/VdSA');
AO.(ifam).Vd.MemberOf = {'Plotfamily'};

AO.(ifam).Sum = AO.(ifam).History;
AO.(ifam).Sum.TangoNames(:,:)       = strcat(dev, '/SumSA');
AO.(ifam).Sum.MemberOf = {'Plotfamily'};

AO.(ifam).Quad = AO.(ifam).History;
AO.(ifam).Quad.TangoNames(:,:)       = strcat(dev, '/QuadSA');
AO.(ifam).Quad.MemberOf = {'Plotfamily'};

AO.(ifam).Gaidevnumberpm = AO.(ifam).History;
AO.(ifam).Gaidevnumberpm.TangoNames(:,:)       = strcat(dev, '/Gain');
AO.(ifam).Gaidevnumberpm.MemberOf = {'Plotfamily'};

AO.(ifam).Switch = AO.(ifam).History;
AO.(ifam).Switch.TangoNames(:,:)       = strcat(dev, '/Switches');
AO.(ifam).Switch.MemberOf = {'Plotfamily'};

%% BPMz Vertical plane
ifam = 'BPMz';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'BPM';
AO.(ifam).MemberOf                 = {'BPM'; 'VBPM'; 'PlotFamily'; 'Archivable'};
AO.(ifam).Monitor.Mode             = Mode;
%AO.(ifam).Monitor.Mode             = 'Special';
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'mm';
AO.(ifam).Monitor.PhysicsUnits     = 'm';
AO.(ifam).Monitor.SpecialFunctionGet = 'getvbpmgroup';
%AO.(ifam).Monitor.SpecialFunctionGet = 'getvbpmaverage';
%AO.(ifam).Monitor.SpecialFunctionGet = 'getvturdevnumberyturn';

% devliste tangoname status common
varlist = {
    1 [ 1  2] 'ANS-C01/DG/BPM.2'  1 'BPMz001'
    2 [ 1  3] 'ANS-C01/DG/BPM.3'  1 'BPMz002'
    3 [ 1  4] 'ANS-C01/DG/BPM.4'  1 'BPMz003'
    4 [ 1  5] 'ANS-C01/DG/BPM.5'  1 'BPMz004'
    5 [ 1  6] 'ANS-C01/DG/BPM.6'  1 'BPMz005'
    6 [ 1  7] 'ANS-C01/DG/BPM.7'  1 'BPMz006'
    7 [ 2  1] 'ANS-C02/DG/BPM.1'  1 'BPMz007'
    8 [ 2  2] 'ANS-C02/DG/BPM.2'  1 'BPMz008'
    9 [ 2  3] 'ANS-C02/DG/BPM.3'  1 'BPMz009'
    10 [ 2  4] 'ANS-C02/DG/BPM.4'  1 'BPMz010'
    11 [ 2  5] 'ANS-C02/DG/BPM.5'  1 'BPMz011'
    12 [ 2  6] 'ANS-C02/DG/BPM.6'  1 'BPMz012'
    13 [ 2  7] 'ANS-C02/DG/BPM.7'  1 'BPMz013'
    14 [ 2  8] 'ANS-C02/DG/BPM.8'  1 'BPMz014'
    15 [ 3  1] 'ANS-C03/DG/BPM.1'  1 'BPMz015'
    16 [ 3  2] 'ANS-C03/DG/BPM.2'  1 'BPMz016'
    17 [ 3  3] 'ANS-C03/DG/BPM.3'  1 'BPMz017'
    18 [ 3  4] 'ANS-C03/DG/BPM.4'  1 'BPMz018'
    19 [ 3  5] 'ANS-C03/DG/BPM.5'  1 'BPMz019'
    20 [ 3  6] 'ANS-C03/DG/BPM.6'  1 'BPMz020'
    21 [ 3  7] 'ANS-C03/DG/BPM.7'  1 'BPMz021'
    22 [ 3  8] 'ANS-C03/DG/BPM.8'  1 'BPMz022'
    23 [ 4  1] 'ANS-C04/DG/BPM.1'  1 'BPMz023'
    24 [ 4  2] 'ANS-C04/DG/BPM.2'  1 'BPMz024'
    25 [ 4  3] 'ANS-C04/DG/BPM.3'  1 'BPMz025'
    26 [ 4  4] 'ANS-C04/DG/BPM.4'  1 'BPMz026'
    27 [ 4  5] 'ANS-C04/DG/BPM.5'  1 'BPMz027'
    28 [ 4  6] 'ANS-C04/DG/BPM.6'  1 'BPMz028'
    29 [ 4  7] 'ANS-C04/DG/BPM.7'  1 'BPMz029'
    30 [ 5  1] 'ANS-C05/DG/BPM.1'  1 'BPMz030'
    31 [ 5  2] 'ANS-C05/DG/BPM.2'  1 'BPMz031'
    32 [ 5  3] 'ANS-C05/DG/BPM.3'  1 'BPMz032'
    33 [ 5  4] 'ANS-C05/DG/BPM.4'  1 'BPMz033'
    34 [ 5  5] 'ANS-C05/DG/BPM.5'  1 'BPMz034'
    35 [ 5  6] 'ANS-C05/DG/BPM.6'  1 'BPMz035'
    36 [ 5  7] 'ANS-C05/DG/BPM.7'  1 'BPMz036'
    37 [ 6  1] 'ANS-C06/DG/BPM.1'  1 'BPMz037'
    38 [ 6  2] 'ANS-C06/DG/BPM.2'  1 'BPMz038'
    39 [ 6  3] 'ANS-C06/DG/BPM.3'  1 'BPMz039'
    40 [ 6  4] 'ANS-C06/DG/BPM.4'  1 'BPMz040'
    41 [ 6  5] 'ANS-C06/DG/BPM.5'  1 'BPMz041'
    42 [ 6  6] 'ANS-C06/DG/BPM.6'  1 'BPMz042'
    43 [ 6  7] 'ANS-C06/DG/BPM.7'  1 'BPMz043'
    44 [ 6  8] 'ANS-C06/DG/BPM.8'  1 'BPMz044'
    45 [ 7  1] 'ANS-C07/DG/BPM.1'  1 'BPMz045'
    46 [ 7  2] 'ANS-C07/DG/BPM.2'  1 'BPMz046'
    47 [ 7  3] 'ANS-C07/DG/BPM.3'  1 'BPMz047'
    48 [ 7  4] 'ANS-C07/DG/BPM.4'  1 'BPMz048'
    49 [ 7  5] 'ANS-C07/DG/BPM.5'  1 'BPMz049'
    50 [ 7  6] 'ANS-C07/DG/BPM.6'  1 'BPMz050'
    51 [ 7  7] 'ANS-C07/DG/BPM.7'  1 'BPMz051'
    52 [ 7  8] 'ANS-C07/DG/BPM.8'  1 'BPMz052'
    53 [ 8  1] 'ANS-C08/DG/BPM.1'  1 'BPMz053'
    54 [ 8  2] 'ANS-C08/DG/BPM.2'  1 'BPMz054'
    55 [ 8  3] 'ANS-C08/DG/BPM.3'  1 'BPMz055'
    56 [ 8  4] 'ANS-C08/DG/BPM.4'  1 'BPMz056'
    57 [ 8  5] 'ANS-C08/DG/BPM.5'  1 'BPMz057'
    58 [ 8  6] 'ANS-C08/DG/BPM.6'  1 'BPMz058'
    59 [ 8  7] 'ANS-C08/DG/BPM.7'  1 'BPMz059'
    60 [ 9  1] 'ANS-C09/DG/BPM.1'  1 'BPMz060'
    61 [ 9  2] 'ANS-C09/DG/BPM.2'  1 'BPMz061'
    62 [ 9  3] 'ANS-C09/DG/BPM.3'  1 'BPMz062'
    63 [ 9  4] 'ANS-C09/DG/BPM.4'  1 'BPMz063'
    64 [ 9  5] 'ANS-C09/DG/BPM.5'  1 'BPMz064'
    65 [ 9  6] 'ANS-C09/DG/BPM.6'  1 'BPMz065'
    66 [ 9  7] 'ANS-C09/DG/BPM.7'  1 'BPMz066'
    67 [10  1] 'ANS-C10/DG/BPM.1'  1 'BPMz067'
    68 [10  2] 'ANS-C10/DG/BPM.2'  1 'BPMz068'
    69 [10  3] 'ANS-C10/DG/BPM.3'  1 'BPMz069'
    70 [10  4] 'ANS-C10/DG/BPM.4'  1 'BPMz070'
    71 [10  5] 'ANS-C10/DG/BPM.5'  1 'BPMz071'
    72 [10  6] 'ANS-C10/DG/BPM.6'  1 'BPMz072'
    73 [10  7] 'ANS-C10/DG/BPM.7'  1 'BPMz073'
    74 [10  8] 'ANS-C10/DG/BPM.8'  1 'BPMz074'
    75 [11  1] 'ANS-C11/DG/BPM.1'  1 'BPMz075'
    76 [11  2] 'ANS-C11/DG/BPM.2'  1 'BPMz076'
    77 [11  3] 'ANS-C11/DG/BPM.3'  1 'BPMz077'
    78 [11  4] 'ANS-C11/DG/BPM.4'  1 'BPMz078'
    79 [11  5] 'ANS-C11/DG/BPM.5'  1 'BPMz079'
    80 [11  6] 'ANS-C11/DG/BPM.6'  1 'BPMz080'
    81 [11  7] 'ANS-C11/DG/BPM.7'  1 'BPMz081'
    82 [11  8] 'ANS-C11/DG/BPM.8'  1 'BPMz082'
    83 [12  1] 'ANS-C12/DG/BPM.1'  1 'BPMz083'
    84 [12  2] 'ANS-C12/DG/BPM.2'  1 'BPMz084'
    85 [12  3] 'ANS-C12/DG/BPM.3'  1 'BPMz085'
    86 [12  4] 'ANS-C12/DG/BPM.4'  1 'BPMz086'
    87 [12  5] 'ANS-C12/DG/BPM.5'  1 'BPMz087'
    88 [12  6] 'ANS-C12/DG/BPM.6'  1 'BPMz088'
    89 [12  7] 'ANS-C12/DG/BPM.7'  1 'BPMz089'
    90 [13  1] 'ANS-C13/DG/BPM.1'  1 'BPMz090'
    91 [13  2] 'ANS-C13/DG/BPM.2'  1 'BPMz091'
    92 [13  3] 'ANS-C13/DG/BPM.3'  1 'BPMz092'
    93 [13  4] 'ANS-C13/DG/BPM.4'  1 'BPMz093'
    94 [13  5] 'ANS-C13/DG/BPM.5'  1 'BPMz094'
    95 [13  6] 'ANS-C13/DG/BPM.6'  1 'BPMz095'
    96 [13  7] 'ANS-C13/DG/BPM.7'  1 'BPMz096'
    97 [14  1] 'ANS-C14/DG/BPM.1'  1 'BPMz097'
    98 [14  2] 'ANS-C14/DG/BPM.2'  1 'BPMz098'
    99 [14  3] 'ANS-C14/DG/BPM.3'  1 'BPMz099'
    100 [14  4] 'ANS-C14/DG/BPM.4'  1 'BPMz100'
    101 [14  5] 'ANS-C14/DG/BPM.5'  1 'BPMz101'
    102 [14  6] 'ANS-C14/DG/BPM.6'  1 'BPMz102'
    103 [14  7] 'ANS-C14/DG/BPM.7'  1 'BPMz103'
    104 [14  8] 'ANS-C14/DG/BPM.8'  1 'BPMz104'
    105 [15  1] 'ANS-C15/DG/BPM.1'  1 'BPMz105'
    106 [15  2] 'ANS-C15/DG/BPM.2'  1 'BPMz106'
    107 [15  3] 'ANS-C15/DG/BPM.3'  1 'BPMz107'
    108 [15  4] 'ANS-C15/DG/BPM.4'  1 'BPMz108'
    109 [15  5] 'ANS-C15/DG/BPM.5'  1 'BPMz109'
    110 [15  6] 'ANS-C15/DG/BPM.6'  1 'BPMz110'
    111 [15  7] 'ANS-C15/DG/BPM.7'  1 'BPMz111'
    112 [15  8] 'ANS-C15/DG/BPM.8'  1 'BPMz112'
    113 [16  1] 'ANS-C16/DG/BPM.1'  1 'BPMz113'
    114 [16  2] 'ANS-C16/DG/BPM.2'  1 'BPMz114'
    115 [16  3] 'ANS-C16/DG/BPM.3'  1 'BPMz115'
    116 [16  4] 'ANS-C16/DG/BPM.4'  1 'BPMz116'
    117 [16  5] 'ANS-C16/DG/BPM.5'  1 'BPMz117'
    118 [16  6] 'ANS-C16/DG/BPM.6'  1 'BPMz118'
    119 [16  7] 'ANS-C16/DG/BPM.7'  1 'BPMz119'
    120 [ 1  1] 'ANS-C01/DG/BPM.1'  1 'BPMz120'
    };

devnumber = length(varlist);

% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Monitor.HW2PhysicsParams(:,:) = 1e-3*ones(devnumber,1);
AO.(ifam).Monitor.Physics2HWParams(:,:) = 1e3*ones(devnumber,1);
AO.(ifam).Monitor.Handles(:,1)          = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType              = 'Scalar';

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames{k}  = strcat(AO.(ifam).DeviceName{k}, '/ZPosSA');
end

% Group
if ControlRoomFlag
        AO.(ifam).GroupId = tango_group_create2('BPMz');
        tango_group_add(AO.(ifam).GroupId,AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = NaN;
end

AO.(ifam).History = AO.(ifam).Monitor;
AO.(ifam).History = rmfield(AO.(ifam).History,'SpecialFunctionGet');

dev = AO.(ifam).DeviceName;
AO.(ifam).History.TangoNames(:,:)       = strcat(dev, '/ZPosSAHistory');
AO.(ifam).History.MemberOf = {'Plotfamily'};

AO.(ifam).Va = AO.(ifam).History;
AO.(ifam).Va.TangoNames(:,:)       = strcat(dev, '/VaSA');
AO.(ifam).Va.MemberOf = {'Plotfamily'};

AO.(ifam).Vb = AO.(ifam).History;
AO.(ifam).Vb.TangoNames(:,:)       = strcat(dev, '/VbSA');
AO.(ifam).Vb.MemberOf = {'Plotfamily'};

AO.(ifam).Vc = AO.(ifam).History;
AO.(ifam).Vc.TangoNames(:,:)       = strcat(dev, '/VcSA');
AO.(ifam).Vc.MemberOf = {'Plotfamily'};

AO.(ifam).Vd = AO.(ifam).History;
AO.(ifam).Vd.TangoNames(:,:)       = strcat(dev, '/VdSA');
AO.(ifam).Vd.MemberOf = {'Plotfamily'};

AO.(ifam).Sum = AO.(ifam).History;
AO.(ifam).Sum.TangoNames(:,:)       = strcat(dev, '/SumSA');
AO.(ifam).Sum.MemberOf = {'Plotfamily'};

AO.(ifam).Quad = AO.(ifam).History;
AO.(ifam).Quad.TangoNames(:,:)       = strcat(dev, '/QuadSA');
AO.(ifam).Quad.MemberOf = {'Plotfamily'};

AO.(ifam).Gaidevnumberpm = AO.(ifam).History;
AO.(ifam).Gaidevnumberpm.TangoNames(:,:)       = strcat(dev, '/Gain');
AO.(ifam).Gaidevnumberpm.MemberOf = {'Plotfamily'};

AO.(ifam).Switch = AO.(ifam).History;
AO.(ifam).Switch.TangoNames(:,:)       = strcat(dev, '/Switches');
AO.(ifam).Switch.MemberOf = {'Plotfamily'};

%===========================================================
% Corrector data: status field designates if corrector in use
%===========================================================

%% BLx : Source points
ifam = 'BeamLine';

AO.(ifam).FamilyName  = ifam;
AO.(ifam).FamilyType  = 'Diagnostic';
AO.(ifam).MemberOf    = {'DG'; 'PlotFamily'; 'Archivable'};

% D2 en C08: ASTROPHYSICS
% AO.(ifam).CommonNames = {'INJECTION';'ODE'; 'CRYOMODULE2'; 'SMIS'; 'SEXTANTS'; ...
%     'CRYOMODULE1'; 'AILES'; 'PSICHE'; 'PLEIADES'; 'DISCO'; ...
%     'DESIRS'; 'METRO'; 'SDM06'; 'CRISTAL'; 'DEIMOS'; 'GALAXIES'; 'TEMPO'; ...
%     'SDL09'; 'SAMBA'; 'MICROXM'; 'PX1'; 'PX2'; 'SWING'; ...
%     'ANTARES'; 'SDL13'; 'DIFFABS'; 'MICROFOC'; 'SIXS'; 'CASSIOPEE'; 'SIRIUS'; 'LUCIA'}; % to be completed with new dipoles
% AO.(ifam).DeviceList  = [1 1; 1 2; 2 1; 2 2; 2 3; 3 1; 3 2; 3 3; 4 1; 4 2; ...
%     5 1; 5 2; 6 1; 6 2; 7 1; 7 2; 8 1; ...
%     9 1; 9 2; 10 1; 10 2; 11 1; 11 2; 12 1; ...
%     13 1; 13 2;14 1; 14 2; 15 1; 15 2; 16 1];

% AO.(ifam).DeviceName             = { ...
%     'ANS-C01/DG/CALC-SDL-POSITION-ANGLE'; ...
%     'ANS-C01/DG/CALC-D2-POSITION-ANGLE'; ...
%     'ANS-C02/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C02/DG/CALC-D1-POSITION-ANGLE'; ...
%     'ANS-C02/DG/CALC-SDC-POSITION-ANGLE'; ...
%     'ANS-C03/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C03/DG/CALC-D1-POSITION-ANGLE'; ...
%     'ANS-C03/DG/CALC-SDC-POSITION-ANGLE'; ...
%     'ANS-C04/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C04/DG/CALC-D2-POSITION-ANGLE'; ...
%     'ANS-C05/DG/CALC-SDL-POSITION-ANGLE'; ...
%     'ANS-C05/DG/CALC-D2-POSITION-ANGLE'; ...
%     'ANS-C06/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C06/DG/CALC-SDC-POSITION-ANGLE'; ...
%     'ANS-C07/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C07/DG/CALC-SDC-POSITION-ANGLE'; ...
%     'ANS-C08/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C09/DG/CALC-SDL-POSITION-ANGLE'; ...
%     'ANS-C09/DG/CALC-D2-POSITION-ANGLE'; ...
%     'ANS-C10/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C10/DG/CALC-SDC-POSITION-ANGLE'; ...
%     'ANS-C11/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C11/DG/CALC-SDC-POSITION-ANGLE'; ...
%     'ANS-C12/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C13/DG/CALC-SDL-POSITION-ANGLE'; ...
%     'ANS-C13/DG/CALC-D2-POSITION-ANGLE' ; ...
%     'ANS-C14/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C14/DG/CALC-SDC-POSITION-ANGLE'; ...
%     'ANS-C15/DG/CALC-SDM-POSITION-ANGLE'; ...
%     'ANS-C15/DG/CALC-SDC-POSITION-ANGLE'; ...
%     'ANS-C16/DG/CALC-SDM-POSITION-ANGLE'};

% AO.(ifam).ElementList = (1:size(AO.(ifam).DeviceList,1))';
% AO.(ifam).Status      = ones(size(AO.(ifam).DeviceList,1),1);


% ID based BLs
%spos = getspos('BPMx',getidbpmlist)
%spos2 = zeros(24,1);
%spos2(2:end) = (spos(3:2:end)+spos(2:2:end-1))/2;
% ODE, SAMBA, DIFFABS, AILES, SMIS
% dippos = getspos('BEND',[1 1])
% Position = [spos2(1); dippos(2); spos2(2); dippos(3); spos2(3:4); dippos(5); spos2(5:6); dippos(8); spos2(7);  dippos(10); spos2(8:13); dippos(18); spos2(14:19); dippos(26); spos2(20:end)];
% AO.(ifam).Position = [
%     0
%     14.7752
%     22.0301
%     28.2320
%     33.1454
%     44.2619
%     50.4638
%     55.3772
%     66.4936
%     78.2007
%     88.5230
%     103.2981
%     110.5530
%     121.6689
%     132.7853
%     143.9012
%     155.0177
%     177.0476
%     191.8227
%     199.0776
%     210.1934
%     221.3099
%     232.4258
%     243.5422
%     265.5721
%     280.3473
%     287.6022
%     298.7180
%     309.8345
%     320.9503
%     332.0668];

varlist = {
  1 [ 1  1] 'ANS-C01/DG/CALC-SDL-POSITION-ANGLE '  1 'INJECTION       ' 0.000000e+00
  2 [ 1  2] 'ANS-C01/DG/CALC-D2-POSITION-ANGLE  '  1 'ODE             ' 1.477520e+01
  3 [ 2  1] 'ANS-C02/DG/CALC-SDM-POSITION-ANGLE '  1 'CRYOMODULE2     ' 2.203010e+01
  4 [ 2  2] 'ANS-C02/DG/CALC-D1-POSITION-ANGLE  '  1 'SMIS            ' 2.823200e+01
  5 [ 2  3] 'ANS-C02/DG/CALC-SDC-POSITION-ANGLE '  1 'MICROSCO        ' 3.314540e+01
  6 [ 3  1] 'ANS-C03/DG/CALC-SDM-POSITION-ANGLE '  1 'CRYOMODULE1     ' 4.426190e+01
  7 [ 3  2] 'ANS-C03/DG/CALC-D1-POSITION-ANGLE  '  1 'AILES           ' 5.046380e+01
  8 [ 3  3] 'ANS-C03/DG/CALC-SDC-POSITION-ANGLE '  1 'PSICHE          ' 5.537720e+01
  9 [ 4  1] 'ANS-C04/DG/CALC-SDM-POSITION-ANGLE '  1 'PLEIADES        ' 6.649360e+01
 10 [ 4  2] 'ANS-C04/DG/CALC-D2-POSITION-ANGLE  '  1 'DISCO           ' 7.820070e+01
 11 [ 5  1] 'ANS-C05/DG/CALC-SDL-POSITION-ANGLE '  1 'DESIRS          ' 8.852300e+01
 12 [ 5  2] 'ANS-C05/DG/CALC-D2-POSITION-ANGLE  '  1 'METRO           ' 1.032981e+02
 13 [ 6  1] 'ANS-C06/DG/CALC-SDM-POSITION-ANGLE '  1 'SDM06           ' 1.105530e+02
 14 [ 6  2] 'ANS-C06/DG/CALC-SDC-POSITION-ANGLE '  1 'CRISTAL         ' 1.216689e+02
 15 [ 7  1] 'ANS-C07/DG/CALC-SDM-POSITION-ANGLE '  1 'DEIMOS          ' 1.327853e+02
 16 [ 7  2] 'ANS-C07/DG/CALC-SDC-POSITION-ANGLE '  1 'GALAXIES        ' 1.439012e+02
 17 [ 8  1] 'ANS-C08/DG/CALC-SDM-POSITION-ANGLE '  1 'TEMPO           ' 1.550177e+02
 18 [ 9  1] 'ANS-C09/DG/CALC-SDL-POSITION-ANGLE '  1 'SDL09           ' 1.770476e+02
 19 [ 9  2] 'ANS-C09/DG/CALC-D2-POSITION-ANGLE  '  1 'SAMBA           ' 1.918227e+02
 20 [10  1] 'ANS-C10/DG/CALC-SDM-POSITION-ANGLE '  1 'MICROXM         ' 1.990776e+02
 21 [10  2] 'ANS-C10/DG/CALC-SDC-POSITION-ANGLE '  1 'PX1             ' 2.101934e+02
 22 [11  1] 'ANS-C11/DG/CALC-SDM-POSITION-ANGLE '  1 'PX2             ' 2.213099e+02
 23 [11  2] 'ANS-C11/DG/CALC-SDC-POSITION-ANGLE '  1 'SWING           ' 2.324258e+02
 24 [12  1] 'ANS-C12/DG/CALC-SDM-POSITION-ANGLE '  1 'ANTARES         ' 2.435422e+02
 25 [13  1] 'ANS-C13/DG/CALC-SDL-POSITION-ANGLE '  1 'SDL13           ' 2.655721e+02
 26 [13  2] 'ANS-C13/DG/CALC-D2-POSITION-ANGLE  '  1 'DIFFABS         ' 2.803473e+02
 27 [14  1] 'ANS-C14/DG/CALC-SDM-POSITION-ANGLE '  1 'MICROFOC        ' 2.876022e+02
 28 [14  2] 'ANS-C14/DG/CALC-SDC-POSITION-ANGLE '  1 'SIXS            ' 2.987180e+02
 29 [15  1] 'ANS-C15/DG/CALC-SDM-POSITION-ANGLE '  1 'CASSIOPEE       ' 3.098345e+02
 30 [15  2] 'ANS-C15/DG/CALC-SDC-POSITION-ANGLE '  1 'SIRIUS          ' 3.209503e+02
 31 [16  1] 'ANS-C16/DG/CALC-SDM-POSITION-ANGLE '  1 'LUCIA           ' 3.320668e+02
};

devnumber = length(varlist);
% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Position(k,:)      = varlist{k,6};
end

AO.(ifam).Monitor.Mode                   = Mode; %'Simulator';  % Mode;
AO.(ifam).Monitor.DataType               = 'Scalar';
AO.(ifam).Monitor.TangoNames(:,:)        = strcat(AO.(ifam).DeviceName, '/positionX');

AO.(ifam).Monitor.Units                  = 'Hardware';
AO.(ifam).Monitor.Handles                = NaN;
AO.(ifam).Monitor.HW2PhysicsParams       = 1e-6;
AO.(ifam).Monitor.Physics2HWParams       = 1e6;
AO.(ifam).Monitor.HWUnits                = 'um';
AO.(ifam).Monitor.PhysicsUnits           = 'm';


AO.(ifam).Positionx = AO.(ifam).Monitor;
AO.(ifam).Positionx.MemberOf            = {'PlotFamily'};

AO.(ifam).Anglex = AO.(ifam).Positionx;

AO.(ifam).Anglex.HWUnits                = 'urad';
AO.(ifam).Anglex.PhysicsUnits           = 'rad';
AO.(ifam).Anglex.TangoNames(:,:)        = strcat(AO.(ifam).DeviceName, '/angleX');

AO.(ifam).Positionz = AO.(ifam).Positionx;
AO.(ifam).Positionz.TangoNames(:,:)     = strcat(AO.(ifam).DeviceName, '/positionZ');

AO.(ifam).Anglez = AO.(ifam).Anglex;
AO.(ifam).Anglez.TangoNames(:,:)        = strcat(AO.(ifam).DeviceName, '/angleZ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SLOW HORIZONTAL CORRECTORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ifam = 'HCOR';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'COR';
AO.(ifam).MemberOf                 = {'MachineConfig'; 'HCOR'; 'COR'; 'Magnet'; 'PlotFamily'; 'Archivable'};

AO.(ifam).Monitor.Mode             = Mode;
AO.(ifam).Monitor.DataType         = 'Scalar';
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'A';
AO.(ifam).Monitor.PhysicsUnits     = 'rad';
AO.(ifam).Monitor.HW2PhysicsFcn    = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn    = @k2amp;

AO.(ifam).Setpoint.Mode             = Mode;
AO.(ifam).Setpoint.DataType         = 'Scalar';
AO.(ifam).Setpoint.Units            = 'Hardware';
AO.(ifam).Setpoint.HWUnits          = 'A';
AO.(ifam).Setpoint.PhysicsUnits     = 'rad';
AO.(ifam).Setpoint.HW2PhysicsFcn    = @amp2k;
AO.(ifam).Setpoint.Physics2HWFcn    = @k2amp;

AO.(ifam).Voltage.Mode             = Mode;
AO.(ifam).Voltage.DataType         = 'Scalar';
AO.(ifam).Voltage.Units            = 'Hardware';
AO.(ifam).Voltage.HWUnits          = 'V';
AO.(ifam).Voltage.PhysicsUnits     = 'rad';
AO.(ifam).Voltage.HW2PhysicsFcn    = @amp2k;
AO.(ifam).Voltage.Physics2HWFcn    = @k2amp;

% % Get mapping from TANGO static database
AO.(ifam).Voltage.PhysicsUnits     = 'rad';
AO.(ifam).Voltage.HW2PhysicsFcn    = @amp2k;
AO.(ifam).Voltage.Physics2HWFcn    = @k2amp;

% elemlist devlist tangoname       status  common  attR           attW      range
varlist = {
     1  [ 1 1] 'ANS-C01/AE/S1-CH      ' 1 'HCOR001' 'current   ' 'currentPM ' [-11  11]
     2  [ 1 2] 'ANS-C01/AE/S2-CH      ' 0 'HCOR002' 'current   ' 'currentPM ' [-11  11]
     3  [ 1 3] 'ANS-C01/AE/S3.1-CH    ' 0 'HCOR003' 'current   ' 'currentPM ' [-11  11]
     4  [ 1 4] 'ANS-C01/AE/S4-CH      ' 1 'HCOR004' 'current   ' 'currentPM ' [-11  11]
     5  [ 1 5] 'ANS-C01/AE/S3.2-CH    ' 0 'HCOR005' 'current   ' 'currentPM ' [-11  11]
     6  [ 1 6] 'ANS-C01/AE/S5-CH      ' 0 'HCOR006' 'current   ' 'currentPM ' [-11  11]
     7  [ 1 7] 'ANS-C01/AE/S6-CH      ' 1 'HCOR007' 'current   ' 'currentPM ' [-11  11]
     8  [ 2 1] 'ANS-C02/AE/S8.1-CH    ' 1 'HCOR008' 'current   ' 'currentPM ' [-11  11]
     9  [ 2 2] 'ANS-C02/AE/S7.1-CH    ' 0 'HCOR009' 'current   ' 'currentPM ' [-11  11]
    10  [ 2 3] 'ANS-C02/AE/S9.1-CH    ' 0 'HCOR010' 'current   ' 'currentPM ' [-11  11]
    11  [ 2 4] 'ANS-C02/AE/S10.1-CH   ' 1 'HCOR011' 'current   ' 'currentPM ' [-11  11]
    12  [ 2 5] 'ANS-C02/AE/S10.2-CH   ' 1 'HCOR012' 'current   ' 'currentPM ' [-11  11]
    13  [ 2 6] 'ANS-C02/AE/S9.2-CH    ' 0 'HCOR013' 'current   ' 'currentPM ' [-11  11]
    14  [ 2 7] 'ANS-C02/AE/S7.2-CH    ' 0 'HCOR014' 'current   ' 'currentPM ' [-11  11]
    15  [ 2 8] 'ANS-C02/AE/S8.2-CH    ' 1 'HCOR015' 'current   ' 'currentPM ' [-11  11]
    16  [ 3 1] 'ANS-C03/AE/S8.1-CH    ' 1 'HCOR016' 'current   ' 'currentPM ' [-11  11]
    17  [ 3 2] 'ANS-C03/AE/S7.1-CH    ' 0 'HCOR017' 'current   ' 'currentPM ' [-11  11]
    18  [ 3 3] 'ANS-C03/AE/S9.1-CH    ' 0 'HCOR018' 'current   ' 'currentPM ' [-11  11]
    19  [ 3 4] 'ANS-C03/AE/S10.1-CH   ' 1 'HCOR019' 'current   ' 'currentPM ' [-11  11]
    20  [ 3 5] 'ANS-C03/AE/S10.2-CH   ' 1 'HCOR020' 'current   ' 'currentPM ' [-11  11]
    21  [ 3 6] 'ANS-C03/AE/S9.2-CH    ' 0 'HCOR021' 'current   ' 'currentPM ' [-11  11]
    22  [ 3 7] 'ANS-C03/AE/S7.2-CH    ' 0 'HCOR022' 'current   ' 'currentPM ' [-11  11]
    23  [ 3 8] 'ANS-C03/AE/S8.2-CH    ' 1 'HCOR023' 'current   ' 'currentPM ' [-11  11]
    24  [ 4 1] 'ANS-C04/AE/S6-CH      ' 1 'HCOR024' 'current   ' 'currentPM ' [-11  11]
    25  [ 4 2] 'ANS-C04/AE/S5-CH      ' 0 'HCOR025' 'current   ' 'currentPM ' [-11  11]
    26  [ 4 3] 'ANS-C04/AE/S3.1-CH    ' 0 'HCOR026' 'current   ' 'currentPM ' [-11  11]
    27  [ 4 4] 'ANS-C04/AE/S4-CH      ' 1 'HCOR027' 'current   ' 'currentPM ' [-11  11]
    28  [ 4 5] 'ANS-C04/AE/S3.2-CH    ' 0 'HCOR028' 'current   ' 'currentPM ' [-11  11]
    29  [ 4 6] 'ANS-C04/AE/S2-CH      ' 0 'HCOR029' 'current   ' 'currentPM ' [-11  11]
    30  [ 4 7] 'ANS-C04/AE/S1-CH      ' 1 'HCOR030' 'current   ' 'currentPM ' [-11  11]
    31  [ 5 8] 'ANS-C05/EI/L-HU640    ' 0 'HCOR121' 'currentCHE' 'currentCHE' [ -5   5]
    32  [ 5 9] 'ANS-C05/EI/L-HU640    ' 0 'HCOR122' 'currentCHS' 'currentCHS' [ -5   5]
    33  [ 5 1] 'ANS-C05/AE/S1-CH      ' 1 'HCOR031' 'current   ' 'currentPM ' [-11  11]
    34  [ 5 2] 'ANS-C05/AE/S2-CH      ' 0 'HCOR032' 'current   ' 'currentPM ' [-11  11]
    35  [ 5 3] 'ANS-C05/AE/S3.1-CH    ' 0 'HCOR033' 'current   ' 'currentPM ' [-11  11]
    36  [ 5 4] 'ANS-C05/AE/S4-CH      ' 1 'HCOR034' 'current   ' 'currentPM ' [-11  11]
    37  [ 5 5] 'ANS-C05/AE/S3.2-CH    ' 0 'HCOR035' 'current   ' 'currentPM ' [-11  11]
    38  [ 5 6] 'ANS-C05/AE/S5-CH      ' 0 'HCOR036' 'current   ' 'currentPM ' [-11  11]
    39  [ 5 7] 'ANS-C05/AE/S6-CH      ' 1 'HCOR037' 'current   ' 'currentPM ' [-11  11]
    40  [ 6 1] 'ANS-C06/AE/S8.1-CH    ' 1 'HCOR038' 'current   ' 'currentPM ' [-11  11]
    41  [ 6 2] 'ANS-C06/AE/S7.1-CH    ' 0 'HCOR039' 'current   ' 'currentPM ' [-11  11]
    42  [ 6 3] 'ANS-C06/AE/S9.1-CH    ' 0 'HCOR040' 'current   ' 'currentPM ' [-11  11]
    43  [ 6 4] 'ANS-C06/AE/S10.1-CH   ' 1 'HCOR041' 'current   ' 'currentPM ' [-11  11]
    44  [ 6 5] 'ANS-C06/AE/S10.2-CH   ' 1 'HCOR042' 'current   ' 'currentPM ' [-11  11]
    45  [ 6 6] 'ANS-C06/AE/S9.2-CH    ' 0 'HCOR043' 'current   ' 'currentPM ' [-11  11]
    46  [ 6 7] 'ANS-C06/AE/S7.2-CH    ' 0 'HCOR044' 'current   ' 'currentPM ' [-11  11]
    47  [ 6 8] 'ANS-C06/AE/S8.2-CH    ' 1 'HCOR045' 'current   ' 'currentPM ' [-11  11]
    48  [ 7 1] 'ANS-C07/AE/S8.1-CH    ' 1 'HCOR046' 'current   ' 'currentPM ' [-11  11]
    49  [ 7 2] 'ANS-C07/AE/S7.1-CH    ' 0 'HCOR047' 'current   ' 'currentPM ' [-11  11]
    50  [ 7 3] 'ANS-C07/AE/S9.1-CH    ' 0 'HCOR048' 'current   ' 'currentPM ' [-11  11]
    51  [ 7 4] 'ANS-C07/AE/S10.1-CH   ' 1 'HCOR049' 'current   ' 'currentPM ' [-11  11]
    52  [ 7 5] 'ANS-C07/AE/S10.2-CH   ' 1 'HCOR050' 'current   ' 'currentPM ' [-11  11]
    53  [ 7 6] 'ANS-C07/AE/S9.2-CH    ' 0 'HCOR051' 'current   ' 'currentPM ' [-11  11]
    54  [ 7 7] 'ANS-C07/AE/S7.2-CH    ' 0 'HCOR052' 'current   ' 'currentPM ' [-11  11]
    55  [ 7 8] 'ANS-C07/AE/S8.2-CH    ' 1 'HCOR053' 'current   ' 'currentPM ' [-11  11]
    56  [ 8 1] 'ANS-C08/AE/S6-CH      ' 1 'HCOR054' 'current   ' 'currentPM ' [-11  11]
    57  [ 8 2] 'ANS-C08/AE/S5-CH      ' 0 'HCOR055' 'current   ' 'currentPM ' [-11  11]
    58  [ 8 3] 'ANS-C08/AE/S3.1-CH    ' 0 'HCOR056' 'current   ' 'currentPM ' [-11  11]
    59  [ 8 4] 'ANS-C08/AE/S4-CH      ' 1 'HCOR057' 'current   ' 'currentPM ' [-11  11]
    60  [ 8 5] 'ANS-C08/AE/S3.2-CH    ' 0 'HCOR058' 'current   ' 'currentPM ' [-11  11]
    61  [ 8 6] 'ANS-C08/AE/S2-CH      ' 0 'HCOR059' 'current   ' 'currentPM ' [-11  11]
    62  [ 8 7] 'ANS-C08/AE/S1-CH      ' 1 'HCOR060' 'current   ' 'currentPM ' [-11  11]
    63  [ 9 1] 'ANS-C09/AE/S1-CH      ' 1 'HCOR061' 'current   ' 'currentPM ' [-11  11]
    64  [ 9 2] 'ANS-C09/AE/S2-CH      ' 0 'HCOR062' 'current   ' 'currentPM ' [-11  11]
    65  [ 9 3] 'ANS-C09/AE/S3.1-CH    ' 0 'HCOR063' 'current   ' 'currentPM ' [-11  11]
    66  [ 9 4] 'ANS-C09/AE/S4-CH      ' 1 'HCOR064' 'current   ' 'currentPM ' [-11  11]
    67  [ 9 5] 'ANS-C09/AE/S3.2-CH    ' 0 'HCOR065' 'current   ' 'currentPM ' [-11  11]
    68  [ 9 6] 'ANS-C09/AE/S5-CH      ' 0 'HCOR066' 'current   ' 'currentPM ' [-11  11]
    69  [ 9 7] 'ANS-C09/AE/S6-CH      ' 1 'HCOR067' 'current   ' 'currentPM ' [-11  11]
    70  [10 1] 'ANS-C10/AE/S8.1-CH    ' 1 'HCOR068' 'current   ' 'currentPM ' [-11  11]
    71  [10 2] 'ANS-C10/AE/S7.1-CH    ' 0 'HCOR069' 'current   ' 'currentPM ' [-11  11]
    72  [10 3] 'ANS-C10/AE/S9.1-CH    ' 0 'HCOR070' 'current   ' 'currentPM ' [-11  11]
    73  [10 4] 'ANS-C10/AE/S10.1-CH   ' 1 'HCOR071' 'current   ' 'currentPM ' [-11  11]
    74  [10 5] 'ANS-C10/AE/S10.2-CH   ' 1 'HCOR072' 'current   ' 'currentPM ' [-11  11]
    75  [10 6] 'ANS-C10/AE/S9.2-CH    ' 0 'HCOR073' 'current   ' 'currentPM ' [-11  11]
    76  [10 7] 'ANS-C10/AE/S7.2-CH    ' 0 'HCOR074' 'current   ' 'currentPM ' [-11  11]
    77  [10 8] 'ANS-C10/AE/S8.2-CH    ' 1 'HCOR075' 'current   ' 'currentPM ' [-11  11]
    78  [11 1] 'ANS-C11/AE/S8.1-CH    ' 1 'HCOR076' 'current   ' 'currentPM ' [-11  11]
    79  [11 2] 'ANS-C11/AE/S7.1-CH    ' 0 'HCOR077' 'current   ' 'currentPM ' [-11  11]
    80  [11 3] 'ANS-C11/AE/S9.1-CH    ' 0 'HCOR078' 'current   ' 'currentPM ' [-11  11]
    81  [11 4] 'ANS-C11/AE/S10.1-CH   ' 1 'HCOR079' 'current   ' 'currentPM ' [-11  11]
    82  [11 5] 'ANS-C11/AE/S10.2-CH   ' 1 'HCOR080' 'current   ' 'currentPM ' [-11  11]
    83  [11 6] 'ANS-C11/AE/S9.2-CH    ' 0 'HCOR081' 'current   ' 'currentPM ' [-11  11]
    84  [11 7] 'ANS-C11/AE/S7.2-CH    ' 0 'HCOR082' 'current   ' 'currentPM ' [-11  11]
    85  [11 8] 'ANS-C11/AE/S8.2-CH    ' 1 'HCOR083' 'current   ' 'currentPM ' [-11  11]
    86  [12 1] 'ANS-C12/AE/S6-CH      ' 1 'HCOR084' 'current   ' 'currentPM ' [-11  11]
    87  [12 2] 'ANS-C12/AE/S5-CH      ' 0 'HCOR085' 'current   ' 'currentPM ' [-11  11]
    88  [12 3] 'ANS-C12/AE/S3.1-CH    ' 0 'HCOR086' 'current   ' 'currentPM ' [-11  11]
    89  [12 4] 'ANS-C12/AE/S4-CH      ' 1 'HCOR087' 'current   ' 'currentPM ' [-11  11]
    90  [12 5] 'ANS-C12/AE/S3.2-CH    ' 0 'HCOR088' 'current   ' 'currentPM ' [-11  11]
    91  [12 6] 'ANS-C12/AE/S2-CH      ' 0 'HCOR089' 'current   ' 'currentPM ' [-11  11]
    92  [12 7] 'ANS-C12/AE/S1-CH      ' 1 'HCOR090' 'current   ' 'currentPM ' [-11  11]
    93  [13 1] 'ANS-C13/AE/S1-CH      ' 1 'HCOR091' 'current   ' 'currentPM ' [-11  11]
    94  [13 2] 'ANS-C13/AE/S2-CH      ' 0 'HCOR092' 'current   ' 'currentPM ' [-11  11]
    95  [13 3] 'ANS-C13/AE/S3.1-CH    ' 0 'HCOR093' 'current   ' 'currentPM ' [-11  11]
    96  [13 4] 'ANS-C13/AE/S4-CH      ' 1 'HCOR094' 'current   ' 'currentPM ' [-11  11]
    97  [13 5] 'ANS-C13/AE/S3.2-CH    ' 0 'HCOR095' 'current   ' 'currentPM ' [-11  11]
    98  [13 6] 'ANS-C13/AE/S5-CH      ' 0 'HCOR096' 'current   ' 'currentPM ' [-11  11]
    99  [13 7] 'ANS-C13/AE/S6-CH      ' 1 'HCOR097' 'current   ' 'currentPM ' [-11  11]
   100  [14 1] 'ANS-C14/AE/S8.1-CH    ' 1 'HCOR098' 'current   ' 'currentPM ' [-11  11]
   101  [14 2] 'ANS-C14/AE/S7.1-CH    ' 0 'HCOR099' 'current   ' 'currentPM ' [-11  11]
   102  [14 3] 'ANS-C14/AE/S9.1-CH    ' 0 'HCOR100' 'current   ' 'currentPM ' [-11  11]
   103  [14 4] 'ANS-C14/AE/S10.1-CH   ' 1 'HCOR101' 'current   ' 'currentPM ' [-11  11]
   104  [14 5] 'ANS-C14/AE/S10.2-CH   ' 1 'HCOR102' 'current   ' 'currentPM ' [-11  11]
   105  [14 6] 'ANS-C14/AE/S9.2-CH    ' 0 'HCOR103' 'current   ' 'currentPM ' [-11  11]
   106  [14 7] 'ANS-C14/AE/S7.2-CH    ' 0 'HCOR104' 'current   ' 'currentPM ' [-11  11]
   107  [14 8] 'ANS-C14/AE/S8.2-CH    ' 1 'HCOR105' 'current   ' 'currentPM ' [-11  11]
   108  [15 1] 'ANS-C15/AE/S8.1-CH    ' 1 'HCOR106' 'current   ' 'currentPM ' [-11  11]
   109  [15 2] 'ANS-C15/AE/S7.1-CH    ' 0 'HCOR107' 'current   ' 'currentPM ' [-11  11]
   110  [15 3] 'ANS-C15/AE/S9.1-CH    ' 0 'HCOR108' 'current   ' 'currentPM ' [-11  11]
   111  [15 4] 'ANS-C15/AE/S10.1-CH   ' 1 'HCOR109' 'current   ' 'currentPM ' [-11  11]
   112  [15 5] 'ANS-C15/AE/S10.2-CH   ' 1 'HCOR110' 'current   ' 'currentPM ' [-11  11]
   113  [15 6] 'ANS-C15/AE/S9.2-CH    ' 0 'HCOR111' 'current   ' 'currentPM ' [-11  11]
   114  [15 7] 'ANS-C15/AE/S7.2-CH    ' 0 'HCOR112' 'current   ' 'currentPM ' [-11  11]
   115  [15 8] 'ANS-C15/AE/S8.2-CH    ' 1 'HCOR113' 'current   ' 'currentPM ' [-11  11]
   116  [16 1] 'ANS-C16/AE/S6-CH      ' 1 'HCOR114' 'current   ' 'currentPM ' [-11  11]
   117  [16 2] 'ANS-C16/AE/S5-CH      ' 0 'HCOR115' 'current   ' 'currentPM ' [-11  11]
   118  [16 3] 'ANS-C16/AE/S3.1-CH    ' 0 'HCOR116' 'current   ' 'currentPM ' [-11  11]
   119  [16 4] 'ANS-C16/AE/S4-CH      ' 1 'HCOR117' 'current   ' 'currentPM ' [-11  11]
   120  [16 5] 'ANS-C16/AE/S3.2-CH    ' 0 'HCOR118' 'current   ' 'currentPM ' [-11  11]
   121  [16 6] 'ANS-C16/AE/S2-CH      ' 0 'HCOR119' 'current   ' 'currentPM ' [-11  11]
   122  [16 7] 'ANS-C16/AE/S1-CH      ' 1 'HCOR120' 'current   ' 'currentPM ' [-11  11]
    };

devnumber = length(varlist);
% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Setpoint.TangoNames = cell(devnumber,1);
AO.(ifam).Setpoint.Range = zeros(devnumber,2);

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames(k)  = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,6)));
    AO.(ifam).Setpoint.TangoNames(k) = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,7)));
    AO.(ifam).Setpoint.Range(k,:)      = varlist{k,8};
    % information for getrunflag
    AO.(ifam).Setpoint.RunFlagFcn = @corgetrunflag;
    AO.(ifam).Setpoint.RampRate = 1;
end

%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
setao(AO);
[C, Leff, MagnetType, coefficients] = magnetcoefficients(AO.(ifam).FamilyName);
for ii = 1:devnumber,
    AO.(ifam).Monitor.HW2PhysicsParams{1}(ii,:)   = coefficients;
    AO.(ifam).Monitor.Physics2HWParams{1}(ii,:)   = coefficients;
    AO.(ifam).Setpoint.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(ifam).Setpoint.Physics2HWParams{1}(ii,:)  = coefficients;
end


AO.(ifam).Monitor.HW2PhysicsParams{1}(31,:)   = [0 0 0 0 0 0  2.475e-4 0]; % T.m
AO.(ifam).Monitor.Physics2HWParams{1}(31,:)   = [0 0 0 0 0 0  2.475e-4 0]; % T.m
AO.(ifam).Setpoint.HW2PhysicsParams{1}(31,:)  = [0 0 0 0 0 0  2.475e-4 0]; % T.m
AO.(ifam).Setpoint.Physics2HWParams{1}(31,:)  = [0 0 0 0 0 0  2.475e-4 0]; % T.m
AO.(ifam).Monitor.HW2PhysicsParams{1}(32,:)   = [0 0 0 0 0 0 -2.443e-4 0]; % T.m
AO.(ifam).Monitor.Physics2HWParams{1}(32,:)   = [0 0 0 0 0 0 -2.443e-4 0]; % T.m
AO.(ifam).Setpoint.HW2PhysicsParams{1}(32,:)  = [0 0 0 0 0 0 -2.443e-4 0]; % T.m
AO.(ifam).Setpoint.Physics2HWParams{1}(32,:)  = [0 0 0 0 0 0 -2.443e-4 0]; % T.m

AO.(ifam).Setpoint.Tolerance(:,:)    = 1e-2*ones(devnumber,1);
% Warning optics dependent cf. Low alpha lattice
AO.(ifam).Setpoint.DeltaRespMat(:,:) = ones(devnumber,1)*5e-6*2; % 2*5 urad (half used for kicking)
%AO.(ifam).Setpoint.DeltaRespMat(:,:) = ones(devnumber,1)*0.5e-4*1; % 2*25 urad (half used for kicking)
AO.(ifam).Setpoint.DeltaRespMat(31)  = 10e-6; %rad
AO.(ifam).Setpoint.DeltaRespMat(32)  = 10e-6; %rad

dummy = strcat(AO.(ifam).DeviceName,'/voltage');
AO.(ifam).Voltage.TangoNames(:,:)     = {dummy{:},'ANS-C05/EI/L-HU640/currentCHE', 'ANS-C05/EI/L-HU640/currentCHS'}';

AO.(ifam).Monitor.MemberOf      = {'PlotFamily'};
AO.(ifam).Setpoint.MemberOf     = {'PlotFamily'};
AO.(ifam).Voltage.MemberOf      = {'PlotFamily'};

% Profibus configuration sync/unsync mecanism
AO.(ifam).Profibus.BoardNumber = int32(0);
AO.(ifam).Profibus.Group       = int32(1);
AO.(ifam).Profibus.DeviceName  = 'ANS/AE/DP.CH';

% Group
if ControlRoomFlag
        AO.(ifam).GroupId = tango_group_create2(ifam);
        tango_group_add(AO.(ifam).GroupId,AO.(ifam).DeviceName(find(AO.(ifam).Status),:)');
else
    AO.(ifam).GroupId = nan;
end

%convert response matrix kicks to HWUnits (after AO is loaded to AppData)
setao(AO);   %required to make physics2hw function
AO.(ifam).Setpoint.DeltaRespMat = physics2hw(AO.(ifam).FamilyName,'Setpoint', ...
    AO.(ifam).Setpoint.DeltaRespMat, AO.(ifam).DeviceList);

AO.(ifam).TangoSetpoint = AO.(ifam).Setpoint;
AO.(ifam).TangoSetpoint.TangoNames(:,:)    = strcat(AO.(ifam).DeviceName,'/currentTotal');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SLOW VERTICAL CORRECTORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ifam = 'VCOR';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'COR';
AO.(ifam).MemberOf                 = {'MachineConfig'; 'VCOR'; 'COR'; 'Magnet'; 'PlotFamily'; 'Archivable'};

AO.(ifam).Monitor.Mode             = Mode;
AO.(ifam).Monitor.DataType         = 'Scalar';
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'A';
AO.(ifam).Monitor.PhysicsUnits     = 'meter^-2';
AO.(ifam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn = @k2amp;

AO.(ifam).Setpoint.Mode             = Mode;
AO.(ifam).Setpoint.DataType         = 'Scalar';
AO.(ifam).Setpoint.Units            = 'Hardware';
AO.(ifam).Setpoint.HWUnits          = 'A';
AO.(ifam).Setpoint.PhysicsUnits     = 'rad';
AO.(ifam).Setpoint.HW2PhysicsFcn    = @amp2k;
AO.(ifam).Setpoint.Physics2HWFcn    = @k2amp;

AO.(ifam).Voltage.Mode             = Mode;
AO.(ifam).Voltage.DataType         = 'Scalar';
AO.(ifam).Voltage.Units            = 'Hardware';
AO.(ifam).Voltage.HWUnits          = 'A';
AO.(ifam).Voltage.PhysicsUnits     = 'rad';
AO.(ifam).Voltage.HW2PhysicsFcn    = @amp2k;
AO.(ifam).Voltage.Physics2HWFcn    = @k2amp;

varlist = {
     1 [ 1 1] 'ANS-C01/AE/S1-CV         ' 0 'VCOR001' 'current   ' 'currentPM ' [-14.0  14.0]
     2 [ 1 2] 'ANS-C01/AE/S2-CV         ' 1 'VCOR002' 'current   ' 'currentPM ' [-14.0  14.0]
     3 [ 1 3] 'ANS-C01/AE/S3.1-CV       ' 0 'VCOR003' 'current   ' 'currentPM ' [-14.0  14.0]
     4 [ 1 4] 'ANS-C01/AE/S4-CV         ' 1 'VCOR004' 'current   ' 'currentPM ' [-14.0  14.0]
     5 [ 1 5] 'ANS-C01/AE/S3.2-CV       ' 0 'VCOR005' 'current   ' 'currentPM ' [-14.0  14.0]
     6 [ 1 6] 'ANS-C01/AE/S5-CV         ' 1 'VCOR006' 'current   ' 'currentPM ' [-14.0  14.0]
     7 [ 1 7] 'ANS-C01/AE/S6-CV         ' 0 'VCOR007' 'current   ' 'currentPM ' [-14.0  14.0]
     8 [ 2 1] 'ANS-C02/AE/S8.1-CV       ' 0 'VCOR008' 'current   ' 'currentPM ' [-14.0  14.0]
     9 [ 2 2] 'ANS-C02/AE/S7.1-CV       ' 1 'VCOR009' 'current   ' 'currentPM ' [-14.0  14.0]
    10 [ 2 3] 'ANS-C02/AE/S9.1-CV       ' 1 'VCOR010' 'current   ' 'currentPM ' [-14.0  14.0]
    11 [ 2 4] 'ANS-C02/AE/S10.1-CV      ' 0 'VCOR011' 'current   ' 'currentPM ' [-14.0  14.0]
    12 [ 2 5] 'ANS-C02/AE/S10.2-CV      ' 0 'VCOR012' 'current   ' 'currentPM ' [-14.0  14.0]
    13 [ 2 6] 'ANS-C02/AE/S9.2-CV       ' 1 'VCOR013' 'current   ' 'currentPM ' [-14.0  14.0]
    14 [ 2 7] 'ANS-C02/AE/S7.2-CV       ' 1 'VCOR014' 'current   ' 'currentPM ' [-14.0  14.0]
    15 [ 2 8] 'ANS-C02/AE/S8.2-CV       ' 0 'VCOR015' 'current   ' 'currentPM ' [-14.0  14.0]
    16 [ 3 1] 'ANS-C03/AE/S8.1-CV       ' 0 'VCOR016' 'current   ' 'currentPM ' [-14.0  14.0]
    17 [ 3 2] 'ANS-C03/AE/S7.1-CV       ' 1 'VCOR017' 'current   ' 'currentPM ' [-14.0  14.0]
    18 [ 3 3] 'ANS-C03/AE/S9.1-CV       ' 1 'VCOR018' 'current   ' 'currentPM ' [-14.0  14.0]
    19 [ 3 4] 'ANS-C03/AE/S10.1-CV      ' 0 'VCOR019' 'current   ' 'currentPM ' [-14.0  14.0]
    20 [ 3 5] 'ANS-C03/AE/S10.2-CV      ' 0 'VCOR020' 'current   ' 'currentPM ' [-14.0  14.0]
    21 [ 3 6] 'ANS-C03/AE/S9.2-CV       ' 1 'VCOR021' 'current   ' 'currentPM ' [-14.0  14.0]
    22 [ 3 7] 'ANS-C03/AE/S7.2-CV       ' 1 'VCOR022' 'current   ' 'currentPM ' [-14.0  14.0]
    23 [ 3 8] 'ANS-C03/AE/S8.2-CV       ' 0 'VCOR023' 'current   ' 'currentPM ' [-14.0  14.0]
    24 [ 4 1] 'ANS-C04/AE/S6-CV         ' 0 'VCOR024' 'current   ' 'currentPM ' [-14.0  14.0]
    25 [ 4 2] 'ANS-C04/AE/S5-CV         ' 1 'VCOR025' 'current   ' 'currentPM ' [-14.0  14.0]
    26 [ 4 3] 'ANS-C04/AE/S3.1-CV       ' 0 'VCOR026' 'current   ' 'currentPM ' [-14.0  14.0]
    27 [ 4 4] 'ANS-C04/AE/S4-CV         ' 1 'VCOR027' 'current   ' 'currentPM ' [-14.0  14.0]
    28 [ 4 5] 'ANS-C04/AE/S3.2-CV       ' 0 'VCOR028' 'current   ' 'currentPM ' [-14.0  14.0]
    29 [ 4 6] 'ANS-C04/AE/S2-CV         ' 1 'VCOR029' 'current   ' 'currentPM ' [-14.0  14.0]
    30 [ 4 7] 'ANS-C04/AE/S1-CV         ' 0 'VCOR030' 'current   ' 'currentPM ' [-14.0  14.0]
    31 [ 5 8] 'ANS-C05/EI/L-HU640       ' 0 'VCOR121' 'currentCVE' 'currentCVE' [ -5       5]
    32 [ 5 9] 'ANS-C05/EI/L-HU640       ' 0 'VCOR122' 'currentCVS' 'currentCVS' [ -5       5]
    33 [ 5 1] 'ANS-C05/AE/S1-CV         ' 0 'VCOR031' 'current   ' 'currentPM ' [-14.0  14.0]
    34 [ 5 2] 'ANS-C05/AE/S2-CV         ' 1 'VCOR032' 'current   ' 'currentPM ' [-14.0  14.0]
    35 [ 5 3] 'ANS-C05/AE/S3.1-CV       ' 0 'VCOR033' 'current   ' 'currentPM ' [-14.0  14.0]
    36 [ 5 4] 'ANS-C05/AE/S4-CV         ' 1 'VCOR034' 'current   ' 'currentPM ' [-14.0  14.0]
    37 [ 5 5] 'ANS-C05/AE/S3.2-CV       ' 0 'VCOR035' 'current   ' 'currentPM ' [-14.0  14.0]
    38 [ 5 6] 'ANS-C05/AE/S5-CV         ' 1 'VCOR036' 'current   ' 'currentPM ' [-14.0  14.0]
    39 [ 5 7] 'ANS-C05/AE/S6-CV         ' 0 'VCOR037' 'current   ' 'currentPM ' [-14.0  14.0]
    40 [ 6 1] 'ANS-C06/AE/S8.1-CV       ' 0 'VCOR038' 'current   ' 'currentPM ' [-14.0  14.0]
    41 [ 6 2] 'ANS-C06/AE/S7.1-CV       ' 1 'VCOR039' 'current   ' 'currentPM ' [-14.0  14.0]
    42 [ 6 3] 'ANS-C06/AE/S9.1-CV       ' 1 'VCOR040' 'current   ' 'currentPM ' [-14.0  14.0]
    43 [ 6 4] 'ANS-C06/AE/S10.1-CV      ' 0 'VCOR041' 'current   ' 'currentPM ' [-14.0  14.0]
    44 [ 6 5] 'ANS-C06/AE/S10.2-CV      ' 0 'VCOR042' 'current   ' 'currentPM ' [-14.0  14.0]
    45 [ 6 6] 'ANS-C06/AE/S9.2-CV       ' 1 'VCOR043' 'current   ' 'currentPM ' [-14.0  14.0]
    46 [ 6 7] 'ANS-C06/AE/S7.2-CV       ' 1 'VCOR044' 'current   ' 'currentPM ' [-14.0  14.0]
    47 [ 6 8] 'ANS-C06/AE/S8.2-CV       ' 0 'VCOR045' 'current   ' 'currentPM ' [-14.0  14.0]
    48 [ 7 1] 'ANS-C07/AE/S8.1-CV       ' 0 'VCOR046' 'current   ' 'currentPM ' [-14.0  14.0]
    49 [ 7 2] 'ANS-C07/AE/S7.1-CV       ' 1 'VCOR047' 'current   ' 'currentPM ' [-14.0  14.0]
    50 [ 7 3] 'ANS-C07/AE/S9.1-CV       ' 1 'VCOR048' 'current   ' 'currentPM ' [-14.0  14.0]
    51 [ 7 4] 'ANS-C07/AE/S10.1-CV      ' 0 'VCOR049' 'current   ' 'currentPM ' [-14.0  14.0]
    52 [ 7 5] 'ANS-C07/AE/S10.2-CV      ' 0 'VCOR050' 'current   ' 'currentPM ' [-14.0  14.0]
    53 [ 7 6] 'ANS-C07/AE/S9.2-CV       ' 1 'VCOR051' 'current   ' 'currentPM ' [-14.0  14.0]
    54 [ 7 7] 'ANS-C07/AE/S7.2-CV       ' 1 'VCOR052' 'current   ' 'currentPM ' [-14.0  14.0]
    55 [ 7 8] 'ANS-C07/AE/S8.2-CV       ' 0 'VCOR053' 'current   ' 'currentPM ' [-14.0  14.0]
    56 [ 8 1] 'ANS-C08/AE/S6-CV         ' 0 'VCOR054' 'current   ' 'currentPM ' [-14.0  14.0]
    57 [ 8 2] 'ANS-C08/AE/S5-CV         ' 1 'VCOR055' 'current   ' 'currentPM ' [-14.0  14.0]
    58 [ 8 3] 'ANS-C08/AE/S3.1-CV       ' 0 'VCOR056' 'current   ' 'currentPM ' [-14.0  14.0]
    59 [ 8 4] 'ANS-C08/AE/S4-CV         ' 1 'VCOR057' 'current   ' 'currentPM ' [-14.0  14.0]
    60 [ 8 5] 'ANS-C08/AE/S3.2-CV       ' 0 'VCOR058' 'current   ' 'currentPM ' [-14.0  14.0]
    61 [ 8 6] 'ANS-C08/AE/S2-CV         ' 1 'VCOR059' 'current   ' 'currentPM ' [-14.0  14.0]
    62 [ 8 7] 'ANS-C08/AE/S1-CV         ' 0 'VCOR060' 'current   ' 'currentPM ' [-14.0  14.0]
    63 [ 9 1] 'ANS-C09/AE/S1-CV         ' 0 'VCOR061' 'current   ' 'currentPM ' [-14.0  14.0]
    64 [ 9 2] 'ANS-C09/AE/S2-CV         ' 1 'VCOR062' 'current   ' 'currentPM ' [-14.0  14.0]
    65 [ 9 3] 'ANS-C09/AE/S3.1-CV       ' 0 'VCOR063' 'current   ' 'currentPM ' [-14.0  14.0]
    66 [ 9 4] 'ANS-C09/AE/S4-CV         ' 1 'VCOR064' 'current   ' 'currentPM ' [-14.0  14.0]
    67 [ 9 5] 'ANS-C09/AE/S3.2-CV       ' 0 'VCOR065' 'current   ' 'currentPM ' [-14.0  14.0]
    68 [ 9 6] 'ANS-C09/AE/S5-CV         ' 1 'VCOR066' 'current   ' 'currentPM ' [-14.0  14.0]
    69 [ 9 7] 'ANS-C09/AE/S6-CV         ' 0 'VCOR067' 'current   ' 'currentPM ' [-14.0  14.0]
    70 [10 1] 'ANS-C10/AE/S8.1-CV       ' 0 'VCOR068' 'current   ' 'currentPM ' [-14.0  14.0]
    71 [10 2] 'ANS-C10/AE/S7.1-CV       ' 1 'VCOR069' 'current   ' 'currentPM ' [-14.0  14.0]
    72 [10 3] 'ANS-C10/AE/S9.1-CV       ' 1 'VCOR070' 'current   ' 'currentPM ' [-14.0  14.0]
    73 [10 4] 'ANS-C10/AE/S10.1-CV      ' 0 'VCOR071' 'current   ' 'currentPM ' [-14.0  14.0]
    74 [10 5] 'ANS-C10/AE/S10.2-CV      ' 0 'VCOR072' 'current   ' 'currentPM ' [-14.0  14.0]
    75 [10 6] 'ANS-C10/AE/S9.2-CV       ' 1 'VCOR073' 'current   ' 'currentPM ' [-14.0  14.0]
    76 [10 7] 'ANS-C10/AE/S7.2-CV       ' 1 'VCOR074' 'current   ' 'currentPM ' [-14.0  14.0]
    77 [10 8] 'ANS-C10/AE/S8.2-CV       ' 0 'VCOR075' 'current   ' 'currentPM ' [-14.0  14.0]
    78 [11 1] 'ANS-C11/AE/S8.1-CV       ' 0 'VCOR076' 'current   ' 'currentPM ' [-14.0  14.0]
    79 [11 2] 'ANS-C11/AE/S7.1-CV       ' 1 'VCOR077' 'current   ' 'currentPM ' [-14.0  14.0]
    80 [11 3] 'ANS-C11/AE/S9.1-CV       ' 1 'VCOR078' 'current   ' 'currentPM ' [-14.0  14.0]
    81 [11 4] 'ANS-C11/AE/S10.1-CV      ' 0 'VCOR079' 'current   ' 'currentPM ' [-14.0  14.0]
    82 [11 5] 'ANS-C11/AE/S10.2-CV      ' 0 'VCOR080' 'current   ' 'currentPM ' [-14.0  14.0]
    83 [11 6] 'ANS-C11/AE/S9.2-CV       ' 1 'VCOR081' 'current   ' 'currentPM ' [-14.0  14.0]
    84 [11 7] 'ANS-C11/AE/S7.2-CV       ' 1 'VCOR082' 'current   ' 'currentPM ' [-14.0  14.0]
    85 [11 8] 'ANS-C11/AE/S8.2-CV       ' 0 'VCOR083' 'current   ' 'currentPM ' [-14.0  14.0]
    86 [12 1] 'ANS-C12/AE/S6-CV         ' 0 'VCOR084' 'current   ' 'currentPM ' [-14.0  14.0]
    87 [12 2] 'ANS-C12/AE/S5-CV         ' 1 'VCOR085' 'current   ' 'currentPM ' [-14.0  14.0]
    88 [12 3] 'ANS-C12/AE/S3.1-CV       ' 0 'VCOR086' 'current   ' 'currentPM ' [-14.0  14.0]
    89 [12 4] 'ANS-C12/AE/S4-CV         ' 1 'VCOR087' 'current   ' 'currentPM ' [-14.0  14.0]
    90 [12 5] 'ANS-C12/AE/S3.2-CV       ' 0 'VCOR088' 'current   ' 'currentPM ' [-14.0  14.0]
    91 [12 6] 'ANS-C12/AE/S2-CV         ' 1 'VCOR089' 'current   ' 'currentPM ' [-14.0  14.0]
    92 [12 7] 'ANS-C12/AE/S1-CV         ' 0 'VCOR090' 'current   ' 'currentPM ' [-14.0  14.0]
    93 [13 1] 'ANS-C13/AE/S1-CV         ' 0 'VCOR091' 'current   ' 'currentPM ' [-14.0  14.0]
    94 [13 2] 'ANS-C13/AE/S2-CV         ' 1 'VCOR092' 'current   ' 'currentPM ' [-14.0  14.0]
    95 [13 3] 'ANS-C13/AE/S3.1-CV       ' 0 'VCOR093' 'current   ' 'currentPM ' [-14.0  14.0]
    96 [13 4] 'ANS-C13/AE/S4-CV         ' 1 'VCOR094' 'current   ' 'currentPM ' [-14.0  14.0]
    97 [13 5] 'ANS-C13/AE/S3.2-CV       ' 0 'VCOR095' 'current   ' 'currentPM ' [-14.0  14.0]
    98 [13 6] 'ANS-C13/AE/S5-CV         ' 1 'VCOR096' 'current   ' 'currentPM ' [-14.0  14.0]
    99 [13 7] 'ANS-C13/AE/S6-CV         ' 0 'VCOR097' 'current   ' 'currentPM ' [-14.0  14.0]
   100 [14 1] 'ANS-C14/AE/S8.1-CV       ' 0 'VCOR098' 'current   ' 'currentPM ' [-14.0  14.0]
   101 [14 2] 'ANS-C14/AE/S7.1-CV       ' 1 'VCOR099' 'current   ' 'currentPM ' [-14.0  14.0]
   102 [14 3] 'ANS-C14/AE/S9.1-CV       ' 1 'VCOR100' 'current   ' 'currentPM ' [-14.0  14.0]
   103 [14 4] 'ANS-C14/AE/S10.1-CV      ' 0 'VCOR101' 'current   ' 'currentPM ' [-14.0  14.0]
   104 [14 5] 'ANS-C14/AE/S10.2-CV      ' 0 'VCOR102' 'current   ' 'currentPM ' [-14.0  14.0]
   105 [14 6] 'ANS-C14/AE/S9.2-CV       ' 1 'VCOR103' 'current   ' 'currentPM ' [-14.0  14.0]
   106 [14 7] 'ANS-C14/AE/S7.2-CV       ' 1 'VCOR104' 'current   ' 'currentPM ' [-14.0  14.0]
   107 [14 8] 'ANS-C14/AE/S8.2-CV       ' 0 'VCOR105' 'current   ' 'currentPM ' [-14.0  14.0]
   108 [15 1] 'ANS-C15/AE/S8.1-CV       ' 0 'VCOR106' 'current   ' 'currentPM ' [-14.0  14.0]
   109 [15 2] 'ANS-C15/AE/S7.1-CV       ' 1 'VCOR107' 'current   ' 'currentPM ' [-14.0  14.0]
   110 [15 3] 'ANS-C15/AE/S9.1-CV       ' 1 'VCOR108' 'current   ' 'currentPM ' [-14.0  14.0]
   111 [15 4] 'ANS-C15/AE/S10.1-CV      ' 0 'VCOR109' 'current   ' 'currentPM ' [-14.0  14.0]
   112 [15 5] 'ANS-C15/AE/S10.2-CV      ' 0 'VCOR110' 'current   ' 'currentPM ' [-14.0  14.0]
   113 [15 6] 'ANS-C15/AE/S9.2-CV       ' 1 'VCOR111' 'current   ' 'currentPM ' [-14.0  14.0]
   114 [15 7] 'ANS-C15/AE/S7.2-CV       ' 1 'VCOR112' 'current   ' 'currentPM ' [-14.0  14.0]
   115 [15 8] 'ANS-C15/AE/S8.2-CV       ' 0 'VCOR113' 'current   ' 'currentPM ' [-14.0  14.0]
   116 [16 1] 'ANS-C16/AE/S6-CV         ' 0 'VCOR114' 'current   ' 'currentPM ' [-14.0  14.0]
   117 [16 2] 'ANS-C16/AE/S5-CV         ' 1 'VCOR115' 'current   ' 'currentPM ' [-14.0  14.0]
   118 [16 3] 'ANS-C16/AE/S3.1-CV       ' 0 'VCOR116' 'current   ' 'currentPM ' [-14.0  14.0]
   119 [16 4] 'ANS-C16/AE/S4-CV         ' 1 'VCOR117' 'current   ' 'currentPM ' [-14.0  14.0]
   120 [16 5] 'ANS-C16/AE/S3.2-CV       ' 0 'VCOR118' 'current   ' 'currentPM ' [-14.0  14.0]
   121 [16 6] 'ANS-C16/AE/S2-CV         ' 1 'VCOR119' 'current   ' 'currentPM ' [-14.0  14.0]
   122 [16 7] 'ANS-C16/AE/S1-CV         ' 0 'VCOR120' 'current   ' 'currentPM ' [-14.0  14.0]
    };

devnumber = length(varlist);
% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Setpoint.TangoNames = cell(devnumber,1);
AO.(ifam).Setpoint.Range = zeros(devnumber,2);

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames(k)  = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,6)));
    AO.(ifam).Setpoint.TangoNames(k) = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,7)));
    AO.(ifam).Setpoint.Range(k,:)      = varlist{k,8};
    % information for getrunflag
    AO.(ifam).Setpoint.RunFlagFcn = @corgetrunflag;
    AO.(ifam).Setpoint.RampRate = 1;
end


%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
setao(AO);
[C, Leff, MagnetType, coefficients] = magnetcoefficients(AO.(ifam).FamilyName);

for ii = 1:devnumber,
    AO.(ifam).Monitor.HW2PhysicsParams{1}(ii,:)   = coefficients;
    AO.(ifam).Monitor.Physics2HWParams{1}(ii,:)   = coefficients;
    AO.(ifam).Setpoint.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(ifam).Setpoint.Physics2HWParams{1}(ii,:)  = coefficients;
end

AO.(ifam).Monitor.HW2PhysicsParams{1}(31,:)   = [0 0 0 0 0 0  2.138e-4 0]; % T.m/A
AO.(ifam).Monitor.Physics2HWParams{1}(31,:)   = [0 0 0 0 0 0  2.138e-4 0]; % T.m/A
AO.(ifam).Setpoint.HW2PhysicsParams{1}(31,:)  = [0 0 0 0 0 0  2.138e-4 0]; % T.m/A
AO.(ifam).Setpoint.Physics2HWParams{1}(31,:)  = [0 0 0 0 0 0  2.138e-4 0]; % T.m/A
AO.(ifam).Monitor.HW2PhysicsParams{1}(32,:)   = [0 0 0 0 0 0  2.135e-4 0]; % T.m/A
AO.(ifam).Monitor.Physics2HWParams{1}(32,:)   = [0 0 0 0 0 0  2.135e-4 0]; % T.m/A
AO.(ifam).Setpoint.HW2PhysicsParams{1}(32,:)  = [0 0 0 0 0 0  2.135e-4 0]; % T.m/A
AO.(ifam).Setpoint.Physics2HWParams{1}(32,:)  = [0 0 0 0 0 0  2.135e-4 0]; % T.m/A

AO.(ifam).Monitor.MemberOf  = {'PlotFamily'};
AO.(ifam).Voltage.MemberOf  = {'PlotFamily'};
AO.(ifam).Setpoint.MemberOf  = {'PlotFamily'};

AO.(ifam).Setpoint.Tolerance(:,:)    = 1e-2*ones(devnumber,1);
% Warning optics dependent cf. Low alpha lattice
AO.(ifam).Setpoint.DeltaRespMat(:,:) = ones(devnumber,1)*10e-6*2; % 2*10 urad (half used for kicking)
%AO.(ifam).Setpoint.DeltaRespMat(:,:) = ones(devnumber,1)*1e-4*2; % 2*25 urad (half used for kicking)
AO.(ifam).Setpoint.DeltaRespMat(31)  = 10e-6; %rad
AO.(ifam).Setpoint.DeltaRespMat(32)  = 10e-6; %rad

dummy = strcat(AO.(ifam).DeviceName,'/voltage');
AO.(ifam).Voltage.TangoNames(:,:)     = {dummy{:},'ANS-C05/EI/L-HU640/currentCVE', 'ANS-C05/EI/L-HU640/currentCVS'}';


% Profibus configuration sync/yn=unsync mecanism
AO.(ifam).Profibus.BoardNumber = int32(0);
AO.(ifam).Profibus.Group       = int32(1);
AO.(ifam).Profibus.DeviceName  = 'ANS/AE/DP.CV';

% Group
if ControlRoomFlag
        AO.(ifam).GroupId = tango_group_create2(ifam);
        tango_group_add(AO.(ifam).GroupId,AO.(ifam).DeviceName(find(AO.(ifam).Status),:)');
else
    AO.(ifam).GroupId = nan;
end

%convert response matrix kicks to HWUnits (after AO is loaded to AppData)
setao(AO);   %required to make physics2hw function
AO.(ifam).Setpoint.DeltaRespMat = physics2hw(AO.(ifam).FamilyName,'Setpoint', ...
    AO.(ifam).Setpoint.DeltaRespMat, AO.(ifam).DeviceList);
AO.(ifam).TangoSetpoint = AO.(ifam).Setpoint;
AO.(ifam).TangoSetpoint.TangoNames(:,:)    = strcat(AO.(ifam).DeviceName,'/currentTotal');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Skew Quadrupole data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ifam = 'QT';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'SkewQuad';
AO.(ifam).MemberOf                 = {'MachineConfig'; 'Magnet'; 'PlotFamily'; 'Archivable'};

AO.(ifam).Monitor.Mode             = Mode;
AO.(ifam).Monitor.DataType         = 'Scalar';
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'A';
AO.(ifam).Monitor.PhysicsUnits     = 'meter^-2';
AO.(ifam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn = @k2amp;

% elemlist devlist tangoname status  common   range
varlist = {
     1  [ 1 1] 'ANS-C01/AE/S1-QT         ' 1 'QT001'  [ -7.0   7.0]
     2  [ 1 2] 'ANS-C01/AE/S2-QT         ' 0 'QT002'  [ -7.0   7.0]
     3  [ 1 3] 'ANS-C01/AE/S3.1-QT       ' 0 'QT003'  [ -7.0   7.0]
     4  [ 1 4] 'ANS-C01/AE/S4-QT         ' 1 'QT004'  [ -7.0   7.0]
     5  [ 1 5] 'ANS-C01/AE/S3.2-QT       ' 0 'QT005'  [ -7.0   7.0]
     6  [ 1 6] 'ANS-C01/AE/S5-QT         ' 0 'QT006'  [ -7.0   7.0]
     7  [ 1 7] 'ANS-C01/AE/S6-QT         ' 0 'QT007'  [ -7.0   7.0]
     8  [ 2 1] 'ANS-C02/AE/S8.1-QT       ' 0 'QT008'  [ -7.0   7.0]
     9  [ 2 2] 'ANS-C02/AE/S7.1-QT       ' 0 'QT009'  [ -7.0   7.0]
    10  [ 2 3] 'ANS-C02/AE/S9.1-QT       ' 0 'QT010'  [ -7.0   7.0]
    11  [ 2 4] 'ANS-C02/AE/S10.1-QT      ' 1 'QT011'  [ -7.0   7.0]
    12  [ 2 5] 'ANS-C02/AE/S10.2-QT      ' 0 'QT012'  [ -7.0   7.0]
    13  [ 2 6] 'ANS-C02/AE/S9.2-QT       ' 0 'QT013'  [ -7.0   7.0]
    14  [ 2 7] 'ANS-C02/AE/S7.2-QT       ' 0 'QT014'  [ -7.0   7.0]
    15  [ 2 8] 'ANS-C02/AE/S8.2-QT       ' 1 'QT015'  [ -7.0   7.0]
    16  [ 3 1] 'ANS-C03/AE/S8.1-QT       ' 1 'QT016'  [ -7.0   7.0]
    17  [ 3 2] 'ANS-C03/AE/S7.1-QT       ' 0 'QT017'  [ -7.0   7.0]
    18  [ 3 3] 'ANS-C03/AE/S9.1-QT       ' 0 'QT018'  [ -7.0   7.0]
    19  [ 3 4] 'ANS-C03/AE/S10.1-QT      ' 0 'QT019'  [ -7.0   7.0]
    20  [ 3 5] 'ANS-C03/AE/S10.2-QT      ' 1 'QT020'  [ -7.0   7.0]
    21  [ 3 6] 'ANS-C03/AE/S9.2-QT       ' 0 'QT021'  [ -7.0   7.0]
    22  [ 3 7] 'ANS-C03/AE/S7.2-QT       ' 0 'QT022'  [ -7.0   7.0]
    23  [ 3 8] 'ANS-C03/AE/S8.2-QT       ' 0 'QT023'  [ -7.0   7.0]
    24  [ 4 1] 'ANS-C04/AE/S6-QT         ' 0 'QT024'  [ -7.0   7.0]
    25  [ 4 2] 'ANS-C04/AE/S5-QT         ' 0 'QT025'  [ -7.0   7.0]
    26  [ 4 3] 'ANS-C04/AE/S3.1-QT       ' 0 'QT026'  [ -7.0   7.0]
    27  [ 4 4] 'ANS-C04/AE/S4-QT         ' 1 'QT027'  [ -7.0   7.0]
    28  [ 4 5] 'ANS-C04/AE/S3.2-QT       ' 0 'QT028'  [ -7.0   7.0]
    29  [ 4 6] 'ANS-C04/AE/S2-QT         ' 0 'QT029'  [ -7.0   7.0]
    30  [ 4 7] 'ANS-C04/AE/S1-QT         ' 1 'QT030'  [ -7.0   7.0]
    31  [ 5 1] 'ANS-C05/AE/S1-QT         ' 1 'QT031'  [ -7.0   7.0]
    32  [ 5 2] 'ANS-C05/AE/S2-QT         ' 0 'QT032'  [ -7.0   7.0]
    33  [ 5 3] 'ANS-C05/AE/S3.1-QT       ' 0 'QT033'  [ -7.0   7.0]
    34  [ 5 4] 'ANS-C05/AE/S4-QT         ' 1 'QT034'  [ -7.0   7.0]
    35  [ 5 5] 'ANS-C05/AE/S3.2-QT       ' 0 'QT035'  [ -7.0   7.0]
    36  [ 5 6] 'ANS-C05/AE/S5-QT         ' 0 'QT036'  [ -7.0   7.0]
    37  [ 5 7] 'ANS-C05/AE/S6-QT         ' 0 'QT037'  [ -7.0   7.0]
    38  [ 6 1] 'ANS-C06/AE/S8.1-QT       ' 0 'QT038'  [ -7.0   7.0]
    39  [ 6 2] 'ANS-C06/AE/S7.1-QT       ' 0 'QT039'  [ -7.0   7.0]
    40  [ 6 3] 'ANS-C06/AE/S9.1-QT       ' 0 'QT040'  [ -7.0   7.0]
    41  [ 6 4] 'ANS-C06/AE/S10.1-QT      ' 1 'QT041'  [ -7.0   7.0]
    42  [ 6 5] 'ANS-C06/AE/S10.2-QT      ' 0 'QT042'  [ -7.0   7.0]
    43  [ 6 6] 'ANS-C06/AE/S9.2-QT       ' 0 'QT043'  [ -7.0   7.0]
    44  [ 6 7] 'ANS-C06/AE/S7.2-QT       ' 0 'QT044'  [ -7.0   7.0]
    45  [ 6 8] 'ANS-C06/AE/S8.2-QT       ' 1 'QT045'  [ -7.0   7.0]
    46  [ 7 1] 'ANS-C07/AE/S8.1-QT       ' 1 'QT046'  [ -7.0   7.0]
    47  [ 7 2] 'ANS-C07/AE/S7.1-QT       ' 0 'QT047'  [ -7.0   7.0]
    48  [ 7 3] 'ANS-C07/AE/S9.1-QT       ' 0 'QT048'  [ -7.0   7.0]
    49  [ 7 4] 'ANS-C07/AE/S10.1-QT      ' 0 'QT049'  [ -7.0   7.0]
    50  [ 7 5] 'ANS-C07/AE/S10.2-QT      ' 1 'QT050'  [ -7.0   7.0]
    51  [ 7 6] 'ANS-C07/AE/S9.2-QT       ' 0 'QT051'  [ -7.0   7.0]
    52  [ 7 7] 'ANS-C07/AE/S7.2-QT       ' 0 'QT052'  [ -7.0   7.0]
    53  [ 7 8] 'ANS-C07/AE/S8.2-QT       ' 0 'QT053'  [ -7.0   7.0]
    54  [ 8 1] 'ANS-C08/AE/S6-QT         ' 0 'QT054'  [ -7.0   7.0]
    55  [ 8 2] 'ANS-C08/AE/S5-QT         ' 0 'QT055'  [ -7.0   7.0]
    56  [ 8 3] 'ANS-C08/AE/S3.1-QT       ' 0 'QT056'  [ -7.0   7.0]
    57  [ 8 4] 'ANS-C08/AE/S4-QT         ' 1 'QT057'  [ -7.0   7.0]
    58  [ 8 5] 'ANS-C08/AE/S3.2-QT       ' 0 'QT058'  [ -7.0   7.0]
    59  [ 8 6] 'ANS-C08/AE/S2-QT         ' 0 'QT059'  [ -7.0   7.0]
    60  [ 8 7] 'ANS-C08/AE/S1-QT         ' 1 'QT060'  [ -7.0   7.0]
    61  [ 9 1] 'ANS-C09/AE/S1-QT         ' 1 'QT061'  [ -7.0   7.0]
    62  [ 9 2] 'ANS-C09/AE/S2-QT         ' 0 'QT062'  [ -7.0   7.0]
    63  [ 9 3] 'ANS-C09/AE/S3.1-QT       ' 0 'QT063'  [ -7.0   7.0]
    64  [ 9 4] 'ANS-C09/AE/S4-QT         ' 1 'QT064'  [ -7.0   7.0]
    65  [ 9 5] 'ANS-C09/AE/S3.2-QT       ' 0 'QT065'  [ -7.0   7.0]
    66  [ 9 6] 'ANS-C09/AE/S5-QT         ' 0 'QT066'  [ -7.0   7.0]
    67  [ 9 7] 'ANS-C09/AE/S6-QT         ' 0 'QT067'  [ -7.0   7.0]
    68  [10 1] 'ANS-C10/AE/S8.1-QT       ' 0 'QT068'  [ -7.0   7.0]
    69  [10 2] 'ANS-C10/AE/S7.1-QT       ' 0 'QT069'  [ -7.0   7.0]
    70  [10 3] 'ANS-C10/AE/S9.1-QT       ' 0 'QT070'  [ -7.0   7.0]
    71  [10 4] 'ANS-C10/AE/S10.1-QT      ' 1 'QT071'  [ -7.0   7.0]
    72  [10 5] 'ANS-C10/AE/S10.2-QT      ' 0 'QT072'  [ -7.0   7.0]
    73  [10 6] 'ANS-C10/AE/S9.2-QT       ' 0 'QT073'  [ -7.0   7.0]
    74  [10 7] 'ANS-C10/AE/S7.2-QT       ' 0 'QT074'  [ -7.0   7.0]
    75  [10 8] 'ANS-C10/AE/S8.2-QT       ' 1 'QT075'  [ -7.0   7.0]
    76  [11 1] 'ANS-C11/AE/S8.1-QT       ' 1 'QT076'  [ -7.0   7.0]
    77  [11 2] 'ANS-C11/AE/S7.1-QT       ' 0 'QT077'  [ -7.0   7.0]
    78  [11 3] 'ANS-C11/AE/S9.1-QT       ' 0 'QT078'  [ -7.0   7.0]
    79  [11 4] 'ANS-C11/AE/S10.1-QT      ' 0 'QT079'  [ -7.0   7.0]
    80  [11 5] 'ANS-C11/AE/S10.2-QT      ' 1 'QT080'  [ -7.0   7.0]
    81  [11 6] 'ANS-C11/AE/S9.2-QT       ' 0 'QT081'  [ -7.0   7.0]
    82  [11 7] 'ANS-C11/AE/S7.2-QT       ' 0 'QT082'  [ -7.0   7.0]
    83  [11 8] 'ANS-C11/AE/S8.2-QT       ' 0 'QT083'  [ -7.0   7.0]
    84  [12 1] 'ANS-C12/AE/S6-QT         ' 0 'QT084'  [ -7.0   7.0]
    85  [12 2] 'ANS-C12/AE/S5-QT         ' 0 'QT085'  [ -7.0   7.0]
    86  [12 3] 'ANS-C12/AE/S3.1-QT       ' 0 'QT086'  [ -7.0   7.0]
    87  [12 4] 'ANS-C12/AE/S4-QT         ' 1 'QT087'  [ -7.0   7.0]
    88  [12 5] 'ANS-C12/AE/S3.2-QT       ' 0 'QT088'  [ -7.0   7.0]
    89  [12 6] 'ANS-C12/AE/S2-QT         ' 0 'QT089'  [ -7.0   7.0]
    90  [12 7] 'ANS-C12/AE/S1-QT         ' 1 'QT090'  [ -7.0   7.0]
    91  [13 1] 'ANS-C13/AE/S1-QT         ' 1 'QT091'  [ -7.0   7.0]
    92  [13 2] 'ANS-C13/AE/S2-QT         ' 0 'QT092'  [ -7.0   7.0]
    93  [13 3] 'ANS-C13/AE/S3.1-QT       ' 0 'QT093'  [ -7.0   7.0]
    94  [13 4] 'ANS-C13/AE/S4-QT         ' 1 'QT094'  [ -7.0   7.0]
    95  [13 5] 'ANS-C13/AE/S3.2-QT       ' 0 'QT095'  [ -7.0   7.0]
    96  [13 6] 'ANS-C13/AE/S5-QT         ' 0 'QT096'  [ -7.0   7.0]
    97  [13 7] 'ANS-C13/AE/S6-QT         ' 0 'QT097'  [ -7.0   7.0]
    98  [14 1] 'ANS-C14/AE/S8.1-QT       ' 0 'QT098'  [ -7.0   7.0]
    99  [14 2] 'ANS-C14/AE/S7.1-QT       ' 0 'QT099'  [ -7.0   7.0]
   100  [14 3] 'ANS-C14/AE/S9.1-QT       ' 0 'QT100'  [ -7.0   7.0]
   101  [14 4] 'ANS-C14/AE/S10.1-QT      ' 1 'QT101'  [ -7.0   7.0]
   102  [14 5] 'ANS-C14/AE/S10.2-QT      ' 0 'QT102'  [ -7.0   7.0]
   103  [14 6] 'ANS-C14/AE/S9.2-QT       ' 0 'QT103'  [ -7.0   7.0]
   104  [14 7] 'ANS-C14/AE/S7.2-QT       ' 0 'QT104'  [ -7.0   7.0]
   105  [14 8] 'ANS-C14/AE/S8.2-QT       ' 1 'QT105'  [ -7.0   7.0]
   106  [15 1] 'ANS-C15/AE/S8.1-QT       ' 1 'QT106'  [ -7.0   7.0]
   107  [15 2] 'ANS-C15/AE/S7.1-QT       ' 0 'QT107'  [ -7.0   7.0]
   108  [15 3] 'ANS-C15/AE/S9.1-QT       ' 0 'QT108'  [ -7.0   7.0]
   109  [15 4] 'ANS-C15/AE/S10.1-QT      ' 0 'QT109'  [ -7.0   7.0]
   110  [15 5] 'ANS-C15/AE/S10.2-QT      ' 1 'QT110'  [ -7.0   7.0]
   111  [15 6] 'ANS-C15/AE/S9.2-QT       ' 0 'QT111'  [ -7.0   7.0]
   112  [15 7] 'ANS-C15/AE/S7.2-QT       ' 0 'QT112'  [ -7.0   7.0]
   113  [15 8] 'ANS-C15/AE/S8.2-QT       ' 0 'QT113'  [ -7.0   7.0]
   114  [16 1] 'ANS-C16/AE/S6-QT         ' 0 'QT114'  [ -7.0   7.0]
   115  [16 2] 'ANS-C16/AE/S5-QT         ' 0 'QT115'  [ -7.0   7.0]
   116  [16 3] 'ANS-C16/AE/S3.1-QT       ' 0 'QT116'  [ -7.0   7.0]
   117  [16 4] 'ANS-C16/AE/S4-QT         ' 1 'QT117'  [ -7.0   7.0]
   118  [16 5] 'ANS-C16/AE/S3.2-QT       ' 0 'QT118'  [ -7.0   7.0]
   119  [16 6] 'ANS-C16/AE/S2-QT         ' 0 'QT119'  [ -7.0   7.0]
   120  [16 7] 'ANS-C16/AE/S1-QT         ' 1 'QT120'  [ -7.0   7.0]
    };

devnumber = length(varlist);
% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Setpoint.TangoNames = cell(devnumber,1);
AO.(ifam).Setpoint.Range = zeros(devnumber,2);
AO.(ifam).Monitor.Handles(:,1)    = NaN*ones(devnumber,1);

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames{k}  = strcat(AO.(ifam).DeviceName{k}, '/currentPM');
    AO.(ifam).Setpoint.TangoNames{k} = strcat(AO.(ifam).DeviceName{k}, '/currentPM');
    AO.(ifam).Setpoint.Range(k,:)      = varlist{k,6};
    % information for getrunflag
    AO.(ifam).Setpoint.RunFlagFcn = @tangogetrunflag;
    AO.(ifam).Setpoint.RampRate = 1;
end
% Load coeeficients fot thin element
coefficients = magnetcoefficients(AO.(ifam).FamilyName);

for ii=1:devnumber,
    AO.(ifam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(ifam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(ifam).Setpoint.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(ifam).Setpoint.Physics2HWParams{1}(ii,:)  = coefficients;
end

AO.(ifam).Desired = AO.(ifam).Monitor;
AO.(ifam).Setpoint.MemberOf  = {'PlotFamily'};
AO.(ifam).Setpoint.TangoNames(:,:)    = strcat(AO.(ifam).DeviceName,'/currentPM');

AO.(ifam).Setpoint.Tolerance(:,:) = 1000*ones(devnumber,1);
AO.(ifam).Setpoint.DeltaRespMat(:,:) = 3*ones(devnumber,1);
AO.(ifam).Setpoint.DeltaSkewK = 1; % for SkewQuad efficiency toward dispersion .
% information for getrunflag
AO.(ifam).Setpoint.RunFlagFcn = @tangogetrunflag;
AO.(ifam).Setpoint.RampRate = 1;

% Profibus configuration
AO.(ifam).Profibus.BoardNumber = int32(0);
AO.(ifam).Profibus.Group       = int32(1);
AO.(ifam).Profibus.DeviceName  = 'ANS/AE/DP.QT';

% Group
if ControlRoomFlag
        AO.(ifam).GroupId = tango_group_create2(ifam);
        tango_group_add(AO.(ifam).GroupId,AO.(ifam).DeviceName(find(AO.(ifam).Status),:)');
else
    AO.(ifam).GroupId = nan;
end

%convert response matrix kicks to HWUnits (after AO is loaded to AppData)
setao(AO);   %required to make physics2hw function
AO.(ifam).Setpoint.DeltaRespMat = physics2hw(AO.(ifam).FamilyName, 'Setpoint', ...
    AO.(ifam).Setpoint.DeltaRespMat, AO.(ifam).DeviceList);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Skew Quadrupole data (virtual QT)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ifam = 'SQ';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'SkewQuad';
AO.(ifam).MemberOf                 = {'Magnet'; 'PlotFamily'; 'Archivable'};
%AO.(ifam).MemberOf                 = {'MachineConfig'; 'Magnet'; 'PlotFamily'; 'Archivable'};

AO.(ifam).Monitor.Mode             = Mode;
AO.(ifam).Monitor.DataType         = 'Scalar';
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'A';
AO.(ifam).Monitor.PhysicsUnits     = 'meter^-2';
AO.(ifam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn = @k2amp;

AO.(ifam).Setpoint = AO.(ifam).Monitor;

skewquadrupoles = {
    
1, 'ANS-C1/AE/QTvirtuel/1', 1,    [1 1],  'VirtualSkewQuad_1'
2, 'ANS-C1/AE/QTvirtuel/2', 1,    [1 2],  'VirtualSkewQuad_2'
3, 'ANS-C1/AE/QTvirtuel/3', 1,    [1 3],  'VirtualSkewQuad_3'
4, 'ANS-C1/AE/QTvirtuel/4', 1,    [1 4],  'VirtualSkewQuad_4'
5, 'ANS-C1/AE/QTvirtuel/5', 1,    [1 5],  'VirtualSkewQuad_5'
6, 'ANS-C1/AE/QTvirtuel/6', 1,    [1 6],  'VirtualSkewQuad_6'
7, 'ANS-C1/AE/QTvirtuel/7', 1,    [1 7],  'VirtualSkewQuad_7'
8, 'ANS-C1/AE/QTvirtuel/8', 1,    [1 8],  'VirtualSkewQuad_8'
9, 'ANS-C1/AE/QTvirtuel/9', 1,    [1 9],  'VirtualSkewQuad_9'
10, 'ANS-C1/AE/QTvirtuel/10', 1,    [1 10],  'VirtualSkewQuad_10'


11, 'ANS-C2/AE/QTvirtuel/1', 1,    [2 1],  'VirtualSkewQuad_11'
12, 'ANS-C2/AE/QTvirtuel/2', 1,    [2 2],  'VirtualSkewQuad_12'
13, 'ANS-C2/AE/QTvirtuel/3', 1,    [2 3],  'VirtualSkewQuad_13'
14, 'ANS-C2/AE/QTvirtuel/4', 1,    [2 4],  'VirtualSkewQuad_14'
15, 'ANS-C2/AE/QTvirtuel/5', 1,    [2 5],  'VirtualSkewQuad_15'
16, 'ANS-C2/AE/QTvirtuel/6', 1,    [2 6],  'VirtualSkewQuad_16'
17, 'ANS-C2/AE/QTvirtuel/7', 1,    [2 7],  'VirtualSkewQuad_17'
18, 'ANS-C2/AE/QTvirtuel/8', 1,    [2 8],  'VirtualSkewQuad_18'
19, 'ANS-C2/AE/QTvirtuel/9', 1,    [2 9],  'VirtualSkewQuad_19'
20, 'ANS-C2/AE/QTvirtuel/10', 1,    [2 10],  'VirtualSkewQuad_20'



21, 'ANS-C3/AE/QTvirtuel/1', 1,    [3 1],  'VirtualSkewQuad_21'
22, 'ANS-C3/AE/QTvirtuel/2', 1,    [3 2],  'VirtualSkewQuad_22'
23, 'ANS-C3/AE/QTvirtuel/3', 1,    [3 3],  'VirtualSkewQuad_23'
24, 'ANS-C3/AE/QTvirtuel/4', 1,    [3 4],  'VirtualSkewQuad_24'
25, 'ANS-C3/AE/QTvirtuel/5', 1,    [3 5],  'VirtualSkewQuad_25'
26, 'ANS-C3/AE/QTvirtuel/6', 1,    [3 6],  'VirtualSkewQuad_26'
27, 'ANS-C3/AE/QTvirtuel/7', 1,    [3 7],  'VirtualSkewQuad_27'
28, 'ANS-C3/AE/QTvirtuel/8', 1,    [3 8],  'VirtualSkewQuad_28'
29, 'ANS-C3/AE/QTvirtuel/9', 1,    [3 9],  'VirtualSkewQuad_29'
30, 'ANS-C3/AE/QTvirtuel/10', 1,    [3 10],  'VirtualSkewQuad_30'


31, 'ANS-C4/AE/QTvirtuel/1', 1,    [4 1],  'VirtualSkewQuad_31'
32, 'ANS-C4/AE/QTvirtuel/2', 1,    [4 2],  'VirtualSkewQuad_32'
33, 'ANS-C4/AE/QTvirtuel/3', 1,    [4 3],  'VirtualSkewQuad_33'
34, 'ANS-C4/AE/QTvirtuel/4', 1,    [4 4],  'VirtualSkewQuad_34'
35, 'ANS-C4/AE/QTvirtuel/5', 1,    [4 5],  'VirtualSkewQuad_35'
36, 'ANS-C4/AE/QTvirtuel/6', 1,    [4 6],  'VirtualSkewQuad_36'
37, 'ANS-C4/AE/QTvirtuel/7', 1,    [4 7],  'VirtualSkewQuad_37'
38, 'ANS-C4/AE/QTvirtuel/8', 1,    [4 8],  'VirtualSkewQuad_38'


39, 'ANS-C5/AE/QTvirtuel/1', 1,    [5 1],  'VirtualSkewQuad_39'
40, 'ANS-C5/AE/QTvirtuel/2', 1,    [5 2],  'VirtualSkewQuad_40'
41, 'ANS-C5/AE/QTvirtuel/3', 1,    [5 3],  'VirtualSkewQuad_41'
42, 'ANS-C5/AE/QTvirtuel/4', 1,    [5 4],  'VirtualSkewQuad_42'
43, 'ANS-C5/AE/QTvirtuel/5', 1,    [5 5],  'VirtualSkewQuad_43'
44, 'ANS-C5/AE/QTvirtuel/6', 1,    [5 6],  'VirtualSkewQuad_44'
45, 'ANS-C5/AE/QTvirtuel/7', 1,    [5 7],  'VirtualSkewQuad_45'
46, 'ANS-C5/AE/QTvirtuel/8', 1,    [5 8],  'VirtualSkewQuad_46'
47, 'ANS-C5/AE/QTvirtuel/9', 1,    [5 9],  'VirtualSkewQuad_47'
48, 'ANS-C5/AE/QTvirtuel/10', 1,    [5 10],  'VirtualSkewQuad_48'


49, 'ANS-C6/AE/QTvirtuel/1', 1,    [6 1],  'VirtualSkewQuad_49'
50, 'ANS-C6/AE/QTvirtuel/2', 1,    [6 2],  'VirtualSkewQuad_50'
51, 'ANS-C6/AE/QTvirtuel/3', 1,    [6 3],  'VirtualSkewQuad_51'
52, 'ANS-C6/AE/QTvirtuel/4', 1,    [6 4],  'VirtualSkewQuad_52'
53, 'ANS-C6/AE/QTvirtuel/5', 1,    [6 5],  'VirtualSkewQuad_53'
54, 'ANS-C6/AE/QTvirtuel/6', 1,    [6 6],  'VirtualSkewQuad_54'
55, 'ANS-C6/AE/QTvirtuel/7', 1,    [6 7],  'VirtualSkewQuad_55'
56, 'ANS-C6/AE/QTvirtuel/8', 1,    [6 8],  'VirtualSkewQuad_56'
57, 'ANS-C6/AE/QTvirtuel/9', 1,    [6 9],  'VirtualSkewQuad_57'
58, 'ANS-C6/AE/QTvirtuel/10', 1,    [6 10],  'VirtualSkewQuad_58'


59, 'ANS-C7/AE/QTvirtuel/1', 1,    [7 1],  'VirtualSkewQuad_59'
60, 'ANS-C7/AE/QTvirtuel/2', 1,    [7 2],  'VirtualSkewQuad_60'
61, 'ANS-C7/AE/QTvirtuel/3', 1,    [7 3],  'VirtualSkewQuad_61'
62, 'ANS-C7/AE/QTvirtuel/4', 1,    [7 4],  'VirtualSkewQuad_62'
63, 'ANS-C7/AE/QTvirtuel/5', 1,    [7 5],  'VirtualSkewQuad_63'
64, 'ANS-C7/AE/QTvirtuel/6', 1,    [7 6],  'VirtualSkewQuad_64'
65, 'ANS-C7/AE/QTvirtuel/7', 1,    [7 7],  'VirtualSkewQuad_65'
66, 'ANS-C7/AE/QTvirtuel/8', 1,    [7 8],  'VirtualSkewQuad_66'
67, 'ANS-C7/AE/QTvirtuel/9', 1,    [7 9],  'VirtualSkewQuad_67'
68, 'ANS-C7/AE/QTvirtuel/10', 1,    [7 10],  'VirtualSkewQuad_68'


69, 'ANS-C8/AE/QTvirtuel/1', 1,    [8 1],  'VirtualSkewQuad_69'
70, 'ANS-C8/AE/QTvirtuel/2', 1,    [8 2],  'VirtualSkewQuad_70'
71, 'ANS-C8/AE/QTvirtuel/3', 1,    [8 3],  'VirtualSkewQuad_71'
72, 'ANS-C8/AE/QTvirtuel/4', 1,    [8 4],  'VirtualSkewQuad_72'
73, 'ANS-C8/AE/QTvirtuel/5', 1,    [8 5],  'VirtualSkewQuad_73'
74, 'ANS-C8/AE/QTvirtuel/6', 1,    [8 6],  'VirtualSkewQuad_74'
75, 'ANS-C8/AE/QTvirtuel/7', 1,    [8 7],  'VirtualSkewQuad_75'
76, 'ANS-C8/AE/QTvirtuel/8', 1,    [8 8],  'VirtualSkewQuad_76'


77, 'ANS-C9/AE/QTvirtuel/1', 1,    [9 1],  'VirtualSkewQuad_77'
78, 'ANS-C9/AE/QTvirtuel/2', 1,    [9 2],  'VirtualSkewQuad_78'
79, 'ANS-C9/AE/QTvirtuel/3', 1,    [9 3],  'VirtualSkewQuad_79'
80, 'ANS-C9/AE/QTvirtuel/4', 1,    [9 4],  'VirtualSkewQuad_80'
81, 'ANS-C9/AE/QTvirtuel/5', 1,    [9 5],  'VirtualSkewQuad_81'
82, 'ANS-C9/AE/QTvirtuel/6', 1,    [9 6],  'VirtualSkewQuad_82'
83, 'ANS-C9/AE/QTvirtuel/7', 1,    [9 7],  'VirtualSkewQuad_83'
84, 'ANS-C9/AE/QTvirtuel/8', 1,    [9 8],  'VirtualSkewQuad_84'
85, 'ANS-C9/AE/QTvirtuel/9', 1,    [9 9],  'VirtualSkewQuad_85'
86, 'ANS-C9/AE/QTvirtuel/10', 1,    [9 10],  'VirtualSkewQuad_86'


87, 'ANS-C10/AE/QTvirtuel/1', 1,    [10 1],  'VirtualSkewQuad_87'
88, 'ANS-C10/AE/QTvirtuel/2', 1,    [10 2],  'VirtualSkewQuad_88'
89, 'ANS-C10/AE/QTvirtuel/3', 1,    [10 3],  'VirtualSkewQuad_89'
90, 'ANS-C10/AE/QTvirtuel/4', 1,    [10 4],  'VirtualSkewQuad_90'
91, 'ANS-C10/AE/QTvirtuel/5', 1,    [10 5],  'VirtualSkewQuad_91'
92, 'ANS-C10/AE/QTvirtuel/6', 1,    [10 6],  'VirtualSkewQuad_92'
93, 'ANS-C10/AE/QTvirtuel/7', 1,    [10 7],  'VirtualSkewQuad_93'
94, 'ANS-C10/AE/QTvirtuel/8', 1,    [10 8],  'VirtualSkewQuad_94'
95, 'ANS-C10/AE/QTvirtuel/9', 1,    [10 9],  'VirtualSkewQuad_95'
96, 'ANS-C10/AE/QTvirtuel/10', 1,    [10 10],  'VirtualSkewQuad_96'


97, 'ANS-C11/AE/QTvirtuel/1', 1,    [11 1],  'VirtualSkewQuad_97'
98, 'ANS-C11/AE/QTvirtuel/2', 1,    [11 2],  'VirtualSkewQuad_98'
99, 'ANS-C11/AE/QTvirtuel/3', 1,    [11 3],  'VirtualSkewQuad_99'
100, 'ANS-C11/AE/QTvirtuel/4', 1,    [11 4],  'VirtualSkewQuad_100'
101, 'ANS-C11/AE/QTvirtuel/5', 1,    [11 5],  'VirtualSkewQuad_101'
102, 'ANS-C11/AE/QTvirtuel/6', 1,    [11 6],  'VirtualSkewQuad_102'
103, 'ANS-C11/AE/QTvirtuel/7', 1,    [11 7],  'VirtualSkewQuad_103'
104, 'ANS-C11/AE/QTvirtuel/8', 1,    [11 8],  'VirtualSkewQuad_104'
105, 'ANS-C11/AE/QTvirtuel/9', 1,    [11 9],  'VirtualSkewQuad_105'
106, 'ANS-C11/AE/QTvirtuel/10', 1,    [11 10],  'VirtualSkewQuad_106'


107, 'ANS-C12/AE/QTvirtuel/1', 1,    [12 1],  'VirtualSkewQuad_107'
108, 'ANS-C12/AE/QTvirtuel/2', 1,    [12 2],  'VirtualSkewQuad_108'
109, 'ANS-C12/AE/QTvirtuel/3', 1,    [12 3],  'VirtualSkewQuad_109'
110, 'ANS-C12/AE/QTvirtuel/4', 1,    [12 4],  'VirtualSkewQuad_110'
111, 'ANS-C12/AE/QTvirtuel/5', 1,    [12 5],  'VirtualSkewQuad_111'
112, 'ANS-C12/AE/QTvirtuel/6', 1,    [12 6],  'VirtualSkewQuad_112'
113, 'ANS-C12/AE/QTvirtuel/7', 1,    [12 7],  'VirtualSkewQuad_113'
114, 'ANS-C12/AE/QTvirtuel/8', 1,    [12 8],  'VirtualSkewQuad_114'

115, 'ANS-C13/AE/QTvirtuel/1', 1,    [13 1],  'VirtualSkewQuad_115'
116, 'ANS-C13/AE/QTvirtuel/2', 1,    [13 2],  'VirtualSkewQuad_116'
117, 'ANS-C13/AE/QTvirtuel/3', 1,    [13 3],  'VirtualSkewQuad_117'
118, 'ANS-C13/AE/QTvirtuel/4', 1,    [13 4],  'VirtualSkewQuad_118'
119, 'ANS-C13/AE/QTvirtuel/5', 1,    [13 5],  'VirtualSkewQuad_119'
120, 'ANS-C13/AE/QTvirtuel/6', 1,    [13 6],  'VirtualSkewQuad_120'
121, 'ANS-C13/AE/QTvirtuel/7', 1,    [13 7],  'VirtualSkewQuad_121'
122, 'ANS-C13/AE/QTvirtuel/8', 1,    [13 8],  'VirtualSkewQuad_122'
123, 'ANS-C13/AE/QTvirtuel/9', 1,    [13 9],  'VirtualSkewQuad_123'
124, 'ANS-C13/AE/QTvirtuel/10', 1,    [13 10],  'VirtualSkewQuad_124'

125, 'ANS-C14/AE/QTvirtuel/1', 1,    [14 1],  'VirtualSkewQuad_125'
126, 'ANS-C14/AE/QTvirtuel/2', 1,    [14 2],  'VirtualSkewQuad_126'
127, 'ANS-C14/AE/QTvirtuel/3', 1,    [14 3],  'VirtualSkewQuad_127'
128, 'ANS-C14/AE/QTvirtuel/4', 1,    [14 4],  'VirtualSkewQuad_128'
129, 'ANS-C14/AE/QTvirtuel/5', 1,    [14 5],  'VirtualSkewQuad_129'
130, 'ANS-C14/AE/QTvirtuel/6', 1,    [14 6],  'VirtualSkewQuad_130'
131, 'ANS-C14/AE/QTvirtuel/7', 1,    [14 7],  'VirtualSkewQuad_131'
132, 'ANS-C14/AE/QTvirtuel/8', 1,    [14 8],  'VirtualSkewQuad_132'
133, 'ANS-C14/AE/QTvirtuel/9', 1,    [14 9],  'VirtualSkewQuad_133'
134, 'ANS-C14/AE/QTvirtuel/10', 1,    [14 10],  'VirtualSkewQuad_134'


135, 'ANS-C15/AE/QTvirtuel/1', 1,    [15 1],  'VirtualSkewQuad_135'
136, 'ANS-C15/AE/QTvirtuel/2', 1,    [15 2],  'VirtualSkewQuad_136'
137, 'ANS-C15/AE/QTvirtuel/3', 1,    [15 3],  'VirtualSkewQuad_137'
138, 'ANS-C15/AE/QTvirtuel/4', 1,    [15 4],  'VirtualSkewQuad_138'
139, 'ANS-C15/AE/QTvirtuel/5', 1,    [15 5],  'VirtualSkewQuad_139'
140, 'ANS-C15/AE/QTvirtuel/6', 1,    [15 6],  'VirtualSkewQuad_140'
141, 'ANS-C15/AE/QTvirtuel/7', 1,    [15 7],  'VirtualSkewQuad_141'
142, 'ANS-C15/AE/QTvirtuel/8', 1,    [15 8],  'VirtualSkewQuad_142'
143, 'ANS-C15/AE/QTvirtuel/9', 1,    [15 9],  'VirtualSkewQuad_143'
144, 'ANS-C15/AE/QTvirtuel/10', 1,    [15 10],  'VirtualSkewQuad_144'


145, 'ANS-C16/AE/QTvirtuel/1', 1,    [16 1],  'VirtualSkewQuad_145'
146, 'ANS-C16/AE/QTvirtuel/2', 1,    [16 2],  'VirtualSkewQuad_146'
147, 'ANS-C16/AE/QTvirtuel/3', 1,    [16 3],  'VirtualSkewQuad_147'
148, 'ANS-C16/AE/QTvirtuel/4', 1,    [16 4],  'VirtualSkewQuad_148'
149, 'ANS-C16/AE/QTvirtuel/5', 1,    [16 5],  'VirtualSkewQuad_149'
150, 'ANS-C16/AE/QTvirtuel/6', 1,    [16 6],  'VirtualSkewQuad_150'
151, 'ANS-C16/AE/QTvirtuel/7', 1,    [16 7],  'VirtualSkewQuad_151'
152, 'ANS-C16/AE/QTvirtuel/8', 1,    [16 8],  'VirtualSkewQuad_152'
};

% Load coeeficients fot thin element
coefficients = magnetcoefficients(AO.(ifam).FamilyName);
%[C, Leff, MagnetType, coefficients] = magnetcoefficients(AO.(ifam).FamilyName);
for ii = 1:size(skewquadrupoles,1)
    AO.(ifam).ElementList(ii,:)            = skewquadrupoles{ii,1};
    AO.(ifam).DeviceName(ii,:)            = skewquadrupoles(ii,2);
    AO.(ifam).Monitor.TangoNames(ii,:)    = strcat(skewquadrupoles(ii,2),'/current');
    AO.(ifam).Setpoint.TangoNames(ii,:)    = strcat(skewquadrupoles(ii,2),'/currentPM');
    AO.(ifam).Status(ii,:)            = skewquadrupoles{ii,3};
    AO.(ifam).DeviceList(ii,:)            = skewquadrupoles{ii,4};
    AO.(ifam).CommonName(ii,:)            = skewquadrupoles(ii,5);
    AO.(ifam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(ifam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(ifam).Setpoint.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(ifam).Setpoint.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(ifam).Setpoint.Range(ii,:) = repmat([-7 7],1,1); % 7 A for
    AO.(ifam).Monitor.Range(ii,:) = repmat([-7 7],1,1);
    AO.(ifam).Setpoint.Tolerance(ii,:) = 1000;
    AO.(ifam).Setpoint.DeltaRespMat(ii,:) = 3*ones(1,1);
    AO.(ifam).Monitor.Handles(ii,1)    = NaN*ones(1,1);
    AO.(ifam).Setpoint.Handles(ii,1)    = NaN*ones(1,1);
    
end
AO.(ifam).Status = AO.(ifam).Status(:);

%convert response matrix kicks to HWUnits (after AO is loaded to AppData)
setao(AO);   %required to make physics2hw function
AO.(ifam).Setpoint.DeltaRespMat = physics2hw(AO.(ifam).FamilyName, 'Setpoint', ...
    AO.(ifam).Setpoint.DeltaRespMat, AO.(ifam).DeviceList);
AO.(ifam).Setpoint.DeltaSkewK = 1; % for efficiency measurement.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FAST HORIZONTAL CORRECTORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ifam = 'FHCOR';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'FCOR';
AO.(ifam).MemberOf                 = {'COR'; 'FCOR'; 'Magnet'; 'PlotFamily'; 'Archivable'};

AO.(ifam).Monitor.Mode              = Mode;
AO.(ifam).Monitor.DataType          = 'Scalar';
AO.(ifam).Monitor.Units             = 'Hardware';
AO.(ifam).Monitor.HWUnits           = 'A';
AO.(ifam).Monitor.PhysicsUnits      = 'rad';
AO.(ifam).Monitor.HW2PhysicsFcn     = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn     = @k2amp;

AO.(ifam).Setpoint.Mode             = Mode;
AO.(ifam).Setpoint.DataType         = 'Scalar';
AO.(ifam).Setpoint.Units            = 'Hardware';
AO.(ifam).Setpoint.HWUnits          = 'A';
AO.(ifam).Setpoint.PhysicsUnits     = 'rad';
AO.(ifam).Setpoint.HW2PhysicsFcn    = @amp2k;
AO.(ifam).Setpoint.Physics2HWFcn    = @k2amp;

AO.(ifam).SetpointMean.Mode         = Mode;
AO.(ifam).SetpointMean.DataType         = 'Scalar';
AO.(ifam).SetpointMean.Units            = 'Hardware';
AO.(ifam).SetpointMean.HWUnits          = 'A';
AO.(ifam).SetpointMean.PhysicsUnits     = 'rad';
AO.(ifam).SetpointMean.HW2PhysicsFcn    = @amp2k;
AO.(ifam).SetpointMean.Physics2HWFcn    = @k2amp;

varlist = {
     1  [ 1 2] 'ANS-C01/DG/CH.2          ' 1 'FHCOR02' 'current   ' 'current   ' [-10  10]
     2  [ 2 1] 'ANS-C02/DG/CH.1          ' 1 'FHCOR03' 'current   ' 'current   ' [-10  10]
     3  [ 2 2] 'ANS-C02/DG/CH.2          ' 1 'FHCOR04' 'current   ' 'current   ' [-10  10]
     4  [ 2 3] 'ANS-C02/DG/CH.3          ' 1 'FHCOR05' 'current   ' 'current   ' [-10  10]
     5  [ 2 4] 'ANS-C02/DG/CH.4          ' 1 'FHCOR06' 'current   ' 'current   ' [-10  10]
     6  [ 3 1] 'ANS-C03/DG/CH.1          ' 1 'FHCOR07' 'current   ' 'current   ' [-10  10]
     7  [ 3 2] 'ANS-C03/DG/CH.2          ' 1 'FHCOR08' 'current   ' 'current   ' [-10  10]
     8  [ 3 3] 'ANS-C03/DG/CH.3          ' 1 'FHCOR09' 'current   ' 'current   ' [-10  10]
     9  [ 3 4] 'ANS-C03/DG/CH.4          ' 1 'FHCOR10' 'current   ' 'current   ' [-10  10]
    10  [ 4 1] 'ANS-C04/DG/CH.1          ' 1 'FHCOR11' 'current   ' 'current   ' [-10  10]
    11  [ 4 2] 'ANS-C04/DG/CH.2          ' 1 'FHCOR12' 'current   ' 'current   ' [-10  10]
    12  [ 5 1] 'ANS-C05/DG/CH.1          ' 1 'FHCOR13' 'current   ' 'current   ' [-10  10]
    13  [ 5 2] 'ANS-C05/DG/CH.2          ' 1 'FHCOR14' 'current   ' 'current   ' [-10  10]
    14  [ 6 1] 'ANS-C06/DG/CH.1          ' 1 'FHCOR15' 'current   ' 'current   ' [-10  10]
    15  [ 6 2] 'ANS-C06/DG/CH.2          ' 1 'FHCOR16' 'current   ' 'current   ' [-10  10]
    16  [ 6 3] 'ANS-C06/DG/CH.3          ' 1 'FHCOR17' 'current   ' 'current   ' [-10  10]
    17  [ 6 4] 'ANS-C06/DG/CH.4          ' 1 'FHCOR18' 'current   ' 'current   ' [-10  10]
    18  [ 7 1] 'ANS-C07/DG/CH.1          ' 1 'FHCOR19' 'current   ' 'current   ' [-10  10]
    19  [ 7 2] 'ANS-C07/DG/CH.2          ' 1 'FHCOR20' 'current   ' 'current   ' [-10  10]
    20  [ 7 3] 'ANS-C07/DG/CH.3          ' 1 'FHCOR21' 'current   ' 'current   ' [-10  10]
    21  [ 7 4] 'ANS-C07/DG/CH.4          ' 1 'FHCOR22' 'current   ' 'current   ' [-10  10]
    22  [ 8 1] 'ANS-C08/DG/CH.1          ' 1 'FHCOR23' 'current   ' 'current   ' [-10  10]
    23  [ 8 2] 'ANS-C08/DG/CH.2          ' 1 'FHCOR24' 'current   ' 'current   ' [-10  10]
    24  [ 9 1] 'ANS-C09/DG/CH.1          ' 1 'FHCOR25' 'current   ' 'current   ' [-10  10]
    25  [ 9 2] 'ANS-C09/DG/CH.2          ' 1 'FHCOR26' 'current   ' 'current   ' [-10  10]
    26  [10 1] 'ANS-C10/DG/CH.1          ' 1 'FHCOR27' 'current   ' 'current   ' [-10  10]
    27  [10 2] 'ANS-C10/DG/CH.2          ' 1 'FHCOR28' 'current   ' 'current   ' [-10  10]
    28  [10 3] 'ANS-C10/DG/CH.3          ' 1 'FHCOR29' 'current   ' 'current   ' [-10  10]
    29  [10 4] 'ANS-C10/DG/CH.4          ' 1 'FHCOR30' 'current   ' 'current   ' [-10  10]
    30  [11 1] 'ANS-C11/DG/CH.1          ' 1 'FHCOR31' 'current   ' 'current   ' [-10  10]
    31  [11 2] 'ANS-C11/DG/CH.2          ' 1 'FHCOR32' 'current   ' 'current   ' [-10  10]
    32  [11 3] 'ANS-C11/DG/CH.3          ' 1 'FHCOR33' 'current   ' 'current   ' [-10  10]
    33  [11 4] 'ANS-C11/DG/CH.4          ' 1 'FHCOR34' 'current   ' 'current   ' [-10  10]
    34  [12 1] 'ANS-C12/DG/CH.1          ' 1 'FHCOR35' 'current   ' 'current   ' [-10  10]
    35  [12 2] 'ANS-C12/DG/CH.2          ' 1 'FHCOR36' 'current   ' 'current   ' [-10  10]
    36  [13 1] 'ANS-C13/DG/CH.1          ' 1 'FHCOR37' 'current   ' 'current   ' [-10  10]
    37  [13 2] 'ANS-C13/DG/CH.2          ' 1 'FHCOR38' 'current   ' 'current   ' [-10  10]
    38  [14 1] 'ANS-C14/DG/CH.1          ' 1 'FHCOR39' 'current   ' 'current   ' [-10  10]
    39  [14 2] 'ANS-C14/DG/CH.2          ' 1 'FHCOR40' 'current   ' 'current   ' [-10  10]
    40  [14 3] 'ANS-C14/DG/CH.3          ' 1 'FHCOR41' 'current   ' 'current   ' [-10  10]
    41  [14 4] 'ANS-C14/DG/CH.4          ' 1 'FHCOR42' 'current   ' 'current   ' [-10  10]
    42  [15 1] 'ANS-C15/DG/CH.1          ' 1 'FHCOR43' 'current   ' 'current   ' [-10  10]
    43  [15 2] 'ANS-C15/DG/CH.2          ' 1 'FHCOR44' 'current   ' 'current   ' [-10  10]
    44  [15 3] 'ANS-C15/DG/CH.3          ' 1 'FHCOR45' 'current   ' 'current   ' [-10  10]
    45  [15 4] 'ANS-C15/DG/CH.4          ' 1 'FHCOR46' 'current   ' 'current   ' [-10  10]
    46  [16 1] 'ANS-C16/DG/CH.1          ' 1 'FHCOR47' 'current   ' 'current   ' [-10  10]
    47  [16 2] 'ANS-C16/DG/CH.2          ' 1 'FHCOR48' 'current   ' 'current   ' [-10  10]
    48  [ 1 1] 'ANS-C01/DG/CH.1          ' 1 'FHCOR01' 'current   ' 'current   ' [-10  10]
    };

devnumber = length(varlist);
% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Setpoint.TangoNames = cell(devnumber,1);
AO.(ifam).Setpoint.Range = zeros(devnumber,2);
AO.(ifam).SetpointMean.TangoNames = cell(devnumber,1);
AO.(ifam).SetpointMean.Range = zeros(devnumber,2);

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames(k)  = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,6)));
    AO.(ifam).Setpoint.TangoNames(k) = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,7)));
    AO.(ifam).Setpoint.Range(k,:)      = varlist{k,8};
    AO.(ifam).SetpointMean.TangoNames(k) = strcat(AO.(ifam).DeviceName{k}, {'/spMean'});
    AO.(ifam).SetpointMean.Range(k,:)   = varlist{k,8};
end

%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients(AO.(ifam).FamilyName);

for ii = 1:devnumber,
    AO.(ifam).Monitor.HW2PhysicsParams{1}(ii,:)   = coefficients;
    AO.(ifam).Monitor.Physics2HWParams{1}(ii,:)   = coefficients;
    AO.(ifam).Setpoint.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(ifam).Setpoint.Physics2HWParams{1}(ii,:)  = coefficients;
end


AO.(ifam).Monitor.MemberOf      = {'PlotFamily'};
AO.(ifam).Setpoint.MemberOf     = {'PlotFamily'};
AO.(ifam).Setpoint.Tolerance(:,:)    = 1e-1*ones(devnumber,1);
%AO.(ifam).Setpoint.DeltaRespMat(:,:) = 24e-6*ones(devnumber,1); % 2*12 �urad for 0.2 mm
AO.(ifam).Setpoint.DeltaRespMat(:,:) = 6e-6*ones(devnumber,1); % 2*3 �urad for 0.05 mm

%convert response matrix kicks to HWUnits (after AO is loaded to AppData)
setao(AO);   %required to make physics2hw function
AO.(ifam).Setpoint.DeltaRespMat = physics2hw(AO.(ifam).FamilyName,'Setpoint', ...
    AO.(ifam).Setpoint.DeltaRespMat, AO.(ifam).DeviceList);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FAST VERTICAL CORRECTORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ifam = 'FVCOR';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'FCOR';
AO.(ifam).MemberOf                 = {'COR'; 'FCOR'; 'Magnet'; 'PlotFamily'; 'Archivable'};

AO.(ifam).Monitor.Mode              = Mode;
AO.(ifam).Monitor.DataType          = 'Scalar';
AO.(ifam).Monitor.Units             = 'Hardware';
AO.(ifam).Monitor.HWUnits           = 'A';
AO.(ifam).Monitor.PhysicsUnits      = 'rad';
AO.(ifam).Monitor.HW2PhysicsFcn     = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn     = @k2amp;

AO.(ifam).Setpoint.Mode             = Mode;
AO.(ifam).Setpoint.DataType         = 'Scalar';
AO.(ifam).Setpoint.Units            = 'Hardware';
AO.(ifam).Setpoint.HWUnits          = 'A';
AO.(ifam).Setpoint.PhysicsUnits     = 'rad';
AO.(ifam).Setpoint.HW2PhysicsFcn    = @amp2k;
AO.(ifam).Setpoint.Physics2HWFcn    = @k2amp;

AO.(ifam).SetpointMean.Mode         = Mode;
AO.(ifam).SetpointMean.DataType         = 'Scalar';
AO.(ifam).SetpointMean.Units            = 'Hardware';
AO.(ifam).SetpointMean.HWUnits          = 'A';
AO.(ifam).SetpointMean.PhysicsUnits     = 'rad';
AO.(ifam).SetpointMean.HW2PhysicsFcn    = @amp2k;
AO.(ifam).SetpointMean.Physics2HWFcn    = @k2amp;


% devliste tangoname status common attR attW range
varlist = {
     1  [ 1 2] 'ANS-C01/DG/CV.2          ' 1 'FVCOR01' 'current   ' 'current   ' [-10  10]
     2  [ 2 1] 'ANS-C02/DG/CV.1          ' 1 'FVCOR02' 'current   ' 'current   ' [-10  10]
     3  [ 2 2] 'ANS-C02/DG/CV.2          ' 1 'FVCOR03' 'current   ' 'current   ' [-10  10]
     4  [ 2 3] 'ANS-C02/DG/CV.3          ' 1 'FVCOR04' 'current   ' 'current   ' [-10  10]
     5  [ 2 4] 'ANS-C02/DG/CV.4          ' 1 'FVCOR05' 'current   ' 'current   ' [-10  10]
     6  [ 3 1] 'ANS-C03/DG/CV.1          ' 1 'FVCOR06' 'current   ' 'current   ' [-10  10]
     7  [ 3 2] 'ANS-C03/DG/CV.2          ' 1 'FVCOR07' 'current   ' 'current   ' [-10  10]
     8  [ 3 3] 'ANS-C03/DG/CV.3          ' 1 'FVCOR08' 'current   ' 'current   ' [-10  10]
     9  [ 3 4] 'ANS-C03/DG/CV.4          ' 1 'FVCOR09' 'current   ' 'current   ' [-10  10]
    10  [ 4 1] 'ANS-C04/DG/CV.1          ' 1 'FVCOR10' 'current   ' 'current   ' [-10  10]
    11  [ 4 2] 'ANS-C04/DG/CV.2          ' 1 'FVCOR11' 'current   ' 'current   ' [-10  10]
    12  [ 5 1] 'ANS-C05/DG/CV.1          ' 1 'FVCOR12' 'current   ' 'current   ' [-10  10]
    13  [ 5 2] 'ANS-C05/DG/CV.2          ' 1 'FVCOR13' 'current   ' 'current   ' [-10  10]
    14  [ 6 1] 'ANS-C06/DG/CV.1          ' 1 'FVCOR14' 'current   ' 'current   ' [-10  10]
    15  [ 6 2] 'ANS-C06/DG/CV.2          ' 1 'FVCOR15' 'current   ' 'current   ' [-10  10]
    16  [ 6 3] 'ANS-C06/DG/CV.3          ' 1 'FVCOR16' 'current   ' 'current   ' [-10  10]
    17  [ 6 4] 'ANS-C06/DG/CV.4          ' 1 'FVCOR17' 'current   ' 'current   ' [-10  10]
    18  [ 7 1] 'ANS-C07/DG/CV.1          ' 1 'FVCOR18' 'current   ' 'current   ' [-10  10]
    19  [ 7 2] 'ANS-C07/DG/CV.2          ' 1 'FVCOR19' 'current   ' 'current   ' [-10  10]
    20  [ 7 3] 'ANS-C07/DG/CV.3          ' 1 'FVCOR20' 'current   ' 'current   ' [-10  10]
    21  [ 7 4] 'ANS-C07/DG/CV.4          ' 1 'FVCOR21' 'current   ' 'current   ' [-10  10]
    22  [ 8 1] 'ANS-C08/DG/CV.1          ' 1 'FVCOR22' 'current   ' 'current   ' [-10  10]
    23  [ 8 2] 'ANS-C08/DG/CV.2          ' 1 'FVCOR23' 'current   ' 'current   ' [-10  10]
    24  [ 9 1] 'ANS-C09/DG/CV.1          ' 1 'FVCOR24' 'current   ' 'current   ' [-10  10]
    25  [ 9 2] 'ANS-C09/DG/CV.2          ' 1 'FVCOR25' 'current   ' 'current   ' [-10  10]
    26  [10 1] 'ANS-C10/DG/CV.1          ' 1 'FVCOR26' 'current   ' 'current   ' [-10  10]
    27  [10 2] 'ANS-C10/DG/CV.2          ' 1 'FVCOR27' 'current   ' 'current   ' [-10  10]
    28  [10 3] 'ANS-C10/DG/CV.3          ' 1 'FVCOR28' 'current   ' 'current   ' [-10  10]
    29  [10 4] 'ANS-C10/DG/CV.4          ' 1 'FVCOR29' 'current   ' 'current   ' [-10  10]
    30  [11 1] 'ANS-C11/DG/CV.1          ' 1 'FVCOR30' 'current   ' 'current   ' [-10  10]
    31  [11 2] 'ANS-C11/DG/CV.2          ' 1 'FVCOR31' 'current   ' 'current   ' [-10  10]
    32  [11 3] 'ANS-C11/DG/CV.3          ' 1 'FVCOR32' 'current   ' 'current   ' [-10  10]
    33  [11 4] 'ANS-C11/DG/CV.4          ' 1 'FVCOR33' 'current   ' 'current   ' [-10  10]
    34  [12 1] 'ANS-C12/DG/CV.1          ' 1 'FVCOR34' 'current   ' 'current   ' [-10  10]
    35  [12 2] 'ANS-C12/DG/CV.2          ' 1 'FVCOR35' 'current   ' 'current   ' [-10  10]
    36  [13 1] 'ANS-C13/DG/CV.1          ' 1 'FVCOR36' 'current   ' 'current   ' [-10  10]
    37  [13 2] 'ANS-C13/DG/CV.2          ' 1 'FVCOR37' 'current   ' 'current   ' [-10  10]
    38  [14 1] 'ANS-C14/DG/CV.1          ' 1 'FVCOR38' 'current   ' 'current   ' [-10  10]
    39  [14 2] 'ANS-C14/DG/CV.2          ' 1 'FVCOR39' 'current   ' 'current   ' [-10  10]
    40  [14 3] 'ANS-C14/DG/CV.3          ' 1 'FVCOR40' 'current   ' 'current   ' [-10  10]
    41  [14 4] 'ANS-C14/DG/CV.4          ' 1 'FVCOR41' 'current   ' 'current   ' [-10  10]
    42  [15 1] 'ANS-C15/DG/CV.1          ' 1 'FVCOR42' 'current   ' 'current   ' [-10  10]
    43  [15 2] 'ANS-C15/DG/CV.2          ' 1 'FVCOR43' 'current   ' 'current   ' [-10  10]
    44  [15 3] 'ANS-C15/DG/CV.3          ' 1 'FVCOR44' 'current   ' 'current   ' [-10  10]
    45  [15 4] 'ANS-C15/DG/CV.4          ' 1 'FVCOR45' 'current   ' 'current   ' [-10  10]
    46  [16 1] 'ANS-C16/DG/CV.1          ' 1 'FVCOR46' 'current   ' 'current   ' [-10  10]
    47  [16 2] 'ANS-C16/DG/CV.2          ' 1 'FVCOR47' 'current   ' 'current   ' [-10  10]
    48  [ 1 1] 'ANS-C01/DG/CV.1          ' 1 'FVCOR48' 'current   ' 'current   ' [-10  10]
    };

devnumber = length(varlist);
% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Setpoint.TangoNames = cell(devnumber,1);
AO.(ifam).Setpoint.Range = zeros(devnumber,2);
AO.(ifam).SetpointMean.TangoNames = cell(devnumber,1);
AO.(ifam).SetpointMean.Range = zeros(devnumber,2);

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames(k)  = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,6)));
    AO.(ifam).Setpoint.TangoNames(k) = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,7)));
    AO.(ifam).Setpoint.Range(k,:)      = varlist{k,8};
    AO.(ifam).SetpointMean.TangoNames(k) = strcat(AO.(ifam).DeviceName{k}, {'/spMean'});
    AO.(ifam).SetpointMean.Range(k,:)   = varlist{k,8};
end

%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients(AO.(ifam).FamilyName);

for ii = 1:devnumber,
    AO.(ifam).Monitor.HW2PhysicsParams{1}(ii,:)   = coefficients;
    AO.(ifam).Monitor.Physics2HWParams{1}(ii,:)   = coefficients;
    AO.(ifam).Setpoint.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(ifam).Setpoint.Physics2HWParams{1}(ii,:)  = coefficients;
end


AO.(ifam).Monitor.MemberOf      = {'PlotFamily'};
AO.(ifam).Setpoint.MemberOf     = {'PlotFamily'};
AO.(ifam).Setpoint.Tolerance(:,:)    = 1e-1*ones(devnumber,1);
%AO.(ifam).Setpoint.DeltaRespMat(:,:) = 24e-6*ones(devnumber,1); % 2*12 �urad
AO.(ifam).Setpoint.DeltaRespMat(:,:) = 6e-6*ones(devnumber,1); % 2*3 �urad

%convert response matrix kicks to HWUnits (after AO is loaded to AppData)
setao(AO);   %required to make physics2hw function
AO.(ifam).Setpoint.DeltaRespMat = physics2hw(AO.(ifam).FamilyName,'Setpoint', ...
    AO.(ifam).Setpoint.DeltaRespMat, AO.(ifam).DeviceList);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PX2 correctors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ifam = 'PX2C';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'COR';
AO.(ifam).MemberOf                 = {'COR'; 'FCOR'; 'Magnet'; 'PlotFamily'; 'Archivable'};

AO.(ifam).Monitor.Mode              = Mode;
AO.(ifam).Monitor.DataType          = 'Scalar';
AO.(ifam).Monitor.Units             = 'Hardware';
AO.(ifam).Monitor.HWUnits           = 'A';
AO.(ifam).Monitor.PhysicsUnits      = 'rad';
AO.(ifam).Monitor.HW2PhysicsParams(:,:) = [0.0218; 0.0225; 0.0216]*1e-3/getbrho(2.739);
AO.(ifam).Monitor.Physics2HWParams(:,:) = 1./AO.(ifam).Monitor.HW2PhysicsParams(:,:);

AO.(ifam).Setpoint.Mode             = Mode;
AO.(ifam).Setpoint.DataType         = 'Scalar';
AO.(ifam).Setpoint.Units            = 'Hardware';
AO.(ifam).Setpoint.HWUnits          = 'A';
AO.(ifam).Setpoint.PhysicsUnits     = 'rad';
AO.(ifam).Setpoint.HW2PhysicsParams(:,:) = AO.(ifam).Monitor.HW2PhysicsParams(:,:);
AO.(ifam).Setpoint.Physics2HWParams(:,:) = AO.(ifam).Monitor.Physics2HWParams(:,:);

% devliste tangoname status common attR attW range
varlist = {
    1  [ 11 1] 'ANS-C11/AE/PX2-D.1' 1 'PX2C01' 'current' 'current' [-10  10]
    2  [ 11 2] 'ANS-C11/AE/PX2-D.2' 1 'PX2C02' 'current' 'current' [-10  10]
    3  [ 11 3] 'ANS-C11/AE/PX2-D.3' 1 'PX2C03' 'current' 'current' [-10  10]
    };

devnumber = size(varlist,1);
% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Setpoint.TangoNames = cell(devnumber,1);
AO.(ifam).Setpoint.Range = zeros(devnumber,2);

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames(k)  = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,6)));
    AO.(ifam).Setpoint.TangoNames(k) = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,7)));
    AO.(ifam).Setpoint.Range(k,:)      = varlist{k,8};
end

AO.(ifam).Monitor.MemberOf      = {'PlotFamily'};
AO.(ifam).Setpoint.MemberOf     = {'PlotFamily'};
AO.(ifam).Setpoint.Tolerance(:,:)    = 1e-1*ones(devnumber,1);
AO.(ifam).Setpoint.DeltaRespMat(:,:) = 8e-6*ones(devnumber,1); % 2*2 �urad

%convert response matrix kicks to HWUnits (after AO is loaded to AppData)
setao(AO);   %required to make physics2hw function
AO.(ifam).Setpoint.DeltaRespMat = physics2hw(AO.(ifam).FamilyName,'Setpoint', ...
    AO.(ifam).Setpoint.DeltaRespMat, AO.(ifam).DeviceList);

%=============================
%        MAIN MAGNETS
%=============================

%==============
%% DIPOLES
%==============

% *** BEND ***
ifam='BEND';
AO.(ifam).FamilyName                 = ifam;
AO.(ifam).FamilyType                 = 'BEND';
AO.(ifam).MemberOf                   = {'MachineConfig'; 'BEND'; 'Magnet'; 'PlotFamily'; 'Archivable'};
HW2PhysicsParams                    = magnetcoefficients('BEND');
Physics2HWParams                    = HW2PhysicsParams;

AO.(ifam).Monitor.Mode               = Mode;
AO.(ifam).Monitor.DataType           = 'Scalar';
AO.(ifam).Monitor.Units              = 'Hardware';
AO.(ifam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(ifam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(ifam).Monitor.HWUnits            = 'A';
AO.(ifam).Monitor.PhysicsUnits       = 'GeV';

AO.(ifam).DeviceName(:,:) = {'ANS/AE/Dipole'};
AO.(ifam).Monitor.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName(:,:),'/current');

AO.(ifam).DeviceList(:,:) = [1 1];
AO.(ifam).ElementList(:,:)= 1;
AO.(ifam).Status          = 1;

val = 1;
AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(ifam).Monitor.Range(:,:) = [0 560]; % 525 A for 1.71T

AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Desired  = AO.(ifam).Monitor;
AO.(ifam).Setpoint.MemberOf  = {'PlotFamily'};
AO.(ifam).Setpoint.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentPM');

AO.(ifam).Setpoint.Tolerance(:,:) = 0.05;
AO.(ifam).Setpoint.DeltaRespMat(:,:) = 0.05;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% QUADRUPOLE MAGNETS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear varlist;
varlist.Q1={
    1 [ 1  1] 'ANS-C01/AE/Q1      ' 1 ' Q1.1' [-250 +0]
    2 [ 4  1] 'ANS-C04/AE/Q1      ' 1 ' Q1.2' [-250 +0]
    3 [ 5  1] 'ANS-C05/AE/Q1      ' 1 ' Q1.3' [-250 +0]
    4 [ 8  1] 'ANS-C08/AE/Q1      ' 1 ' Q1.4' [-250 +0]
    5 [ 9  1] 'ANS-C09/AE/Q1      ' 1 ' Q1.5' [-250 +0]
    6 [12  1] 'ANS-C12/AE/Q1      ' 1 ' Q1.6' [-250 +0]
    7 [13  1] 'ANS-C13/AE/Q1      ' 1 ' Q1.7' [-250 +0]
    8 [16  1] 'ANS-C16/AE/Q1      ' 1 ' Q1.8' [-250 +0]
    };
varlist.Q2={
    1 [ 1  1] 'ANS-C01/AE/Q2      ' 1 ' Q2.1' [+0 +250]
    2 [ 4  1] 'ANS-C04/AE/Q2      ' 1 ' Q2.2' [+0 +250]
    3 [ 5  1] 'ANS-C05/AE/Q2      ' 1 ' Q2.3' [+0 +250]
    4 [ 8  1] 'ANS-C08/AE/Q2      ' 1 ' Q2.4' [+0 +250]
    5 [ 9  1] 'ANS-C09/AE/Q2      ' 1 ' Q2.5' [+0 +250]
    6 [12  1] 'ANS-C12/AE/Q2      ' 1 ' Q2.6' [+0 +250]
    7 [13  1] 'ANS-C13/AE/Q2      ' 1 ' Q2.7' [+0 +250]
    8 [16  1] 'ANS-C16/AE/Q2      ' 1 ' Q2.8' [+0 +250]
    };
varlist.Q3={
    1 [ 1  1] 'ANS-C01/AE/Q3      ' 1 ' Q3.1' [-250 +0]
    2 [ 4  1] 'ANS-C04/AE/Q3      ' 1 ' Q3.2' [-250 +0]
    3 [ 5  1] 'ANS-C05/AE/Q3      ' 1 ' Q3.3' [-250 +0]
    4 [ 8  1] 'ANS-C08/AE/Q3      ' 1 ' Q3.4' [-250 +0]
    5 [ 9  1] 'ANS-C09/AE/Q3      ' 1 ' Q3.5' [-250 +0]
    6 [12  1] 'ANS-C12/AE/Q3      ' 1 ' Q3.6' [-250 +0]
    7 [13  1] 'ANS-C13/AE/Q3      ' 1 ' Q3.7' [-250 +0]
    8 [16  1] 'ANS-C16/AE/Q3      ' 1 ' Q3.8' [-250 +0]
    };
varlist.Q4={
     1 [ 1  1] 'ANS-C01/AE/Q4.1    ' 1 'Q4.01' [-250 +0]
     2 [ 1  2] 'ANS-C01/AE/Q4.2    ' 1 'Q4.02' [-250 +0]
     3 [ 4  1] 'ANS-C04/AE/Q4.1    ' 1 'Q4.03' [-250 +0]
     4 [ 4  2] 'ANS-C04/AE/Q4.2    ' 1 'Q4.04' [-250 +0]
     5 [ 5  1] 'ANS-C05/AE/Q4.1    ' 1 'Q4.05' [-250 +0]
     6 [ 5  2] 'ANS-C05/AE/Q4.2    ' 1 'Q4.06' [-250 +0]
     7 [ 8  1] 'ANS-C08/AE/Q4.1    ' 1 'Q4.07' [-250 +0]
     8 [ 8  2] 'ANS-C08/AE/Q4.2    ' 1 'Q4.08' [-250 +0]
     9 [ 9  1] 'ANS-C09/AE/Q4.1    ' 1 'Q4.09' [-250 +0]
    10 [ 9  2] 'ANS-C09/AE/Q4.2    ' 1 'Q4.10' [-250 +0]
    11 [12  1] 'ANS-C12/AE/Q4.1    ' 1 'Q4.11' [-250 +0]
    12 [12  2] 'ANS-C12/AE/Q4.2    ' 1 'Q4.12' [-250 +0]
    13 [13  1] 'ANS-C13/AE/Q4.1    ' 1 'Q4.13' [-250 +0]
    14 [13  2] 'ANS-C13/AE/Q4.2    ' 1 'Q4.14' [-250 +0]
    15 [16  1] 'ANS-C16/AE/Q4.1    ' 1 'Q4.15' [-250 +0]
    16 [16  2] 'ANS-C16/AE/Q4.2    ' 1 'Q4.16' [-250 +0]
    };
varlist.Q5={
     1 [ 1  1] 'ANS-C01/AE/Q5.1    ' 1 'Q5.01' [+0 +250]
     2 [ 1  2] 'ANS-C01/AE/Q5.2    ' 1 'Q5.02' [+0 +250]
     3 [ 4  1] 'ANS-C04/AE/Q5.1    ' 1 'Q5.03' [+0 +250]
     4 [ 4  2] 'ANS-C04/AE/Q5.2    ' 1 'Q5.04' [+0 +250]
     5 [ 5  1] 'ANS-C05/AE/Q5.1    ' 1 'Q5.05' [+0 +250]
     6 [ 5  2] 'ANS-C05/AE/Q5.2    ' 1 'Q5.06' [+0 +250]
     7 [ 8  1] 'ANS-C08/AE/Q5.1    ' 1 'Q5.07' [+0 +250]
     8 [ 8  2] 'ANS-C08/AE/Q5.2    ' 1 'Q5.08' [+0 +250]
     9 [ 9  1] 'ANS-C09/AE/Q5.1    ' 1 'Q5.09' [+0 +250]
    10 [ 9  2] 'ANS-C09/AE/Q5.2    ' 1 'Q5.10' [+0 +250]
    11 [12  1] 'ANS-C12/AE/Q5.1    ' 1 'Q5.11' [+0 +250]
    12 [12  2] 'ANS-C12/AE/Q5.2    ' 1 'Q5.12' [+0 +250]
    13 [13  1] 'ANS-C13/AE/Q5.1    ' 1 'Q5.13' [+0 +250]
    14 [13  2] 'ANS-C13/AE/Q5.2    ' 1 'Q5.14' [+0 +250]
    15 [16  1] 'ANS-C16/AE/Q5.1    ' 1 'Q5.15' [+0 +250]
    16 [16  2] 'ANS-C16/AE/Q5.2    ' 1 'Q5.16' [+0 +250]
    };
varlist.Q6={
     1 [ 1  1] 'ANS-C01/AE/Q6      ' 1 'Q6.01' [-250 +0]
     2 [ 2  1] 'ANS-C02/AE/Q6.1    ' 1 'Q6.02' [-250 +0]
     3 [ 2  2] 'ANS-C02/AE/Q6.2    ' 1 'Q6.03' [-250 +0]
     4 [ 3  1] 'ANS-C03/AE/Q6.1    ' 1 'Q6.04' [-250 +0]
     5 [ 3  2] 'ANS-C03/AE/Q6.2    ' 1 'Q6.05' [-250 +0]
     6 [ 4  1] 'ANS-C04/AE/Q6      ' 1 'Q6.06' [-250 +0]
     7 [ 5  1] 'ANS-C05/AE/Q6      ' 1 'Q6.07' [-250 +0]
     8 [ 6  1] 'ANS-C06/AE/Q6.1    ' 1 'Q6.08' [-250 +0]
     9 [ 6  2] 'ANS-C06/AE/Q6.2    ' 1 'Q6.09' [-250 +0]
    10 [ 7  1] 'ANS-C07/AE/Q6.1    ' 1 'Q6.10' [-250 +0]
    11 [ 7  2] 'ANS-C07/AE/Q6.2    ' 1 'Q6.11' [-250 +0]
    12 [ 8  1] 'ANS-C08/AE/Q6      ' 1 'Q6.12' [-250 +0]
    13 [ 9  1] 'ANS-C09/AE/Q6      ' 1 'Q6.13' [-250 +0]
    14 [10  1] 'ANS-C10/AE/Q6.1    ' 1 'Q6.14' [-250 +0]
    15 [10  2] 'ANS-C10/AE/Q6.2    ' 1 'Q6.15' [-250 +0]
    16 [11  1] 'ANS-C11/AE/Q6.1    ' 1 'Q6.16' [-250 +0]
    17 [11  2] 'ANS-C11/AE/Q6.2    ' 1 'Q6.17' [-250 +0]
    18 [12  1] 'ANS-C12/AE/Q6      ' 1 'Q6.18' [-250 +0]
    19 [13  1] 'ANS-C13/AE/Q6      ' 1 'Q6.19' [-250 +0]
    20 [14  1] 'ANS-C14/AE/Q6.1    ' 1 'Q6.20' [-250 +0]
    21 [14  2] 'ANS-C14/AE/Q6.2    ' 1 'Q6.21' [-250 +0]
    22 [15  1] 'ANS-C15/AE/Q6.1    ' 1 'Q6.22' [-250 +0]
    23 [15  2] 'ANS-C15/AE/Q6.2    ' 1 'Q6.23' [-250 +0]
    24 [16  1] 'ANS-C16/AE/Q6      ' 1 'Q6.24' [-250 +0]
    };
varlist.Q7={
     1 [ 1  1] 'ANS-C01/AE/Q7      ' 1 'Q7.01' [+0 +250]
     2 [ 2  1] 'ANS-C02/AE/Q7.1    ' 1 'Q7.02' [+0 +250]
     3 [ 2  2] 'ANS-C02/AE/Q7.2    ' 1 'Q7.03' [+0 +250]
     4 [ 3  1] 'ANS-C03/AE/Q7.1    ' 1 'Q7.04' [+0 +250]
     5 [ 3  2] 'ANS-C03/AE/Q7.2    ' 1 'Q7.05' [+0 +250]
     6 [ 4  1] 'ANS-C04/AE/Q7      ' 1 'Q7.06' [+0 +250]
     7 [ 5  1] 'ANS-C05/AE/Q7      ' 1 'Q7.07' [+0 +250]
     8 [ 6  1] 'ANS-C06/AE/Q7.1    ' 1 'Q7.08' [+0 +250]
     9 [ 6  2] 'ANS-C06/AE/Q7.2    ' 1 'Q7.09' [+0 +250]
    10 [ 7  1] 'ANS-C07/AE/Q7.1    ' 1 'Q7.10' [+0 +250]
    11 [ 7  2] 'ANS-C07/AE/Q7.2    ' 1 'Q7.11' [+0 +250]
    12 [ 8  1] 'ANS-C08/AE/Q7      ' 1 'Q7.12' [+0 +250]
    13 [ 9  1] 'ANS-C09/AE/Q7      ' 1 'Q7.13' [+0 +250]
    14 [10  1] 'ANS-C10/AE/Q7.1    ' 1 'Q7.14' [+0 +250]
    15 [10  2] 'ANS-C10/AE/Q7.2    ' 1 'Q7.15' [+0 +250]
    16 [11  1] 'ANS-C11/AE/Q7.1    ' 1 'Q7.16' [+0 +250]
    17 [11  2] 'ANS-C11/AE/Q7.2    ' 1 'Q7.17' [+0 +250]
    18 [12  1] 'ANS-C12/AE/Q7      ' 1 'Q7.18' [+0 +250]
    19 [13  1] 'ANS-C13/AE/Q7      ' 1 'Q7.19' [+0 +250]
    20 [14  1] 'ANS-C14/AE/Q7.1    ' 1 'Q7.20' [+0 +250]
    21 [14  2] 'ANS-C14/AE/Q7.2    ' 1 'Q7.21' [+0 +250]
    22 [15  1] 'ANS-C15/AE/Q7.1    ' 1 'Q7.22' [+0 +250]
    23 [15  2] 'ANS-C15/AE/Q7.2    ' 1 'Q7.23' [+0 +250]
    24 [16  1] 'ANS-C16/AE/Q7      ' 1 'Q7.24' [+0 +250]
    };
varlist.Q8={
     1 [ 1  1] 'ANS-C01/AE/Q8      ' 1 'Q8.01' [-250 +0]
     2 [ 2  1] 'ANS-C02/AE/Q8.1    ' 1 'Q8.02' [-250 +0]
     3 [ 2  2] 'ANS-C02/AE/Q8.2    ' 1 'Q8.03' [-250 +0]
     4 [ 3  1] 'ANS-C03/AE/Q8.1    ' 1 'Q8.04' [-250 +0]
     5 [ 3  2] 'ANS-C03/AE/Q8.2    ' 1 'Q8.05' [-250 +0]
     6 [ 4  1] 'ANS-C04/AE/Q8      ' 1 'Q8.06' [-250 +0]
     7 [ 5  1] 'ANS-C05/AE/Q8      ' 1 'Q8.07' [-250 +0]
     8 [ 6  1] 'ANS-C06/AE/Q8.1    ' 1 'Q8.08' [-250 +0]
     9 [ 6  2] 'ANS-C06/AE/Q8.2    ' 1 'Q8.09' [-250 +0]
    10 [ 7  1] 'ANS-C07/AE/Q8.1    ' 1 'Q8.10' [-250 +0]
    11 [ 7  2] 'ANS-C07/AE/Q8.2    ' 1 'Q8.11' [-250 +0]
    12 [ 8  1] 'ANS-C08/AE/Q8      ' 1 'Q8.12' [-250 +0]
    13 [ 9  1] 'ANS-C09/AE/Q8      ' 1 'Q8.13' [-250 +0]
    14 [10  1] 'ANS-C10/AE/Q8.1    ' 1 'Q8.14' [-250 +0]
    15 [10  2] 'ANS-C10/AE/Q8.2    ' 1 'Q8.15' [-250 +0]
    16 [11  1] 'ANS-C11/AE/Q8.1    ' 1 'Q8.16' [-250 +0]
    17 [11  2] 'ANS-C11/AE/Q8.2    ' 1 'Q8.17' [-250 +0]
    18 [12  1] 'ANS-C12/AE/Q8      ' 1 'Q8.18' [-250 +0]
    19 [13  1] 'ANS-C13/AE/Q8      ' 1 'Q8.19' [-250 +0]
    20 [14  1] 'ANS-C14/AE/Q8.1    ' 1 'Q8.20' [-250 +0]
    21 [14  2] 'ANS-C14/AE/Q8.2    ' 1 'Q8.21' [-250 +0]
    22 [15  1] 'ANS-C15/AE/Q8.1    ' 1 'Q8.22' [-250 +0]
    23 [15  2] 'ANS-C15/AE/Q8.2    ' 1 'Q8.23' [-250 +0]
    24 [16  1] 'ANS-C16/AE/Q8      ' 1 'Q8.24' [-250 +0]
    };
varlist.Q9={
     1 [ 2  1] 'ANS-C02/AE/Q9.1    ' 1 'Q9.01' [-250 +0]
     2 [ 2  2] 'ANS-C02/AE/Q9.2    ' 1 'Q9.02' [-250 +0]
     3 [ 3  1] 'ANS-C03/AE/Q9.1    ' 1 'Q9.03' [-250 +0]
     4 [ 3  2] 'ANS-C03/AE/Q9.2    ' 1 'Q9.04' [-250 +0]
     5 [ 6  1] 'ANS-C06/AE/Q9.1    ' 1 'Q9.05' [-250 +0]
     6 [ 6  2] 'ANS-C06/AE/Q9.2    ' 1 'Q9.06' [-250 +0]
     7 [ 7  1] 'ANS-C07/AE/Q9.1    ' 1 'Q9.07' [-250 +0]
     8 [ 7  2] 'ANS-C07/AE/Q9.2    ' 1 'Q9.08' [-250 +0]
     9 [10  1] 'ANS-C10/AE/Q9.1    ' 1 'Q9.09' [-250 +0]
    10 [10  2] 'ANS-C10/AE/Q9.2    ' 1 'Q9.10' [-250 +0]
    11 [11  1] 'ANS-C11/AE/Q9.1    ' 1 'Q9.11' [-250 +0]
    12 [11  2] 'ANS-C11/AE/Q9.2    ' 1 'Q9.12' [-250 +0]
    13 [14  1] 'ANS-C14/AE/Q9.1    ' 1 'Q9.13' [-250 +0]
    14 [14  2] 'ANS-C14/AE/Q9.2    ' 1 'Q9.14' [-250 +0]
    15 [15  1] 'ANS-C15/AE/Q9.1    ' 1 'Q9.15' [-250 +0]
    16 [15  2] 'ANS-C15/AE/Q9.2    ' 1 'Q9.16' [-250 +0]
    };
varlist.Q10={
     1 [ 2  1] 'ANS-C02/AE/Q10.1   ' 1 'Q10.01' [+0 +250]
     2 [ 2  2] 'ANS-C02/AE/Q10.2   ' 1 'Q10.02' [+0 +250]
     3 [ 3  1] 'ANS-C03/AE/Q10.1   ' 1 'Q10.03' [+0 +250]
     4 [ 3  2] 'ANS-C03/AE/Q10.2   ' 1 'Q10.04' [+0 +250]
     5 [ 6  1] 'ANS-C06/AE/Q10.1   ' 1 'Q10.05' [+0 +250]
     6 [ 6  2] 'ANS-C06/AE/Q10.2   ' 1 'Q10.06' [+0 +250]
     7 [ 7  1] 'ANS-C07/AE/Q10.1   ' 1 'Q10.07' [+0 +250]
     8 [ 7  2] 'ANS-C07/AE/Q10.2   ' 1 'Q10.08' [+0 +250]
     9 [10  1] 'ANS-C10/AE/Q10.1   ' 1 'Q10.09' [+0 +250]
    10 [10  2] 'ANS-C10/AE/Q10.2   ' 1 'Q10.10' [+0 +250]
    11 [11  1] 'ANS-C11/AE/Q10.1   ' 1 'Q10.11' [+0 +250]
    12 [11  2] 'ANS-C11/AE/Q10.2   ' 1 'Q10.12' [+0 +250]
    13 [14  1] 'ANS-C14/AE/Q10.1   ' 1 'Q10.13' [+0 +250]
    14 [14  2] 'ANS-C14/AE/Q10.2   ' 1 'Q10.14' [+0 +250]
    15 [15  1] 'ANS-C15/AE/Q10.1   ' 1 'Q10.15' [+0 +250]
    16 [15  2] 'ANS-C15/AE/Q10.2   ' 1 'Q10.16' [+0 +250]
    };
% for nanscopium
varlist.Q11={
    1 [13  1] 'ANS-C13/AE/Q11.1   ' 1 'Q11.1' [-250 +0]
    1 [13  2] 'ANS-C13/AE/Q11.2   ' 1 'Q11.2' [-250 +0]
    };
varlist.Q12={   
    1 [13  1] 'ANS-C13/AE/Q12     ' 1 'Q12.1' [+0 +250]
    };

for k = 1:12,
    ifam = ['Q' num2str(k)];
    AO.(ifam).FamilyName                 = ifam;
    AO.(ifam).FamilyType                 = 'QUAD';
    AO.(ifam).MemberOf                   = {'MachineConfig'; 'QUAD'; 'Magnet'; 'PlotFamily'; 'Archivable'};
    AO.(ifam).Monitor.Mode               = Mode;
    AO.(ifam).Monitor.DataType           = 'Scalar';
    AO.(ifam).Monitor.Units              = 'Hardware';
    AO.(ifam).Monitor.HWUnits            = 'A';
    AO.(ifam).Monitor.PhysicsUnits       = 'meter^-2';
    AO.(ifam).Monitor.HW2PhysicsFcn      = @amp2k;
    AO.(ifam).Monitor.Physics2HWFcn      = @k2amp;
    
    ifam = sprintf('Q%s', num2str(k));
    devnumber = size(varlist.(ifam),1);
    % preallocation
    AO.(ifam).ElementList = zeros(devnumber,1);
    AO.(ifam).Status      = zeros(devnumber,1);
    AO.(ifam).DeviceName  = cell(devnumber,1);
    AO.(ifam).DeviceName  = cell(devnumber,1);
    AO.(ifam).CommonNames = cell(devnumber,1);
    AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
    % make Setpoint structure than specific data
    AO.(ifam).Setpoint = AO.(ifam).Monitor;
    AO.(ifam).Setpoint.Range = zeros(devnumber,2);
    
    for ik = 1: devnumber,
        AO.(ifam).ElementList(ik)  = varlist.(ifam){ik,1};
        AO.(ifam).DeviceList(ik,:) = varlist.(ifam){ik,2};
        AO.(ifam).DeviceName(ik)   = deblank(varlist.(ifam)(ik,3));
        AO.(ifam).Status(ik)       = varlist.(ifam){ik,4};
        AO.(ifam).CommonNames(ik)  = deblank(varlist.(ifam)(ik,5));
        AO.(ifam).Monitor.TangoNames(ik)  = strcat(AO.(ifam).DeviceName{ik}, {'/currentPM'});
        AO.(ifam).Setpoint.TangoNames(ik) = strcat(AO.(ifam).DeviceName{ik}, {'/currentPM'});
        AO.(ifam).Setpoint.Range(ik,:)      = varlist.(ifam){ik,6};
    end
    
    %AO.(ifam).Monitor.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,{'/currentPM'});
    AO.(ifam).Monitor.Handles(:,1) = NaN*ones(devnumber,1);
    
    HW2PhysicsParams = magnetcoefficients(AO.(ifam).FamilyName);
    Physics2HWParams = magnetcoefficients(AO.(ifam).FamilyName);
    
    for ii=1:devnumber,
        val = 1.0;
        AO.(ifam).Monitor.HW2PhysicsParams{1}(ii,:)                 = HW2PhysicsParams;
        AO.(ifam).Monitor.HW2PhysicsParams{2}(ii,:)                 = val;
        AO.(ifam).Monitor.Physics2HWParams{1}(ii,:)                 = Physics2HWParams;
        AO.(ifam).Monitor.Physics2HWParams{2}(ii,:)                 = val;
    end
    % same configuration for Monitor and Setpoint value concerning hardware to physics units
    AO.(ifam).Setpoint.HW2PhysicsParams = AO.(ifam).Monitor.HW2PhysicsParams;
    AO.(ifam).Setpoint.Physics2HWParams = AO.(ifam).Monitor.Physics2HWParams;
    
    % Group
    if ControlRoomFlag
            AO.(ifam).GroupId = tango_group_create2(ifam);
            tango_group_add(AO.(ifam).GroupId,AO.(ifam).DeviceName');
    else
        AO.(ifam).GroupId = nan;
    end
    
    % to be part of plotfamily
    AO.(ifam).Setpoint.MemberOf  = {'PlotFamily'};
    % set tolerance for setting values
    AO.(ifam).Setpoint.Tolerance(:,:) = 0.02*ones(devnumber,1);
    % information for getrunflag
    AO.(ifam).Setpoint.RunFlagFcn = @tangogetrunflag;
    AO.(ifam).Setpoint.RampRate = 1;
       
    AO.(ifam).Desired  = AO.(ifam).Monitor;
    
    AO.(ifam).Voltage = AO.(ifam).Monitor;
    AO.(ifam).Voltage.MemberOf  = {'PlotFamily'};
    AO.(ifam).Voltage.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/voltage');
    AO.(ifam).Voltage.HWUnits            = 'V';
    
    AO.(ifam).DCCT = AO.(ifam).Monitor;
    AO.(ifam).DCCT.MemberOf  = {'PlotFamily'};
    AO.(ifam).DCCT.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/current');
    
    AO.(ifam).CurrentTotal = AO.(ifam).Monitor;
    AO.(ifam).CurrentTotal.MemberOf  = {'PlotFamily'};
    AO.(ifam).CurrentTotal.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentTotal');
    
    AO.(ifam).CurrentSetpoint = AO.(ifam).Monitor;
    AO.(ifam).CurrentSetpoint.MemberOf  = {'PlotFamily'};
    AO.(ifam).CurrentSetpoint.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentSetpoint');
    
    AO.(ifam).SumOffset = AO.(ifam).Monitor;
    AO.(ifam).SumOffset.MemberOf  = {'PlotFamily'};
    AO.(ifam).SumOffset.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/ecart3');
    
    AO.(ifam).Offset1 = AO.(ifam).Monitor;
    AO.(ifam).Offset1.MemberOf  = {'PlotFamily'};
    AO.(ifam).Offset1.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentOffset1');
        
    % Profibus configuration
    if k < 6
        AO.(ifam).Profibus.BoardNumber = int32(1);
        AO.(ifam).Profibus.Group       = int32(k);
        AO.(ifam).Profibus.DeviceName  = 'ANS/AE/DP.QP';
    elseif k < 11
        AO.(ifam).Profibus.BoardNumber = int32(0);
        AO.(ifam).Profibus.Group       = int32(k-5);
        AO.(ifam).Profibus.DeviceName  = 'ANS/AE/DP.QP';
    else % NANOSCOPIUM
        AO.(ifam).Profibus.BoardNumber = int32(0);
        AO.(ifam).Profibus.Group       = int32(6);
        AO.(ifam).Profibus.DeviceName  = 'ANS-C13/AE/DP.NANOSCOPIUM';
    end
    %convert response matrix kicks to HWUnits (after AO is loaded to AppData)
    setao(AO);   %required to make physics2hw function
    AO.(ifam).Setpoint.DeltaKBeta = 1; % for betatron function measurement.
end

% TODO : this is lattice dependent !
% step for tuneshift of 1-e-2 in one of the planes
AO.Q1.Setpoint.DeltaRespMat = 0.4; %A
AO.Q2.Setpoint.DeltaRespMat = 0.15;%A
AO.Q3.Setpoint.DeltaRespMat = 0.5; %A
AO.Q4.Setpoint.DeltaRespMat = 0.2; %A
AO.Q5.Setpoint.DeltaRespMat = 0.2; %A
AO.Q6.Setpoint.DeltaRespMat = 0.2; %A
AO.Q7.Setpoint.DeltaRespMat = 0.2; %A
AO.Q8.Setpoint.DeltaRespMat = 0.2; %A
AO.Q9.Setpoint.DeltaRespMat = 0.2; %A
AO.Q10.Setpoint.DeltaRespMat = 0.2; %A
AO.Q11.Setpoint.DeltaRespMat = 0.2; %A to be modified
AO.Q12.Setpoint.DeltaRespMat = 0.2; %A to be modified



%% All quadrupoles
ifam = 'Qall';
AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'PlotFamily'};
AO.(ifam).FamilyType             = 'Qall';
AO.(ifam).Mode                   = Mode;

AO.(ifam).DeviceName= {};

for k = 1:12, % nanoscopium
    iquad = sprintf('Q%d',k);
    AO.(ifam).DeviceName = {AO.(ifam).DeviceName{:} AO.(iquad).DeviceName{:}};
end

AO.(ifam).DeviceName = AO.(ifam).DeviceName';

AO.(ifam).DeviceList = [];

devnumber = length(AO.(ifam).DeviceName);

% build fake device list in order to use plotfamily
for k =1:devnumber,
    AO.(ifam).DeviceList = [AO.(ifam).DeviceList; 1 k];
end

AO.(ifam).ElementList            = (1:devnumber)';
AO.(ifam).Status                 = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'meter^-2';
AO.(ifam).Monitor.HW2PhysicsFcn  = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn  = @k2amp;
AO.(ifam).Monitor.TangoNames     = strcat(AO.(ifam).DeviceName, '/current');
AO.(ifam).Setpoint               = AO.(ifam).Monitor;
AO.(ifam).Setpoint.MemberOf      = {'PlotFamily'};
AO.(ifam).Setpoint.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentPM');

AO.(ifam).Voltage = AO.(ifam).Monitor;
AO.(ifam).Voltage.MemberOf  = {'PlotFamily'};
AO.(ifam).Voltage.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/voltage');

AO.(ifam).DCCT = AO.(ifam).Monitor;
AO.(ifam).DCCT.MemberOf  = {'PlotFamily'};
AO.(ifam).DCCT.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/current');

AO.(ifam).CurrentTotal = AO.(ifam).Monitor;
AO.(ifam).CurrentTotal.MemberOf  = {'PlotFamily'};
AO.(ifam).CurrentTotal.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentTotal');

AO.(ifam).CurrentSetpoint = AO.(ifam).Monitor;
AO.(ifam).CurrentSetpoint.MemberOf  = {'PlotFamily'};
AO.(ifam).CurrentSetpoint.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentSetpoint');

AO.(ifam).SumOffset = AO.(ifam).Monitor;
AO.(ifam).SumOffset.MemberOf  = {'PlotFamily'};
AO.(ifam).SumOffset.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/ecart3');

AO.(ifam).Offset1 = AO.(ifam).Monitor;
AO.(ifam).Offset1.MemberOf  = {'PlotFamily'};
AO.(ifam).Offset1.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentOffset1');


%% QC13
ifam = 'QC13';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'PlotFamily'};
AO.(ifam).FamilyType             = 'QUADC13';
AO.(ifam).Mode                   = Mode;
AO.(ifam).DeviceName             = {'ANS-C13/AE/Q1'; 'ANS-C13/AE/Q2'; 'ANS-C13/AE/Q3';  'ANS-C13/AE/Q4.1';
    'ANS-C13/AE/Q5.1'; 'ANS-C13/AE/Q5.2'; 'ANS-C13/AE/Q4.2'; 'ANS-C13/AE/Q6';
    'ANS-C13/AE/Q7'; 'ANS-C13/AE/Q8' };
AO.(ifam).DeviceList             = [13 1; 13 2; 13 3; 13 4; 13 5; 13 6; 13 7; 13 8; 13 9; 13 10];
devnumber = length(AO.(ifam).DeviceList);
AO.(ifam).ElementList            = (1:devnumber)';
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'meter^-2';
AO.(ifam).Monitor.HW2PhysicsFcn  = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn  = @k2amp;
AO.(ifam).Monitor.TangoNames     = strcat(AO.(ifam).DeviceName, '/current');
AO.(ifam).Setpoint               = AO.(ifam).Monitor;
AO.(ifam).Desired                = AO.(ifam).Monitor;
AO.(ifam).Setpoint.MemberOf      = {'PlotFamily'};
AO.(ifam).Setpoint.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentPM');


clear tune

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SEXTUPOLE MAGNETS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % 343 A for 320 Tm-2
% S11 et S12 use quadrupole power supplies (nanoscopium)
clear varlist;
varlist={
  1 [ 1  1] 'ANS/AE/S1     ' 1 'S1 ' [+000 +349]
  1 [ 1  1] 'ANS/AE/S2     ' 1 'S2 ' [-349 +000]
  1 [ 1  1] 'ANS/AE/S3     ' 1 'S3 ' [-349 +000]
  1 [ 1  1] 'ANS/AE/S4     ' 1 'S4 ' [+000 +349]
  1 [ 1  1] 'ANS/AE/S5     ' 1 'S5 ' [-349 +000]
  1 [ 1  1] 'ANS/AE/S6     ' 1 'S6 ' [+000 +349]
  1 [ 1  1] 'ANS/AE/S7     ' 1 'S7 ' [-349 +000]
  1 [ 1  1] 'ANS/AE/S8     ' 1 'S8 ' [+000 +349]
  1 [ 1  1] 'ANS/AE/S9     ' 1 'S9 ' [-349 +000]
  1 [ 1  1] 'ANS/AE/S10    ' 1 'S10' [+000 +349]
  1 [ 1  1] 'ANS/AE/S11    ' 1 'S11' [+000 +249]
  1 [13  1] 'ANS-C13/AE/S12' 1 'S12' [-249 +000]
};

for k = 1:length(varlist),
    ifam = ['S' num2str(k)];
    
    AO.(ifam).FamilyName                = ifam;
    AO.(ifam).FamilyType                = 'SEXT';
    AO.(ifam).MemberOf                  = {'MachineConfig'; 'SEXT'; 'Magnet'; 'Archivable'};
    
    AO.(ifam).Monitor.Mode              = Mode;
    AO.(ifam).Monitor.DataType          = 'Scalar';
    AO.(ifam).Monitor.Units             = 'Hardware';
    AO.(ifam).Monitor.HW2PhysicsFcn     = @amp2k;
    AO.(ifam).Monitor.Physics2HWFcn     = @k2amp;
    AO.(ifam).Monitor.HWUnits           = 'A';
    AO.(ifam).Monitor.PhysicsUnits      = 'meter^-3';
    AO.(ifam).Monitor.Handles           = NaN;
    AO.(ifam).Setpoint                  = AO.(ifam).Monitor;

    AO.(ifam).DeviceList                = varlist{k,2};
    AO.(ifam).ElementList               = varlist{k,1};    
    AO.(ifam).DeviceName                = deblank(varlist{k,3});
    AO.(ifam).Status                    = varlist{k,4};
    AO.(ifam).CommonNames               = deblank(varlist{k,5});
    AO.(ifam).Setpoint.Range(:,:)       = varlist{k,6};
    
    dev = AO.(ifam).DeviceName;
    AO.(ifam).Monitor.TangoNames  = strcat(dev,'/current');
   
    % Configuration of HW to Physics
    HW2PhysicsParams = magnetcoefficients(AO.(ifam).FamilyName);    
    val = 1.0; % scaling factor
    AO.(ifam).Monitor.HW2PhysicsParams{1}(1,:) = HW2PhysicsParams;
    AO.(ifam).Monitor.HW2PhysicsParams{2}(1,:) = val;
    AO.(ifam).Monitor.Physics2HWParams{1}(1,:) = HW2PhysicsParams;
    AO.(ifam).Monitor.Physics2HWParams{2}(1,:) = val;
    AO.(ifam).Setpoint.HW2PhysicsParams = AO.(ifam).Monitor.HW2PhysicsParams;
    AO.(ifam).Setpoint.Physics2HWParams = AO.(ifam).Monitor.Physics2HWParams;
        
    AO.(ifam).Setpoint.MemberOf  = {'PlotFamily'};
    AO.(ifam).Desired  = AO.(ifam).Monitor;
    AO.(ifam).Setpoint.TangoNames   = strcat(dev,'/currentPM');
    
    AO.(ifam).Setpoint.Tolerance     = 0.05;
    AO.(ifam).Setpoint.DeltaRespMat  = 2e7; % Physics units for a thin sextupole
    
    % information for getrunflag
    AO.(ifam).Setpoint.RunFlagFcn = @tangogetrunflag;
    AO.(ifam).Setpoint.RampRate = 1;
    
    %convert response matrix kicks to HWUnits (after AO is loaded to AppData)
    setao(AO);   %required to make physics2hw function
    AO.(ifam).Setpoint.DeltaRespMat=physics2hw(AO.(ifam).FamilyName,'Setpoint',AO.(ifam).Setpoint.DeltaRespMat,AO.(ifam).DeviceList);
end

% Config profibus not used for sextupoles NANOSCOPIUM
%         AO.(ifam).Profibus.BoardNumber = int32(0);
%         AO.(ifam).Profibus.Group       = int32(1);
%         AO.(ifam).Profibus.DeviceName  = 'ANS-C13/AE/DP.NANOSCOPIUM';

%% All Sextupoles
ifam = 'Sall';
AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'PlotFamily'};
AO.(ifam).FamilyType             = 'Sall';
AO.(ifam).Mode                   = Mode;

AO.(ifam).DeviceName= {};

for k = 1:12, % for nanoscopium
    isext = sprintf('S%d',k);
    AO.(ifam).DeviceName = {AO.(ifam).DeviceName{:} AO.(isext).DeviceName};
end

AO.(ifam).DeviceName = AO.(ifam).DeviceName';

AO.(ifam).DeviceList = [];

devnumber = length(AO.(ifam).DeviceName);

% build fake device list in order to use plotfamily
for k =1:devnumber,
    AO.(ifam).DeviceList = [AO.(ifam).DeviceList; 1 k];
end

AO.(ifam).ElementList            = (1:devnumber)';
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'meter^-3';
AO.(ifam).Monitor.HW2PhysicsFcn  = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn  = @k2amp;
AO.(ifam).Monitor.TangoNames     = strcat(AO.(ifam).DeviceName, '/current');

AO.(ifam).Setpoint               = AO.(ifam).Monitor;
AO.(ifam).Setpoint.MemberOf      = {'PlotFamily'};
AO.(ifam).Setpoint.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentPM');

AO.(ifam).Voltage = AO.(ifam).Monitor;
AO.(ifam).Voltage.MemberOf  = {'PlotFamily'};
AO.(ifam).Voltage.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/voltage');

AO.(ifam).DCCT = AO.(ifam).Monitor;
AO.(ifam).DCCT.MemberOf  = {'PlotFamily'};
AO.(ifam).DCCT.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/current');

AO.(ifam).CurrentTotal = AO.(ifam).Monitor;
AO.(ifam).CurrentTotal.MemberOf  = {'PlotFamily'};
AO.(ifam).CurrentTotal.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentTotal');

AO.(ifam).CurrentSetpoint = AO.(ifam).Monitor;
AO.(ifam).CurrentSetpoint.MemberOf  = {'PlotFamily'};
AO.(ifam).CurrentSetpoint.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentSetpoint');

AO.(ifam).SumOffset = AO.(ifam).Monitor;
AO.(ifam).SumOffset.MemberOf  = {'PlotFamily'};
AO.(ifam).SumOffset.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/ecart3');

AO.(ifam).Offset1 = AO.(ifam).Monitor;
AO.(ifam).Offset1.MemberOf  = {'PlotFamily'};
AO.(ifam).Offset1.TangoNames(:,:)  = strcat(AO.(ifam).DeviceName,'/currentOffset1');

% Build up fake poistion for plotfamily
AO.(ifam).Position = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);

%%%%%%%%%%%%%%%%%%
%% Pulsed Magnet
%%%%%%%%%%%%%%%%%%

%% All Injection kicker
ifam = 'K_INJ';
AO.(ifam).FamilyName           = ifam;
AO.(ifam).FamilyType           = ifam;
AO.(ifam).MemberOf             = {'Injection';'Archivable';'EP'};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

AO.(ifam).Status                   = ones(4,1);
AO.(ifam).DeviceName               = ['ANS-C01/EP/AL_K.1'; 'ANS-C01/EP/AL_K.2'; 'ANS-C01/EP/AL_K.3'; 'ANS-C01/EP/AL_K.4'];
AO.(ifam).CommonNames              = ifam;
AO.(ifam).ElementList              = [1 2 3 4]';
AO.(ifam).DeviceList(:,:)          = [1 1; 1 2; 1 3; 1 4];
AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(4,1);
AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/voltage');
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'V';
AO.(ifam).Monitor.PhysicsUnits     = 'mrad';
AO.(ifam).Monitor.HW2PhysicsFcn     = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn     = @k2amp;
HW2PhysicsParams                    = magnetcoefficients(ifam);
Physics2HWParams                    = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{2}(:,:) = 1;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Physics2HWParams{2}(:,:) = 1;
AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Desired = AO.(ifam).Monitor;

%% K1 and K4 Injection kicker
ifam = 'K_INJ1';
AO.(ifam).FamilyName           = ifam;
AO.(ifam).FamilyType           = ifam;
AO.(ifam).MemberOf             = {'Injection';'Archivable';'EP'};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

AO.(ifam).Status                   = ones(2,1);
AO.(ifam).DeviceName               = ['ANS-C01/EP/AL_K.1'; 'ANS-C01/EP/AL_K.4'];
AO.(ifam).CommonNames              = ifam;
AO.(ifam).ElementList              = [1 2]';
AO.(ifam).DeviceList(:,:)          = [1 1;  1 4];
AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(2,1);
AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/voltage');
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'V';
AO.(ifam).Monitor.PhysicsUnits     = 'mrad';
AO.(ifam).Monitor.HW2PhysicsFcn     = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn     = @k2amp;
HW2PhysicsParams                    = magnetcoefficients(ifam);
Physics2HWParams                    = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{2}(:,:) = 1;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Physics2HWParams{2}(:,:) = 1;
AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Desired = AO.(ifam).Monitor;

%% K2 and K3 Injection kicker
ifam = 'K_INJ2';
AO.(ifam).FamilyName           = ifam;
AO.(ifam).FamilyType           = ifam;
AO.(ifam).MemberOf             = {'Injection';'Archivable';'EP'};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

AO.(ifam).Status                   = ones(2,1);
AO.(ifam).DeviceName               = ['ANS-C01/EP/AL_K.2'; 'ANS-C01/EP/AL_K.3'];
AO.(ifam).CommonNames              = ifam;
AO.(ifam).ElementList              = [1 2]';
AO.(ifam).DeviceList(:,:)          = [1 2;  1 3];
AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(2,1);
AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/voltage');
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'V';
AO.(ifam).Monitor.PhysicsUnits     = 'mrad';
AO.(ifam).Monitor.HW2PhysicsFcn     = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn     = @k2amp;
HW2PhysicsParams                    = magnetcoefficients(ifam);
Physics2HWParams                    = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{2}(:,:) = 1;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Physics2HWParams{2}(:,:) = 1;
AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Desired = AO.(ifam).Monitor;

%% Septum Passif
ifam = 'SEP_P';
AO.(ifam).FamilyName           =  ifam;
AO.(ifam).FamilyType           =  ifam;
AO.(ifam).MemberOf             =  {'Injection';'Archivable';'EP'};
AO.(ifam).Monitor.Mode         =   Mode;
AO.(ifam).Monitor.DataType     =  'Scalar';

AO.(ifam).Status                   = 1;
AO.(ifam).DeviceName               = cellstr('ANS-C01/EP/AL_SEP_P.1');
AO.(ifam).CommonNames              = ifam;
AO.(ifam).ElementList              = 1;
AO.(ifam).DeviceList(:,:)          = [1 1];
AO.(ifam).Monitor.Handles(:,1)     = NaN;
AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/serialVoltage');
AO.(ifam).Monitor.HW2PhysicsFcn    = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn    = @k2amp;
HW2PhysicsParams                   = magnetcoefficients(ifam);
Physics2HWParams                   = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{2}(:,:) = 1;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Physics2HWParams{2}(:,:) = 1;
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'V';
AO.(ifam).Monitor.PhysicsUnits     = 'mrad';
AO.(ifam).Desired  = AO.(ifam).Monitor;
AO.(ifam).Setpoint = AO.(ifam).Monitor;


%% Septum Actif
ifam = 'SEP_A';
AO.(ifam).FamilyName           = ifam;
AO.(ifam).FamilyType           = ifam;
AO.(ifam).MemberOf             = {'Injection';'Archivable';'EP'};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

AO.(ifam).Status                   = 1;
AO.(ifam).DeviceName               = cellstr('ANS-C01/EP/AL_SEP_A');
AO.(ifam).CommonNames              = 'SEP_A_INJ';
AO.(ifam).ElementList              = 1;
AO.(ifam).DeviceList(:,:)           = [1 1];
AO.(ifam).Monitor.Handles(:,1)     = NaN;
AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/voltage');
AO.(ifam).Monitor.HW2PhysicsFcn     = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn     = @k2amp;
HW2PhysicsParams                    = magnetcoefficients(ifam);
Physics2HWParams                    = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{2}(:,:) = 1;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Physics2HWParams{2}(:,:) = 1;
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'V';
AO.(ifam).Monitor.PhysicsUnits     = 'mrad';
AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Desired = AO.(ifam).Monitor;

%% KEMH
ifam = 'KEMH';
AO.(ifam).FamilyName           = ifam;
AO.(ifam).FamilyType           = ifam;
AO.(ifam).MemberOf             = {'KEM';'Archivable';'EP'};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

AO.(ifam).Status                   = 1;
AO.(ifam).DeviceName               = cellstr('ANS-C01/EP/AL_KEM_H');
AO.(ifam).CommonNames              = 'KEM_H';
AO.(ifam).ElementList              = 1;
AO.(ifam).DeviceList(:,:)           = [1 1];
AO.(ifam).Monitor.Handles(:,1)     = NaN;
AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/voltage');
AO.(ifam).Monitor.HW2PhysicsFcn     = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn     = @k2amp;
HW2PhysicsParams                    = magnetcoefficients(ifam);
Physics2HWParams                    = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{2}(:,:) = 1;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Physics2HWParams{2}(:,:) = 1;
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'V';
AO.(ifam).Monitor.PhysicsUnits     = 'rad';
AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Desired  = AO.(ifam).Monitor;

%% KEMV
ifam = 'KEMV';
AO.(ifam).FamilyName           = ifam;
AO.(ifam).FamilyType           = ifam;
AO.(ifam).MemberOf             = {'KEM';'Archivable';'EP'};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

AO.(ifam).Status                    = 1;
AO.(ifam).DeviceName                = cellstr('ANS-C01/EP/AL_KEM_V');
AO.(ifam).CommonNames               = 'KEM_V';
AO.(ifam).ElementList               = 1;
AO.(ifam).DeviceList(:,:)           = [1 1];
AO.(ifam).Monitor.Handles(:,1)      = NaN;
AO.(ifam).Monitor.TangoNames        = strcat(AO.(ifam).DeviceName, '/voltage');
AO.(ifam).Monitor.HW2PhysicsFcn     = @amp2k;
AO.(ifam).Monitor.Physics2HWFcn     = @k2amp;
HW2PhysicsParams                    = magnetcoefficients(ifam);
Physics2HWParams                    = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(ifam).Monitor.HW2PhysicsParams{2}(:,:) = 1;
AO.(ifam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(ifam).Monitor.Physics2HWParams{2}(:,:) = 1;
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'V';
AO.(ifam).Monitor.PhysicsUnits     = 'rad';
AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Desired  = AO.(ifam).Monitor;


%% tune correctors
AO.Q7.MemberOf  = {AO.Q7.MemberOf{:} 'Tune Corrector'}';
AO.Q9.MemberOf = {AO.Q9.MemberOf{:} 'Tune Corrector'}';

%% chromaticity correctors
AO.S9.MemberOf  = {AO.S9.MemberOf{:} 'Chromaticity Corrector'}';
AO.S10.MemberOf = {AO.S10.MemberOf{:} 'Chromaticity Corrector'}';

%%%%%%%%%%%%%%%%%%
%% CYCLAGE
%%%%%%%%%%%%%%%%%

disp('cycling configuration ...');

%% cycleramp For dipole magnet
ifam = 'CycleBEND';

AO.(ifam).FamilyName             = 'CycleBEND';
AO.(ifam).MemberOf               = {'CycleBEND'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;


AO.(ifam).DeviceName             = 'ANS/AE/cycleDipole';
AO.(ifam).DeviceList             = [1 1];
AO.(ifam).ElementList            = 1;
AO.(ifam).Inom = 541.789;
AO.(ifam).Imax = 579.9;
AO.(ifam).Status = 1;

if ControlRoomFlag
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName);    
else
    AO.(ifam).GroupId = nan;
end

%% cycleramp For H-corrector magnets
ifam  = 'CycleHCOR';
ifamQ = 'HCOR';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {'CycleCOR'; ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = AO.(ifamQ).DeviceList;
AO.(ifam).ElementList            = AO.(ifamQ).ElementList;

if ControlRoomFlag
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName(find(AO.(ifamQ).Status),:)')
else
    AO.(ifam).GroupId = nan;
end

devnumber = length(AO.(ifam).DeviceName);
AO.(ifam).Inom = 1.*ones(1,devnumber);
AO.(ifam).Imax = 10.99*ones(1,devnumber);
AO.(ifam).Status = AO.(ifamQ).Status;
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% cycleramp For V-corrector magnets
ifam = 'CycleVCOR';
ifamQ = 'VCOR';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {'CycleCOR'; ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = AO.(ifamQ).DeviceList;
AO.(ifam).ElementList            = AO.(ifamQ).ElementList;


if ControlRoomFlag
    AO.(ifam).GroupId                = tango_group_create(ifam);
    tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName(find(AO.(ifamQ).Status),:)')
else
    AO.(ifam).GroupId = nan;
end

devnumber = length(AO.(ifam).DeviceName);
AO.(ifam).Inom = 1.*ones(1,devnumber);
AO.(ifam).Imax = 13.99*ones(1,devnumber);
AO.(ifam).Status = AO.(ifamQ).Status;
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% cycleramp For QT-corrector magnets
ifam = 'CycleQT';
ifamQ = 'QT';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {'CycleCOR'; ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = AO.(ifamQ).DeviceList;
AO.(ifam).ElementList            = AO.(ifamQ).ElementList;


if ControlRoomFlag
    AO.(ifam).GroupId                = tango_group_create(ifam);
    tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName(find(AO.(ifamQ).Status),:)')
else
    AO.(ifam).GroupId = nan;
end

devnumber = length(AO.(ifam).DeviceName);
AO.(ifam).Inom = 1.*ones(1,devnumber);
AO.(ifam).Imax = 6.99*ones(1,devnumber);
AO.(ifam).Status = AO.(ifamQ).Status;
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');


% cycleramp For quadrupoles magnets
%% CYCLEQ1
ifam = 'CycleQ1';
ifamQ = 'Q1';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);

if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end

devnumber = length(AO.(ifam).ElementList);
%AO.(ifam).Inom = 200.*ones(1,devnumber);
AO.(ifam).Imax = -249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ2
ifam = 'CycleQ2';
ifamQ = 'Q2';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);

if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end

devnumber = length(AO.(ifam).ElementList);
%AO.(ifam).Inom = 150*ones(1,devnumber);
AO.(ifam).Imax = 249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ3
ifam = 'CycleQ3';
ifamQ = 'Q3';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);

if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end

devnumber = length(AO.(ifam).ElementList);
%AO.(ifam).Inom = 150*ones(1,devnumber);
AO.(ifam).Imax = -249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ4
ifam = 'CycleQ4';
ifamQ = 'Q4';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
%AO.(ifam).Inom = 150*ones(1,devnumber);
AO.(ifam).Imax = -249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ5
ifam = 'CycleQ5';
ifamQ = 'Q5';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);

if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
%AO.(ifam).Inom = 150*ones(1,devnumber);
AO.(ifam).Imax = 249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ6
ifam = 'CycleQ6';
ifamQ = 'Q6';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
%AO.(ifam).Inom = 150*ones(1,devnumber);
AO.(ifam).Imax = -249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ7
ifam = 'CycleQ7';
ifamQ = 'Q7';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
%AO.(ifam).Inom = 150*ones(1,devnumber);
AO.(ifam).Imax = 249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ8
ifam = 'CycleQ8';
ifamQ = 'Q8';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);

if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end

devnumber = length(AO.(ifam).ElementList);
%AO.(ifam).Inom = 150*ones(1,devnumber);
AO.(ifam).Imax = -249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ9
ifam = 'CycleQ9';
ifamQ = 'Q9';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
%AO.(ifam).Inom = 150*ones(1,devnumber);
AO.(ifam).Imax = -249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ10
ifam = 'CycleQ10';
ifamQ = 'Q10';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = 249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ11
ifam = 'CycleQ11';
ifamQ = 'Q11';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = 2; %length(AO.(ifam).ElementList);
AO.(ifam).Imax = -249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'K';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQ12
ifam = 'CycleQ12';
ifamQ = 'Q12';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/Q','/cycleQ');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = 1; % length(AO.(ifam).ElementList);
AO.(ifam).Imax = 249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLEQC13
ifam = 'CycleQC13';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam;'CycleQP'; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
%dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = {'ANS-C13/AE/cycleQ1'; 'ANS-C13/AE/cycleQ2'; 'ANS-C13/AE/cycleQ3';  'ANS-C13/AE/cycleQ4.1';
    'ANS-C13/AE/cycleQ5.1'; 'ANS-C13/AE/cycleQ5.2'; 'ANS-C13/AE/cycleQ4.2'; ...
    'ANS-C13/AE/cycleQ6'; 'ANS-C13/AE/cycleQ7'; 'ANS-C13/AE/cycleQ8' };
AO.(ifam).DeviceList             = [13 1; 13 2; 13 3; 13 4; 13 5; 13 6; 13 7; 13 8; 13 9; 13 10];
AO.(ifam).ElementList            = (1:10)';
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId                = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = 249*[-1 1 -1 -1 1 1 -1 -1 1 -1];
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% cycleramp for sextupole magnets
%% CYCLES1
ifam = 'CycleS1';
ifamQ = 'S1';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = 349*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLES2
ifam = 'CycleS2';
ifamQ = 'S2';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = -349*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLES3
ifam = 'CycleS3';
ifamQ = 'S3';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = -349*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLES4
ifam = 'CycleS4';
ifamQ = 'S4';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = 349*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLES5
ifam = 'CycleS5';
ifamQ = 'S5';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = -349*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLES6
ifam = 'CycleS6';
ifamQ = 'S6';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = 349*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLES7
ifam = 'CycleS7';
ifamQ = 'S7';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = -349*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLES8
ifam = 'CycleS8';
ifamQ = 'S8';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = 349*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLES9
ifam = 'CycleS9';
ifamQ = 'S9';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = family2dev(ifamQ);
AO.(ifam).ElementList            = family2elem(ifamQ);
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = -349*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%% CYCLES10
ifam = 'CycleS10';
ifamQ = 'S10';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = AO.(ifamQ).DeviceList;
AO.(ifam).ElementList            = AO.(ifamQ).ElementList;
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = 349*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');


%% CYCLES11
ifam = 'CycleS11';
ifamQ = 'S11';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = AO.(ifamQ).DeviceList;
AO.(ifam).ElementList            = AO.(ifamQ).ElementList;
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = 249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');


%% CYCLES12
ifam = 'CycleS12';
ifamQ = 'S12';

AO.(ifam).FamilyName             = ifam;
AO.(ifam).MemberOf               = {ifam; 'Cyclage'};
AO.(ifam).Mode                   = Mode;
dev = getfamilydata(ifamQ,'DeviceName');
AO.(ifam).DeviceName             = regexprep(dev,'/S','/cycleS');
AO.(ifam).DeviceList             = AO.(ifamQ).DeviceList;
AO.(ifam).ElementList            = AO.(ifamQ).ElementList;
if ControlRoomFlag
        %add devices to group
        AO.(ifam).GroupId        = tango_group_create(ifam);
        tango_group_add(AO.(ifam).GroupId, AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = nan;
end
devnumber = length(AO.(ifam).ElementList);
AO.(ifam).Imax = -249*ones(1,devnumber);
AO.(ifam).Status = ones(devnumber,1);
AO.(ifam).Monitor.Mode           = Mode;
AO.(ifam).Monitor.Handles(:,1)   = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType       = 'Scalar';
AO.(ifam).Monitor.Units          = 'Hardware';
AO.(ifam).Monitor.HWUnits        = 'ampere';
AO.(ifam).Monitor.PhysicsUnits   = 'rad';
AO.(ifam).Monitor.TangoNames   = strcat(AO.(ifam).DeviceName, '/totalProgression');

%============
%% RF System
%============
ifam = 'RF';
AO.(ifam).FamilyName                = ifam;
AO.(ifam).FamilyType                = 'RF';
AO.(ifam).MemberOf                  = {'RF','RFSystem'};
AO.(ifam).Status                    = 1;
AO.(ifam).CommonNames               = 'RF';
AO.(ifam).DeviceList                = [1 1];
AO.(ifam).ElementList               = 1;
AO.(ifam).DeviceName(:,:)           = {'ANS/RF/MasterClock/'};

%Frequency Readback
AO.(ifam).Monitor.Mode                = Mode;
% AO.(ifam).Monitor.Mode                = 'Special';
% AO.(ifam).Monitor.SpecialFunctionGet = 'getrf2';
AO.(ifam).Monitor.DataType            = 'Scalar';
AO.(ifam).Monitor.Units               = 'Hardware';
AO.(ifam).Monitor.HW2PhysicsParams    = 1e+6;       %no hw2physics function necessary
AO.(ifam).Monitor.Physics2HWParams    = 1e-6;
AO.(ifam).Monitor.HWUnits             = 'MHz';
AO.(ifam).Monitor.PhysicsUnits        = 'Hz';
AO.(ifam).Monitor.TangoNames          = {'ANS/RF/MasterClock/frequency'};
AO.(ifam).Monitor.Handles             = NaN;
AO.(ifam).Monitor.Range               = [351 353];

AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Desired  = AO.(ifam).Monitor;


%============
%% Cryomodules System
%=========
% ifam = 'CM';
% %Voltage control
% AO.(ifam).Monitor.Mode               = Mode;
% AO.(ifam).Monitor.DataType           = 'Scalar';
% AO.(ifam).Monitor.Units              = 'Hardware';
% AO.(ifam).Monitor.HW2PhysicsParams   = 1;
% AO.(ifam).Monitor.Physics2HWParams   = 1;
% AO.(ifam).Monitor.HWUnits            = 'Volts';
% AO.(ifam).Monitor.PhysicsUnits       = 'Volts';
% AO.(ifam).Monitor.TangoNames          = ['ANS-C03/RF/LLE.1/voltageRF', 'ANS-C03/RF/LLE.2/voltageRF', ...
%     'ANS-C02/RF/LLE.3/voltageRF', 'ANS-C02/RF/LLE.4/voltageRF'];
% AO.(ifam).Monitor.Range              = [0 2000];
% AO.(ifam).Monitor.Tolerance          = inf;
% AO.(ifam).Monitor.Handles            = NaN;

% %Power Control
% AO.(ifam).PowerCtrl.Mode               = Mode;
% AO.(ifam).PowerCtrl.DataType           = 'Scalar';
% AO.(ifam).PowerCtrl.Units              = 'Hardware';
% AO.(ifam).PowerCtrl.HW2PhysicsParams   = 1;
% AO.(ifam).PowerCtrl.Physics2HWParams   = 1;
% AO.(ifam).PowerCtrl.HWUnits            = 'MWatts';
% AO.(ifam).PowerCtrl.PhysicsUnits       = 'MWatts';
% AO.(ifam).PowerCtrl.ChannelNames       = 'SRF1:KLYSDRIVFRWD:POWER:ON';
% AO.(ifam).PowerCtrl.Range              = [-inf inf];
% AO.(ifam).PowerCtrl.Tolerance          = inf;
% AO.(ifam).PowerCtrl.Handles            = NaN;
%
% %Power Monitor
% AO.(ifam).Power.Mode               = Mode;
% AO.(ifam).Power.DataType           = 'Scalar';
% AO.(ifam).Power.Units              = 'Hardware';
% AO.(ifam).Power.HW2PhysicsParams   = 1;
% AO.(ifam).Power.Physics2HWParams   = 1;
% AO.(ifam).Power.HWUnits            = 'MWatts';
% AO.(ifam).Power.PhysicsUnits       = 'MWatts';
% AO.(ifam).Power.ChannelNames       = 'SRF1:KLYSDRIVFRWD:POWER';
% AO.(ifam).Power.Range              = [-inf inf];
% AO.(ifam).Power.Tolerance          = inf;
% AO.(ifam).Power.Handles            = NaN;
%
% %Station Phase Control
% AO.(ifam).PhaseCtrl.Mode               = Mode;
% AO.(ifam).PhaseCtrl.DataType           = 'Scalar';
% AO.(ifam).PhaseCtrl.Units              = 'Hardware';
% AO.(ifam).PhaseCtrl.HW2PhysicsParams   = 1;
% AO.(ifam).PhaseCtrl.Physics2HWParams   = 1;
% AO.(ifam).PhaseCtrl.HWUnits            = 'Degrees';
% AO.(ifam).PhaseCtrl.PhysicsUnits       = 'Degrees';
% AO.(ifam).PhaseCtrl.ChannelNames       = 'SRF1:STN:PHASE';
% AO.(ifam).PhaseCtrl.Range              = [-200 200];
% AO.(ifam).PhaseCtrl.Tolerance          = inf;
% AO.(ifam).PhaseCtrl.Handles            = NaN;
%
% %Station Phase Monitor
% AO.(ifam).Phase.Mode               = Mode;
% AO.(ifam).Phase.DataType           = 'Scalar';
% AO.(ifam).Phase.Units              = 'Hardware';
% AO.(ifam).Phase.HW2PhysicsParams   = 1;
% AO.(ifam).Phase.Physics2HWParams   = 1;
% AO.(ifam).Phase.HWUnits            = 'Degrees';
% AO.(ifam).Phase.PhysicsUnits       = 'Degrees';
% AO.(ifam).Phase.ChannelNames       = 'SRF1:STN:PHASE:CALC';
% AO.(ifam).Phase.Range              = [-200 200];
% AO.(ifam).Phase.Tolerance          = inf;
% AO.(ifam).Phase.Handles            = NaN;

%=======
%% CHROMATICITIES: Soft Family
%=======
ifam = 'CHRO';
AO.(ifam).FamilyName  = ifam;
AO.(ifam).FamilyType  = 'Diagnostic';
AO.(ifam).MemberOf    = {'Diagnostics'};
AO.(ifam).CommonNames = ['xix';'xiz'];
AO.(ifam).DeviceList  = [ 1 1; 1 2;];
AO.(ifam).ElementList = [1; 2];
AO.(ifam).Status      = [1; 1];

%======
%% Dipole B-FIELD from RMN probe
%======
ifam = 'RMN';
AO.(ifam).FamilyName                     = ifam;
AO.(ifam).FamilyType                     = 'EM';
AO.(ifam).MemberOf                       = {'EM',ifam};
AO.(ifam).CommonNames                    = ifam;
AO.(ifam).DeviceList                     = [1 1];
AO.(ifam).ElementList                    = 1;
AO.(ifam).Status                         = AO.(ifam).ElementList;

AO.(ifam).Monitor.Mode                   = Mode;
AO.(ifam).Monitor.DataType               = 'Scalar';
AO.(ifam).Monitor.TangoNames             = 'ANS-C13/EM/RMN/magneticField'; %afin de ne pas avoir de bug
AO.(ifam).Monitor.Units                  = 'Hardware';
AO.(ifam).Monitor.Handles                = NaN;
AO.(ifam).Monitor.HWUnits                = 'mGauss';
AO.(ifam).Monitor.PhysicsUnits           = 'T';
AO.(ifam).Monitor.HW2PhysicsParams       = 1;
AO.(ifam).Monitor.Physics2HWParams       = 1;

%=======
%% TUNE
%=======
ifam = 'TUNE';
AO.(ifam).FamilyName  = ifam;
AO.(ifam).FamilyType  = 'Diagnostic';
AO.(ifam).MemberOf    = {'Diagnostics'};
AO.(ifam).CommonNames = ['nux';'nuz';'nus'];
AO.(ifam).DeviceList  = [1 1; 1 2; 1 3];
AO.(ifam).ElementList = [1 2 3]';
AO.(ifam).Status      = [1 1 1]';

AO.(ifam).Monitor.Mode                   = Mode; %'Simulator';  % Mode;
AO.(ifam).Monitor.DataType               = 'Scalar';
AO.(ifam).Monitor.DataTypeIndex          = [1 2];
AO.(ifam).Monitor.TangoNames             = ['ANS/DG/BPM-TUNEX/Nu';'ANS/DG/BPM-TUNEZ/Nu';'ANS/DG/BPM-TUNEZ/Nu'];
AO.(ifam).Monitor.Units                  = 'Hardware';
AO.(ifam).Monitor.Handles                = NaN;
AO.(ifam).Monitor.HW2PhysicsParams       = 1;
AO.(ifam).Monitor.Physics2HWParams       = 1;
AO.(ifam).Monitor.HWUnits                = 'fractional tune';
AO.(ifam).Monitor.PhysicsUnits           = 'fractional tune';


%=======
%% TUNEFBT
%=======
ifam = 'TUNEFBT';
AO.(ifam).FamilyName  = ifam;
AO.(ifam).FamilyType  = 'Diagnostic';
AO.(ifam).MemberOf    = {'Diagnostics'};
AO.(ifam).CommonNames = ['nux';'nuz'];
AO.(ifam).DeviceList  = [1 1; 1 2];
AO.(ifam).ElementList = [1 2]';
AO.(ifam).Status      = [1 1]';

AO.(ifam).Monitor.Mode                   = Mode; %'Simulator';  % Mode;
AO.(ifam).Monitor.DataType               = 'Scalar';
AO.(ifam).Monitor.DataTypeIndex          = [1 2];
AO.(ifam).Monitor.TangoNames             = ['ANS/RF/BBFDataViewer.1-TUNEX/Nu';'ANS/RF/BBFDataViewer.2-TUNEZ/Nu'];
AO.(ifam).Monitor.Units                  = 'Hardware';
AO.(ifam).Monitor.Handles                = NaN;
AO.(ifam).Monitor.HW2PhysicsParams       = 1;
AO.(ifam).Monitor.Physics2HWParams       = 1;
AO.(ifam).Monitor.HWUnits                = 'fractional tune';
AO.(ifam).Monitor.PhysicsUnits           = 'fractional tune';

%======
%% DCCT
%======
ifam = 'DCCT';
AO.(ifam).FamilyName                     = ifam;
AO.(ifam).FamilyType                     = 'Diagnostic';
AO.(ifam).MemberOf                       = {'Diagnostics','DCCT'};
AO.(ifam).CommonNames                    = 'DCCT';
AO.(ifam).DeviceList                     = [1 1];
AO.(ifam).ElementList                    = 1;
AO.(ifam).Status                         = AO.(ifam).ElementList;

AO.(ifam).Monitor.Mode                   = Mode;
AO.(ifam).FamilyName                     = 'DCCT';
AO.(ifam).Monitor.DataType               = 'Scalar';
AO.(ifam).Monitor.TangoNames             = 'ANS-C03/DG/DCCT/current'; %afin de ne pas avoir de bug
AO.(ifam).Monitor.Units                  = 'Hardware';
AO.(ifam).Monitor.Handles                = NaN;
AO.(ifam).Monitor.HWUnits                = 'mA';
AO.(ifam).Monitor.PhysicsUnits           = 'A';
AO.(ifam).Monitor.HW2PhysicsParams       = 1;
AO.(ifam).Monitor.Physics2HWParams       = 1;

% On une famille ???
ifam = 'DCCT1';
AO.(ifam).FamilyName                     = ifam;
AO.(ifam).FamilyType                     = 'Diagnostic';
AO.(ifam).MemberOf                       = {'Diagnostics','DCCT'};
AO.(ifam).CommonNames                    = 'DCCT1';
AO.(ifam).DeviceList                     = [1 1];
AO.(ifam).ElementList                    = 1;
AO.(ifam).Status                         = AO.(ifam).ElementList;

AO.(ifam).Monitor.Mode                   = Mode;
AO.(ifam).FamilyName                     = 'DCCT';
AO.(ifam).Monitor.DataType               = 'Scalar';
AO.(ifam).Monitor.TangoNames              = 'ANS-C03/DG/DCCT/current'; %afin de ne pas avoir de bug
AO.(ifam).Monitor.Units                  = 'Hardware';
AO.(ifam).Monitor.Handles                = NaN;
AO.(ifam).Monitor.HWUnits                = 'mA';
AO.(ifam).Monitor.PhysicsUnits           = 'A';
AO.(ifam).Monitor.HW2PhysicsParams       = 1;
AO.(ifam).Monitor.Physics2HWParams       = 1;

ifam = 'DCCT2';
AO.(ifam).FamilyName                     = ifam;
AO.(ifam).FamilyType                     = 'Diagnostic';
AO.(ifam).MemberOf                       = {'Diagnostics','DCCT'};
AO.(ifam).CommonNames                    = 'ANS-C14/DG/DCCT';
AO.(ifam).DeviceList                     = [1 1];
AO.(ifam).ElementList                    = 1;
AO.(ifam).Status                         = AO.(ifam).ElementList;

AO.(ifam).Monitor.Mode                   = Mode;
AO.(ifam).FamilyName                     = 'DCCT';
AO.(ifam).Monitor.DataType               = 'Scalar';
AO.(ifam).Monitor.TangoNames             = 'ANS-C03/DG/DCCT/current'; %afin de ne pas avoir de bug
AO.(ifam).Monitor.Units                  = 'Hardware';
AO.(ifam).Monitor.Handles                = NaN;
AO.(ifam).Monitor.HWUnits                = 'mA';
AO.(ifam).Monitor.PhysicsUnits           = 'A';
AO.(ifam).Monitor.HW2PhysicsParams       = 1;
AO.(ifam).Monitor.Physics2HWParams       = 1;

ifam = 'LifeTime';
AO.(ifam).FamilyName                     = ifam;
AO.(ifam).FamilyType                     = 'Diagnostic';
AO.(ifam).MemberOf                       = {'Diagnostics','DCCT'};
AO.(ifam).CommonNames                    = 'LifeTime';
AO.(ifam).DeviceList                     = [1 1];
AO.(ifam).ElementList                    = 1;
AO.(ifam).Status                         = AO.(ifam).ElementList;

AO.(ifam).Monitor.Mode                   = Mode;
AO.(ifam).FamilyName                     = 'LifeTime';
AO.(ifam).Monitor.DataType               = 'Scalar';
AO.(ifam).Monitor.TangoNames             = 'ANS-C03/DG/DCCT/lifeTime'; %afin de ne pas avoir de bug
AO.(ifam).Monitor.Units                  = 'Hardware';
AO.(ifam).Monitor.Handles                = NaN;
AO.(ifam).Monitor.HWUnits                = 'Hours';
AO.(ifam).Monitor.PhysicsUnits           = 'Hours';
AO.(ifam).Monitor.HW2PhysicsParams       = 1;
AO.(ifam).Monitor.Physics2HWParams       = 1;

if ControlRoomFlag
    
    %%%%%%%%%%%%%%%%%%
    %% Alignment
    %%%%%%%%%%%%%%%%%%
    
    disp('aligment configuration ...')
    
    %% HLS
    ifam = 'HLS';
    AO.(ifam).FamilyName           = ifam;
    AO.(ifam).FamilyType           = ifam;
    AO.(ifam).MemberOf             = {'PlotFamily'; 'Alignment'; 'HLS'; 'Archivable'};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map       = tango_get_db_property('anneau','HLS');
    cellindex = cell2mat(regexpi(map,'C[0-9]','once'))+1;
    %numindex  = cell2mat(regexpi(map,'\.[0-9]','once'))+1;
    
    % builds up devicelist
    devnumber = size(map,2);
    ik = 0; cellnum = 1;
    for k = 1:devnumber,
        if cellnum - str2double(map{k}(cellindex(k):cellindex(k)+1)) ~=0
            ik = 1;
        else
            ik = ik+1;
        end
        cellnum = str2double(map{k}(cellindex(k):cellindex(k)+1));
        AO.(ifam).DeviceList(k,:)  = [cellnum ik];
        AO.(ifam).CommonNames(k,:)      = map{k}(12:17);
    end
    
    AO.(ifam).ElementList = (1:devnumber)';
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/height');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = 'mm';
    AO.(ifam).Monitor.PhysicsUnits     = 'm';
    %AO.(ifam).Position = (1:168)'/168*354;
    
    AO.(ifam).Position = [6.000 7.370 9.270 10.323 12.549 14.775 15.828 17.178 18.548 25.512 27.272 28.232 29.284 30.244 31.241 35.051 36.047 37.007 38.059 39.019 40.779 50.262 51.632 53.532 54.585 56.811 59.037 60.090 61.440 62.810 69.774 71.534 72.494 73.546 74.506 75.503 79.313 80.309 81.269 82.322 83.282 85.042 94.524 95.894 97.794 98.847 101.073 103.299 104.352 105.702 107.072 114.036 115.796 116.756 117.809 118.769 119.765 123.575 124.571 125.531 126.584 127.544 129.304 138.786 140.156 142.056 143.109 145.335 147.561 148.614 149.964 151.334 158.298 160.058 161.018 162.071 163.031 164.027 167.837 168.833 169.793 170.846 171.806 173.566 183.049 184.419 186.319 187.371 189.597 191.824 192.876 194.226 195.596 202.560 204.320 205.280 206.333 207.293 208.289 212.099 213.095 214.055 215.108 216.068 217.828 227.311 228.681 230.581 231.633 233.859 236.086 237.138 238.488 239.858 246.822 248.582 249.542 250.595 251.555 252.551 256.361 257.357 258.317 259.370 260.330 262.090 271.573 272.943 274.843 275.895 278.121 280.348 281.400 282.750 284.120 291.085 292.845 293.805 294.857 295.817 296.813 300.623 301.620 302.580 303.632 304.592 306.352 315.835 317.205 319.105 320.157 322.384 324.610 325.662 327.012 328.382 335.347 337.107 338.067 339.119 340.079 341.075 344.885 345.882 346.842 347.894 348.854 350.614];
    AO.(ifam).Position = AO.(ifam).Position(:);
    
    AO.(ifam).Mean = AO.(ifam).Monitor;
    AO.(ifam).Mean.TangoNames       = strcat(AO.(ifam).DeviceName, '/average');
    AO.(ifam).Mean.MemberOf = {'Plotfamily'};
    
    AO.(ifam).Std = AO.(ifam).Monitor;
    AO.(ifam).Std.TangoNames       = strcat(AO.(ifam).DeviceName, '/standardDeviation');
    AO.(ifam).Std.MemberOf = {'Plotfamily'};
    
    AO.(ifam).Voltage = AO.(ifam).Monitor;
    AO.(ifam).Voltage.TangoNames       = strcat(AO.(ifam).DeviceName, '/voltage');
    AO.(ifam).Voltage.MemberOf = {'Plotfamily'};
    
end
%%%%%%%%%%%%%%%%%%
%% AVLS GEOPHONES
%%%%%%%%%%%%%%%%%%

ifam = 'GEO'; % vibration
AO.(ifam).FamilyName           = ifam;
AO.(ifam).MemberOf             = {'PlotFamily'; 'VIB'; 'Archivable'};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

varlist = {
    1  [ 1  1] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C09_Q5_X'
    2  [ 1  2] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C09_Q5_Z'
    3  [ 1  3] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C09_BPM4_X'
    4  [ 1  4] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C09_BPM4_Z'
    5  [ 1  5] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C10_BPM1_X'
    6  [ 1  6] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C10_BPM1_Z'
    7  [ 1  7] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_TDL_D09-2_XBPM_Z'
    8  [ 1  8] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_TDL_I10_XBPM_Z'
    9  [ 1  9] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_HALL_I10C_X'
    10  [ 1 10] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_HALL_I10C_Z'
    11  [ 1 11] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_HALL_I02C_X'
    12  [ 1 12] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_HALL_I02C_Z'
    13  [ 1 13] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_EXT_D306_X'
    14  [ 1 14] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_EXT_D306_Z'
    15  [ 1 15] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_EXT_T3_X'
    16  [ 1 16] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_EXT_T3_Z'
    };

devnumber = length(varlist);

% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).RMS.TangoNames  = cell(devnumber,1);

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,6));
    AO.(ifam).Monitor.TangoNames(k)  = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,6)), '_PEAK');
    AO.(ifam).RMS.TangoNames(k)      = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,6)), '_RMS');
end

% peak values
AO.(ifam).Monitor.HW2PhysicsParams = 1;
AO.(ifam).Monitor.Physics2HWParams = 1;
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'm';
AO.(ifam).Monitor.PhysicsUnits     = 'm';
AO.(ifam).Monitor.MemberOf         = {'Plotfamily'};
AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);

%rms values
AO.(ifam).RMS.HW2PhysicsParams = 1;
AO.(ifam).RMS.Physics2HWParams = 1;
AO.(ifam).RMS.Units            = 'Hardware';
AO.(ifam).RMS.HWUnits          = 'm';
AO.(ifam).RMS.PhysicsUnits     = 'm';
AO.(ifam).RMS.MemberOf = {'Plotfamily'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AVLS GEOPHONES PEAK VALUE
%%%%%%%%%%%%%%%%%%%%%%%%%%%

ifam = 'PEAKGEO'; % vibration
AO.(ifam).FamilyName           = ifam;
AO.(ifam).MemberOf             = {'PlotFamily'; 'VIB'; 'Archivable'};
AO.(ifam).Monitor.Mode         = Mode;
AO.(ifam).Monitor.DataType     = 'Scalar';

varlist = {
    1  [ 1  1] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C09_Q5_X_PEAK'
    2  [ 1  2] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C09_Q5_Z_PEAK'
    3  [ 1  3] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C09_BPM4_X_PEAK'
    4  [ 1  4] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C09_BPM4_Z_PEAK'
    5  [ 1  5] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C10_BPM1_X_PEAK'
    6  [ 1  6] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_C10_BPM1_Z_PEAK'
    7  [ 1  7] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_TDL_D09-2_XBPM_Z_PEAK'
    8  [ 1  8] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_TDL_I10_XBPM_Z_PEAK'
    9  [ 1  9] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_HALL_I10C_X_PEAK'
    10  [ 1 10] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_HALL_I10C_Z_PEAK'
    11  [ 1 11] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_HALL_I02C_X_PEAK'
    12  [ 1 12] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_HALL_I02C_Z_PEAK'
    13  [ 1 13] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_EXT_D306_X_PEAK'
    14  [ 1 14] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_EXT_D306_Z_PEAK'
    15  [ 1 15] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_EXT_T3_X_PEAK'
    16  [ 1 16] 'ANS/DG/SURVIB_DATA.1' 1 'GEOPH01' 'GEOPHONE_EXT_T3_Z_PEAK'
    };

devnumber = length(varlist);

% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);

for k = 1: devnumber,
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,6));
    AO.(ifam).Monitor.TangoNames(k)  = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,6)));
end

AO.(ifam).Monitor.HW2PhysicsParams = 1;
AO.(ifam).Monitor.Physics2HWParams = 1;
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'm/sqrt(Hz)';
AO.(ifam).Monitor.PhysicsUnits     = 'm/sqrt(Hz)';
AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);



%%%%%%%%%%%%%%%%%%
%% VACUUM SYSTEM
%%%%%%%%%%%%%%%%%%
if ControlRoomFlag
    
    disp('vacuum configuration ...')
    
    %% IonPump
    ifam = 'PI';
    AO.(ifam).FamilyName           = 'PI';
    AO.(ifam).FamilyType           = 'PI';
    AO.(ifam).MemberOf             = {'PlotFamily'; 'IonPump'; 'Pressure'; 'Archivable';};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map       = tango_get_db_property('anneau','pompe_ionique');
    cellindex = cell2mat(regexpi(map,'C[0-9]','once'))+1;
    numindex  = cell2mat(regexpi(map,'\.[0-9]','once'))+1;
    
    
    devnumber = size(map,2);
    for k = 1:devnumber,
        AO.(ifam).DeviceList(k,:)  = [str2double(map{k}(cellindex(k):cellindex(k)+1)) str2double(map{k}(numindex(k):end))];
    end
    
    AO.(ifam).ElementList = (1:devnumber)';
    
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).CommonNames              = [repmat('PI',devnumber,1) num2str((1:devnumber)','%02d')];
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/pressure');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = 'mbar';
    AO.(ifam).Monitor.PhysicsUnits     = 'mbar';
    AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
    
    %% Penning Gauges
    ifam = 'JPEN';
    AO.(ifam).FamilyName           = 'JPEN';
    AO.(ifam).MemberOf             = {'PlotFamily'; 'PenningGauge'; 'Pressure'; 'Archivable';'PlotFamily'};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map     = tango_get_db_property('anneau','jauge_penning');
    cellindex = cell2mat(regexpi(map,'C[0-9]','once'))+1;
    numindex  = cell2mat(regexpi(map,'\.[0-9]','once'))+1;
    
    devnumber = size(map,2);
    for k = 1:devnumber,
        AO.(ifam).DeviceList(k,:)  = [str2double(map{k}(cellindex(k):cellindex(k)+1)) str2double(map{k}(numindex(k):end))];
    end
    
    AO.(ifam).ElementList = (1:devnumber)';
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).CommonNames              = [repmat('JPEN',devnumber,1) num2str((1:devnumber)','%02d')];
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/pressure');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = 'mbar';
    AO.(ifam).Monitor.PhysicsUnits     = 'mbar';
    AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
    
    %% Pirani Gauges
    ifam = 'JPIR';
    AO.(ifam).FamilyName           = 'JPIR';
    AO.(ifam).MemberOf             = {'PlotFamily'; 'PiraniGauge'; 'Pressure'; 'Archivable';'PlotFamily'};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map     = tango_get_db_property('anneau','jauge_piranni');
    cellindex = cell2mat(regexpi(map,'C[0-9]','once'))+1;
    numindex  = cell2mat(regexpi(map,'\.[0-9]','once'))+1;
    
    devnumber = size(map,2);
    for k = 1:devnumber,
        AO.(ifam).DeviceList(k,:)  = [str2double(map{k}(cellindex(k):cellindex(k)+1)) str2double(map{k}(numindex(k):end))];
    end
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).CommonNames              = [repmat('JPIR',devnumber,1) num2str((1:devnumber)','%02d')];
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/pressure');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = 'mbar';
    AO.(ifam).Monitor.PhysicsUnits     = 'mbar';
    
    %% Bayer Albert Gauges
    ifam = 'JBA';
    AO.(ifam).FamilyName           = ifam;
    AO.(ifam).MemberOf             = {'PlotFamily'; 'PiraniGauge'; 'Pressure'; 'Archivable';'PlotFamily'};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map     = tango_get_db_property('anneau','jauge_bayer_alpert');
    cellindex = cell2mat(regexpi(map,'C[0-9]','once'))+1;
    numindex  = cell2mat(regexpi(map,'\.[0-9]','once'))+1;
    
    devnumber = size(map,2);
    for k = 1:devnumber,
        AO.(ifam).DeviceList(k,:)  = [str2double(map{k}(cellindex(k):cellindex(k)+1)) str2double(map{k}(numindex(k):end))];
    end
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).CommonNames              = [repmat(ifam,devnumber,1) num2str((1:devnumber)','%02d')];
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/pressure');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = 'mbar';
    AO.(ifam).Monitor.PhysicsUnits     = 'mbar';
    AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
    
    %% Thermocouples
    ifam = 'TC';
    AO.(ifam).FamilyName           = ifam;
    AO.(ifam).MemberOf             = {'PlotFamily'; 'TC'; 'Archivable';'PlotFamily'};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map     = tango_get_db_property('anneau','thermocouple');
    cellindex = cell2mat(regexpi(map,'C[0-9]','once'))+1;
    numindex  = cell2mat(regexpi(map,'\.[0-9]','once'))+1;
    
    devnumber = size(map,2);
    for k = 1:devnumber,
        AO.(ifam).DeviceList(k,:)  = [str2double(map{k}(cellindex(k):cellindex(k)+1)) str2double(map{k}(numindex(k):end))];
    end
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).CommonNames              = [repmat(ifam,devnumber,1) num2str((1:devnumber)','%02d')];
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/temperature');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = '°C';
    AO.(ifam).Monitor.PhysicsUnits     = '°C';
    AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
    
    %% TDL-Thermocouples
    ifam = 'TDL_TC';
    AO.(ifam).FamilyName           = ifam;
    AO.(ifam).MemberOf             = {'PlotFamily'; 'TC'; 'Archivable';'PlotFamily'};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map     = tango_get_db_property('TDL','thermocouple');
    cellindex = cell2mat(regexpi(map,'[DI][0-9]','once'))+1;
    numindex  = cell2mat(regexpi(map,'\.[0-9]','once'))+1;
    
    devnumber = size(map,2);
    for k = 1:devnumber,
        AO.(ifam).DeviceList(k,:)  = [str2double(map{k}(cellindex(k):cellindex(k)+1)) str2double(map{k}(numindex(k):end))];
    end
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).CommonNames              = [repmat(ifam,devnumber,1) num2str((1:devnumber)','%02d')];
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/temperature');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = '�C';
    AO.(ifam).Monitor.PhysicsUnits     = '�C';
    AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
    
    %% DEBIMETRES
    ifam = 'DEB';
    AO.(ifam).FamilyName           = ifam;
    AO.(ifam).MemberOf             = {'Vaccuum'};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map     = tango_get_db_property('anneau','debitmetre');
    cellindex = cell2mat(regexpi(map,'C[0-9]','once'))+1;
    numindex  = cell2mat(regexpi(map,'\.[0-9]','once'))+1;
    
    devnumber = size(map,2);
    for k = 1:devnumber,
        AO.(ifam).DeviceList(k,:)  = [str2double(map{k}(cellindex(k):cellindex(k)+1)) str2double(map{k}(numindex(k):end))];
    end
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).CommonNames              = [repmat(ifam,devnumber,1) num2str((1:devnumber)','%02d')];
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/temperature');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = 'au';
    AO.(ifam).Monitor.PhysicsUnits     = 'au';
    AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
    
    %% VM
    ifam = 'VM';
    AO.(ifam).FamilyName           = ifam;
    AO.(ifam).MemberOf             = {'PlotFamily'; 'Vaccuum'; 'Archivable';'PlotFamily'};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map     = tango_get_db_property('anneau','vanne_manuelle');
    cellindex = cell2mat(regexpi(map,'C[0-9]','once'))+1;
    numindex  = cell2mat(regexpi(map,'\.[0-9]','once'))+1;
    
    devnumber = size(map,2);
    for k = 1:devnumber,
        AO.(ifam).DeviceList(k,:)  = [str2double(map{k}(cellindex(k):cellindex(k)+1)) str2double(map{k}(numindex(k):end))];
    end
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).CommonNames              = [repmat(ifam,devnumber,1) num2str((1:devnumber)','%02d')];
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/temperature');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = 'au';
    AO.(ifam).Monitor.PhysicsUnits     = 'au';
    
    %% VS
    ifam = 'VS';
    AO.(ifam).FamilyName           = ifam;
    AO.(ifam).MemberOf             = {'PlotFamily'; 'Vaccuum'; 'Archivable';'PlotFamily'};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map     = tango_get_db_property('anneau','vanne_secteur');
    cellindex = cell2mat(regexpi(map,'C[0-9]','once'))+1;
    numindex  = cell2mat(regexpi(map,'\.[0-9]','once'))+1;
    
    devnumber = size(map,2);
    for k = 1:devnumber,
        AO.(ifam).DeviceList(k,:)  = [str2double(map{k}(cellindex(k):cellindex(k)+1)) str2double(map{k}(numindex(k):end))];
    end
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).CommonNames              = [repmat(ifam,devnumber,1) num2str((1:devnumber)','%02d')];
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/temperature');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = 'au';
    AO.(ifam).Monitor.PhysicsUnits     = 'au';
    AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
    
    %% Thermocouples GTC
    ifam = 'GTC_TC';
    AO.(ifam).FamilyName           = ifam;
    AO.(ifam).MemberOf             = {'PlotFamily'; 'GTC'; 'Temperature'; 'Archivable';'PlotFamily'};
    AO.(ifam).Monitor.Mode         = Mode;
    AO.(ifam).Monitor.DataType     = 'Scalar';
    
    map     = tango_get_db_property('anneau','GTC_Tunnel_TC');
    cellindex = cell2mat(regexpi(map,'C[0-9]','once'))+1;
    numindex  = cell2mat(regexpi(map,'MT[0-9]','once'))+2;
    
    devnumber = size(map,2);
    for k = 1:devnumber,
        AO.(ifam).DeviceList(k,:)  = [str2double(map{k}(cellindex(k):cellindex(k)+1)) str2double(map{k}(numindex(k):end))];
    end
    
    AO.(ifam).Status                   = ones(devnumber,1);
    AO.(ifam).DeviceName               = map';
    AO.(ifam).CommonNames              = [repmat(ifam,devnumber,1) num2str((1:devnumber)','%02d')];
    AO.(ifam).ElementList              = (1:devnumber)';
    AO.(ifam).Monitor.Handles(:,1)     = NaN*ones(devnumber,1);
    AO.(ifam).Monitor.TangoNames       = strcat(AO.(ifam).DeviceName, '/temperature');
    AO.(ifam).Monitor.HW2PhysicsParams = 1;
    AO.(ifam).Monitor.Physics2HWParams = 1;
    AO.(ifam).Monitor.Units            = 'Hardware';
    AO.(ifam).Monitor.HWUnits          = '°C';
    AO.(ifam).Monitor.PhysicsUnits     = '°C';
    AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
    
    
    %=====================
    %% Gamma Monitors DOSE
    %====================
    ifam = 'CIGdose';
    AO.(ifam).FamilyName                     = 'CIGdose';
    AO.(ifam).FamilyType                     = 'Radioprotection';
    AO.(ifam).MemberOf                       = {'Radioprotection','Archivable','Plotfamily','PlotFamily'};
    AO.(ifam).CommonNames                    = 'CIG';
    
    map = tango_get_db_property('anneau','gammamonitor_mapping');
    AO.(ifam).DeviceName = map';
    
    AO.(ifam).Monitor.Mode                   = Mode;
    AO.(ifam).FamilyName                     = 'CIGdose';
    AO.(ifam).Monitor.DataType               = 'Scalar';
    
    devnumber = length(AO.(ifam).DeviceName);
    AO.(ifam).DeviceList                     = [ones(1,devnumber); (1:devnumber)]';
    AO.(ifam).ElementList                    = (1:devnumber)';
    AO.(ifam).Status                         = ones(devnumber,1);
    
    AO.(ifam).Monitor.TangoNames            = strcat(AO.(ifam).DeviceName,'/dose');
    
    %afin de ne pas avoir de bug
    AO.(ifam).Monitor.Units                  = 'Hardware';
    AO.(ifam).Monitor.Handles                = NaN;
    AO.(ifam).Monitor.HWUnits                = 'uGy';
    AO.(ifam).Monitor.PhysicsUnits           = 'uGy';
    AO.(ifam).Monitor.HW2PhysicsParams       = 1;
    AO.(ifam).Monitor.Physics2HWParams       = 1;
    AO.(ifam).Position                       = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
    
    %=========================
    %% Gamma Monitors DOSErate
    %=========================
    
    ifam = 'CIGrate';
    AO.(ifam) = AO.CIGdose;
    AO.(ifam).FamilyName                     = ifam;
    AO.(ifam).CommonNames                    = ifam;
    
    devnumber = length(AO.(ifam).DeviceName);
    AO.(ifam).Status                         = ones(devnumber,1);
    AO.(ifam).DeviceList                     = [ones(1,devnumber); (1:devnumber)]';
    AO.(ifam).ElementList                    = (1:devnumber)';
    
    AO.(ifam).Monitor.TangoNames            = strcat(AO.(ifam).DeviceName,'/doseRate');
    
    %afin de ne pas avoir de bug
    AO.CIG.Monitor.HWUnits                = 'uGy/h';
    AO.CIG.Monitor.PhysicsUnits           = 'uGy/h';
    
    %=====================
    %% Neutron Monitors DOSE
    %====================
    ifam = 'MONdose';
    AO.(ifam).FamilyName                     = ifam;
    AO.(ifam).FamilyType                     = 'Radioprotection';
    AO.(ifam).MemberOf                       = {'Radioprotection','Archivable','Plotfamily','PlotFamily'};
    AO.(ifam).CommonNames                    = 'MON';
    
    map = tango_get_db_property('anneau','neutronmonitor_mapping');
    AO.(ifam).DeviceName = map';
    
    AO.(ifam).Monitor.Mode                   = Mode;
    AO.(ifam).FamilyName                     = ifam;
    AO.(ifam).Monitor.DataType               = 'Scalar';
    
    devnumber = length(AO.(ifam).DeviceName);
    AO.(ifam).DeviceList                     = [ones(1,devnumber); (1:devnumber)]';
    AO.(ifam).ElementList                    = (1:devnumber)';
    AO.(ifam).Status                         = ones(devnumber,1);
    
    AO.(ifam).Monitor.TangoNames            = strcat(AO.(ifam).DeviceName,'/dDoseSinceReset');
    
    %afin de ne pas avoir de bug
    AO.(ifam).Monitor.Units                  = 'Hardware';
    AO.(ifam).Monitor.Handles                = NaN;
    AO.(ifam).Monitor.HWUnits                = 'uGy';
    AO.(ifam).Monitor.PhysicsUnits           = 'uGy';
    AO.(ifam).Monitor.HW2PhysicsParams       = 1;
    AO.(ifam).Monitor.Physics2HWParams       = 1;
    AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
    
    %============================
    %% Neutron Monitors DOSE RATE
    %============================
    ifam = 'MONrate';
    AO.(ifam).FamilyName                     = ifam;
    AO.(ifam).FamilyType                     = 'Radioprotection';
    AO.(ifam).MemberOf                       = {'Radioprotection','Archivable','Plotfamily','PlotFamily'};
    AO.(ifam).CommonNames                    = 'MON';
    
    map = tango_get_db_property('anneau','neutronmonitor_mapping');
    AO.(ifam).DeviceName = map';
    
    AO.(ifam).Monitor.Mode                   = Mode;
    AO.(ifam).FamilyName                     = ifam;
    AO.(ifam).Monitor.DataType               = 'Scalar';
    
    devnumber = length(AO.(ifam).DeviceName);
    AO.(ifam).DeviceList                     = [ones(1,devnumber); (1:devnumber)]';
    AO.(ifam).ElementList                    = (1:devnumber)';
    AO.(ifam).Status                         = ones(devnumber,1);
    
    AO.(ifam).Monitor.TangoNames            = strcat(AO.(ifam).DeviceName,'/dCountsSinceReset');
    
    %afin de ne pas avoir de bug
    AO.(ifam).Monitor.Units                  = 'Hardware';
    AO.(ifam).Monitor.Handles                = NaN;
    AO.(ifam).Monitor.HWUnits                = 'uradGy';
    AO.(ifam).Monitor.PhysicsUnits           = 'uradGy';
    AO.(ifam).Monitor.HW2PhysicsParams       = 1;
    AO.(ifam).Monitor.Physics2HWParams       = 1;
    AO.(ifam).Position                 = (1:length(AO.(ifam).DeviceName))'*354/length(AO.(ifam).DeviceName);
end

% %====================
% %% Machine Parameters
% %====================
% AO.MachineParameters.FamilyName                = 'MachineParameters';
% AO.MachineParameters.FamilyType                = 'Parameter';
% AO.MachineParameters.MemberOf                  = {'Diagnostics'};
% AO.MachineParameters.Status                    = [1 1 1 1]';
%
% AO.MachineParameters.Monitor.Mode              = Mode;
% AO.MachineParameters.Monitor.DataType          = 'Scalar';
% AO.MachineParameters.Monitor.Units             = 'Hardware';
%
% %use spear2 process variable names
% mp={
%     'mode    '    'SPEAR:BeamStatus  '          [1 1]  1; ...
%     'energy  '    'SPEAR:Energy      '          [1 2]  2; ...
%     'current '    'SPEAR:BeamCurrAvg '          [1 3]  3; ...
%     'lifetime'    'SPEAR:BeamLifetime'          [1 4]  4; ...
%     };
% AO.MachineParameters.Monitor.HWUnits          = ' ';
% AO.MachineParameters.Monitor.PhysicsUnits     = ' ';
%
% AO.MachineParameters.Setpoint.HWUnits         = ' ';
% AO.MachineParameters.Setpoint.PhysicsUnits    = ' ';
%
% for ii=1:size(mp,1),
%     name  =mp(ii,1);    AO.MachineParameters.CommonNames(ii,:)            = char(name{1});
%     %     name  =mp(ii,2);    AO.MachineParameters.Monitor.ChannelNames(ii,:)   = char(name{1});
%     %     name  =mp(ii,2);    AO.MachineParameters.Setpoint.ChannelNames(ii,:)  = char(name{1});
%     val   =mp(ii,3);    AO.MachineParameters.DeviceList(ii,:)             = val{1};
%     val   =mp(ii,4);    AO.MachineParameters.ElementList(ii,:)            = val{1};
%
%     AO.MachineParameters.Monitor.HW2PhysicsParams(ii,:)    = 1;
%     AO.MachineParameters.Monitor.Physics2HWParams(ii,:)    = 1;
%     AO.MachineParameters.Monitor.Handles(ii,1)  = NaN;
%     AO.MachineParameters.Setpoint.HW2PhysicsParams(ii,:)   = 1;
%     AO.MachineParameters.Setpoint.Physics2HWParams(ii,:)   = 1;
%     AO.MachineParameters.Setpoint.Handles(ii,1)  = NaN;
% end


%======
%% Septum
%======
% ifam=ifam+1;
% AO.Septum.FamilyName                  = 'Septum';
% AO.Septum.FamilyType                  = 'Septum';
% AO.Septum.MemberOf                    = {'Injection'};
% AO.Septum.Status                      = 1;
%
% AO.Septum.CommonNames                 = 'Septum  ';
% AO.Septum.DeviceList                  = [3 1];
% AO.Septum.ElementList                 = [1];
%
% AO.Septum.Monitor.Mode                = Mode;
% AO.Septum.Monitor.DataType            = 'Scalar';
% AO.Septum.Monitor.Units               = 'Hardware';
% AO.Septum.Monitor.HWUnits             = 'ampere';
% AO.Septum.Monitor.PhysicsUnits        = 'rad';
% AO.Septum.Monitor.ChannelNames        = 'BTS-B9V:Curr';
% AO.Septum.Monitor.Handles             = NaN;
%
% AO.Septum.Setpoint.Mode               = Mode;
% AO.Septum.Setpoint.DataType           = 'Scalar';
% AO.Septum.Setpoint.Units              = 'Hardware';
% AO.Septum.Setpoint.HWUnits            = 'ampere';
% AO.Septum.Setpoint.PhysicsUnits       = 'rad';
% AO.Septum.Setpoint.ChannelNames       = 'BTS-B9V:CurrSetpt';
% AO.Septum.Setpoint.Range              = [0, 249.90];
% AO.Septum.Setpoint.Tolerance          = 100.0;
% AO.Septum.Setpoint.Handles            = NaN;
%
% AO.Septum.Monitor.HW2PhysicsParams    = 1;
% AO.Septum.Monitor.Physics2HWParams    = 1;
% AO.Septum.Setpoint.HW2PhysicsParams   = 1;
% AO.Septum.Setpoint.Physics2HWParams   = 1;

% Save AO
setao(AO);

% The operational mode sets the path, filenames, and other important params
% Run setoperationalmode after most of the AO is built so that the Units and Mode fields
% can be set in setoperationalmode

waitbar(0.4,h);

setoperationalmode(OperationalMode);

waitbar(0.5,h);

%======================================================================
%======================================================================
%% Append Accelerator Toolbox information
%======================================================================
%======================================================================
disp('** Initializing Accelerator Toolbox information');

AO = getao;

%% Machine Params
ifam = ('MachineParams');
AO.(ifam).AT.ATType       = 'MachineParams';
AO.(ifam).AT.ATName(1,:)  = 'Energy  ';
AO.(ifam).AT.ATName(2,:)  = 'current ';
AO.(ifam).AT.ATName(3,:)  = 'Lifetime';

% Save AO
setao(AO);

disp('Setting min max configuration from TANGO static database ...');

waitbar(0.80,h);

disp('Setting gain offset configuration  ...');

setfamilydata([0.20; 0.30; NaN],'TUNE','Golden');
setfamilydata([0.0; 0.0],'CHRO','Golden');
setfamilydata_local('BPMx');
setfamilydata_local('BPMz');
setfamilydata_local('HCOR');
setfamilydata_local('VCOR');
setfamilydata_local('FHCOR');
setfamilydata_local('FVCOR');
setfamilydata_local('Q1');
setfamilydata_local('Q2');
setfamilydata_local('Q3');
setfamilydata_local('Q4');
setfamilydata_local('Q5');
setfamilydata_local('Q6');
setfamilydata_local('Q7');
setfamilydata_local('Q8');
setfamilydata_local('Q9');
setfamilydata_local('Q10');
setfamilydata_local('Q11');
setfamilydata_local('Q12');
setfamilydata_local('S1');
setfamilydata_local('S2');
setfamilydata_local('S3');
setfamilydata_local('S4');
setfamilydata_local('S5');
setfamilydata_local('S6');
setfamilydata_local('S7');
setfamilydata_local('S8');
setfamilydata_local('S9');
setfamilydata_local('S10');
% nanoscopium
setfamilydata_local('S11');
%setfamilydata_local('S12');
setfamilydata_local('BEND');

waitbar(0.95,h);

if iscontrolroom
    switch2online;
else
    switch2sim;
end

delete(h);
% set close orbit
% fprintf('%3d %3d  %10.6f  %10.6f\n', [family2dev('BPMx'), getgolden('BPMx'), getgolden('BPMz')]');
% fprintf('%3d %3d  %10.6f  %10.6f\n', [family2dev('BPMx'), getam('BPMx'), getam('BPMz')]');
% fprintf('%3d %3d  %10.6f  %10.6f\n', [family2dev('BPMx'), gethbpmaverage, getvbpmaverage]'); % With averaging

%% 1 November 2010, 100 mA, 8 bunches
% Golden = [
%   1   2    0.016345    0.032149
%   1   3    0.016961    0.048611
%   1   4    0.005294    0.028680
%   1   5   -0.106745    0.027269
%   1   6    0.090583    0.064031
%   1   7   -0.012068    0.161952
%   2   1   -0.045148   -0.053773
%   2   2   -0.031657    0.062942
%   2   3   -0.028058    0.085674
%   2   4   -0.001069    0.061447
%   2   5   -0.021188   -0.054335
%   2   6    0.000233    0.055889
%   2   7    0.081396    0.027054
%   2   8    0.047110   -0.127170
%   3   1   -0.075029    0.047635
%   3   2   -0.073932    0.170573
%   3   3    0.036058   -0.156983
%   3   4    0.043860    0.047354
%   3   5   -0.116479    0.057157
%   3   6   -0.027964    0.068107
%   3   7   -0.022696   -0.009808
%   3   8    0.019911   -0.034260
%   4   1   -0.003700    0.094862
%   4   2    0.023829   -0.022505
%   4   3    0.021355    0.077993
%   4   4   -0.132808    0.063380
%   4   5   -0.032417    0.021840
%   4   6    0.100986    0.020014
%   4   7    0.041342   -0.000490
%   5   1    0.026344    0.048286
%   5   2   -0.057137   -0.012724
%   5   3   -0.014899    0.090461
%   5   4    0.061359    0.017747
%   5   5   -0.061539    0.030594
%   5   6   -0.027320    0.044954
%   5   7   -0.053060   -0.019833
%   6   1   -0.013820    0.032068
%   6   2   -0.015491    0.097888
%   6   3   -0.059813   -0.022452
%   6   4   -0.110275    0.024518
%   6   5    0.067538    0.006971
%   6   6   -0.106836    0.041192
%   6   7    0.029155    0.022466
%   6   8    0.009789   -0.124377
%   7   1    0.002325    0.110398
%   7   2   -0.056687   -0.056676
%   7   3    0.007593    0.026913
%   7   4    0.023821    0.046949
%   7   5   -0.014372   -0.004262
%   7   6   -0.080500    0.107662
%   7   7    0.157559   -0.034857
%   7   8   -0.011032    0.006873
%   8   1    0.033792    0.049671
%   8   2    0.009650   -0.017292
%   8   3   -0.071568    0.125278
%   8   4   -0.075801    0.008001
%   8   5   -0.049135   -0.005426
%   8   6    0.106108    0.069929
%   8   7   -0.085924    0.044483
%   9   1    0.043681   -0.034826
%   9   2    0.023218    0.029768
%   9   3   -0.041154    0.038563
%   9   4    0.015624    0.003208
%   9   5   -0.006598    0.038795
%   9   6    0.025799   -0.010056
%   9   7   -0.027015    0.015066
%  10   1    0.042671    0.043401
%  10   2    0.059791   -0.017355
%  10   3   -0.003680    0.060614
%  10   4   -0.053241   -0.000351
%  10   5   -0.014054    0.000681
%  10   6    0.019173    0.058396
%  10   7   -0.081427   -0.016236
%  10   8   -0.038545   -0.153397
%  11   1    0.052273    0.098443
%  11   2    0.046001    0.011273
%  11   3   -0.024048   -0.022239
%  11   4   -0.020922    0.047930
%  11   5    0.001203    0.007482
%  11   6   -0.118557    0.139495
%  11   7   -0.005381    0.009126
%  11   8    0.021087   -0.147026
%  12   1    0.007320    0.152963
%  12   2   -0.009885   -0.095798
%  12   3   -0.036567    0.120397
%  12   4   -0.020770   -0.028452
%  12   5   -0.068473    0.018120
%  12   6   -0.035726    0.068041
%  12   7   -0.035734    0.077598
%  13   1    0.034521    0.036254
%  13   2    0.008060   -0.085722
%  13   3   -0.025867    0.191533
%  13   4    0.057237    0.065225
%  13   5    0.031814   -0.038756
%  13   6   -0.095517    0.061334
%  13   7   -0.020761    0.075604
%  14   1    0.041534    0.016812
%  14   2   -0.035987   -0.065275
%  14   3   -0.002298    0.126982
%  14   4   -0.038774   -0.023886
%  14   5    0.036175    0.011716
%  14   6   -0.009999    0.071074
%  14   7    0.024754   -0.020716
%  14   8   -0.014612    0.036705
%  15   1   -0.014409    0.053757
%  15   2    0.009989   -0.110797
%  15   3   -0.000704    0.139605
%  15   4   -0.057129    0.046420
%  15   5    0.037405   -0.034540
%  15   6   -0.000056   -0.044614
%  15   7    0.000619    0.051375
%  15   8    0.045704   -0.148878
%  16   1   -0.078725    0.120226
%  16   2   -0.038085   -0.020403
%  16   3   -0.033360    0.104118
%  16   4   -0.035493    0.033354
%  16   5   -0.039215    0.001572
%  16   6   -0.034544    0.081431
%  16   7    0.030171    0.094878
%   1   1   -0.022958   -0.065639
% ];
%% 30 aout, 2010, 400mA, 120 BPM, 4/4, after 1h @ 400mA, top-up on
Golden = [
  1   2    0.006856   -0.002046
  1   3   -0.007166    0.031381
  1   4   -0.015442    0.001163
  1   5   -0.111951   -0.014862
  1   6    0.147278    0.019714
  1   7   -0.019939    0.139009
  2   1   -0.011475   -0.094653
  2   2    0.055081    0.011237
  2   3   -0.037250    0.061448
  2   4    0.019700    0.045965
  2   5   -0.009656   -0.080373
  2   6   -0.065788    0.063886
  2   7    0.061426   -0.020762
  2   8    0.033285   -0.129804
  3   1   -0.068102    0.041084
  3   2   -0.008265    0.116157
  3   3    0.012609   -0.188080
  3   4   -0.016128   -0.017910
  3   5    0.009898    0.042864
  3   6   -0.031271    0.025254
  3   7    0.023580   -0.015062
  3   8    0.000614   -0.057267
  4   1   -0.008317    0.059714
  4   2    0.022390   -0.024202
  4   3    0.007105    0.011231
  4   4   -0.127603    0.011938
  4   5   -0.012065   -0.003867
  4   6    0.137812    0.000599
  4   7   -0.060800   -0.026952
  5   1    0.059211    0.037001
  5   2   -0.031928   -0.023488
  5   3    0.013358    0.032376
  5   4    0.028795   -0.001651
  5   5   -0.013764    0.019058
  5   6   -0.027115   -0.002603
  5   7    0.003983   -0.000299
  6   1   -0.001273   -0.002330
  6   2    0.021005    0.036622
  6   3   -0.012890   -0.039734
  6   4   -0.034390    0.014097
  6   5    0.039591   -0.017175
  6   6   -0.064323    0.007359
  6   7    0.061640    0.002813
  6   8   -0.012456   -0.127135
  7   1    0.006525    0.123439
  7   2   -0.020262   -0.078069
  7   3    0.006598    0.051906
  7   4    0.010064   -0.009943
  7   5    0.006661   -0.002720
  7   6   -0.139337    0.109901
  7   7    0.150211   -0.063662
  7   8   -0.027520   -0.001287
  8   1    0.005109    0.024387
  8   2    0.037418   -0.039567
  8   3   -0.013181    0.062029
  8   4   -0.080459    0.000618
  8   5   -0.035821    0.000253
  8   6    0.125783    0.014937
  8   7   -0.053813    0.047949
  9   1    0.043285   -0.037679
  9   2    0.006039    0.013197
  9   3   -0.004108    0.004239
  9   4   -0.036872   -0.010673
  9   5   -0.007169    0.006149
  9   6    0.046131    0.009260
  9   7   -0.033374    0.003034
 10   1    0.035716    0.022797
 10   2    0.025421   -0.045074
 10   3   -0.012046    0.064993
 10   4   -0.061350   -0.012028
 10   5    0.052221    0.026554
 10   6    0.024470    0.020115
 10   7   -0.036041   -0.009331
 10   8   -0.023796   -0.147465
 11   1    0.034842    0.116906
 11   2    0.067055   -0.019981
 11   3   -0.040301   -0.030616
 11   4   -0.036049    0.007173
 11   5    0.035995   -0.023059
 11   6   -0.021438    0.103816
 11   7    0.009998   -0.045482
 11   8   -0.001599   -0.156865
 12   1   -0.001028    0.165850
 12   2    0.016282   -0.122944
 12   3   -0.016822    0.120879
 12   4    0.039226   -0.042006
 12   5   -0.004430   -0.005715
 12   6   -0.039647    0.055368
 12   7   -0.030623    0.053464
 13   1    0.050688   -0.021504
 13   2    0.011777   -0.142915
 13   3   -0.016364    0.153845
 13   4    0.022348    0.086566
 13   5    0.026364   -0.067777
 13   6   -0.059906    0.018269
 13   7   -0.017170    0.065623
 14   1    0.041785    0.000809
 14   2   -0.001011   -0.077591
 14   3   -0.001517    0.125123
 14   4   -0.049640   -0.000749
 14   5    0.047128   -0.009968
 14   6   -0.017230    0.039915
 14   7    0.012912   -0.017657
 14   8   -0.017499    0.023705
 15   1    0.018401    0.039845
 15   2    0.031601   -0.104378
 15   3   -0.020209    0.149938
 15   4   -0.053535    0.041727
 15   5    0.056124   -0.038312
 15   6   -0.020499   -0.064035
 15   7    0.009281    0.053264
 15   8    0.031391   -0.143362
 16   1   -0.057865    0.136878
 16   2   -0.022599   -0.088826
 16   3    0.014637    0.061252
 16   4    0.000994    0.022685
 16   5    0.025738   -0.039009
 16   6   -0.031708    0.039920
 16   7   -0.029343    0.069699
  1   1    0.036023   -0.061148
  ];

%% 30 aout, 2010, 15mA, 120 BPM, 3/4, after 1h @ 15mA, top-up off
% Golden = [
%       1   2    0.006588   -0.001133
%   1   3   -0.007887    0.020259
%   1   4   -0.013064   -0.000158
%   1   5   -0.109003   -0.011352
%   1   6    0.140942    0.016984
%   1   7   -0.018859    0.138771
%   2   1   -0.007879   -0.096057
%   2   2    0.031941    0.016589
%   2   3   -0.023153    0.054283
%   2   4    0.022222    0.051771
%   2   5   -0.014391   -0.089246
%   2   6   -0.065712    0.062526
%   2   7    0.062101   -0.018480
%   2   8    0.031155   -0.133693
%   3   1   -0.062465    0.042200
%   3   2   -0.021967    0.119712
%   3   3    0.020168   -0.194052
%   3   4   -0.009220   -0.015754
%   3   5    0.000874    0.042220
%   3   6   -0.021693    0.015171
%   3   7    0.014338   -0.008911
%   3   8    0.001921   -0.055672
%   4   1   -0.008547    0.058844
%   4   2    0.024373   -0.024240
%   4   3    0.005069    0.008305
%   4   4   -0.120490    0.019986
%   4   5   -0.005520    0.001011
%   4   6    0.124219   -0.013461
%   4   7   -0.062820   -0.022391
%   5   1    0.064569    0.036742
%   5   2   -0.031672   -0.015715
%   5   3    0.013026    0.023018
%   5   4    0.027979   -0.002708
%   5   5   -0.017098    0.012285
%   5   6   -0.021109    0.006163
%   5   7   -0.000263   -0.005705
%   6   1    0.004970   -0.003124
%   6   2    0.018927    0.041714
%   6   3   -0.012663   -0.046511
%   6   4   -0.028809    0.013478
%   6   5    0.034833   -0.016684
%   6   6   -0.064319    0.009906
%   6   7    0.062784    0.001245
%   6   8   -0.015248   -0.122565
%   7   1    0.012245    0.118387
%   7   2   -0.028624   -0.073137
%   7   3    0.009653    0.047531
%   7   4    0.025797   -0.007581
%   7   5   -0.008477   -0.006964
%   7   6   -0.144139    0.111972
%   7   7    0.157087   -0.064198
%   7   8   -0.026614   -0.002911
%   8   1    0.002613    0.026800
%   8   2    0.036058   -0.042966
%   8   3   -0.012339    0.067016
%   8   4   -0.078501   -0.002404
%   8   5   -0.036172   -0.001623
%   8   6    0.124301    0.020163
%   8   7   -0.051030    0.050180
%   9   1    0.039760   -0.040284
%   9   2    0.006211    0.009295
%   9   3   -0.001575    0.002340
%   9   4   -0.052427    0.005818
%   9   5   -0.002115    0.005543
%   9   6    0.056713   -0.005754
%   9   7   -0.033126    0.007263
%  10   1    0.032634    0.026133
%  10   2    0.024461   -0.048546
%  10   3   -0.011519    0.067836
%  10   4   -0.057527   -0.011072
%  10   5    0.048997    0.024725
%  10   6    0.019902    0.020858
%  10   7   -0.030694   -0.009637
%  10   8   -0.024079   -0.147010
%  11   1    0.034588    0.114033
%  11   2    0.066924   -0.013660
%  11   3   -0.040786   -0.038153
%  11   4   -0.030403    0.006405
%  11   5    0.030948   -0.021894
%  11   6   -0.023140    0.104433
%  11   7    0.012747   -0.046253
%  11   8   -0.000400   -0.160648
%  12   1   -0.001411    0.163108
%  12   2    0.002765   -0.110117
%  12   3   -0.007435    0.100703
%  12   4    0.035812   -0.035098
%  12   5   -0.002294   -0.006109
%  12   6   -0.039689    0.049007
%  12   7   -0.030368    0.053778
%  13   1    0.050632   -0.019038
%  13   2    0.008500   -0.143347
%  13   3   -0.014398    0.153223
%  13   4    0.023765    0.086362
%  13   5    0.025631   -0.068868
%  13   6   -0.060494    0.019319
%  13   7   -0.011435    0.065602
%  14   1    0.033533   -0.000282
%  14   2   -0.009775   -0.075804
%  14   3    0.003958    0.123405
%  14   4   -0.043403   -0.001774
%  14   5    0.040671   -0.009713
%  14   6   -0.018253    0.046106
%  14   7    0.014802   -0.021492
%  14   8   -0.013441    0.026146
%  15   1    0.010684    0.038643
%  15   2    0.036139   -0.105726
%  15   3   -0.022691    0.152677
%  15   4   -0.048208    0.043405
%  15   5    0.051054   -0.041331
%  15   6   -0.018357   -0.062275
%  15   7    0.007725    0.052658
%  15   8    0.029495   -0.151444
%  16   1   -0.056481    0.137586
%  16   2   -0.005586   -0.077943
%  16   3    0.002912    0.047178
%  16   4    0.010566    0.016929
%  16   5    0.023262   -0.030738
%  16   6   -0.036834    0.036158
%  16   7   -0.029239    0.073402
%   1   1    0.037958   -0.063636
%   ];

%% 29 aout, 2010, 15mA, 120 BPM, 3/4, after 1h @ 15mA, top-up off
% Golden = [
%    1   2    0.005519   -0.002198
%   1   3   -0.009473    0.018148
%   1   4   -0.013281   -0.003514
%   1   5   -0.108927   -0.011923
%   1   6    0.141126    0.015744
%   1   7   -0.018203    0.139160
%   2   1   -0.008553   -0.097371
%   2   2    0.028749    0.004221
%   2   3   -0.022640    0.053672
%   2   4    0.022460    0.048481
%   2   5   -0.013749   -0.091944
%   2   6   -0.065359    0.058096
%   2   7    0.063013   -0.022042
%   2   8    0.030027   -0.133682
%   3   1   -0.062462    0.038741
%   3   2   -0.022472    0.116882
%   3   3    0.018485   -0.196273
%   3   4   -0.003531   -0.021368
%   3   5   -0.002620    0.033096
%   3   6   -0.021515    0.010715
%   3   7    0.016448   -0.011796
%   3   8    0.001507   -0.058138
%   4   1   -0.009535    0.053501
%   4   2    0.024275   -0.030095
%   4   3    0.004478    0.004816
%   4   4   -0.119228    0.017415
%   4   5   -0.004799   -0.000774
%   4   6    0.122711   -0.015789
%   4   7   -0.062303   -0.027904
%   5   1    0.062795    0.035900
%   5   2   -0.032568   -0.019556
%   5   3    0.014551    0.020366
%   5   4    0.028740   -0.008892
%   5   5   -0.018452    0.006466
%   5   6   -0.021354    0.001337
%   5   7   -0.000946   -0.010978
%   6   1    0.004914   -0.009300
%   6   2    0.018923    0.036814
%   6   3   -0.010828   -0.049917
%   6   4   -0.030086    0.010611
%   6   5    0.033149   -0.018546
%   6   6   -0.065427    0.007932
%   6   7    0.061193   -0.001258
%   6   8   -0.013623   -0.125057
%   7   1    0.012414    0.113477
%   7   2   -0.027153   -0.075772
%   7   3    0.009945    0.045030
%   7   4    0.026502   -0.014227
%   7   5   -0.012510   -0.008229
%   7   6   -0.147125    0.111980
%   7   7    0.157779   -0.065769
%   7   8   -0.025543   -0.003905
%   8   1    0.003780    0.022444
%   8   2    0.035876   -0.047778
%   8   3   -0.013203    0.061467
%   8   4   -0.079070   -0.009959
%   8   5   -0.036284   -0.005915
%   8   6    0.124029    0.015899
%   8   7   -0.048953    0.051929
%   9   1    0.037788   -0.050545
%   9   2    0.002977    0.007222
%   9   3   -0.001619    0.002287
%   9   4   -0.047194    0.006677
%   9   5   -0.004650    0.002850
%   9   6    0.054801   -0.008421
%   9   7   -0.032676    0.005829
%  10   1    0.031674    0.022948
%  10   2    0.019244   -0.058451
%  10   3   -0.010067    0.072389
%  10   4   -0.057173   -0.016819
%  10   5    0.050360    0.020825
%  10   6    0.021541    0.018578
%  10   7   -0.029885   -0.013816
%  10   8   -0.025528   -0.150772
%  11   1    0.034284    0.106782
%  11   2    0.066963   -0.016040
%  11   3   -0.041072   -0.040030
%  11   4   -0.030381   -0.000704
%  11   5    0.030850   -0.026794
%  11   6   -0.022526    0.098457
%  11   7    0.012124   -0.055005
%  11   8   -0.000553   -0.165598
%  12   1   -0.003691    0.156274
%  12   2    0.001026   -0.117009
%  12   3   -0.005375    0.097639
%  12   4    0.036721   -0.036122
%  12   5   -0.003059   -0.007160
%  12   6   -0.041421    0.045407
%  12   7   -0.029797    0.049173
%  13   1    0.050341   -0.025589
%  13   2    0.010158   -0.147299
%  13   3   -0.013779    0.147450
%  13   4    0.021467    0.077490
%  13   5    0.022845   -0.076101
%  13   6   -0.057039    0.013787
%  13   7   -0.014202    0.057222
%  14   1    0.035809   -0.005705
%  14   2    0.000317   -0.073144
%  14   3    0.000684    0.112848
%  14   4   -0.042960   -0.001280
%  14   5    0.037263   -0.009404
%  14   6   -0.020858    0.042073
%  14   7    0.012538   -0.024438
%  14   8   -0.011211    0.021330
%  15   1    0.012027    0.034419
%  15   2    0.037726   -0.112195
%  15   3   -0.021321    0.148945
%  15   4   -0.050089    0.040019
%  15   5    0.048802   -0.045301
%  15   6   -0.020736   -0.065544
%  15   7    0.006349    0.049177
%  15   8    0.032333   -0.154165
%  16   1   -0.056359    0.136218
%  16   2   -0.006319   -0.085302
%  16   3    0.003381    0.039860
%  16   4    0.006738    0.016729
%  16   5    0.027736   -0.037876
%  16   6   -0.040020    0.027953
%  16   7   -0.026426    0.065566
%   1   1    0.035265   -0.061654 
% ];

%%  23 November, 2009, 400mA, 120 BPM, 4/4, after 6h @ 400mA, top-up off
% Golden = [
%   1   2    0.002029   -0.008229
%   1   3   -0.016893    0.026100
%   1   4   -0.015172   -0.019001
%   1   5   -0.114024   -0.006831
%   1   6    0.152653    0.019670
%   1   7   -0.025143    0.142441
%   2   1   -0.007858   -0.089854
%   2   2    0.030861   -0.022048
%   2   3   -0.019826    0.093670
%   2   4    0.030907    0.026464
%   2   5   -0.019382   -0.069930
%   2   6   -0.070347    0.073769
%   2   7    0.081850   -0.031747
%   2   8    0.036344   -0.116644
%   3   1   -0.081106    0.032058
%   3   2   -0.014571    0.120384
%   3   3    0.020284   -0.203138
%   3   4    0.008848   -0.026130
%   3   5   -0.021635    0.039991
%   3   6   -0.017804    0.007987
%   3   7    0.001855   -0.013802
%   3   8   -0.005742   -0.048333
%   4   1    0.000777    0.044010
%   4   2    0.055459   -0.039893
%   4   3   -0.009262    0.022676
%   4   4   -0.133837    0.013747
%   4   5   -0.009283   -0.018432
%   4   6    0.132152    0.006566
%   4   7   -0.067811   -0.026251
%   5   1    0.057710    0.021284
%   5   2   -0.021850   -0.007270
%   5   3    0.020481    0.021295
%   5   4    0.029735   -0.008168
%   5   5   -0.022407    0.017295
%   5   6   -0.042089   -0.003680
%   5   7    0.005685   -0.010708
%   6   1   -0.007240   -0.007034
%   6   2    0.046488    0.031829
%   6   3   -0.021638   -0.040056
%   6   4   -0.054686    0.013182
%   6   5    0.054372   -0.033158
%   6   6   -0.054305    0.008764
%   6   7    0.041719   -0.002200
%   6   8   -0.009685   -0.107517
%   7   1    0.000980    0.123431
%   7   2   -0.027138   -0.080044
%   7   3   -0.006857    0.066786
%   7   4    0.004198    0.000402
%   7   5   -0.006952   -0.025075
%   7   6   -0.106707    0.103219
%   7   7    0.112809   -0.045282
%   7   8   -0.023845    0.006732
%   8   1   -0.009150    0.032298
%   8   2    0.030902   -0.051337
%   8   3   -0.020029    0.065809
%   8   4   -0.053900    0.007747
%   8   5   -0.044756   -0.014493
%   8   6    0.114454    0.010274
%   8   7   -0.069436    0.023371
%   9   1    0.056108   -0.024845
%   9   2    0.018738    0.000199
%   9   3   -0.014617    0.005879
%   9   4   -0.045680   -0.009865
%   9   5   -0.004769    0.003346
%   9   6    0.054530    0.012475
%   9   7   -0.041639    0.014072
%  10   1    0.028337    0.026056
%  10   2    0.016550   -0.056310
%  10   3   -0.002997    0.087650
%  10   4   -0.075533   -0.024969
%  10   5    0.071778    0.034296
%  10   6    0.014466   -0.002983
%  10   7   -0.023510   -0.007526
%  10   8   -0.025012   -0.159753
%  11   1    0.031167    0.123138
%  11   2    0.064896   -0.044175
%  11   3   -0.041487   -0.005483
%  11   4   -0.034816   -0.006699
%  11   5    0.025957   -0.005239
%  11   6   -0.019788    0.086007
%  11   7    0.012042   -0.054441
%  11   8    0.004218   -0.159520
%  12   1   -0.014335    0.156688
%  12   2    0.005308   -0.145706
%  12   3   -0.010359    0.122560
%  12   4    0.027522   -0.021435
%  12   5   -0.001802   -0.021829
%  12   6   -0.044313    0.041809
%  12   7   -0.024854    0.041145
%  13   1    0.048999   -0.024470
%  13   2   -0.006526   -0.115663
%  13   3   -0.006496    0.118086
%  13   4    0.036219    0.075528
%  13   5    0.016212   -0.057939
%  13   6   -0.081271   -0.005013
%  13   7   -0.004162    0.024156
%  14   1    0.029248    0.012258
%  14   2    0.011144   -0.068937
%  14   3   -0.014571    0.089728
%  14   4   -0.059358   -0.011430
%  14   5    0.066404    0.013160
%  14   6   -0.029031    0.040967
%  14   7    0.025450   -0.026824
%  14   8   -0.022484    0.023487
%  15   1    0.007571    0.039077
%  15   2    0.013885   -0.113153
%  15   3   -0.010637    0.151075
%  15   4   -0.056538    0.032897
%  15   5    0.069841   -0.041709
%  15   6    0.007850   -0.065815
%  15   7   -0.016163    0.049007
%  15   8    0.038600   -0.137666
%  16   1   -0.080264    0.128128
%  16   2   -0.025569   -0.107178
%  16   3    0.014235    0.059120
%  16   4    0.007970    0.011004
%  16   5    0.011226   -0.041107
%  16   6   -0.024778    0.032385
%  16   7   -0.018219    0.045577
%   1   1    0.007766   -0.035362
% ];
  
% %% 30 August 2009 New BBA SMIS Bump removed at 15mA in 3/4 filling pattern
% Golden = [
%   1   2    0.001439    0.001652
%   1   3   -0.007091    0.017834
%   1   4   -0.006591   -0.011706
%   1   5   -0.111409   -0.009861
%   1   6    0.149007    0.019923
%   1   7   -0.016221    0.140724
%   2   1   -0.012326   -0.098728
%   2   2    0.022745    0.002045
%   2   3   -0.017265    0.057335
%   2   4    0.030117    0.045905
%   2   5   -0.014439   -0.092500
%   2   6   -0.076057    0.073762
%   2   7    0.078586   -0.030317
%   2   8    0.032366   -0.122302
%   3   1   -0.069194    0.027148
%   3   2   -0.024865    0.128594
%   3   3    0.023162   -0.209758
%   3   4    0.005185   -0.022135
%   3   5   -0.008367    0.032886
%   3   6   -0.025321    0.009339
%   3   7    0.022625   -0.009644
%   3   8    0.004808   -0.045184
%   4   1   -0.013162    0.040981
%   4   2    0.024655   -0.024763
%   4   3    0.009763    0.004499
%   4   4   -0.120164    0.014080
%   4   5   -0.007917    0.001188
%   4   6    0.127279   -0.016816
%   4   7   -0.059909   -0.017120
%   5   1    0.067177    0.021188
%   5   2   -0.033817   -0.014992
%   5   3    0.020363    0.017370
%   5   4    0.038278   -0.009647
%   5   5   -0.015577    0.010705
%   5   6   -0.035002    0.001076
%   5   7    0.007233   -0.004009
%   6   1    0.004876   -0.018708
%   6   2    0.018958    0.050652
%   6   3   -0.008517   -0.065843
%   6   4   -0.037120    0.008628
%   6   5    0.042010   -0.015112
%   6   6   -0.062148   -0.000035
%   6   7    0.061490    0.002638
%   6   8   -0.013969   -0.117166
%   7   1    0.017591    0.104004
%   7   2   -0.038242   -0.065918
%   7   3    0.013324    0.037782
%   7   4    0.031001   -0.016255
%   7   5   -0.008171   -0.007454
%   7   6   -0.141648    0.112682
%   7   7    0.162265   -0.068776
%   7   8   -0.025494    0.010029
%   8   1    0.000738    0.011495
%   8   2    0.029703   -0.042968
%   8   3   -0.010064    0.063031
%   8   4   -0.073307   -0.011270
%   8   5   -0.033144   -0.009564
%   8   6    0.131276    0.022435
%   8   7   -0.052013    0.041565
%   9   1    0.036586   -0.043457
%   9   2    0.006134    0.012435
%   9   3   -0.004680   -0.003617
%   9   4   -0.040993    0.003242
%   9   5   -0.009967    0.007224
%   9   6    0.069160   -0.014063
%   9   7   -0.034088    0.021542
%  10   1    0.030060    0.014328
%  10   2    0.016756   -0.060234
%  10   3   -0.007433    0.080666
%  10   4   -0.049369   -0.015788
%  10   5    0.047482    0.021982
%  10   6    0.024118    0.010022
%  10   7   -0.028365   -0.009081
%  10   8   -0.023524   -0.141268
%  11   1    0.033100    0.100282
%  11   2    0.063702   -0.016999
%  11   3   -0.037193   -0.035528
%  11   4   -0.021609   -0.000204
%  11   5    0.024595   -0.026905
%  11   6   -0.020315    0.095136
%  11   7    0.012547   -0.053682
%  11   8    0.004781   -0.149360
%  12   1   -0.007228    0.142436
%  12   2    0.001215   -0.108709
%  12   3   -0.002472    0.090346
%  12   4    0.042099   -0.031966
%  12   5    0.000411   -0.000045
%  12   6   -0.048330    0.032040
%  12   7   -0.023035    0.033025
%  13   1    0.051491   -0.013401
%  13   2    0.004173   -0.128078
%  13   3   -0.009898    0.124427
%  13   4    0.040102    0.071330
%  13   5    0.025983   -0.069281
%  13   6   -0.076844    0.012332
%  13   7   -0.003092    0.041857
%  14   1    0.032198    0.000741
%  14   2   -0.002463   -0.065692
%  14   3   -0.003516    0.097877
%  14   4   -0.031015   -0.002773
%  14   5    0.036756   -0.006621
%  14   6   -0.020060    0.034095
%  14   7    0.024106   -0.020057
%  14   8   -0.010341    0.016619
%  15   1    0.005888    0.034718
%  15   2    0.025686   -0.110044
%  15   3   -0.019190    0.145568
%  15   4   -0.044768    0.039564
%  15   5    0.059525   -0.043613
%  15   6   -0.008457   -0.073426
%  15   7    0.009205    0.051279
%  15   8    0.027809   -0.128621
%  16   1   -0.059105    0.121604
%  16   2   -0.007262   -0.090021
%  16   3    0.006021    0.055724
%  16   4    0.010074    0.016936
%  16   5    0.023634   -0.039754
%  16   6   -0.025321    0.030519
%  16   7   -0.023471    0.057133
%   1   1    0.027700   -0.054217   
% ];

% Golden du March 31 2009, 56 vp dans les 2 plans, 200 mA 3/4 (AVEC modification BBA)
% offset for SMIS (-75 Âµm)
% Golden = [
%   1   2    0.009517    0.042990
%   1   3   -0.006912   -0.020981
%   1   4   -0.008131   -0.020004
%   1   5   -0.128837    0.013248
%   1   6    0.153328    0.044941
%   1   7   -0.015086    0.217996
%   2   1   -0.008673   -0.059031
%   2   2    0.064185   -0.055041
%   2   3   -0.043694   -0.031985
%   2   4    0.032966   -0.030984
%   2   5   -0.018060   -0.121653
%   2   6   -0.079164    0.067819
%   2   7    0.079500    0.028741
%   2   8    0.039440   -0.095657
%   3   1   -0.076281    0.013838
%   3   2   -0.028893    0.124158
%   3   3    0.020463   -0.187586
%   3   4   -0.004275   -0.015461
%   3   5    0.006217    0.029326
%   3   6   -0.015080   -0.003430
%   3   7    0.019742   -0.002939
%   3   8   -0.010000   -0.038951
%   4   1    0.003815    0.035406
%   4   2    0.003631   -0.017151
%   4   3   -0.001206    0.010410
%   4   4    0.002572   -0.003328
%   4   5   -0.079047    0.013685
%   4   6    0.102416   -0.008571
%   4   7   -0.053985   -0.032613
%   5   1    0.049493    0.038489
%   5   2   -0.038116   -0.021715
%   5   3    0.022548    0.018919
%   5   4   -0.010487   -0.006437
%   5   5   -0.044115    0.014717
%   5   6    0.056807   -0.007830
%   5   7    0.029018   -0.015024
%   6   1   -0.065438   -0.006192
%   6   2    0.007563    0.057855
%   6   3    0.004598   -0.077867
%   6   4   -0.028412    0.027629
%   6   5    0.031710   -0.032835
%   6   6   -0.054573   -0.031474
%   6   7    0.049633    0.022857
%   6   8   -0.008758   -0.123239
%   7   1    0.007826    0.114230
%   7   2   -0.031132   -0.081831
%   7   3    0.020347    0.056831
%   7   4    0.025359   -0.022916
%   7   5   -0.018023    0.001148
%   7   6   -0.168047    0.153523
%   7   7    0.172901   -0.092309
%   7   8   -0.024282    0.014386
%   8   1    0.008647    0.007565
%   8   2    0.033849   -0.040678
%   8   3   -0.017340    0.055864
%   8   4    0.005713    0.000270
%   8   5   -0.062488   -0.002284
%   8   6    0.061882    0.003296
%   8   7   -0.039267    0.032761
%   9   1    0.047468   -0.026321
%   9   2    0.013949    0.008803
%   9   3    0.002068   -0.019551
%   9   4   -0.080058    0.049047
%   9   5    0.001749   -0.025064
%   9   6    0.072247   -0.012855
%   9   7   -0.008409   -0.020559
%  10   1   -0.002844    0.044009
%  10   2    0.028449   -0.053548
%  10   3   -0.013310    0.050035
%  10   4   -0.063004   -0.021360
%  10   5    0.060199    0.036064
%  10   6    0.023595   -0.007369
%  10   7   -0.027561    0.002287
%  10   8   -0.027888   -0.151662
%  11   1    0.040506    0.111583
%  11   2    0.070149   -0.012097
%  11   3   -0.049883   -0.046897
%  11   4   -0.027172    0.003815
%  11   5    0.040897   -0.033778
%  11   6   -0.011005    0.090152
%  11   7    0.012352   -0.049901
%  11   8    0.003093   -0.143565
%  12   1   -0.014488    0.141303
%  12   2   -0.023394   -0.100100
%  12   3   -0.004888    0.096986
%  12   4   -0.033700    0.022641
%  12   5    0.049386   -0.059648
%  12   6   -0.012757    0.053991
%  12   7   -0.048246    0.024330
%  13   1    0.057075   -0.015535
%  13   2    0.021994   -0.146814
%  13   3   -0.028242    0.130104
%  13   4    0.042461    0.080305
%  13   5    0.027648   -0.078012
%  13   6   -0.068133    0.007657
%  13   7   -0.019029    0.042027
%  14   1    0.039834    0.004204
%  14   2   -0.002020   -0.061227
%  14   3   -0.000927    0.098654
%  14   4   -0.000385   -0.004974
%  14   5    0.001997    0.008001
%  14   6   -0.000082    0.040959
%  14   7   -0.002337   -0.027026
%  14   8   -0.017742    0.028978
%  15   1    0.012388    0.019261
%  15   2    0.044181   -0.098953
%  15   3   -0.015445    0.144318
%  15   4   -0.069711    0.037561
%  15   5    0.061587   -0.039499
%  15   6   -0.001112   -0.043836
%  15   7   -0.020052    0.035631
%  15   8    0.019645   -0.134164
%  16   1   -0.021730    0.135700
%  16   2   -0.014351   -0.109644
%  16   3    0.031979    0.062489
%  16   4   -0.075720    0.082745
%  16   5    0.047563   -0.086837
%  16   6    0.012484    0.023260
%  16   7   -0.039474    0.081965
%   1   1    0.060349   -0.086699
%   ];

% Golden du 19 janvier 2009, 56 vp dans les 2 plans, 200 mA 3/4 (AVEC modification BBA)
% Golden = [
%   1   2    0.009517    0.043785
%   1   3   -0.006912   -0.020423
%   1   4   -0.008131   -0.019617
%   1   5   -0.128837    0.006179
%   1   6    0.153328    0.013081
%   1   7   -0.015086    0.154057
%   2   1   -0.008673   -0.111905
%   2   2    0.064185    0.016447
%   2   3   -0.043694    0.042714
%   2   4    0.032966    0.043878
%   2   5   -0.018060   -0.081909
%   2   6   -0.079164    0.052099
%   2   7    0.079500   -0.017959
%   2   8    0.039440   -0.095618
%   3   1   -0.076281    0.014079
%   3   2   -0.028893    0.124642
%   3   3    0.020463   -0.187276
%   3   4   -0.004275   -0.015337
%   3   5    0.006217    0.029269
%   3   6   -0.015080   -0.003917
%   3   7    0.019742   -0.003768
%   3   8   -0.010000   -0.039642
%   4   1    0.003815    0.034818
%   4   2    0.003631   -0.016422
%   4   3   -0.001206    0.011188
%   4   4    0.002572   -0.002472
%   4   5   -0.079047    0.014327
%   4   6    0.102416   -0.007792
%   4   7   -0.053985   -0.032063
%   5   1    0.049493    0.039000
%   5   2   -0.038116   -0.022328
%   5   3    0.022548    0.018285
%   5   4   -0.010487   -0.007248
%   5   5   -0.044115    0.014030
%   5   6    0.056807   -0.008725
%   5   7    0.029018   -0.015771
%   6   1   -0.065438   -0.006884
%   6   2    0.007563    0.058502
%   6   3    0.004598   -0.077121
%   6   4   -0.028412    0.028443
%   6   5    0.031710   -0.032359
%   6   6   -0.054573   -0.031481
%   6   7    0.049633    0.022617
%   6   8   -0.008758   -0.123633
%   7   1    0.007826    0.113672
%   7   2   -0.031132   -0.081975
%   7   3    0.020347    0.056898
%   7   4    0.025359   -0.022643
%   7   5   -0.018023    0.001430
%   7   6   -0.168047    0.153976
%   7   7    0.172901   -0.091647
%   7   8   -0.024282    0.014844
%   8   1    0.008647    0.007849
%   8   2    0.033849   -0.041430
%   8   3   -0.017340    0.055167
%   8   4    0.005713   -0.000393
%   8   5   -0.062488   -0.002715
%   8   6    0.061882    0.002824
%   8   7   -0.039267    0.032556
%   9   1    0.047468   -0.026376
%   9   2    0.013949    0.009700
%   9   3    0.002068   -0.018750
%   9   4   -0.080058    0.049916
%   9   5    0.001749   -0.024398
%   9   6    0.072247   -0.012035
%   9   7   -0.008409   -0.019966
%  10   1   -0.002844    0.044461
%  10   2    0.028449   -0.054310
%  10   3   -0.013310    0.049275
%  10   4   -0.063004   -0.022087
%  10   5    0.060199    0.035701
%  10   6    0.023595   -0.007130
%  10   7   -0.027561    0.002881
%  10   8   -0.027888   -0.151008
%  11   1    0.040506    0.112322
%  11   2    0.070149   -0.012328
%  11   3   -0.049883   -0.047325
%  11   4   -0.027172    0.003210
%  11   5    0.040897   -0.034216
%  11   6   -0.011005    0.089843
%  11   7    0.012352   -0.050235
%  11   8    0.003093   -0.143680
%  12   1   -0.014488    0.141392
%  12   2   -0.023394   -0.099507
%  12   3   -0.004888    0.097433
%  12   4   -0.033700    0.022950
%  12   5    0.049386   -0.059532
%  12   6   -0.012757    0.054042
%  12   7   -0.048246    0.024140
%  13   1    0.057075   -0.015949
%  13   2    0.021994   -0.147777
%  13   3   -0.028242    0.129331
%  13   4    0.042461    0.079590
%  13   5    0.027648   -0.078495
%  13   6   -0.068133    0.007111
%  13   7   -0.019029    0.041731
%  14   1    0.039834    0.004103
%  14   2   -0.002020   -0.060535
%  14   3   -0.000927    0.099243
%  14   4   -0.000385   -0.004511
%  14   5    0.001997    0.008162
%  14   6   -0.000082    0.040546
%  14   7   -0.002337   -0.027829
%  14   8   -0.017742    0.028223
%  15   1    0.012388    0.018520
%  15   2    0.044181   -0.098403
%  15   3   -0.015445    0.145002
%  15   4   -0.069711    0.038350
%  15   5    0.061587   -0.039011
%  15   6   -0.001112   -0.043746
%  15   7   -0.020052    0.035556
%  15   8    0.019645   -0.134421
%  16   1   -0.021730    0.135260
%  16   2   -0.014351   -0.109934
%  16   3    0.031979    0.062401
%  16   4   -0.075720    0.082865
%  16   5    0.047563   -0.086610
%  16   6    0.012484    0.023642
%  16   7   -0.039474    0.082504
%   1   1    0.060349   -0.085917];
%   
 
% Golden du 2 septembre 2008, 56 vp dans les 2 plans, 200 mA 3/4 (AVEC modification BBA)

% Golden = [
%   1   2    0.014080    0.027995
%   1   3   -0.015017   -0.006834
%   1   4    0.001339   -0.018215
%   1   5   -0.098797    0.007590
%   1   6    0.120840    0.009376
%   1   7   -0.039947    0.138526
%   2   1    0.034321   -0.100989
%   2   2    0.044410    0.011604
%   2   3   -0.038116    0.041077
%   2   4    0.037042    0.047169
%   2   5   -0.021790   -0.087980
%   2   6   -0.074621    0.050033
%   2   7    0.076922   -0.015480
%   2   8    0.034039   -0.116260
%   3   1   -0.068354    0.026599
%   3   2   -0.035272    0.123393
%   3   3    0.028334   -0.196749
%   3   4   -0.015965   -0.016258
%   3   5    0.014401    0.027561
%   3   6   -0.025394   -0.009448
%   3   7    0.023784    0.002773
%   3   8    0.005618   -0.056352
%   4   1   -0.020089    0.046474
%   4   2    0.010287   -0.020217
%   4   3   -0.003262   -0.000739
%   4   4    0.001993    0.003699
%   4   5   -0.078392    0.022109
%   4   6    0.097169   -0.029574
%   4   7   -0.059339    0.007094
%   5   1    0.062428    0.012735
%   5   2   -0.041804   -0.029184
%   5   3    0.026528    0.031903
%   5   4   -0.014066    0.001641
%   5   5   -0.040464    0.012282
%   5   6    0.053852   -0.010373
%   5   7    0.029335   -0.001039
%   6   1   -0.060964   -0.014823
%   6   2    0.012427    0.053094
%   6   3   -0.001035   -0.072104
%   6   4   -0.027023    0.016122
%   6   5    0.030725   -0.022372
%   6   6   -0.048488   -0.017886
%   6   7    0.047154    0.015233
%   6   8   -0.013264   -0.111035
%   7   1    0.015598    0.106922
%   7   2   -0.042463   -0.079758
%   7   3    0.022318    0.056654
%   7   4    0.013974   -0.017714
%   7   5   -0.016369   -0.023448
%   7   6   -0.147282    0.128224
%   7   7    0.153143   -0.076973
%   7   8   -0.003325    0.046550
%   8   1   -0.030410   -0.016153
%   8   2    0.030121   -0.036981
%   8   3   -0.016824    0.065006
%   8   4    0.018036    0.001005
%   8   5   -0.074179   -0.006329
%   8   6    0.071340    0.007431
%   8   7   -0.027189    0.033314
%   9   1    0.020663   -0.030535
%   9   2    0.008529    0.010179
%   9   3    0.002444   -0.028136
%   9   4   -0.077035    0.051694
%   9   5   -0.000878   -0.011164
%   9   6    0.080845   -0.035218
%   9   7   -0.014411   -0.022028
%  10   1   -0.002121    0.050543
%  10   2    0.019680   -0.056096
%  10   3   -0.005566    0.056126
%  10   4   -0.049007   -0.006605
%  10   5    0.045128    0.013208
%  10   6    0.025164   -0.006298
%  10   7   -0.030589    0.001936
%  10   8   -0.023016   -0.143811
%  11   1    0.034480    0.098620
%  11   2    0.059635   -0.007838
%  11   3   -0.037433   -0.046675
%  11   4   -0.034446    0.001414
%  11   5    0.039605   -0.027117
%  11   6   -0.014043    0.090012
%  11   7    0.007863   -0.049324
%  11   8    0.004225   -0.135383
%  12   1   -0.010035    0.133013
%  12   2   -0.013248   -0.104747
%  12   3    0.009981    0.076090
%  12   4   -0.023320    0.016010
%  12   5    0.038207   -0.061615
%  12   6   -0.015499    0.057995
%  12   7   -0.040260    0.028347
%  13   1    0.056596   -0.019766
%  13   2    0.008288   -0.132306
%  13   3   -0.016487    0.129720
%  13   4    0.042462    0.072296
%  13   5    0.022989   -0.068868
%  13   6   -0.066653    0.012322
%  13   7   -0.022359    0.034639
%  14   1    0.051999    0.003575
%  14   2   -0.009229   -0.056179
%  14   3    0.006565    0.081081
%  14   4   -0.009191    0.003641
%  14   5    0.031496    0.017935
%  14   6   -0.001590    0.053899
%  14   7   -0.004041   -0.037862
%  14   8   -0.004698    0.025091
%  15   1    0.005901    0.014729
%  15   2    0.027293   -0.079146
%  15   3   -0.011109    0.113783
%  15   4   -0.055653    0.034036
%  15   5    0.052696   -0.039749
%  15   6    0.008478   -0.036938
%  15   7   -0.021645    0.029693
%  15   8    0.025802   -0.129670
%  16   1   -0.036393    0.124912
%  16   2   -0.006107   -0.098246
%  16   3    0.018415    0.045549
%  16   4   -0.075575    0.079506
%  16   5    0.046051   -0.083994
%  16   6    0.022178    0.022794
%  16   7   -0.018777    0.054936
%   1   1    0.020838   -0.058082
%       ];
% Golden du 22 janvier 2008, 56 vp dans les 2 plans, 200 mA 3/4 (AVEC modification BBA)

% Golden = [
%   1   2    0.001137    0.020286
%   1   3   -0.007885   -0.002782
%   1   4    0.013372   -0.016374
%   1   5   -0.110741    0.000870
%   1   6    0.116219    0.014824
%   1   7   -0.008404    0.151559
%   2   1   -0.014766   -0.112526
%   2   2    0.027201    0.015441
%   2   3   -0.022391    0.042599
%   2   4    0.035645    0.038730
%   2   5   -0.024297   -0.074306
%   2   6   -0.070021    0.051237
%   2   7    0.070637   -0.018583
%   2   8    0.033809   -0.101326
%   3   1   -0.067308    0.014939
%   3   2   -0.020015    0.129027
%   3   3    0.019403   -0.198117
%   3   4   -0.020633   -0.027931
%   3   5    0.017706    0.045542
%   3   6   -0.012646   -0.007170
%   3   7    0.010102   -0.002047
%   3   8   -0.003212   -0.029679
%   4   1   -0.001031    0.025833
%   4   2    0.013636   -0.013470
%   4   3   -0.003807    0.004624
%   4   4   -0.023971   -0.002292
%   4   5   -0.059656    0.014053
%   4   6    0.098861   -0.012846
%   4   7   -0.051330   -0.013594
%   5   1    0.051626    0.024360
%   5   2   -0.032833   -0.031515
%   5   3    0.013679    0.030105
%   5   4    0.039017    0.007369
%   5   5   -0.010362    0.003347
%   5   6   -0.031218   -0.006791
%   5   7   -0.009173    0.013580
%   6   1    0.021043   -0.020869
%   6   2    0.016468    0.040772
%   6   3   -0.010208   -0.049758
%   6   4   -0.029491    0.010271
%   6   5    0.034507   -0.015692
%   6   6   -0.042881   -0.006936
%   6   7    0.040813    0.007554
%   6   8   -0.010593   -0.100311
%   7   1    0.011235    0.094935
%   7   2   -0.031931   -0.067042
%   7   3    0.014855    0.044219
%   7   4    0.023529   -0.015931
%   7   5   -0.023330   -0.016535
%   7   6   -0.148193    0.093369
%   7   7    0.159398   -0.056313
%   7   8   -0.027269    0.017217
%   8   1    0.007903    0.002616
%   8   2    0.028833   -0.033703
%   8   3   -0.02 
%   8   4    0.028401   -0.010545
%   8   5   -0.060420   -0.002266
%   8   6    0.047888    0.013057
%   8   7   -0.042725    0.028089
%   9   1    0.047646   -0.028836
%   9   2    0.003468    0.003793
%   9   3    0.004181   -0.011428
%   9   4   -0.070738    0.030509
%   9   5    0.008953   -0.009745
%   9   6    0.066151   -0.016182
%   9   7   -0.036488    0.002906
%  10   1    0.036717    0.027358 
%   1   2    0.014080    0.027995
%  10   2    0.019544   -0.054169
%  10   3   -0.009078    0.065518
%  10   4   -0.058783   -0.019897
%  10   5    0.054836    0.030392
%  10   6    0.027918    0.004048
%  10   7   -0.034062   -0.007499
%  10   8   -0.026943   -0.140771
%  11   1    0.041422    0.095041
%  11   2    0.059925   -0.003561
%  11   3   -0.039014   -0.049822
%  11   4   -0.028460    0.001142
%  11   5    0.034204   -0.027257
%  11   6   -0.013114    0.092867
%  11   7    0.009262   -0.052272
%  11   8   -0.005570   -0.126547
%  12   1    0.002074    0.121140
%  12   2    0.019717   -0.092405
%  12   3   -0.009038    0.071242
%  12   4   -0.037759   -0.002998
%  12   5    0.053096   -0.041698
%  12   6   -0.016625    0.055760
%  12   7   -0.033902    0.027230
%  13   1    0.049066   -0.019389
%  13   2    0.000856   -0.120359
%  13   3   -0.010566    0.120867
%  13   4    0.037986    0.056823
%  13   5    0.023220   -0.059330
%  13   6   -0.065245    0.012920
%  13   7   -0.012015    0.024399
%  14   1    0.032654   -0.000683
%  14   2    0.000380   -0.034999
%  14   3   -0.001244    0.053492
%  14   4    0.006781    0.018426
%  14   5    0.019530    0.000311
%  14   6    0.002973    0.038450
%  14   7   -0.002681   -0.024865
%  14   8   -0.024247    0.038859
%  15   1    0.022845    0.002283
%  15   2    0.045624   -0.081376
%  15   3   -0.025782    0.123667
%  15   4   -0.057911    0.037201
%  15   5    0.059793   -0.037149
%  15   6    0.003004   -0.066812
%  15   7   -0.014295    0.047189
%  15   8    0.023029   -0.115488
%  16   1   -0.038348    0.108472
%  16   2    0.007818   -0.080603
%  16   3    0.006904    0.029538
%  16   4   -0.073669    0.078677
%  16   5    0.053688   -0.078394
%  16   6    0.014573    0.016682
%  16   7   -0.027156    0.043728
%   1   1    0.033352   -0.045016
%      ];


% Golden du 16 octobre 2007, 56 vp dans les 2 plans, 200 mA 3/4 (pas de modification BBA)
% NON APPLIQUEE

% Golden = [
%   1   2    0.014250   -0.024037
%   1   3   -0.016639    0.035907
%   1   4    0.019084    0.002410
%   1   5   -0.119830   -0.007753
%   1   6    0.120804    0.007976
%   1   7   -0.022937    0.137358
%   2   1    0.008113   -0.084322
%   2   2    0.023569   -0.011963
%   2   3   -0.016054    0.069334
%   2   4   -0.000527   -0.014190
%   2   5    0.008516    0.008846
%   2   6   -0.074727    0.028071
%   2   7    0.068064   -0.026147
%   2   8    0.048045   -0.117431
%   3   1   -0.088135    0.022480
%   3   2   -0.019006    0.131422
%   3   3    0.017430   -0.196451
%   3   4    0.013543   -0.021408
%   3   5   -0.019292    0.051431
%   3   6    0.001114   -0.035772
%   3   7   -0.006132    0.008605
%   3   8    0.022192   -0.049778
%   4   1   -0.041588    0.030503
%   4   2    0.024150   -0.010478
%   4   3   -0.014262    0.002526
%   4   4    0.014805    0.009626
%   4   5   -0.085479   -0.001239
%   4   6    0.081323    0.008092
%   4   7   -0.051622    0.001162
%   5   1    0.060193    0.019066
%   5   2   -0.032265   -0.054699
%   5   3    0.014591    0.045671
%   5   4    0.021652    0.031240
%   5   5    0.009123   -0.038685
%   5   6   -0.040344    0.005864
%   5   7   -0.013930    0.024692
%   6   1    0.028910   -0.031000
%   6   2    0.037889    0.011368
%   6   3   -0.020786   -0.013112
%   6   4   -0.017492   -0.031608
%   6   5    0.024303    0.057994
%   6   6   -0.021494   -0.001741
%   6   7    0.025597   -0.006960
%   6   8   -0.027287   -0.097723
%   7   1    0.039144    0.083252
%   7   2   -0.051188   -0.053537
%   7   3    0.027552    0.029429
%   7   4   -0.012157   -0.034078
%   7   5    0.015645    0.036651
%   7   6   -0.074393    0.064355
%   7   7    0.080066   -0.043736
%   7   8   -0.020104   -0.006878
%   8   1    0.008852    0.016000
%   8   2    0.030014   -0.023632
%   8   3   -0.016238    0.025648
%   8   4   -0.024379   -0.002892
%   8   5   -0.047320   -0.012616
%   8   6    0.083553    0.013076
%   8   7   -0.051579    0.025949
%   9   1    0.049475   -0.028640
%   9   2    0.015032   -0.000843
%   9   3   -0.004910   -0.006267
%   9   4   -0.068139    0.032493
%   9   5    0.024059   -0.017622
%   9   6    0.044610   -0.007918
%   9   7   -0.046033    0.003152
%  10   1    0.053186    0.033896
%  10   2    0.021647   -0.072365
%  10   3   -0.007898    0.087325
%  10   4   -0.085680   -0.033573
%  10   5    0.074480    0.043470
%  10   6    0.059256    0.022563
%  10   7   -0.073918   -0.022859
%  10   8   -0.013203   -0.127175
%  11   1    0.027794    0.092758
%  11   2    0.056501   -0.020985
%  11   3   -0.031653   -0.020443
%  11   4   -0.050838   -0.021920
%  11   5    0.047526    0.018614
%  11   6    0.018037    0.060469
%  11   7   -0.028532   -0.042040
%  11   8   -0.007344   -0.109655
%  12   1    0.014537    0.112859
%  12   2    0.026062   -0.106096
%  12   3   -0.014099    0.095858
%  12   4   -0.078599    0.011029
%  12   5    0.080972   -0.049948
%  12   6   -0.014364    0.054264
%  12   7   -0.026169    0.005699
%  13   1    0.039179    0.002087
%  13   2   -0.001059   -0.113836
%  13   3   -0.006785    0.102929
%  13   4    0.008160    0.073502
%  13   5    0.025690   -0.069686
%  13   6   -0.045332    0.006245
%  13   7   -0.007202    0.038880
%  14   1    0.012483   -0.008307
%  14   2    0.037463   -0.045823
%  14   3   -0.019618    0.072928
%  14   4   -0.062369   -0.008896
%  14   5    0.068684    0.017848
%  14   6    0.013944    0.042669
%  14   7   -0.021869   -0.027130
%  14   8   -0.009939    0.063369
%  15   1    0.011146   -0.008930
%  15   2    0.030784   -0.082335
%  15   3   -0.014717    0.124281
%  15   4   -0.075177    0.004151
%  15   5    0.076417    0.011248
%  15   6    0.009148   -0.073543
%  15   7   -0.017132    0.044834
%  15   8    0.011749   -0.107199
%  16   1   -0.023594    0.110669
%  16   2    0.012563   -0.087627
%  16   3    0.000590    0.035807
%  16   4   -0.068713    0.081797
%  16   5    0.052300   -0.084988
%  16   6    0.007680    0.012223
%  16   7   -0.020805    0.048784
%   1   1    0.022075   -0.047247
%     ];

% Golden du 16 octobre 2007, 56 vp dans les 2 plans, 15 mA 3/4 (pas de modification BBA)
% Golden = [
%   1   2   -0.013216   -0.027826
%   1   3   -0.049346    0.032357
%   1   4    0.050703   -0.006037
%   1   5   -0.074755   -0.014606
%   1   6    0.148497    0.003298
%   1   7   -0.052060    0.127654
%   2   1   -0.014701   -0.085177
%   2   2    0.023663    0.000074
%   2   3   -0.006484    0.071678
%   2   4    0.011874   -0.005673
%   2   5    0.019461    0.007869
%   2   6   -0.068565    0.021149
%   2   7    0.069517   -0.033841
%   2   8    0.035578   -0.130449
%   3   1   -0.087006    0.015884
%   3   2   -0.010647    0.135989
%   3   3    0.035445   -0.195345
%   3   4    0.008507   -0.015926
%   3   5   -0.030260    0.057907
%   3   6   -0.010282   -0.031601
%   3   7   -0.013055    0.005479
%   3   8    0.035137   -0.056158
%   4   1   -0.033174    0.027279
%   4   2    0.031098   -0.017763
%   4   3   -0.010996   -0.006689
%   4   4    0.019148    0.006622
%   4   5   -0.084048    0.003767
%   4   6    0.076598    0.005591
%   4   7   -0.044980   -0.000206
%   5   1    0.064866    0.032719
%   5   2   -0.025005   -0.033315
%   5   3    0.022582    0.048495
%   5   4    0.022997    0.048236
%   5   5    0.008154   -0.026770
%   5   6   -0.042516    0.011748
%   5   7   -0.015852    0.028418
%   6   1    0.031012   -0.024116
%   6   2    0.040578    0.012865
%   6   3    0.007113   -0.032278
%   6   4   -0.030506   -0.041209
%   6   5    0.001968    0.054260
%   6   6   -0.039713   -0.000211
%   6   7    0.009709    0.006414
%   6   8   -0.001258   -0.089711
%   7   1    0.052483    0.093898
%   7   2   -0.042973   -0.061011
%   7   3    0.034399    0.013801
%   7   4   -0.014947   -0.043601
%   7   5    0.005552    0.025685
%   7   6   -0.077666    0.062188
%   7   7    0.079602   -0.041600
%   7   8   -0.003175   -0.000261
%   8   1    0.020052    0.018562
%   8   2    0.014712   -0.018684
%   8   3   -0.035658    0.026641
%   8   4    0.000672   -0.012989
%   8   5   -0.022225   -0.010046
%   8   6    0.102277    0.014382
%   8   7   -0.066952    0.033618
%   9   1    0.030344   -0.031508
%   9   2    0.003853    0.007741
%   9   3   -0.011288   -0.008782
%   9   4   -0.049365    0.037559
%   9   5    0.048177   -0.015308
%   9   6    0.064303   -0.005617
%   9   7   -0.067421   -0.000845
%  10   1    0.039831    0.027090
%  10   2    0.027413   -0.083217
%  10   3    0.000595    0.074001
%  10   4   -0.078094   -0.035297
%  10   5    0.079200    0.039801
%  10   6    0.062145    0.034037
%  10   7   -0.069179   -0.006786
%  10   8   -0.015566   -0.114631
%  11   1    0.031082    0.106583
%  11   2    0.068894   -0.032609
%  11   3   -0.015247   -0.043588
%  11   4   -0.051730   -0.042036
%  11   5    0.034355    0.008844
%  11   6    0.007646    0.063354
%  11   7   -0.036452   -0.034497
%  11   8    0.004620   -0.100013
%  12   1    0.026440    0.125383
%  12   2    0.029218   -0.096968
%  12   3    0.001565    0.081549
%  12   4   -0.082537    0.002749
%  12   5    0.072557   -0.058901
%  12   6   -0.019963    0.034992
%  12   7   -0.008415   -0.013441
%  13   1    0.053534   -0.023102
%  13   2    0.017920   -0.137287
%  13   3    0.016333    0.089404
%  13   4    0.008133    0.068020
%  13   5    0.019022   -0.072647
%  13   6   -0.055726    0.000622
%  13   7    0.003350    0.043811
%  14   1    0.021489   -0.009066
%  14   2    0.053137   -0.040703
%  14   3    0.006410    0.066762
%  14   4   -0.081065   -0.018853
%  14   5    0.045687    0.016346
%  14   6   -0.005536    0.036588
%  14   7   -0.035399   -0.029316
%  14   8    0.017087    0.058026
%  15   1    0.026831   -0.008883
%  15   2    0.041477   -0.078628
%  15   3   -0.013789    0.124641
%  15   4   -0.077712    0.009852
%  15   5    0.070393    0.005401
%  15   6    0.006121   -0.084168
%  15   7   -0.014306    0.041356
%  15   8    0.030071   -0.122946
%  16   1   -0.014704    0.106849
%  16   2   -0.003412   -0.081677
%  16   3   -0.020850    0.035052
%  16   4   -0.043099    0.089381
%  16   5    0.086417   -0.078506
%  16   6    0.032080    0.018536
%  16   7   -0.032215    0.053356
%   1   1    0.010783   -0.041133
%   ];
%   
  
% Golden du 4 septembre 2007, 56 vp dans les 2 plans, 180 mA 3/4, appliquÃ©e jusqu'au 22 janvier 2008

% Golden = [
%   1   2   -0.028155   -0.029061
%   1   3    0.013719    0.043988
%   1   4    0.019645   -0.004169
%   1   5   -0.117808   -0.002250
%   1   6    0.112074    0.006225
%   1   7   -0.020643    0.148069
%   2   1    0.005932   -0.095248
%   2   2    0.028255   -0.010924
%   2   3   -0.021168    0.072671
%   2   4    0.011164   -0.008658
%   2   5   -0.001331    0.005682
%   2   6   -0.063211    0.029719
%   2   7    0.060709   -0.019280
%   2   8    0.033189   -0.135252
%   3   1   -0.065311    0.043423
%   3   2   -0.024153    0.112296
%   3   3    0.018468   -0.192310
%   3   4    0.003539   -0.032877
%   3   5   -0.007438    0.058995
%   3   6   -0.001470   -0.025232
%   3   7   -0.002557    0.005895
%   3   8    0.024512   -0.046841
%   4   1   -0.045051    0.036698
%   4   2    0.023707   -0.014033
%   4   3   -0.014549   -0.003107
%   4   4    0.028023    0.004955
%   4   5   -0.084968   -0.001111
%   4   6    0.071732   -0.004056
%   4   7   -0.054668   -0.009685
%   5   1    0.060069    0.021312
%   5   2   -0.026446   -0.051910
%   5   3    0.011145    0.042668
%   5   4    0.018607    0.030743
%   5   5    0.009025   -0.033932
%   5   6   -0.036105    0.009650
%   5   7   -0.011373    0.020727
%   6   1    0.019812   -0.020617
%   6   2    0.030137    0.015544
%   6   3   -0.019755   -0.011176
%   6   4   -0.016814   -0.036279
%   6   5    0.021300    0.062798
%   6   6   -0.033869   -0.020496
%   6   7    0.034157    0.002436
%   6   8   -0.024706   -0.086970
%   7   1    0.038440    0.079022
%   7   2   -0.051158   -0.052581
%   7   3    0.027865    0.031471
%   7   4   -0.001103   -0.030264
%   7   5    0.004723    0.027390
%   7   6   -0.076856    0.073380
%   7   7    0.081277   -0.049523
%   7   8   -0.019911    0.011277
%   8   1    0.011651   -0.000452
%   8   2    0.029399   -0.018054
%   8   3   -0.016106    0.039556
%   8   4   -0.025132   -0.023986
%   8   5   -0.040122   -0.023073
%   8   6    0.071941    0.052647
%   8   7   -0.034137    0.034574
%   9   1    0.029582   -0.048160
%   9   2   -0.001715   -0.004153
%   9   3    0.006582    0.004567
%   9   4   -0.063604    0.031046
%   9   5    0.017825   -0.014763
%   9   6    0.045621   -0.013067
%   9   7   -0.043268    0.000884
%  10   1    0.050019    0.031825
%  10   2    0.025095   -0.067253
%  10   3   -0.011250    0.080787
%  10   4   -0.088504   -0.035593
%  10   5    0.078382    0.048776
%  10   6    0.060877    0.027455
%  10   7   -0.074262   -0.024883
%  10   8   -0.015045   -0.122232
%  11   1    0.029280    0.088527
%  11   2    0.055201   -0.019245
%  11   3   -0.032614   -0.023472
%  11   4   -0.051235   -0.016294
%  11   5    0.049522    0.009738
%  11   6    0.023537    0.058872
%  11   7   -0.031031   -0.037680
%  11   8   -0.015675   -0.105767
%  12   1    0.028207    0.114489
%  12   2    0.008420   -0.109706
%  12   3   -0.001843    0.097681
%  12   4   -0.072880    0.004150
%  12   5    0.081682   -0.045407
%  12   6   -0.020481    0.050915
%  12   7   -0.029941    0.003168
%  13   1    0.042475    0.001778
%  13   2    0.006869   -0.109370
%  13   3   -0.011521    0.102175
%  13   4    0.012289    0.065081
%  13   5    0.022662   -0.059335
%  13   6   -0.041979    0.005662
%  13   7   -0.009720    0.033784
%  14   1    0.015994   -0.003658
%  14   2    0.039885   -0.039897
%  14   3   -0.022333    0.060993
%  14   4   -0.046682   -0.010402
%  14   5    0.043773    0.008220
%  14   6    0.022415    0.029266
%  14   7   -0.028542   -0.019251
%  14   8   -0.019982    0.066626
%  15   1    0.033433   -0.007538
%  15   2    0.019167   -0.089237
%  15   3   -0.006941    0.137022
%  15   4   -0.082210    0.005276
%  15   5    0.077842    0.008922
%  15   6    0.010460   -0.063753
%  15   7   -0.024270    0.037617
%  15   8    0.010000   -0.094053
%  16   1   -0.016074    0.101962
%  16   2    0.017409   -0.095570
%  16   3   -0.003457    0.057132
%  16   4   -0.067148    0.073524
%  16   5    0.047710   -0.078498
%  16   6    0.010775    0.019768
%  16   7   -0.018920    0.032869
%   1   1    0.023157   -0.029353
%   ];
% Golden du 1 septembre 2007, 56 vp dans les 2 plans
% Golden = [
%  1   2   -0.027858   -0.030542
%   1   3    0.012536    0.044968
%   1   4    0.021965    0.000777
%   1   5   -0.116693   -0.000956
%   1   6    0.108575    0.001924
%   1   7   -0.019895    0.147664
%   2   1    0.005398   -0.094941
%   2   2    0.026499   -0.007051
%   2   3   -0.020212    0.064585
%   2   4    0.009811   -0.005104
%   2   5   -0.000599   -0.000234
%   2   6   -0.062377    0.023774
%   2   7    0.059475   -0.014091
%   2   8    0.029575   -0.138797
%   3   1   -0.059724    0.048408
%   3   2   -0.022896    0.108484
%   3   3    0.016734   -0.189955
%   3   4    0.006129   -0.033593
%   3   5   -0.009674    0.056558
%   3   6   -0.003737   -0.022345
%   3   7   -0.000599    0.002688
%   3   8    0.030850   -0.054699
%   4   1   -0.056047    0.040489
%   4   2    0.026126   -0.010909
%   4   3   -0.015521   -0.009685
%   4   4    0.029599    0.006901
%   4   5   -0.083594    0.005124
%   4   6    0.069912   -0.013660
%   4   7   -0.054640   -0.018080
%   5   1    0.059450    0.030188
%   5   2   -0.025639   -0.046117
%   5   3    0.009803    0.033378
%   5   4    0.019496    0.025771
%   5   5    0.009493   -0.028455
%   5   6   -0.036233    0.005421
%   5   7   -0.011167    0.019038
%   6   1    0.020189   -0.022627
%   6   2    0.021822    0.022987
%   6   3   -0.015649   -0.020487
%   6   4   -0.009867   -0.035787
%   6   5    0.014022    0.063302
%   6   6   -0.034251   -0.024991
%   6   7    0.035588    0.004188
%   6   8   -0.025715   -0.093793
%   7   1    0.039350    0.080011
%   7   2   -0.051573   -0.047105
%   7   3    0.029089    0.022377
%   7   4   -0.010421   -0.038260
%   7   5    0.013739    0.040707
%   7   6   -0.082454    0.070593
%   7   7    0.085574   -0.050179
%   7   8   -0.019858    0.010144
%   8   1    0.010979   -0.000751
%   8   2    0.024929   -0.017113
%   8   3   -0.013282    0.039993
%   8   4   -0.022376   -0.030314
%   8   5   -0.043184   -0.018109
%   8   6    0.071274    0.052574
%   8   7   -0.029894    0.040403
%   9   1    0.023808   -0.053312
%   9   2   -0.005289   -0.001243
%   9   3    0.009312    0.001261
%   9   4   -0.062275    0.033270
%   9   5    0.012478   -0.013254
%   9   6    0.049760   -0.017662
%   9   7   -0.043038   -0.002186
%  10   1    0.049126    0.033606
%  10   2    0.027092   -0.064088
%  10   3   -0.012791    0.074454
%  10   4   -0.086155   -0.035600
%  10   5    0.076398    0.046566
%  10   6    0.059197    0.031879
%  10   7   -0.072315   -0.027331
%  10   8   -0.018011   -0.122009
%  11   1    0.034009    0.085840
%  11   2    0.054316   -0.012872
%  11   3   -0.033359   -0.031390
%  11   4   -0.045219   -0.018371
%  11   5    0.043686    0.011151
%  11   6    0.021998    0.061303
%  11   7   -0.029207   -0.040006
%  11   8   -0.014762   -0.109270
%  12   1    0.027404    0.116932
%  12   2    0.001844   -0.110700
%  12   3    0.002366    0.096270
%  12   4   -0.072004    0.008529
%  12   5    0.080153   -0.043381
%  12   6   -0.020794    0.044043
%  12   7   -0.028838    0.004427
%  13   1    0.041599    0.003023
%  13   2    0.004953   -0.111264
%  13   3   -0.011826    0.100701
%  13   4    0.016131    0.067533
%  13   5    0.019653   -0.058397
%  13   6   -0.042259   -0.000127
%  13   7   -0.008728    0.037806
%  14   1    0.015633   -0.009469
%  14   2    0.034222   -0.031606
%  14   3   -0.018602    0.053046
%  14   4   -0.054360   -0.014683
%  14   5    0.050846    0.016620
%  14   6    0.023082    0.026700
%  14   7   -0.030703   -0.019320
%  14   8   -0.015381    0.073187
%  15   1    0.025624   -0.013209
%  15   2    0.020657   -0.088825
%  15   3   -0.008477    0.139241
%  15   4   -0.079408    0.010916
%  15   5    0.075600    0.003128
%  15   6    0.012343   -0.072734
%  15   7   -0.024977    0.044762
%  15   8    0.008240   -0.098319
%  16   1   -0.014611    0.104539
%  16   2    0.016501   -0.094690
%  16   3   -0.002599    0.051623
%  16   4   -0.065708    0.076135
%  16   5    0.047496   -0.080684
%  16   6    0.009583    0.017959
%  16   7   -0.020536    0.032306
%   1   1    0.025425   -0.028610
%   ];
% Golden du 3 juillet 2007
% Golden = [
%   1   2   -0.007594   -0.021714
%   1   3    0.002187   -0.017964
%   1   4    0.021211    0.047716
%   1   5   -0.113478    0.064429
%   1   6    0.112499    0.073170
%   1   7   -0.020111    0.145255
%   2   1    0.008120   -0.115766
%   2   2    0.024954   -0.044238
%   2   3   -0.016520    0.050018
%   2   4    0.016876    0.116967
%   2   5   -0.005394    0.058243
%   2   6   -0.075553   -0.029812
%   2   7    0.074715   -0.122298
%   2   8    0.039947   -0.077009
%   3   1   -0.077658    0.059490
%   3   2    0.004989    0.111802
%   3   3    0.002775   -0.216281
%   3   4    0.011065   -0.032990
%   3   5   -0.011767    0.065500
%   3   6    0.002669   -0.084588
%   3   7   -0.004318   -0.142722
%   3   8    0.031123    0.001873
%   4   1   -0.052921    0.108021
%   4   2    0.039392   -0.025069
%   4   3   -0.022416   -0.018715
%   4   4    0.032308   -0.051938
%   4   5   -0.084679   -0.058199
%   4   6    0.070868   -0.049164
%   4   7   -0.046716    0.037040
%   5   1    0.056959    0.076846
%   5   2   -0.037686   -0.101103
%   5   3    0.019103    0.007918
%   5   4    0.024799    0.056612
%   5   5    0.006841    0.022291
%   5   6   -0.034068    0.060166
%   5   7   -0.011246    0.033728
%   6   1    0.023896   -0.023504
%   6   2    0.023624   -0.016600
%   6   3   -0.017607   -0.068162
%   6   4   -0.003732    0.072421
%   6   5    0.013229    0.123402
%   6   6   -0.021602   -0.013558
%   6   7    0.031255   -0.012124
%   6   8   -0.029779   -0.054105
%   7   1    0.046242    0.101884
%   7   2   -0.050508   -0.097106
%   7   3    0.029047   -0.007003
%   7   4   -0.018461    0.041510
%   7   5    0.034468    0.113356
%   7   6   -0.119989    0.084767
%   7   7    0.129120   -0.068006
%   7   8   -0.008859    0.012006
%   8   1   -0.017642    0.001583
%   8   2    0.024293   -0.025482
%   8   3   -0.005824    0.020921
%   8   4   -0.022549    0.011018
%   8   5   -0.048098    0.011870
%   8   6    0.077986    0.055401
%   8   7   -0.033353    0.029691
%   9   1    0.029658   -0.058526
%   9   2   -0.002483   -0.024911
%   9   3    0.017211    0.020764
%   9   4   -0.072618    0.028057
%   9   5    0.009373   -0.004307
%   9   6    0.060295   -0.002043
%   9   7   -0.042154    0.055012
%  10   1    0.052610    0.078645
%  10   2    0.024697   -0.151877
%  10   3   -0.006689    0.006608
%  10   4   -0.080909    0.085086
%  10   5    0.067788    0.135524
%  10   6    0.060652    0.139244
%  10   7   -0.073424    0.116710
%  10   8   -0.016015   -0.153248
%  11   1    0.040074    0.055833
%  11   2    0.054969   -0.060763
%  11   3   -0.033153   -0.047759
%  11   4   -0.035734    0.149436
%  11   5    0.037102    0.137872
%  11   6    0.021631    0.082188
%  11   7   -0.023231   -0.052305
%  11   8   -0.015020   -0.107837
%  12   1    0.028622    0.100118
%  12   2    0.012981   -0.105963
%  12   3   -0.007092    0.097425
%  12   4   -0.063902    0.031108
%  12   5    0.077869   -0.024295
%  12   6   -0.012544    0.064029
%  12   7   -0.027216    0.009903
%  13   1    0.036883    0.000296
%  13   2   -0.023359   -0.178520
%  13   3   -0.004168    0.050205
%  13   4    0.081404    0.137223
%  13   5   -0.010547    0.045264
%  13   6   -0.064022    0.113455
%  13   7   -0.009638    0.030705
%  14   1    0.022829   -0.018488
%  14   2    0.029441   -0.101603
%  14   3   -0.020897    0.016530
%  14   4   -0.058407    0.157072
%  14   5    0.065698    0.105188
%  14   6    0.022049    0.028387
%  14   7   -0.019035   -0.104373
%  14   8   -0.020298    0.086102
%  15   1    0.020547    0.004138
%  15   2    0.030194   -0.082926
%  15   3   -0.016974    0.125642
%  15   4   -0.065261    0.011502
%  15   5    0.068834   -0.008313
%  15   6    0.020477   -0.065407
%  15   7   -0.026192    0.021900
%  15   8    0.004562   -0.055489
%  16   1   -0.014757    0.133870
%  16   2    0.014734   -0.153631
%  16   3   -0.000427   -0.003731
%  16   4   -0.066313    0.144939
%  16   5    0.048631    0.040035
%  16   6    0.011340    0.106006
%  16   7   -0.010801   -0.006162
%   1   1    0.011340   -0.098546
%   ];



% Golden = [
%   1   2   -0.000000   -0.000000
%   1   3   -0.000000   -0.000000
%   1   4    0.000000    0.000000
%   1   5   -0.000000   -0.000000
%   1   6   -0.000000    0.000000
%   1   7    0.000000   -0.000000
%   2   1    0.000000    0.000000
%   2   2    0.000000    0.000000
%   2   3   -0.000000   -0.000000
%   2   4    0.000000   -0.000000
%   2   5   -0.000000    0.000000
%   2   6    0.000000   -0.000000
%   2   7    0.000000   -0.000000
%   2   8    0.000000   -0.000000
%   3   1    0.000000    0.000000
%   3   2   -0.000000   -0.000000
%   3   3    0.000000    0.000000
%   3   4   -0.000000   -0.000000
%   3   5    0.000000   -0.000000
%   3   6    0.000000    0.000000
%   3   7    0.000000   -0.000000
%   3   8    0.000000    0.000000
%   4   1    0.000000   -0.000000
%   4   2    0.000000   -0.000000
%   4   3   -0.000000   -0.000000
%   4   4    0.000000    0.000000
%   4   5    0.000000    0.000000
%   4   6   -0.000000    0.000000
%   4   7   -0.000000   -0.000000
%   5   1   -0.000000    0.000000
%   5   2   -0.000000    0.000000
%   5   3    0.000000    0.000000
%   5   4    0.000000   -0.000000
%   5   5   -0.000000   -0.000000
%   5   6    0.000000    0.000000
%   5   7   -0.000000   -0.000000
%   6   1   -0.000000    0.000000
%   6   2    0.000000   -0.000000
%   6   3    0.000000   -0.000000
%   6   4   -0.000000    0.000000
%   6   5    0.000000    0.000000
%   6   6   -0.000000   -0.000000
%   6   7   -0.000000    0.000000
%   6   8    0.000000    0.000000
%   7   1   -0.000000   -0.000000
%   7   2   -0.000000   -0.000000
%   7   3   -0.000000    0.000000
%   7   4    0.000000   -0.000000
%   7   5   -0.000000   -0.000000
%   7   6    0.000000   -0.000000
%   7   7    0.000000    0.000000
%   7   8    0.000000    0.000000
%   8   1   -0.000000   -0.000000
%   8   2   -0.000000    0.000000
%   8   3    0.000000   -0.000000
%   8   4    0.000000   -0.000000
%   8   5    0.000000   -0.000000
%   8   6    0.000000   -0.000000
%   8   7   -0.000000   -0.000000
%   9   1   -0.000000   -0.000000
%   9   2   -0.000000   -0.000000
%   9   3    0.000000    0.000000
%   9   4   -0.000000    0.000000
%   9   5    0.000000   -0.000000
%   9   6    0.000000    0.000000
%   9   7   -0.000000    0.000000
%  10   1    0.000000   -0.000000
%  10   2    0.000000   -0.000000
%  10   3   -0.000000    0.000000
%  10   4   -0.000000    0.000000
%  10   5   -0.000000    0.000000
%  10   6   -0.000000   -0.000000
%  10   7    0.000000   -0.000000
%  10   8    0.000000    0.000000
%  11   1    0.000000   -0.000000
%  11   2    0.000000   -0.000000
%  11   3    0.000000    0.000000
%  11   4   -0.000000    0.000000
%  11   5   -0.000000   -0.000000
%  11   6    0.000000   -0.000000
%  11   7   -0.000000   -0.000000
%  11   8    0.000000   -0.000000
%  12   1   -0.000000    0.000000
%  12   2   -0.000000    0.000000
%  12   3    0.000000    0.000000
%  12   4    0.000000   -0.000000
%  12   5   -0.000000    0.000000
%  12   6    0.000000   -0.000000
%  12   7    0.000000    0.000000
%  13   1   -0.000000   -0.000000
%  13   2    0.000000   -0.000000
%  13   3   -0.000000    0.000000
%  13   4    0.000000    0.000000
%  13   5   -0.000000   -0.000000
%  13   6    0.000000   -0.000000
%  13   7    0.000000    0.000000
%  14   1   -0.000000    0.000000
%  14   2    0.000000   -0.000000
%  14   3   -0.000000   -0.000000
%  14   4    0.000000    0.000000
%  14   5   -0.000000    0.000000
%  14   6   -0.000000    0.000000
%  14   7   -0.000000   -0.000000
%  14   8    0.000000    0.000000
%  15   1   -0.000000    0.000000
%  15   2    0.000000    0.000000
%  15   3   -0.000000   -0.000000
%  15   4   -0.000000    0.000000
%  15   5   -0.000000   -0.000000
%  15   6    0.000000   -0.000000
%  15   7    0.000000    0.000000
%  15   8   -0.000000   -0.000000
%  16   1   -0.000000   -0.000000
%  16   2    0.000000    0.000000
%  16   3   -0.000000   -0.000000
%  16   4    0.000000    0.000000
%  16   5   -0.000000   -0.000000
%  16   6   -0.000000    0.000000
%  16   7    0.000000   -0.000000
%   1   1   -0.000000    0.000000
% ];

%  Golden = [
%   1   2   -0.016109   -0.029329
%   1   3   -0.004005    0.007886
%   1   4    0.072212    0.062367
%   1   5   -0.024518    0.055139
%   1   6   -0.064471    0.013784
%   1   7    0.013930    0.135550
%   2   1   -0.011554   -0.070312
%   2   2   -0.050394   -0.078194
%   2   3    0.032176    0.082341
%   2   4   -0.029317    0.117253
%   2   5    0.015890   -0.007759
%   2   6    0.016312    0.063036
%   2   7   -0.032808   -0.177137
%   2   8    0.039461   -0.045681
%   3   1   -0.062566   -0.018802
%   3   2   -0.006602    0.184429
%   3   3    0.018723   -0.176785
%   3   4   -0.116008   -0.105529
%   3   5    0.097402   -0.028190
%   3   6    0.045757   -0.097066
%   3   7   -0.066598   -0.175309
%   3   8   -0.007365    0.036844
%   4   1    0.019824    0.142689
%   4   2   -0.000176   -0.068519
%   4   3   -0.006800   -0.041942
%   4   4   -0.034004   -0.022842
%   4   5    0.037034    0.059478
%   4   6   -0.014433   -0.166529
%   4   7   -0.061407    0.017149
%   5   1    0.080897    0.076740
%   5   2   -0.012038    0.040356
%   5   3   -0.013179   -0.002226
%   5   4    0.069290   -0.111001
%   5   5    0.038943    0.008662
%   5   6   -0.129630    0.011481
%   5   7   -0.000920    0.036813
%   6   1    0.016272   -0.052574
%   6   2    0.031234    0.061241
%   6   3   -0.009122   -0.018313
%   6   4   -0.159022    0.006281
%   6   5    0.139226   -0.058757
%   6   6    0.100120   -0.090672
%   6   7   -0.129504   -0.062824
%   6   8    0.006805   -0.001487
%   7   1    0.006808    0.106658
%   7   2   -0.029256   -0.086426
%   7   3    0.005422    0.016194
%   7   4    0.055600    0.003513
%   7   5   -0.042632   -0.001866
%   7   6   -0.126872    0.026082
%   7   7    0.140233   -0.081977
%   7   8   -0.019330    0.079127
%   8   1   -0.012088   -0.008979
%   8   2    0.053612   -0.018187
%   8   3   -0.014107    0.028316
%   8   4   -0.157538   -0.013089
%   8   5   -0.046621   -0.070002
%   8   6    0.207227    0.065337
%   8   7   -0.054266    0.021793
%   9   1    0.017264   -0.015635
%   9   2    0.030052   -0.042293
%   9   3   -0.025084    0.007168
%   9   4   -0.020432    0.075486
%   9   5    0.110440   -0.049762
%   9   6   -0.116610   -0.012532
%   9   7   -0.011486    0.106592
%  10   1    0.024628    0.050707
%  10   2    0.021953   -0.143527
%  10   3   -0.013703   -0.001596
%  10   4   -0.049724    0.108386
%  10   5    0.030980    0.098887
%  10   6    0.108697    0.146079
%  10   7   -0.121358    0.133295
%  10   8   -0.033342   -0.219970
%  11   1    0.062153    0.036616
%  11   2    0.078471    0.011798
%  11   3   -0.032535   -0.064236
%  11   4   -0.225522    0.118926
%  11   5    0.211643    0.060435
%  11   6    0.046324    0.023765
%  11   7   -0.084590   -0.050827
%  11   8    0.003712   -0.108471
%  12   1    0.002993    0.105625
%  12   2    0.004842   -0.084366
%  12   3   -0.001208    0.108071
%  12   4   -0.083760   -0.032507
%  12   5    0.084936   -0.059238
%  12   6   -0.024508    0.018948
%  12   7   -0.029985    0.033101
%  13   1    0.043133    0.026918
%  13   2   -0.034035   -0.122866
%  13   3    0.000740    0.025621
%  13   4    0.095937    0.106642
%  13   5    0.045263    0.013359
%  13   6   -0.171956    0.045738
%  13   7    0.031641    0.052087
%  14   1   -0.027082   -0.000829
%  14   2    0.021254   -0.099073
%  14   3   -0.019560    0.017342
%  14   4   -0.004520    0.162526
%  14   5    0.005853    0.059292
%  14   6    0.008179   -0.047524
%  14   7   -0.001401   -0.102715
%  14   8   -0.055800    0.098121
%  15   1    0.074795    0.023368
%  15   2    0.038731   -0.095221
%  15   3   -0.026061    0.115497
%  15   4   -0.088833   -0.011771
%  15   5    0.084328    0.015239
%  15   6    0.037638   -0.097017
%  15   7   -0.055234   -0.009165
%  15   8    0.021390   -0.027726
%  16   1   -0.036755    0.149702
%  16   2   -0.002163   -0.181339
%  16   3    0.014132    0.013743
%  16   4   -0.133713    0.140012
%  16   5    0.094131    0.004707
%  16   6    0.016469    0.147084
%  16   7   -0.008833   -0.018775
%   1   1    0.001073   -0.109009
% Golden du 27 mars 2007 100 mA

% Golden = [
%   1   2   -0.005797   -0.017312
%   1   3    0.000919   -0.015076
%   1   4    0.016414    0.026510
%   1   5   -0.121497    0.059608
%   1   6    0.119201    0.067078
%   1   7   -0.010365    0.157920
%   2   1   -0.013197   -0.098301
%   2   2    0.027261   -0.062963
%   2   3   -0.015684    0.035225
%   2   4    0.012233    0.143302
%   2   5   -0.009376    0.059353
%   2   6   -0.071856    0.011451
%   2   7    0.063409   -0.081142
%   2   8    0.034665   -0.063435
%   3   1   -0.066037    0.002726
%   3   2   -0.000859    0.136815
%   3   3    0.005278   -0.203492
%   3   4    0.012004   -0.012404
%   3   5   -0.019908    0.055614
%   3   6   -0.002221   -0.089575
%   3   7   -0.001831   -0.125075
%   3   8    0.002154    0.032087
%   4   1   -0.003133    0.071877
%   4   2    0.014281   -0.002156
%   4   3   -0.008564   -0.015328
%   4   4   -0.006155   -0.087721
%   4   5   -0.072595   -0.035561
%   4   6    0.089426   -0.031385
%   4   7   -0.051357    0.030795
%   5   1    0.052918    0.072909
%   5   2   -0.030896   -0.097133
%   5   3    0.008768    0.026373
%   5   4    0.033223    0.053668
%   5   5   -0.007694    0.023427
%   5   6   -0.029986    0.035607
%   5   7   -0.002398    0.014364
%   6   1    0.005333   -0.030141
%   6   2    0.019401    0.023267
%   6   3   -0.013229   -0.071641
%   6   4   -0.038290    0.057127
%   6   5    0.044439    0.032870
%   6   6   -0.033921   -0.006965
%   6   7    0.034656    0.009146
%   6   8   -0.020039   -0.026317
%   7   1    0.021750    0.100296
%   7   2   -0.044471   -0.118606
%   7   3    0.023370   -0.006066
%   7   4   -0.010557    0.077161
%   7   5    0.023680    0.111695
%   7   6   -0.116233    0.089491
%   7   7    0.124305   -0.080779
%   7   8   -0.015208    0.036310
%   8   1   -0.010741   -0.009796
%   8   2    0.020530   -0.024978
%   8   3   -0.008444    0.043667
%   8   4   -0.025326   -0.012376
%   8   5   -0.035473    0.007320
%   8   6    0.065758    0.055126
%   8   7   -0.036453    0.023429
%   9   1    0.029467   -0.053588
%   9   2    0.001633   -0.006234
%   9   3    0.005615    0.007891
%   9   4   -0.062090   -0.002766
%   9   5    0.001910    0.010628
%   9   6    0.058936    0.007838
%   9   7   -0.042798    0.072547
%  10   1    0.045262    0.073408
%  10   2    0.028056   -0.154367
%  10   3   -0.012088   -0.011784
%  10   4   -0.073822    0.088175
%  10   5    0.057591    0.178638
%  10   6    0.064805    0.124674
%  10   7   -0.081578    0.138036
%  10   8   -0.024542   -0.172935
%  11   1    0.046510    0.017614
%  11   2    0.062073    0.000009
%  11   3   -0.037480   -0.060397
%  11   4   -0.041096    0.146472
%  11   5    0.032956    0.081438
%  11   6    0.018984    0.048677
%  11   7   -0.033127   -0.079176
%  11   8   -0.010214   -0.112762
%  12   1    0.024785    0.113373
%  12   2   -0.004146   -0.072407
%  12   3    0.005921    0.070591
%  12   4   -0.068544   -0.015913
%  12   5    0.069606   -0.052789
%  12   6   -0.014413    0.044067
%  12   7   -0.033711    0.029776
%  13   1    0.049872    0.020370
%  13   2   -0.026395   -0.149377
%  13   3    0.003372    0.039093
%  13   4    0.062395    0.114775
%  13   5   -0.023654    0.034983
%  13   6   -0.046041    0.074071
%  13   7    0.015033    0.025198
%  14   1   -0.017785    0.007114
%  14   2    0.014314   -0.103821
%  14   3   -0.007818    0.005819
%  14   4   -0.046975    0.152992
%  14   5    0.040307    0.106990
%  14   6    0.024887    0.000457
%  14   7   -0.030497   -0.099698
%  14   8   -0.015308    0.088554
%  15   1    0.022300    0.006944
%  15   2    0.023296   -0.082432
%  15   3   -0.012034    0.129612
%  15   4   -0.076630    0.008548
%  15   5    0.074119   -0.030656
%  15   6    0.014081   -0.082962
%  15   7   -0.026948    0.012078
%  15   8    0.013663   -0.070121
%  16   1   -0.026518    0.147481
%  16   2    0.002385   -0.127930
%  16   3    0.006044   -0.040092
%  16   4   -0.066371    0.131605
%  16   5    0.044381    0.027604
%  16   6    0.013263    0.111982
%  16   7   -0.018533   -0.015504
%   1   1    0.014970   -0.090054
%  ];

% Golden ci dessous orbite su 23 avril 2007
% Golden = [
%   1   2   -0.006983   -0.027905
%   1   3   -0.003142   -0.015461
%   1   4    0.016421    0.040723
%   1   5   -0.114860    0.070363
%   1   6    0.114470    0.077863
%   1   7   -0.017136    0.142959
%   2   1   -0.002380   -0.119856
%   2   2    0.020268   -0.037834
%   2   3   -0.016944    0.036558
%   2   4    0.004818    0.125072
%   2   5    0.003800    0.055701
%   2   6   -0.067714   -0.020920
%   2   7    0.064931   -0.124026
%   2   8    0.030373   -0.049196
%   3   1   -0.067756    0.042688
%   3   2    0.005212    0.106980
%   3   3   -0.000994   -0.206161
%   3   4    0.008764   -0.040780
%   3   5   -0.012362    0.065270
%   3   6    0.008649   -0.071649
%   3   7   -0.010105   -0.116539
%   3   8    0.004831    0.033727
%   4   1   -0.012951    0.069927
%   4   2    0.018719   -0.016921
%   4   3   -0.013218   -0.023794
%   4   4    0.025955   -0.031911
%   4   5   -0.087577   -0.055929
%   4   6    0.073246   -0.043108
%   4   7   -0.051022    0.013304
%   5   1    0.054389    0.066226
%   5   2   -0.030216   -0.082380
%   5   3    0.014207    0.015782
%   5   4    0.018652    0.040588
%   5   5    0.007467    0.018579
%   5   6   -0.035390    0.023740
%   5   7   -0.011514    0.007948
%   6   1    0.021412   -0.013609
%   6   2    0.019742    0.016051
%   6   3   -0.014610   -0.070518
%   6   4   -0.012430    0.038418
%   6   5    0.011704    0.077612
%   6   6   -0.021676   -0.055172
%   6   7    0.019028   -0.064592
%   6   8   -0.017033   -0.085768
%   7   1    0.027233    0.110291
%   7   2   -0.044686   -0.035923
%   7   3    0.025766   -0.035378
%   7   4   -0.018853   -0.022782
%   7   5    0.026612    0.040724
%   7   6   -0.119231    0.069974
%   7   7    0.122805   -0.064904
%   7   8   -0.016395    0.040026
%   8   1   -0.002400   -0.010129
%   8   2    0.022870   -0.036095
%   8   3   -0.014112    0.035739
%   8   4   -0.020547    0.006218
%   8   5   -0.043064    0.018189
%   8   6    0.071954    0.071437
%   8   7   -0.034260    0.040567
%   9   1    0.028537   -0.064069
%   9   2   -0.010896   -0.014936
%   9   3    0.012216   -0.004277
%   9   4   -0.068464    0.027311
%   9   5    0.009988    0.010894
%   9   6    0.063530    0.017090
%   9   7   -0.053769    0.063549
%  10   1    0.062576    0.054632
%  10   2    0.017741   -0.146863
%  10   3   -0.010410   -0.007074
%  10   4   -0.074049    0.078694
%  10   5    0.064659    0.188452
%  10   6    0.069572    0.152665
%  10   7   -0.079175    0.118005
%  10   8   -0.029903   -0.160250
%  11   1    0.050193    0.037427
%  11   2    0.055632   -0.042655
%  11   3   -0.035655   -0.054405
%  11   4   -0.036151    0.149242
%  11   5    0.035143    0.116151
%  11   6    0.028927    0.086803
%  11   7   -0.035726   -0.064940
%  11   8   -0.011351   -0.092493
%  12   1    0.021298    0.086223
%  12   2    0.018891   -0.081681
%  12   3   -0.006108    0.093825
%  12   4   -0.073790   -0.017286
%  12   5    0.077053   -0.049686
%  12   6   -0.015203    0.047587
%  12   7   -0.030149    0.022547
%  13   1    0.046047    0.008691
%  13   2   -0.022754   -0.168597
%  13   3    0.003678    0.050055
%  13   4    0.056820    0.126502
%  13   5   -0.025694    0.041400
%  13   6   -0.039813    0.096348
%  13   7    0.014860    0.042901
%  14   1   -0.018464   -0.017584
%  14   2    0.025080   -0.093626
%  14   3   -0.012440    0.002405
%  14   4   -0.057945    0.140560
%  14   5    0.051345    0.101783
%  14   6    0.024152    0.011598
%  14   7   -0.033475   -0.116291
%  14   8   -0.001281    0.101211
%  15   1   -0.002106    0.013078
%  15   2    0.019324   -0.106988
%  15   3   -0.007300    0.136143
%  15   4   -0.070168    0.021354
%  15   5    0.067422   -0.004631
%  15   6    0.013393   -0.080552
%  15   7   -0.025907    0.012427
%  15   8    0.006644   -0.071089
%  16   1   -0.015333    0.132740
%  16   2    0.007727   -0.131224
%  16   3    0.002030   -0.023141
%  16   4   -0.067771    0.132597
%  16   5    0.044049    0.024878
%  16   6    0.014250    0.105506
%  16   7   -0.017992   -0.006555
%   1   1    0.017883   -0.087854
%   ];


setfamilydata(Golden(:,3),'BPMx','Golden',Golden(:,1:2));
setfamilydata(Golden(:,4),'BPMz','Golden',Golden(:,1:2));
end

%% LOCAL FUNCTIONS
function local_tango_kill_allgroup(AO)
% kill all group if exist
FamilyList = getfamilylist('Cell');
for k=1:length(FamilyList)
    if isfield(AO.(FamilyList{k}), 'GroupId'),
        tango_group_kill(AO.(FamilyList{k}).GroupId);
    end
end
end

function setfamilydata_local(Family)
% set all data in one command

if ismemberof(Family,'QUAD') || ismemberof(Family,'COR') || ...
        ismemberof(Family,'COR') || ismemberof(Family,'SEXT') || ...
        ismemberof(Family,'BPM') || ismemberof(Family,'BEND')
    setfamilydata(1,Family,'Gain');
    setfamilydata(0,Family,'Offset');
    setfamilydata(0,Family,'Coupling');
end

if ismemberof(Family,'BPM')
    setfamilydata(0.001,Family,'Sigma');
    setfamilydata(0.0,Family,'Golden');
    setfamilydata(measdisp(Family,'struct','model'),Family,'Dispersion'); % needed for orbit correction w/ RF
end

% Order fields for all families
AO = getao;
Familylist = getfamilylist;
for k = 1:length(Familylist),
    FamilyName = deblank(Familylist(k,:));
    AO.(FamilyName) = orderfields(AO.(FamilyName));
    FieldNamelist = fieldnames(AO.(FamilyName));
    for k1 = 1:length(FieldNamelist);
        FieldName = FieldNamelist{k1};
        if isstruct((AO.(FamilyName).(FieldName)))
            AO.(FamilyName).(FieldName) = orderfields(AO.(FamilyName).(FieldName));
        end
    end
end
setao(AO)
end

