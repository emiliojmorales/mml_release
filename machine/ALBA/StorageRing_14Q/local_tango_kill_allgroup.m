function local_tango_kill_allgroup(AO)
FN=fieldnames(AO);
nn=numel(FN);
for ii=1:nn
    if isfield(AO.(FN{ii}),'GrupId')
        tango_group_kill(AO.(FN{ii}).GrupId);
    end
end
end