function [LocoMeasData, BPMData, CMData, RINGData, FitParameters, LocoFlags] = buildlocofitparameters_auto(varargin)
%BUILDLOCOFITPARAMETERS - ALBA LOCO fit parameters
%
%  [LocoMeasData, BPMData, CMData, RINGData, FitParameters, LocoFlags] = buildlocoinput(FileName,Coupling_mode_fit,Quad_mode_fit)

%%%%%%%%%%%%%%
% Input file %
%%%%%%%%%%%%%%

% Quad_mode_fit
%1->'Fit by 14 quad families'
%2->'Fit 112 quad power supplies'
%3->'Fit 112 quads and 1 bend power supply'
%4->'Fit 112 quads + 32 bends'
%5->'Fit 112 quads + 32 bends+ 120 sexts'
%6->'Fit 32 bends only'

%Coupling_mode_fit
%1->'Fit by Skew Quad power supply',
%2->'Fit by 32 Skews',
%3->'Fit at Sextupoles', ...           
%4->'Fit by tilt dipoles',
%5->'Fit by tilt quads',
%6->'Fit sext, tilt quads, tilt dipoles',...           
%7->'Fit skews, sext, tilt quads, tilt dipoles',
%8->'Do Not Fit Skew Quadrupoles'

Coupling_mode_fit=3;
Quad_mode_fit=2;

if nargin == 0
    [FileName, DirectoryName, FilterIndex] = uigetfile('*.mat','Select a LOCO input file');
    if FilterIndex == 0
        return;
    end
    FileName = [DirectoryName, FileName];
elseif nargin==1
    FileName=varargin{1};
elseif nargin==2
    FileName=varargin{1};
    Coupling_mode_fit=varargin{2};
elseif nargin==3
    FileName=varargin{1};
    Coupling_mode_fit=varargin{2};
    Quad_mode_fit=varargin{3};
end

load(FileName);

%%%%%%%%%%%%%%%%%%%%%%
% Remove bad devices %
%%%%%%%%%%%%%%%%%%%%%%
RemoveHCMDeviceList = [];
RemoveVCMDeviceList = [];

RemoveHBPMDeviceList = [];
RemoveVBPMDeviceList = [];



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function only works on the first iteration %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if exist('BPMData','var') && length(BPMData)>1
    BPMData = BPMData(1);
end
if exist('CMData','var') && length(CMData)>1
    CMData = CMData(1);
end
if exist('FitParameters','var') && length(FitParameters)>1
    FitParameters = FitParameters(1);
end
if exist('LocoFlags','var') && length(LocoFlags)>1
    LocoFlags = LocoFlags(1);
end
if exist('LocoModel','var') && length(LocoModel)>1
    LocoModel = LocoModel(1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make sure the start point in loaded in the AT model %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~isempty(FitParameters)
    for i = 1:length(FitParameters.Params)
        RINGData = locosetlatticeparam(RINGData, FitParameters.Params{i}, FitParameters.Values(i));
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tune up a few parameters based on the MachineConfig %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% try
%     if isfield(LocoMeasData, 'MachineConfig')
%         % Sextupoles
%         if all(MachineConfig.SF.Setpoint.Data == 0)
%             fprintf('   Turning S1 family off in the LOCO model.\n');
%             ATIndex = findcells(RINGData.Lattice,'FamName','S1')';
%             for i = 1:length(ATIndex)
%                 RINGData.Lattice{ATIndex(i)}.PolynomB(3) = 0;
%             end
%         end
%
%         % Skew quadrupoles
%         if all(MachineConfig.SQSF.Setpoint.Data == 0)
%             fprintf('   SQSF family was at zero when the response matrix was taken.\n');
%         end
%         if all(MachineConfig.SQSD.Setpoint.Data == 0)
%             fprintf('   SQSD family was at zero when the response matrix was taken.\n');
%         end
%     end
% catch
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LocoFlags to change from the default %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LocoFlags.Threshold = 1e-5;
% LocoFlags.OutlierFactor = 10;
% LocoFlags.SVmethod = 1e-2;
% LocoFlags.HorizontalDispersionWeight = 12.5;
% LocoFlags.VerticalDispersionWeight = 12.5;
% LocoFlags.AutoCorrectDelta = 'Yes';
% LocoFlags.Coupling = 'No';
% LocoFlags.Dispersion = 'No';
% LocoFlags.Normalize = 'Yes';
% LocoFlags.ResponseMatrixCalculatorFlag1 = 'Linear';
% LocoFlags.ResponseMatrixCalculatorFlag2 = 'FixedPathLength';
% LocoFlags.CalculateSigma = 'No';
% LocoFlags.SinglePrecision = 'Yes';

% CMData.FitKicks    = 'Yes';
% CMData.FitCoupling = 'No';
%
% BPMData.FitGains    = 'Yes';
% BPMData.FitCoupling = 'No';


% Corrector magnets to disable
j = findrowindex(RemoveHCMDeviceList, LocoMeasData.HCM.DeviceList);
if ~isempty(j)
    irm = findrowindex(j(:),CMData.HCMGoodDataIndex(:));
    CMData.HCMGoodDataIndex(irm) = [];
end

j = findrowindex(RemoveVCMDeviceList, LocoMeasData.VCM.DeviceList);
if ~isempty(j)
    irm = findrowindex(j(:),CMData.VCMGoodDataIndex(:));
    CMData.VCMGoodDataIndex(irm) = [];
end


% BPMs to disable
j = findrowindex(RemoveHBPMDeviceList, LocoMeasData.HBPM.DeviceList);
if ~isempty(j)
    irm = findrowindex(j(:),BPMData.HBPMGoodDataIndex(:));
    BPMData.HBPMGoodDataIndex(irm) = [];
end

j = findrowindex(RemoveVBPMDeviceList, LocoMeasData.VBPM.DeviceList);
if ~isempty(j)
    irm = findrowindex(j(:),BPMData.VBPMGoodDataIndex(:));
    BPMData.VBPMGoodDataIndex(irm) = [];
end

%%%%%%%%%%%%%%%%%
% FitParameters %
%%%%%%%%%%%%%%%%%

% Individual magnets
% For each parameter which is fit in the model a numerical response matrix
% gradient needs to be determined.  The FitParameters data structure determines what
% parameter in the model get varied and how are they grouped.  For no parameter fits, set
% FitParameters.Params to an empty vector.
%     FitParameters.Params = parameter group definition (cell array for AT)
%     FitParameters.Values = Starting value for the parameter fit
%     FitParameters.Deltas = change in parameter value used to compute the gradient (NaN forces loco to choose, see auto-correct delta flag below)
%     FitParameters.FitRFFrequency = ('Yes'/'No') Fit the RF frequency?
%     FitParameters.DeltaRF = Change in RF frequency when measuring "dispersion".
%                             If the RF frequency is being fit the output is stored here.
%
% The FitParameters structure also contains the standard deviations of the fits:
%     LocoValuesSTD
%     FitParameters.DeltaRFSTD
%
% Note: FitParameters.DeltaRF is used when including dispersion in the response matrix.
%       LocoMeasData.DeltaRF is not used directly in loco.  Usually one would set
%       FitParameters.DeltaRF = LocoMeasData.DeltaRF as a starting point for the RF frequency.


%ModeCell = {'Fit by 14 quad families', 'Fit 112 quad power supplies', 'Fit 112 quads and 1 bend power supply','Fit 112 quads + 32 bends','Fit 112 quads + 32 bends+120sexts','Fit 32 bends only'};
%%%%%%%%%%%%%%  Edited by Z.Marti
%[ButtonName, OKFlag] = listdlg('Name','BUILDLOCOINPUT','PromptString',{'Fit Parameter Selection:',...
%    '(Not including skew quadrupoles)'}, 'SelectionMode','single', 'ListString', ModeCell, 'ListSize', [350 125]);
OKFlag=1;
ButtonName=Quad_mode_fit;
%%%%%%%%%%%%%
if OKFlag ~= 1
    ButtonName = 1;
end
drawnow;
FitParameters = [];
n = 0;

global THERING; % TODO remove not clean at all

switch ButtonName
    case 1  % By 14 quad family
        
        n = 0;
        FitParameters.Deltas = [];
        FitParameters.Values = [];
        
        
        %             % Q1 K-values
        %             n = n+1;
        %             Q1 = findcells(THERING,'FamName','QH01');
        %             Q2 = findcells(THERING,'FamName','QH02');
        %             FitParameters.Params{n} = mkparamgroup(THERING, [Q1, Q2], 'cK',ones(1,length([Q1, Q2])));
        %             FitParameters.Values = [FitParameters.Values; 1];
        %             FitParameters.Deltas = [FitParameters.Deltas;  0.002];
        
        for k=1:10,
            % Q1 K-values
            n = n+1;
            QI = findcells(THERING,'FamName',['QH' num2str(k,'%02u')]);
            FitParameters.Params{n} = mkparamgroup(THERING, QI, 'K');
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI(1))];
            FitParameters.Deltas = [FitParameters.Deltas;  0.002];
        end
        
        for k=1:4,
            % Q1 K-values
            n = n+1;
            QI = findcells(THERING,'FamName',['QV' num2str(k,'%02u')]);
            FitParameters.Params{n} = mkparamgroup(THERING, QI, 'K');
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI(1))];
            FitParameters.Deltas = [FitParameters.Deltas;  0.002];
        end
        
        
    case 2  % Fit by 112 quad power supply
        
        n = 0;
        FitParameters.Deltas = [];
        FitParameters.Values = [];
        
        % K-values
        
        %         % Q1-2 K-values
        %         Q1 = findcells(THERING,'FamName','QH01');
        %         Q2 = findcells(THERING,'FamName','QH02');
        %         QI = [Q1, Q2];
        %         for loop=1:length(QI)
        %             n = n + 1;
        %             FitParameters.Params{n} = mkparamgroup(THERING, [Q1, Q2], 'cK',ones(1,length([Q1, Q2])));
        %         end
        %         FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI)];
        %         FitParameters.Deltas = [FitParameters.Deltas; 0.002 * ones(length(QI),1)];
        %
        for k=1:10,
            QI = findcells(THERING,'FamName',['QH' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'K');
            end
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.002 * ones(length(QI),1)];
        end
        
        for k=1:4,
            QI = findcells(THERING,'FamName',['QV' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'K');
            end
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.002 * ones(length(QI),1)];
        end
        
    case 3  % Fit 112 quads and 1 bend power supply
        
        n = 0;
        FitParameters.Deltas = [];
        FitParameters.Values = [];
        
        % K-values
        for k=1:10,
            QI = findcells(THERING,'FamName',['QH' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'K');
            end
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.002 * ones(length(QI),1)];
        end
        
        for k=1:4,
            QI = findcells(THERING,'FamName',['QV' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'K');
            end
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.002 * ones(length(QI),1)];
        end
        
        BI = findcells(THERING,'FamName','BEND');
        n = n + 1;
        FitParameters.Params{n} = mkparamgroup(THERING, BI, 'K');
        FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', BI(1))];
        FitParameters.Deltas = [FitParameters.Deltas; 0.001];
        
    case 4  % Fit by single element (112quads + 32bends)
        
        n = 0;
        FitParameters.Deltas = [];
        FitParameters.Values = [];
        
        % K-values
        for k=1:10,
            QI = findcells(THERING,'FamName',['QH' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'K');
            end
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.002 * ones(length(QI),1)];
        end
        
        for k=1:4,
            QI = findcells(THERING,'FamName',['QV' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'K');
            end
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.002 * ones(length(QI),1)];
        end
        
        BI = findcells(THERING,'FamName','BEND');
        
        for loop1=1:length(BI)/2,
            loop2 = [(2*loop1-1) (2*loop1)];
            n = n + 1;
            FitParameters.Params{n} = mkparamgroup(THERING, BI(loop2), 'K');
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', BI(loop2(1)) )];
            FitParameters.Deltas = [FitParameters.Deltas; 0.001];
        end
    case 5  % Fit by single element (112quads + 32bends + 120 skew)
        
        n = 0;
        FitParameters.Deltas = [];
        FitParameters.Values = [];
        
        % K-values
        for k=1:10,
            QI = findcells(THERING,'FamName',['QH' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'K');
            end
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.002 * ones(length(QI),1)];
        end
        
        for k=1:4,
            QI = findcells(THERING,'FamName',['QV' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'K');
            end
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', QI)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.002 * ones(length(QI),1)];
        end
        
        BI = findcells(THERING,'FamName','BEND');
        
        for loop1=1:length(BI)/2,
            loop2 = [(2*loop1-1) (2*loop1)];
            n = n + 1;
            FitParameters.Params{n} = mkparamgroup(THERING, BI(loop2), 'K');
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', BI(loop2(1)) )];
            FitParameters.Deltas = [FitParameters.Deltas; 0.001];
        end
        
        % K-values at sextupoles
        for k=1:4,
            SI = findcells(THERING,'FamName',['SF' num2str(k,'%1u')]);
            for loop1=1:length(SI)/2,
                loop2 = [(2*loop1-1) (2*loop1)];
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, SI(loop2), 'K');
                FitParameters.Values = [FitParameters.Values; 0.0 ];
                FitParameters.Deltas = [FitParameters.Deltas; 0.001];
            end
        end
        
        for k=1:5,
            SI = findcells(THERING,'FamName',['SD' num2str(k,'%1u')]);
            for loop1=1:length(SI)/2,
                loop2 = [(2*loop1-1) (2*loop1)];
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, SI(loop2), 'K');
                FitParameters.Values = [FitParameters.Values; 0.0 ];
                FitParameters.Deltas = [FitParameters.Deltas; 0.001];
            end
        end
        
    case 6 % Only the bends
        BI = findcells(THERING,'FamName','BEND');
        n = 0;
        FitParameters.Deltas = [];
        FitParameters.Values = [];
        for loop1=1:length(BI)/2,
            loop2 = [(2*loop1-1) (2*loop1)];
            n = n + 1;
            FitParameters.Params{n} = mkparamgroup(THERING, BI(loop2), 'K');
            FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'K', BI(loop2(1)) )];
            FitParameters.Deltas = [FitParameters.Deltas; 0.001];
        end
        
        
    otherwise
end

%%%%%%%%%%%%%%%%%%%%%%%
% Skew quadrupole fits
%%%%%%%%%%%%%%%%%%%%%%%

%ModeCell = {'Fit by Skew Quad power supply','Fit by 32 Skews', 'Fit at Sextupoles', ...
%            'Fit by tilt dipoles','Fit by tilt quads','Fit sext, tilt quads, tilt dipoles',...
%            'Fit skews, sext, tilt quads, tilt dipoles','Do Not Fit Skew Quadrupoles'};
%[ButtonName, OKFlag] = listdlg('Name','BUILDLOCOINPUT','PromptString','Skew Quadrupole Fits?', 'SelectionMode','single', 'ListString', ModeCell, 'ListSize', [350 125], 'InitialValue', length(ModeCell));
ButtonName=Coupling_mode_fit;
%OKFlag=1;
% if OKFlag ~= 1
%     ButtonName = length(ModeCell);  % Default
% end

switch ButtonName
    
    case 1 % 'Fit by Power Supply'
        % skew quad
        for k=1:2,
            QSI = findcells(THERING,'FamName',['QS' num2str(k,'%1u')]);
            
            n = n + 1;
            FitParameters.Params{n} = mkparamgroup(THERING, QSI, 'KS1');
            FitParameters.Values = [FitParameters.Values; 0.0 ];
            FitParameters.Deltas = [FitParameters.Deltas; 0.001];
        end
    case 2 % 'Fit by 32 Power Supplyies
        % skew quad
        
        QSI = findcells(THERING,'FamName','QS');
        
        for loop=1:length(QSI)
            n = n + 1;
            FitParameters.Params{n} = mkparamgroup(THERING, QSI(loop), 'KS1');
        end
        FitParameters.Values = [FitParameters.Values; getcellstruct(THERING, 'PolynomA', QSI,2)];
        FitParameters.Deltas = [FitParameters.Deltas; 1 * ones(length(QSI),1)];
        
    case 3 % 'Fit at Sextupoles'
        
        for k=1:4,
            SI = findcells(THERING,'FamName',['SF' num2str(k,'%1u')]);
            for loop1=1:length(SI)/2,
                loop2 = [(2*loop1-1) (2*loop1)];
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, SI(loop2), 'KS1');
                FitParameters.Values = [FitParameters.Values; 0.0 ];
                FitParameters.Deltas = [FitParameters.Deltas; 0.001];
            end
        end
        
        for k=1:5,
            SI = findcells(THERING,'FamName',['SD' num2str(k,'%1u')]);
            for loop1=1:length(SI)/2,
                loop2 = [(2*loop1-1) (2*loop1)];
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, SI(loop2), 'KS1');
                FitParameters.Values = [FitParameters.Values; 0.0 ];
                FitParameters.Deltas = [FitParameters.Deltas; 0.001];
            end
        end
    case 4 % 'Fit by sext, tilt quads, 
        
        % skew term at sextupoles
        for k=1:4,
            SI = findcells(THERING,'FamName',['SF' num2str(k,'%1u')]);
            for loop1=1:length(SI)/2,
                loop2 = [(2*loop1-1) (2*loop1)];
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, SI(loop2), 'KS1');
                FitParameters.Values = [FitParameters.Values; 0.0 ];
                FitParameters.Deltas = [FitParameters.Deltas; 0.001];
            end
        end
        
        for k=1:5,
            SI = findcells(THERING,'FamName',['SD' num2str(k,'%1u')]);
            for loop1=1:length(SI)/2,
                loop2 = [(2*loop1-1) (2*loop1)];
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, SI(loop2), 'KS1');
                FitParameters.Values = [FitParameters.Values; 0.0 ];
                FitParameters.Deltas = [FitParameters.Deltas; 0.001];
            end
        end
        
        % quad roll
        for k=1:10,
            QI = findcells(THERING,'FamName',['QH' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'tilt');
            end
            FitParameters.Values = [FitParameters.Values; zeros(length(QI),1)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001*ones(length(QI),1)];
        end
        
        for k=1:4,
            QI = findcells(THERING,'FamName',['QV' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'tilt');
            end
            FitParameters.Values = [FitParameters.Values; zeros(length(QI),1)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001*ones(length(QI),1)];
        end
        
    case 5 % 'Fit by sext, tilt quads, tilt bends'
        
        % skew term at sextupoles
        for k=1:4,
            SI = findcells(THERING,'FamName',['SF' num2str(k,'%1u')]);
            for loop1=1:length(SI)/2,
                loop2 = [(2*loop1-1) (2*loop1)];
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, SI(loop2), 'KS1');
                FitParameters.Values = [FitParameters.Values; 0.0 ];
                FitParameters.Deltas = [FitParameters.Deltas; 0.001];
            end
        end
        
        for k=1:5,
            SI = findcells(THERING,'FamName',['SD' num2str(k,'%1u')]);
            for loop1=1:length(SI)/2,
                loop2 = [(2*loop1-1) (2*loop1)];
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, SI(loop2), 'KS1');
                FitParameters.Values = [FitParameters.Values; 0.0 ];
                FitParameters.Deltas = [FitParameters.Deltas; 0.001];
            end
        end
        
        % quad roll
        for k=1:10,
            QI = findcells(THERING,'FamName',['QH' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'tilt');
            end
            FitParameters.Values = [FitParameters.Values; zeros(length(QI),1)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001*ones(length(QI),1)];
        end
        
        for k=1:4,
            QI = findcells(THERING,'FamName',['QV' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'tilt');
            end
            FitParameters.Values = [FitParameters.Values; zeros(length(QI),1)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001*ones(length(QI),1)];
        end
        
        % bend roll
        BI = findcells(THERING,'FamName','BEND');
        for loop1=1:length(BI)/2,
            loop2 = [(2*loop1-1) (2*loop1)];
            n = n + 1;
            FitParameters.Params{n} = mkparamgroup(THERING, BI(loop2), 'tilt');
            FitParameters.Values = [FitParameters.Values; 0.0];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001];
        end
        
    case 6 % 'Fit by tilt dipoles'
        BI = findcells(THERING,'FamName','BEND');
        for loop1=1:length(BI)/2,
            loop2 = [(2*loop1-1) (2*loop1)];
            n = n + 1;
            FitParameters.Params{n} = mkparamgroup(THERING, BI(loop2), 'tilt');
            FitParameters.Values = [FitParameters.Values; 0.0];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001];
        end
        
    case 7 % 'Fit by tilt quads'
        for k=1:10,
            QI = findcells(THERING,'FamName',['QH' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'tilt');
            end
            FitParameters.Values = [FitParameters.Values; zeros(length(QI),1)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001*ones(length(QI),1)];
        end
        
        for k=1:4,
            QI = findcells(THERING,'FamName',['QV' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'tilt');
            end
            FitParameters.Values = [FitParameters.Values; zeros(length(QI),1)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001*ones(length(QI),1)];
        end
        
        
        
    case 8 % 'Fit by skew, sext, tilt quads, tilt bends'
        
        % skew quad
        for k=1:2,
            QSI = findcells(THERING,'FamName',['QS' num2str(k,'%1u')]);
            
            n = n + 1;
            FitParameters.Params{n} = mkparamgroup(THERING, QSI, 'KS1');
            FitParameters.Values = [FitParameters.Values; 0.0 ];
            FitParameters.Deltas = [FitParameters.Deltas; 0.001];
        end
        
        % skew term at sextupoles
        for k=1:4,
            SI = findcells(THERING,'FamName',['SF' num2str(k,'%1u')]);
            for loop1=1:length(SI)/2,
                loop2 = [(2*loop1-1) (2*loop1)];
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, SI(loop2), 'KS1');
                FitParameters.Values = [FitParameters.Values; 0.0 ];
                FitParameters.Deltas = [FitParameters.Deltas; 0.001];
            end
        end
        
        for k=1:5,
            SI = findcells(THERING,'FamName',['SD' num2str(k,'%1u')]);
            for loop1=1:length(SI)/2,
                loop2 = [(2*loop1-1) (2*loop1)];
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, SI(loop2), 'KS1');
                FitParameters.Values = [FitParameters.Values; 0.0 ];
                FitParameters.Deltas = [FitParameters.Deltas; 0.001];
            end
        end
        
        % quad roll
        for k=1:10,
            QI = findcells(THERING,'FamName',['QH' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'tilt');
            end
            FitParameters.Values = [FitParameters.Values; zeros(length(QI),1)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001*ones(length(QI),1)];
        end
        
        for k=1:4,
            QI = findcells(THERING,'FamName',['QV' num2str(k,'%02u')]);
            for loop=1:length(QI)
                n = n + 1;
                FitParameters.Params{n} = mkparamgroup(THERING, QI(loop), 'tilt');
            end
            FitParameters.Values = [FitParameters.Values; zeros(length(QI),1)];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001*ones(length(QI),1)];
        end
        
        % bend roll
        BI = findcells(THERING,'FamName','BEND');
        for loop1=1:length(BI)/2,
            loop2 = [(2*loop1-1) (2*loop1)];
            n = n + 1;
            FitParameters.Params{n} = mkparamgroup(THERING, BI(loop2), 'tilt');
            FitParameters.Values = [FitParameters.Values; 0.0];
            FitParameters.Deltas = [FitParameters.Deltas; 0.0001];
        end
        
    case 9 %'Don't Fit'
        fprintf('   Skew quadrupole not fit.\n\n');
        
    otherwise
        fprintf('   Skew quadrupole not fit.\n\n');
end

fprintf('\n');



%%%%%%%%%%%%%%%%%%%%%
% Parameter Weights %
%%%%%%%%%%%%%%%%%%%%%

% For each cell of FitParameters.Weight.Value, a rows is added to the A matrix
% Index of the row of A:  FitParameters.Weight.Index{i}
% Value of the weight:    FitParameters.Weight.Value{i} / mean(Mstd)

% ModeCell = {'No Parameter Weigths', 'Add Parameter Weights'};
% [ButtonName, OKFlag] = listdlg('Name','BUILDLOCOINPUT','PromptString','Weights?', 'SelectionMode','single', 'ListString', ModeCell, 'ListSize', [200 75], 'InitialValue', 1);

OKFlag=1;
ButtonName=1;
if OKFlag ~= 1
    ButtonName = 1;  % Default
end

switch ButtonName
    case 1
        % No Weights
        if isfield(FitParameters, 'Weight')
            FitParameters = rmfield(FitParameters, 'Weight');
        end
    case 2
        %for ii = 1:length(FitParameters.Values)
        for ii = 1:112, % specific weight for K-values
            FitParameters.Weight.Index{ii,1} = ii;
            FitParameters.Weight.Value{ii,1} = 0.1;
        end
end

% RF parameter fit setup (There is a flag to actually do the fit)
if isempty(LocoMeasData.DeltaRF)
    FitParameters.DeltaRF = 500;  % It's good to have something here so that LOCO will compute a model dispersion
else
    FitParameters.DeltaRF = LocoMeasData.DeltaRF;
end


% File check
[BPMData, CMData, LocoMeasData, LocoModel, FitParameters, LocoFlags, RINGData] = locofilecheck({BPMData, CMData, LocoMeasData, LocoModel, FitParameters, LocoFlags, RINGData});


% Save
save(FileName, 'LocoModel', 'FitParameters', 'BPMData', 'CMData', 'RINGData', 'LocoMeasData', 'LocoFlags');

