function ErrorFlag = setpvgroup(varargin)


Fam=varargin{1};
values= varargin{3};
Field=varargin{2};
tangolist=getfamilydata(Fam,Field,'TangoNames');
[attribute device]  = getattribute(tangolist(1));

GroupID = getfamilydata(Fam, 'GroupId');


tango_group_write_attribute(GroupID, attribute{1}, 0,double(values'));
%tango_group_write_attribute(GroupID, attribute{1}, 0,double(values'));
R=tango_group_write_attribute(GroupID, attribute{1}, 0,double(values'));

ErrorFlag=R.has_failed;
if tango_error == -1
    tango_print_error_stack;
    ErrorFlag = 1;
    return;
end



