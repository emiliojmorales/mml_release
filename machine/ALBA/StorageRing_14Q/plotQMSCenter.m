function result=plotqmscenter(varargin)
% result=plotqmscenter(dir)
% plot the qms data for a given directory
% if not directory give, ask for one
global oldfolder
if length(varargin)==0
    try
        folder_name = uigetdir(oldfolder);
    catch
        ad=getad;
        folder_name = uigetdir(ad.Directory.QMS);
    end
else
    folder_name = cell2mat(varargin(1));
end
oldfolder=folder_name
list=dir([folder_name '/s*mat']);
nbpms=length(list);
ix=0;
iy=0;
ao=getao;
for loop=1:nbpms,
    load([folder_name '/' list(loop).name]);
    list(loop).name
    QMS.BPMDev;
    if QMS.QuadPlane==1,
        ix=ix+1;
        result.x.center(ix)=QMS.Center;
        result.x.centerstd(ix)=QMS.CenterSTD;
%        result.bpmx(ix,:)=['[' num2str(QMS.BPMDev) ']'];
        result.x.bpm(ix,:)=ao.BPMx.CommonNames(dev2elem('BPMx',[QMS.BPMDev]),:);
        result.x.s(ix)=getspos(QMS.BPMFamily, QMS.BPMDev);
        result.x.timestamp(ix,:)=QMS.TimeStamp;
    else
        iy=iy+1;
        result.y.center(iy)=QMS.Center;
        result.y.centerstd(iy)=QMS.CenterSTD;
%        result.bpmy(iy,:)=['[' num2str(QMS.BPMDev) ']'];
        result.y.bpm(iy,:)=ao.BPMy.CommonNames(dev2elem('BPMy',[QMS.BPMDev]),:);
        result.y.s(iy)=getspos(QMS.BPMFamily, QMS.BPMDev);
        result.y.timestamp(iy,:)=QMS.TimeStamp;
    end
    
end
%rearrange the results
if (ix)
    [b indexx]=sort(result.x.s);
    result.x.s=result.x.s(indexx);
    result.x.center=result.x.center(indexx);
    result.x.centerstd=result.x.centerstd(indexx);
    result.x.bpm=result.x.bpm(indexx,:);
    figure;
    hx=errorbar(result.x.s, result.x.center, result.x.centerstd,'.r')
    [B,I,J] = unique(result.x.s);
    h=gca;
    xaxis([0 268.8])
    set(h,'XTick',B,'XTickLabel',result.x.bpm(I,:))
%    set(h,'position',[0.13 0.25 0.775 0.65])
    result.x.th=rotateticklabel(h);
    ylabel('Horizontal Center')
    title(['BBA measurement: ' datestr(result.x.timestamp(end,:))])
end
if (iy)
    [b indexy]=sort(result.y.s);
    result.y.s=result.y.s(indexy);
    result.y.center=result.y.center(indexy);
    result.y.centerstd=result.y.centerstd(indexy);
    result.y.bpm=result.y.bpm(indexy,:);
    figure;
    hy=errorbar(result.y.s, result.y.center, result.y.centerstd,'.b')
    [B,I,J] = unique(result.y.s);
    h=gca;
    set(h,'XTick',B,'XTickLabel',result.y.bpm(I,:))
    xaxis([0 268.8])
  %  set(h,'position',[0.13 0.25 0.775 0.65])
    result.y.th=rotateticklabel(h,90);
    ylabel('Vertical Center')
    title(['BBA measurement: ' datestr(result.y.timestamp(end,:))])
end
