function albainit(varargin)
% Initialize parameters for ALBA control in MATLAB
% Parametes:
% 'SA' - slow adquisition mode, by [default].
% 'Average' - uses the XMeasPosSA mode of the BPM
% 'Orbit' - uses 88 BPMs [default]
% 'AP'    - Accelerator Physics Mode, uses 104 BPMs
% To set some BPMs Status to zero, use the bpmstat.txt file in the OpsData
% example albainit('Average','AP')


% 26 June 07 Change the number of BPMs and Correctors to 88
% Modified by Laurent S. Nadolski
%
% 29-Nov-11: Added the option to select the number of BPMs and the mode of
% the BPMs.   M. Munoz
%
OperationalMode = 1;

NBPM=88;
BPMMode='SA';
if sum(strcmp('Average',varargin)),
    BPMMode='Mean';
end
if sum(strcmp('AP',varargin)),
    NBPM=120;
end
% If controlromm user is operator and online mode
disp 'Hello'
[statuss WHO] = system('whoami');
% system gives back an visible character: carriage return!
% so comparison on the number of caracters
if strncmp(WHO, 'operator',8),
    ControlRoomFlag = 1;
    Mode = 'Online';
else
    ControlRoomFlag = 0;
    Mode = 'Simulator';
end

if strcmp(BPMMode,'Mean')
    tcx='/XMeanPosSA';
    tcy='/ZMeanPosSA';
else
    tcx='/XPosSA';
    tcy='/ZPosSA';
end
fprintf(1,'   Using %ld BPMs and the %s mode\n',NBPM,BPMMode);

setad([]);     %clear AcceleratorData memory

%%%%%%%%%%%%%%%%%%%%
% ACCELERATOR OBJECT
%%%%%%%%%%%%%%%%%%%%

if ~isempty(getappdata(0, 'AcceleratorObjects'))
    AO = getao;
    % Check if online and AO is from Storagering
    if ControlRoomFlag
        local_tango_kill_allgroup(AO); % kill all TANGO group
    end
end
setao([]); AO =[];   %clear previous AcceleratorObjects



%=============================================
%% BPMx data: status field designates if BPM in use
%=============================================
iFam = 'BPMx';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).MemberOf                 = {'PlotFamily'; 'BPMx'; 'HBPM'; 'BPM'; 'Diagnostics';'Archivable'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';
%AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvonline_bpm_sr';
%AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvonline_groups';

ii=0;
for ai=1:16
    if any(ismember(ai,[1 4 5 8 9 12 13 16]))
        nmax=7;
    else
        nmax=8;
    end
    for bi=1:nmax
        ii=ii+1;
        AO.(iFam).ElementList(ii,:)        = ii;
        AO.(iFam).DeviceName(ii,:)         = {sprintf('sr%.2d/di/bpm-%.2d',ai,bi)};
%         if (ai==9 && bi==2),
%             AO.(iFam).DeviceName(ii,:) = {'sr09/di/bpm-04'};
%         end
        AO.(iFam).Monitor.TangoNames(ii,:) = strcat(AO.(iFam).DeviceName(ii,:), tcx);
        
        AO.(iFam).Status(ii,:)             = 1;
        AO.(iFam).ManagerStatus(ii,:)     = 1;
        AO.(iFam).DeviceList(ii,:)         = [ai bi];
        AO.(iFam).CommonNames(ii,:)        = sprintf('BPMx-%.2d-%.2d',ai,bi);
        AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
        AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
        AO.(iFam).Monitor.Golden(ii,:)   =   0.0;
        AO.(iFam).Offset(ii,:)                = 0;
        % Starting to remove BPMs from the init.   
        if (NBPM==104 && bi==4),                           % 104 BPMs
            AO.(iFam).Status(ii,:)             = 0;
            AO.(iFam).ManagerStatus(ii,:)     = 0;
        end
        if (NBPM==88 && (bi==5||bi==4)),                           % 88 BPMs
            AO.(iFam).Status(ii,:)             = 0;
            AO.(iFam).ManagerStatus(ii,:)     = 0;
        end

        % if you want to remove bpms do it in the file:
        % /data/MML/Release/machine/ALBA/StorageRingOpsData/a25/bpmstat.txt
        % do not do it here.
    end
    
end

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('BPMx');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end

%R=tango_group_read_attributes(AO.(iFam).GroupId ,'XPosSA', 0);

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';

%=============================================
%% BPMy data: status field designates if BPM in use
%=============================================

iFam = 'BPMy';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).MemberOf                 = {'PlotFamily'; 'BPMy'; 'VBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';
%AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvonline_groups';

ii=0;
for ai=1:16
    if any(ismember(ai,[1 4 5 8 9 12 13 16]))
        nmax=7;
    else
        nmax=8;
    end
    for bi=1:nmax
        ii=ii+1;
        AO.(iFam).ElementList(ii,:)        = ii;
        AO.(iFam).DeviceName(ii,:)         = {sprintf('sr%.2d/di/bpm-%.2d',ai,bi)};
%         if (ai==9 && bi==2),
%             AO.(iFam).DeviceName(ii,:) = {'sr09/di/bpm-04'};
%         end
        AO.(iFam).Monitor.TangoNames(ii,:) = strcat(AO.(iFam).DeviceName(ii,:), tcy);
        AO.(iFam).Status(ii,:)             = 1;
        AO.(iFam).ManagerStatus(ii,:)     = 1;
        AO.(iFam).DeviceList(ii,:)         = [ai bi];
        AO.(iFam).CommonNames(ii,:)        = sprintf('BPMy-%.2d-%.2d',ai,bi);
        AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
        AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
        AO.(iFam).Monitor.Golden(ii,:)   =   0.0;
        AO.(iFam).Offset(ii,:)                = 0;
        % Starting to remove BPMs from the init.
        % Starting to remove BPMs from the init.   
        if (NBPM==104 && bi==4),                           % 104 BPMs
            AO.(iFam).Status(ii,:)             = 0;
            AO.(iFam).ManagerStatus(ii,:)     = 0;
        end
        if (NBPM==88 && (bi==5||bi==4)),                           % 88 BPMs
            AO.(iFam).Status(ii,:)             = 0;
            AO.(iFam).ManagerStatus(ii,:)     = 0;
        end
        % if you want to remove bpms do it in the file:
        % /data/MML/Release/machine/ALBA/StorageRingOpsData/a25/bpmstat.txt
        % do not do it here.
        
    end
end

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('BPMy');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';

%=============================================
%% DBPMx
%=============================================

iFam = 'DBPMx';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).MemberOf                 = {'PlotFamily'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';

bpm={
    1, 'sr03/di/dbpm-01', 1,[3 1],'DBPM_03_01'
    2, 'sr01/di/bpm-06', 1,[1 1],'DBPM_01_01'
    3, 'sr05/di/bpm-06', 1,[5 1],'DBPM_05_01'
    };
ndpm=size(bpm,1);
%Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/XPosDD');
    AO.(iFam).Status(ii,:)             = bpm{ii,3};
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end


% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';


%=============================================
%% DBPMy
%=============================================

iFam = 'DBPMy';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).MemberOf                 = {'PlotFamily'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';


%Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/ZPosDD');
    AO.(iFam).Status(ii,:)             = bpm{ii,3};
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end


% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';

%=============================================
%% BLM data: status field designates if BLM in use
%=============================================
iFam = 'BLM';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).MemberOf                 = {'PlotFamily'; 'BLM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'counts';
AO.(iFam).Monitor.PhysicsUnits     = 'counts';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';
%AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvonline_bpm_sr';
%AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvonline_groups';

ii=0;
for ai=1:16
    if any(ismember(ai,[1 4 5 8 9 12 13 16]))
        nmax=7;
    else
        nmax=8;
    end
    for bi=1:nmax
        ii=ii+1;
        AO.(iFam).ElementList(ii,:)        = ii;
        AO.(iFam).DeviceName(ii,:)         = {sprintf('sr%.2d/di/blm-%.2d',ai,bi)};
        AO.(iFam).Monitor.TangoNames(ii,:) = strcat(AO.(iFam).DeviceName(ii,:), '/Counts1s');
        AO.(iFam).Status(ii,:)             = 1;
        AO.(iFam).ManagerStatus(ii,:)     = 1;
        AO.(iFam).DeviceList(ii,:)         = [ai bi];
        AO.(iFam).CommonNames(ii,:)        = sprintf('BLM-%.2d-%.2d',ai,bi);
        AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1;
        AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1;
        AO.(iFam).Monitor.Golden(ii,:)   =   0.0;
        AO.(iFam).Offset(ii,:)                = 0;
        
        
        if (ai==11 &&bi==1)||(ai==11 &&bi==2)||(ai==14 &&bi==5)||(ai==14 &&bi==6)||(ai==16 &&bi==1)||(ai==16 &&bi==2)||(ai==10 &&bi==5)||(ai==15 &&bi==8)
            AO.(iFam).Status(ii,:)             = 0;
            AO.(iFam).Status(ii,:)             = 0;
            AO.(iFam).ManagerStatus(ii,:)     = 0;
        end
        if (ai==15)
            AO.(iFam).Status(ii,:)             = 0;
            AO.(iFam).Status(ii,:)             = 0;
            AO.(iFam).ManagerStatus(ii,:)     = 0;
        end
    end
    
end

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('BLM');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end


% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';


%=============================
%        MAIN MAGNETS
%=============================

%===========
%%Dipole data
%===========

%% *** BEND ***
iFam = 'BEND';
AO.(iFam).FamilyName                 = 'BEND';
AO.(iFam).MemberOf                   = {'BEND'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('BEND');
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';


AO.(iFam).DeviceName(:,:) = {'sr/pc/bend'};
AO.(iFam).Monitor.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName(:,:),'/Current');

AO.(iFam).DeviceList(:,:) = [1 1];
AO.(iFam).ElementList(:,:)= 1;
AO.(iFam).Status          = 1;

val = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(iFam).Monitor.Range(:,:) = [0 600]; % 580 A for 1.4214 T @ 3 GeV

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');
AO.(iFam).Setpoint.Tolerance(:,:) = 0.05;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.05;

%QUADRUPOLES
%% *** QH01 ***
iFam = 'QH01';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADH';'Archivable'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr01/pc/qh01', 1, [01 1], 'QH01_01_01'
    2,	'sr04/pc/qh01', 1, [04 1], 'QH01_01_04'
    3,	'sr05/pc/qh01', 1, [05 1], 'QH01_01_05'
    4,	'sr08/pc/qh01', 1, [08 1], 'QH01_01_08'
    5,	'sr09/pc/qh01', 1, [09 1], 'QH01_01_09'
    6,	'sr12/pc/qh01', 1, [12 1], 'QH01_01_12'
    7,	'sr13/pc/qh01', 1, [13 1], 'QH01_01_13'
    8,	'sr16/pc/qh01', 1, [16 1], 'QH01_01_16'
    };



HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);
% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QH01');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end

%% *** QH02 ***
iFam = 'QH02';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADH';'Archivable'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr01/pc/qh02', 1, [01 1], 'QH02_01_01'
    2,	'sr04/pc/qh02', 1, [04 1], 'QH02_01_04'
    3,	'sr05/pc/qh02', 1, [05 1], 'QH02_01_05'
    4,	'sr08/pc/qh02', 1, [08 1], 'QH02_01_08'
    5,	'sr09/pc/qh02', 1, [09 1], 'QH02_01_09'
    6,	'sr12/pc/qh02', 1, [12 1], 'QH02_01_12'
    7,	'sr13/pc/qh02', 1, [13 1], 'QH02_01_13'
    8,	'sr16/pc/qh02', 1, [16 1], 'QH02_01_16'
    };


HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);
% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QH02');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end


%% *** QH03 ***
iFam = 'QH03';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector'; 'QUADH';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr01/pc/qh03', 1, [01 1], 'QH03_01_01'
    2,	'sr04/pc/qh03', 1, [04 1], 'QH03_01_04'
    3,	'sr05/pc/qh03', 1, [05 1], 'QH03_01_05'
    4,	'sr08/pc/qh03', 1, [08 1], 'QH03_01_08'
    5,	'sr09/pc/qh03', 1, [09 1], 'QH03_01_09'
    6,	'sr12/pc/qh03', 1, [12 1], 'QH03_01_12'
    7,	'sr13/pc/qh03', 1, [13 1], 'QH03_01_13'
    8,	'sr16/pc/qh03', 1, [16 1], 'QH03_01_16'
    };


HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QH03');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end


%% *** QH04 ***
iFam = 'QH04';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADH';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr01/pc/qh04', 1, [01 1], 'QH04_01_01'
    2,	'sr04/pc/qh04', 1, [04 1], 'QH04_01_04'
    3,	'sr05/pc/qh04', 1, [05 1], 'QH04_01_05'
    4,	'sr08/pc/qh04', 1, [08 1], 'QH04_01_08'
    5,	'sr09/pc/qh04', 1, [09 1], 'QH04_01_09'
    6,	'sr12/pc/qh04', 1, [12 1], 'QH04_01_12'
    7,	'sr13/pc/qh04', 1, [13 1], 'QH04_01_13'
    8,	'sr16/pc/qh04', 1, [16 1], 'QH04_01_16'
    };


HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QH04');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end
%% *** QH05 ***
iFam = 'QH05';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADH';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr01/pc/qh05', 1, [01 1], 'QH05_01_01'
    2,	'sr04/pc/qh05', 1, [04 1], 'QH05_01_04'
    3,	'sr05/pc/qh05', 1, [05 1], 'QH05_01_05'
    4,	'sr08/pc/qh05', 1, [08 1], 'QH05_01_08'
    5,	'sr09/pc/qh05', 1, [09 1], 'QH05_01_09'
    6,	'sr12/pc/qh05', 1, [12 1], 'QH05_01_12'
    7,	'sr13/pc/qh05', 1, [13 1], 'QH05_01_13'
    8,	'sr16/pc/qh05', 1, [16 1], 'QH05_01_16'
    };



HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);
% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QH05');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end


%% *** QH06 ***
iFam = 'QH06';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADH';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';


AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr01/pc/qh06', 1, [01 1], 'QH06_01_01'
    2,	'sr04/pc/qh06', 1, [04 1], 'QH06_01_04'
    3,	'sr05/pc/qh06', 1, [05 1], 'QH06_01_05'
    4,	'sr08/pc/qh06', 1, [08 1], 'QH06_01_08'
    5,	'sr09/pc/qh06', 1, [09 1], 'QH06_01_09'
    6,	'sr12/pc/qh06', 1, [12 1], 'QH06_01_12'
    7,	'sr13/pc/qh06', 1, [13 1], 'QH06_01_13'
    8,	'sr16/pc/qh06', 1, [16 1], 'QH06_01_16'
    };


HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 225];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QH06');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end

%% *** QH07 ***
iFam = 'QH07';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADH';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr02/pc/qh07', 1, [02 1], 'QH07_01_02'
    2,	'sr03/pc/qh07', 1, [03 1], 'QH07_01_03'
    3,	'sr06/pc/qh07', 1, [06 1], 'QH07_01_06'
    4,	'sr07/pc/qh07', 1, [07 1], 'QH07_01_07'
    5,	'sr10/pc/qh07', 1, [10 1], 'QH07_01_10'
    6,	'sr11/pc/qh07', 1, [11 1], 'QH07_01_11'
    7,	'sr14/pc/qh07', 1, [14 1], 'QH07_01_14'
    8,	'sr15/pc/qh07', 1, [15 1], 'QH07_01_15'
    };



HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 225];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QH07');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end


%% *** QH08 ***
iFam = 'QH08';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADH';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr02/pc/qh08', 1, [02 1], 'QH08_01_02'
    2,	'sr03/pc/qh08', 1, [03 1], 'QH08_01_03'
    3,	'sr06/pc/qh08', 1, [06 1], 'QH08_01_06'
    4,	'sr07/pc/qh08', 1, [07 1], 'QH08_01_07'
    5,	'sr10/pc/qh08', 1, [10 1], 'QH08_01_10'
    6,	'sr11/pc/qh08', 1, [11 1], 'QH08_01_11'
    7,	'sr14/pc/qh08', 1, [14 1], 'QH08_01_14'
    8,	'sr15/pc/qh08', 1, [15 1], 'QH08_01_15'
    };



HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QH08');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end
%% *** QH09 ***
iFam = 'QH09';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADH';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr02/pc/qh09', 1, [02 1], 'QH09_01_02'
    2,	'sr03/pc/qh09', 1, [03 1], 'QH09_01_03'
    3,	'sr06/pc/qh09', 1, [06 1], 'QH09_01_06'
    4,	'sr07/pc/qh09', 1, [07 1], 'QH09_01_07'
    5,	'sr10/pc/qh09', 1, [10 1], 'QH09_01_10'
    6,	'sr11/pc/qh09', 1, [11 1], 'QH09_01_11'
    7,	'sr14/pc/qh09', 1, [14 1], 'QH09_01_14'
    8,	'sr15/pc/qh09', 1, [15 1], 'QH09_01_15'
    };



HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QH09');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end
%% *** QH10 ***
iFam = 'QH10';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADH';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr02/pc/qh10', 1, [02 1], 'QH10_01_02'
    2,	'sr03/pc/qh10', 1, [03 1], 'QH10_01_03'
    3,	'sr06/pc/qh10', 1, [06 1], 'QH10_01_06'
    4,	'sr07/pc/qh10', 1, [07 1], 'QH10_01_07'
    5,	'sr10/pc/qh10', 1, [10 1], 'QH10_01_10'
    6,	'sr11/pc/qh10', 1, [11 1], 'QH10_01_11'
    7,	'sr14/pc/qh10', 1, [14 1], 'QH10_01_14'
    8,	'sr15/pc/qh10', 1, [15 1], 'QH10_01_15'
    };



HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 225];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);
% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QH10');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end
%% *** QV01 ***
iFam = 'QV01';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADV';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr01/pc/qv01', 1, [01 1], 'QV01_01_01'
    2,	'sr04/pc/qv01', 1, [04 1], 'QV01_01_04'
    3,	'sr05/pc/qv01', 1, [05 1], 'QV01_01_05'
    4,	'sr08/pc/qv01', 1, [08 1], 'QV01_01_08'
    5,	'sr09/pc/qv01', 1, [09 1], 'QV01_01_09'
    6,	'sr12/pc/qv01', 1, [12 1], 'QV01_01_12'
    7,	'sr13/pc/qv01', 1, [13 1], 'QV01_01_13'
    8,	'sr16/pc/qv01', 1, [16 1], 'QV01_01_16'
    };



HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QV01');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end

%% *** QV02 ***
iFam = 'QV02';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADV';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr01/pc/qv02', 1, [01 1], 'QV02_01_01'
    2,	'sr04/pc/qv02', 1, [04 1], 'QV02_01_04'
    3,	'sr05/pc/qv02', 1, [05 1], 'QV02_01_05'
    4,	'sr08/pc/qv02', 1, [08 1], 'QV02_01_08'
    5,	'sr09/pc/qv02', 1, [09 1], 'QV02_01_09'
    6,	'sr12/pc/qv02', 1, [12 1], 'QV02_01_12'
    7,	'sr13/pc/qv02', 1, [13 1], 'QV02_01_13'
    8,	'sr16/pc/qv02', 1, [16 1], 'QV02_01_16'
    };


HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QV02');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end

%% *** QV03 ***
iFam = 'QV03';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADV';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Monitor.SpecialFunctionSet     = 'getpvgroup';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr02/pc/qv03', 1, [02 1], 'QV03_01_02'
    2,	'sr03/pc/qv03', 1, [03 1], 'QV03_01_03'
    3,	'sr06/pc/qv03', 1, [06 1], 'QV03_01_06'
    4,	'sr07/pc/qv03', 1, [07 1], 'QV03_01_07'
    5,	'sr10/pc/qv03', 1, [10 1], 'QV03_01_10'
    6,	'sr11/pc/qv03', 1, [11 1], 'QV03_01_11'
    7,	'sr14/pc/qv03', 1, [14 1], 'QV03_01_14'
    8,	'sr15/pc/qv03', 1, [15 1], 'QV03_01_15'
    };


HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QV03');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end

%% *** QV04 ***
iFam = 'QV04';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet'; 'Tune Corrector';'QUADV';};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr02/pc/qv04', 1, [02 1], 'QV04_01_02'
    2,	'sr03/pc/qv04', 1, [03 1], 'QV04_01_03'
    3,	'sr06/pc/qv04', 1, [06 1], 'QV04_01_06'
    4,	'sr07/pc/qv04', 1, [07 1], 'QV04_01_07'
    5,	'sr10/pc/qv04', 1, [10 1], 'QV04_01_10'
    6,	'sr11/pc/qv04', 1, [11 1], 'QV04_01_11'
    7,	'sr14/pc/qv04', 1, [14 1], 'QV04_01_14'
    8,	'sr15/pc/qv04', 1, [15 1], 'QV04_01_15'
    };


HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);



% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('QV04');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end



%QUADRUPOLES FAMILIES
%% *** FQH01 ***
iFam = 'FQH01';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = { 'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qh01', 1, [01 1], 'FQH01'};

HW2PhysicsParams  = magnetcoefficients('QH01');
Physics2HWParams  = magnetcoefficients('QH01');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


%% *** FQH02 ***
iFam = 'FQH02';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = { 'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qh02', 1, [01 1], 'FQH02'   };

HW2PhysicsParams  = magnetcoefficients('QH02');
Physics2HWParams  = magnetcoefficients('QH02');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


%% *** FQH03 ***
iFam = 'FQH03';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = { 'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={1,	'sr/pc/qh03', 1, [01 1], 'QH03'};

HW2PhysicsParams  = magnetcoefficients('QH03');
Physics2HWParams  = magnetcoefficients('QH03');

for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);



%% *** FQH04 ***
iFam = 'FQH04';
AO.(iFam).FamilyName                 = iFam;
AAO.(iFam).MemberOf                   = { 'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qh04', 1, [01 1], 'FQH04' };

HW2PhysicsParams  = magnetcoefficients('QH04');
Physics2HWParams  = magnetcoefficients('QH04');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


%% *** FQH05 ***
iFam = 'FQH05';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = { 'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qh05', 1, [01 1], 'FQH05'};

HW2PhysicsParams  = magnetcoefficients('QH05');
Physics2HWParams  = magnetcoefficients('QH05');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


%% *** FQH06 ***
iFam = 'FQH06';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = { 'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qh06', 1, [01 1], 'FQH06'};

HW2PhysicsParams  = magnetcoefficients('QH06');
Physics2HWParams  = magnetcoefficients('QH06');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 225];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


%% *** FQH07 ***
iFam = 'FQH07';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qh07', 1, [02 1], 'QH07'};

HW2PhysicsParams  = magnetcoefficients('QH07');
Physics2HWParams  = magnetcoefficients('QH07');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 225];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);




%% *** FQH08 ***
iFam = 'FQH08';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qh08', 1, [02 1], 'FQH08'};

HW2PhysicsParams  = magnetcoefficients('QH08');
Physics2HWParams  = magnetcoefficients('QH08');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


%% *** FQH09 ***
iFam = 'FQH09';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qh09', 1, [02 2], 'FQH09'};

HW2PhysicsParams  = magnetcoefficients('QH09');
Physics2HWParams  = magnetcoefficients('QH09');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


%% *** FQH10 ***
iFam = 'FQH10';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qh10', 1, [02 2], 'FQH10'};

HW2PhysicsParams  = magnetcoefficients('QH10');
Physics2HWParams  = magnetcoefficients('QH10');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 225];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%% *** FQV01 ***
iFam = 'FQV01';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qv01', 1, [01 1], 'QV01'};

HW2PhysicsParams  = magnetcoefficients('QV01');
Physics2HWParams  = magnetcoefficients('QV01');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);



%% *** FQV02 ***
iFam = 'FQV02';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qv02', 1, [01 1], 'FQV02'};

HW2PhysicsParams  = magnetcoefficients('QV02');
Physics2HWParams  = magnetcoefficients('QV02');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


%% *** FQV03 ***
iFam = 'FQV03';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr/pc/qv03', 1, [02 1], 'FQV03'};

HW2PhysicsParams  = magnetcoefficients('QV03');
Physics2HWParams  = magnetcoefficients('QV03');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);


%% *** FQV04 ***
iFam = 'FQV04';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
    1,	'sr02/pc/qv04', 1, [02 2], 'FQV04'};

HW2PhysicsParams  = magnetcoefficients('QV04');
Physics2HWParams  = magnetcoefficients('QV04');


for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [20 200];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);





%===============
%Sextupole data
%===============
%% *** SF1 ***
iFam = 'SF1';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector';};
AO.(iFam).DeviceList                 = [
    1 1
    %     4 2
    %     5 1
    %     8 2
    %     9 1
    %     12 2
    %     15 1
    %     16 2
    ];
AO.(iFam).ElementList                = (1:size(AO.(iFam).DeviceList,1))';
AO.(iFam).Status          = 1;
AO.(iFam).DeviceName    = 'sr/pc/sh01';
%AO.(iFam).CommonNames   = iFam;

HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');

val = 1.0; % scaling factor
AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');

AO.(iFam).Setpoint.Range(:,:) = [0 215];
AO.(iFam).Setpoint.Tolerance     = 0.05;
AO.(iFam).Setpoint.DeltaRespMat  = 0.5; % Hardware units (gets set later)


%% *** SF2 ***
iFam = 'SF2';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector';};
AO.(iFam).DeviceList                 = [
    1 1
    %     4 1
    %     5 1
    %     8 1
    %     9 1
    %     12 1
    %     15 1
    %     16 1
    ];
AO.(iFam).ElementList                = (1:size(AO.(iFam).DeviceList,1))';
AO.(iFam).Status          = 1;
AO.(iFam).DeviceName    = 'sr/pc/sh02';
%AO.(iFam).CommonNames   = iFam;

HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');

val = 1.0; % scaling factor
AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');

AO.(iFam).Setpoint.Range(:,:) = [0 215];
AO.(iFam).Setpoint.Tolerance     = 0.05;
AO.(iFam).Setpoint.DeltaRespMat  = 0.5; % Hardware units (gets set later)


%% *** SF3 ***
iFam = 'SF3';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector';};
AO.(iFam).DeviceList                 = [
    1 2
    %     2 1
    %     2 2
    %     3 1
    %     3 2
    %     4 1
    %     5 2
    %     6 1
    %     6 2
    %     7 1
    %     7 2
    %     8 1
    %     9 2
    %     10 1
    %     10 2
    %     11 1
    %     11 2
    %     12 1
    %     13 2
    %     14 1
    %     14 2
    %     15 1
    %     15 2
    %     16 1
    ];
AO.(iFam).ElementList                = (1:size(AO.(iFam).DeviceList,1))';
AO.(iFam).Status          = 1;
AO.(iFam).DeviceName    = 'sr/pc/sh03';
%AO.(iFam).CommonNames   = iFam;

HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');

val = 1.0; % scaling factor
AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');

AO.(iFam).Setpoint.Range(:,:) = [0 215];
AO.(iFam).Setpoint.Tolerance     = 0.05;
AO.(iFam).Setpoint.DeltaRespMat  = 0.5; % Hardware units (gets set later)


%% *** SF4 ***
iFam = 'SF4';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector';};
AO.(iFam).DeviceList                 = [
    2 1
    %     2 2
    %     3 1
    %     3 2
    %     6 1
    %     6 2
    %     7 1
    %     7 2
    %     10 1
    %     10 2
    %     11 1
    %     11 2
    %     14 1
    %     14 2
    %     15 1
    %     15 2
    ];
AO.(iFam).ElementList                = (1:size(AO.(iFam).DeviceList,1))';
AO.(iFam).Status          = 1;
AO.(iFam).DeviceName    = 'sr/pc/sh04';
%AO.(iFam).CommonNames   = iFam;

HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');

val = 1.0; % scaling factor
AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');

AO.(iFam).Setpoint.Range(:,:) = [0 215];
AO.(iFam).Setpoint.Tolerance     = 0.05;
AO.(iFam).Setpoint.DeltaRespMat  = 0.5; % Hardware units (gets set later)


%% *** SD1 ***
iFam = 'SD1';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector';};
AO.(iFam).DeviceList                 = [
    1 1
    %     4 2
    %     5 1
    %     8 2
    %     9 1
    %     12 2
    %     15 1
    %     16 2
    ];
AO.(iFam).ElementList                = (1:size(AO.(iFam).DeviceList,1))';
AO.(iFam).Status          = 1;
AO.(iFam).DeviceName    = 'sr/pc/sv01';
%AO.(iFam).CommonNames   = iFam;

HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');

val = 1.0; % scaling factor
AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');

AO.(iFam).Setpoint.Range = [0 215];
AO.(iFam).Setpoint.Tolerance     = 0.05;
AO.(iFam).Setpoint.DeltaRespMat  = 0.5; % Hardware units (gets set later)



%% *** SD2 ***
iFam = 'SD2';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector';};
AO.(iFam).DeviceList                 = [
    1 1
    %     4 2
    %     5 1
    %     8 2
    %     9 1
    %     12 2
    %     13 1
    %     16 2
    ];
AO.(iFam).ElementList                = (1:size(AO.(iFam).DeviceList,1))';
AO.(iFam).Status          = 1;
AO.(iFam).DeviceName    = 'sr/pc/sv02';
%AO.(iFam).CommonNames   = iFam;

HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');

val = 1.0; % scaling factor
AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');

AO.(iFam).Setpoint.Range = [0 215];
AO.(iFam).Setpoint.Tolerance     = 0.05;
AO.(iFam).Setpoint.DeltaRespMat  = 0.5; % Hardware units (gets set later)


%% *** SD3 ***
iFam = 'SD3';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector';};
AO.(iFam).DeviceList                 = [
    1 2
    %     4 1
    %     5 2
    %     8 1
    %     9 2
    %     12 1
    %     13 2
    %     16 1
    ];
AO.(iFam).ElementList                = (1:size(AO.(iFam).DeviceList,1))';
AO.(iFam).Status          = 1;
AO.(iFam).DeviceName    = 'sr/pc/sv03';
%AO.(iFam).CommonNames   = iFam;

HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');

val = 1.0; % scaling factor
AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');

AO.(iFam).Setpoint.Range = [0 215];
AO.(iFam).Setpoint.Tolerance     = 0.05;
AO.(iFam).Setpoint.DeltaRespMat  = 0.5; % Hardware units (gets set later)



%% *** SD4 ***
iFam = 'SD4';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector';};
AO.(iFam).DeviceList                 = [
    1 2
    %     2 1
    %     2 2
    %     3 1
    %     3 2
    %     4 1
    %     5 2
    %     6 1
    %     6 2
    %     7 1
    %     7 2
    %     8 1
    %     9 2
    %     10 1
    %     10 2
    %     11 1
    %     11 2
    %     12 1
    %     13 2
    %     14 1
    %     14 2
    %     15 1
    %     15 2
    %     16 1
    ];
AO.(iFam).ElementList                = (1:size(AO.(iFam).DeviceList,1))';
AO.(iFam).Status          = 1;
AO.(iFam).DeviceName    = 'sr/pc/sv04';
%AO.(iFam).CommonNames   = iFam;

HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');

val = 1.0; % scaling factor
AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');
AO.(iFam).Setpoint.Range = [0 215];
AO.(iFam).Setpoint.Tolerance     = 0.05;
AO.(iFam).Setpoint.DeltaRespMat  = 0.5; % Hardware units (gets set later)


%% *** SD5 ***
iFam = 'SD5';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SEXT'; 'Magnet'; 'Chromaticity Corrector';};
AO.(iFam).DeviceList                 = [
    2 1
    %     2 2
    %     3 1
    %     3 2
    %     6 1
    %     6 2
    %     7 1
    %     7 2
    %     10 1
    %     10 2
    %     11 1
    %     11 2
    %     14 1
    %     14 2
    %     15 1
    %     15 2
    ];
AO.(iFam).ElementList                = (1:size(AO.(iFam).DeviceList,1))';
AO.(iFam).Status          = 1;
AO.(iFam).DeviceName    = 'sr/pc/sv05';
%AO.(iFam).CommonNames   = iFam;

HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'm^-2';
AO.(iFam).Monitor.TangoNames  = strcat(AO.(iFam).DeviceName,'/Current');

val = 1.0; % scaling factor
AO.(iFam).Monitor.HW2PhysicsParams{1}(1,:)               = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(1,:)               = val;
AO.(iFam).Monitor.Physics2HWParams{1}(1,:)               = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(1,:)               = val;

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf     = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames   = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');
AO.(iFam).Setpoint.Range = [0 215];
AO.(iFam).Setpoint.Tolerance     = 0.05;
AO.(iFam).Setpoint.DeltaRespMat  = 0.5; % Hardware units (gets set later)


%% *** QS ***
iFam = 'QS';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'SkewQuad'; 'Magnet'; 'Coupling Corrector';};
AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'm^-2';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;

%  ElemList TangoName    Status DevList Common name
cor={
    1, 'sr01/pc/qsk-01', 1,[1 1],'QSK_01_01'
    2, 'sr01/pc/qsk-02', 1,[1 2],'QSK_01_02'
    3, 'sr02/pc/qsk-01', 1,[2 1],'QSK_02_01'
    4, 'sr02/pc/qsk-02', 1,[2 2],'QSK_02_02'
    5, 'sr03/pc/qsk-01', 1,[3 1],'QSK_03_01'
    6, 'sr03/pc/qsk-02', 1,[3 2],'QSK_03_02'
    7, 'sr04/pc/qsk-01', 1,[4 1],'QSK_04_01'
    8, 'sr04/pc/qsk-02', 1,[4 2],'QSK_04_02'
    9, 'sr05/pc/qsk-01', 1,[5 1],'QSK_05_01'
    10, 'sr05/pc/qsk-02', 1,[5 2],'QSK_05_02'
    11, 'sr06/pc/qsk-01', 1,[6 1],'QSK_06_01'
    12, 'sr06/pc/qsk-02', 1,[6 2],'QSK_06_02'
    13, 'sr07/pc/qsk-01', 1,[7 1],'QSK_07_01'
    14, 'sr07/pc/qsk-02', 1,[7 2],'QSK_07_02'
    15, 'sr08/pc/qsk-01', 1,[8 1],'QSK_08_01'
    16, 'sr08/pc/qsk-02', 1,[8 2],'QSK_08_02'
    17, 'sr09/pc/qsk-01', 1,[9 1],'QSK_09_01'
    18, 'sr09/pc/qsk-02', 1,[9 2],'QSK_09_02'
    19, 'sr10/pc/qsk-01', 1,[10 1],'QSK_10_01'
    20, 'sr10/pc/qsk-02', 1,[10 2],'QSK_10_02'
    21, 'sr11/pc/qsk-01', 1,[11 1],'QSK_11_01'
    22, 'sr11/pc/qsk-02', 1,[11 2],'QSK_11_02'
    23, 'sr12/pc/qsk-01', 1,[12 1],'QSK_12_01'
    24, 'sr12/pc/qsk-02', 1,[12 2],'QSK_12_02'
    25, 'sr13/pc/qsk-01', 1,[13 1],'QSK_13_01'
    26, 'sr13/pc/qsk-02', 1,[13 2],'QSK_13_02'
    27, 'sr14/pc/qsk-01', 1,[14 1],'QSK_14_01'
    28, 'sr14/pc/qsk-02', 1,[14 2],'QSK_14_02'
    29, 'sr15/pc/qsk-01', 1,[15 1],'QSK_15_01'
    30, 'sr15/pc/qsk-02', 1,[15 2],'QSK_15_02'
    31, 'sr16/pc/qsk-01', 1,[16 1],'QSK_16_01'
    32, 'sr16/pc/qsk-02', 1,[16 2],'QSK_16_02'

    };

HW2PhysicsParams                     = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams                     = HW2PhysicsParams;

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};
    AO.(iFam).ManagerStatus(ii,:)             = cor{ii,3};
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-20 20];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-2; % with 1e-7 it can't be stabilized
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 1.0;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;

end

%===========================================================
%% HCM
%===========================================================

iFam ='HCM';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).MemberOf                 = {'HCOR'; 'COR'; 'HCM'; 'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
AO.(iFam).Setpoint.SpecialFunctionGet     = 'getpvgroup';

%  ElemList TangoName    Status DevList Common name
cor={
    1, 'sr01/pc/corh-01', 1,[1 1],'HCM_01_01'
    2, 'sr01/pc/corh-02', 1,[1 2],'HCM_01_02'
    3, 'sr01/pc/corh-03', 1,[1 3],'HCM_01_03'
    4, 'sr01/pc/corh-04', 1,[1 4],'HCM_01_04'
    5, 'sr01/pc/corh-05', 1,[1 5],'HCM_01_05'
    6, 'sr02/pc/corh-01', 1,[2 1],'HCM_02_01'
    7, 'sr02/pc/corh-02', 1,[2 2],'HCM_02_02'
    8, 'sr02/pc/corh-03', 1,[2 3],'HCM_02_03'
    9, 'sr02/pc/corh-04', 1,[2 4],'HCM_02_04'
    10, 'sr02/pc/corh-05', 1,[2 5],'HCM_02_05'
    11, 'sr02/pc/corh-06', 1,[2 6],'HCM_02_06'
    12, 'sr03/pc/corh-01', 1,[3 1],'HCM_03_01'
    13, 'sr03/pc/corh-02', 1,[3 2],'HCM_03_02'
    14, 'sr03/pc/corh-03', 1,[3 3],'HCM_03_03'
    15, 'sr03/pc/corh-04', 1,[3 4],'HCM_03_04'
    16, 'sr03/pc/corh-05', 1,[3 5],'HCM_03_05'
    17, 'sr03/pc/corh-06', 1,[3 6],'HCM_03_06'
    18, 'sr04/pc/corh-01', 1,[4 1],'HCM_04_01'
    19, 'sr04/pc/corh-02', 1,[4 2],'HCM_04_02'
    20, 'sr04/pc/corh-03', 1,[4 3],'HCM_04_03'
    21, 'sr04/pc/corh-04', 1,[4 4],'HCM_04_04'
    22, 'sr04/pc/corh-05', 1,[4 5],'HCM_04_05'
    23, 'sr05/pc/corh-01', 1,[5 1],'HCM_05_01'
    24, 'sr05/pc/corh-02', 1,[5 2],'HCM_05_02'
    25, 'sr05/pc/corh-03', 1,[5 3],'HCM_05_03'
    26, 'sr05/pc/corh-04', 1,[5 4],'HCM_05_04'
    27, 'sr05/pc/corh-05', 1,[5 5],'HCM_05_05'
    28, 'sr06/pc/corh-01', 1,[6 1],'HCM_06_01'
    29, 'sr06/pc/corh-02', 1,[6 2],'HCM_06_02'
    30, 'sr06/pc/corh-03', 1,[6 3],'HCM_06_03'
    31, 'sr06/pc/corh-04', 1,[6 4],'HCM_06_04'
    32, 'sr06/pc/corh-05', 1,[6 5],'HCM_06_05'
    33, 'sr06/pc/corh-06', 1,[6 6],'HCM_06_06'
    34, 'sr07/pc/corh-01', 1,[7 1],'HCM_07_01'
    35, 'sr07/pc/corh-02', 1,[7 2],'HCM_07_02'
    36, 'sr07/pc/corh-03', 1,[7 3],'HCM_07_03'
    37, 'sr07/pc/corh-04', 1,[7 4],'HCM_07_04'
    38, 'sr07/pc/corh-05', 1,[7 5],'HCM_07_05'
    39, 'sr07/pc/corh-06', 1,[7 6],'HCM_07_06'
    40, 'sr08/pc/corh-01', 1,[8 1],'HCM_08_01'
    41, 'sr08/pc/corh-02', 1,[8 2],'HCM_08_02'
    42, 'sr08/pc/corh-03', 1,[8 3],'HCM_08_03'
    43, 'sr08/pc/corh-04', 1,[8 4],'HCM_08_04'
    44, 'sr08/pc/corh-05', 1,[8 5],'HCM_08_05'
    45, 'sr09/pc/corh-01', 1,[9 1],'HCM_09_01'
    46, 'sr09/pc/corh-02', 1,[9 2],'HCM_09_02'
    47, 'sr09/pc/corh-03', 1,[9 3],'HCM_09_03'
    48, 'sr09/pc/corh-04', 1,[9 4],'HCM_09_04'
    49, 'sr09/pc/corh-05', 1,[9 5],'HCM_09_05'
    50, 'sr10/pc/corh-01', 1,[10 1],'HCM_10_01'
    51, 'sr10/pc/corh-02', 1,[10 2],'HCM_10_02'
    52, 'sr10/pc/corh-03', 1,[10 3],'HCM_10_03'
    53, 'sr10/pc/corh-04', 1,[10 4],'HCM_10_04'
    54, 'sr10/pc/corh-05', 1,[10 5],'HCM_10_05'
    55, 'sr10/pc/corh-06', 1,[10 6],'HCM_10_06'
    56, 'sr11/pc/corh-01', 1,[11 1],'HCM_11_01'
    57, 'sr11/pc/corh-02', 1,[11 2],'HCM_11_02' %it failed
    58, 'sr11/pc/corh-03', 1,[11 3],'HCM_11_03' %it failed
    59, 'sr11/pc/corh-04', 1,[11 4],'HCM_11_04'
    60, 'sr11/pc/corh-05', 1,[11 5],'HCM_11_05'
    61, 'sr11/pc/corh-06', 1,[11 6],'HCM_11_06'
    62, 'sr12/pc/corh-01', 1,[12 1],'HCM_12_01'
    63, 'sr12/pc/corh-02', 1,[12 2],'HCM_12_02'
    64, 'sr12/pc/corh-03', 1,[12 3],'HCM_12_03'
    65, 'sr12/pc/corh-04', 1,[12 4],'HCM_12_04'
    66, 'sr12/pc/corh-05', 1,[12 5],'HCM_12_05'
    67, 'sr13/pc/corh-01', 1,[13 1],'HCM_13_01'
    68, 'sr13/pc/corh-02', 1,[13 2],'HCM_13_02'
    69, 'sr13/pc/corh-03', 1,[13 3],'HCM_13_03'
    70, 'sr13/pc/corh-04', 1,[13 4],'HCM_13_04'
    71, 'sr13/pc/corh-05', 1,[13 5],'HCM_13_05'
    72, 'sr14/pc/corh-01', 1,[14 1],'HCM_14_01'
    73, 'sr14/pc/corh-02', 1,[14 2],'HCM_14_02'
    74, 'sr14/pc/corh-03', 1,[14 3],'HCM_14_03'
    75, 'sr14/pc/corh-04', 1,[14 4],'HCM_14_04'
    76, 'sr14/pc/corh-05', 1,[14 5],'HCM_14_05'
    77, 'sr14/pc/corh-06', 1,[14 6],'HCM_14_06'
    78, 'sr15/pc/corh-01', 1,[15 1],'HCM_15_01'
    79, 'sr15/pc/corh-02', 1,[15 2],'HCM_15_02'
    80, 'sr15/pc/corh-03', 1,[15 3],'HCM_15_03'
    81, 'sr15/pc/corh-04', 1,[15 4],'HCM_15_04'
    82, 'sr15/pc/corh-05', 1,[15 5],'HCM_15_05'
    83, 'sr15/pc/corh-06', 1,[15 6],'HCM_15_06'
    84, 'sr16/pc/corh-01', 1,[16 1],'HCM_16_01'
    85, 'sr16/pc/corh-02', 1,[16 2],'HCM_16_02'
    86, 'sr16/pc/corh-03', 1,[16 3],'HCM_16_03'
    87, 'sr16/pc/corh-04', 1,[16 4],'HCM_16_04'
    88, 'sr16/pc/corh-05', 1,[16 5],'HCM_16_05'
    };



%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset


for ii=1:size(cor,1)
    [C, Leff, MagnetType, coefficients] = magnetcoefficients('HCM',230,'Amps',ii);
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint'); % use the setpoint to monitor
    AO.(iFam).Status(ii,:)             = cor{ii,3};
    AO.(iFam).ManagerStatus(ii,:)             = cor{ii,3};
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-10 10];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-5; % with 1e-7 it can't be stabilized
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.08e-3/coefficients(end-1); % was 1
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
end

AO.(iFam).Status = AO.(iFam).Status(:);

% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('HCM');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end

%% VCM

iFam ='VCM';

AO.(iFam).FamilyName               = iFam;
AO.(iFam).MemberOf                 = {'COR'; 'VCOR'; 'VCM'; 'Magnet'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;
AO.(iFam).Monitor.SpecialFunctionGet     = 'getpvgroup';

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;
%to work properly, in setpv, 'Special' flag has to be activated only if all
%elements of the samily are set
AO.(iFam).Setpoint.SpecialFunctionSet     = 'setpvgroup';
AO.(iFam).Setpoint.SpecialFunctionGet     = 'getpvgroup';

%  ElemList TangoName    Status DevList Common name
cor={
    1, 'sr01/pc/corv-01', 1,[1 1],'VCM_01_01'
    2, 'sr01/pc/corv-02', 1,[1 2],'VCM_01_02'
    3, 'sr01/pc/corv-03', 1,[1 3],'VCM_01_03'
    4, 'sr01/pc/corv-04', 1,[1 4],'VCM_01_04'
    5, 'sr01/pc/corv-05', 1,[1 5],'VCM_01_05'
    6, 'sr02/pc/corv-01', 1,[2 1],'VCM_02_01'
    7, 'sr02/pc/corv-02', 1,[2 2],'VCM_02_02'
    8, 'sr02/pc/corv-03', 1,[2 3],'VCM_02_03'
    9, 'sr02/pc/corv-04', 1,[2 4],'VCM_02_04'
    10, 'sr02/pc/corv-05', 1,[2 5],'VCM_02_05'
    11, 'sr02/pc/corv-06', 1,[2 6],'VCM_02_06'
    12, 'sr03/pc/corv-01', 1,[3 1],'VCM_03_01'
    13, 'sr03/pc/corv-02', 1,[3 2],'VCM_03_02'
    14, 'sr03/pc/corv-03', 1,[3 3],'VCM_03_03'
    15, 'sr03/pc/corv-04', 1,[3 4],'VCM_03_04'
    16, 'sr03/pc/corv-05', 1,[3 5],'VCM_03_05'
    17, 'sr03/pc/corv-06', 1,[3 6],'VCM_03_06'
    18, 'sr04/pc/corv-01', 1,[4 1],'VCM_04_01'
    19, 'sr04/pc/corv-02', 1,[4 2],'VCM_04_02'
    20, 'sr04/pc/corv-03', 1,[4 3],'VCM_04_03'
    21, 'sr04/pc/corv-04', 1,[4 4],'VCM_04_04'
    22, 'sr04/pc/corv-05', 1,[4 5],'VCM_04_05'
    23, 'sr05/pc/corv-01', 1,[5 1],'VCM_05_01'
    24, 'sr05/pc/corv-02', 1,[5 2],'VCM_05_02'
    25, 'sr05/pc/corv-03', 1,[5 3],'VCM_05_03'
    26, 'sr05/pc/corv-04', 1,[5 4],'VCM_05_04'
    27, 'sr05/pc/corv-05', 1,[5 5],'VCM_05_05'
    28, 'sr06/pc/corv-01', 1,[6 1],'VCM_06_01'
    29, 'sr06/pc/corv-02', 1,[6 2],'VCM_06_02'
    30, 'sr06/pc/corv-03', 1,[6 3],'VCM_06_03'
    31, 'sr06/pc/corv-04', 1,[6 4],'VCM_06_04'
    32, 'sr06/pc/corv-05', 1,[6 5],'VCM_06_05'
    33, 'sr06/pc/corv-06', 1,[6 6],'VCM_06_06'
    34, 'sr07/pc/corv-01', 1,[7 1],'VCM_07_01'
    35, 'sr07/pc/corv-02', 1,[7 2],'VCM_07_02'
    36, 'sr07/pc/corv-03', 1,[7 3],'VCM_07_03'
    37, 'sr07/pc/corv-04', 1,[7 4],'VCM_07_04'
    38, 'sr07/pc/corv-05', 1,[7 5],'VCM_07_05'
    39, 'sr07/pc/corv-06', 1,[7 6],'VCM_07_06'
    40, 'sr08/pc/corv-01', 1,[8 1],'VCM_08_01'
    41, 'sr08/pc/corv-02', 1,[8 2],'VCM_08_02'
    42, 'sr08/pc/corv-03', 1,[8 3],'VCM_08_03'
    43, 'sr08/pc/corv-04', 1,[8 4],'VCM_08_04'
    44, 'sr08/pc/corv-05', 1,[8 5],'VCM_08_05'
    45, 'sr09/pc/corv-01', 1,[9 1],'VCM_09_01'
    46, 'sr09/pc/corv-02', 1,[9 2],'VCM_09_02'
    47, 'sr09/pc/corv-03', 1,[9 3],'VCM_09_03'
    48, 'sr09/pc/corv-04', 1,[9 4],'VCM_09_04'
    49, 'sr09/pc/corv-05', 1,[9 5],'VCM_09_05'
    50, 'sr10/pc/corv-01', 1,[10 1],'VCM_10_01'
    51, 'sr10/pc/corv-02', 1,[10 2],'VCM_10_02'
    52, 'sr10/pc/corv-03', 1,[10 3],'VCM_10_03'
    53, 'sr10/pc/corv-04', 1,[10 4],'VCM_10_04'
    54, 'sr10/pc/corv-05', 1,[10 5],'VCM_10_05'
    55, 'sr10/pc/corv-06', 1,[10 6],'VCM_10_06'
    56, 'sr11/pc/corv-01', 1,[11 1],'VCM_11_01'
    57, 'sr11/pc/corv-02', 1,[11 2],'VCM_11_02' % it failed
    58, 'sr11/pc/corv-03', 1,[11 3],'VCM_11_03' % it failed
    59, 'sr11/pc/corv-04', 1,[11 4],'VCM_11_04'
    60, 'sr11/pc/corv-05', 1,[11 5],'VCM_11_05'
    61, 'sr11/pc/corv-06', 1,[11 6],'VCM_11_06'
    62, 'sr12/pc/corv-01', 1,[12 1],'VCM_12_01'
    63, 'sr12/pc/corv-02', 1,[12 2],'VCM_12_02'
    64, 'sr12/pc/corv-03', 1,[12 3],'VCM_12_03'
    65, 'sr12/pc/corv-04', 1,[12 4],'VCM_12_04'
    66, 'sr12/pc/corv-05', 1,[12 5],'VCM_12_05'
    67, 'sr13/pc/corv-01', 1,[13 1],'VCM_13_01'
    68, 'sr13/pc/corv-02', 1,[13 2],'VCM_13_02'
    69, 'sr13/pc/corv-03', 1,[13 3],'VCM_13_03'
    70, 'sr13/pc/corv-04', 1,[13 4],'VCM_13_04'
    71, 'sr13/pc/corv-05', 1,[13 5],'VCM_13_05'
    72, 'sr14/pc/corv-01', 1,[14 1],'VCM_14_01'
    73, 'sr14/pc/corv-02', 1,[14 2],'VCM_14_02'
    74, 'sr14/pc/corv-03', 1,[14 3],'VCM_14_03'
    75, 'sr14/pc/corv-04', 1,[14 4],'VCM_14_04'
    76, 'sr14/pc/corv-05', 1,[14 5],'VCM_14_05'
    77, 'sr14/pc/corv-06', 1,[14 6],'VCM_14_06'
    78, 'sr15/pc/corv-01', 1,[15 1],'VCM_15_01'
    79, 'sr15/pc/corv-02', 1,[15 2],'VCM_15_02'
    80, 'sr15/pc/corv-03', 1,[15 3],'VCM_15_03'
    81, 'sr15/pc/corv-04', 1,[15 4],'VCM_15_04'
    82, 'sr15/pc/corv-05', 1,[15 5],'VCM_15_05'
    83, 'sr15/pc/corv-06', 1,[15 6],'VCM_15_06'
    84, 'sr16/pc/corv-01', 1,[16 1],'VCM_16_01'
    85, 'sr16/pc/corv-02', 1,[16 2],'VCM_16_02'
    86, 'sr16/pc/corv-03', 1,[16 3],'VCM_16_03'
    87, 'sr16/pc/corv-04', 1,[16 4],'VCM_16_04'
    88, 'sr16/pc/corv-05', 1,[16 5],'VCM_16_05'
    };

%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset


for ii=1:size(cor,1)
    [C, Leff, MagnetType, coefficients] = magnetcoefficients('VCM',230,'Amps',ii);
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Status(ii,:)             = cor{ii,3};
    AO.(iFam).ManagerStatus(ii,:)             = cor{ii,3};
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-10 10];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-5;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.20e-3/coefficients(end-1); %was 1
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
end

AO.(iFam).Status = AO.(iFam).Status(:);


% Group
if ControlRoomFlag
    AO.(iFam).GroupId = tango_group_create2('VCM');
    tango_group_add(AO.(iFam).GroupId,AO.(iFam).DeviceName');
else
    AO.(iFam).GroupId = NaN;
end

%
%% Kickers magnets
%
% *** IK ***
iFam = 'IK';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).FamilyType           = 'InjectionKicker';
AO.(iFam).MemberOf                   = {'KICKER'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('IK');
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'V';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';
AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf           = {'MachineConfig'; 'PlotFamily';};

kick={1,	'sr/pc/kiinj-01', 1, [16 1], 'KIINJ1',1
    2,	'sr/pc/kiinj-02', 1, [16 2], 'KIINJ2',1
    3,	'sr/pc/kiinj-03', 1, [1 1], 'KIINJ3',1
    4,	'sr/pc/kiinj-04', 1, [1 2], 'KIINJ4',1
    };

val=1;
for ii=1:size(kick,1)
    AO.(iFam).ElementList(ii,:)        = kick{ii,1};
    AO.(iFam).DeviceName(ii,:)         = kick(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(kick(ii,2), '/VoltageSetpoint');
    AO.(iFam).Status(ii,:)             = kick{ii,3};
    AO.(iFam).DeviceList(ii,:)         = kick{ii,4};
    AO.(iFam).CommonNames(ii,:)        = kick{ii,5};
    AO.(iFam).Polarity(ii,:)             = kick{ii,6};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = kick{ii,6}*HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = kick{ii,6}*Physics2HWParams;
    AO.(iFam).Monitor.HW2PhysicsParams{2}(ii,:) = val;
    AO.(iFam).Monitor.Physics2HWParams{2}(ii,:) = val;
    AO.(iFam).Monitor.Range(ii,:)        = [0 8500];
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(kick(ii,2), '/VoltageSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 8500];
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 0.001;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.5;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = kick{ii,6}*HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = kick{ii,6}*Physics2HWParams;
    AO.(iFam).Setpoint.HW2PhysicsParams{2}(ii,:) = val;
    AO.(iFam).Setpoint.Physics2HWParams{2}(ii,:) = val;
end
AO.(iFam).Status = AO.(iFam).Status(:);


%% SEPTUM magnet
%
% *** SEPTUM ***
iFam = 'SEPTUM';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).FamilyType           = 'SEPTUM';
AO.(iFam).MemberOf                   = {'SEPTUM'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('SEPTUM');
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'V';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';
AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf           = {'MachineConfig'; 'PlotFamily';};

AO.(iFam).DeviceName(:,:) = {'sr/pc/seinj'};

AO.(iFam).DeviceName(:,:) = {'sr/pc/seinj'};
val = 1;
AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/VoltageSetpoint');
AO.(iFam).DeviceList(:,:) = [1 1];
AO.(iFam).ElementList(:,:)= 1;
AO.(iFam).Status          = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(iFam).Monitor.Range(:,:) = [0 700];
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/VoltageSetpoint');
AO.(iFam).Setpoint.Tolerance(:,:) = 0.001;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.5;



%====
%% DCCT
%====
AO.DCCT.FamilyName                     = 'DCCT';
AO.DCCT.MemberOf                       = {'DCCT'};
AO.DCCT.DeviceList                     = [1 1];
AO.DCCT.ElementList                    = 1;
AO.DCCT.Status                         = AO.DCCT.ElementList;
AO.DCCT.DeviceName                     = 'sr/di/dcct';
AO.DCCT.CommonNames                    = 'DCCT';

AO.DCCT.Monitor.Mode                   = Mode;
AO.DCCT.Monitor.DataType               = 'Scalar';
AO.DCCT.Monitor.TangoNames             = strcat(AO.DCCT.DeviceName(1,:), '/AverageCurrent');
AO.DCCT.Monitor.Units                  = 'Hardware';
AO.DCCT.Monitor.HWUnits                = 'milli-A';
AO.DCCT.Monitor.PhysicsUnits           = 'A';
AO.DCCT.Monitor.HW2PhysicsParams       = 1;
AO.DCCT.Monitor.Physics2HWParams       = 1;


% %============
% %% RF System
% %============
% AO.RF.FamilyName                  = 'RF';
% AO.RF.MemberOf                    = {'RF'};
% AO.RF.DeviceList                  = [1 1];
% AO.RF.ElementList                 = 1;
% AO.RF.Status                      = 1;
% AO.RF.DeviceName                     = 'sr09/rf/sgn-01';
% AO.RF.CommonNames                 = 'RF';
%
% AO.RF.Monitor.MemberOf           = {};
% AO.RF.Monitor.Mode                = Mode;
% AO.RF.Monitor.DataType            = 'Scalar';
% AO.RF.Monitor.Units               = 'Hardware';
% AO.RF.Monitor.HW2PhysicsParams    = 1e+6;
% AO.RF.Monitor.Physics2HWParams    = 1e-6;
% AO.RF.Monitor.HWUnits             = 'MHz';
% AO.RF.Monitor.PhysicsUnits        = 'Hz';
% AO.RF.Monitor.TangoNames        = [strcat(AO.RF.DeviceName), '/Frequency'];
%
% AO.RF.Setpoint.MemberOf           = {'MachineConfig';};
% AO.RF.Setpoint.Mode               = Mode;
% AO.RF.Setpoint.DataType           = 'Scalar';
% AO.RF.Setpoint.Units              = 'Hardware';
% AO.RF.Setpoint.HW2PhysicsParams   = 1e+6;
% AO.RF.Setpoint.Physics2HWParams   = 1e-6;
% AO.RF.Setpoint.HWUnits            = 'MHz';
% AO.RF.Setpoint.PhysicsUnits       = 'Hz';
% AO.RF.Setpoint.TangoNames       = '';
% AO.RF.Setpoint.Range              = [0 501];
% AO.RF.Setpoint.Tolerance          = 1;
%============
%% RF System
%============

AO.RF.FamilyName                  = 'RF';
AO.RF.MemberOf                    = {'RF'};
AO.RF.ElementList                 = 1;
AO.RF.Status                      = 1;
AO.RF.DeviceList                  = [1 1];
AO.RF.DeviceName                  = 'SR09/rf/sgn-01';
AO.RF.CommonNames                 = 'RF';

AO.RF.Monitor.MemberOf           = {};
AO.RF.Monitor.Mode                = Mode;
AO.RF.Monitor.DataType            = 'Scalar';
AO.RF.Monitor.Units               = 'Hardware';
AO.RF.Monitor.HW2PhysicsParams    = 1;
AO.RF.Monitor.Physics2HWParams    = 1;
AO.RF.Monitor.HWUnits             = 'Hz';
AO.RF.Monitor.PhysicsUnits        = 'Hz';
% AO.RF.Monitor.HW2PhysicsParams    = 1e+6;
% AO.RF.Monitor.Physics2HWParams    = 1e-6;
% AO.RF.Monitor.HWUnits             = 'MHz';
% AO.RF.Monitor.PhysicsUnits        = 'Hz';
AO.RF.Monitor.TangoNames        = [strcat(AO.RF.DeviceName), '/Frequency'];

AO.RF.Setpoint.MemberOf           = {'MachineConfig'};
AO.RF.Setpoint.Mode               = Mode;
AO.RF.Setpoint.DataType           = 'Scalar';
AO.RF.Setpoint.Units              = 'Hardware';
AO.RF.Setpoint.HW2PhysicsParams   = 1;
AO.RF.Setpoint.Physics2HWParams   = 1;
AO.RF.Setpoint.HWUnits            = 'Hz';
AO.RF.Setpoint.PhysicsUnits       = 'Hz';
% AO.RF.Setpoint.HW2PhysicsParams   = 1e+6;
% AO.RF.Setpoint.Physics2HWParams   = 1e-6;
% AO.RF.Setpoint.HWUnits            = 'MHz';
% AO.RF.Setpoint.PhysicsUnits       = 'Hz';
AO.RF.Setpoint.TangoNames       =  [strcat(AO.RF.DeviceName), '/Frequency'];
AO.RF.Setpoint.Range              = [499E6 501E6];
AO.RF.Setpoint.Tolerance          = 1;


%====
%% TUNE
%====
AO.TUNE.FamilyName  = 'TUNE';
AO.TUNE.MemberOf    = {'TUNE'};
AO.TUNE.DeviceList  = [ 1 1; 1 2; 1 3];
AO.TUNE.DeviceName  = ['SR/DI/TuneH','SR/DI/TuneV'];
AO.TUNE.ElementList = [1 2 3]';
AO.TUNE.Status      = [1 1 0]';
AO.TUNE.CommonNames = ['xtune';'ytune'];

AO.TUNE.Monitor.MemberOf         = {};
AO.TUNE.Monitor.Mode             = Mode;
AO.TUNE.Monitor.DataType         = 'Scalar';
AO.TUNE.Monitor.TangoNames       = ['SR/DI/TuneH/Nu';'SR/DI/TuneV/Nu'];
AO.TUNE.Monitor.Units            = 'Hardware';
AO.TUNE.Monitor.HW2PhysicsParams = 1;
AO.TUNE.Monitor.Physics2HWParams = 1;
AO.TUNE.Monitor.HWUnits          = 'fractional tune';
AO.TUNE.Monitor.PhysicsUnits     = 'fractional tune';



% Marker for the id source. Behave like a bpm
% ntxrs=15;
% AO.XRS.FamilyName               = 'XRS';
% AO.XRS.MemberOf                 = {'PlotFamily';  'Diagnostics'};
% AO.XRS.Monitor.Mode             = Mode;
% AO.XRS.Monitor.DataType         = 'Vector';
% AO.XRS.Monitor.DataTypeIndex    = [1:ntxrs];
% AO.XRS.Monitor.Units            = 'Hardware';
% AO.XRS.Monitor.HWUnits          = 'mm';
% AO.XRS.Monitor.PhysicsUnits     = 'meter';
% AO.XRS.Monitor.HW2PhysicsParams = 1e-3;
% AO.XRS.Monitor.Physics2HWParams = 1000;


% The operational mode sets the path, filenames, and other important parameters
% Run setoperationalmode after most of the AO is built so that the Units and Mode fields
% can be set in setoperationalmode
setao(AO);
setoperationalmode(OperationalMode);
AO = getao;


%======================================================================
%======================================================================
%% Append Accelerator Toolbox information   --->>> this gets done in updateatindex (GP)
%======================================================================
%======================================================================
% disp('   Initializing Accelerator Toolbox information');
%
% ATindx = atindex(THERING);  %structure with fields containing indices
%
% s = findspos(THERING,1:length(THERING)+1)';
%
% %% Horizontal BPMs
% % WARNING: BPM1 is the one before the injection straigth section
% %          since a cell begins from begin of Straigths
% % CELL1 BPM1 to BPM7
% iFam = ('BPMx');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BPM(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
%
% %% Vertical BPMs
% iFam = ('BPMy');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BPM(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
%
% %% SLOW HORIZONTAL CORRECTORS
% iFam = ('HCM');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.COR(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
%
% %% SLOW VERTICAL CORRECTORS
% iFam = ('VCM');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.COR(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);  %for SPEAR 3 horizontal and vertical correctors at same s-position
%
%
% %% SKEW QUADS
% iFam = ('QS');
% AO.(iFam).AT.ATType  = 'SkewQuad';
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
%
% %% BENDING magnets
% iFam = ('BEND');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BEND(:);
% %AT.(iFam).Position   = s(AT.(iFam).AT.ATIndex);
% % One group of all dipoles
% AO.(iFam).Position   = reshape(s(AO.(iFam).AT.ATIndex),1,32);
% %AT.(iFam).AT.ATParamGroup = mkparamgroup(THERING,AT.(iFam).AT.ATIndex,'K2');
%
%
% %% QUADRUPOLES
% for k = 1:3,
%     iFam = ['QD' num2str(k)];
%     AO.(iFam).AT.ATType  = 'QUAD';
%     AO.(iFam).AT.ATIndex = eval(['ATindx.' iFam '(:)']);
%     AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% end
%
% for k = 1:8,
%     iFam = ['QF' num2str(k)];
%     AO.(iFam).AT.ATType  = 'QUAD';
%     AO.(iFam).AT.ATIndex = eval(['ATindx.' iFam '(:)']);
%     AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% end
%
% %% SEXTUPOLES
% for k = 1:5,
%     iFam = ['SD' num2str(k)];
%     AO.(iFam).AT.ATType  = 'SEXT';
%     AO.(iFam).AT.ATIndex = eval(['ATindx.' iFam '(:)']);
%     AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
%     AO.(iFam).AT.ATParamGroup = mkparamgroup(THERING,AO.(iFam).AT.ATIndex,'K2');
% end
%
% for k = 1:4,
%     iFam = ['SF' num2str(k)];
%     AO.(iFam).AT.ATType  = 'SEXT';
%     AO.(iFam).AT.ATIndex = eval(['ATindx.' iFam '(:)']);
%     AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
%     AO.(iFam).AT.ATParamGroup = mkparamgroup(THERING,AO.(iFam).AT.ATIndex,'K2');
% end


%======================================================================
%% Set the deltas used when getting a response matrix
%======================================================================
% AO.HCM.Setpoint.DeltaRespMat = physics2hw('HCM','Setpoint', 0.2e-4, AO.HCM.DeviceList);
% AO.VCM.Setpoint.DeltaRespMat = physics2hw('VCM','Setpoint', 0.2e-4, AO.VCM.DeviceList);

% AO.QH01.Setpoint.DeltaRespMat  = physics2hw('QH01', 'Setpoint', AO.QH01.Setpoint.DeltaRespMat,  AO.QH01.DeviceList);
% AO.QH02.Setpoint.DeltaRespMat  = physics2hw('QH02', 'Setpoint', AO.QH02.Setpoint.DeltaRespMat,  AO.QH02.DeviceList);
% AO.QH03.Setpoint.DeltaRespMat  = physics2hw('QH03', 'Setpoint', AO.QH03.Setpoint.DeltaRespMat,  AO.QH03.DeviceList);
% AO.QH04.Setpoint.DeltaRespMat  = physics2hw('QH04', 'Setpoint', AO.QH04.Setpoint.DeltaRespMat,  AO.QH04.DeviceList);
% AO.QH05.Setpoint.DeltaRespMat  = physics2hw('QH05', 'Setpoint', AO.QH05.Setpoint.DeltaRespMat,  AO.QH05.DeviceList);
% AO.QH06.Setpoint.DeltaRespMat  = physics2hw('QH06', 'Setpoint', AO.QH06.Setpoint.DeltaRespMat,  AO.QH06.DeviceList);
% AO.QH07.Setpoint.DeltaRespMat  = physics2hw('QH07', 'Setpoint', AO.QH07.Setpoint.DeltaRespMat,  AO.QH07.DeviceList);
% AO.QH08.Setpoint.DeltaRespMat  = physics2hw('QH08', 'Setpoint', AO.QH08.Setpoint.DeltaRespMat,  AO.QH08.DeviceList);
%
% AO.QV01.Setpoint.DeltaRespMat  = physics2hw('QV01', 'Setpoint', AO.QV01.Setpoint.DeltaRespMat,  AO.QV01.DeviceList);
% AO.QV02.Setpoint.DeltaRespMat  = physics2hw('QV02', 'Setpoint', AO.QV02.Setpoint.DeltaRespMat,  AO.QV02.DeviceList);
% AO.QV03.Setpoint.DeltaRespMat  = physics2hw('QV03', 'Setpoint', AO.QV03.Setpoint.DeltaRespMat,  AO.QV03.DeviceList);

% AO.SF1.Setpoint.DeltaRespMat  = physics2hw('SF1', 'Setpoint', AO.SF1.Setpoint.DeltaRespMat,  AO.SF1.DeviceList);
% AO.SF2.Setpoint.DeltaRespMat  = physics2hw('SF2', 'Setpoint', AO.SF2.Setpoint.DeltaRespMat,  AO.SF2.DeviceList);
% AO.SF3.Setpoint.DeltaRespMat  = physics2hw('SF3', 'Setpoint', AO.SF3.Setpoint.DeltaRespMat,  AO.SF3.DeviceList);
% AO.SF4.Setpoint.DeltaRespMat  = physics2hw('SF4', 'Setpoint', AO.SF4.Setpoint.DeltaRespMat,  AO.SF4.DeviceList);
%
% AO.SD1.Setpoint.DeltaRespMat  = physics2hw('SD1', 'Setpoint', AO.SD1.Setpoint.DeltaRespMat,  AO.SD1.DeviceList);
% AO.SD2.Setpoint.DeltaRespMat  = physics2hw('SD2', 'Setpoint', AO.SD2.Setpoint.DeltaRespMat,  AO.SD2.DeviceList);
% AO.SD3.Setpoint.DeltaRespMat  = physics2hw('SD3', 'Setpoint', AO.SD3.Setpoint.DeltaRespMat,  AO.SD3.DeviceList);
% AO.SD4.Setpoint.DeltaRespMat  = physics2hw('SD4', 'Setpoint', AO.SD4.Setpoint.DeltaRespMat,  AO.SD4.DeviceList);
% AO.SD5.Setpoint.DeltaRespMat  = physics2hw('SD5', 'Setpoint', AO.SD5.Setpoint.DeltaRespMat,  AO.SD5.DeviceList);

%% Extras - Checking is BPM [9 2] works

% AO.BPMx.Monitor.TangoNames(62)={'sr09/di/bpm-04/XPosSA'};
% AO.BPMy.Monitor.TangoNames(62)={'sr09/di/bpm-04/ZPosSA'};

%%

setao(AO);

% reference values
global refOptic;
disp '   Reference optics, tunes and AO stored in refOptic'
refOptic.AO=getao();
refOptic.twiss=gettwiss();
%refOptic.tune= thetune(0);

%disp '   set 6 samples for the SA average'
%replies = tango_group_write_attribute(AO.BPMx.GroupId, 'SAStatNumSamples',0,int32(6));

%[devlist status]=ReadBadBPMs;
%fprintf(1,'%ld bpms set to Status 0\n',size(devlist,1))
%SetBPMGolden('Golden');
