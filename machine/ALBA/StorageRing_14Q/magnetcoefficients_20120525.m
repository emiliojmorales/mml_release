function [C, Leff, MagnetType, A] = magnetcoefficients(MagnetCoreType, Amps, InputType,element)
%MAGNETCOEFFICIENTS - Retrieves coefficient for conversion between Physics and Hardware units
%[C, Leff, MagnetType, A] = magnetcoefficients(MagnetCoreType)
%
% INPUTS
% 1. MagnetCoreType - Family name or type of magnet
%
% OUTPUTS
% 1. C vector coefficients for the polynomial expansion of the magnet field
% based on magnet measurements
% 2. Leff - Effective length ie, which is used in AT
% 3. MagnetType
% 4. A - vector coefficients for the polynomial expansion of the curviline
% integral of the magnet field based on magnet measurements
%
% C and A are vector coefficients for the polynomial expansion of the magnet field
% based on magnet measurements.
%
% The amp2k and k2amp functions convert between the two types of units.
%   amp2k returns BLeff, B'Leff, or B"Leff scaled by Brho if A-coefficients are used.
%   amp2k returns B , B' , or B" scaled by Brho if C-coefficients are used.
%
% The A coefficients are direct from magnet measurements with a DC term:
%   a8*I^8+a7*I^7+a6*I^6+a5*I^5+a4*I^4+a3*I^3+a2*I^2+a1*I+a0 = B*Leff or B'*Leff or B"*Leff
%   A = [a8 a7 a6 a5 a4 a3 a2 a1 a0]
%
% C coefficients have been scaled to field (AT units, except correctors) and includes a DC term:
%   c8 * I^8+ c7 * I^7+ c6 * I^6 + c5 * I^5 + c4 * I^4 + c3 * I^3 + c2 * I^2 + c1*I + c0 = B or B' or B"
%   C = A/Leff
%
% For dipole:  k = B / Brho  (for AT: KickAngle = BLeff / Brho)
% For quadrupole:  k = B'/ Brho
% For sextupole:   k = B"/ Brho / 2  (to be compatible with AT)
%  (all coefficients all divided by 2 for sextupoles)
%
% MagnetCoreType is the magnet measurements name for the magnet core (string, string matrix, or cell)
%   For SOLEIL:   BEND
% Q1 - Q10 S1 - S10,
% QT, HCOR, VCOR, FHCOR, FVCOR
%
% Leff is the effective length of the magnet
%
% See Also amp2k, k2amp

%
% Written by M. Yoon 4/8/03
% Adapted By Laurent S. Nadolski
%

% NOTE: Make sure the sign on the 'C' coefficients is reversed where positive current generates negative K-values
% Or use Tango K value set to -1
 global THERING;

if nargin < 1
    error('MagnetCoreType input required');
end

if nargin < 2
    Amps = 230;  % not sure!!!
end

if nargin < 3
    InputType = 'Amps';
end

if nargin < 4
    element = 1;
end


% For a string matrix
if iscell(MagnetCoreType)
    for i = 1:size(MagnetCoreType,1)
        for j = 1:size(MagnetCoreType,2)
            [C{i,j}, Leff{i,j}, MagnetType{i,j}, A{i,j}] = magnetcoefficients(MagnetCoreType{i});
        end
    end
    return
end

% For a string matrix
if size(MagnetCoreType,1) > 1
    C=[]; Leff=[]; MagnetType=[]; A=[];
    for i = 1:size(MagnetCoreType,1)
        [C1, Leff1, MagnetType1, A1] = magnetcoefficients(MagnetCoreType(i,:));
        C(i,:) = C1;
        Leff(i,:) = Leff1;
        MagnetType = strvcat(MagnetType, MagnetType1);
        A(i,:) = A1;
    end
    return
end

switch upper(deblank(MagnetCoreType))
    
    case 'BEND'
        % B = 1.4214 T for I = 580 A
        Leff = 1.373885080906250;
        B2BL=1.38207940624283;
        a7= 0.0;
        a6= 2.5197534E-17;
        a5= -6.3708275E-14;
        a4= 4.9566676E-11;
        a3= -1.6507507E-08;
        a2= 2.4260118E-06;
        a1= 2.6631898E-03; % BL/I @ 3 GeV
        a0= 3.0441848E-03;
        A = B2BL*[a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'BEND';
        
    case {'QH01'}
        % k=2 for I = 180 A @ 3 GeV
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.290162591216293*(1-0.30e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  1.873266E-13;
        a5=  -1.156563E-10;
        a4=  2.428201E-08;
        a3=  -2.218933E-06;
        a2=  8.800573E-05;
        a1=  3.409444E-02; % kL/I*Brho
        a0=  2.712275E-02;
        
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';
        
    case {'QH02'}
        % k=2 for I = 180 A @ 3 GeV
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.289076322974338*(1-0.30e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  1.873266E-13;
        a5=  -1.156563E-10;
        a4=  2.428201E-08;
        a3=  -2.218933E-06;
        a2=  8.800573E-05;
        a1=  3.409444E-02; % kL/I*Brho
        a0=  2.712275E-02;
        
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';
        
    case {'QH03'}
        % k=2 for I = 180 A @ 3 GeV
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.290050455701024*(1-0.10e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  1.873266E-13;
        a5=  -1.156563E-10;
        a4=  2.428201E-08;
        a3=  -2.218933E-06;
        a2=  8.800573E-05;
        a1=  3.409444E-02; % kL/I*Brho
        a0=  2.712275E-02;
        
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';
        
    case {'QH04'}
        % k=2 for I = 180 A
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.229925397373565*(1-0.20e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  3.5949816E-13;
        a5=  -2.0498900E-10;
        a4=  4.1260880E-08;
        a3=  -3.6783256E-06;
        a2=  1.4367192E-04;
        a1=  2.6042896E-02; % kL/I*Brho
        a0=  2.0190358E-02;
        
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';
        
    case {'QH05'}
        % k=2 for I = 180 A
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.309585300051539*(1-0.15e-3); % modified according to LOCO 22/05/2012
        
        
        a7=  0.0;
        a6=  1.396818E-13;
        a5=  -9.045494E-11;
        a4=  1.937861E-08;
        a3=  -1.788428E-06;
        a2=  7.130147E-05;
        a1=  3.679677E-02; % kL/I*Brho
        a0=  2.710055E-02;
        
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';
        
    case {'QH06'}
        % k=2.2 for I = 215 A
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.527653513646467*(1-0.30e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  2.080858E-13;
        a5=  -1.482385E-10;
        a4=  3.614300E-08;
        a3=  - 3.893774E-06;
        a2=   1.840073E-04;
        a1=  6.163093E-02; % k = 2.2 for 215 A @ 3 GeV
        a0=  4.428150E-02;
        
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';
    case {'QH07'}
        % k=2.2 for I = 215 A
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.528184007720045*(1-1.20e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  2.080858E-13;
        a5=  -1.482385E-10;
        a4=  3.614300E-08;
        a3=  - 3.893774E-06;
        a2=   1.840073E-04;
        a1=  6.163093E-02; % k = 2.2 for 215 A @ 3 GeV
        a0=  4.428150E-02;
        
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';
    case {'QH08'}
        % k=2 for I = 180 A
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.308817938383968*(1-6.95e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  1.396818E-13;
        a5=  -9.045494E-11;
        a4=  1.937861E-08;
        a3=  -1.788428E-06;
        a2=  7.130147E-05;
        a1=  3.679677E-02; % kL/I*Brho
        a0=  2.710055E-02;
        
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';

     case {'QH09'}
        % k=2 for I = 180 A
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.308817938383968*(1-7.10e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  1.396818E-13;
        a5=  -9.045494E-11;
        a4=  1.937861E-08;
        a3=  -1.788428E-06;
        a2=  7.130147E-05;
        a1=  3.679677E-02; % kL/I*Brho
        a0=  2.710055E-02;
        
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';       

    case {'QH10'}
        % k=2.2 for I = 215 A
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.528184007720045*(1-0.80e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  2.080858E-13;
        a5=  -1.482385E-10;
        a4=  3.614300E-08;
        a3=  - 3.893774E-06;
        a2=   1.840073E-04;
        a1=  6.163093E-02; % k = 2.2 for 215 A @ 3 GeV
        a0=  4.428150E-02;
        
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';

        
    case {'QV01'}
        % Find the current from the given polynomial for B'Leff
        Leff=0.229461036229344*(1+0.25e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  3.5949816E-13;
        a5=  -2.0498900E-10;
        a4=  4.1260880E-08;
        a3=  -3.6783256E-06;
        a2=  1.4367192E-04;
        a1=  2.6042896E-02; % kL/I*Brho
        a0=  2.0190358E-02;
        
        A = -[a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'quad';
    case {'QV02'}
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.289137158793229*(1+1.70e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  1.873266E-13;
        a5=  -1.156563E-10;
        a4=  2.428201E-08;
        a3=  -2.218933E-06;
        a2=  8.800573E-05;
        a1=  3.409444E-02; % kL/I*Brho
        a0=  2.712275E-02;
        
        A = -[a7 a6 a5 a4 a3 a2 a1 a0];
        
        
        MagnetType = 'quad';
    case {'QV03'}
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.289210179039298*(1+3.90e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  1.873266E-13;
        a5=  -1.156563E-10;
        a4=  2.428201E-08;
        a3=  -2.218933E-06;
        a2=  8.800573E-05;
        a1=  3.409444E-02; % kL/I*Brho
        a0=  2.712275E-02;
        
        A = -[a7 a6 a5 a4 a3 a2 a1 a0];
        
        
        MagnetType = 'quad'; 
    case {'QV04'}
        
        % Find the current from the given polynomial for B'Leff
        Leff=0.289210179039298*(1+3.00e-3); % modified according to LOCO 22/05/2012
        
        a7=  0.0;
        a6=  1.873266E-13;
        a5=  -1.156563E-10;
        a4=  2.428201E-08;
        a3=  -2.218933E-06;
        a2=  8.800573E-05;
        a1=  3.409444E-02; % kL/I*Brho
        a0=  2.712275E-02;
        
        A = -[a7 a6 a5 a4 a3 a2 a1 a0];
        
        
        MagnetType = 'quad';
    case {'IK'}
        % SR injection Kicker
         Leff = 0.715; % m
%         A = [0 0 0 0 0 0 1.21140E-02/1000 1.66728E-04]*Leff;
        A = [0 0 0 0 0 0 1.21140E-02/1000 0]*Leff;
        
        MagnetType = 'KICKER';
        
     case {'SEPTUM'}
         % SR injection Septum
         Leff = 1.751; % m
%          A = [0 0 0 0 0 0 1.50856E-03 -5.76481E-03]*Leff;
         A = [0 0 0 0 0 0 1.50856E-03 0]*Leff;
         MagnetType = 'SEPTUM';
         
    case {'SD1','SD2', 'SD3', 'SD5','SF1','SF4'}
        % Find the current from the given polynomial for B''Leff
        Leff=0.15/2;
        switch upper(deblank(MagnetCoreType))
            case {'SD1'}
                Leff=0.17086697576;
                sig=-1;
            case {'SD2'}
                Leff=0.17041858771;
                sig=-1;
            case {'SD3'}
                Leff=0.17000351367;
                sig=-1;
            case {'SD5'}
                Leff=0.16991020052;
                sig=-1;
            case {'SF1'}
                Leff=0.172958010486228;
                sig=1;
            case {'SF4'}
                Leff=0.171819968084414;
                sig=1;
        end
        Leff=Leff/2;
        
        a7=  0.0;
        a6=  1.346747E-11;
        a5=  -7.542114E-09;
        a4=  1.504323E-06;
        a3=  -1.334211E-04;
        a2=  5.194605E-03;
        a1=  5.674396E-01; % ML = 5 for 200 A @ 3 GeV
        a0=  1.312733E+00;
        A = sig*[a7 a6 a5 a4 a3 a2 a1 a0]/2;
        
        MagnetType = 'SEXT';
        
    case {'SD4','SF2', 'SF3'}
        % Find the current from the given polynomial for B''Leff
        switch upper(deblank(MagnetCoreType))
            case {'SD4'}
                Leff=0.240773319561292;
                sig=-1;
            case {'SF2'}
                Leff=0.240764887993104;
                sig=1;
            case {'SF3'}
                Leff=0.240698339722994;
                sig=1;
        end
        Leff=Leff/2;
        a7=  0.0;
        a6=  2.5261813E-12;
        a5= - 1.7947663E-09;
        a4=  3.9877679E-07;
        a3=  - 3.7447882E-05;
        a2=  1.5063696E-03;
        a1=   8.7735799E-01; % ML = 5 for 200 A @ 3 GeV
        a0=  1.7721389E+00;
        A =  sig*[a7 a6 a5 a4 a3 a2 a1 a0]/2;
        MagnetType = 'SEXT';
        
    case 'QS1'
        Leff =0.170996209371774;
        Leff = 1e-6;
        v=[ 0.012239754361416  -0.000013907070265];
        Is=131.7851054366414;
        a7= 0.0;
        a6= 0.0;
        a5= 0.0;
        a4= 0.0;
        a3= 0.0;
        a2= 0.0;
        a1= v(1)+v(2)*Is;
        a0= 0.0;
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'SkewQuad';
    case 'QS2'
        Leff = 0.170996209371774;
        Leff = 1e-6;
        v=[ 0.012239754361416  -0.000013907070265];
        Is=138.6606277301034;
        a7= 0.0;
        a6= 0.0;
        a5= 0.0;
        a4= 0.0;
        a3= 0.0;
        a2= 0.0;
        a1= v(1)+v(2)*Is;
        a0= 0.0;
        A = [a7 a6 a5 a4 a3 a2 a1 a0];
        
        MagnetType = 'SkewQuad';
        
    case {'HCM'} 
        
        % element lists: the correctors in the different sextupole
        % fam,ilies have diferent calibrations and effective lengths
        Len_S220=[ 3 4 5 6 7 10 11 12 13 16 17 18 19 20 25 26 27 28 29 32 ...
            33 34 35 38 39 40 41 42 47 48 49 50 51 54 55 56 57 60 61 62 ...
            63 64 69 70 71 72 73 76 77 78 79 82 83 84 85 86];
        Len_S150=[1 2 8 9 14 15 21 22 23 24 30 31 36 37 43 44 45 46 52 ...
            53 58 59 65 66 67 68 74 75 80 81 87 88];
        SD1_hcm =[ 2 21 24 43 46 65 68 87];
        SD4_hcm =[ 4  7 10 13 16 19 26 29 32 35 38 41 48 51 54 57 60 63 ...
            70 73 76 79 82 85];
        SF1_hcm =[ 1 22 23 44 45 66 67 88];
        SF2_hcm =[ 3 20 25 42 47 64 69 86];
        SF3_hcm =[ 5  6 11 12 17 18 27 28 33 34 39 40 49 50 55 56 61 62 ...
            71 72 77 78 83 84];
        SF4_hcm =[ 8  9 14 15 30 31 36 37 52 53 58 59 74 75 80 81];

        %set length and 2D calibration  BL=v1+v2*Is+v3*I+v4*Is*I+v5*Is*Is
        if ismember(element,Len_S220)
            Leff = 0.240745515759130; 
             v=1e-3*[1.423870319293385  -0.000550531258128];
        elseif ismember(element,Len_S150)
            Leff =  0.170996209371774; 
             v=1e-3*[1.094389838543990  -0.001031642115164];
        else
            error('HCM element not found');
        end
        
        %set coefficients !!!!!!Try to do this reading the PC values? !!!!!
        if ismember(element,SD1_hcm)
            Is= 116.517;
        elseif ismember(element,SD4_hcm)
            Is= 129.690;
        elseif ismember(element,SF1_hcm)
            Is= 68.8897;
        elseif ismember(element,SF2_hcm)
            Is= 129.772;
        elseif ismember(element,SF3_hcm)
            Is= 130.4303;
        elseif ismember(element,SF4_hcm)
           Is=94.104;
        else
            error('HCM element not found');
        end
%         if ismember(element,SD1_hcm)
%             Is= 118.6;
%         elseif ismember(element,SD4_hcm)
%             Is= 129.9;
%         elseif ismember(element,SF1_hcm)
%             Is= 64.5;
%         elseif ismember(element,SF2_hcm)
%             Is= 119.9;
%         elseif ismember(element,SF3_hcm)
%             Is= 137.5;
%         elseif ismember(element,SF4_hcm)
%             Is= 96.6;
%         else
%             error('HCM element not found');
%         end

        a7= 0.0;
        a6= 0.0;
        a5= 0.0;
        a4= 0.0;
        a3= 0.0;
        a2= 0.0;
        a1= v(1)+v(2)*Is;
        a0= 0.0;
        A = [a7 a6 a5 a4 a3 a2 a1 a0];

        MagnetType = 'COR';
        
    case {'VCM'} 
        % element lists: the correctors in the different sextupole
        % fam,ilies have diferent calibrations and effective lengths
        Len_S220=[ 3 4 5 6 7 10 11 12 13 16 17 18 19 20 25 26 27 28 29 32 ...
            33 34 35 38 39 40 41 42 47 48 49 50 51 54 55 56 57 60 61 62 ...
            63 64 69 70 71 72 73 76 77 78 79 82 83 84 85 86];
        Len_S150=[1 2 8 9 14 15 21 22 23 24 30 31 36 37 43 44 45 46 52 ...
            53 58 59 65 66 67 68 74 75 80 81 87 88];
        SD1_hcm =[ 2 21 24 43 46 65 68 87];
        SD4_hcm =[ 4  7 10 13 16 19 26 29 32 35 38 41 48 51 54 57 60 63 ...
            70 73 76 79 82 85];
        SF1_hcm =[ 1 22 23 44 45 66 67 88];
        SF2_hcm =[ 3 20 25 42 47 64 69 86];
        SF3_hcm =[ 5  6 11 12 17 18 27 28 33 34 39 40 49 50 55 56 61 62 ...
            71 72 77 78 83 84];
        SF4_hcm =[ 8  9 14 15 30 31 36 37 52 53 58 59 74 75 80 81];

        %set length and 2D calibration  BL=v1+v2*Is+v3*I+v4*Is*I+v5*Is*Is
        if ismember(element,Len_S220)
            Leff = 0.240745515759130; 
             v=1e-3*[1.664380267057950  -0.000670668554612];
        elseif ismember(element,Len_S150)
            Leff =  0.170996209371774; 
             v=1e-3*[1.283756744361277  -0.001067989180490];
        else
            error('VCM element not found');
        end
        
        %set Sextupole settings
        if ismember(element,SD1_hcm)
            Is= 116.517;
        elseif ismember(element,SD4_hcm)
            Is= 129.690;
        elseif ismember(element,SF1_hcm)
            Is= 68.8897;
        elseif ismember(element,SF2_hcm)
            Is= 129.772;
        elseif ismember(element,SF3_hcm)
            Is= 130.4303;
        elseif ismember(element,SF4_hcm)
           Is=94.104;
        else
            error('HCM element not found');
        end


        a7= 0.0;
        a6= 0.0;
        a5= 0.0;
        a4= 0.0;
        a3= 0.0;
        a2= 0.0;
        a1= v(1)+v(2)*Is;
        a0= 0.0;
        A = [a7 a6 a5 a4 a3 a2 a1 a0];

        MagnetType = 'COR';       
    otherwise
        error(sprintf('MagnetCoreType %s is not unknown', MagnetCoreType));
        k = 0;
        MagnetType = '';
        return
end

% compute B-field = int(Bdl)/Leff
C = A / Leff;

MagnetType = upper(MagnetType);


% Power Series Denominator (Factoral) be AT compatible
if strcmpi(MagnetType,'SEXT')
    C = C / 2;
end
if strcmpi(MagnetType,'OCTO')
    C = C / 6;
end

end

function HCM_Coeff(element)

end
