function [AM, tout, DataTime, ErrorFlag] = getpvgroup(varargin)
%GETHBPMGROUP - Gets reading using TANGO groups
%
%  INPUTS
%  1. Familyname
%  2. Field
%  3. DeviceList - BPM devicelist
%  4. time
%
%  OUTPUTS
%  1. AM - horizontal beam position
%
% NOTES
% First shot, Status flag
%
%
% Written by Laurent S. Nadolski


t0 = clock;  % starting time for getting data
DataTime = 0;
ErrorFlag = 1;


Fam=varargin{1};
DeviceListTotal = family2dev(Fam);
DeviceList = varargin{3};
Field=varargin{2};
tangolist=getfamilydata(Fam,Field,'TangoNames');
[attribute device]  = getattribute(tangolist(1));


% if strcmp('BPM',Fam(1:3));
%     pause(0.25);
% end

GroupID = getfamilydata(Fam, 'GroupId');


R = tango_group_read_attributes(GroupID, attribute(1), 0);

if tango_error == -1
    tango_print_error_stack;
    ErrorFlag = 1;
    tout = etime(clock, t0);
    return;
else
    if R.has_failed > 0
        ErrorFlag = 1;
        for k=1:length(R.dev_replies),
            if R.dev_replies(k).has_failed
                %tango_print_error_stack_as_it_is(R.dev_replies(k).attr_values.error);
                %fprintf('Fail in BPM-%d of %d',k,length(R.dev_replies));
            end
        end
        ErrorFlag = 1;
        tout = etime(clock, t0);
        %return;
    end
end

% construct data
for k = 1:length(R.dev_replies),
    if ~R.dev_replies(k).has_failed 
        AM(k,1) = R.dev_replies(k).attr_values(1).value(1);
    else
        AM(k,1) = NaN;
    end
end
n=numel(AM);
tout = etime(clock, t0);
DataTime = R.dev_replies(1).attr_values(1).time; %time when data was measured accordint to Tango system
ao=getao;
Status = logical(ao.(Fam).Status);
%ElemList=(1:numel(Status))==dev2elem(Fam,DeviceList);
% keep only BPMs with status 1
ElemList=false(n,1);
ElemList(dev2elem(Fam,DeviceList))=true;
AM = double(AM(Status&ElemList));


end
