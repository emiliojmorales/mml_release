function  ALBAsteptune_AT_Q2_lin(varargin)
% ALBAsteptune([dqx, dqy]')
% Step the tune, using the fitted values of the quads
% 10/2010 Edited by Z.Marti, based in a previous version called ALBAsteptune

quadlist=findmemberof('QUAD');
nmagnet= size(quadlist,1);
%
%This has to be moved to the AO
if length(varargin) >= 1
    DeltaTune = varargin{1};
else
    DeltaTune = [];    
end
if isempty(DeltaTune)
    answer = inputdlg({'Change the horizontal tune by', 'Change the vertical tune by'},'STEPTUNE',1,{'0','0'});
    if isempty(answer)
        return
    end
    DeltaTune(1,1) = str2num(answer{1});
    DeltaTune(2,1) = str2num(answer{2});
end
DeltaTune = DeltaTune(:);
if size(DeltaTune,1) ~= 2
	error('Input must be a 2x1 column vector.');
end
if DeltaTune(1)==0 && DeltaTune(2)==0
    return
end

dqx=DeltaTune(1,1);
dqy=DeltaTune(2,1);

% Coeff=[0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                0                       0               0
%   -0.184552301683494   0.019444150447760  -1.293604936365715   1.755821200557125  -0.417951433135181   0.010913680633299   0.037459461348613   0.008019473569424 0.008019473569424 0.037459461348613   0.209969871505969  -0.070977525488766  -0.140984571725668 -0.140984571725668
%    0.132072721226580   0.108823579217246  -0.368471006969151   0.355976376427192  -0.126444489801525   0.070722775452058   0.068333060405694  -0.011005335040507 -0.011005335040507 0.068333060405694  -0.160184181305919  -0.080740412177741   0.036494746070601 0.036494746070601  
% ];

Coeff=[   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0
   -0.157937488151113  -0.001945991668562  -1.270862241909376   1.670490049230127  -0.392195142230796   0.013240086137106   0.029430119036358   0.011231552109752  0.011231552109752 0.029430119036358   0.207879135034606  -0.062486722379316  -0.122504416001955 -0.122504416001955
    0.171527430821284   0.082032221855173  -0.312366086724235   0.284165086758414  -0.112250422092648   0.066835187373428   0.065055424623903  -0.021218808302194 -0.021218808302194 0.065055424623903  -0.223929624106313  -0.041951889786755   0.073575335068197 0.073575335068197
];   

for i=1:nmagnet,
    magnet=cell2mat(quadlist(i));
    dm(i)=quadlist(i);
    dsp(i)=kk_func(Coeff(:,i),dqx,dqy);
end

for i=1:nmagnet,
    setsp(cell2mat(dm(i)), dsp(i)+getsp(cell2mat(dm(i)),1,'Physics'),'Physics');
end
% for i=1:nmagnet,
%     perturbe_family_quads(dm(i), dsp(i));
% end

end

function K2=kk_func(v,x,y)

order=2;
ndim=sumrec(order);
ll=0;
z_i=v;
for ii=1:order
    for jj=1:(order-ii+1)
        ll=ll+1;
        z_i(ll)=x^(ii-1)*y^(jj-1);
    end
end

K2=0;
for ii=1:ndim
    K2=K2+v(ii)*z_i(ii);
end

end

function jj=sumrec(ii)

if ii>1
    jj=ii+sumrec(ii-1);
else
    jj=1;
end
end

function perturbe_family_quads(family,dk)

global THERING;

ind=atindex(THERING);
indq=ind.(family{1});
nq=length(indq);

for ii=1:nq
    pB=THERING{indq(ii)}.PolynomB;
    THERING{indq(ii)}.PolynomB=[pB(1) pB(2)+dk pB(3:length(pB))];
    THERING{indq(ii)}.K=pB(2)+dk;
end

end
