function BPMData = quadcenterall(Plane)
%  Quadrupole Center Measurement in function of the BPM
%  Call quadcenter for all the quad families and BPM
%   Plane  = Plane (1=horizontal , 2=vertical)

disp 'Setting the init to 104 BPMs'
disp 'Remember to load again the right one afterwards!'
albainit('AP')
current=4;
outlimit=100;

%%%%%%%%%%%%%%%%%%%%%
% Make the BPM list %
%%%%%%%%%%%%%%%%%%%%%
BPMFamily = 'BPMy';

BPMDevList = getlist(BPMFamily); % only the ones with good status
%BPMDevList=[5 2;5 3];
%BPMDevList=[16 7]; 

startBPM=1
endBPM=length(BPMDevList(:,1))

% for i = 1:4
%     RemoveDeviceList(i,:) = [i 3];
% end
% i = findrowindex(RemoveDeviceList, BPMDevList);
% BPMDevList(i,:) = [];



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Clean out the data directory %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[QUADFamily, QUADDev] = bpm2quad(BPMFamily, BPMDevList(1,:));
q = quadcenterinit(QUADFamily, QUADDev, Plane);

DirStart = pwd;
%NewDir = 'old';
cd(q.DataDirectory);
% if ~exist(NewDir,'dir')
%     mkdir(NewDir);
% end
% try
%     movefile('*.mat', NewDir);
%     movefile('quadcenter.log', NewDir);
% catch
% end
% cd(DirStart);
% Make Directory with the start time
NewDir = sprintf('%4d-%02d-%02d_%02d-%02d-%02.0f', clock);
if ~exist(NewDir,'dir')
    try
        mkdir(NewDir);
    catch
    end
end
cd(DirStart)

fprintf(1,'Saving data in %s/%s\n',q.DataDirectory, NewDir);

%%%%%%%%%%%%%%%%%%%%%%%%
% Loop on all the BPMs %
%%%%%%%%%%%%%%%%%%%%%%%%
t0 = gettime;
for i = startBPM:endBPM,
     
    
    [QUADFamily, QUADDev, DelSpos, PhaseAdvanceX, PhaseAdvanceY] = bpm2quad(BPMFamily, BPMDevList(i,:));
    fprintf('   %d. BPM(%2d,%d)  %s(%2d,%d)  BPM-to-Quad Distance=%f meters\n', i, BPMDevList(i,:), QUADFamily, QUADDev, DelSpos);
    QMS=quadcenterinit2(QUADFamily,QUADDev,Plane, BPMDevList(i,:));
    QMS.OutlierFactor=outlimit;
    QMS.ExtraDelay=1.5;
    QMS.DataDirectory=[QMS.DataDirectory '/' NewDir];
    
    try
        [QMS1 QMS2] = quadcenter(QMS,2);
    catch
        fprintf('       Error occurred in BPM(%2d,%d).  Moving to the next BPM.\n', BPMDevList);
    end
    
    DCCT = getdcct;
    if DCCT < current
        % Redo magnet if the beam dumped
        sound(cos(1:10000));
        beep
        beep
        beep
        fprintf('   Current to low.  Refill and hit return.\n');
        pause;
        fprintf(' \n');
        [QMS1 QMS2] = quadcenter(QMS,2);
    elseif DCCT < current & i<size(BPMDevList,1)-1
        sound(cos(1:10000));
        beep
        beep
        beep
        fprintf('   Current to low.  Refill and hit return.\n');
        pause;
        fprintf(' \n');
    end
    BPMData(i)=QMS1;
    
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Move data to new directory by date %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DirStart = pwd;
% NewDir = sprintf('%4d-%02d-%02d_%02d-%02d-%02.0f', clock);
% cd(QMS1.DataDirectory);
% if ~exist(NewDir,'dir')
%     try
%         mkdir(NewDir);
%     catch
%     end
% end
% try
%     movefile('*.mat', NewDir);
%     fprintf('   Data moved to %s\n', [QMS1.DataDirectory '/' NewDir]);
%     try
%         movefile('quadcenter.log', NewDir);
%     catch
%         fprintf('   Error occurred when moving log files to %s\n', [QMS1.DataDirectory NewDir]);
%     end
% catch
%     fprintf('   Error occurred when moving data files to %s\n', [QMS1.DataDirectory NewDir]);
% end
% cd(NewDir)
save ([QMS.DataDirectory '/BPMData.mat'],'BPMData')


fprintf(1,'Data saved in %s/%s\n',q.DataDirectory, NewDir);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Print time and wake-up call %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('   Data collection time %f minutes\n', (gettime-t0)/60);
sound(cos(1:10000));
sound(cos(1:10000));
disp 'Remember to load again the right albainit!'
disp 'type albainit for the init for orbit correction (88 BPMs)'
disp 'type albainit('AP') for the one for measures (104 BPMs)'
