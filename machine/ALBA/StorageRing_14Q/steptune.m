function  ALBAsteptune_AT_newbend_HightQx_14Q(varargin)
% ALBAsteptune([dqx, dqy]')
% Step the tune, using the fitted values of the quads
% 10/2010 Edited by Z.Marti, based in a previous version called ALBAsteptune
% Ment change the tune in the region [0.05-0.45; 0.05-0.45] 

quadlist=findmemberof('QUAD');
nmagnet= size(quadlist,1);
%
%This has to be moved to the AO
if length(varargin) >= 1
    DeltaTune = varargin{1};
else
    DeltaTune = [];    
end
if isempty(DeltaTune)
    answer = inputdlg({'Change the horizontal tune by', 'Change the vertical tune by'},'STEPTUNE',1,{'0','0'});
    if isempty(answer)
        return
    end
    DeltaTune(1,1) = str2num(answer{1});
    DeltaTune(2,1) = str2num(answer{2});
end
DeltaTune = DeltaTune(:);
if size(DeltaTune,1) ~= 2
	error('Input must be a 2x1 column vector.');
end
if DeltaTune(1)==0 && DeltaTune(2)==0
    return
end

dqx=DeltaTune(1,1);
dqy=DeltaTune(2,1);

Coeff=[                    0                   0                   0                   0                   0                   0                   0          0         0       0            0                   0                   0         0
   0.029986759658061  -0.042044642073583  -0.181046374309545   0.012008625414135  -0.059109407976877   0.160484784975064   0.140605293653069  -0.196564353445072 -0.196564353445072 0.140605293653069 -0.131370449812888  -0.091556479825804  -0.289974377080385 -0.289974377080385
   0.059475558918550   0.016486939295560  -0.076655975020640   0.017836669771904  -0.026447435213768   0.084798134497927   0.143930813399157  -0.090299371628958 -0.090299371628958 0.143930813399157 -0.027433015029627  -0.057377707737083  -0.118254963126770 -0.118254963126770
   ];
for i=1:nmagnet,
    magnet=cell2mat(quadlist(i));
    dm(i)=quadlist(i);
    dsp(i)=kk_func(Coeff(:,i),dqx,dqy);
end

mode=getfamilydata('BPMx','Monitor','Mode');
if strcmp(mode,'Online')
    for i=1:nmagnet,
        setsp(cell2mat(dm(i)), dsp(i)+getsp(cell2mat(dm(i)),'Physics'),'Physics');
    end
else
    for i=1:nmagnet,
        perturbe_family_quads(dm(i), dsp(i));
    end
end

end

function K2=kk_func(v,x,y)

order=2;
ndim=sumrec(order);
ll=0;
z_i=v;
for ii=1:order
    for jj=1:(order-ii+1)
        ll=ll+1;
        z_i(ll)=x^(ii-1)*y^(jj-1);
    end
end

K2=0;
for ii=1:ndim
    K2=K2+v(ii)*z_i(ii);
end

end

function jj=sumrec(ii)

if ii>1
    jj=ii+sumrec(ii-1);
else
    jj=1;
end
end

function perturbe_family_quads(family,dk)

global THERING;

ind=atindex(THERING);
indq=ind.(family{1});
nq=length(indq);

for ii=1:nq
    pB=THERING{indq(ii)}.PolynomB;
    THERING{indq(ii)}.PolynomB=[pB(1) pB(2)+dk pB(3:length(pB))];
    THERING{indq(ii)}.K=pB(2)+dk;
end

end
