function f = ScaleMagnetsGUI()
% f = tl_display
% Displays the theoreticals optical functions and beam sizes
% in a transfer line.
% The input values are enter in the leftmost pane.
% The right pane provides tabs with the plots and the beam sizes
% at the position of the FS/OTR.
% author: M. Munoz, 27 November 2009.

% close already running versions
oldfig = findobj(allchild(0),'tag','ScaleMagnetGUI');
if ~isempty(oldfig), delete(oldfig); end
%create the figure, and the panels
f = figure('menubar','none', 'Position', [0 0 1200 900],'Resize', 'Off','Tag','ScaleMagnetGUI',...
    'Interruptible', 'on', ...
    'HandleVisibility','off', ...
    'Name', 'Scale Magnets GUI', ...
    'NumberTitle','off','CloseRequestFcn',@closeME);

%% 1st some shared information is created

% the information required by the different components of the gui is stored
% in handles.
handles = guihandles(f);
handles.f = f;
global THERING;
handles.ati=atindex(THERING);
%
quadh=findmemberof('QUADH');
handles.quadh=quadh;
quadv=findmemberof('QUADV');
handles.quadv=quadv;
bend=findmemberof('BEND');
handles.bend=bend;
handles.ref.quadh=getpv(quadh);
handles.ref.quadv=getpv(quadv);
handles.ref.bend=getpv(bend);
handles.bck=handles.ref;


%% Prepare the Gui

% The Gui is compossed of 3 main panels
mainpanel = uipanel('Parent', f);
% For input of the data
inputpanel = uipanel('Parent', mainpanel,'Units','normalized','Position',[0 0 1 0.2],'Title','Input');
% For a table with the quads current and strength
handles.magpanel=uipanel('Parent', mainpanel,'Units','normalized','Position',[0 0.2 1 0.8],'Title','Select Magnet Families to Scale');
handles.bendpanel=uipanel('Parent', handles.magpanel,'Units','normalized','Position',[0.02 0.05 0.3 0.93],'Title','Bend');
handles.qhpanel=uipanel('Parent', handles.magpanel,'Units','normalized','Position',  [0.35 0.05 0.3 0.93],'Title','Horizontal Quads');
handles.qvpanel=uipanel('Parent', handles.magpanel,'Units','normalized','Position',  [0.68 0.05 0.3 0.93],'Title','Vertical Quads');
handles.bendstatus=uicontrol('Style','checkbox', 'Parent', handles.bendpanel, 'Units','normalized','Position',[0.2 0.5 0.04 0.05], ...
    'UserData',bend, 'CallBack',@selectMagnet ,'TooltipString','Select it for scaling the bending magnet');
uibutton('Style','text', 'Parent', handles.bendpanel, 'Units','normalized','Position',[0.25 0.495 0.3 0.05],'String','Bending Magnet');

% now the quadh
for loop=1:length(quadh),
    handles.quadhstatus(loop)=uicontrol('Style','checkbox', 'Parent', handles.qhpanel, 'Units','normalized','Position',[0.2 1-loop*0.1 0.04 0.05], ...
        'UserData',cell2mat(quadh(loop)), 'CallBack',@selectMagnet,'TooltipString','Select it for scaling this quad family' );
    uibutton('Style','text', 'Parent', handles.qhpanel, 'Units','normalized','Position',[0.25 1-loop*0.1-0.005 0.3 0.05],'String',quadh(loop));
end
loop=9.5;
handles.quadhall=uicontrol('Style','checkbox', 'Parent', handles.qhpanel, 'Units','normalized','Position',[0.2 1-loop*0.1 0.04 0.05], ...
    'UserData','QUADH', 'CallBack',@selectMagnet,'TooltipString','Select it for scaling all horizontal quad family' );
uibutton('Style','text', 'Parent', handles.qhpanel, 'Units','normalized','Position',[0.25 1-loop*0.1-0.005 0.3 0.05],'String','All QF families');
% now for the quadv

for loop=1:length(quadv),
    handles.quadvstatus(loop)=uicontrol('Style','checkbox', 'Parent', handles.qvpanel, 'Units','normalized','Position',[0.2 1-loop*0.1 0.04 0.05], ...
        'UserData',cell2mat(quadv(loop)), 'CallBack',@selectMagnet,'TooltipString','Select it for scaling this quad family' );
    uibutton('Style','text', 'Parent', handles.qvpanel, 'Units','normalized','Position',[0.25 1-loop*0.1-0.005 0.3 0.05],'String',quadv(loop));
end
loop=9.5;
handles.quadvall=uicontrol('Style','checkbox', 'Parent', handles.qvpanel, 'Units','normalized','Position',[0.2 1-loop*0.1 0.04 0.05], ...
    'UserData','QUADV', 'CallBack',@selectMagnet ,'TooltipString','Select it for scaling all vertical quad family' );
uibutton('Style','text', 'Parent', handles.qvpanel, 'Units','normalized','Position',[0.25 1-loop*0.1-0.005 0.3 0.05],'String','All QD families');


% For the input panel, 2 buttons and a slider are created
handles.get_ref=uibutton('Style','pushbutton', 'Parent', inputpanel,'Units','normalized', 'String', 'Set Refence Values', ...
    'Position', [0.05 0.55 0.15 0.4], 'HorizontalAlignment', 'Right','CallBack', @setref_Callback, ...
    'HorizontalAlignment', 'Center',  'ForegroundColor', [0.3 .6 0.6]...
    ,'TooltipString','Set the refence values for the quads (all scaling is applied based in this values) ');
handles.set_ref=uibutton('Style','pushbutton', 'Parent', inputpanel,'Units','normalized', 'String', 'Restore Refence Values', ...
    'Position', [0.05 0.05 0.15 0.4], 'HorizontalAlignment', 'Right','CallBack', @getref_Callback, ...
    'HorizontalAlignment', 'Center',  'ForegroundColor', [0.9 .3 0.6],...
    'TooltipString','Restores the Refence Value to ALL families. Equivalent to apply a scaling of 100% to all the magnets.');

min_=90;
max_=110;
step_=0.01;
value=100;
tmp=step_/(max_-min_);

handles.slider=uicontrol('Style','Slider','Parent', inputpanel, 'Units','normalized', 'Position', [0.35 0.45 0.5 0.3],'CallBack',@SliderValue,...
    'Min',min_,'Max',max_,'Value',value,'SliderStep',[tmp  10*tmp],'Tag','Slider'...
    ,'TooltipString','Move to scale the selected families');
handles.min=uicontrol('Style','edit','Parent', inputpanel, 'Units','normalized', 'Position',      [0.25 0.45 0.08 0.3],'CallBack',@SliderSetup,...
    'String',num2str(min_),'Tag','Min',...
    'TooltipString','Minimum value for the slider. Enter to apply');
handles.max=uicontrol('Style','edit','Parent', inputpanel, 'Units','normalized', 'Position',      [0.87 0.45 0.08 0.3],'CallBack',@SliderSetup,...
    'String',num2str(max_),'Tag','Max',...
    'TooltipString','Maximum value for the slider. Enter to apply');
handles.step=uicontrol('Style','edit','Parent', inputpanel, 'Units','normalized', 'Position',     [0.55 0.8 0.1 0.15],'CallBack',@SliderSetup,...
    'String',num2str(step_),'Tag','Step',...
    'TooltipString','Step value for the slider. Enter to apply');
handles.value=uicontrol('Style','edit','Parent', inputpanel, 'Units','normalized', 'Position',    [0.55 0.05 0.1 0.3],'CallBack',@SliderSetup,...
    'String',num2str(value),'Tag','Value',...
    'TooltipString','Value (in %) for the slider. Enter to apply');


%% Finally, a small menu bar
m1=uimenu('Parent',f,'Label','File');
%m11=uimenu('Parent',m1,'Label','Create print','Callback', @print_menu);
m12=uimenu('Parent',m1,'Label','Exit','Separator','on','Callback', @exit_menu);
m2=uimenu('Parent',f,'Label','Save/Restore');
m21=uimenu('Parent',m2,'Label','Restore Starting Values','Callback', @restoreStart);
m22=uimenu('Parent',m2,'Label','Save Machine Config','Separator','on','Callback', @SaveMachineConfig);

% save the handles
guidata(f,handles);
% center the gui in the screen
movegui(f, 'center');

function print_menu(hObject, eventdata, handles)

function getref_Callback(hObject, eventdata, handles)
origen=get(hObject);
handles=guidata(hObject);
setpv(handles.quadh, handles.ref.quadh);
setpv(handles.quadv, handles.ref.quadv);
setpv(handles.bend, handles.ref.bend);
value=100;
set(handles.value,'String',num2str(100));
min_=min(value,str2num(get(handles.min,'String')));
set(handles.min,'String',num2str(min_));
max_=max(value,str2num(get(handles.max,'String')));
set(handles.max,'String',num2str(max_));
step_=str2num(get(handles.step,'String'));
step_=step_/(max_-min_);
set(handles.slider,'Value', value,'Max',max_,'Min',min_,'SliderStep',[step_ 10*step_]);

function setref_Callback(hObject, eventdata, handles)
origen=get(hObject);
handles=guidata(hObject);
handles.ref.quadh=getpv(handles.quadh);
handles.ref.quadv=getpv(handles.quadv);
handles.ref.bend=getpv(handles.bend);
guidata(hObject, handles);


function selectMagnet(hObject, eventdata, handles)
origen=get(hObject);
handles=guidata(hObject);
if strcmp(origen.UserData,'QUADH'),
    for loop=1:length(handles.quadh),
        set(handles.quadhstatus(loop),'Value', origen.Value)
    end
elseif strcmp(origen.UserData,'QUADV'),
    for loop=1:length(handles.quadv),
        set(handles.quadvstatus(loop),'Value', origen.Value)
    end
end

function SliderSetup(hObject, eventdata, handles)
origen=get(hObject);
handles=guidata(hObject);
value=str2num(get(handles.value,'String'));
min_=min(value,str2num(get(handles.min,'String')));
set(handles.min,'String',num2str(min_));
max_=max(value,str2num(get(handles.max,'String')));
set(handles.max,'String',num2str(max_));
step_=str2num(get(handles.step,'String'));
step_=step_/(max_-min_);
set(handles.slider,'Value', value,'Max',max_,'Min',min_,'SliderStep',[step_ 10*step_]);
if strcmp(origen.Tag,'Value'),
    UpdateMagnets(value,handles);
end




function SliderValue(hObject, eventdata, handles)
origen=get(hObject);
handles=guidata(hObject);
value=get(handles.slider,'Value');
set(handles.value, 'String', num2str(value));
UpdateMagnets(value,handles);


function exit_menu(hObject, eventdata, handles)
closeME(hObject, eventdata);

function closeME(hObject, eventdata)
disp 'Closing the figure and children'
try
    handles=guidata(hObject);
    delete(findall(0,'Tag','DeleteOnClose'))
    delete(handles.f);
catch
    delete(handles.f);
end

function SaveMachineConfig(hObject, eventdata)
savemachineconfig

function restoreStart(hObject, eventdata)
handles=guidata(hObject);
setsp(handles.bend, handles.bck.bend)
setsp(handles.quadh, handles.bck.quadh)
setsp(handles.quadv, handles.bck.quadv)
handles.ref=handles.bck;
value=100;
set(handles.value,'String',num2str(100));
min_=min(value,str2num(get(handles.min,'String')));
set(handles.min,'String',num2str(min_));
max_=max(value,str2num(get(handles.max,'String')));
set(handles.max,'String',num2str(max_));
step_=str2num(get(handles.step,'String'));
step_=step_/(max_-min_);
set(handles.slider,'Value', value,'Max',max_,'Min',min_,'SliderStep',[step_ 10*step_]);
guidata(hObject, handles);


function UpdateMagnets(value,handles)
value=value/100;
% bend
if get(handles.bendstatus,'Value'),
    setsp('BEND',value*cell2mat(handles.ref.bend));
end
% quad h
for loop=1:length(handles.quadh),
    if get(handles.quadhstatus(loop),'Value'),
        setsp(handles.quadh(loop), value*cell2mat(handles.ref.quadh(loop)));
    end
end
% quad v
for loop=1:length(handles.quadv),
    if get(handles.quadvstatus(loop),'Value'),
        setsp(handles.quadv(loop), value*cell2mat((handles.ref.quadv(loop))));
    end
end





