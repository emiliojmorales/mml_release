function result=SetQMSOfset(varargin)
% SetQMSOffset(dir)
% Set the QMS Offset found in a directory to the current machine.
% They are only apply for this session.

global oldfolder
if length(varargin)==0
    try
        folder_name = uigetdir(oldfolder);
    catch
        ad=getad;
        folder_name = uigetdir(ad.Directory.QMS);
    end
else
    folder_name = cell2mat(varargin(1));
end
oldfolder=folder_name
list=dir([folder_name '/s*mat']);
nbpms=length(list);
ao=getao;
ix=0;
iy=0;
for loop=1:nbpms,
    load([folder_name '/' list(loop).name]);
    if QMS.QuadPlane==1,
        ix=ix+1;
        BPMx(ix,:)=QMS.BPMDev;
        Offsetx(ix)=QMS.Center;
     else
        iy=iy+1;
        BPMy(iy,:)=QMS.BPMDev;
        Offsety(iy)=QMS.Center;
    end
end
SX=[];
Sy=[];
if ix,
    SX.FamilyName='BPMx';
    SX.Data=Offsetx';
    SX.DeviceList=BPMx;
    setoffset(SX)
end
if iy,
    SY.FamilyName='BPMy';
    SY.Data=Offsety';
    SY.DeviceList=BPMy;
    setoffset(SY)
end
end