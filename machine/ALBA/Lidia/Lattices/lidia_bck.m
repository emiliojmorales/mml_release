function lidia
%ALBA_LTB lattice definition file
% Created by G. Benedetti 18/08/08
%

global FAMLIST THERING GLOBVAL

GLOBVAL.E0 = 0.1e+9;
GLOBVAL.LatticeFile = mfilename;
FAMLIST = cell(0);

disp(['**   Loading ALBA LTB lattice ', mfilename]);

AP = aperture('AP', [-0.0155, 0.0155, -0.0155, 0.0155],'AperturePass');

% Cavity
L0 =  5.365+0.248;	% design length  [m]
C0 =   299792458; 	% speed of light [m/s]

DLI0  =    drift('DLI0', 0.248, 'DriftPass');
D101  =    drift('D101', 0.300, 'DriftPass');
D102  =    drift('D102', 0.150, 'DriftPass');
D103  =    drift('D103', 0.330, 'DriftPass');
D104  =    drift('D104', 0.180, 'DriftPass');
D105  =    drift('D105', 0.180, 'DriftPass');
D106  =    drift('D106', 0.445, 'DriftPass');
D107  =    drift('D106', 0.115, 'DriftPass');
D108  =    drift('D106', 0.230, 'DriftPass');

DIA1  =    drift('DIA1', 1.180, 'DriftPass');
DIA2  =    drift('DIA2', 0.359, 'DriftPass');
DIA3  =    drift('DIA3', 0.453, 'DriftPass');
DIA4  =    drift('DIA4', 0.660, 'DriftPass');

QT0101 =  quadrupole('Q', 0.12,  +5.76, 'StrMPoleSymplectic4Pass');
QT0102 =  quadrupole('Q', 0.12,  -7.87, 'StrMPoleSymplectic4Pass');
QT0103 =  quadrupole('Q', 0.12,  +6.98, 'StrMPoleSymplectic4Pass');
QT0201 =  quadrupole('Q', 0.12,  +1.e-10, 'StrMPoleSymplectic4Pass');

BEND = sbend('BEND', 0.303, 30*pi/180, 8.75/2*pi/180, (30-8.75/2)*pi/180,...
             0.0, 'BndMPoleSymplectic4Pass');

HCM = corrector('HCM', 0.0, [0 0],'CorrectorPass');
VCM = corrector('VCM', 0.0, [0 0],'CorrectorPass');
BPM = marker('BPM','IdentityPass');
VALVE = marker('VALVE','IdentityPass');
FSOTR = marker('FSOTR','IdentityPass');
SCRH = marker('SCRH','IdentityPass');
FCUP = marker('FCUP','IdentityPass');

% Begin Lattice
%----- Linac to Booster Transfer Line: LTB --------------------------!;
LIDIA = [BPM, DLI0, VALVE, D101, HCM, D102, VCM, D103, QT0101, D104, QT0102, D105,...
        QT0103, D106, BPM, D107, FSOTR, D108, BEND, DIA1, QT0201, DIA2, SCRH, DIA3,...
        FSOTR, DIA4, FCUP];

% INIT_LTB : Beta0,
%                 betx = 5.00, alfx = -1.00,
%                 bety = 5.00, alfy = -1.00,
%                 dx   = 0.00, dpx  = 0.00;

buildlat(LIDIA);

% set all magnets to the same energy
THERING = setcellstruct(THERING,'Energy',1:length(THERING),GLOBVAL.E0);

%evalin('caller','global THERING FAMLIST GLOBVAL');

%atsummary;

if nargout
    varargout{1} = THERING;
end

% Compute total length and RF frequency
L0_tot=0;
for i=1:length(THERING)
    L0_tot=L0_tot+THERING{i}.Length;
end
fprintf('   Model LTB length is %.6f meters\n', L0_tot)
%fprintf('   Model RF frequency is %.6f MHz\n', HarmNumber*C0/L0_tot/1e6)

clear global FAMLIST
%clear global GLOBVAL when GWig... has been changed.

%evalin('caller','global THERING FAMLIST GLOBVAL');
