function lidia
%ALBA_LTB lattice definition file
% Created by G. Benedetti 18/08/08
% Modificated by Z.Marti 02/03/2010

global FAMLIST THERING GLOBVAL
E_0 = .51099891e6;
GLOBVAL.E0 = 0.11005e+9-E_0; % Modificated by Z.Marti 02/03/2010
GLOBVAL.LatticeFile = mfilename;
FAMLIST = cell(0);

disp(['**   Loading ALBA LIDIA lattice ', mfilename]);

AP = aperture('AP', [-0.0155, 0.0155, -0.0155, 0.0155],'AperturePass');

% Cavity
L0 =  5.365+0.248;	% design length  [m]
C0 =   299792458; 	% speed of light [m/s]

%===============================
% Calibrated parameters  @ 02/03/2010, by  Z.Marti
%===============================

b1_phi1=6.212135996;
b1_phi2=6.03828097;
b1_leff=0.314389376;
b1_l0=0.303;
b1_edge_mult_1=[0 0 -1.320743228 -63.09785603];
b1_edge_mult_2=[0 0 -1.299040184 -60.9021407];
b1_mult=[0 0.10544360 10.24222874 524.6884223];

%===============================
DLI1   =    drift('DLI1' , 0.6955, 'DriftPass'); 
DLI2   =    drift('DLI2' , 0.2365, 'DriftPass'); 
DLI3   =    drift('DLI3' , 0.248, 'DriftPass'); %drift from last linac BPM and the linac valve interface (end ofthe linac)
D101  =    drift('D101', 0.300, 'DriftPass');
D102  =    drift('D102', 0.150, 'DriftPass');
D103  =    drift('D103', 0.327, 'DriftPass');
D104  =    drift('D104', 0.174, 'DriftPass');
D105  =    drift('D105', 0.174, 'DriftPass');
D106  =    drift('D106', 0.442, 'DriftPass');
D107  =    drift('D106', 0.130, 'DriftPass');
D108  =    drift('D106', 0.215-(b1_leff-b1_l0)/2, 'DriftPass');

DIA1  =    drift('DIA1', (1.180-0.003)/4, 'DriftPass');
DIA2  =    drift('DIA2', (0.359-0.003)/3, 'DriftPass');
DIA3  =    drift('DIA3', 0.453/3, 'DriftPass');
DIA4  =    drift('DIA4', 0.660/3, 'DriftPass');

% beam based calibration
QT0101=quadrupole('Q', 0.126,-6.9387845937,'StrMPoleSymplectic4Pass');
QT0102=quadrupole('Q', 0.126,9.9312806526,'StrMPoleSymplectic4Pass');
QT0103=quadrupole('Q', 0.126,-9.0324969043,'StrMPoleSymplectic4Pass');

QT0201 =  quadrupole('Q', 0.126,  +1.e-10, 'StrMPoleSymplectic4Pass');

EdgeB1_l=multipole('EdgeB1_l',0,[0 0 0 0],b1_edge_mult_1,'ThinMPolePass');
EdgeB1_r=multipole('EdgeB1_r',0,[0 0 0 0],b1_edge_mult_2,'ThinMPolePass');
BM01 = sbend('BEND', 0.303, 30*pi/180, 8.75/2*pi/180, (30-8.75/2)*pi/180,0.0, 'BndMPoleSymplectic4Pass');

CORH = corrector('HCM', 0.0, [0 0],'CorrectorPass');
CORV = corrector('VCM', 0.0, [0 0],'CorrectorPass');
BPM = marker('BPM','IdentityPass');
FSOTR = marker('FSOTR','IdentityPass');
MatchP = marker('MatchP','IdentityPass');
VALVE = marker('VALVE','IdentityPass');
SCRH = marker('SCRH','IdentityPass');
FCUP = marker('FCUP','IdentityPass');

% Begin Lattice
%----- Linac to Booster Transfer Line: LTB --------------------------!;
LIDIA = [DLI1,FSOTR,DLI2, BPM, DLI3, D101, CORH, D102, CORV, D103, QT0101, D104, ...
       QT0102, D105, QT0103, D106, BPM, D107, FSOTR, D108, EdgeB1_l, BM01, EdgeB1_r, ...
       DIA1,DIA1,DIA1,DIA1, QT0201, DIA2,DIA2,DIA2, SCRH, DIA3,DIA3,DIA3, FSOTR, DIA4,DIA4,DIA4, FCUP];

% INIT_LTB : Beta0,
%                 betx = 5.00, alfx = -1.00,
%                 bety = 5.00, alfy = -1.00,
%                 dx   = 0.00, dpx  = 0.00;

buildlat(LIDIA);

% set all magnets to the same energy
THERING = setcellstruct(THERING,'Energy',1:length(THERING),GLOBVAL.E0);

%evalin('caller','global THERING FAMLIST GLOBVAL');

% atsummary;

if nargout
    varargout{1} = THERING;
end

% Compute total length and RF frequency
L0_tot=0;
for i=1:length(THERING)
    L0_tot=L0_tot+THERING{i}.Length;
end
fprintf('   Model LTB length is %.6f meters\n', L0_tot)
%fprintf('   Model RF frequency is %.6f MHz\n', HarmNumber*C0/L0_tot/1e6)

clear global FAMLIST
%clear global GLOBVAL when GWig... has been changed.

%evalin('caller','global THERING FAMLIST GLOBVAL');

end


function z=sbendC2(fname,L,A,A1,A2,polynomA,polynomB,method)
%BEND('FAMILYNAME',  Length[m], BendingAngle[rad], EntranceAngle[rad],ExitAngle[rad], polynomA,polynomB, 'METHOD')
%	creates a new family in the FAMLIST - a structure with fields
%		FamName        	family name
%		Length         	length of the arc for an on-energy particle [m]
%		BendingAngle		total bending angle [rad]
%		EntranceAngle		[rad] (0 - for sector bends)
%		ExitAngle			[rad] (0 - for sector bends)
%		ByError				error in the dipole field relative to the design value 
%		polynomA			skew multipole body values
%		polynomB			normal multipole body values
%		PassMethod        name of the function to use for tracking
% returns assigned address in the FAMLIST that is uniquely identifies
% the family

MaxOrder=max([length(polynomA) length(polynomB)]-1);
polynomA_copy=zeros(1,MaxOrder+1);
polynomB_copy=zeros(1,MaxOrder+1);
polynomA_copy(1:length(polynomA))=polynomA;
polynomB_copy(1:length(polynomB))=polynomB;

ElemData.FamName = fname;  % add check for identical family names
ElemData.Length			= L;
ElemData.MaxOrder			= MaxOrder;
ElemData.NumIntSteps 	= 10;
ElemData.BendingAngle  	= A;
ElemData.EntranceAngle 	= A1;
ElemData.ExitAngle     	= A2;
ElemData.ByError     	= 0;
ElemData.K      			= polynomB_copy(2);

ElemData.R1 = diag(ones(6,1));
ElemData.R2 = diag(ones(6,1));
ElemData.T1 = zeros(1,6);
ElemData.T2 = zeros(1,6);

ElemData.PolynomA			= polynomA_copy;	 
ElemData.PolynomB			= polynomB_copy; 

ElemData.PassMethod 		= method;

global FAMLIST
z = length(FAMLIST)+1; % number of declare families including this one
FAMLIST{z}.FamName = fname;
FAMLIST{z}.NumKids = 0;
FAMLIST{z}.KidsList= [];
FAMLIST{z}.ElemData= ElemData;

end
