function lidiainit(OperationalMode)
% Initialize parameters for ALBA LTB control in MATLAB

% Created by G.Benedetti - 18 August 2008
% Modified by GB - 13 July 2009

% Tango 'State' codes
% 0 ON
% 1 OFF
% 2 CLOSE
% 3 OPEN
% 4 INSERT
% 5 EXTRACT
% 6 MOVING
% 7 STANDBY
% 8 FAULT
% 9 INIT
% 10 RUNNING
% 11 ALARM
% 12 DISABLE
% 13 UNKNOWN


if nargin < 1
    OperationalMode = 1;
end

%==============================
% load AcceleratorData structure
%==============================

%Mode             = 'Simulator';
Mode             = 'Online';
setad([]);       %clear AcceleratorData memory

%%%%%%%%%%%%%%%%%%%%
% ACCELERATOR OBJECT
%%%%%%%%%%%%%%%%%%%%

setao([]);   %clear previous AcceleratorObjects

%=============================================
%% BPMx data: status field designates if BPM in use
%=============================================
iFam = 'BPMx';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'BPM';
AO.(iFam).MemberOf                 = {'PlotFamily'; 'HBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';

bpm={
     1,	'LI/DI/BPM-01', 1, [1  1], '01BPM01'
     2,	'LT/DI/BPM-01', 1, [1  2], '01BPM02'
    };

% Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/XPosSA');
    AO.(iFam).Status(ii,:)             = bpm{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';

%=============================================
%% BPMy data: status field designates if BPM in use
%=============================================

iFam = 'BPMy';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'BPM';
AO.(iFam).MemberOf                 = {'PlotFamily'; 'VBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';

bpm={
     1,	'LI/DI/BPM-01', 1, [1  1], '01BPM01'
     2,	'LT/DI/BPM-01', 1, [1  2], '01BPM02'
    };

%Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/ZPosSA');
    AO.(iFam).Status(ii,:)             = bpm{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';

%===========================================================
%% HCM
%===========================================================

iFam ='HCM';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).MemberOf                 = {'COR'; 'MCOR'; 'HCM'; 'Magnet'; 'CORH'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;

cor={ 1, 'LT01/PC/CORH-01', 1, [1  1], '01CORH01'};

%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients('HCM');

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-2 2]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-4;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%% CORV

iFam ='VCM';

AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'COR';
AO.(iFam).MemberOf                 = {'COR'; 'MCOR'; 'VCM'; 'Magnet'; 'CORV'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;

cor={1,	'LT01/PC/CORV-01', 1, [1  1], '01CORV01'};

% Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients('VCM');

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-2 2]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-4;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%=============================
%        MAIN MAGNETS
%=============================

%===========
% Dipole data
%===========
%% *** BEND ***
iFam = 'BEND';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'BEND'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('BEND');
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';

AO.(iFam).DeviceName(:,:) = {'lt01/pc/bend-01'};
AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/Current');

AO.(iFam).DeviceList(:,:) = [1 1];
AO.(iFam).ElementList(:,:)= 1;
AO.(iFam).Status          = 1;

val = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
% AO.(iFam).Monitor.Range(:,:) = [0 200]; % 148 A for 0.170 T @ 0.1 GeV

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/CurrentSetpoint');
AO.(iFam).Setpoint.Range(:,:) = [0 200]; % 148 A for 0.170 T @ 0.1 GeV
AO.(iFam).Setpoint.Tolerance(:,:) = 0.001;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.5;

%============
% QUADRUPOLES
%============
%% *** QUADS ***
iFam = 'Q';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet';};

HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';

AO.(iFam).Setpoint.MemberOf          = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode              = Mode;
AO.(iFam).Setpoint.DataType          = 'Scalar';
AO.(iFam).Setpoint.Units             = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn     = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn     = @k2amp;
AO.(iFam).Setpoint.HWUnits           = 'A';
AO.(iFam).Setpoint.PhysicsUnits      = 'meter^-2';
%A0.(iFam).DataType = @single;

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
     1,	'LT01/PC/Q-01', 1, [1 1], '01Q01'
     2,	'LT01/PC/Q-02', 1, [1 2], '01Q02'
     3,	'LT01/PC/Q-03', 1, [1 3], '01Q03'
     4,	'LT02/PC/Q-01', 1, [1 4], '02Q01'
    };



for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
%     AO.(iFam).Monitor.Range(ii,:)        = [-15 15]; 
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-15 15]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 0.0005;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.2;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%%%%%%%%%%%%%%%%%%
%%% Diagnostics
%%%%%%%%%%%%%%%%%%

% Beam Charge Monitors - 
%% *** BCMs ***

iFam = 'BCM';

AO.(iFam).FamilyName             = 'BCM';
AO.(iFam).MemberOf               = {'Plotfamiliy';'Diagnostics'; 'Diag'; 'MC'; 'Archivable'; 'BCM'};
AO.(iFam).Mode                   = Mode;
AO.(iFam).Monitor.DataType       = 'Scalar';
AO.(iFam).Monitor.Units          = 'Hardware';
AO.(iFam).Monitor.Mode            = Mode; % Zeus added this at 7/15/2009
AO.(iFam).Monitor.HWUnits        = 'nC';
AO.(iFam).Monitor.PhysicsUnits   = 'nC';

bcm={
     1, 'li/di/bcm-01'  , 1, [1 1], 'BCM1'
     2, 'LT02/DI/bcm-01', 1, [1 2], 'BCM2'
     };
 
for ii=1:size(bcm,1)
    AO.(iFam).ElementList(ii,:)        = bcm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bcm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bcm(ii,2), '/Charge');
    AO.(iFam).Status(ii,:)             = bcm{ii,3};
    AO.(iFam).DeviceList(ii,:)         = bcm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bcm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1;
end


% AO.(iFam).DeviceName             = {'LI/di/BCM-01'; 'LT02/di/BCM-01'};
% AO.(iFam).CommonNames            = ['bcm1';'bcm2'];
% AO.(iFam).DeviceList(:,:)        = [1 1; 1 2];
% AO.(iFam).ElementList            = [1 2]';
% AO.(iFam).Status                 = [1 1]';
% % AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
% AO.(iFam).Monitor.TangoNames     = [strcat(AO.(iFam).DeviceName(1,:), '/Charge'); ...
%                                     strcat(AO.(iFam).DeviceName(2,:), '/Charge')];
% AO.(iFam).Monitor.DataType       = 'Scalar';
% AO.(iFam).Monitor.Units          = 'Hardware';
% AO.(iFam).Monitor.HWUnits        = 'nC';
% AO.(iFam).Monitor.PhysicsUnits   = 'nC';
% AO.(iFam).Monitor.HW2PhysicsParams = 1.0;
% AO.(iFam).Monitor.Physics2HWParams = 1.0;

% Horizontal scraper - 
%% *** SCRH_LEFT ***

iFam = 'SCRH_LEFT';

AO.(iFam).FamilyName             = iFam;
AO.(iFam).FamilyType               = 'SCRH';
AO.(iFam).MemberOf               = {'Diag';  'Diagnostics'; 'SCRH';};
AO.(iFam).Mode                   = Mode;
%AO.(iFam).DeviceName             = {'pm/lt02_pmenchl_ctrl/1'};
%AO.(iFam).DeviceName             = {'motor/ltb_ipapctrl/1'};
AO.(iFam).DeviceName             = {'motor/lt_ipapscrapers_ctrl/1'};
AO.(iFam).CommonNames            = 'SCRH_LEFT';
AO.(iFam).DeviceList(:,:)        = [1 1];
AO.(iFam).ElementList            = 1;
AO.(iFam).Status                 = 1;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.TangoNames     = strcat(AO.(iFam).DeviceName(:,:), '/Position');
AO.(iFam).Monitor.Mode           = Mode;
AO.(iFam).Monitor.DataType       = 'Scalar';
AO.(iFam).Monitor.Units          = 'Hardware';
AO.(iFam).Monitor.HWUnits        = 'mm';
AO.(iFam).Monitor.PhysicsUnits   = 'mm';
AO.(iFam).Monitor.HW2PhysicsParams = 1.0;
AO.(iFam).Monitor.Physics2HWParams = 1.0;
AO.(iFam).Monitor.Range = [-11.7 17.8];

AO.(iFam).Setpoint = AO.(iFam).Monitor;

%% *** SCRH_RIGHT ***

iFam = 'SCRH_RIGHT';

AO.(iFam).FamilyName             = iFam;
AO.(iFam).FamilyType               = 'SCRH';
AO.(iFam).MemberOf               = {'Diag';  'Diagnostics'; 'SCRH';};
AO.(iFam).Mode                   = Mode;
%AO.(iFam).DeviceName             = {'pm/lt02_pmenchr_ctrl/1'};
%AO.(iFam).DeviceName             = {'motor/ltb_ipapctrl/2'};
AO.(iFam).DeviceName             = {'motor/lt_ipapscrapers_ctrl/2'};
AO.(iFam).CommonNames            = 'SCRH_RIGHT';
AO.(iFam).DeviceList(:,:)        = [1 1];
AO.(iFam).ElementList            = 1;
AO.(iFam).Status                 = 1;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.TangoNames     = strcat(AO.(iFam).DeviceName(:,:), '/Position');
AO.(iFam).Monitor.Mode           = Mode;
AO.(iFam).Monitor.DataType       = 'Scalar';
AO.(iFam).Monitor.Units          = 'Hardware';
AO.(iFam).Monitor.HWUnits        = 'mm';
AO.(iFam).Monitor.PhysicsUnits   = 'mm';
AO.(iFam).Monitor.HW2PhysicsParams = 1.0;
AO.(iFam).Monitor.Physics2HWParams = 1.0;
AO.(iFam).Monitor.Range = [-12.5 18.9];

AO.(iFam).Setpoint = AO.(iFam).Monitor;

%% *** SCRAPER GAP ***
iFam = 'SCRH_GAP';

AO.(iFam).FamilyName             = iFam;
AO.(iFam).FamilyType               = 'SCRH';
AO.(iFam).MemberOf               = {'Diag';  'Diagnostics'; 'SCRH';};
AO.(iFam).Mode                   = Mode;
%AO.(iFam).DeviceName             = {'pm/lt02_hslit_ctrl/1'};
%AO.(iFam).DeviceName             = {'motor/ltb_ipapctrl/1'};
AO.(iFam).DeviceName             = {'pm/lt02_hslit_ctrl/1'};
AO.(iFam).CommonNames            = 'SCRH_GAP';
AO.(iFam).DeviceList(:,:)        = [1 1];
AO.(iFam).ElementList            = 1;
AO.(iFam).Status                 = 1;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.TangoNames     = strcat(AO.(iFam).DeviceName(:,:), '/Position');
AO.(iFam).Monitor.Mode           = Mode;
AO.(iFam).Monitor.DataType       = 'Scalar';
AO.(iFam).Monitor.Units          = 'Hardware';
AO.(iFam).Monitor.HWUnits        = 'mm';
AO.(iFam).Monitor.PhysicsUnits   = 'mm';
AO.(iFam).Monitor.HW2PhysicsParams = 1.0;
AO.(iFam).Monitor.Physics2HWParams = 1.0;
AO.(iFam).Monitor.Range = [0 34];

AO.(iFam).Setpoint = AO.(iFam).Monitor;

%% *** SCRAPER OFFSET ***
iFam = 'SCRH_OFFSET';

AO.(iFam).FamilyName             = iFam;
AO.(iFam).FamilyType               = 'SCRH';
AO.(iFam).MemberOf               = {'Diag';  'Diagnostics'; 'SCRH';};
AO.(iFam).Mode                   = Mode;
%AO.(iFam).DeviceName             = {'pm/lt02_hslit_ctrl/2'};
AO.(iFam).DeviceName             = {'pm/lt02_hslit_ctrl/2'};
%AO.(iFam).DeviceName             = {'motor/ltb_ipapctrl/1'};
AO.(iFam).CommonNames            = 'SCRH_OFFSET';
AO.(iFam).DeviceList(:,:)        = [1 1];
AO.(iFam).ElementList            = 1;
AO.(iFam).Status                 = 1;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.TangoNames     = strcat(AO.(iFam).DeviceName(:,:), '/Position');
AO.(iFam).Monitor.Mode           = Mode;
AO.(iFam).Monitor.DataType       = 'Scalar';
AO.(iFam).Monitor.Units          = 'Hardware';
AO.(iFam).Monitor.HWUnits        = 'mm';
AO.(iFam).Monitor.PhysicsUnits   = 'mm';
AO.(iFam).Monitor.HW2PhysicsParams = 1.0;
AO.(iFam).Monitor.Physics2HWParams = 1.0;
AO.(iFam).Monitor.Range = [-15 15];

AO.(iFam).Setpoint = AO.(iFam).Monitor;
%%
% The operational mode sets the path, filenames, and other important parameters
% Run setoperationalmode after most of the AO is built so that the Units and Mode fields
% can be set in setoperationalmode
setao(AO);
setoperationalmode(OperationalMode);
AO = getao;

%======================================================================
%% Append Accelerator Toolbox information
%======================================================================
%======================================================================
% disp('** Initializing Accelerator Toolbox information');
% 
% AO = getao;
% 
% ATindx = atindex(THERING);  %structure with fields containing indices
% 
% s = findspos(THERING,1:length(THERING)+1)';
% 
% %% Horizontal BPMs
% iFam = ('BPMx');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BPM(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% Vertical BPMs
% iFam = ('BPMy');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BPM(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);  
% 
% %% HORIZONTAL CORRECTORS
% iFam = ('HCM');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% VERTICAL CORRECTORS
% iFam = ('VCM');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% BENDING magnets
% iFam = ('BEND');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% % One group of all dipoles
% %AO.(iFam).Position   = reshape(s(AO.(iFam).AT.ATIndex),1,40);
% %AT.(iFam).AT.ATParamGroup = mkparamgroup(THERING,AT.(iFam).AT.ATIndex,'K2');
% 
% %% QUADRUPOLES
% iFam = ('Q');
% AO.(iFam).AT.ATType  = 'QUAD';
% AO.(iFam).AT.ATIndex = eval(['ATindx.' iFam '(:)']);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %%
% 
% AO.HCM.Setpoint.DeltaRespMat = physics2hw('HCM','Setpoint', 1e-4, AO.HCM.DeviceList);
% AO.VCM.Setpoint.DeltaRespMat = physics2hw('VCM','Setpoint', 1e-4, AO.VCM.DeviceList);
% 
% AO.Q.Setpoint.DeltaRespMat  = physics2hw('Q', 'Setpoint', AO.Q.Setpoint.DeltaRespMat,  AO.Q.DeviceList);

setao(AO);

% % reference values
% global RefOptics;
% %disp '    Reference optics, tunes and AO stored in RefOptics'
% RefOptics.AO=getao();
% RefOptics.twiss=gettwiss();
% RefOptics.tune= gettune();
