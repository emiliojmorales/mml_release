function varargout = my_slider(varargin)
% MY_SLIDER M-file for my_slider.fig
%      MY_SLIDER, by itself, creates a new MY_SLIDER or raises the existing
%      singleton*.
%
%      H = MY_SLIDER returns the handle to a new MY_SLIDER or the handle to
%      the existing singleton*.
%
%      MY_SLIDER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MY_SLIDER.M with the given input arguments.
%
%      MY_SLIDER('Property','Value',...) creates a new MY_SLIDER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before my_slider_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to my_slider_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES
%
% To control a device:
%
% H= my_slider('UserData',{'DeviceName' DeviceNumber 'functionName'})
% eg: my_slider('UserData',{'Q' 1})
% DeviceNumber and functionName are optional
% functionName is a function to be fired after setting the new value 



% Edit the above text to modify the response to help my_slider

% Last Modified by GUIDE v2.5 01-Dec-2009 13:53:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @my_slider_OpeningFcn, ...
                   'gui_OutputFcn',  @my_slider_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before my_slider is made visible.
function my_slider_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to my_slider (see VARARGIN)

% Choose default command line output for my_slider
handles.output = hObject;
userdata=get(handles.output,'Userdata');
if length(userdata) < 1 || length(userdata) > 3,
    disp 'Usage my_slider('UserData',{'DeviceName' DeviceNumver 'FunctionName'})'
    close(handles.output);
    return
end
handles.device_name=cell2mat(userdata(1));
handles.device_number=1;
handles.function='';
if length(userdata)>1,
    handles.device_number=cell2mat(userdata(2));
end
if length(userdata)>2,
    handles.function=cell2mat(userdata(3));
end

set(handles.output, 'Name', cell2mat(dev2tango(handles.device_name, handles.device_number)));
set(handles.SetpointFamily,'ToolTip', cell2mat(dev2tango(handles.device_name, handles.device_number)));
set(handles.SetpointFamily, 'String', [handles.device_name '/' num2str(handles.device_number)]);
handles.family2datastruct_= family2datastruct(handles.device_name, handles.device_number);
familydata=getfamilydata(handles.device_name);
handles.min=familydata.Setpoint.Range(handles.device_number,1);
handles.max=familydata.Setpoint.Range(handles.device_number,2);
handles.range=handles.max-handles.min;
handles.value=getsp(handles.device_name, handles.device_number,'Hardware');
handles.res=abs(handles.value/handles.range)*1e-4;
handles.valueRef=handles.value;
unit=familydata.Monitor.HWUnits;
set(handles.SetpointMax, 'String',  [num2str(handles.max)]);
set(handles.SetpointSlider,'Max', handles.max);
set(handles.SetpointMin, 'String', [num2str(handles.min)]);
set(handles.SetpointSlider,'Min', handles.min);
set(handles.SetpointSlider,'SliderStep',[handles.res 10*handles.res]);
set(handles.SetpointSlider,'Value', handles.valueRef);
set(handles.SetpointStepSize,'String', num2str(abs(handles.value/1e4),'%f'));
set(handles.SetpointPushButton,'String', [num2str(handles.valueRef,'%f') '  ' unit]);
set(handles.SetpointEditBox,'String', num2str(handles.valueRef,'%f'));
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes my_slider wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = my_slider_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
try
    varargout{1} = handles.output;
end


% --- Executes on slider movement.
function SetpointSlider_Callback(hObject, eventdata, handles)
% hObject    handle to SetpointSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.SetpointEditBox,'String',num2str(get(hObject,'Value'),'%f'));
handles.value=get(hObject,'Value');
guidata(hObject, handles);
SetthePV(handles);

% --- Executes during object creation, after setting all properties.
function SetpointSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SetpointSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function SetpointEditBox_Callback(hObject, eventdata, handles)
% hObject    handle to SetpointEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SetpointEditBox as text
%        str2double(get(hObject,'String')) returns contents of SetpointEditBox as a double
set(handles.SetpointSlider,'Value',str2double(get(hObject,'String')) );
handles.value=str2double(get(hObject,'String'));
guidata(hObject, handles);
SetthePV(handles);

% --- Executes during object creation, after setting all properties.
function SetpointEditBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SetpointEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SetpointMax_Callback(hObject, eventdata, handles)
% hObject    handle to SetpointMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SetpointMax as text
%        str2double(get(hObject,'String')) returns contents of SetpointMax as a double
handles.max=str2num(get(hObject,'String'));
handles.range=handles.max-handles.min;
handles.res=abs(str2double(get(handles.SetpointStepSize','String'))/handles.range);
set(handles.SetpointSlider,'Max',handles.max);
set(handles.SetpointSlider,'SliderStep',[handles.res handles.res*10]);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function SetpointMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SetpointMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SetpointMin_5Callback(hObject, eventdata, handles)
% hObject    handle to SetpointMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SetpointMin as text
%        str2double(get(hObject,'String')) returns contents of SetpointMin as a double
handles.min=str2num(get(hObject,'String'));
handles.range=handles.max-handles.min;
handles.res=abs(str2double(get(handles.SetpointStepSize','String'))/handles.range);
set(handles.SetpointSlider,'Min',handles.min);
set(handles.SetpointSlider,'SliderStep',[handles.res handles.res*10]);

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function SetpointMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SetpointMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SetpointStepSize_Callback(hObject, eventdata, handles)
% hObject    handle to SetpointStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SetpointStepSize as text
%        str2double(get(hObject,'String')) returns contents of SetpointStepSize as a double
handles.res=abs(str2double(get(hObject,'String'))/handles.range);
set(handles.SetpointSlider,'SliderStep',[handles.res handles.res*10]);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function SetpointStepSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SetpointStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in SetpointPushButton.
function SetpointPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to SetpointPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.SetpointSlider,'Value',handles.valueRef);
handles.value=handles.valueRef;
set(handles.SetpointEditBox,'String',num2str(handles.valueRef,'%f'));
guidata(hObject, handles);
SetthePV(handles);

function SetthePV(handles)
    setsp(handles.device_name,  handles.value, handles.device_number);
    if length(handles.function)>1,
        handles.function
    end
