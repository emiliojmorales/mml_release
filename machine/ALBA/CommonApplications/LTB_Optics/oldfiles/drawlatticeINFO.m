function h = drawlatticeINFO (Offset, Scaling, hAxes, Ldraw)
% Extension of drawlattice to add a context menu with information
% when right clicking over an element.
% The input parameters are the same than  drawlattice.
% The output parameters are the same than  drawlattice.

global THERING;
if nargin < 1
    Offset = 0;
end
Offset = Offset(1);
if nargin < 2
    Scaling = 1;
end
Scaling = Scaling(1);

if nargin < 3
    hAxes = gca;
end



SPositions = findspos(THERING, 1:length(THERING)+1);
L = SPositions(end);

if nargin < 4
    Ldraw = L;
end

ati=atindex(THERING);
h=drawlattice(Offset, Scaling, hAxes, Ldraw);
% this is the tricky part, find a parent of the figure type
thetype='';
theparent=hAxes;
while ~strcmp('figure', thetype),
    theparent=get(theparent,'Parent');
    thetype=get(theparent,'Type');
end
for loop=1:length(h),
    hcmenu(loop) = uicontextmenu('Parent', theparent);
    atsubmen  = uimenu(hcmenu(loop), 'Label', '<HTML><U> AT Info: </u></html>');
    MMLsubmen = uimenu(hcmenu(loop), 'Label', '<HTML><U> MML Info: </u></html>');
    item1_at(loop) = uimenu(atsubmen, 'Label', ['<HTML><B>Family : </b>' THERING{get(h(loop),'Userdata')}.FamName '</html>'] );
    item2_at(loop) = uimenu(atsubmen, 'Label', ['<HTML><B>ATindex: </b>' num2str(get(h(loop),'Userdata')) '</html>']);
    [a, b]=atindex2family(get(h(loop),'Userdata'));
    if (length(a) > 0 ) && numel(b) > 1,
        item1_mml(loop) = uimenu(MMLsubmen, 'Label', ['<HTML><B>MML family: </b>' a '</html>']);
        item2_mml(loop) = uimenu(MMLsubmen, 'Label', ['<HTML><B>Device: </b>' num2str(b(2)) '</html>']);
        tname=cell2mat(family2tangodev(a,b));
        item3_mml(loop) = uimenu(MMLsubmen,  'Label', ['<HTML><B>Tango channel: </b>' tname(1,:) '</html>']);
%         item9(loop) = uimenu(hcmenu(loop), 'Label',tname(1,:) 
    editItem(loop) = uimenu(hcmenu(loop),  ...
            'Label', '<HTML><B><FONT color="red">Edit Element</Font></b></html>',...
            'Callback',{@editElement a num2str(b(2))});
         end
     index(loop)=get(h(loop),'Userdata');
    set(h(loop),'UIContextMenu', hcmenu(loop));
end

function editElement(hObject, eventdata, device, devicenumber)
    h=my_slider('Userdata',{device str2num(devicenumber)},'Tag','DeleteOnClose');
