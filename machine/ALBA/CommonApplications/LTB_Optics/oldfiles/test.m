function test

f0=figure
% f1 = jcontrol(gcf,'javax.swing.JPanel',...
%     'Units','normalized','Position',[0. 0. 1 1]);
% set(f1,'ComponentResizedCallback', @(handle,evt) disp(evt.getSource.getSize));
f1= uitabpanel(...
  'Parent',f0,...
  'TabPosition','righttop',...
  'Position',[0.3,0.5,0.7,0.5],...
  'Margins',{[0,-1,1,0],'pixels'},...
  'PanelBorderType','line',...
  'Title',{'Optics','Beam Size','Features','Todo'},'CreateFcn',@CreateTab);


function CreateTab(htab,evdt,hpanel,hstatus)
global THERING;
tw0=twissring(THERING, 0,1,'chrom');
tw0.beta=[5 5];
tw0.alpha=[0 0];
twiss_line=twissline(THERING, 0, tw0, 1:length(THERING)+1);
s=findspos(THERING, 1:length(THERING)+1);
BETA = cat(1,twiss_line.beta);
bx=BETA(:,1)
by=BETA(:,2)
opticaxe=axes('Parent', hpanel(1),'Units','normalized',...
     'Position',[0.1 0.1 0.8 0.8]);
betax   =	line('parent',opticaxe,'XData',s,'YData',bx,'Color','r');