function ltb_optic()

% close already running versions
oldfig = findobj(allchild(0),'tag','ltb_optic'); 
if ~isempty(oldfig), delete(oldfig); end
%create the figure, and the panels
f = figure('menubar','none', 'Position', [0 0 1000 600],'Resize', 'Off','Tag','ltb_optic',...
    'Interruptible', 'on', ...   
    'HandleVisibility','off', ...
    'MenuBar','none', ...
    'Name', 'Transfer line optics', ...
    'NumberTitle','off');
handles = guihandles(f);
global THERING;
handles.spos=findspos(THERING, 1:length(THERING)+1);
mainpanel = uipanel('Parent', f);
inputpanel = uipanel('Parent', mainpanel,'Position',[0 0 0.12 0.95],'Title','Input Parameters');
quadpanel = uipanel('Parent', mainpanel,'Position',[0.12 0 0.23 0.95],'Title', 'Quad Values');
betapanel = uipanel('Parent', mainpanel,'Position',[0.35 0 0.65 0.95],'Title', 'Model Optics');
% inputpanel
y0=500;
dy=7;
stepy=35;
handles.betaxlabel=uibutton('Style','text', 'Parent', inputpanel,'String', '\beta_x [m]=','Position', [25 y0 40 20], ...
    'HorizontalAlignment', 'Right');
handles.betaxinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '5'           ,'Position', [70 y0+dy 40 20], ...
    'BackgroundColor','w');
y0=y0-stepy;
handles.alphaxlabel=uibutton('Style','text', 'Parent', inputpanel,'String', '\alpha_x =','Position', [25 y0 40 20], 'HorizontalAlignment', 'Right');
handles.alphaxinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '0'           ,'Position', [70 y0+dy 40 20], ...
    'BackgroundColor','w' );
y0=y0-stepy;
handles.betaylabel=uibutton('Style','text', 'Parent', inputpanel,'String', '\beta_y [m]=','Position', [25 y0 40 20], 'HorizontalAlignment', 'Right');
handles.betayinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '5'           ,'Position', [70 y0+dy 40 20] , ...
    'BackgroundColor','w');
y0=y0-stepy;
handles.alphaylabel=uibutton('Style','text', 'Parent', inputpanel,'String', '\alpha_y =','Position', [25 y0 40 20], 'HorizontalAlignment', 'Right');
handles.alphayinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '0'           ,'Position', [70 y0+dy 40 20], ...
    'BackgroundColor','w' );
y0=y0-stepy;
handles.dxlabel=uibutton('Style','text', 'Parent', inputpanel,'String', 'D_x [m]=','Position', [25 y0 40 20], 'HorizontalAlignment', 'Right');
handles.dxinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '0'           ,'Position', [70 y0+dy 40 20], ...
    'BackgroundColor','w' );
y0=y0-stepy;
handles.dpxlabel=uibutton('Style','text', 'Parent', inputpanel,'String', 'Dp_x =','Position', [25 y0 40 20], 'HorizontalAlignment', 'Right');
handles.dpxinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '0'           ,'Position', [70 y0+dy 40 20], ...
    'BackgroundColor','w' );
y0=y0-stepy;
handles.energylabel=uibutton('Style','text', 'Parent', inputpanel,'String', 'E [MeV]=','Position', [25 y0 40 20], 'HorizontalAlignment', 'Right');
handles.energyinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '100'           ,'Position', [70 y0+dy 40 20], ...
    'BackgroundColor','w' );
handles.restorebutton=uibutton('Style','pushbutton', 'Parent', inputpanel,'String', 'Restore\newline values','Position', [15 100 100 40], ...
   'CallBack', @restorebutton_Callback );
% quad panel
handles.quadtable=uitable('Parent', quadpanel,'ColumnName',{'Name', 'Current'}, 'Position',[5 200 219 340] );
handles.getdatabutton=uibutton('Style','pushbutton', 'Parent', quadpanel,'String', 'Get data\newline from machine','Position', [15 100 100 40], ...
    'ForegroundColor','b', 'CallBack', @getdatabutton_Callback);
% beta panel
handles.opticaxe=axes('Parent', betapanel, 'Position', [0.1 0.3 0.8 0.68]);
handles.latticeaxe=axes('Parent', betapanel, 'Position', [0.1 0.05 0.8 0.2]);
handles.lattice=drawlattice(0,1, handles.latticeaxe);
handles.betax   =	line('parent',handles.opticaxe,'XData',handles.spos,'YData',0*handles.spos,'Color','r');
handles.betay   =	line('parent',handles.opticaxe,'XData',handles.spos,'YData',0.*handles.spos,'Color','b');
handles.dx      =	line('parent',handles.opticaxe,'XData',handles.spos,'YData',0.*handles.spos,'Color','g');

set(handles.latticeaxe, 'YTick', []);
xlim= get(handles.latticeaxe,'XLim');
set(handles.opticaxe,'XLim', xlim);
set(handles.opticaxe, 'XTick',[]);
guidata(f,handles) 
movegui(f, 'center');




function getdatabutton_Callback(hObject, eventdata, handles)
    handles=guidata(hObject);
    ConfigSetpoint = getmachineconfig('Online');
    global THERING;
    tw0=twissring(THERING, 0,1,'Chrom');
    tw0.beta=[str2num(get(handles.betaxinput,'String')) str2num(get(handles.betayinput,'String'))];
    tw0.alpha=[str2num(get(handles.alphaxinput,'String')) str2num(get(handles.alphayinput,'String'))];
    tw0.Dispersion(1)=str2num(get(handles.dxinput,'String'));
    tw0.Dispersion(2)=str2num(get(handles.dpxinput,'String'));
    twiss_line=twissline(THERING, 0, tw0, 1:length(THERING)+1,'Chrom');
    BETA = cat(1,twiss_line.beta);
    Dx =  cat(2,twiss_line.Dispersion)';
    set(handles.betax,'YData',BETA(:,1));
    set(handles.betay,'YData',BETA(:,2));
    set(handles.dx,'YData',10*Dx(:,1));
    

function restorebutton_Callback(hObject, eventdata, handles)
    disp 'hello2'
    handles=guidata(hObject);
    set(handles.betaxinput,'String',5);    
    set(handles.alphaxinput,'String',0);
    set(handles.betayinput,'String',5);
    set(handles.dxinput,'String',0);
    set(handles.dpxinput,'String',0);
    set(handles.energyinput,'String',0);
    
    
    
    