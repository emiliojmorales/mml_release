function [ cx cy ] = bump( x, px, y, py )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
Mxinv= [ 0.7605   -1.2327;  0.0396    1.7359] % A/mm o A/mrad
Myinv= [ 0.9971   -1.6164; -0.2936    2.2761] % A/mm o A/mrad
cx=Mxinv*[x;px];
cy=Myinv*[y;py];
stepsp('HCM',cx(1), 3,'Hardware'); 
stepsp('HCM',cx(2), 4,'Hardware'); 
stepsp('VCM',cy(1), 3,'Hardware'); 
stepsp('VCM',cy(2), 4,'Hardware'); 
end

