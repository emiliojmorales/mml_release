function getOrbitRamping_group(varargin)
%% Parameters of the calculation
% DDNSamples for the ACQs and DDBufferSize for non-ACQs
%TotalNumberTurns=180000;
% For checking the Intensity.
%NumberRampPoints=9;
% For checking the Intensity.
%TurnTest=500;
% intensity threshold for the chek
%Imin=0.0E7;
% step turns vector



if length(varargin)==4
    TotalNumberTurns = varargin{1};
    NumberRampPoints= varargin{2};
    Imin = varargin{3};
else
    TotalNumberTurns=[];
    NumberRampPoints=[];
    Imin=[];
end
if isempty(TotalNumberTurns)
    answer = inputdlg({'Range of turns ', 'Number of analisis points','Intensity threshold'},'BPMs during Ramp',1,{'180000','9','0.3e7'});
    if isempty(answer)
        return
    end
    TotalNumberTurns = str2num(answer{1});
    NumberRampPoints= str2num(answer{2});
    Imin = str2num(answer{3});
end
TurnsInitial=5:floor(TotalNumberTurns/NumberRampPoints):TotalNumberTurns;



%% Reading the BPMS


MC=getmachineconfig();
s=getspos('BPMx');
AO=getao();
bpm_names=AO.BPMx.DeviceName;
bpm_status=AO.BPMx.Status;
bpm_names=bpm_names(bpm_status==1);
bpm_names_noACQ=bpm_names;
n_bpm=length(bpm_names);
res=findstr('ACQ',bpm_names{1});
pos_=findstr('-',bpm_names{1});
if isempty(res)
    fprintf('The BPMs are not ACQ\n');
    for ii=1:n_bpm
        str_bpm=bpm_names_noACQ{ii};
        bpm_names{ii}=[str_bpm(1:pos_(1)) 'ACQ-' str_bpm((pos_(1)+1):end)];
    end
else
    fprintf('The BPMs are ACQ\n');
    for ii=1:n_bpm
        str_bpm=bpm_names{ii};
        bpm_names_noACQ{ii}=[str_bpm(1:pos_(1)) str_bpm((pos_(1)+5):end)];
    end
end

ny=floor(sqrt(NumberRampPoints));
nx=ceil(NumberRampPoints/ny);

M_x=zeros(n_bpm,NumberRampPoints);
M_y=zeros(n_bpm,NumberRampPoints);
M_I=zeros(n_bpm,NumberRampPoints);
BPMs_group_id = tango_group_create ('BPM');
for ii =1:n_bpm,
    tango_group_add (BPMs_group_id, bpm_names{ii});
end

try
    figure;
    for jj=1:NumberRampPoints
        ChangeBoosterBPMsDDoffset(TurnsInitial(jj));
        h(jj)=subplot(ny,nx,jj);
        pause(1);
        valid=false;
        while(valid==false)
            try
                XTango = tango_group_read_attributes(BPMs_group_id,{'XPosDDMean','ZPosDDMean','SumDDMean'},0);
                for ii=1:n_bpm
                    M_x(ii,jj)=XTango.dev_replies(ii).attr_values(1).value;
                    M_y(ii,jj)=XTango.dev_replies(ii).attr_values(2).value;
                    M_I(ii,jj)=XTango.dev_replies(ii).attr_values(3).value;
                end
                Itest=M_I(:,jj);
            catch ME1
                fprintf('Efective buffer size is: %d\n',numel(X));
                M_x=[];
                M_y=[];
                fprintf('Error: %s\n',ME1.message);
                Itest=-1;
                tango_group_kill(BPMs_group_id);
            end
            if any(Itest < Imin),
                fprintf('Itest=%f, Bad shot, BPMs:\n',mean(Itest));
                list=1:n_bpm;
                list=list(Itest < Imin);
                for ij=1:numel(list)
                    fprintf('%d ',list(ij));
                end
                fprintf('\n');
                pause(0.1);
            else
                valid=true;
                fprintf('mean(Itest)=%f, Adquired all BPM\n',mean(Itest));
            end
            
        end
        
        plot(s, M_x(:,jj),'r'); hold on; plot(s, M_y(:,jj),'b'); drawlattice(0,0.3); hold off
        xlabel('s [m]');
        ylabel('x & y [mm]');
        title(['Turn ' num2str(TurnsInitial(jj))]);
    end
    xli=max([max(max(abs(M_x))) max(max(abs(M_y)))]);
    linkaxes(h,'xy');
    ylim([-xli xli]);
    
    ChangeBoosterBPMsDDoffset(0);
catch ME
    ChangeBoosterBPMsDDoffset(0);
    fprintf('Error: %s',ME.message);
    tango_group_kill(BPMs_group_id);
    
end

tango_group_kill(BPMs_group_id);
end