function x=getx_s

global THERING;
ind=atindex(THERING);
indb=ind.BPM;
AO=getao();
bpm_status=AO.BPMx.Status;
orbit = findorbit(THERING,0,indb(bpm_status==1));
x=1000*orbit(1,:)';

end