function printtwiss(varargin)
thefile='twiss.txt';
if length(varargin) >= 1,
    thefile=cell2mat(varargin(1));
end
outf=fopen(thefile,'w'); 
tw=gettwiss;
fprintf(outf,'name          s        betax      alphax    Dx        Dpx       mux       betay     alphay    Dy        Dpy       muy\n');
for loop=1:length(tw.alfax),
fprintf(outf,'%s  %8.4f  %8.4f  %8.4f  %8.4f  %8.4f  %8.4f  %8.4f  %8.4f  %8.4f  %8.4f  %8.4f\n',...
    tw.name(loop,:),tw.s(loop),tw.betax(loop),tw.alfax(loop),tw.etax(loop),tw.etapx(loop),tw.phix(loop),...
    tw.betay(loop), tw.alfay(loop), tw.etay(loop), tw.etapy(loop), tw.phiy(loop));
end
fclose(outf);
