function [ QMS1 ] = BBABPM11
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%   Delay : Delay in seconds before reading the BPM;
%   QuadChange : Relative change of the quad ([%])
%   Correctorchange: 
BPM=[1 1]
Delay=1.5
QuadChange=2
OrbitChange=0.4
[QUADFamily, QUADDev, DelSpos, PhaseAdvanceX, PhaseAdvanceY] = bpm2quad('BPMx', BPM);
fprintf('   %d. BPM(%2d,%d)  %s(%2d,%d)  BPM-to-Quad Distance=%f meters\n', i, BPM, QUADFamily, QUADDev, DelSpos);
QMS=quadcenterinit2(QUADFamily,QUADDev,1, BPM);
QMS.OutlierFactor=10;
cwd=pwd;
QMS.DataDirectory='/data/MML/Release/measdata/ALBA/StorageRing/QMS/CheckBPM11';
QMS.ExtraDelay=Delay;
quadval=getpv(QUADFamily, QUADDev);
QMS.QuadDelta=quadval*QuadChange/100;
QMS.CorrDelta=QMS.CorrDelta*OrbitChange/0.5; % the default change in the orbit is 0.5. Set in quadcenterinit2
[QMS1 QMS2] = quadcenter(QMS,2);
end

