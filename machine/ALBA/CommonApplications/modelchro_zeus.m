function Chro = modelchro_zeus(varargin)
dd=0.0001;
de=[-2*dd -dd 0 dd 2*dd];
t1=thetune(-2*dd);
t2=thetune(-dd);
t3=thetune(0);
t4=thetune(dd);
t5=thetune(2*dd);
t=[t1;t2;t3;t4;t5];
cx=polyfit(de',t(:,1),2);
cy=polyfit(de',t(:,2),2);
Chro=[cx(2) cy(2)];
end