function SetPlotSize(h,x,y)
% SetPlotSize(h,x,y)
set(h, 'PaperUnits', 'centimeters');
set(h, 'PaperSize', [x  y]);
set(h, 'PaperPositionMode', 'manual');
set(h, 'PaperPosition', [0 0 x y]);
