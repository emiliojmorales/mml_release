%function [M_x M_y M_I]=getBPMs_freeze

%boosterinit;

MC=getmachineconfig();
Buffersize=16000;
AO=getao();
bpm_names=AO.BPMx.DeviceName;
bpm_status=AO.BPMx.Status;
bpm_names=bpm_names(bpm_status==1);
n_bpm=length(bpm_names);
%Xstr=tango_read_attribute2(bpm_names{1},'XPosDD');
%X=Xstr.value;
%nturns=length(X);
M_x=zeros(Buffersize,n_bpm);
M_y=zeros(Buffersize,n_bpm);
M_I=zeros(Buffersize,n_bpm);

buff_size=zeros(n_bpm,1);
for ii=1:n_bpm
    str_bpm=tango_read_attribute2(bpm_names{ii},'DDBufferSize');
    buff_size(ii)=str_bpm.value(1);
end


try
    
    for ii=1:n_bpm
        tango_write_attribute2(bpm_names{ii},'DDBufferSize', int32(Buffersize));
        tango_command_inout2(bpm_names{ii},'EnableDDBufferFreezing');
    end
    
    
    pause(1);
    for ii=1:n_bpm
        tango_command_inout2(bpm_names{ii},'UnfreezeDDBuffer');
    end
    pause(1);
    
    
    for ii=1:n_bpm
        bpm=bpm_names{ii};
        Xstr=tango_read_attribute2(bpm,'XPosDD');
        Ystr=tango_read_attribute2(bpm,'ZPosDD');
        Istr=tango_read_attribute2(bpm,'SumDD');
        X=Xstr.value;
        Y=Ystr.value;
        I=Istr.value;
        M_x(:,ii)=X';
        M_y(:,ii)=Y';
        M_I(:,ii)=I';
    end
    
    
    %% Back to previous BPM state
    
    for ii=1:n_bpm
        bpm=bpm_names{ii};
        tango_command_inout2(bpm,'DisableDDBufferFreezing');
        tango_write_attribute2(bpm,'DDBufferSize', int32(buff_size(ii)));
    end
    
    daydir=['/data/MML/WorkDirectory/zeus/BPMs/',datestr(now,'yyyymmdd')];
    mkdir(daydir);
    cd(daydir);
    name=[datestr(now) '.mat'];
    save(name,'M_x','M_y','M_I','MC');
catch ME
    for ii=1:n_bpm
        bpm=bpm_names{ii};
        tango_command_inout2(bpm,'DisableDDBufferFreezing');
        tango_write_attribute2(bpm,'DDBufferSize', int32(buff_size(ii)));
    end
end
%end