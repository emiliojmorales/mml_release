function Step_Transfer_AnglePosition(FamilyCor,elementsCor,Place,element,side,vect)
% exemple
% StepAnglePosition('HCM',[3 4],'SEINJ',1,'exit',[0 0.001])
% vect is in [m rad];

global THERING Transfer_RespMat;

if numel(elementsCor)~=2
    error('Two actuators must be used');
end
if numel(vect)~=2
    error('Wrong vector size');
end
ind=atindex(THERING);
if not(isfield(ind,Place))
    error('Wrong Place name');
else
    ind_place=ind.(Place);
end
if element>numel(ind_place)
    error('element number too big');
else
    ind_place=ind_place(element);
end
if strcmp(side,'exit')||strcmp(side,'Exit')||strcmp(side,'EXIT')
    ind_place=ind_place+1;
end
M=zeros(2);
if strcmp(FamilyCor,'HCM')
    Mt=Transfer_RespMat.XResp;
    M(:,:)=Mt([1 2],ind_place,elementsCor);
elseif strcmp(FamilyCor,'VCM')
    Mt=Transfer_RespMat.YResp;
    M(:,:)=Mt([3 4],ind_place,elementsCor);
else
    error('Wrong FamilyCor name');
end

change=M\[vect(1) vect(2)]';

steppv(FamilyCor,change(1),elementsCor(1));
steppv(FamilyCor,change(2),elementsCor(2));
end