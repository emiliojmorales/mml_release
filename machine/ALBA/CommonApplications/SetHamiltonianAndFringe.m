function varargout=SetHamiltonianAndFringe(varargin)
% Change all pass methods for full to aprox hamiltonian and the other way
% arround if indicated



numargin = size(varargin,2);
numargout = nargout;


global GLOBVAL;
dosomething=0;

flag='Approx';
if numargin>0
    flag=varargin{1};
end

% cases
full_flags={'Full','full','exact','Exact'};
full_flags_a={'Full_a','full_a','exact_a','Exact_a'};
full_flags_b={'Full_b','full_b','exact_b','Exact_b'};
full_flags_c={'Full_c','full_c','exact_c','Exact_c'};
app_flags={'Approx','approx','approximate','approximated','Approximate','Approximated'};
fringe_flags={'Fringe','fringe','FRINGE','Fring','Frin','frin','Frin'};
fringe_flags_a={'Fringe_a','fringe_a','FRINGE_a','Fring_a','Frin_a','frin_a','Frin_a'};
fringe_flags_b={'Fringe_b','fringe_b','FRINGE_b','Fring_b','Frin_b','frin_b','Frin_b'};

for ii=1:length(fringe_flags_a)
   if strcmp(fringe_flags_a{ii},flag) 
       Change2Fringe_a;
       dosomething=1;
       GLOBVAL.Hamiltonian = 'FringeFull';
   end
end

for ii=1:length(fringe_flags_b)
   if strcmp(fringe_flags_b{ii},flag) 
       Change2Fringe_b;
       dosomething=1;
       GLOBVAL.Hamiltonian = 'FringeFull';
   end
end

for ii=1:length(fringe_flags)
   if strcmp(fringe_flags{ii},flag) 
       Change2Fringe;
       dosomething=1;
       GLOBVAL.Hamiltonian = 'FringeFull';
   end
end

for ii=1:length(full_flags_a)
   if strcmp(full_flags_a{ii},flag) 
       Change2Full_a;
       dosomething=1;
       GLOBVAL.Hamiltonian = 'Full';
   end
end

for ii=1:length(full_flags_b)
   if strcmp(full_flags_b{ii},flag) 
       Change2Full_b;
       dosomething=1;
       GLOBVAL.Hamiltonian = 'Full';
   end
end

for ii=1:length(full_flags_c)
   if strcmp(full_flags_c{ii},flag) 
       Change2Full_c;
       dosomething=1;
       GLOBVAL.Hamiltonian = 'Full';
   end
end

for ii=1:length(full_flags)
   if strcmp(full_flags{ii},flag) 
       Change2Full;
       dosomething=1;
       GLOBVAL.Hamiltonian = 'Full';
   end
end

for ii=1:length(app_flags)
   if strcmp(app_flags{ii},flag) 
       Change2Approx;
       dosomething=1;
       GLOBVAL.Hamiltonian = 'Approx';
   end
end

if dosomething==0
    error('flag not apropiate ');
end

end

function Change2Fringe
global THERING;
full_pass_fringe={'StrMPoleSymplectic4FringeHFullPass','BndMPoleSymplectic4FringeHFullPass','DriftHFullPass','BndMPole_sliced_Symplectic4HFullPass','StrMPoleSymplectic4FringeHFullRadPass','BndMPoleSymplectic4FringeHFullRadPass','BndMPole_sliced_Symplectic4HFullRadPass'};
app_pass={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftPass','BndMPole_sliced_Symplectic4Pass','StrMPoleSymplectic4RadPass','BndMPoleSymplectic4RadPass','BndMPole_sliced_Symplectic4RadPass'};
full_pass={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass','BndMPole_sliced_Symplectic4HFullPass','StrMPoleSymplectic4HFullRadPass','BndMPoleSymplectic4HFullRadPass','BndMPole_sliced_Symplectic4HFullRadPass'};
for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(app_pass)
        if strcmp(app_pass{jj},pass)||strcmp(full_pass{jj},pass)
            THERING{ii}.PassMethod=full_pass_fringe{jj};
        end
    end
end
end

function Change2Fringe_a
global THERING;
full_pass_fringe_a={'StrMPoleSymplectic4FringeHFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass'};
app_pass={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftPass'};
full_pass={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass'};
full_pass_fringe={'StrMPoleSymplectic4FringeHFullPass','BndMPoleSymplectic4FringeHFullPass','DriftHFullPass'};
for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(app_pass)
        if strcmp(app_pass{jj},pass)||strcmp(full_pass{jj},pass)||strcmp(full_pass_fringe{jj},pass)
            THERING{ii}.PassMethod=full_pass_fringe_a{jj};
        end
    end
end
end

function Change2Fringe_b
global THERING;
full_pass_fringe_b={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4FringeHFullPass','DriftHFullPass'};
app_pass={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftPass'};
full_pass={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass'};
full_pass_fringe={'StrMPoleSymplectic4FringeHFullPass','BndMPoleSymplectic4FringeHFullPass','DriftHFullPass'};
for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(app_pass)
        if strcmp(app_pass{jj},pass)||strcmp(full_pass{jj},pass)||strcmp(full_pass_fringe{jj},pass)
            THERING{ii}.PassMethod=full_pass_fringe_b{jj};
        end
    end
end
end

function Change2Full
global THERING;
full_pass_fringe={'StrMPoleSymplectic4FringeHFullPass','BndMPoleSymplectic4FringeHFullPass','DriftHFullPass','BndMPole_sliced_Symplectic4HFullPass','StrMPoleSymplectic4FringeHFullRadPass','BndMPoleSymplectic4FringeHFullRadPass','BndMPole_sliced_Symplectic4HFullRadPass'};
app_pass={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftPass','BndMPole_sliced_Symplectic4Pass','StrMPoleSymplectic4RadPass','BndMPoleSymplectic4RadPass','BndMPole_sliced_Symplectic4RadPass'};
full_pass={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass','BndMPole_sliced_Symplectic4HFullPass','StrMPoleSymplectic4HFullRadPass','BndMPoleSymplectic4HFullRadPass','BndMPole_sliced_Symplectic4HFullRadPass'};

for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(app_pass)
        if strcmp(app_pass{jj},pass)||strcmp(full_pass_fringe{jj},pass)
            THERING{ii}.PassMethod=full_pass{jj};
        end
    end
end
end

function Change2Full_a
global THERING;
full_pass_fringe={'StrMPoleSymplectic4FringeHFullPass','BndMPoleSymplectic4FringeHFullPass','DriftHFullPass'};
app_pass={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftPass'};
full_pass_a={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4Pass','DriftPass'};
full_pass={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass'};
for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(app_pass)
        if strcmp(app_pass{jj},pass)||strcmp(full_pass_fringe{jj},pass)||strcmp(full_pass{jj},pass)
            THERING{ii}.PassMethod=full_pass_a{jj};
        end
    end
end
end
function Change2Full_b
global THERING;
full_pass_fringe={'StrMPoleSymplectic4FringeHFullPass','BndMPoleSymplectic4FringeHFullPass','DriftHFullPass'};
app_pass={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftPass'};
full_pass_b={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4HFullPass','DriftPass'};
full_pass={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass'};
for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(app_pass)
        if strcmp(app_pass{jj},pass)||strcmp(full_pass_fringe{jj},pass)||strcmp(full_pass{jj},pass)
            THERING{ii}.PassMethod=full_pass_b{jj};
        end
    end
end
end
function Change2Full_c
global THERING;
full_pass_fringe={'StrMPoleSymplectic4FringeHFullPass','BndMPoleSymplectic4FringeHFullPass','DriftHFullPass'};
app_pass={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftPass'};
full_pass_c={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftHFullPass'};
full_pass={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass'};
for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(app_pass)
        if strcmp(app_pass{jj},pass)||strcmp(full_pass_fringe{jj},pass)||strcmp(full_pass{jj},pass)
            THERING{ii}.PassMethod=full_pass_c{jj};
        end
    end
end
end

function Change2Approx
global THERING;
full_pass_fringe={'StrMPoleSymplectic4FringeHFullPass','BndMPoleSymplectic4FringeHFullPass','DriftHFullPass','BndMPole_sliced_Symplectic4HFullPass','StrMPoleSymplectic4FringeHFullRadPass','BndMPoleSymplectic4FringeHFullRadPass','BndMPole_sliced_Symplectic4HFullRadPass'};
app_pass={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftPass','BndMPole_sliced_Symplectic4Pass','StrMPoleSymplectic4RadPass','BndMPoleSymplectic4RadPass','BndMPole_sliced_Symplectic4RadPass'};
full_pass={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass','BndMPole_sliced_Symplectic4HFullPass','StrMPoleSymplectic4HFullRadPass','BndMPoleSymplectic4HFullRadPass','BndMPole_sliced_Symplectic4HFullRadPass'};

for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(full_pass)
        if strcmp(full_pass{jj},pass)||strcmp(full_pass_fringe{jj},pass)
            THERING{ii}.PassMethod=app_pass{jj};
        end
    end
end
end