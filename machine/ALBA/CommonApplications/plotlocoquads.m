function plotlocoquads
% G.B. 2012
%% load loco file
DirStart = [getfamilydata('Directory', 'DataRoot'), 'LOCO', filesep];
[filename, LocoDirectory] = uigetfile('*.mat','Select a locodata file',DirStart);

load([LocoDirectory filename], 'FitParameters','RINGData');

iter=length(FitParameters);
%% get quad settings
Iquadval0=cell2mat(getpv(findmemberof('QUAD'),'Hardware'));

%% plot quad changes (A)
figure
for loop=1:14,
    quaindex=(loop*8-7:loop*8);
    dkFitRelative=-(FitParameters(iter).Values(quaindex)-FitParameters(1).Values(quaindex))./FitParameters(1).Values(quaindex);
    dCurrents=dkFitRelative.*Iquadval0(quaindex);
    stem(quaindex,dCurrents)
    xlim([0 113])
    % ylim([-1. 1.])
    hold all
end

set(gca,'XTick',0:10:112)
set(gca,'FontSize',12)
xlabel('quad number','FontSize',14)
ylabel('current change [A]','FontSize',14)
legend(findmemberof('QUAD'),'Location','EastOutside')
title(['LOCO iteration ',num2str(iter-1),': fit by quad power supplies'],'FontSize',12)
hold off

% %% plot quad changes (%)
% figure
% for loop=1:14,
%     quaindex=(loop*8-7:loop*8);
%     dkFitRelative=-(FitParameters(iter).Values(quaindex)-FitParameters(1).Values(quaindex))./FitParameters(1).Values(quaindex);
%     stem(quaindex,100*dkFitRelative)
%     xlim([0 113])
%     % ylim([-1. 1.])
%     hold all
% end
% 
% set(gca,'XTick',0:10:112)
% set(gca,'FontSize',12)
% xlabel('quad number','FontSize',14)
% ylabel('quad change [%]','FontSize',14)
% legend(findmemberof('QUAD'),'Location','EastOutside')
% title(['LOCO iteration ',num2str(iter-1),': fit by quad power supplies'],'FontSize',12)
% hold off

%% plot quad variations within each family (A)
figure
for loop=1:14,
    quaindex=(loop*8-7:loop*8);
    dkFitRelative=-(FitParameters(iter).Values(quaindex)-FitParameters(1).Values(quaindex))./FitParameters(1).Values(quaindex);
    dCurrents=dkFitRelative.*Iquadval0(quaindex);
    Iquad_mean=mean(Iquadval0(quaindex)); %Iquad_mean=mean(Iquadval0(quaindex));
    stem(quaindex,Iquadval0(quaindex)-Iquad_mean)
    xlim([0 113])
    
    hold all
end

set(gca,'XTick',0:10:112)
%set(gca,'YTick',-5:1:5)
set(gca,'FontSize',12)
xlabel('quad number','FontSize',14)
ylabel('quad variation w.r.t. family mean current [A]','FontSize',14)
legend(findmemberof('QUAD'),'Location','EastOutside')
title('quad families settings','FontSize',12)
hold off

%% plot quad changes vs. spos (A)
figure
for loop=1:14,
    quaindex=(loop*8-7:loop*8);
    dkFitRelative=-(FitParameters(iter).Values(quaindex)-FitParameters(1).Values(quaindex))./FitParameters(1).Values(quaindex);
    dCurrents=dkFitRelative.*Iquadval0(quaindex);
    
    for loop2=1:length(quaindex),
        iqua=FitParameters(iter).Params{quaindex(loop2)}.ElemIndex;
        squa(loop2)=findspos(RINGData.Lattice,iqua);
    end
    
    stem(squa,dCurrents)
     xlim([0 269])
    % ylim([-1. 1.])
    hold all
end

% set(gca,'XTick',0:10:112)
set(gca,'FontSize',12)
xlabel('quad position [m]','FontSize',14)
ylabel('current change [A]','FontSize',14)
legend(findmemberof('QUAD'),'Location','EastOutside')
title(['LOCO iteration ',num2str(iter-1),': fit by quad power supplies'],'FontSize',12)
hold off

%% plot quad variations within each family vs. spos (A)
figure
for loop=1:14,
    quaindex=(loop*8-7:loop*8);
    dkFitRelative=-(FitParameters(iter).Values(quaindex)-FitParameters(1).Values(quaindex))./FitParameters(1).Values(quaindex);
    dCurrents=dkFitRelative.*Iquadval0(quaindex);
    Iquad_mean=mean(Iquadval0(quaindex));
    
    for loop2=1:length(quaindex),
        iqua=FitParameters(iter).Params{quaindex(loop2)}.ElemIndex;
        squa(loop2)=findspos(RINGData.Lattice,iqua);
    end
    
    stem(squa,Iquadval0(quaindex)-Iquad_mean)
    xlim([0 269])
    
    hold all
end

%set(gca,'XTick',0:10:112)
%set(gca,'YTick',-5:1:5)
set(gca,'FontSize',12)
xlabel('quad position [m]','FontSize',14)
ylabel(' quad variation w.r.t. family mean current [A]','FontSize',14)
legend(findmemberof('QUAD'),'Location','EastOutside')
title('quad families settings','FontSize',12)
hold off


