function cycling_one(varargin)
%cycling_one(Family,Min_Current,Max_Current,elem)
% This has to be moved to the AO
%
% DEFAULTS:
% family='QH01';


if length(varargin) == 4
    family = varargin{1};
    Min_Current=varargin{2};
    Max_Current=varargin{3};
    elem=varargin{4};
else
    family = [];    
end

if isempty(family)
    answer = inputdlg({'Family to cycle','Min Current','Min Current','number of element'},'Cycling',1,{'QH01','20','200','1'});
    if isempty(answer)
        return;
    end
    family = answer{1};
    Min_Current = str2double(answer{2}); 
    Max_Current = str2double(answer{3});
    elem = str2double(answer{4});
end
 
time_wait=(Max_Current-Min_Current)/10+20;


time_wait_small=0.2;


q0=getpv(family,elem);
n_cycle=3;

for ii=1:(n_cycle-1)
    setpv(family,Max_Current,elem);
    pause(time_wait_small);
    setpv(family,Max_Current,elem);
    pause(time_wait_small);
    setpv(family,Max_Current,elem);
    % wait
    pause(time_wait);
    % 
    setpv(family,Min_Current,elem);
    pause(time_wait_small);
    setpv(family,Min_Current,elem);
    pause(time_wait_small);
    setpv(family,Min_Current,elem);
    % wait
    pause(time_wait);
end

setpv(family,Max_Current,elem);
pause(time_wait_small);
setpv(family,Max_Current,elem);
pause(time_wait_small);
setpv(family,Max_Current,elem);
% wait
pause(time_wait);
%
setpv(family,q0,elem);
pause(time_wait_small);
setpv(family,q0,elem);
pause(time_wait_small);
setpv(family,q0,elem);

end

