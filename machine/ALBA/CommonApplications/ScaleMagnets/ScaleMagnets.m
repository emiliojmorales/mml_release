function varargout = ScaleMagnets(varargin)
% SCALEMAGNETS M-file for ScaleMagnets.fig
%      SCALEMAGNETS, by itself, creates a new SCALEMAGNETS or raises the existing
%      singleton*.
%
%      H = SCALEMAGNETS returns the handle to a new SCALEMAGNETS or the handle to
%      the existing singleton*.
%
%      SCALEMAGNETS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SCALEMAGNETS.M with the given input arguments.
%
%      SCALEMAGNETS('Property','Value',...) creates a new SCALEMAGNETS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ScaleMagnets_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ScaleMagnets_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ScaleMagnets

% Last Modified by GUIDE v2.5 26-Jul-2010 12:55:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @ScaleMagnets_OpeningFcn, ...
    'gui_OutputFcn',  @ScaleMagnets_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ScaleMagnets is made visible.
function ScaleMagnets_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ScaleMagnets (see VARARGIN)

% Choose default command line output for ScaleMagnets
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ScaleMagnets wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ScaleMagnets_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function Scale_Callback(hObject, eventdata, handles)
% hObject    handle to Scale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
scale=get(handles.Scale,'Value')/100;
if (get(handles.ScaleBend,'Value')==1),
    setsp('BEND', scale*handles.valueRef.BEND);
end
if (get(handles.ScaleQH01,'Value')==1),
    setsp('QH01', scale*handles.valueRef.QH01);
end
if (get(handles.ScaleQV01,'Value')==1),
    setsp('QV01', scale*handles.valueRef.QV01);
end
if (get(handles.ScaleQH02,'Value')==1),
    setsp('QH02', scale*handles.valueRef.QH02);
end
if (get(handles.ScaleQV02,'Value')==1),
    setsp('QV02', scale*handles.valueRef.QV02);
end
if (get(handles.ScaleSH,'Value')==1),
    setsp('SH', scale*handles.valueRef.SH);
end
if (get(handles.ScaleSV,'Value')==1),
    setsp('SV',  scale*handles.valueRef.SV);
end
pause(1);
set(handles.ValueBend,'String', getsp('BEND'));
set(handles.ValueQH01,'String', getsp('QH01'));
set(handles.ValueQH02,'String', getsp('QH02'));
set(handles.ValueQV01,'String', getsp('QV01'));
set(handles.ValueQV02,'String', getsp('QV02'));
set(handles.ValueSH,'String', getsp('SH'));
set(handles.ValueSV,'String', getsp('SV'));
set(handles.ScaleField,'String',num2str(100*scale,8));

% --- Executes during object creation, after setting all properties.
function Scale_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Scale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in SetRef.
function SetRef_Callback(hObject, eventdata, handles)
% hObject    handle to SetRef (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% read the values and store the references
handles.valueRef.BEND=getpv('BEND', 'Current');
handles.valueRef.QH01=getpv('QH01', 'Current');
handles.valueRef.QV01=getpv('QV01', 'Current');
handles.valueRef.QH02=getpv('QH02', 'Current');
handles.valueRef.QV02=getpv('QV02', 'Current');
handles.valueRef.SH=getpv('SH', 'Current');
handles.valueRef.SV=getpv('SV', 'Current');
% update the displays
set(handles.RefBend,'String',num2str(handles.valueRef.BEND));
set(handles.RefQH01,'String',num2str(handles.valueRef.QH01));
set(handles.RefQH02,'String',num2str(handles.valueRef.QH02));
set(handles.RefQV01,'String',num2str(handles.valueRef.QV01));
set(handles.RefQV02,'String',num2str(handles.valueRef.QV02));
set(handles.RefSH,'String',num2str(handles.valueRef.SH));
set(handles.RefSV,'String',num2str(handles.valueRef.SV));

set(handles.ValueBend,'String',num2str(handles.valueRef.BEND));
set(handles.ValueQH01,'String',num2str(handles.valueRef.QH01));
set(handles.ValueQH02,'String',num2str(handles.valueRef.QH02));
set(handles.ValueQV01,'String',num2str(handles.valueRef.QV01));
set(handles.ValueQV02,'String',num2str(handles.valueRef.QV02));
set(handles.ValueSH,'String',num2str(handles.valueRef.SH));
set(handles.ValueSV,'String',num2str(handles.valueRef.SV));

set(handles.Scale,'Value',100.0);
set(handles.ScaleField,'String', '100.0');
guidata(hObject, handles);


function SetValue_Callback(hObject, eventdata, handles)
% hObject    handle to SetValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SetValue as text
%        str2double(get(hObject,'String')) returns contents of SetValue as a double
scale=str2double(get(handles.SetValue,'String'));
set(handles.Scale,'Value', scale);
Scale_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function SetValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SetValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ScaleKicker.
function ScaleKicker_Callback(hObject, eventdata, handles)
% hObject    handle to ScaleKicker (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ScaleKicker


% --- Executes on button press in ScaleSeptum.
function ScaleSeptum_Callback(hObject, eventdata, handles)
% hObject    handle to ScaleSeptum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ScaleSeptum


% --- Executes on button press in ScaleSV.
function ScaleSV_Callback(hObject, eventdata, handles)
% hObject    handle to ScaleSV (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ScaleSV


% --- Executes on button press in ScaleSH.
function ScaleSH_Callback(hObject, eventdata, handles)
% hObject    handle to ScaleSH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ScaleSH


% --- Executes on button press in ScaleQV02.
function ScaleQV02_Callback(hObject, eventdata, handles)
% hObject    handle to ScaleQV02 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ScaleQV02


% --- Executes on button press in ScaleQV01.
function ScaleQV01_Callback(hObject, eventdata, handles)
% hObject    handle to ScaleQV01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ScaleQV01


% --- Executes on button press in ScaleQH02.
function ScaleQH02_Callback(hObject, eventdata, handles)
% hObject    handle to ScaleQH02 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ScaleQH02


% --- Executes on button press in ScaleQH01.
function ScaleQH01_Callback(hObject, eventdata, handles)
% hObject    handle to ScaleQH01 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ScaleQH01


% --- Executes on button press in ScaleBend.
function ScaleBend_Callback(hObject, eventdata, handles)
% hObject    handle to ScaleBend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ScaleBend


% --- Executes on button press in Restore.
function Restore_Callback(hObject, eventdata, handles)
% hObject    handle to Restore (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setsp('BEND', handles.valueRef.BEND);
setsp('QH01', handles.valueRef.QH01);
setsp('QV01', handles.valueRef.QV01);
setsp('QH02', handles.valueRef.QH02);
setsp('QV02', handles.valueRef.QV02);
setsp('SH', handles.valueRef.SH);
setsp('SV', handles.valueRef.SV);
SetRef_Callback(hObject, eventdata, handles)
