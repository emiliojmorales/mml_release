function LinacEnergySpreadGUI

% Original MML script by G.Benedetti (bendscan.m)
% Created by Z.Martí - 14 July 2009
%  Energy spread measurement using two different methods


% Tango 'State' codes
% 0 ON
% 1 OFF
% 2 CLOSE
% 3 OPEN
% 4 INSERT
% 5 EXTRACT
% 6 MOVING
% 7 STANDBY
% 8 FAULT
% 9 INIT
% 10 RUNNING
% 11 ALARM
% 12 DISABLE
% 13 UNKNOWN
%
% Modified by Z.Martí - 12 Jenuary 2010

global mode_in daydir time_name;
format long;

RangeLeftJaw=[minpv('SCRH_LEFT') maxpv('SCRH_LEFT')];
RangeRightJaw=[minpv('SCRH_RIGHT') maxpv('SCRH_RIGHT')];
RangeBEND=[minpv('BEND')+1e-5 maxpv('BEND')];
%RangeBEND=[146 150];

%% Read field with default values
daydir=['/data/LTB/ENSPREAD/',datestr(now,'yyyymmdd')];
%disp('++++++++DEBUGMODE: Directori local a pluto....');
%daydir=['/home/zeus/ZeusApps/Studies/Linac/EnergySpread/',datestr(now,'yyyymmdd')];

mkdir(daydir);


filename='LinacEnergySpreadGUI_params.dat';
fid=fopen(filename,'r');

ParamsDataDefault=struct;
ParamsDataDefault.gap=5;
ParamsDataDefault.I0=150.0;
ParamsDataDefault.Minim_s=max([RangeLeftJaw(1)-ParamsDataDefault.gap/2 ParamsDataDefault.gap/2-RangeRightJaw(2)]);
ParamsDataDefault.Maxim_s=min([RangeLeftJaw(2)-ParamsDataDefault.gap/2 ParamsDataDefault.gap/2-RangeRightJaw(1)]);
ParamsDataDefault.Minim_d=RangeBEND(1);
ParamsDataDefault.Maxim_d=RangeBEND(2);
ParamsDataDefault.Minim=-10; % by default scraper
ParamsDataDefault.Maxim=10; % by default scraper
ParamsDataDefault.NumAveStep=3;
ParamsDataDefault.Num=10;
ParamsDataDefault.Dx=1013;
ParamsDataDefault.Bx=2;
ParamsDataDefault.Ex=150;
ParamsDataDefault.offset=0;
ParamsDataDefault.LinFr=1.00;
ParamsDataDefault.offset_BCM_1=+0.016490885458419;
ParamsDataDefault.offset_BCM_2=-0.007640080322791;

fid=-1;
if fid==-1
    ParamsData=ParamsDataDefault;
else
    fclose(fid);
    ParamsData=importdata(filename);
    fieldnamesDefault=fieldnames(ParamsDataDefault);
    fieldnamesfile=fieldnames(ParamsData);
    samestruct=0;
    sisa=size(fieldnamesfile);
    sisb=size(fieldnamesDefault);
    if sisa(1,1)==sisb(1,1)
        samestruct=1;
        for iis=1:sisa(1,1)
            if ~strcmp(fieldnamesfile(iis,:),fieldnamesfile(iis,:))
                samestruct=0;
            end
        end
    end
    if isempty(ParamsData)|| ~samestruct
        ParamsData=ParamsDataDefault;
    end
end

mode=getmode('SCRH_LEFT');

if strcmp(mode,'Simulator')||strcmp(mode,'simulator')||strcmp(mode,'sim')
    mode_in='Simulator';
    gap_meas=5;
    scr_l=17.8;
    scr_r=17.8;
    scra_l = scr_l-gap_meas/2;
    scra_r = gap_meas/2-scr_r;
    offset_meas=(scra_r+scra_l)/2;
    ParamsData.LinFr=10000.00;
    ParamsData.NumAveStep=1;
    
else
    mode_in='Online';
    gap_meas=getpv('SCRH_LEFT')+getpv('SCRH_RIGHT');
    scra_l = getpv('SCRH_LEFT')-gap_meas/2;
    scra_r = gap_meas/2-getpv('SCRH_RIGHT');
    offset_meas=(scra_r+scra_l)/2;
    
end

curbend = getpv('BEND');

ParamsData.gap_meas=gap_meas;
ParamsData.I0_meas=curbend;
ParamsData.offset_meas=offset_meas;
ParamsData.A=0.705787;
ParamsData.B=0.244807;
ParamsData.offset=offset_meas;

%% Construct the components for aquisition

% reference positioning values
Sizex1=80;
Sizex2=120;
dx=5;
x1=Sizex1+2*dx;
x2=2*x1;
x3=3*x1;
x4=4*x1;
x5=700;
x5b=Sizex1+x5-4*dx;
x6=1000;


z1=70;
z2=150;
z2b=220;
z2c=170;
z3=430;
z3b=450;
z4=480;
z5=500;
dz=10;
stepz=25;
Sizez1=15;
Sizez2=25;

% create the figure panel
f = figure('Visible','on','Position',[300,500,x6,z5]);
%set(f,'MenuBar','none');
fact=1.45;

%figures
hcheck_fig = uibuttongroup('visible','on','Position',[(x4+Sizex1-floor(fact*Sizex1))/x6,z4/z5,fact*Sizex1/x6,fact*Sizez1/z5]);
hcheck_fig1 = uicontrol('Style','radiobutton','String','Fig1','Position',[Sizex1*(fact-1)/2/2,Sizez1*(fact-1)/2,Sizex1/2,Sizez1],'parent',hcheck_fig,'HandleVisibility','on','ForegroundColor',[1 0 0]);
hcheck_fig2 = uicontrol('Style','radiobutton','String','Fig2','Position',[Sizex1*fact/2,Sizez1*(fact-1)/2,Sizex1/2,Sizez1],'parent',hcheck_fig,'HandleVisibility','on');
set(hcheck_fig,'SelectionChangeFcn',@selcFig);
set(hcheck_fig,'SelectedObject',hcheck_fig2); % default selection
set(hcheck_fig,'Visible','on');

%Main
htext_aquire  = uicontrol('Style','text','String','scan mode:','Position',[x2-Sizex2/2,z4,Sizex2,Sizez1]);
hcheck_mode = uibuttongroup('visible','off','Position',[(x2-floor(fact*Sizex1))/x6,z3b/z5,fact*2*Sizex1/x6,fact*Sizez1/z5]);
hcheck_modeDipole = uicontrol('Style','radiobutton','String','Dipole','Position',[Sizex1*(fact-1)/2,Sizez1*(fact-1)/2,Sizex1,Sizez1],'parent',hcheck_mode,'HandleVisibility','on');
hcheck_modeScraper = uicontrol('Style','radiobutton','String','Scraper','Position',[Sizex1*fact,Sizez1*(fact-1)/2,Sizex1,Sizez1],'parent',hcheck_mode,'HandleVisibility','on');
set(hcheck_mode,'SelectionChangeFcn',@selcbk);
set(hcheck_mode,'SelectedObject',hcheck_modeScraper); % default selection
flag_mode='Scraper';
set(hcheck_mode,'Visible','on');

%callback for the dipole/Scraper radio button

    function selcbk(source,eventdata)
        if strcmp(get(eventdata.NewValue,'String'),'Scraper')
            set(htext_Min,'String','Min Off. (mm):');
            set(htext_Max,'String','Max Off. (mm):');
            ParamsData.gap=str2double(get(hedit_gap,'String'));
            ParamsData.Minim_s=max([RangeLeftJaw(1)-ParamsData.gap/2 ParamsData.gap/2-RangeRightJaw(2)]);
            ParamsData.Maxim_s=min([RangeLeftJaw(2)-ParamsData.gap/2 ParamsData.gap/2-RangeRightJaw(1)]);
            set(hedit_Min,'String',num2str(ParamsData.Minim_s));
            set(hedit_Max,'String',num2str(ParamsData.Maxim_s));
            flag_mode='Scraper';
            Updatehistory('Change to Scraper scan');
        elseif strcmp(get(eventdata.NewValue,'String'),'Dipole')
            set(htext_Min,'String','Min I_0 (A):');
            set(htext_Max,'String','Max I_0 (A):');
            set(hedit_Min,'String',num2str(ParamsData.Minim_d));
            set(hedit_Max,'String',num2str(ParamsData.Maxim_d));
            flag_mode='Dipole';
            Updatehistory('Change to Dipole scan');
        end
    end


    function selcFig(source,eventdata)
        if strcmp(get(eventdata.NewValue,'String'),'Fig1')
            axes(findobj('Tag','ax1'));
        elseif strcmp(get(eventdata.NewValue,'String'),'Fig2')
            axes(findobj('Tag','ax2'));
        end
    end

%
    function UpdateGap(source,eventdata)
        ParamsData.gap=str2double(get(source,'String'));
        setpv('SCRH_GAP',ParamsData.gap);
        WaitDevices('SCRH_GAP','SCRH_OFFSET');
        ParamsData.gap_meas=getpv('SCRH_GAP');
        ParamsData.offset_meas=getpv('SCRH_OFFSET');
        
                
        ParamsData.Minim_s=max([RangeLeftJaw(1)-ParamsData.gap_meas/2 ParamsData.gap_meas/2-RangeRightJaw(2)]);
        ParamsData.Maxim_s=min([RangeLeftJaw(2)-ParamsData.gap_meas/2 ParamsData.gap_meas/2-RangeRightJaw(1)]);
        if strcmp(flag_mode,'Scraper')
            set(hedit_Min,'String',num2str(ParamsData.Minim_s));
            set(hedit_Max,'String',num2str(ParamsData.Maxim_s));
        end
        set(htext_offset_meas,'String',num2str(ParamsData.offset_meas));
        set(htext_gap_meas,'String',num2str(ParamsData.gap_meas));
        
        
    end

%
    function Updateoffset(source,eventdata)
        ParamsData.offset=str2double(get(source,'String'));
        setpv('SCRH_OFFSET',ParamsData.offset);
        WaitDevices('SCRH_GAP','SCRH_OFFSET');
        ParamsData.gap_meas=getpv('SCRH_GAP');
        ParamsData.offset_meas=getpv('SCRH_OFFSET');
        
                
        ParamsData.Minim_s=max([RangeLeftJaw(1)-ParamsData.gap_meas/2 ParamsData.gap_meas/2-RangeRightJaw(2)]);
        ParamsData.Maxim_s=min([RangeLeftJaw(2)-ParamsData.gap_meas/2 ParamsData.gap_meas/2-RangeRightJaw(1)]);
        set(hedit_Min,'String',num2str(ParamsData.Minim_s));
        set(hedit_Max,'String',num2str(ParamsData.Maxim_s));
        set(htext_offset_meas,'String',num2str(ParamsData.offset_meas));
        set(htext_gap_meas,'String',num2str(ParamsData.gap_meas));
        
    end

%
    function UpdateI0(source,eventdata)
        ParamsData.I0=str2double(get(source,'String'));
        ParamsData.I0_meas=ParamsData.I0;
        %setpv('BEND',ParamsData.I0_meas);
        WaitDevices('BEND');
        curbend = getpv('BEND');
        set(htext_I0_meas,'String',num2str(ParamsData.I0_meas));
    end
%
    function Updatebetas(source,eventdata)
        ParamsData.Dx  = str2double(get(hedit_Dx,'String'));
        ParamsData.Bx  = str2double(get(hedit_Bx,'String'));
        ParamsData.Ex  = str2double(get(hedit_Ex,'String'));
        eres=100*2*sqrt(ParamsData.Bx*(ParamsData.Ex*1e-9))/(ParamsData.Dx/1000);
        gapeq=eres*ParamsData.Dx/100;
        htext_Sum_A  = uicontrol('Style','text','String',sprintf('Min Energy resolution:\t %1.2f %%',eres),'Position',[dx,z2c+stepz,x4-4*dx,Sizez1]);
        htext_Sum_B  = uicontrol('Style','text','String',sprintf('Min Gap equivalen resolution (mm):\t %1.2f',gapeq),'Position',[dx,z2c,x4-4*dx,Sizez1]);
    end

% function to update the history text
    function Updatehistory(line)
        c=clock;
        if c(5)>=10
            cmin=sprintf('%1.0f',c(5));
        else
            cmin=sprintf('0%1.0f',c(5));
        end
        newline=sprintf('%1.0f\\%1.0f\\%1.0f    %1.0f:%s h  \t\t %s...\n',c(1),c(2),c(3),c(4),cmin,line);
        s=get(htext_history,'String');
        ns=size(s);
        nsmax=0;
        if ns(1)>1
            nsmax=(ns(1)-1);
        elseif ns(1)==1
            nsmax=1;
        end
        fs=[];
        for ni=1:nsmax
            fs=[fs s(ni,:) sprintf('\n')];
        end
        fs=[fs newline];
        set(htext_history,'String',fs);
    end

% titles
htext_var  = uicontrol('Style','text','String','Variables:','Position',[x1-Sizex1/2,z3,Sizex1,Sizez1]);
htext_par  = uicontrol('Style','text','String','Parameters:','Position',[x3-Sizex1/2,z3,Sizex1,Sizez1]);
%variables
htext_gap  = uicontrol('Style','text','String','gap (mm):','Position',[dx,z3-stepz,Sizex1,Sizez1]);
htext_gap_meas  = uicontrol('Style','text','String',num2str(ParamsData.gap_meas),'Position',[x1+dx,z3-stepz,Sizex1/2,Sizez1]);
hedit_gap  = uicontrol('Style','edit','String',num2str(ParamsData.gap),'Position',[x2-Sizex1/2-dx/2,z3-stepz,Sizex1/2,Sizez1],'Callback',{@UpdateGap},'BackgroundColor','w');
htext_I0  = uicontrol('Style','text','String','I_0 BEND (A):','Position',[dx,z3-2*stepz,Sizex1,Sizez1]);
htext_I0_meas  = uicontrol('Style','text','String',num2str(ParamsData.I0_meas),'Position',[x1+dx,z3-2*stepz,Sizex1/2,Sizez1]);
hedit_I0  = uicontrol('Style','edit','String',num2str(ParamsData.I0),'Position',[x2-Sizex1/2-dx/2,z3-2*stepz,Sizex1/2,Sizez1],'Callback',{@UpdateI0},'BackgroundColor','w');
htext_offset  = uicontrol('Style','text','String','Offset (mm):','Position',[dx,z3-3*stepz,Sizex1,Sizez1]);
htext_offset_meas  = uicontrol('Style','text','String',num2str(ParamsData.offset_meas),'Position',[x1+dx,z3-3*stepz,Sizex1/2,Sizez1]);
hedit_offset  = uicontrol('Style','edit','String',num2str(ParamsData.offset),'Position',[x2-Sizex1/2-dx/2,z3-3*stepz,Sizex1/2,Sizez1],'Callback',{@Updateoffset},'BackgroundColor','w');



%
htext_Min  = uicontrol('Style','text','String','Min Off. (mm):','Position',[dx,z3-4*stepz,Sizex1,Sizez1]);                  % by default scraper
hedit_Min  = uicontrol('Style','edit','String',num2str(ParamsData.Minim_s),'Position',[x1+dx,z3-4*stepz,Sizex1,Sizez1],'BackgroundColor','w');    % by default scraper
htext_Max  = uicontrol('Style','text','String','Max Off. (mm ):','Position',[dx,z3-5*stepz,Sizex1,Sizez1]);                 % by default scraper
hedit_Max  = uicontrol('Style','edit','String',num2str(ParamsData.Maxim_s),'Position',[x1+dx,z3-5*stepz,Sizex1,Sizez1],'BackgroundColor','w');    % by default scraper
htext_Num  = uicontrol('Style','text','String','Nº steps:','Position',[dx,z3-6*stepz,Sizex1,Sizez1]);
hedit_Num  = uicontrol('Style','edit','String',num2str(ParamsData.Num),'Position',[x1+dx,z3-6*stepz,Sizex1,Sizez1],'BackgroundColor','w');
htext_NumAveStep  = uicontrol('Style','text','String','Nº meas/step:','Position',[dx,z3-7*stepz,Sizex1,Sizez1]);
hedit_NumAveStep  = uicontrol('Style','edit','String',num2str(ParamsData.NumAveStep),'Position',[x1+dx,z3-7*stepz,Sizex1,Sizez1],'BackgroundColor','w');

%parameters
htext_Dx  = uicontrol('Style','text','String','D_x (mm):','Position',[x2+dx,z3-stepz,Sizex1,Sizez1]);
hedit_Dx  = uicontrol('Style','edit','String',num2str(ParamsData.Dx),'Position',[x3+dx,z3-stepz,Sizex1,Sizez1],'Callback',{@Updatebetas},'BackgroundColor','w');
htext_Bx  = uicontrol('Style','text','String','beta_x: (m)','Position',[x2+dx,z3-2*stepz,Sizex1,Sizez1]);
hedit_Bx  = uicontrol('Style','edit','String',num2str(ParamsData.Bx),'Position',[x3+dx,z3-2*stepz,Sizex1,Sizez1],'Callback',{@Updatebetas},'BackgroundColor','w');
htext_Ex  = uicontrol('Style','text','String','E_x: (nm rad)','Position',[x2+dx,z3-3*stepz,Sizex1,Sizez1]);
hedit_Ex  = uicontrol('Style','edit','String',num2str(ParamsData.Ex),'Position',[x3+dx,z3-3*stepz,Sizex1,Sizez1],'Callback',{@Updatebetas},'BackgroundColor','w');
htext_LinFr  = uicontrol('Style','text','String','Linac Fr. (Hz)','Position',[x2+dx,z3-4*stepz,Sizex1,Sizez1]);
hedit_LinFr = uicontrol('Style','edit','String',num2str(ParamsData.LinFr),'Position',[x3+dx,z3-4*stepz,Sizex1,Sizez1],'BackgroundColor','w');



% htext_A  = uicontrol('Style','text','String','A coeff:','Position',[x2+dx,z3-4*stepz,Sizex1,Sizez1]);
% hedit_A  = uicontrol('Style','edit','String',num2str(ParamsData.A,8),'Position',[x3+dx,z3-4*stepz,Sizex1,Sizez1],'BackgroundColor','w');
% htext_B  = uicontrol('Style','text','String','B coeff:','Position',[x2+dx,z3-5*stepz,Sizex1,Sizez1]);
% hedit_B  = uicontrol('Style','edit','String',num2str(ParamsData.B,8),'Position',[x3+dx,z3-5*stepz,Sizex1,Sizez1],'BackgroundColor','w');

% push button
h_GetData= uicontrol('Style','pushbutton','String','Get Data','Position',[x2-Sizex2/2,z2b,Sizex2,Sizez2],'Callback',{@GetData_Callback});
htext_info  = uicontrol('Style','text','ForegroundColor',[0 1 0],'String','Ready...','Position',[x2+Sizex2/2+dx,z2b+(Sizez2-Sizez1)/2,Sizex1/1.5,Sizez1],'BackgroundColor','w');
% double axes
ha = axes('Units','pixels','Position',[x4+Sizex1,z1+2*Sizez2,(x6-x4)-2*Sizex1,z5-z1-4*Sizez2]);
ax1 = gca;
set(ax1,'Tag','ax1','Color','none');
ylabel(ax1,'Q_{BCM2}/Q_{BCM1}','Color','r');

xlabel(ax1,'Beam Energy (MeV)','Color','r');
set(ax1,'Units','normalized','YColor',[1 0 0],'XColor',[1 0 0]);
ax2 = axes('Position',get(ax1,'Position'),'Tag','ax2','XAxisLocation','top','YAxisLocation','right','Color','none');
axes(findobj('Tag','ax2'));
%linkaxes([ax1 ax2],'x');
ylabel(ax2,'Q_{BCM2}/Q_{BCM1}/(gap*E/D_x)');
grid on;

%summary data
eres=100*2*sqrt(ParamsData.Bx*(ParamsData.Ex*1e-9))/(ParamsData.Dx/1000);
gapeq=eres*ParamsData.Dx/100;
htext_Sum_A  = uicontrol('Style','text','String',sprintf('Min Energy resolution:\t %1.2f %%',eres),'Position',[dx,z2c+stepz,x4-4*dx,Sizez1]);
htext_Sum_B  = uicontrol('Style','text','String',sprintf('Min Gap equivalen resolution (mm):\t %1.2f',gapeq),'Position',[dx,z2c,x4-4*dx,Sizez1]);
htext_Sum_1  = uicontrol('Style','text','String',sprintf('Total Normalized charge:\t 0.00'),'Position',[dx,z2c-stepz,x4-4*dx,Sizez1]);
htext_Sum_2  = uicontrol('Style','text','String',sprintf('Energy Spread HWHM:     \t 0.00'),'Position',[dx,z2c-2*stepz,x4-4*dx,Sizez1]);
htext_Sum_3  = uicontrol('Style','text','String',sprintf('Energy Spread RMS:      \t 0.00'),'Position',[dx,z2c-3*stepz,x4-4*dx,Sizez1]);

% initialize history text

htext  = uicontrol('Style','text','String','History: ','Position',[dx,z1,Sizex1,Sizez1]);
c=clock;
if c(5)>=10
    htext_history  = uicontrol('Style','edit','Max',2,'HorizontalAlignment','left','String',sprintf('%1.0f\\%1.0f\\%1.0f    %1.0f:%2.0f h  \t\t Initialization of the Linac Energy Spread GUI...',c(1),c(2),c(3),c(4),c(5)),'Position',[dx,dz,x6-2*dx,z1-dz],'BackgroundColor','w');
else
    htext_history  = uicontrol('Style','edit','Max',2,'HorizontalAlignment','left','String',sprintf('%1.0f\\%1.0f\\%1.0f    %1.0f:0%1.0f h. \t\t Initialization of the Linac Energy Spread GUI...',c(1),c(2),c(3),c(4),c(5)),'Position',[dx,dz,x6-2*dx,z1-dz],'BackgroundColor','w');
end
align(htext_history,'HorizontalAlignment','Left');

%% Initialize the GUI.
% Change units to normalized so components resize automatically.
%
set([f,hcheck_fig,hcheck_fig1,hcheck_fig2,htext_gap_meas,htext_LinFr,hedit_LinFr,htext_I0_meas,htext_offset,htext_offset_meas,hedit_offset,htext_NumAveStep,hedit_NumAveStep,htext_info,htext_I0,hedit_I0,htext_Sum_A,htext_Sum_B,ax1,ax2,htext_Sum_1,htext_Sum_2,htext_Sum_3,htext_Num,hedit_Num,htext_history,htext,hcheck_modeDipole,hcheck_modeScraper,htext_aquire,htext_Bx,hedit_Bx,hcheck_mode,htext,h_GetData,ha,hedit_Ex,htext_Ex,hedit_Dx,htext_Dx,hedit_Max,htext_Max,hedit_Min,htext_Min,hedit_gap,htext_gap,htext_par,htext_var],'Units','normalized');
% Assign the GUI a name to appear in the window title.
set(f,'Name','Linac Energy Spread Measurement')
% Move the GUI to the center of the screen.
movegui(f,'center');
% Make the GUI visible.
set(f,'Visible','on');



%% Push button callbacks.

    function GetData_Callback(source,eventdata)
        %linkaxes([ax1 ax2],'off');
        % Display surf plot of the currently selected data.
        if strcmp(flag_mode,'None')
            error('None measurement mode choosen');
        end
        ParamsData.gap=str2double(get(hedit_gap,'String'));
        ParamsData.I0=str2double(get(hedit_I0,'String'));
        ParamsData.Minim=str2double(get(hedit_Min,'String'));
        ParamsData.Maxim=str2double(get(hedit_Max,'String'));
        ParamsData.Num=str2double(get(hedit_Num,'String'));
        ParamsData.NumAveStep=str2double(get(hedit_NumAveStep,'String'));
        ParamsData.Dx=str2double(get(hedit_Dx,'String'));
        ParamsData.Bx=str2double(get(hedit_Bx,'String'));
        ParamsData.Ex=str2double(get(hedit_Ex,'String'));
        %         ParamsData.A=str2double(get(hedit_A,'String'));
        %         ParamsData.B=str2double(get(hedit_B,'String'));
        ParamsData.Minim_s=max([RangeLeftJaw(1)-ParamsData.gap/2 ParamsData.gap/2-RangeRightJaw(2)]);
        ParamsData.Maxim_s=min([RangeLeftJaw(2)-ParamsData.gap/2 ParamsData.gap/2-RangeRightJaw(1)]);
        ParamsData.offset=str2double(get(hedit_offset,'String'));
        ParamsData.LinFr=str2double(get(hedit_LinFr,'String'));
        
        set(hedit_Min,'ForegroundColor',[0 0 0]);
        set(hedit_Max,'ForegroundColor',[0 0 0]);
        
        if strcmp(flag_mode,'Scraper')
            if ParamsData.Minim<ParamsData.Minim_s
                set(hedit_Min,'ForegroundColor',[1 0 0]);
                error('Limits out of range!! ');
            elseif ParamsData.Maxim>ParamsData.Maxim_s
                set(hedit_Max,'ForegroundColor',[1 0 0]);
                error('Limits out of range!! ');
            end
        elseif strcmp(flag_mode,'Dipole')
            if ParamsData.Minim<ParamsData.Minim_d
%            if ParamsData.Minim< 120 
                set(hedit_Min,'ForegroundColor',[1 0 0]);
                error('Limits out of range!! ');
            elseif ParamsData.Maxim>ParamsData.Maxim_d
%            if ParamsData.Maxim< 160 
                set(hedit_Max,'ForegroundColor',[1 0 0]);
                error('Limits out of range!! ');
            end
        end
        
        DataRaw=struct;
        DataRaw.Data=[];
        DataRaw.offset=[];
        DataRaw.energy=[];
        time_name=datestr(now,'yyyymmdd-HHMMSS');
        save(filename,'ParamsData');
        if strcmp(flag_mode,'Scraper')
            SummaryData=ScraperMeasure(ParamsData,htext_info,ax1,ax2,f,htext_offset_meas,mode_in);
            
        elseif strcmp(flag_mode,'Dipole')
            SummaryData=DipoleMeasure(ParamsData,htext_info,ax1,ax2,f,htext_I0_meas,mode_in);
            
        end
        
        set(htext_info,'String','Done...','ForegroundColor',[0 1 0]);
        set(htext_Sum_1,'String',sprintf('Total Normalized charge:\t %1.4f',SummaryData.sum));
        set(htext_Sum_2,'String',sprintf('Energy Spread HWHM:     \t %1.4f %%',SummaryData.HWHM));
        set(htext_Sum_3,'String',sprintf('Energy Spread STD:      \t %1.4f %%',SummaryData.rms));
        Updatehistory(sprintf('Measure started...TNC:    %1.4f    HWHM:  %1.4f %%  STD:  %1.4f %%    E_{mean}:  %1.4f MeV   E_{max}:  %1.4f MeV',SummaryData.sum,SummaryData.HWHM,SummaryData.rms,SummaryData.ener0,SummaryData.ener_max));
        %linkaxes([ax1 ax2],'xy');
        c=clock;
        %SetPlotSize(40,20,f);  
        full_nomfile_pic = [daydir, '/EnergySpread-',time_name,'.png'];
        print('-dpng',full_nomfile_pic);
        Update_Gap_Ofset;
    end

    function Update_Gap_Ofset
        ParamsData.gap_meas=getpv('SCRH_GAP');
        ParamsData.offset_meas=getpv('SCRH_OFFSET');
        ParamsData.Minim_s=max([RangeLeftJaw(1)-ParamsData.gap_meas/2 ParamsData.gap_meas/2-RangeRightJaw(2)]);
        ParamsData.Maxim_s=min([RangeLeftJaw(2)-ParamsData.gap_meas/2 ParamsData.gap_meas/2-RangeRightJaw(1)]);
        if strcmp(flag_mode,'Scraper')
            set(hedit_Min,'String',num2str(ParamsData.Minim_s));
            set(hedit_Max,'String',num2str(ParamsData.Maxim_s));
        end
        set(htext_offset_meas,'String',num2str(ParamsData.offset_meas));
        set(htext_gap_meas,'String',num2str(ParamsData.gap_meas));
    end

set(f,'HandleVisibility','callback');

end

%% functions for the MML part
function SummaryData=DipoleMeasure(ParamsData,htext_info,ax1,ax2,f,htext_I0_meas,mode_in)

offset_BCM_1=ParamsData.offset_BCM_1;
offset_BCM_2=ParamsData.offset_BCM_2;
gap=ParamsData.gap;
I0=ParamsData.I0;
Minim=ParamsData.Minim;
Maxim=ParamsData.Maxim;
Num=ParamsData.Num;
NumAveStep=ParamsData.NumAveStep;
Dx=ParamsData.Dx;
A=ParamsData.A;
B=ParamsData.B;


% Set gap to the desired value
setpv('SCRH_LEFT',gap/2);
setpv('SCRH_RIGHT',gap/2);
setpv('BEND',Minim);
WaitDevices('SCRH_LEFT','SCRH_RIGHT','BEND');
pause(4.0);
%scan the dipole
sumi=0;
for ibnd = 1:(Num+1)
    set(htext_info,'String',sprintf('Step %d/%d',ibnd,Num+1),'ForegroundColor',[1 0 0]);
    setbend = Minim+(Maxim-Minim)*(ibnd-1)/(Num);
    setpv('BEND',setbend);
    WaitDevices('BEND');
    pause(2.0);
    curbend = getpv('BEND');
    ParamsData.I0_meas=curbend;
    set(htext_I0_meas,'String',num2str(ParamsData.I0_meas));
    energy(ibnd)=(A*curbend+B);
    offset(ibnd)=curbend;
    disp(['BEND current  ', num2str(curbend),' A']);
    cr =zeros(NumAveStep,1);
    bcm1_in =zeros(NumAveStep,1);
    bcm2_in =zeros(NumAveStep,1);
    for m=1:NumAveStep,
        bcm1_in(m) = getam('BCM',1)-offset_BCM_1;
        bcm2_in(m) = getam('BCM',2)-offset_BCM_2;
        cr(m) = bcm2_in(m)/bcm1_in(m);
        if strcmp('Online',mode_in)
            pause(1.0/ParamsData.LinFr);
        end
    end
    bcm1(ibnd)=mean(bcm1_in);
    bcm2(ibnd)=mean(bcm2_in);
    crave = mean(cr);
    if isnan(crave)
        Data(ibnd)=rand;
    else
        Data(ibnd)=crave/(gap*energy(ibnd)/Dx);
    end
    
    %Data(ibnd)=(exp(-((energy(ibnd)-104.5)/(0.2*104.5/100)).^2)+0.01*randn)/3;
    
    disp(['charge ratio  ', num2str(crave)])  ;
    DataRaw=struct;
    DataRaw.bcm1=bcm1;
    DataRaw.bcm2=bcm2;
    DataRaw.Data=Data;
    DataRaw.offset=offset;
    DataRaw.energy=energy;
    DataRaw.A_generalized=A;
    DataRaw.B_generalized=B;
    DataRaw.A_generalized_y=(gap/Dx);
    DataRaw.B_generalized_y=0;
    if ibnd==(Num+1)
        sumi=1;
    end
    SummaryData=AnalizeAndPlot(ParamsData,DataRaw,ax1,ax2,sumi,'dipole',f);
    xlabel(ax2,' I_{BEND} (A)');
end



%
% % Artificial Test values:
% Data=exp(-((1:ParamsData.Num)-5).^2)+0.1*randn(1,ParamsData.Num).^2;
% offset=1:ParamsData.Num;
% energy=(1:ParamsData.Num)*100;
setpv('BEND',I0);
setpv('SCRH_OFFSET',0);
setpv('SCRH_GAP',30);

end
function SummaryData=ScraperMeasure(ParamsData,htext_info,ax1,ax2,f,htext_offset_meas,mode_in)


gap=ParamsData.gap;
I0=ParamsData.I0;
Minim=ParamsData.Minim;
Maxim=ParamsData.Maxim;
Num=ParamsData.Num;
NumAveStep=ParamsData.NumAveStep;
Dx=ParamsData.Dx;
A=ParamsData.A;
B=ParamsData.B;
offset_BCM_1=ParamsData.offset_BCM_1;
offset_BCM_2=ParamsData.offset_BCM_2;
%


% Set gap to the desired value
%setpv('SCRH_LEFT',gap/2);
%setpv('SCRH_RIGHT',gap/2);
% set dipole to reference value


%scan the scraper

setpv('BEND',I0);
setpv('SCRH_LEFT',gap/2+Minim);
setpv('SCRH_RIGHT',gap/2-Minim);
WaitDevices('SCRH_LEFT','SCRH_RIGHT','BEND');
curbend = getpv('BEND');

sumi=0;
for ioff = 1:(Num+1)
    pause(1.0);
    set(htext_info,'String',sprintf('Step %d/%d',ioff,Num+1),'ForegroundColor',[1 0 0]);
    offset_0= Minim+(Maxim-Minim)*(ioff-1)/(Num);
    setpv('SCRH_LEFT',gap/2+offset_0);
    setpv('SCRH_RIGHT',gap/2-offset_0);
    WaitDevices('SCRH_LEFT','SCRH_RIGHT');
    scra_l = getpv('SCRH_LEFT')-gap/2;
    scra_r = gap/2-getpv('SCRH_RIGHT');
    offset(ioff)=(scra_r+scra_l)/2;
    ParamsData.offset_meas=offset(ioff);
    set(htext_offset_meas,'String',num2str(ParamsData.offset_meas));
    
    energy(ioff)=(A*curbend+B)*(1+offset(ioff)/Dx);
    disp(['Scraper offset ', num2str(offset(ioff)), ' mm']);
    cr =zeros(NumAveStep,1);
    bcm1_in =zeros(NumAveStep,1);
    bcm2_in =zeros(NumAveStep,1);
    for m=1:NumAveStep,
        bcm1_in(m) = getam('BCM',1)-offset_BCM_1;
        bcm2_in(m) = getam('BCM',2)-offset_BCM_2;
        cr(m) = bcm2_in(m)/bcm1_in(m);
        if strcmp('Online',mode_in)
            pause(1.0/ParamsData.LinFr);
        end
    end
    bcm1(ioff)=mean(bcm1_in);
    bcm2(ioff)=mean(bcm2_in);
    crave = mean(cr);
    if isnan(crave)
        Data(ioff)=rand;
    else
        Data(ioff)=crave/(gap*energy(ioff)/Dx);
    end
    
    %Data(ioff)=(exp(-((energy(ioff)-104.5)/(0.2*104.5/100)).^2)+0.01*randn)/3;
    
    disp(['charge ratio  ', num2str(crave)])  ;
    DataRaw=struct;
    DataRaw.bcm1=bcm1;
    DataRaw.bcm2=bcm2;
    DataRaw.Data=Data;
    DataRaw.offset=offset;
    DataRaw.energy=energy;
    DataRaw.B_generalized=(A*curbend+B)*(1);
    DataRaw.A_generalized=(A*curbend+B)/Dx;
    DataRaw.A_generalized_y=(gap/Dx);
    DataRaw.B_generalized_y=0;
    if ioff==(Num+1)
        sumi=1;
    end
    SummaryData=AnalizeAndPlot(ParamsData,DataRaw,ax1,ax2,sumi,'scraper',f);
    xlabel(ax2,'srapper Offset (mm)');
        

end


%scan the scraper

setpv('SCRH_OFFSET',0);
setpv('SCRH_GAP',30);
WaitDevices('SCRH_GAP','SCRH_OFFSET');


% % Artificial Test values:
% Data=exp(-((1:ParamsData.Num)-5).^2)+0.1*randn(1,ParamsData.Num).^2;
% offset=1:ParamsData.Num;
% energy=(1:ParamsData.Num)*100;



end



%% analize and plotdata functions
function SummaryData=AnalizeAndPlot(ParamsData,DataRaw,ax1,ax2,sumi,flag,f)

global daydir time_name;

A_generalized=DataRaw.A_generalized;
B_generalized=DataRaw.B_generalized;
A_generalized_y=DataRaw.A_generalized_y;
Data=DataRaw.Data;
bcm1=DataRaw.bcm1;
bcm2=DataRaw.bcm2;
energy=DataRaw.energy;
offset=DataRaw.offset;


gap=ParamsData.gap;
I0=ParamsData.I0;
Minim=ParamsData.Minim;
Maxim=ParamsData.Maxim;
Num=ParamsData.Num;
NumAveStep=ParamsData.NumAveStep;
Dx=ParamsData.Dx;
Bx=ParamsData.Bx;
Ex=ParamsData.Ex;
A=ParamsData.A;
B=ParamsData.B;








SummaryData=struct;
SummaryData.sum=1;
SummaryData.rms=0;
SummaryData.HWHM=0;

axes(findobj('Tag','ax1'));
cla;
hl1 = line(energy,Data.*energy*A_generalized_y,'LineStyle','--','Marker','o','Color','r');
axes(findobj('Tag','ax2'));
cla;
hl2 = line(offset,Data,'LineStyle','--','Marker','o','Color','k');


%  Normalized Ratio vs offset graph
xlimits = get(ax2,'XLim');
ylimits = get(ax2,'YLim');
xinc = (xlimits(2)-xlimits(1))/5;
yinc = (ylimits(2)-ylimits(1))/5;
set(ax2,'XTick',xlimits(1):xinc:xlimits(2),'YTick',ylimits(1):yinc:ylimits(2));

%  Ratio vs energy graph
xlimits = B_generalized+A_generalized*xlimits;
ylimits = (ylimits.*xlimits*A_generalized_y);
ylimits(2)=2*ylimits(2)-ylimits(1);
yinc = (ylimits(2)-ylimits(1))/5;
xinc = (xlimits(2)-xlimits(1))/5;
set(ax1,'XLim',xlimits);
set(ax1,'YLim',ylimits);
set(ax1,'XTick',xlimits(1):xinc:xlimits(2),'YTick',ylimits(1):yinc:ylimits(2));

%linkaxes([ax1 ax2],'xy');



if sumi
    inidir=pwd;
    
    
    
    % write params to file 
    full_nomfile_params = [daydir, '/EnergySpread_Params-',time_name,'.dat'];
    fid = fopen(full_nomfile_params, 'wt');
    fprintf(fid,' Measurement type: %s scan\n\n',flag);
    fprintf(fid,' Gap: %1.9f\n',gap);
    fprintf(fid,' I0: %1.9f\n',I0);
    fprintf(fid,' Minim of scan: %1.9f\n',Minim);
    fprintf(fid,' Maxim of scan: %1.9f\n',Maxim);
    fprintf(fid,' Num of steps: %d\n',Num);
    fprintf(fid,' Num of measuements per step to smooth the measurement: %d\n',NumAveStep);
    fprintf(fid,' Dispersion at scraper: %1.9f mm\n',Dx);
    fprintf(fid,' beta at scraper: %1.9f m\n',Bx);
    fprintf(fid,' Emitance at scaper: %1.9f nmrad\n',Ex);
    fprintf(fid,' A dipole fiting parameter: %1.9f\n',A);
    fprintf(fid,' B dipole fiting parameter: %1.9f\n',B);
    fclose(fid);
    % write data to file

    full_nomfile = [daydir, '/EnergySpread-',time_name,'.dat'];
    fid = fopen(full_nomfile, 'wt');
    fprintf(fid,' Intensity \t    gap    \t   offset  \t   BCM1  \t   BCM2  \t   Energy  \tNorm.Ratio\n');
    if strcmp(flag,'dipole')
        for jj=1:length(bcm1)
            fprintf(fid,'%1.9f\t%1.9f\t%1.9f\t%1.9f\t%1.9f\t%1.9f\t%1.9f\n',offset(jj),gap,0.000000000,bcm1(jj),bcm2(jj),energy(jj),Data(jj));
        end
    elseif strcmp(flag,'scraper')
        for jj=1:length(bcm1)
            fprintf(fid,'%1.9f\t%1.9f\t%1.9f\t%1.9f\t%1.9f\t%1.9f\t%1.9f\n',I0,gap,offset(jj),bcm1(jj),bcm2(jj),energy(jj),Data(jj));
        end
    end
    fclose(fid);
    cd(inidir);
    
    
    
    SummaryData.sum=trapz(energy,Data);
    p=Data/sum(Data);
    SummaryData.ener0=sum(energy.*p);
    [pm pi]=max(p);
    SummaryData.ener_max=energy(pi);
    emean=sum(energy.*p);
    SummaryData.rms=100*sqrt(sum(abs(p).*(energy-emean).^2))/emean;
    [fmax I]=max(Data);
    hm=fmax/2;
    found1=0;
    in1=I;
    in2=I-1;
    if ~sign(sum(isnan(Data)))&&I>1&&I<length(Data)-1
        
        while found1~=1
            if Data(in1)>hm&&Data(in2)<hm
                found1=1;
                hm1=energy(in1)*(hm-Data(in2))/(Data(in1)-Data(in2))+energy(in2)*(Data(in1)-hm)/(Data(in1)-Data(in2));
            else
                in1=in1-1;
                in2=in2-1;
                found1=0;
            end
            if in2<1
                in1=in1+1;
                in2=in2+1;
                hm1=energy(in1)*(hm-Data(in2))/(Data(in1)-Data(in2))+energy(in2)*(Data(in1)-hm)/(Data(in1)-Data(in2));
                break;
            end
        end
        
        found2=0;
        in1=I;
        in2=I+1;
        while found2~=1
            if Data(in1)>hm&&Data(in2)<hm
                found2=1;
                hm2=energy(in1)*(hm-Data(in2))/(Data(in1)-Data(in2))+energy(in2)*(Data(in1)-hm)/(Data(in1)-Data(in2));
            else
                in1=in1+1;
                in2=in2+1;
                found2=0;
            end
            if in2>length(Data)
                in1=in1-1;
                in2=in2-1;
                hm2=energy(in1)*(hm-Data(in2))/(Data(in1)-Data(in2))+energy(in2)*(Data(in1)-hm)/(Data(in1)-Data(in2));
                break;
            end
        end
        
        HWHM=((1-found1+1)*hm2-(1-found2+1)*hm1)/2;
    else
        HWHM=NaN;
    end
    SummaryData.HWHM=100*HWHM/emean;
    
    

end


end

function WaitDevices(varargin)
% varargout=Set_and_Wait(Fam,value,toleT,toleV)
numargin = size(varargin,2);

global mode_in;

toleT=1.0;

Fam=cell(numargin,1);
attr_prop_list_tango=zeros(numargin,1);
for ii=1:numargin
    Fam{ii}=varargin{ii};
end
tango_names=family2tangodev(Fam);

if strcmp(mode_in,'Online')
    attr_prop=1;
    while attr_prop==1
        pause(toleT);
        for ii=1:numargin
            dir_name=tango_names{ii}{1};
            str=tango_read_attribute2(dir_name,'State');
            attr_prop_list_tango(ii)=convertState_Tango_from_move(str.value);
        end
        attr_prop=sign(sum(attr_prop_list_tango));
    end
end
 pause(toleT);

end
function num=convertState_Tango_from_move(int)

% 0 ON
% 1 OFF
% 2 CLOSE
% 3 OPEN
% 4 INSERT
% 5 EXTRACT
% 6 MOVING
% 7 STANDBY
% 8 FAULT
% 9 INIT
% 10 RUNNING
% 11 ALARM
% 12 DISABLE
% 13 UNKNOWN
num=0;
if int==6
    num=1;
end
    
end




