function [ data ] = LoadArchFile( filename)
% [ data ] = LoadArchFile( filename )

tmp = importdata(filename);
order=tmp.textdata(1,2:end);
data.time=tmp.data(:,1);
if strcmp(order{2}, 'sr/di/dcct/averagecurrent'),
    data.current=tmp.data(:,2);
    data.value=tmp.data(:,3);
    data.device=order{3};
else
    data.current=tmp.data(:,3);
    data.value=tmp.data(:,2);
    data.device=order{2};
end

end

