function inv_R=invSVD(U,S,V,varargin)
% Perform an SVD inversion with n singular values
nargin=numel(varargin);
if nargin==0
    alpha=0;
    n=numel(diag(S));
elseif nargin==1
    v=varargin{1};
    if floor(v)==v
        n=v;
        alpha=0;
    else
        n=size(S,1);
        alpha=v;
    end
else
    error('wrong number of variables');
end
small=1e-16;
sd=diag(S);
in_sd0=find(abs(sd)<small);
d=diag(S)./(diag(S).^2+alpha^2);
%plot(d,'-r.');hold on;plot(1./diag(S),'-k.');
d(in_sd0)=zeros(length(in_sd0),1);
ndiag=length(d);
d((n+1):ndiag)=zeros(ndiag-n,1);
ind_S_0=diag(d);
ind_S=S';
ind_S(1:ndiag,1:ndiag)=ind_S_0;
inv_R=V*ind_S*(U');
end