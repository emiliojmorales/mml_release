function varargout = pb_zeus(varargin)
%PLOTBETA plots UNCOUPLED! beta-functions
% PLOTBETA(RING) calculates beta functions of the lattice RING
% PLOTBETA with no argumnts uses THERING as the default lattice
%  Note: PLOTBETA uses FINDORBIT4 and LINOPT which assume a lattice
%  with NO accelerating cavities and NO radiation
%
% See also PLOTCOD 
global THERING

    RING = THERING;

L = length(RING);
twiss=gettwiss();
betax=twiss.betax;
betay=twiss.betay;
s=twiss.s;
ETA=measeta_at_zeus;
etax=ETA(:,1);
for i=1:length(RING), 
    L(i)=RING{i}.Length; 
end

% [TD, tune] = twissring(RING,0,1:(length(RING)+1),'chroma');
% BETA = cat(1,TD.beta);
% ETA = cat(2,TD.Dispersion);
% S  = cat(1,TD.SPos);
% betax=BETA(:,1);
% betay=BETA(:,2);
% etax=ETA(1,:)';

tune=[twiss.phix(length(twiss.phix)),twiss.phiy(length(twiss.phiy))] ;
disp(tune)

if nargin > 0 
    sim=varargin{1};
else
   sim=1;
end
if nargin > 1
    step = varargin{2};
else
    step=0.1;
end
range=[0  max(s)];
np=floor((range(2)-range(1))/step);
ss=range(1)+[(0:np)*step max(s)];
bxs=spline(s(L>0), betax(L>0), ss);
bxs(np)=bxs(np-1);
bys=spline(s(L>0), betay(L>0), ss);
bxs(np)=bxs(np-1);
etaxs=interp1(s(L>0), etax(L>0), ss,'spline');
% plot betax and betay in two subplots

h1_0 =subplot(32,1,[1 25]);
hold off
[AX,H1,H2] = plotyy(s, betax,s, etax,'plot');
hold on
set(H1,'Marker','.');
set(H1,'LineStyle','none');
set(H2,'LineStyle','none');
set(H1,'Color','r');
H3=plot(s, betay,'.');
plot(ss, bxs,'r-');
plot(ss, bys,'b-');

legend([H1 H3 H2],'\beta_x','\beta_y','D_x','Location','North','Orientation','horizontal');
legend('boxoff');

set(get(AX(1),'Ylabel'),'String','\beta_{x,y} [m]') 
set(get(AX(2),'Ylabel'),'String','D_x [m]') 
set(H2,'Marker','.')
set(AX(1),'XLim',[0 max(s)/sim]);
set(AX(1),'YLim',[0 max([bys bxs])*1.1]);
set(AX(2),'XLim',[0 max(s)/sim]);
set(AX(2),'YLim',[min(etaxs)*1.1*(1-sign(min(etaxs)))/2 max(etaxs)*1.1]);
set(AX(2),'YTickMode','auto');
set(AX(1),'YTickMode','auto');
set(gcf,'CurrentAxes',AX(2));

hold on;
plot(ss,etaxs);
%{
plot(s, betax, '.r');
hold on
plot(s, betay,'.b');
plot(s, 10*etax,'.g');
plot(ss, bxs,'r-');
plot(ss, bys,'b-');
plot(ss, 10*etaxs,'g-');
ylabel('\beta_{x,y} [m]');
grid on
hold on
%}
%plot(S,BETA(:,2),'.-r');

title('\beta-functions');





h2_0 =subplot(32,1,[28 32]);
hold off;
drawlattice();
xlabel('s(m)');
xaxis([0 max(s)/sim]);

linkaxes([h2_0 AX(2)],'x');
linkaxes([h2_0 AX(1)],'x');
set(gca,'ytick',[]);
set(gca,'xtick',[]);
numargout = nargout;
if numargout==1
    varargout{1} = ss;
elseif numargout==2
    varargout{1} = ss;
    varargout{2} = bxs;
elseif numargout==3
    varargout{1} = ss;
    varargout{2} = bxs;
    varargout{3} = bys;
elseif numargout==4
    varargout{1} = ss;
    varargout{2} = bxs;
    varargout{3} = bys;
    varargout{4} = etaxs;
end

end
% Set the same horizontal scale as beta x plot

