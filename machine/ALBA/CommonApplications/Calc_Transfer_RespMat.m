function Calc_Transfer_RespMat(varargin)

global THERING Transfer_RespMat;

switch2sim;
damp=0.01;
numargin = size(varargin,2);
if numargin==1
    damp=varargin{1};
end
L_ring=numel(THERING);
T0=linepass(THERING,[0 0 0 0 0 0]',1:(L_ring+1));

%% Horizontal correctors

AO=getao();
hcm_names=AO.HCM.DeviceName;
hcm_status=AO.HCM.Status;
hcm_names=hcm_names(hcm_status==1);
n_hcm=length(hcm_names);
Mtr=zeros(6,L_ring+1,n_hcm);
for kk=1:n_hcm
    steppv('HCM',damp,kk);
    T=linepass(THERING,[0 0 0 0 0 0]',1:(L_ring+1));
    Mtr(:,:,kk)=(T-T0)/damp;
    steppv('HCM',-damp,kk);
end
Transfer_RespMat.XResp=Mtr;

%% Horizontal correctors

AO=getao();
vcm_names=AO.VCM.DeviceName;
vcm_status=AO.VCM.Status;
vcm_names=vcm_names(vcm_status==1);
n_vcm=length(vcm_names);
Mtr=zeros(6,L_ring+1,n_hcm);
for kk=1:n_vcm
    steppv('VCM',damp,kk);
    T=linepass(THERING,[0 0 0 0 0 0]',1:(L_ring+1));
    Mtr(:,:,kk)=(T-T0)/damp;
    steppv('VCM',-damp,kk);
end
Transfer_RespMat.YResp=Mtr;

end