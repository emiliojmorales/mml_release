function getBPMs_freeze_group(varargin)
% [M_x M_y M_I]=getBPMs_Not_freeze(varargin)


%% Parameters of the calculation
% DDNSamples for the ACQs and DDBufferSize for non-ACQs
%Buffersize=160000;
% For checking the Intensity.
%turn=2000;
% intensity threshold for the chek
%Imin=0.3E7;


if length(varargin)==3
    Buffersize = varargin{1};
    turn = varargin{2};
    Imin = varargin{3};
else
    Buffersize=[];
    turn=[];
    Imin=[];
end
if isempty(Buffersize)
    answer = inputdlg({'BPM buffer Size ', 'Ref turn','Intensity threshold'},'Freezed BPMs shot',1,{'1024','500','0.05e7'});
    if isempty(answer)
        return
    end
    Buffersize = str2num(answer{1});
    turn = str2num(answer{2});
    Imin = str2num(answer{3});
end

tic;
%% Initialization

ChangeBoosterBPMsDDoffset(0);
MC=getmachineconfig();





%% Reading the BPMS

valid=false;
AO=getao();
bpm_names=AO.BPMx.DeviceName;
bpm_status=AO.BPMx.Status;
bpm_names=bpm_names(bpm_status==1);
bpm_names_noACQ=bpm_names;
n_bpm=length(bpm_names);
res=findstr('ACQ',bpm_names{1});
pos_=findstr('-',bpm_names{1});
if isempty(res)
    fprintf('The BPMs are not ACQ\n');
    for ii=1:n_bpm
        str_bpm=bpm_names_noACQ{ii};
        bpm_names{ii}=[str_bpm(1:pos_(1)) 'ACQ-' str_bpm((pos_(1)+1):end)];
    end
else
    fprintf('The BPMs are ACQ\n');
    for ii=1:n_bpm
        str_bpm=bpm_names{ii};
        bpm_names_noACQ{ii}=[str_bpm(1:pos_(1)) str_bpm((pos_(1)+5):end)];
    end
end


%Xstr=tango_read_attribute2(bpm_names{1},'XPosDD');
%X=Xstr.value;
%nturns=length(X);
M_x=zeros(Buffersize,n_bpm);
M_y=zeros(Buffersize,n_bpm);
M_I=zeros(Buffersize,n_bpm);

buff_size=zeros(n_bpm,1);
for ii=1:n_bpm
    str_bpm=tango_read_attribute2(bpm_names{ii},'DDNSamples');
    tango_command_inout2(bpm_names_noACQ{ii},'EnableDDBufferFreezing');
    pause(0.1);
    buff_size(ii)=str_bpm.value(1);
end

buff_size_readback=zeros(n_bpm,1);
Trigger_counter_before=zeros(n_bpm,1);
Trigger_counter_after=zeros(n_bpm,1);
% make group
BPMs_group_id = tango_group_create ('BPM');
for ii =1:n_bpm,
    tango_group_add (BPMs_group_id, bpm_names{ii});
end

BPMs_group_id_noACQ = tango_group_create ('BPM_noACQ');
for ii =1:n_bpm,
    tango_group_add (BPMs_group_id_noACQ, bpm_names_noACQ{ii});
end

try
    
    %prepare all BPMs
    tango_group_command_inout(BPMs_group_id,'DDStop', 0);
    pause(0.1);
    tango_group_write_attribute(BPMs_group_id,'DDNSamples', int32(Buffersize),0);
    pause(0.1);
    tango_group_command_inout(BPMs_group_id_noACQ,'EnableDDBufferFreezing', 0);
    
    
    %test BPMs
    buffer_read=tango_group_read_attribute(BPMs_group_id_noACQ,'DDBufferSize',0);
    if buffer_read.has_failed
        for ii=1:n_bpm
            if buffer_read.replies(ii).has_failed
                fprintf('Fail in BPM %d\n',ii);
                error('Not all BPMs well readed');
            end
        end
    end
    for ii=1:n_bpm
        buff_size_readback(ii)=buffer_read.replies(ii).value(1);
    end
    if sum(buff_size_readback-Buffersize)>0
        figure;plot(buff_size_readback-Buffersize,'-ro');
        xlabel('BPM num');
        ylabel('DDBufferSize(read)-DDNSamples(write)');
    end
    
    
    Itestii=zeros(n_bpm,1);
    ntry=0;
    while((valid==false) && ntry<10)
        ntry=ntry+1;
        tmp=tango_group_read_attribute(BPMs_group_id_noACQ,'DDTriggerCounter',0);
        for ii=1:n_bpm
            Trigger_counter_before(ii)=tmp.replies(ii).value;
        end
        tango_group_command_inout(BPMs_group_id_noACQ,'UnfreezeDDBuffer', 0);
        tmp=tango_group_read_attribute(BPMs_group_id_noACQ,'DDTriggerCounter',0);
        for ii=1:n_bpm
            Trigger_counter_after(ii)=tmp.replies(ii).value;
        end
        ntime=2;
        fprintf('Waiting.');
        for ii=1:ntime
            pause(0.33);
            fprintf('.');
            pause(0.33);
            fprintf('.');
            pause(0.33);
            fprintf('.%d',ii);
        end
        fprintf('\n');
        
        %reading all!!
        tunesx=zeros(n_bpm,1);
        XTango = tango_group_read_attributes(BPMs_group_id_noACQ,{'XPosDD','ZPosDD','SumDD'},0);
        if XTango.has_failed
            for ii=1:n_bpm
                if XTango.dev_replies(ii).has_failed
                    fprintf('Fail in BPM %d\n',ii);
                    error('Not all BPMs well readed');
                end
            end
        end
        for ii=1:n_bpm
            try
                Xstr=XTango.dev_replies(ii).attr_values(1);
                Ystr=XTango.dev_replies(ii).attr_values(2);
                Istr=XTango.dev_replies(ii).attr_values(3);
                X=Xstr.value;
                Y=Ystr.value;
                I=Istr.value;
                tunesx(ii)=tune_search_padd(1,X(500:end),[0.1 0.5]);
                M_x(:,ii)=X';
                M_y(:,ii)=Y';
                M_I(:,ii)=I';
                Itestii(ii)=M_I(turn, ii);
            catch ME1
                fprintf('Efective buffer size is: %d\n',numel(X));
                X=[];
                fprintf('Error: %s\n',ME1.message);
                Itestii(ii)=-1;
                tango_group_kill(BPMs_group_id);
                tango_group_kill(BPMs_group_id_noACQ);
            end
        end
        valid=true;
        tunesx_mean=mean(tunesx);
        for ii=1:n_bpm
            if abs(tunesx(ii)-tunesx_mean)>0.0005
                fprintf('tunesx(%d)=%f(mean:%f), Badly synchronized  BPM nn %d\n',ii,tunesx(ii),tunesx_mean,ii)
                valid=false;
            end
            if (Itestii(ii) >= Imin),
                fprintf('Itest=%f, Adquired BPM nn %d\n',Itestii(ii),ii)
            else
                fprintf('Itest=%f, Bad  BPM nn %d\n',Itestii(ii),ii)
                valid=false;
            end
        end
        
        pause(0.1);
    end
    
    
    %% Back to previous BPM state
            
    tango_group_command_inout(BPMs_group_id,'DDStop', 0);
    pause(0.1);
    tango_group_write_attribute(BPMs_group_id,'DDNSamples',int32(buff_size(ii)),0);
    pause(0.1);
    tango_group_command_inout(BPMs_group_id_noACQ,'DisableDDBufferFreezing', 0);
    pause(0.1);
    for ii=1:n_bpm
        tango_command_inout2(bpm_names{ii},'DDAcquire',uint8(0));
    end
    
    %daydir=['/data/MML/WorkDirectory/zeus/BPMs/',datestr(now,'yyyymmdd')];
    daydir=['/data/BOOSTER/',datestr(now,'yyyy'),'/',datestr(now,'yyyymmdd'),'/BPMs_freezed'];
    mkdir(daydir);
    cd(daydir);
    name0=['BPM_freezed_' datestr(now, 'yyyymmddTHHMMSS') ] ;
    name=[name0 '.mat'];
    save(name,'M_x','M_y','M_I','MC','Trigger_counter_before','Trigger_counter_after');
    
    %     figure;
    %     s=getspos('BPMx');
    %     turns=5:floor(Buffersize/9):Buffersize;
    %     numavesamples=min([Buffersize-turns(end) 500]);
    %     for loop=1:9
    %         subplot(3,3,loop)
    %         plot(s, mean(M_x(turns(loop):turns(loop)+numavesamples,:)),'r'); hold on; plot(s, mean(M_y(turns(loop):turns(loop)+numavesamples,:)),'b'); drawlattice(-9,1); hold off
    %         title(['Turn ' num2str(turns(loop))])
    %         yaxis([-5 5])
    %         x(loop,:)=mean(M_x(turns(loop):turns(loop)+numavesamples,:));
    %         y(loop,:)=mean(M_y(turns(loop):turns(loop)+numavesamples,:));
    %     end
    %     subplot(3,3,1)
    %     legend('Horizontal', 'Vertical');
    
catch ME
    
    tango_group_command_inout(BPMs_group_id,'DDStop', 0);
    pause(0.1);
    tango_group_write_attribute(BPMs_group_id,'DDNSamples',int32(buff_size(ii)),0);
    pause(0.1);
    tango_group_command_inout(BPMs_group_id_noACQ,'DisableDDBufferFreezing', 0);
    pause(0.1);
    for ii=1:n_bpm
        tango_command_inout2(bpm_names{ii},'DDAcquire',uint8(0));
    end
    tango_group_kill(BPMs_group_id);
    tango_group_kill(BPMs_group_id_noACQ);
    fprintf('Error: %s',ME.message);
end

tango_group_kill(BPMs_group_id);
tango_group_kill(BPMs_group_id_noACQ);
toc;
end