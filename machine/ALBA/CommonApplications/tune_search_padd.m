function varargout=tune_search_padd(varargin)
% [nummax Amax mumax fft0]=tune_seach_padd(nmax,f,range)
%finds nmax relative maximums of the fft values of the vector f
%
%INPUTS
%
%  nmax: maximum number of peacks to be analized
%  f: Array with the tracking data to be annalized
%
%OUTPUTS
%
%  nummax: Array with unitary frequencies of the nmax harmonics
%  Amax: Array with the fft back transformes amplitudes of the nmax harmonics
%  mumax: Array with phases of the nmax harmonics
%
% Z.Marti  1/12/2010 (accepts some nan or zero values)


numargout = nargout;

nvar=length(varargin);

nmax=1;
if nvar==1
    f=varargin{1};
    n=length(f);
    range=[3/n (n-2)/n];
elseif nvar==2
    nmax=varargin{1};
    f=varargin{2};
    n=length(f);
    range=[3/n (n-2)/n];
elseif nvar==3
    nmax=varargin{1};
    f=varargin{2};
    range=varargin{3};
end

jj=0;
surt=0;
ampliant=norm(f);
f0=f;
while(jj<nmax &&surt==0)
    jj=jj+1;
    [nu amp mu f1]=single_peak_search(f0,range);
    ampli=norm(f1);
    Amax(jj)=amp;
    numax(jj)=nu;
    mumax(jj)=mu;
    if ampli>ampliant
        surt=1;
    end
    ampliant=ampli;
    f0=f1;
end


numax=numax(:);
Amax=Amax(:);
mumax=mumax(:);


if numargout==1
    varargout{1} = numax;
elseif numargout==2
    varargout{1} = numax;
    varargout{2} = Amax;
elseif numargout==3
    varargout{1} = numax;
    varargout{2} = Amax;
    varargout{3} = mumax;
elseif numargout==4
    varargout{1} = numax;
    varargout{2} = Amax;
    varargout{3} = mumax;
    varargout{4} = ffm;
end

end

function [nu amp mu f1]=single_peak_search(f0,range)
f=f0;
n0=numel(f0);
f0_local=f0(:)';

f=f(:);
f=f';
iszero=(f==0);
isnanu=isnan(f);
isgood=not(iszero)&not(isnanu);
ngood=sum(isgood);

bgrd0=mean(f(isgood));
f(isgood)=f(isgood)-bgrd0;
f(isgood)=f(isgood).*sin(pi*(0:(ngood-1))/ngood).*sin(pi*(0:(ngood-1))/ngood).*sin(pi*(0:(ngood-1))/ngood).*sin(pi*(0:(ngood-1))/ngood);
f(isgood)=f(isgood)-mean(f(isgood));
f(not(isgood))=0;
f_pad=[f0_local zeros(1,n0*20)];
f=[f zeros(1,n0*20)];
n=numel(f);

frq_ind_min=floor(range(1)*n)+1; 
frq_ind_max=floor(range(2)*n);
frq_ind=frq_ind_min:frq_ind_max;

ff=fft(f);
ffm=abs(ff);

v=ffm(frq_ind);
OctaviusMaximus=(v(2:(end-1))>v(1:(end-2)))&(v(2:(end-1))>v(3:end));
OctaviusMaximus=[false OctaviusMaximus false];
fr_ind_max=frq_ind(OctaviusMaximus);
[ala, imax]=max(ffm(fr_ind_max));
if sum(OctaviusMaximus)==0
    [ala, imax]=max(ffm(frq_ind));
    imax=frq_ind(imax);
    d=0;
else
    imax=fr_ind_max(imax);
    inmax=imax+1;
    ipmax=imax-1;
    ffm1=ffm(ipmax);
    ffm2=ffm(imax);
    ffm3=ffm(inmax);
    d=-(1/2+(ffm2-ffm1)/((ffm1+ffm3)-2*ffm2));
end


nu=(imax+d-1)/n;
comp=2*exp(2*pi*1i*nu*(0:(n0-1)))/ngood;
comp(not(isgood))=0;
s=sum(f0_local.*comp);
amp=abs(s);
comp(isgood)=comp(isgood).*sin(pi*(0:(ngood-1))/ngood).*sin(pi*(0:(ngood-1))/ngood).*sin(pi*(0:(ngood-1))/ngood).*sin(pi*(0:(ngood-1))/ngood);
mui=angle(ff.*conj(fft([comp zeros(1,n0*20)])));
mu=interp1(1:n,mui,imax+d);

comp=bgrd0+amp*cos(2*pi*nu*(0:(n0-1))+mu);
comp(not(isgood))=0;
f1=f0_local-comp;
%plot((0:(n-1))/n,ffm);hold all;

% n_scan=100;
% nu_vector=((inmax+((-n_scan):n_scan)+d)/n)';
% n_v=numel(nu_vector);
% comp=2*exp(2*pi*1i*nu_vector*(0:(n0-1)))/n0;
% s=sum((ones(n_v,1)*f0_local).*comp,2);
% amp=abs(s);
% mu=-angle(s);
% Df1=ones(n_v,1)*f0_local-(amp*ones(1,n0)).*cos(2*pi*nu_vector*(0:(n0-1))+(mu*ones(1,n0)));
% vf1=sqrt(sum(abs(Df1.^2),2));
% %plot(nu_vector,vf1);hold on;plot(nu,norm(f1),'ro');
% 
% [~, nu_ind]=min(vf1);
% 
% nu_nind=nu_ind+1;
% nu_pind=nu_ind-1;
% ffm1=vf1(nu_pind);
% ffm2=vf1(nu_ind);
% ffm3=vf1(nu_nind);
% dnu=(1/2+(ffm2-ffm1)/((ffm1+ffm3)-2*ffm2));
% 
% 
% nu=nu_vector(nu_ind)-dnu/n;
% comp=2*exp(2*pi*1i*nu*(0:(n0-1)))/n0;
% s=sum(f0_local.*comp);
% amp=abs(s);
% mu=-angle(s);
% f2=f0_local-amp*cos(2*pi*nu*(0:(n0-1))+mu);

%semilogy(abs(fft(f0_local)));hold all;semilogy(abs(fft(f1)));%plot(abs(fft(f2)));

end

