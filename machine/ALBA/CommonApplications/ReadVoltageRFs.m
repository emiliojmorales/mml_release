function [Voltage pow]= ReadVoltageRFs()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
Voltage=0;
Cavities=['SR06/RF/FACADE-A'; 'SR06/RF/FACADE-B'; ...
    'SR10/RF/FACADE-A'; 'SR10/RF/FACADE-B'; ...
    'SR14/RF/FACADE-A'; 'SR14/RF/FACADE-B'];
Status=[ 1 1 1 1 1 1];
Voltage=zeros(1,6);
pow=zeros(1,6);
for loop=1:6,
    if Status(loop),
        h=tango_read_attribute(Cavities(loop,:),'CAV_VOLT');
        hp=tango_read_attribute(Cavities(loop,:),'PBEAM');
        if isstruct(h)
            Voltage(loop)=h.value;
            pow(loop)=hp.value;
        end
    end
end

end

