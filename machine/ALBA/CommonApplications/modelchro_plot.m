function Chro = modelchro_plot
dd=0.03;
Nsteps=10;
de=dd*(2*((1:Nsteps)-1)-Nsteps+1)/(Nsteps-1);

for ii=1:Nsteps
    t(ii,:)=thetune(de(ii));
end

cx=polyfit(de',t(:,1),2);
cy=polyfit(de',t(:,2),2);
Chro=[cx(2) cy(2)];
plot(t(:,1),t(:,2));
hold all
end