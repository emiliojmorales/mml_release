function getBPMs_freeze(varargin)
% [M_x M_y M_I]=getBPMs_Not_freeze(varargin)


%% Parameters of the calculation
    % DDNSamples for the ACQs and DDBufferSize for non-ACQs
    %Buffersize=160000;
    % For checking the Intensity.
    %turn=2000;
    % intensity threshold for the chek
    %Imin=0.3E7;


if length(varargin)==3
    Buffersize = varargin{1};
    turn = varargin{2};
    Imin = varargin{3};
else
    Buffersize=[];
    turn=[];
    Imin=[];
end
if isempty(Buffersize)
    answer = inputdlg({'BPM buffer Size ', 'Ref turn','Intensity threshold'},'Freezed BPMs shot',1,{'2000','1000','0.05e7'});
    if isempty(answer)
        return
    end
    Buffersize = str2num(answer{1});
    turn = str2num(answer{2});
    Imin = str2num(answer{3});
end

tic;
%% Initialization

ChangeBoosterBPMsDDoffset(0);
MC=getmachineconfig();





%% Reading the BPMS

valid=false;
AO=getao();
bpm_names=AO.BPMx.DeviceName;
bpm_status=AO.BPMx.Status;
bpm_names=bpm_names(bpm_status==1);
bpm_names_noACQ=bpm_names;
n_bpm=length(bpm_names);
res=findstr('ACQ',bpm_names{1});
pos_=findstr('-',bpm_names{1});
if isempty(res)
    fprintf('The BPMs are not ACQ\n');
    for ii=1:n_bpm
        str_bpm=bpm_names_noACQ{ii};
        bpm_names{ii}=[str_bpm(1:pos_(1)) 'ACQ-' str_bpm((pos_(1)+1):end)];
    end
else
    fprintf('The BPMs are ACQ\n');
    for ii=1:n_bpm
        str_bpm=bpm_names{ii};
        bpm_names_noACQ{ii}=[str_bpm(1:pos_(1)) str_bpm((pos_(1)+5):end)];
    end
end


%Xstr=tango_read_attribute2(bpm_names{1},'XPosDD');
%X=Xstr.value;
%nturns=length(X);
M_x=zeros(Buffersize,n_bpm);
M_y=zeros(Buffersize,n_bpm);
M_I=zeros(Buffersize,n_bpm);

buff_size=zeros(n_bpm,1);
for ii=1:n_bpm
    str_bpm=tango_read_attribute2(bpm_names{ii},'DDNSamples');
    tango_command_inout2(bpm_names_noACQ{ii},'EnableDDBufferFreezing');
    pause(0.1);
    buff_size(ii)=str_bpm.value(1);
end

buff_size_readback=zeros(n_bpm,1);
Trigger_counter_before=zeros(n_bpm,1);
Trigger_counter_after=zeros(n_bpm,1);
try
    
    for ii=1:n_bpm
        tango_command_inout2(bpm_names{ii},'DDStop');
        pause(0.1);
        tango_write_attribute2(bpm_names{ii},'DDNSamples', int32(Buffersize));
        tango_command_inout2(bpm_names_noACQ{ii},'EnableDDBufferFreezing');
        pause(0.1);
        %tango_command_inout2(bpm_names{ii},'DDAcquire',uint8(0));
    end
    
    
    for ii=1:n_bpm
        buffer_read=tango_read_attribute2(bpm_names_noACQ{ii},'DDBufferSize');
        buff_size_readback(ii)=buffer_read.value(1);
    end
    if sum(buff_size_readback-Buffersize)>0
        figure;plot(buff_size_readback-Buffersize,'-ro');
        xlabel('BPM num');
        ylabel('DDBufferSize(read)-DDNSamples(write)');
    end

    
    Itestii=zeros(n_bpm,1);
    while(valid==false)
        for ii=1:n_bpm
            tmp=tango_read_attribute2(bpm_names_noACQ{ii},'DDTriggerCounter');
            Trigger_counter_before(ii)=tmp.value;
        end
        for ii=1:n_bpm
            tango_command_inout2(bpm_names_noACQ{ii},'UnfreezeDDBuffer');
        end
        for ii=1:n_bpm
            tmp=tango_read_attribute2(bpm_names_noACQ{ii},'DDTriggerCounter');
            Trigger_counter_after(ii)=tmp.value;
        end
        ntime=2;
        fprintf('Waiting.');
        for ii=1:ntime
            pause(0.33);
            fprintf('.');
            pause(0.33);
            fprintf('.');
            pause(0.33);
            fprintf('.%d',ii);
        end
        fprintf('\n');
        
    
        tunesx=zeros(n_bpm);
        for ii=1:n_bpm
            fprintf('Reading BPM num %d\n',ii);
            bpm=bpm_names_noACQ{ii};
            try
                Xstr=tango_read_attribute2(bpm,'XPosDD');
                Ystr=tango_read_attribute2(bpm,'ZPosDD');
                Istr=tango_read_attribute2(bpm,'SumDD');
                X=Xstr.value;
                Y=Ystr.value;
                I=Istr.value;
                tunesx(ii)=tune_seach_padd(1,X(500:end),[0.1 0.5]);
                M_x(:,ii)=X';
                M_y(:,ii)=Y';
                M_I(:,ii)=I';
                Itestii(ii)=M_I(turn, ii);
            catch ME1
                fprintf('Efective buffer size is: %d\n',numel(X));
                X=[];
                fprintf('Error: %s\n',ME1.message);
                Itestii(ii)=-1;
            end
        end
        valid=true;
        tunesx_mean=mean(tunesx);
        for ii=1:n_bpm
            if abs(tunesx(ii)-tunesx_mean)>0.0001
                fprintf('tunesx(%d)=%f(mean:%f), Badly synchronized  BPM nn %d\n',ii,tunesx(ii),tunesx_mean,ii)
                valid=false;
            end
            if (Itestii(ii) >= Imin),
                fprintf('Itest=%f, Adquired BPM nn %d\n',Itestii(ii),ii)
            else
                fprintf('Itest=%f, Bad  BPM nn %d\n',Itestii(ii),ii)
                valid=false;
            end
        end

        pause(0.1);
    end
    
    
    %% Back to previous BPM state
    
    for ii=1:n_bpm
        bpm=bpm_names{ii};
        tango_command_inout2(bpm_names{ii},'DDStop');
        pause(0.1);
        tango_write_attribute2(bpm,'DDNSamples', int32(buff_size(ii)));
        tango_command_inout2(bpm_names_noACQ{ii},'DisableDDBufferFreezing');
        pause(0.1);
        tango_command_inout2(bpm_names{ii},'DDAcquire',uint8(0)); 
        
    end
    
    %daydir=['/data/MML/WorkDirectory/zeus/BPMs/',datestr(now,'yyyymmdd')];
    daydir=['/data/BOOSTER/',datestr(now,'yyyymmdd'),'/BPMs_freezed'];
    mkdir(daydir);
    cd(daydir);
    name0=['BPM_freezed_' datestr(now, 'yyyymmddTHHMMSS') ] ;
    name=[name0 '.mat'];
    save(name,'M_x','M_y','M_I','MC','Trigger_counter_before','Trigger_counter_after');
    
%     figure;
%     s=getspos('BPMx');
%     turns=5:floor(Buffersize/9):Buffersize;
%     numavesamples=min([Buffersize-turns(end) 500]);
%     for loop=1:9
%         subplot(3,3,loop)
%         plot(s, mean(M_x(turns(loop):turns(loop)+numavesamples,:)),'r'); hold on; plot(s, mean(M_y(turns(loop):turns(loop)+numavesamples,:)),'b'); drawlattice(-9,1); hold off
%         title(['Turn ' num2str(turns(loop))])
%         yaxis([-5 5])
%         x(loop,:)=mean(M_x(turns(loop):turns(loop)+numavesamples,:));
%         y(loop,:)=mean(M_y(turns(loop):turns(loop)+numavesamples,:));
%     end
%     subplot(3,3,1)
%     legend('Horizontal', 'Vertical');

catch ME
    
    for ii=1:n_bpm
        bpm=bpm_names{ii};
        tango_command_inout2(bpm_names{ii},'DDStop');
        pause(0.1);
        tango_write_attribute2(bpm,'DDNSamples', int32(buff_size(ii)));
        tango_command_inout2(bpm_names_noACQ{ii},'DisableDDBufferFreezing');
        pause(0.1);
        tango_command_inout2(bpm_names{ii},'DDAcquire',uint8(0)); 
        
    end
    fprintf('Error: %s',ME.message);
end

toc;
end