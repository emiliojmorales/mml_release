
function cycling_all_tango_group
% 2011 Created by Z.Marti,


[s,v] = listdlg('PromptString','Select Fimilies to cycle:','SelectionMode','single','ListString',{'QUAD','SEXT','BEND','QUAD+SEXT','BEND+SEXT','All'});
if isempty(s)
    fprintf('No action executed.\n');
else
    
    if s==1
        fam=findmemberof('QUAD');
        min=20*ones(1,numel(fam));
        max=200*ones(1,numel(fam));
    elseif s==2
        fam=findmemberof('SEXT');
        min=25*ones(1,numel(fam));
        max=215*ones(1,numel(fam));
    elseif s==3
        fam={'BEND'};
        min=35*ones(1,numel(fam));
        max=600*ones(1,numel(fam));
    elseif s==4
        q=findmemberof('QUAD');
        s=findmemberof('SEXT');
        minq=20*ones(1,numel(q));
        maxq=200*ones(1,numel(q));
        mins=25*ones(1,numel(s));
        maxs=215*ones(1,numel(s));
        min=[minq,mins];
        max=[maxq,maxs];
        fam={q{:},s{:}}';
    elseif s==5
        s=findmemberof('SEXT');
        mins=25*ones(1,numel(s));
        maxs=215*ones(1,numel(s));
        min=[mins,35];
        max=[maxs,600];
        fam={s{:},'BEND'}';
    elseif s==6
        q=findmemberof('QUAD');
        s=findmemberof('SEXT');
        minq=20*ones(1,numel(q));
        maxq=200*ones(1,numel(q));
        mins=25*ones(1,numel(s));
        maxs=215*ones(1,numel(s));
        min=[minq,mins,35];
        max=[maxq,maxs,600];
        fam={q{:},s{:},'BEND'}';
    end
    
    
    MAGNETS_group_id = tango_group_create('MAGNETS');
    
    try
        nfam=numel(fam);
        tangoscell={};
        minall=[];
        maxall=[];
        for jj=1:nfam
            tangos=family2tangodev(fam{jj});
            tangoscell=cat(1,tangoscell(:),tangos(:));
            minall=[minall min(jj)*ones(1,numel(tangos))];
            maxall=[maxall max(jj)*ones(1,numel(tangos))];
        end
        nquads=numel(tangoscell);
        for ii =1:nquads
            tango_group_add(MAGNETS_group_id, tangoscell{ii});
        end
        cycling_tango_group(MAGNETS_group_id,minall,maxall);
    catch meass
        tango_group_kill(MAGNETS_group_id);
    end
    tango_group_kill(MAGNETS_group_id);
    
end

end

function cycling_tango_group(MAGNETS_group_id, Min_Current,Max_Current)


if isempty(MAGNETS_group_id)
    error('three inputs needed');
end
Max_total_Current=max(Max_Current);
Min_total_Current=max(Min_Current);
time_wait=(Max_total_Current-Min_total_Current)/10+20;
time_wait_small=0.2;
fprintf('Theoretical stopping time at each current: %f\n',time_wait);


nq=numel(Max_Current);
q0=zeros(1,nq);
XTango = tango_group_read_attribute(MAGNETS_group_id,'CurrentSetpoint',0);
if XTango.has_failed
    q0=[];
    error('Tango group reading failed...');
else
    for ii =1:nq
        q0(ii)=XTango.replies(ii).value(1);
    end
end


n_cycle=3;

for ii=1:(n_cycle-1)
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));

    pause(time_wait_small);
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));

    pause(time_wait_small);
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));

    % wait
    pause(time_wait);
    %
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Min_Current));
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Min_Current));

    pause(time_wait_small);
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Min_Current));
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Min_Current));

    pause(time_wait_small);
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Min_Current));
    tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Min_Current));

    % wait
    pause(time_wait);
    fprintf('Cycle num %d/%d finished. \n',ii,n_cycle);
end

tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));
pause(time_wait_small);
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));
pause(time_wait_small);
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(Max_Current));
% wait
pause(time_wait);
%
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(q0));
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(q0));
pause(time_wait_small);
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(q0));
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(q0));
pause(time_wait_small);
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(q0));
tango_group_write_attribute(MAGNETS_group_id,'CurrentSetpoint',0,double(q0));
fprintf('Cycle num %d/%d finished. \n',ii+1,n_cycle);
end


