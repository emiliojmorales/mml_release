function varargout = sofbALBA(varargin)
% SOFBALBA M-file for sofbALBA.fig
%      SOFBALBA, by itself, creates a new SOFBALBA or raises the existing
%      singleton*.
%
%      H = SOFBALBA returns the handle to a new SOFBALBA or the handle to
%      the existing singleton*.
%
%      SOFBALBA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SOFBALBA.M with the given input arguments.
%
%      SOFBALBA('Property','Value',...) creates a new SOFBALBA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sofbALBA_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sofbALBA_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sofbALBA

% Last Modified by GUIDE v2.5 18-Nov-2011 08:44:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @sofbALBA_OpeningFcn, ...
    'gui_OutputFcn',  @sofbALBA_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sofbALBA is made visible.
function sofbALBA_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sofbALBA (see VARARGIN)

% Choose default command line output for sofbALBA
handles.output = hObject;
set(handles.stop,'Enable','Off')
ao=getao;

R=getbpmresp('Struct');
handles.Imin=4;
handles.X.R=R(1,1).Data;
handles.Y.R=R(2,2).Data;
handles.X.Golden=getgolden('BPMx','Monitor');
handles.Y.Golden=getgolden('BPMy','Monitor');
handles.X.BPMid=ao.BPMx.GroupId;
handles.X.CMid=ao.HCM.GroupId;
handles.Y.BPMid=ao.BPMx.GroupId;
handles.Y.CMid=ao.VCM.GroupId;

nsvdx=min([sum(ao.BPMx.Status) sum(ao.HCM.Status)]);
nsvdy=min([sum(ao.BPMy.Status) sum(ao.VCM.Status)]);
set(handles.svdX,'String',num2str(nsvdx));
set(handles.svdY,'String',num2str(nsvdy));
handles.X.svd=nsvdx;
handles.X.alpha=str2double(get(handles.alphaX,'String'));
handles.X.reg=get(handles.regX,'Value');
handles.X.gain=str2double(get(handles.gainX,'String'));
handles.X.svdmax=min(size(handles.X.R));

handles.Y.svd=nsvdy;
handles.Y.alpha=str2double(get(handles.alphaY,'String'));
handles.Y.reg=get(handles.regY,'Value');
handles.Y.gain=str2double(get(handles.gainY,'String'));
handles.Y.svdmax=min(size(handles.Y.R));
handles.atr(1).name='rmsX';
handles.atr(2).name='rmsY';
handles.atr(3).name='rmsHCM';
handles.atr(4).name='rmsVCM';

handles.atr(1).value=0.0;
handles.atr(2).value=0.0;
handles.atr(3).value=0.0;
handles.atr(4).value=0.0;

handles.error={'Error log'};



guidata(hObject, handles);
update_Plots(hObject);


% Update handles structure


% UIWAIT makes sofbALBA wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = sofbALBA_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function svdX_Callback(hObject, eventdata, handles)
% hObject    handle to svdX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of svdX as text
%        str2double(get(hObject,'String')) returns contents of svdX as a double
if isnan(str2double(get(handles.svdX,'String'))) || str2double(get(handles.svdX,'String')) > handles.X.svdmax ...
        || str2double(get(handles.svdX,'String')) < 0 ,
    set(handles.svdX,'String',handles.X.svdmax)
end

update_Plot(hObject, 1)

% --- Executes during object creation, after setting all properties.
function svdX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to svdX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function alphaX_Callback(hObject, eventdata, handles)
% hObject    handle to alphaX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of alphaX as text
%        str2double(get(hObject,'String')) returns contents of alphaX as a double
update_Plot(hObject, 1)

% --- Executes during object creation, after setting all properties.
function alphaX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alphaX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in regX.
function regX_Callback(hObject, eventdata, handles)
% hObject    handle to regX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of regX
update_Plot(hObject, 1)


function gainX_Callback(hObject, eventdata, handles)
% hObject    handle to gainX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gainX as text
%        str2double(get(hObject,'String')) returns contents of gainX as a double


% --- Executes during object creation, after setting all properties.
function gainX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gainX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rate_Callback(hObject, eventdata, handles)
% hObject    handle to rate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rate as text
%        str2double(get(hObject,'String')) returns contents of rate as a double


% --- Executes during object creation, after setting all properties.
function rate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start.
function start_Callback(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%the timer

% find the old timer
fprintf(1,'Deleting the old timers\n');
tlist=timerfindall('Tag','sofb');
delete(tlist);

%reload the golden orbit

fprintf(1,'Creating a new timer\n');
handles.timer=timer('TimerFcn',{@CorrectOrbit,hObject}, 'Period', 1,'StartDelay',1, ...
    'ExecutionMode','fixedRate', 'TasksToExecute',Inf,'StartFcn',{@StartTimerSettings,hObject},...
    'StopFcn',{@StopTimerSettings,hObject},'Tag','sofb');
set(handles.timer,'Period', str2num(get(handles.rate,'String')));
guidata(hObject, handles);
fprintf(1,'Starting the timer\n');
start(handles.timer)

% --- Executes on button press in stop.
function stop_Callback(hObject, eventdata, handles)
% hObject    handle to stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stop(handles.timer)
delete(handles.timer)




function svdY_Callback(hObject, eventdata, handles)
% hObject    handle to svdY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of svdY as text
%        str2double(get(hObject,'String')) returns contents of svdY as a double
if isnan(str2double(get(handles.svdY,'String'))) || str2double(get(handles.svdY,'String')) > handles.Y.svdmax ...
        || str2double(get(handles.svdY,'String')) < 0 ,
    set(handles.svdY,'String',handles.Y.svdmax)
end
update_Plot(hObject, 2)

% --- Executes during object creation, after setting all properties.
function svdY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to svdY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function alphaY_Callback(hObject, eventdata, handles)
% hObject    handle to alphaY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of alphaY as text
%        str2double(get(hObject,'String')) returns contents of alphaY as a double
update_Plot(hObject, 2)

% --- Executes during object creation, after setting all properties.
function alphaY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alphaY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in regY.
function regY_Callback(hObject, eventdata, handles)
% hObject    handle to regY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of regY
update_Plot(hObject, 2)


function gainY_Callback(hObject, eventdata, handles)
% hObject    handle to gainY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gainY as text
%        str2double(get(hObject,'String')) returns contents of gainY as a double


% --- Executes during object creation, after setting all properties.
function gainY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gainY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function update_Plots(hObject);
%
%
update_Plot(hObject, 1);
update_Plot(hObject, 2);

function update_Plot(hObject, Plane);
%
handles=guidata(hObject);
plane='X';
if Plane==2; plane='Y'; end;
s1=['svd' plane];
s2=['alpha' plane];
s3=['reg' plane];
s4=['plot' plane];

handles.(plane).svd=str2double(get(handles.(s1),'String'));
handles.(plane).alpha=str2double(get(handles.(s2),'String'));
handles.(plane).reg=get(handles.(s3),'Value');
[handles.(plane).rinv handles.(plane).w handles.(plane).w0 handles.(plane).svddata]=...
    CalcInvMatrix(handles.(plane).R,handles.(plane).svd ,handles.(plane).reg*handles.(plane).alpha );
plot(handles.(s4), handles.(plane).w,'b+')
set(handles.(s4),'YScale','Log')
set(handles.(s4),'XTick',[0:8:90])
hold2(handles.(s4),'on')
plot(handles.(s4), handles.(plane).w0,'r')
hold2(handles.(s4),'off')
axis(handles.(s4),'tight')
guidata(hObject,handles);
set(handles.msgtext,'String', {datestr(now); ['Updated SVD matrix in plane ' plane]})



function [rinv w w0 svddata]=CalcInvMatrix(R, nsvd, alphaR);
[U,S,V] = svd(R);
Si=0*S';
w0 = diag(S);
w=(w0.*w0+alphaR^2)./w0;
w(nsvd+1:end)=0;
for loop=1:nsvd,
    Si(loop,loop)=1/w(loop);
end
svddata.U=U;
svddata.V=V;
svddata.S=S;
svddata.Si=Si;
rinv=V*Si*U';


% --- Executes on button press in enableY.
function enableY_Callback(hObject, eventdata, handles)
% hObject    handle to enableY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of enableY


% --- Executes on button press in enableX.
function enableX_Callback(hObject, eventdata, handles)
% hObject    handle to enableX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of enableX
function StartTimerSettings(obj, event,  hObject)
handles=guidata(hObject);
set(handles.stop,'Enable','on')
set(handles.alive,'Enable','on')
set(handles.start,'Enable','off')
handles.X.gain=str2double(get(handles.gainX,'String'))/100;
handles.Y.gain=str2double(get(handles.gainY,'String'))/100;
handles.X.Enable=get(handles.enableX,'Value');
handles.Y.Enable=get(handles.enableY,'Value');

ao=getao;
handles.X.Golden=getgolden('BPMx','Monitor');
handles.Y.Golden=getgolden('BPMy','Monitor');
handles.X.BPMid=ao.BPMx.GroupId;
handles.X.CMid=ao.HCM.GroupId;
handles.Y.BPMid=ao.BPMx.GroupId;
handles.Y.CMid=ao.VCM.GroupId;

disp 'Using 18 samples'
handles.SAaverages=18
disp 'For average'
replies = tango_group_write_attribute(ao.BPMx.GroupId, 'SAStatNumSamples',0,int32(handles.SAaverages));


handles.Errors=0;
handles.CMLimit=1;

guidata(hObject,handles);

disp 'Start Timing'


function StopTimerSettings(obj, event,  hObject)
handles=guidata(hObject);
set(handles.stop,'Enable','off')
set(handles.alive,'Enable','off')
set(handles.start,'Enable','on')
set(handles.alive,'BackgroundColor',[0.8 0.8 0.8])
disp 'Stop Timing'

function CorrectOrbit(obj, event,  hObject)
handles=guidata(hObject);
msg={['Time: ' datestr(now) ]};

I=getdcct;
readOk=0;
if handles.Errors <5;
    if I > handles.Imin,
        try
            Xread=tango_group_read_attribute(handles.X.BPMid,'XMeanPosSA',1);
            HCMread=tango_group_read_attribute(handles.X.CMid,'Current',1);
            Yread=tango_group_read_attribute(handles.Y.BPMid,'ZMeanPosSA',1);
            VCMread=tango_group_read_attribute(handles.Y.CMid,'Current',1);
            readOk=0;
            handles.error={handles.error; ['Time: ' datestr(now) ];'Error in the data'};
            
            if ~(Xread.has_failed | Yread.has_failed | HCMread.has_failed | VCMread.has_failed),
                X=cat(1,Xread.replies.value);
                HCM=cat(1,HCMread.replies.value);
                Y=cat(1,Yread.replies.value);
                VCM=cat(1,VCMread.replies.value);
                if ~( sum(isnan(X)) | sum(isnan(Y)) | sum(isnan(HCM)) |sum(isnan(VCM)) )
                    readOk=1;
                end
            end
        catch
            readOk=0;
        end
        if readOk,
            mycheck=0; 
            if handles.X.Enable,
                
                if sum(isnan(X)),
                    msg{end+1}='Problem getting the data. Stoping'
                    stop(handles.timer);
                else
                    DeltaX=X-handles.X.Golden;
                    dch=-handles.X.gain*handles.X.rinv*DeltaX;
                    if (dch (dch > handles.CMLimit)),
                        handles.Errors=handles.Errors+1;
                        handles.error={'Last Error:' ;['Time: ' datestr(now) ];'HCM limit exceed';[num2str(handles.Errors) ' in a row']};
                        set(handles.errorlog,'String', handles.error);
                    else
                        ch=HCM+dch;
                        msg{end+1}=['RMS(x)= ', num2str(1e3*std(DeltaX),'%5.3f') ' um'];
                        msg{end+1}=['RMS(DHCM)= ' num2str(1e3*std(dch),'%5.3f') ' mA'];
                        tango_group_write_attribute(handles.X.CMid,'CurrentSetpoint',0, double(ch'));
                        mycheck=1;
                    end
                end
            end
            if handles.Y.Enable,
                if sum(isnan(Y)),
                    msg{end+1}='Problem getting the data. Stoping';
                    stop(handles.timer);
                else
                    DeltaY=Y-handles.Y.Golden;
                    dcv=-handles.Y.gain*handles.Y.rinv*DeltaY;
                    if (dcv (dcv > handles.CMLimit)),
                        handles.Errors=handles.Errors+1;
                        handles.error={'Last Error:' ;['Time: ' datestr(now) ];'VCM limit exceed';[num2str(handles.Errors) ' in a row']};
                        set(handles.errorlog,'String', handles.error);
                    else
                        cv=VCM+dcv;
                        msg{end+1}=['RMS(y)= ', num2str(1e3*std(DeltaY),'%5.3f') ' um'];
                        msg{end+1}=['RMS(DVCM)= ' num2str(1e3*std(dcv),'%5.3f') ' mA'];
                        tango_group_write_attribute(handles.Y.CMid,'CurrentSetpoint',0, double(cv'));
                        if mycheck,
                            handles.Errors=0;
                        end
                    end
                end
            end
            handles.atr(1).value=std(DeltaX);
            handles.atr(2).value=std(DeltaY);
            handles.atr(3).value=std(dch);
            handles.atr(4).value=std(dcv);
            tango_write_attributes('sr/bd/sofb', handles.atr);
            
            set(handles.alive,'BackgroundColor','g')
            set(handles.alive,'String',{[];'Yes, correcting'})
            pause(0.2)
            set(handles.alive,'BackgroundColor',[0.5 0.7 0.5])
            set(handles.alive,'String',{[];'Am I alive?'});
        else
            handles.Errors=handles.Errors+1;
            
            handles.error={'Last Error:' ;['Time: ' datestr(now) ];'Error getting the data';[num2str(handles.Errors) ' in a row']};
            set(handles.errorlog,'String', handles.error);
            set(handles.alive,'BackgroundColor',[0.7 0.1 0.1])
            set(handles.alive,'String',{[];'Yes, but with errors'})
            pause(0.2)
            set(handles.alive,'BackgroundColor',[0.5 0.7 0.5])
            set(handles.alive,'String',{[];'Am I alive?'});
        end
        
        
        guidata(hObject,handles);
        
    else
        
        handles.error={['Time: ' datestr(now) ];'Current too low';'Refill and restart'};
        set(handles.errorlog,'String', handles.error);
        
        guidata(hObject,handles);
        
        stop(handles.timer);
    end
else
    handles.error={['Time: ' datestr(now) ];'Too many consequtive errors';'Check the machine'};
    set(handles.errorlog,'String', handles.error);
    
    guidata(hObject,handles);
    
    stop(handles.timer);
    
end
set(handles.msgtext,'String', msg);




function errorlog_Callback(hObject, eventdata, handles)
% hObject    handle to errorlog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of errorlog as text
%        str2double(get(hObject,'String')) returns contents of errorlog as a double


% --- Executes during object creation, after setting all properties.
function errorlog_CreateFcn(hObject, eventdata, handles)
% hObject    handle to errorlog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
