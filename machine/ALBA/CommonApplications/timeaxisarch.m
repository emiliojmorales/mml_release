function [ output_args ] = timeaxisarch(varargin )
% To set the x axis label from the epoch values used by the archiver
% to the format DD-MM-YY
% vargin :
% AX to use, by default gca
% steps to use, by default 12
% format of the date, by default dd/MM/yy

    AX=gca;
    steps=12;
    dformat='dd/MM/yy';
if nargin>0,
    AX=varargin{1};
end
if nargin >1
    steps=varargin{2};
end
if nargin > 2;
    dformat=varargin{3};
end
   

limits=axis(AX);
set(AX, 'XTick', limits(1):(limits(2)-limits(1))/steps:limits(2))
xtick=get(AX,'XTick');
xticklabel=[];
for loop=1:length(xtick),
    xticklabel{loop}=epoch2date(xtick(loop), dformat); 
end
set(AX,'XTickLabe',xticklabel)
end

