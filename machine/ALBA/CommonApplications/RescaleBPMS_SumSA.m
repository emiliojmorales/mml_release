function M_new=RescaleBPMS_SumSA(M0)
not_isnant=not(isnan(M0(:,1)));
M=M0(not_isnant,:);
n=size(M);
nbpm=n(1);
nmeas=n(2);

D=M-[M(:,1) M(:,1:(end-1))];
D(isnan(D))=0;
mea_D=5*mean(mean(abs(D)));

off_shift=abs(D)>mea_D;
ind=reshape(1:(nbpm*nmeas),nbpm,nmeas);
ind_ant=ind(:,[1 1:(end-1)]);
fact0=M(ind(off_shift))./M(ind_ant(off_shift));

Prod=ones(nbpm,nmeas);
Prod(off_shift)=1./fact0;
CumProd=cumprod(Prod,2);
M_new=CumProd.*M;
end