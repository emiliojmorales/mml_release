function setlocoquads
%% G.B. 2012
DirStart = [getfamilydata('Directory', 'DataRoot'), 'LOCO', filesep];
[filename, LocoDirectory] = uigetfile('*.mat','Select a locodata file',DirStart);

load([LocoDirectory filename], 'FitParameters');

%%
Kquadval0=cell2mat(getpv(findmemberof('QUAD'),'Physics'));
Iquadval0=cell2mat(getpv(findmemberof('QUAD'),'Hardware'));
quadNumbers=[8 8 8 8 8 8 8 8 8 8 8 8 8 8];
%quadNumbers=8*ones(1,14);
iter=length(FitParameters);

%%
figure
for loop=1:14,
    quaindex=(loop*8-7:loop*8);
    dkFitRelative=-(FitParameters(iter).Values(quaindex)-FitParameters(1).Values(quaindex))./FitParameters(1).Values(quaindex);
    dCurrents=dkFitRelative.*Iquadval0(quaindex);
    stem(quaindex,dCurrents)
    xlim([0 113])
    % ylim([-1. 1.])
    hold all
end

set(gca,'XTick',0:10:112)
set(gca,'FontSize',12)
xlabel('quad number','FontSize',14)
ylabel('current change [A]','FontSize',14)
legend(findmemberof('QUAD'),'Location','EastOutside')
title(['LOCO iteration ',num2str(iter-1),': fit by quad power supplies'],'FontSize',12)
hold off

%%
dkFit=FitParameters(iter).Values-FitParameters(1).Values;
dkFitRelative=(FitParameters(iter).Values-FitParameters(1).Values)./FitParameters(1).Values;
FactorCorrection=1-dkFitRelative(1:112);
NewCurrents=FactorCorrection.*Iquadval0;
NewCurrents=mat2cell(NewCurrents,quadNumbers);

%%
toler = 1e-3;
diff=1;
loop = 1;

while diff > toler && loop <= 3,
    setsp(findmemberof('QUAD'), NewCurrents);
    pause(5)
    diff=max(abs(cell2mat(getsp(findmemberof('QUAD'),'Hardware'))-cell2mat(NewCurrents)));
    loop = loop+1;
end

%%
disp(' ')
disp('Applied Loco changes to the Quadrupoles')
disp(['Max Quad diff. : ', num2str(diff), ' A'])
disp(' ')
