function ys=smooth_fit(x,y,nsm,order)
warning off;
ys=y;
nn=numel(x);
nfit=nn-nsm+1;
nh=floor(nsm/2);

% nf=(1+(0:(nsm-1)));
% c=polyfit(x(nf),y(nf),order);
% y_fit=polyval(c,x(nf));
% ys(1:nh)=y_fit(1:nh);
%     
    plot(x,y,'bo');hold all;
for jj=1:nfit
    nf=(jj+(0:(nsm-1)));
    c=polyfit(x(nf),y(nf),order);
    ys(jj+nh)=polyval(c,x(jj+nh));
    %plot(x(nf),polyval(c,x(nf)),'-')
end

% nf=(nfit+(0:(nsm-1)));
% c=polyfit(x(nf),y(nf),order);
% y_fit=polyval(c,x(nf));
% ys((end-nh+1):end)=y_fit((end-nh+1):end);
plot(x,ys,'-r');


end