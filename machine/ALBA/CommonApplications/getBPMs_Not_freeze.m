function getBPMs_Not_freeze(varargin)
% [M_x M_y M_I]=getBPMs_Not_freeze(varargin)


%% Parameters of the calculation
    % DDNSamples for the ACQs and DDBufferSize for non-ACQs
    %Buffersize=160000;
    % For checking the Intensity.
    %turn=2000;
    % intensity threshold for the chek
    %Imin=0.3E7;


if length(varargin)==3
    Buffersize = varargin{1};
    turn = varargin{2};
    Imin = varargin{3};
else
    Buffersize=[];
    turn=[];
    Imin=[];
end
if isempty(Buffersize)
    answer = inputdlg({'BPM buffer Size ', 'Ref turn','Intensity threshold'},'BPMs during Ramp',1,{'180000','2000','0.3e7'});
    if isempty(answer)
        return
    end
    Buffersize = str2num(answer{1});
    turn = str2num(answer{2});
    Imin = str2num(answer{3});
end

tic;
%% Initialization

ChangeBoosterBPMsDDoffset(0);
MC=getmachineconfig();





%% Reading the BPMS

valid=false;
AO=getao();
bpm_names=AO.BPMx.DeviceName;
bpm_status=AO.BPMx.Status;
bpm_names=bpm_names(bpm_status==1);
bpm_names_noACQ=bpm_names;
n_bpm=length(bpm_names);
res=findstr('ACQ',bpm_names{1});
pos_=findstr('-',bpm_names{1});
if isempty(res)
    fprintf('The BPMs are not ACQ\n');
    for ii=1:n_bpm
        str_bpm=bpm_names_noACQ{ii};
        bpm_names{ii}=[str_bpm(1:pos_(1)) 'ACQ-' str_bpm((pos_(1)+1):end)];
    end
else
    fprintf('The BPMs are ACQ\n');
    for ii=1:n_bpm
        str_bpm=bpm_names{ii};
        bpm_names_noACQ{ii}=[str_bpm(1:pos_(1)) str_bpm((pos_(1)+5):end)];
    end
end


%Xstr=tango_read_attribute2(bpm_names{1},'XPosDD');
%X=Xstr.value;
%nturns=length(X);
M_x=zeros(Buffersize,n_bpm);
M_y=zeros(Buffersize,n_bpm);
M_I=zeros(Buffersize,n_bpm);

buff_size=zeros(n_bpm,1);
for ii=1:n_bpm
    str_bpm=tango_read_attribute2(bpm_names{ii},'DDNSamples');
    tango_command_inout2(bpm_names_noACQ{ii},'DisableDDBufferFreezing');
    pause(0.1);
    buff_size(ii)=str_bpm.value(1);
end

buff_size_readback=zeros(n_bpm,1);
try
    
    for ii=1:n_bpm
        tango_command_inout2(bpm_names{ii},'DDStop');
        pause(0.1);
        tango_write_attribute2(bpm_names{ii},'DDNSamples', int32(Buffersize));
        pause(0.1);
        tango_command_inout2(bpm_names{ii},'DDAcquire',int8(0)); 
    end
    
    
    for ii=1:n_bpm
        buffer_read=tango_read_attribute2(bpm_names_noACQ{ii},'DDBufferSize');
        buff_size_readback(ii)=buffer_read.value(1);
    end
    figure;plot(buff_size_readback-Buffersize,'-ro');
    xlabel('BPM num');
    ylabel('DDBufferSize(read)-DDNSamples(write)');
    
    ntime=5;
    fprintf('Waiting.');
    for ii=1:ntime
        pause(0.33);
        fprintf('.');
        pause(0.33);
        fprintf('.');
        pause(0.33);
        fprintf('.%d',ii);
    end
    fprintf('\n');
        
    for ii=1:n_bpm
        valid=false;
        while(valid==false),
            fprintf('Reading BPM num %d\n',ii);
            bpm=bpm_names_noACQ{ii};
            
            try
                Xstr=tango_read_attribute2(bpm,'XPosDD');
                Ystr=tango_read_attribute2(bpm,'ZPosDD');
                Istr=tango_read_attribute2(bpm,'SumDD');
                X=Xstr.value;
                Y=Ystr.value;
                I=Istr.value;
                M_x(:,ii)=X';
                M_y(:,ii)=Y';
                M_I(:,ii)=I';
                Itest=M_I(turn, ii);
            catch ME1
                fprintf('Efective buffer size is: %d\n',numel(X));
                X=[];
                fprintf('Error: %s\n',ME1.message);
                Itest=-1;
            end

            if (Itest >= Imin),
                valid=true;
                fprintf('Itest=%f, Adquired BPM nn %d\n',Itest,ii)
            else
                fprintf('Itest=%f, Bad shot, BPM nn %d\n',Itest,ii)
                pause(1);
            end
            
        end
        pause(0.1);
    end
    
    
    %% Back to previous BPM state
    
    for ii=1:n_bpm
        bpm=bpm_names{ii};
        tango_command_inout2(bpm_names{ii},'DDStop');
        pause(0.1);
        tango_write_attribute2(bpm,'DDNSamples', int32(buff_size(ii)));
        pause(0.1);
        tango_command_inout2(bpm_names{ii},'DDAcquire',int8(0)); 
    end
    
    %daydir=['/data/MML/WorkDirectory/zeus/BPMs/',datestr(now,'yyyymmdd')];
    daydir=['/data//BOOSTER/',datestr(now,'yyyymmdd')];
    mkdir(daydir);
    cd(daydir);
    name0=['BPM_' datestr(now, 'yyyymmdd') ] ;
    name=[name0 '.mat'];
    save(name,'M_x','M_y','M_I','MC');
    
    figure;
    s=getspos('BPMx');
    turns=5:floor(Buffersize/9):Buffersize;
    numavesamples=min([Buffersize-turns(end) 500]);
    for loop=1:9,
        subplot(3,3,loop)
        plot(s, mean(M_x(turns(loop):turns(loop)+numavesamples,:)),'r'); hold on; plot(s, mean(M_y(turns(loop):turns(loop)+numavesamples,:)),'b'); drawlattice(-9,1); hold off
        title(['Turn ' num2str(turns(loop))])
        yaxis([-5 5])
        x(loop,:)=mean(M_x(turns(loop):turns(loop)+numavesamples,:));
        y(loop,:)=mean(M_y(turns(loop):turns(loop)+numavesamples,:));
    end
    subplot(3,3,1)
    legend('Horizontal', 'Vertical');

catch ME
    
    for ii=1:n_bpm
        bpm=bpm_names{ii};
        tango_command_inout2(bpm_names{ii},'DDStop');
        pause(0.1);
        tango_write_attribute2(bpm,'DDNSamples', int32(buff_size(ii)));
        pause(0.1);
        tango_command_inout2(bpm_names{ii},'DDAcquire',int8(0)); 
    end
    fprintf('Error: %s',ME.message);
end

toc;
end