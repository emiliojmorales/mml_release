%% all QP
qnames=findmemberof('QUAD');
qmin=20*ones(numel(qnames),1);
qmax=200*ones(numel(qnames),1);
% qmax(4) = 180;
% qmax([7 10]) = [225; 225];
cycling_2(qnames,num2cell(qmin),num2cell(qmax));

% %% not QH06
% qnames=findmemberof('QUAD');
% qnames=qnames([1:5 7:14]);
% qmin=20*ones(numel(qnames),1);
% qmax=200*ones(numel(qnames),1);
% qmax(9) = 225;
% cycling_2(qnames,num2cell(qmin),num2cell(qmax));

%% all SP
snames=findmemberof('SEXT');
smin=30*ones(numel(snames),1);
smax=215*ones(numel(snames),1);
cycling_2(snames,num2cell(smin),num2cell(smax));

%% Bend
bnames=findmemberof('BEND');
bmin=35*ones(numel(bnames),1);
bmax=600*ones(numel(bnames),1);
cycling_2(bnames,num2cell(bmin),num2cell(bmax));
% %% only QH01 QH02 QH03
% qnames=findmemberof('QUAD');
% qnames=qnames(1:3);
% qmin=20*ones(numel(qnames),1);
% qmax=200*ones(numel(qnames),1);
% cycling_2(qnames,num2cell(qmin),num2cell(qmax));
