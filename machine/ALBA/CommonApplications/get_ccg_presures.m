function atribute_values=get_ccg_presures

ccg_sectors=[3 4 4 4 2 5 3 4 3 6 3 4 3 6 3 4];
nsr=16;
kk=0;
for ii=1:nsr
    for jj=1:ccg_sectors(ii)
        kk=kk+1;
        if ii<10
            names_ccg{kk}=sprintf('sr0%d/vc/ccg-0%d',ii,jj);
        else
            names_ccg{kk}=sprintf('sr%d/vc/ccg-0%d',ii,jj);
        end
    end
end
nccg=numel(names_ccg);
CCG_group_id = tango_group_create ('CCG');

for ii =1:nccg
    tango_group_add(CCG_group_id, names_ccg{ii});
end
TangoResp=tango_group_read_attribute(CCG_group_id,'Pressure',0);
atribute_values=zeros(nccg,1);
old=0;
for ii =1:nccg
    if not(isempty(TangoResp.replies(ii).value))
        atribute_values(ii)=TangoResp.replies(ii).value;
        old=atribute_values(ii);
    else
        atribute_values(ii)=old;
    end
end

tango_group_kill(CCG_group_id);


end
