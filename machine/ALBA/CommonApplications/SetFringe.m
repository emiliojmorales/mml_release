function SetFringe(varargin)



numargin = size(varargin,2);


global GLOBVAL;
dosomething=0;

flag='off';
FINT1=0.7;
gap=2*0.018;
if numargin==1
    flag=varargin{1};
elseif numargin==2
    flag=varargin{1};
    FINT1=varargin{2};
elseif numargin==3
    flag=varargin{1};
    FINT1=varargin{2};
    gap=varargin{3};
end
FINT2=FINT1;

% afirmative cases
on_flags={'On','on','ON','in','In','IN'};
% negative cases
off_flags={'off','Off','OFF','Out','out','OUT'};


for ii=1:length(on_flags)
   if strcmp(on_flags{ii},flag) 
       Change2On(FINT1,FINT2,gap);
       dosomething=1;
       GLOBVAL.BENDFringe = 'On';
   end
end

for ii=1:length(off_flags)
   if strcmp(off_flags{ii},flag) 
       Change20ff;
       dosomething=1;
       GLOBVAL.BENDFringe = 'Off';
   end
end

if dosomething==0
    error('flag not apropiate ');
end
end

function Change2On(FINT1,FINT2,gap)

global THERING;

bend_pass={'BndMPoleSymplectic4Pass','BndMPoleSymplectic4HFullPass','BndMPoleSymplectic4FringeHFullPass'};


for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(bend_pass)
        if strcmp(bend_pass{jj},pass)
            if THERING{ii}.EntranceAngle~=0
                THERING{ii}.FringeInt1=FINT1;
            end
            if THERING{ii}.ExitAngle~=0
                THERING{ii}.FringeInt2=FINT2;
            end
            THERING{ii}.FullGap=gap;
        end
    end
end


end

function Change20ff

global THERING;

bend_pass={'BndMPoleSymplectic4Pass','BndMPoleSymplectic4HFullPass','BndMPoleSymplectic4FringeHFullPass'};

for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(bend_pass)
        if strcmp(bend_pass{jj},pass)
            if isfield(THERING{ii}, 'FringeInt1') 
                THERING{ii}=rmfield(THERING{ii},'FringeInt1');
            end
            if isfield(THERING{ii}, 'FringeInt2') 
                THERING{ii}=rmfield(THERING{ii},'FringeInt2');
            end
            if isfield(THERING{ii}, 'FullGap') 
                THERING{ii}=rmfield(THERING{ii},'FullGap');
            end
        end
    end
end


end