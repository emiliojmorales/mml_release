function m=Get_Inte_sexts

global THERING;
memfam=findmemberof('SEXT');
s=getpv(memfam,'Physics');

atin=atindex(THERING);
m=zeros(numel(memfam),1);
for ii=1:numel(memfam)
    list=atin.(memfam{ii});
    m(ii)=2*s{ii}*THERING{list(1)}.Length;
end

end