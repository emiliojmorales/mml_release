function Transfer_optics


global THERING;
%setpathalba('LTB');
%THERING=Put_lattice_errors(THERING);

AD=getad();
energy=AD.Energy;
enx=AD.TwissData.Emit_N_x;
eny=AD.TwissData.Emit_N_y;
d_energy=AD.TwissData.Ener_sread;
twissdatain=AD.TwissData;

TD = twissline(THERING,0,twissdatain,1:(length(THERING)+1),'chrom');
ElemIndex=cat(1,TD.ElemIndex);
s=cat(1,TD.SPos);
ClosedOrbit=cat(2,TD.ClosedOrbit)';
Dispersion = cat(2,TD.Dispersion)';
beta=cat(1,TD.beta);
betax=beta(:,1);
betay=beta(:,2);
etax=Dispersion(:,1);
etay=Dispersion(:,3);
alpha=cat(1,TD.alpha);
%mu=cat(1,TD.mu);
sim=1;


%========================
% Plot orbit
%========================

figure;

h1_0=subplot(32,1,[1 25]);
x=1000*ClosedOrbit(:,1);
y=1000*ClosedOrbit(:,3);
plot(s,x,'-r.',s,y,'-b.');
ylabel('x(mm) & y(mm)');
legend('x','y');
title('LTB Closed orbit');
h2_0=subplot(32,1,[28 32]);
drawlattice();
xlabel('s(m)');
xaxis([0 max(s)/sim]);
linkaxes([h2_0 h1_0],'x');
set(gca,'ytick',[]);
set(gca,'xtick',[]);


%========================
% Plot beta functions
%========================
figure;

L = length(THERING);
for i=1:length(THERING), 
    L(i)=THERING{i}.Length; 
end


steps=0.03;
range=[0  max(s)];
np=floor((range(2)-range(1))/steps);
ss=range(1)+[(0:np)*steps max(s)];
bxs=interp1(s(L>0), betax(L>0), ss,'pchip');
bxs(np)=bxs(np-1);
bys=interp1(s(L>0), betay(L>0), ss,'pchip');
bxs(np)=bxs(np-1);
etaxs=interp1(s(L>0), etax(L>0), ss,'pchip');
etays=interp1(s(L>0), etay(L>0), ss,'pchip');

h1_0 =subplot(32,1,[1 25]);
hold off
[AX,H1,H2] = plotyy(s, betax,s, etax,'plot');
hold on;
set(H1,'Marker','.');
set(H1,'LineStyle','none');
set(H2,'LineStyle','none');
set(H2,'Marker','.');
set(H1,'Color','r');
H3=plot(s, betay,'.');
set(gcf,'CurrentAxes',AX(2));
hold on;
H4=plot(s,etay,'c.');
legend([H1 H3 H2 H4],'\beta_x','\beta_y','D_x','D_y','Location','North','Orientation','horizontal');
set(gcf,'CurrentAxes',AX(1));
hold on;
plot(ss, bxs,'r-');
plot(ss, bys,'b-');

legend('boxoff');
set(get(AX(1),'Ylabel'),'String','\beta_{x,y} [m]') 
set(get(AX(2),'Ylabel'),'String','D_x [m]') 
set(H2,'Marker','.')
set(AX(1),'XLim',[0 max(s)/sim]);
%set(AX(1),'YLim',[0 max([bys bxs])*1.1]);
set(AX(1),'YLim',[0 40]);
set(AX(2),'XLim',[0 max(s)/sim]);
%set(AX(2),'YLim',[min(etaxs)*1.1*(1-sign(min(etaxs)))/2 max(etaxs)*1.1]);
set(AX(2),'YLim',[-0.4 1.1]);
set(AX(2),'YTickMode','auto');
set(AX(1),'YTickMode','auto');
set(gcf,'CurrentAxes',AX(2));
hold on;
plot(ss,etaxs,ss,etays,'-c');
title('Transfer line \beta-functions');
h2_0 =subplot(32,1,[28 32]);
hold off;
drawlattice();
xlabel('s(m)');
xaxis([0 max(s)/sim]);
linkaxes([h2_0 AX(2)],'x');
linkaxes([h2_0 AX(1)],'x');
set(gca,'ytick',[]);
set(gca,'xtick',[]);

%========================
% Beam Sizes
%========================

figure;
E0 = .51099906e-3; 
betgamm=sqrt(1+(energy/E0)^2);

h1_0=subplot(32,1,[1 25]);
x=1000*sqrt(betax*enx/betgamm+(etax*d_energy).^2);
y=1000*sqrt(betay*eny/betgamm+(etay*d_energy).^2);
xs=1000*sqrt(bxs*enx/betgamm+(etaxs*d_energy).^2);
ys=1000*sqrt(bys*eny/betgamm+(etays*d_energy).^2);
plot(s,x,'r.',s,y,'b.');hold on;
plot(ss,xs,'-r',ss,ys,'-b');
ylabel('\sigma_x(mm) & \sigma_y(mm)');
legend('x','y');
title('Beam sizes');
%ylim([0 1.3]);
h2_0=subplot(32,1,[28 32]);
drawlattice();
xlabel('s(m)');
xaxis([0 max(s)/sim]);
linkaxes([h2_0 h1_0],'x');
set(gca,'ytick',[]);
set(gca,'xtick',[]);


end

function RING_out=Put_lattice_errors(RING)

RING_out=RING;
n=length(RING);
dx=50e-6;
dpx=100e-6;

for ii=1:n
    dr=[dx*randn dpx*randn dx*randn dpx*randn 0 0];
    if isfield(RING{ii},'T1')
        RING_out{ii}.T1=RING{ii}.T1+dr;
        RING_out{ii}.T2=RING{ii}.T2-dr;
    else
        RING_out{ii}.T1=dr;
        RING_out{ii}.T2=-dr;
    end
end

end