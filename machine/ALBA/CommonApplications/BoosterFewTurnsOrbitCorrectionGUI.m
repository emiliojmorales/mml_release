function BoosterFewTurnsOrbitCorrectionGUI


% Created by Z.Marti - 11 June 2010
% GUI for orbit correction whith no closed orbit

global THERING;






%% Params initialization
Params.ShotNumbers=0;
Params.CorrNumb=0;
Params.N1=71;
Params.N2=1000;
Params.Nsvd=40;
Params.factorCorr=0.3;
Params.indx=atindex(THERING);
Params.THERING0=THERING;
bend_inx=findcells(THERING,'BendingAngle');
bend1_inx_if=numel(bend_inx);
bend2_inx_if=numel(bend_inx);
for ii=1:numel(bend_inx)
    bend1_inx_if(ii)=(THERING{bend_inx(ii)}.BendingAngle==5*pi/180);
    bend2_inx_if(ii)=(THERING{bend_inx(ii)}.BendingAngle==10*pi/180);
end
Params.bend1_inx=bend_inx(logical(bend1_inx_if));
Params.bend2_inx=bend_inx(logical(bend2_inx_if));
Params.QH1_inx=Params.indx.QH01;
Params.QH2_inx=Params.indx.QH02;
Params.QV1_inx=Params.indx.QV01;
Params.QV2_inx=Params.indx.QV02;
Params.BPM_inx=Params.indx.BPM;
Params.HCM_inx=Params.indx.HCM;
Params.VCM_inx=Params.indx.VCM;
Params.spos=findspos(THERING,1:length(THERING));
Params.MC=getmachineconfig();
if isstruct(Params.MC)
    Params.VCM_corr=Params.MC.VCM.Setpoint.Data;
    Params.HCM_corr=Params.MC.HCM.Setpoint.Data;
else
    Params.VCM_corr=zeros(28,1);
    Params.HCM_corr=zeros(44,1);
end
Params.mode=getmode('BEND');
Params.Qx_meas=12.42;
Params.Qy_meas=7.38;
Params.Q=thetune(0);

AO=getao();
bpm_names=AO.BPMx.DeviceName;
bpm_status=AO.BPMx.Status;
Params.bpm_status=bpm_status;
bpm_names=bpm_names(bpm_status==1);
Params.bpm_names=bpm_names;
nbpm=length(bpm_names);
buff_size=zeros(1,nbpm);
if strcmp(Params.mode,'Online')
    for kk2=1:nbpm
        bpm=bpm_names{kk2};
        str_bpm=tango_read_attribute2(bpm,'DDBufferSize');
        buff_size(kk2)=str_bpm.value(1);
    end
    Params.Buffersize=min(buff_size);
    Params.Buffersize0=Params.Buffersize;
else
    Params.Buffersize=1024;
    Params.Buffersize0=1024;
end


%% Read field with default values
daydir=['/data/BOOSTER/',datestr(now,'yyyymmdd') '/AveOrbCorrect'];
 if strcmp(Params.mode,'Online')
     mkdir(daydir);
     fprintf('Directory set to: %s\n',daydir);
 else
     disp('++++++++DEBUGMODE: Main Directory set to local folder....');
 end
%daydir=['/home/zeus/ZeusApps/CommisioningApps/LTBInjectionPanel/',datestr(now,'yyyymmdd')];
%Params.daydir=pwd;


%% reference positioning values
DX=700;
DZ=700;
dx=DX/4/3;
dz=DZ/4/6;

xz1=[0 0
    0 DZ/4
    0 DZ/2
    0 3*DZ/4];
xz2=[0 DZ/4-dz];
xz3=[0 0
    DX/4 0
    DX/2 0
    3*DX/4 0];
xz4=[dx/3 2*dz/5+dz
    dx/3 3*dz/5+2*dz
    dx/3 4*dz/5+3*dz
    5*dx/3 2*dz/5+dz
    5*dx/3 3*dz/5+2*dz
    5*dx/3 4*dz/5+3*dz
    dx/3 dz/5
    5*dx/3 dz/5];



%% Handels
Handels.f = figure('Visible','on','Position',[500,500,DX,DZ],...
    'Name','Booster Few Turns Orbit Correction','CloseRequestFcn',{@CloseGuiFunc},'NumberTitle','on','HandleVisibility','on','Color', [0.701961 0.701961 0.701961]);

%frames
Handels.frame_1= uicontrol('Style','frame','String','Horizontal','Position',[xz1(1,:)+[dx dz]/16 DX/4-dx/8 DZ/4-dz/8-dz]);
Handels.frame_2= uicontrol('Style','frame','String','Horizontal','Position',[xz1(2,:)+[dx dz]/16 DX/4-dx/8 DZ/4-dz/8-dz]);
Handels.frame_3= uicontrol('Style','frame','String','Horizontal','Position',[xz1(3,:)+[dx dz]/16 DX/4-dx/8 DZ/4-dz/8-dz]);
Handels.frame_4= uicontrol('Style','frame','String','Horizontal','Position',[xz1(4,:)+[dx dz]/16 3*DX/4-dx/8 DZ/4-dz/8-dz]);

%Main Labels
Handels.Mainlabels_1= uicontrol('Style','text','String','Orbit Correction','Position',[xz1(1,:)+xz2(1,:) DX/4 dz],'ForegroundColor',[0 0 1],'FontSize',16,'HorizontalAlignment','Left');
Handels.Mainlabels_2= uicontrol('Style','text','String','Averaged Orbit','Position',[xz1(2,:)+xz2(1,:) DX/4 dz],'ForegroundColor',[0 0 1],'FontSize',16,'HorizontalAlignment','Left');
Handels.Mainlabels_3= uicontrol('Style','text','String','BPMs intensity','Position',[xz1(3,:)+xz2(1,:) DX/4 dz],'ForegroundColor',[0 0 1],'FontSize',16,'HorizontalAlignment','Left');
Handels.Mainlabels_4= uicontrol('Style','text','String','Machine Model adjustments','Position',[xz1(4,:)+xz2(1,:) DX/2 dz],'ForegroundColor',[0 0 1],'FontSize',16,'HorizontalAlignment','Left');

% text butons
Handels.hcheck_text_1_1 = uicontrol('Style','text','String','nº SVD values','Position',[xz1(1,:)+xz4(3,:) dx dz],'HandleVisibility','on','ForegroundColor',[0 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_1_2 = uicontrol('Style','text','String','Correction factor','Position',[xz1(1,:)+xz4(2,:) dx dz],'HandleVisibility','on','ForegroundColor',[0 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_1_3 = uicontrol('Style','text','String','0 Corr. Applied.','Position',[xz1(1,:)+xz4(7,:) dx dz],'HandleVisibility','on','ForegroundColor',[1 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_2_1 = uicontrol('Style','text','String','Starting turn','Position',[xz1(2,:)+xz4(3,:) dx dz],'HandleVisibility','on','ForegroundColor',[0 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_2_2 = uicontrol('Style','text','String','Ending turn','Position',[xz1(2,:)+xz4(2,:) dx dz],'HandleVisibility','on','ForegroundColor',[0 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_3_1 = uicontrol('Style','text','String','Buffer turns','Position',[xz1(3,:)+xz4(3,:) dx dz],'HandleVisibility','on','ForegroundColor',[0 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_3_2 = uicontrol('Style','text','String','0 shots','Position',[xz1(3,:)+xz4(4,:) dx dz],'HandleVisibility','on','ForegroundColor',[1 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_3_3 = uicontrol('Style','text','String','Done','Position',[xz1(3,:)+xz4(5,:) dx dz],'HandleVisibility','on','ForegroundColor',[0 1 0],'HorizontalAlignment','Center');
Handels.hcheck_text_4_1 = uicontrol('Style','text','String','Measured tune','Position',[xz1(4,:)+xz4(3,:) 2*dx+dx/3 dz],'HandleVisibility','on','ForegroundColor',[0 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_4_2 = uicontrol('Style','text','String','Qx','Position',[xz1(4,:)+xz4(2,:) dx dz],'HandleVisibility','on','ForegroundColor',[0 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_4_3 = uicontrol('Style','text','String','Qy','Position',[xz1(4,:)+xz4(1,:) dx dz],'HandleVisibility','on','ForegroundColor',[0 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_4_4 = uicontrol('Style','text','String','Relative change','Position',[xz1(4,:)+xz3(2,:)+xz4(3,:) 2*dx+dx/3 dz],'HandleVisibility','on','ForegroundColor',[0 0 0],'HorizontalAlignment','Center');
Handels.hcheck_text_4_5 = uicontrol('Style','text','String','in %.','Position',[xz1(4,:)+xz3(2,:)+xz4(5,:) dx dz],'HandleVisibility','on','ForegroundColor',[0 0 0],'HorizontalAlignment','Center');

% edit butons
Handels.hcheck_edit_1_1 = uicontrol('Style','edit','String',num2str(Params.Nsvd),'Position',[xz1(1,:)+xz4(6,:) dx dz],'HandleVisibility','on','BackgroundColor','w','Callback',{@UpdateNsvd});
Handels.hcheck_edit_1_2 = uicontrol('Style','edit','String',num2str(Params.factorCorr),'Position',[xz1(1,:)+xz4(5,:) dx dz],'HandleVisibility','on','BackgroundColor','w','Callback',{@UpdatefactorCorr});
Handels.hcheck_edit_2_1 = uicontrol('Style','edit','String',num2str(Params.N1),'Position',[xz1(2,:)+xz4(6,:) dx dz],'HandleVisibility','on','BackgroundColor','w','Callback',{@UpdateN1});
Handels.hcheck_edit_2_2 = uicontrol('Style','edit','String',num2str(Params.N2),'Position',[xz1(2,:)+xz4(5,:) dx dz],'HandleVisibility','on','BackgroundColor','w','Callback',{@UpdateN2});
Handels.hcheck_edit_3_1 = uicontrol('Style','edit','String',num2str(Params.Buffersize),'Position',[xz1(3,:)+xz4(6,:) dx dz],'HandleVisibility','on','BackgroundColor','w','Callback',{@UpdatBuferSize});
Handels.hcheck_edit_4_1 = uicontrol('Style','edit','String',num2str(Params.Qy_meas),'Position',[xz1(4,:)+xz4(4,:) dx dz],'HandleVisibility','on','BackgroundColor','w','Callback',{@UpdateQy});
Handels.hcheck_edit_4_2 = uicontrol('Style','edit','String',num2str(Params.Qx_meas),'Position',[xz1(4,:)+xz4(5,:) dx dz],'HandleVisibility','on','BackgroundColor','w','Callback',{@UpdateQx});
Handels.hcheck_edit_4_3 = uicontrol('Style','edit','String','0.5','Position',[xz1(4,:)+xz3(2,:)+xz4(2,:) dx dz],'HandleVisibility','on','BackgroundColor','w');

% push butons
Handels.hcheck_push_1_1 = uicontrol('Style','pushbutton','String','Correct','Position',[xz1(1,:)+xz4(1,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@Correct_func});
Handels.hcheck_push_1_3 = uicontrol('Style','pushbutton','String','Apply','Position',[xz1(1,:)+xz4(4,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@ApplyCorrect_func});
Handels.hcheck_push_2_1 = uicontrol('Style','pushbutton','String','Average','Position',[xz1(2,:)+xz4(1,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@Average_func});
Handels.hcheck_push_3_1 = uicontrol('Style','pushbutton','String','Save Shot','Position',[xz1(3,:)+xz4(1,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@Save_Shot_func});
Handels.hcheck_push_3_2 = uicontrol('Style','pushbutton','String','Get Shot','Position',[xz1(3,:)+xz4(2,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@Get_Shot_func});
Handels.hcheck_push_4_1 = uicontrol('Style','pushbutton','String','QH1','Position',[xz1(4,:)+xz3(3,:)+xz4(1,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@QH1_change_func});
Handels.hcheck_push_4_2 = uicontrol('Style','pushbutton','String','QH2','Position',[xz1(4,:)+xz3(3,:)+xz4(2,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@QH2_change_func});
Handels.hcheck_push_4_3 = uicontrol('Style','pushbutton','String','BEND1','Position',[xz1(4,:)+xz3(3,:)+xz4(3,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@BEND1_change_func});
Handels.hcheck_push_4_4 = uicontrol('Style','pushbutton','String','QV1','Position',[xz1(4,:)+xz3(3,:)+xz4(4,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@QV1_change_func});
Handels.hcheck_push_4_5 = uicontrol('Style','pushbutton','String','QV2','Position',[xz1(4,:)+xz3(3,:)+xz4(5,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@QV2_change_func});
Handels.hcheck_push_4_6 = uicontrol('Style','pushbutton','String','BEND2','Position',[xz1(4,:)+xz3(3,:)+xz4(6,:) dx dz],'HandleVisibility','on','BackgroundColor','r','Callback',{@BEND2_change_func});
Handels.hcheck_push_4_7 = uicontrol('Style','pushbutton','String','RESET','Position',[xz1(4,:)+xz3(3,:)+xz4(7,:) dx dz],'HandleVisibility','on','BackgroundColor','g','Callback',{@RESET_change_func});
Handels.hcheck_push_4_8 = uicontrol('Style','pushbutton','String','RESET','Position',[xz1(3,:)+xz4(7,:) dx dz],'HandleVisibility','on','BackgroundColor','g','Callback',{@RESET_shots_change_func});
Handels.hcheck_push_1_2 = uicontrol('Style','pushbutton','String','UNDO','Position',[xz1(1,:)+xz4(8,:) dx dz],'HandleVisibility','on','BackgroundColor','g','Callback',{@UNDO_change_func});
Handels.hcheck_push_4_9 = uicontrol('Style','pushbutton','String','Get Matrix','Position',[xz1(4,:)+xz3(3,:)+xz4(8,:) dx dz],'HandleVisibility','on','BackgroundColor','b','Callback',{@GetMatrix_func});


% Axis
fact_x=0.8;
Handels.ax1 = axes('Units','pixels','Position',[xz1(1,:)+xz3(2,:)+[dx*fact_x dz] 3*DX/4-dx DZ/4-dz-dz]);
Handels.ax2 = axes('Units','pixels','Position',[xz1(2,:)+xz3(2,:)+[dx*fact_x dz] 3*DX/4-1.6*dx DZ/4-dz-dz]);
%set(Handels.ax2 ,'Color','none');
Handels.ax2b = axes('Units','pixels','Position',get(Handels.ax2,'Position'),'YAxisLocation','right','Color','none');
Handels.ax3 = axes('Units','pixels','Position',[xz1(3,:)+xz3(2,:)+[dx*fact_x dz] 3*DX/4-dx DZ/4-dz-dz]);
Handels.ax4 = axes('Units','pixels','Position',[xz1(4,:)+xz3(4,:)+[dx*fact_x dz] DX/4-dx DZ/4-dz-dz]);
xlabel(Handels.ax4,'Q_x','Color','k');
ylabel(Handels.ax4,'Q_y','Color','k');
xlimit=[min([Params.Qx_meas Params.Q(1)])-0.1 max([Params.Qx_meas Params.Q(1)])+0.1];
ylimit=[min([Params.Qy_meas Params.Q(2)])-0.1 max([Params.Qy_meas Params.Q(2)])+0.1];



n=3;
for i=0:(n-1)
    [k, tab] = reson(n-i,1,[xlimit ylimit]);
    hold on;
end
box on;
plot(Params.Qx_meas,Params.Qy_meas,'go',Params.Qx_meas,Params.Qy_meas,'g+',Params.Q(1),Params.Q(2),'m+',Params.Q(1),Params.Q(2),'mo');
xlim(xlimit);
ylim(ylimit);
hold off;


% Normalizing and centering
getnames = fieldnames(Handels);
list=zeros(1,numel(getnames)-1);
for ii=1:(numel(getnames)-1)
    list(ii)=Handels.(getnames{ii+1});
end
set(list,'Units','normalized');
movegui(Handels.f,'center');

% Callback functions

    function CloseGuiFunc(source,eventdata)
        if strcmp(Params.mode,'Online')
            bpm_names=Params.bpm_names;
            nbpm=length(bpm_names);
            for kk3=1:nbpm
                bpm=bpm_names{kk3};
                tango_command_inout2(bpm,'DisableDDBufferFreezing');
                tango_write_attribute2(bpm,'DDBufferSize', int32(Params.Buffersize0));
            end
        end
        delete(get(0,'CurrentFigure'))
    end

    function UpdatefactorCorr(source,eventdata)
        Params.factorCorr=str2double(get(source,'String'));
    end
    function ApplyCorrect_func(source,eventdata)
        if isfield(Params,'dCorr_x')
            CorrNumb=Params.CorrNumb;
            CorrNumb=CorrNumb+1;
            VCM_corr=Params.VCM_corr;
            HCM_corr=Params.HCM_corr;
            VCM_corr=VCM_corr+Params.dCorr_y;
            HCM_corr=HCM_corr+Params.dCorr_x;
            Params.VCM_corr=VCM_corr;
            Params.HCM_corr=HCM_corr;
            setpv('HCM',HCM_corr);
            setpv('VCM',VCM_corr);
            Params.MC=getmachineconfig();
            Params.Correctstatus=1;
            plotOrbita(Handels,Params);
            set(Handels.hcheck_text_1_3,'String',sprintf('%d corr. applied',CorrNumb),'ForegroundColor',[1 0 0]);
            Params.CorrNumb=CorrNumb;
        else
            error('Correction not calculated yet');
        end
    end
    function Correct_func(source,eventdata)
        if isfield(Params,'sdiag')
            if isfield(Params,'MeanBpmX_All')&&isfield(Params,'MeanBpmY_All')
                X=[Params.MeanBpmX_All;Params.MeanBpmY_All];
                U=Params.Uresp;
                S=Params.Sresp;
                V=Params.Vresp;
                Nsvd=Params.Nsvd;
                inv_R=invSVD(U,S,V,Nsvd);
                dCorr=Params.factorCorr*(inv_R*X);
                ncorr=numel(dCorr);
                ncorr_x=numel(Params.HCM_corr);
                Params.dCorr_x=-dCorr(1:ncorr_x);
                Params.dCorr_y=-dCorr((ncorr_x+1):ncorr);
                
                if strcmp(Params.mode,'Online')
                    switch2sim;
                    VCM_corr=Params.VCM_corr+Params.dCorr_y;
                    HCM_corr=Params.HCM_corr+Params.dCorr_x;
                    setpv('VCM',VCM_corr);
                    setpv('HCM',HCM_corr);
                    Params.Orbit1=getcod;
                    switch2online;
                else
                    VCM_corr=Params.VCM_corr+Params.dCorr_y;
                    HCM_corr=Params.HCM_corr+Params.dCorr_x;
                    setpv('VCM',VCM_corr);
                    setpv('HCM',HCM_corr);
                    Params.Orbit1=getcod;
                end
                
                plotOrbita(Handels,Params);
            else
                error('Average Orbit not calculated yet');
            end
        else
            error('Response matrix not calculated yet');
        end
    end
    function UpdateN1(source,eventdata)
        Params.N1=str2double(get(source,'String'));
    end
    function UpdateN2(source,eventdata)
        Params.N2=str2double(get(source,'String'));
    end
    function Average_func(source,eventdata)
        N1=Params.N1;
        N2=Params.N2;
        BpmX_AllShots=Params.BpmX_AllShots(N1:N2,:,:);
        BpmY_AllShots=Params.BpmY_AllShots(N1:N2,:,:);
        BpmX_AllShots=permute(BpmX_AllShots,[2 3 1]);
        BpmY_AllShots=permute(BpmY_AllShots,[2 3 1]);
        MeanBpmX_AllShots=mean(BpmX_AllShots,3);
        MeanBpmY_AllShots=mean(BpmY_AllShots,3);
        MeanBpmX_All=mean(MeanBpmX_AllShots,2);
        MeanBpmY_All=mean(MeanBpmY_AllShots,2);
        Params.MeanBpmX_AllShots=MeanBpmX_AllShots;
        Params.MeanBpmY_AllShots=MeanBpmY_AllShots;
        Params.MeanBpmX_All=MeanBpmX_All;
        Params.MeanBpmY_All=MeanBpmY_All;
        
        switch2sim;
        setmachineconfig(Params.MC);
        Params.Orbit0=getcod;
        Params.Orbit1=Params.Orbit0;
        if strcmp(Params.mode,'Online')
            switch2online;
        end


        plotOrbita(Handels,Params);
    end
    function RESET_shots_change_func(source,eventdata)
        ShotNumbers=0;
        Params.ShotNumbers=ShotNumbers;
        Params.BpmX_AllShots=[];
        Params.BpmY_AllShots=[];
        set(Handels.hcheck_text_3_2,'String',sprintf('%d shots',ShotNumbers),'ForegroundColor',[1 0 0]);
    end
    function Save_Shot_func(source,eventdata)
        ShotNumbers=Params.ShotNumbers+1;
        Buffersize=Params.Buffersize;
        bpm_names=Params.bpm_names;
        nbpm=length(bpm_names);
        New_BpmX_AllShots=zeros(Buffersize,nbpm,ShotNumbers);
        New_BpmY_AllShots=zeros(Buffersize,nbpm,ShotNumbers);
        for ll=1:(ShotNumbers-1)
            New_BpmX_AllShots(:,:,ll)=Params.BpmX_AllShots(:,:,ll);
            New_BpmY_AllShots(:,:,ll)=Params.BpmY_AllShots(:,:,ll);
        end
        New_BpmX_AllShots(:,:,ShotNumbers)=Params.Xbpms_instant;
        New_BpmY_AllShots(:,:,ShotNumbers)=Params.Ybpms_instant;
        Params.ShotNumbers=ShotNumbers;
        Params.BpmX_AllShots=New_BpmX_AllShots;
        Params.BpmY_AllShots=New_BpmY_AllShots;
        set(Handels.hcheck_text_3_2,'String',sprintf('%d shots',ShotNumbers),'ForegroundColor',[1 0 0]);
    end
    function Get_Shot_func(source,eventdata)
        set(Handels.hcheck_text_3_3,'String','Doing','ForegroundColor',[1 0 0]);
        bpm_names=Params.bpm_names;
        nbuff=Params.Buffersize;
        nbpm=length(bpm_names);
        Xbpms_instant=zeros(nbuff,nbpm);
        Ybpms_instant=zeros(nbuff,nbpm);
        Ibpms_instant=zeros(nbuff,nbpm);
        
        if strcmp(Params.mode,'Online')
            for kk=1:nbpm
                bpm=bpm_names{kk};
                tango_command_inout2(bpm,'UnfreezeDDBuffer');
            end
            for kk=1:nbpm
                bpm=bpm_names{kk};
                Xstr=tango_read_attribute2(bpm,'XPosDD');
                Ystr=tango_read_attribute2(bpm,'ZPosDD');
                Istr=tango_read_attribute2(bpm,'SumDD');
                Xbpms_instant(:,kk)=Xstr.value(:);
                Ybpms_instant(:,kk)=Ystr.value(:);
                Ibpms_instant(:,kk)=Istr.value(:);
            end
        else
            Xbpms_instant=ones(nbuff,1)*xran+2*randn(nbuff,nbpm);
            Ybpms_instant=ones(nbuff,1)*yran+2*randn(nbuff,nbpm);
            decay=1./(1:nbuff);
            Ibpms_instant=0.1*abs(randn(nbuff,nbpm)+1000.0*(decay'*ones(1,nbpm)).*(((1:nbuff)>=71)'*ones(1,nbpm)));
        end
        axes(Handels.ax3);
        plot(Ibpms_instant);
        box on;
        xlabel('Turns');
        ylabel('Bpm Intens (a.u.)');
        Params.Xbpms_instant=Xbpms_instant;
        Params.Ybpms_instant=Ybpms_instant;
        set(Handels.hcheck_text_3_3,'String','Done','ForegroundColor',[0 1 0]);
    end
    function UNDO_change_func(source,eventdata)
        if isfield(Params,'dCorr_x')
            if Params.Correctstatus==1
            CorrNumb=Params.CorrNumb;
            CorrNumb=CorrNumb-1;
                VCM_corr=Params.VCM_corr;
                HCM_corr=Params.HCM_corr;
                VCM_corr=VCM_corr-Params.dCorr_y;
                HCM_corr=HCM_corr-Params.dCorr_x;
                Params.VCM_corr=VCM_corr;
                Params.HCM_corr=HCM_corr;
                setpv('HCM',HCM_corr);
                setpv('VCM',VCM_corr);
                Params.MC=getmachineconfig();
                Params.Correctstatus=0;
                Params=rmfield(Params,'dCorr_y');
                Params=rmfield(Params,'dCorr_x');
                plotOrbita(Handels,Params);
                set(Handels.hcheck_text_1_3,'String',sprintf('%d corr. applied',CorrNumb),'ForegroundColor',[1 0 0]);
                Params.CorrNumb=CorrNumb;
            else
                error('Single undo after a single apply is possible');
            end
        else
            error('Correction not calculated yet');
        end
    end
    function UpdatBuferSize(source,eventdata)
        Params.Buffersize=str2double(get(source,'String'));
        bpm_names=Params.bpm_names;
        nbpm=length(bpm_names);
        if strcmp(Params.mode,'Online')
            for jj=1:nbpm
                bpm=bpm_names{jj};
                tango_write_attribute2(bpm,'DDBufferSize', int32(Params.Buffersize));
            end
        end
    end
    function UpdateNsvd(source,eventdata)
        Params.Nsvd=str2double(get(source,'String'));
        sdiag=Params.sdiag;
        axes(Handels.ax1);
        semilogy(sdiag,'-r.');hold on;semilogy(sdiag(1:(Params.Nsvd)),'-b.');hold off;
        ylabel('SV strength');
        xlabel('Single value');
        box on;
    end
    function QH1_change_func(source,eventdata)
        Params.relatChange=str2double(get(Handels.hcheck_edit_4_3,'String'));
        ChangeKatIndex(Params.QH1_inx,Params.relatChange);
        Params.Q=thetune(0);
        plotTuneDiag(Handels,Params);
    end
    function QH2_change_func(source,eventdata)
        Params.relatChange=str2double(get(Handels.hcheck_edit_4_3,'String'));
        ChangeKatIndex(Params.QH2_inx,Params.relatChange);
        Params.Q=thetune(0);
        plotTuneDiag(Handels,Params);
    end
    function BEND1_change_func(source,eventdata)
        Params.relatChange=str2double(get(Handels.hcheck_edit_4_3,'String'));
        ChangeKatIndex(Params.bend1_inx,Params.relatChange);
        Params.Q=thetune(0);
        plotTuneDiag(Handels,Params);
    end
    function QV1_change_func(source,eventdata)
        Params.relatChange=str2double(get(Handels.hcheck_edit_4_3,'String'));
        ChangeKatIndex(Params.QV1_inx,Params.relatChange);
        Params.Q=thetune(0);
        plotTuneDiag(Handels,Params);
    end
    function QV2_change_func(source,eventdata)
        Params.relatChange=str2double(get(Handels.hcheck_edit_4_3,'String'));
        ChangeKatIndex(Params.QV2_inx,Params.relatChange);
        Params.Q=thetune(0);
        plotTuneDiag(Handels,Params);
    end
    function BEND2_change_func(source,eventdata)
        Params.relatChange=str2double(get(Handels.hcheck_edit_4_3,'String'));
        ChangeKatIndex(Params.bend2_inx,Params.relatChange);
        Params.Q=thetune(0);
        plotTuneDiag(Handels,Params);
    end
    function RESET_change_func(source,eventdata)
        THERING=Params.THERING0;
        Params.Q=thetune(0);
        plotTuneDiag(Handels,Params);
    end
    function UpdateQx(source,eventdata)
        Params.Qx_meas=str2double(get(source,'String'));
        plotTuneDiag(Handels,Params);
    end
    function UpdateQy(source,eventdata)
        Params.Qy_meas=str2double(get(source,'String'));
        plotTuneDiag(Handels,Params);
    end
    function GetMatrix_func(source,eventdata)
        switch2sim;
        Mresp=measbpmresp('Model');
        if strcmp(Params.mode,'Online')
            switch2online;
        end
        [U,S,V] = svd(Mresp);
        sdiag=diag(S);
        axes(Handels.ax1);
        semilogy(sdiag,'-r.');hold on;semilogy(sdiag(1:(Params.Nsvd)),'-b.');hold off;
        ylabel('SV strength');
        xlabel('Single value');
        box on;
        Params.Mresp=Mresp;
        Params.Uresp=U;
        Params.Sresp=S;
        Params.sdiag=sdiag;
        Params.Vresp=V;
    end

% Stablish machine config in simulation

switch2sim;
setmachineconfig(Params.MC);
Params.Orbit0=getcod;
Params.Orbit1=Params.Orbit0;
if strcmp(Params.mode,'Online')
    switch2online;
    bpm_names=Params.bpm_names;
    nbpm=length(bpm_names);
    for kk2=1:nbpm
        bpm=bpm_names{kk2};
        tango_command_inout2(bpm,'EnableDDBufferFreezing');
        tango_write_attribute2(bpm,'DDBufferSize', int32(Params.Buffersize));
    end
else
    bpm_names=Params.bpm_names;
    nbpm=length(bpm_names);
    xran=randn(1,nbpm);
    yran=randn(1,nbpm);
end


set(Handels.f,'HandleVisibility','callback');

end

function plotOrbita(Handels,Params)
VCM_corr=Params.VCM_corr;
HCM_corr=Params.HCM_corr;
if isfield(Params,'dCorr_x')
    VCM_corr=VCM_corr+Params.dCorr_y;
    HCM_corr=HCM_corr+Params.dCorr_x;
end
spos=Params.spos;
BPM_inx=Params.BPM_inx(Params.bpm_status==1);
HCM_inx=Params.HCM_inx;
VCM_inx=Params.VCM_inx;
MeanBpmX_AllShots=Params.MeanBpmX_AllShots;
MeanBpmY_AllShots=Params.MeanBpmY_AllShots;
MeanBpmX_All=Params.MeanBpmX_All;
MeanBpmY_All=Params.MeanBpmY_All;
dx=Params.Orbit1(1,BPM_inx)-Params.Orbit0(1,BPM_inx);
dy=Params.Orbit1(3,BPM_inx)-Params.Orbit0(3,BPM_inx);
axes(Handels.ax2);
plot(spos(BPM_inx)',MeanBpmX_AllShots,'r','LineWidth',0.3);hold on;plot(spos(BPM_inx)',MeanBpmY_AllShots,'b','LineWidth',0.3);
plot(spos(BPM_inx)',MeanBpmX_All,'r','LineWidth',2);hold on;plot(spos(BPM_inx)',MeanBpmY_All,'b','LineWidth',2);
plot(spos(BPM_inx)',MeanBpmX_All+1000*dx','--r','LineWidth',1);hold on;plot(spos(BPM_inx)',MeanBpmY_All+1000*dy','--b','LineWidth',1);

yl=ylim;
ml=max(abs(yl));
ylim([-ml ml]);
hold off;
box on;
xlabel('s (m)');
ylabel('x & y (mm)');
%set(Handels.ax2 ,'Color','none');
axes(Handels.ax2b);
h2=bar(spos(HCM_inx)',HCM_corr,'r');set(h2,'BarWidth',0.6,'EdgeColor','none');
hold on;
h2=bar(spos(VCM_inx)',VCM_corr,'b');set(h2,'BarWidth',0.6,'EdgeColor','none');
yl=ylim;
ml=max(abs(yl));
ylim([-ml ml]);
hold off;
set(Handels.ax2b,'YAxisLocation','right','Color','none');
ylabel(Handels.ax2b,'Corr. Strength (A)');
linkaxes([Handels.ax2 Handels.ax2b],'x');

end

function plotTuneDiag(Handels,Params)
axes(Handels.ax4);
xlimit=[min([Params.Qx_meas Params.Q(1)])-0.1 max([Params.Qx_meas Params.Q(1)])+0.1];
ylimit=[min([Params.Qy_meas Params.Q(2)])-0.1 max([Params.Qy_meas Params.Q(2)])+0.1];
n=3;
plot([1 1]);
for i=0:(n-1)
    [k, tab] = reson(n-i,1,[xlimit ylimit]);
    hold on;
end
box on;
plot(Params.Qx_meas,Params.Qy_meas,'go',Params.Qx_meas,Params.Qy_meas,'g+',Params.Q(1),Params.Q(2),'m+',Params.Q(1),Params.Q(2),'mo');
xlabel(Handels.ax4,'Q_x','Color','k');
ylabel(Handels.ax4,'Q_y','Color','k');
xlim(xlimit);
ylim(ylimit);
hold off;
end

function ChangeKatIndex(indx,relat_change)
global THERING;
nn=numel(indx);
for ii=1:nn
    b=THERING{indx(ii)}.PolynomB(2);
    k=THERING{indx(ii)}.K;
    THERING{indx(ii)}.PolynomB(2)=b*(1+relat_change/100);
    THERING{indx(ii)}.K=k*(1+relat_change/100);
end
end

function inv_R=invSVD(U,S,V,n)
% Perform an SVD inversion with n singular values
small=1e-16;
sd=diag(S);
in_sd0=find(abs(sd)<small);
d=1./diag(S);
d(in_sd0)=zeros(length(in_sd0),1);
ndiag=length(d);
d((n+1):ndiag)=zeros(ndiag-n,1);
ind_S_0=diag(d);
ind_S=S';
ind_S(1:ndiag,1:ndiag)=ind_S_0;
inv_R=V*ind_S*(U');
end