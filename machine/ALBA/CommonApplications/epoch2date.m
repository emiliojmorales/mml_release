function [date_str] = epoch2date(epochTime, dateformat)
% converts epoch time to human readable date string

% import java classes
import java.lang.System;
import java.text.SimpleDateFormat;
import java.util.Date;

% convert current system time if no input arguments
if (~exist('epochTime','var'))
    epochTime = System.currentTimeMillis/1000;
end
if nargin == 1,
    dateformat='yyyy/MM/dd HH:mm:ss.SS';
end
% convert epoch time (Date requires milliseconds)
jdate = Date(epochTime*1000);

% format text and convert to cell array
sdf = SimpleDateFormat(dateformat);
date_str = sdf.format(jdate);
date_str = char(cell(date_str));