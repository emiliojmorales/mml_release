function varargout=SetHamiltonian(varargin)
% Change all pass methods for full to aprox hamiltonian and the other way
% arround if indicated



numargin = size(varargin,2);
numargout = nargout;


global GLOBVAL;
dosomething=0;

flag='Approx';
if numargin>0
    flag=varargin{1};
end

% afirmative cases
full_flags={'Full','full','exact','Exact'};
% negative cases
app_flags={'Approx','approx','approximate','approximated','Approximate','Approximated'};


for ii=1:length(full_flags)
   if strcmp(full_flags{ii},flag) 
       Change2Full;
       dosomething=1;
       GLOBVAL.Hamiltonian = 'Full';
   end
end

for ii=1:length(app_flags)
   if strcmp(app_flags{ii},flag) 
       Change2Approx;
       dosomething=1;
       GLOBVAL.Hamiltonian = 'Approx';
   end
end

if dosomething==0
    error('flag not apropiate ');
end

end

function Change2Full

global THERING;

full_pass={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass'};
app_pass={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftPass'};
for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(app_pass)
        if strcmp(app_pass{jj},pass)
            THERING{ii}.PassMethod=full_pass{jj};
        end
    end
end


end

function Change2Approx

global THERING;

full_pass={'StrMPoleSymplectic4HFullPass','BndMPoleSymplectic4HFullPass','DriftHFullPass'};
app_pass={'StrMPoleSymplectic4Pass','BndMPoleSymplectic4Pass','DriftPass'};
pass_bend='BndMPoleSymplectic4HFullPass';

for ii=1:length(THERING)
    pass=THERING{ii}.PassMethod;
    for jj=1:length(full_pass)
        if strcmp(full_pass{jj},pass)
            THERING{ii}.PassMethod=app_pass{jj};
            if strcmp(app_pass{jj},pass_bend)
                %THERING{ii}.NumIntSteps=10;
            end
        end
    end
end


end