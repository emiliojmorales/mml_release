function cycling_2(varargin)
%cycling(Family,Min_Current,Max_Current)
% This has to be moved to the AO
%
% DEFAULTS:
% family='QH01';
 time_wait=30; % 30 is the recomendet time interval

if length(varargin) == 3
    family = varargin{1};
    Min_Current=varargin{2};
    Max_Current=varargin{3};
else
    family = [];    
end

if isempty(family)
    answer = inputdlg({'Family to cycle','Min Current','Min Current'},'Cycling',1,{'QH01','20','200'});
    if isempty(answer)
        return;
    end
    family = answer{1};
    Min_Current = str2double(answer{2}); 
    Max_Current = str2double(answer{3});
end
if iscell(Max_Current)
    Max_total_Current=max(cell2mat(Max_Current));
    Min_total_Current=min(cell2mat(Min_Current));
else
    Max_total_Current=max((Max_Current));
    Min_total_Current=min((Min_Current));
end
time_wait=(Max_total_Current-Min_total_Current)/10+20;
time_wait_small=0.2;

fprintf('Theoretical stopping time at each current: %f\n',time_wait);
q0=getpv(family);


n_cycle=3;

for ii=1:(n_cycle-1)
    setpv(family,Max_Current);
    pause(time_wait_small);
    setpv(family,Max_Current);
    pause(time_wait_small);
    setpv(family,Max_Current);
    % wait
    pause(time_wait);
    % 
    setpv(family,Min_Current);
    pause(time_wait_small);
    setpv(family,Min_Current);
    pause(time_wait_small);
    setpv(family,Min_Current);
    % wait
    pause(time_wait);
end

setpv(family,Max_Current);
pause(time_wait_small);
setpv(family,Max_Current);
pause(time_wait_small);
setpv(family,Max_Current);
% wait
pause(time_wait);
%
setpv(family,q0);
pause(time_wait_small);
setpv(family,q0);
pause(time_wait_small);
setpv(family,q0);

end

