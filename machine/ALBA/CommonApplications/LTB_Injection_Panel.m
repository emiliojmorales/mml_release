function LTB_Injection_Panel

% Created by Z.Marti - 27 May 2010
% GUI for the control of angle and position at several points of the LTB

global THERING;

%% Read field with default values
daydir=['/data/LTB/AngleAndPosition/',datestr(now,'yyyymmdd')];
%disp('++++++++DEBUGMODE: Main Directory set to local folder....');
%daydir=['/home/zeus/ZeusApps/CommisioningApps/LTBInjectionPanel/',datestr(now,'yyyymmdd')];
%Params.daydir=pwd;
mkdir(daydir);



%% Params initialization
AO=getao();
bpm_names=AO.BPMx.DeviceName;
bpm_status=AO.BPMx.Status;
Params.bpm_status=bpm_status;
bpm_names=bpm_names(bpm_status==1);
Params.bpm_names=bpm_names;
nbpm=numel(bpm_names);
state=uint8(0);
for kk2=1:nbpm
    bpm=bpm_names{kk2};
    tango_command_inout2(bpm,'DDAcquire',state);
end
Params.indx=atindex(THERING);
Params.ss=findspos(THERING, 1:(length(THERING)+1));
Params.indx_BPMx=Params.indx.BPM(4);
Params.indx_BPMy=Params.indx.BPM(4);
Params.indx_v_M1=Params.indx.VCM(1);
Params.indx_v_M2=Params.indx.VCM(2);
Params.indx_h_M1=Params.indx.HCM(1);
Params.indx_h_M2=Params.indx.HCM(2);
Params.cell_BPMx={'BPM',[1 4]};
Params.cell_BPMy={'BPM',[1 4]};
Params.cell_v_M1={'VCM',[1 1]};
Params.cell_v_M2={'VCM',[1 2]};
Params.cell_h_M1={'HCM',[1 1]};
Params.cell_h_M2={'HCM',[1 2]};
Params.getmachineconfig=getmachineconfig;
Params.dx=0;
Params.dxp=0;
Params.dy=0;
Params.dyp=0;
%Params.SeptumSettingRef=77.153608515404414;% getpv('SEINJ')
%Params.KickerSettingRef=9.824633084748019e+03;% getpv('KIINJ')
Params.SeptumSettingRef=  getpv('SEINJ');
Params.KickerSettingRef= getpv('KIINJ');
Orbit=getorbit;
Params.Orbit0=Orbit;
Params.Orbit1=Orbit;
Params.corrx=[0;0];
Params.corry=[0;0];
Params.magnetcoefficients.SeptumInjection=magnetcoefficients('SEINJ');
Params.magnetcoefficients.KickerInjection=magnetcoefficients('KIINJ');
Params.brho=getbrho;
Params.mode=getmode('HCM');
Params.Corr_0.HCM=getpv('HCM');
Params.Corr_0.VCM=getpv('VCM');
Params.Corr_0.SEINJ=getpv('SEINJ');
Params.Corr_0.KIINJ=getpv('KIINJ');
Params.PolyB_0.SEINJ=THERING{Params.indx.SEINJ}.PolynomB;
Params.PolyB_0.KIINJ=THERING{Params.indx.KIINJ}.PolynomB;
Params.My=maxpv('VCM');
Params.Mx0=maxpv('HCM');
Params.Mxs=maxpv('SEINJ')-Params.SeptumSettingRef;
Params.Mxk=maxpv('KIINJ')-Params.KickerSettingRef;

%% reference positioning values
DX=500;
DZ=400;
dx=DX/16;
dz=10;

xz1=[DX/2 DZ/2];
xz2=[-DX/4 DZ/2-9*dz
    DX/4 DZ/2-9*dz
    0 -DZ/2.8];
xz3=[0 2*dz
    -2.5*dx 2*dz
    2*dx -3*dz
    2.5*dx 2*dz
    -2*dx -3*dz
    0 7*dz
    1.5*dx -7*dz
    -1.5*dx -7*dz];
xz4=[0 dz/2
    0 -dz/2];

dfx=dx*13;
dfz=12*dz;
xz5=[0 dfz/2
    0 -dfz/10];


%% Handels
Handels.f = figure('Visible','on','Position',[500,500,DX,DZ],'Name','LTB Injection Panel','NumberTitle','off');

%frames
Handels.frame_h= uicontrol('Style','frame','String','Horizontal','Position',[xz1(1,:)+xz2(1,:)-[7.7*dx 20*dz]/2 dx*7.7 18*dz]);
Handels.frame_v= uicontrol('Style','frame','String','Vertical','Position',[xz1(1,:)+xz2(2,:)-[7.7*dx 20*dz]/2 dx*7.7 18*dz]);

% titles
Handels.htext_hor= uicontrol('Style','text','String','Horizontal Plane:','Position',[xz1(1,:)+xz2(1,:)+xz3(6,:)-[dx*4 dz*2]/2 dx*4 dz*2],'ForegroundColor',[0.7 0 0]);
Handels.htext_ver  = uicontrol('Style','text','String','Vertical Plane:','Position',[xz1(1,:)+xz2(2,:)+xz3(6,:)-[dx*4 dz*2]/2 dx*4 dz*2],'ForegroundColor',[0 0 1]);

% Magnet and BPM settings
Handels.hcheck_choose_h_BPM = uicontrol('Style','popupmenu','String',{'BPM 1','BPM 2','BPM 3','BPM 4','BOOSTER BPM','Septum End','Kicker End'},'Position',[xz1(1,:)+xz2(1,:)+xz3(1,:)+xz4(2,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','Callback',{@popup_h_BPM_Callback},'BackgroundColor','w');
Handels.hcheck_choose_h_Magnet1 = uicontrol('Style','popupmenu','String',{'HCM 1','HCM 2','HCM 3','HCM 4','SEPTUM','KICKER'},'Position',[xz1(1,:)+xz2(1,:)+xz3(2,:)+xz4(2,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','Callback',{@popup_h_M1_Callback},'BackgroundColor','w');
Handels.hcheck_choose_h_Magnet2 = uicontrol('Style','popupmenu','String',{'HCM 1','HCM 2','HCM 3','HCM 4','SEPTUM','KICKER'},'Position',[xz1(1,:)+xz2(1,:)+xz3(4,:)+xz4(2,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','Callback',{@popup_h_M2_Callback},'BackgroundColor','w');
Handels.hcheck_choose_v_BPM = uicontrol('Style','popupmenu','String',{'BPM 1','BPM 2','BPM 3','BPM 4','BOOSTER BPM','Septum End','Kicker End'},'Position',[xz1(1,:)+xz2(2,:)+xz3(1,:)+xz4(2,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','Callback',{@popup_v_BPM_Callback},'BackgroundColor','w');
Handels.hcheck_choose_v_Magnet1 = uicontrol('Style','popupmenu','String',{'VCM 1','VCM 2','VCM 3','VCM 4'},'Position',[xz1(1,:)+xz2(2,:)+xz3(2,:)+xz4(2,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','Callback',{@popup_v_M1_Callback},'BackgroundColor','w');
Handels.hcheck_choose_v_Magnet2 = uicontrol('Style','popupmenu','String',{'VCM 1','VCM 2','VCM 3','VCM 4'},'Position',[xz1(1,:)+xz2(2,:)+xz3(4,:)+xz4(2,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','Callback',{@popup_v_M2_Callback},'BackgroundColor','w');
set(Handels.hcheck_choose_v_Magnet2,'Value',2);
set(Handels.hcheck_choose_h_Magnet2,'Value',2);
set(Handels.hcheck_choose_h_BPM,'Value',4);
set(Handels.hcheck_choose_v_BPM,'Value',4);

Handels.hcheck_text_h_BPM = uicontrol('Style','text','String','BPM x','Position',[xz1(1,:)+xz2(1,:)+xz3(1,:)+xz4(1,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','ForegroundColor',[0.7 0 0]);
Handels.hcheck_text_h_Magnet1 = uicontrol('Style','text','String','Magnet 1','Position',[xz1(1,:)+xz2(1,:)+xz3(2,:)+xz4(1,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','ForegroundColor',[0.7 0 0]);
Handels.hcheck_text_h_Magnet2 = uicontrol('Style','text','String','Magnet 2','Position',[xz1(1,:)+xz2(1,:)+xz3(4,:)+xz4(1,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','ForegroundColor',[0.7 0 0]);
Handels.hcheck_text_v_BPM = uicontrol('Style','text','String','BPM y','Position',[xz1(1,:)+xz2(2,:)+xz3(1,:)+xz4(1,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','ForegroundColor',[0 0 1]);
Handels.hcheck_text_v_Magnet1 = uicontrol('Style','text','String','Magnet 1','Position',[xz1(1,:)+xz2(2,:)+xz3(2,:)+xz4(1,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','ForegroundColor',[0 0 1]);
Handels.hcheck_text_v_Magnet2 = uicontrol('Style','text','String','Magnet 2','Position',[xz1(1,:)+xz2(2,:)+xz3(4,:)+xz4(1,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','ForegroundColor',[0 0 1]);

% X, X', Y and Y' step seting
Handels.hcheck_edit_h_x = uicontrol('Style','edit','String','0.0','Position',[xz1(1,:)+xz2(1,:)+xz3(5,:)+2*xz4(2,:)-[2.5*dx dz]/2 dx*2.5 2*dz],'HandleVisibility','on','BackgroundColor','w');
Handels.hcheck_edit_h_xp = uicontrol('Style','edit','String','0.0','Position',[xz1(1,:)+xz2(1,:)+xz3(3,:)+2*xz4(2,:)-[2.5*dx dz]/2 dx*2.5 2*dz],'HandleVisibility','on','BackgroundColor','w');
Handels.hcheck_edit_v_y = uicontrol('Style','edit','String','0.0','Position',[xz1(1,:)+xz2(2,:)+xz3(5,:)+2*xz4(2,:)-[2.5*dx dz]/2 dx*2.5 2*dz],'HandleVisibility','on','BackgroundColor','w');
Handels.hcheck_edit_v_yp = uicontrol('Style','edit','String','0.0','Position',[xz1(1,:)+xz2(2,:)+xz3(3,:)+2*xz4(2,:)-[2.5*dx dz]/2 dx*2.5 2*dz],'HandleVisibility','on','BackgroundColor','w');

Handels.hcheck_text_h_x = uicontrol('Style','text','String','Step X (mm)','Position',[xz1(1,:)+xz2(1,:)+xz3(5,:)+2*xz4(1,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','ForegroundColor',[0.7 0 0]);
Handels.hcheck_text_h_xp = uicontrol('Style','text','String',' Step dX/ds (mrad)','Position',[xz1(1,:)+xz2(1,:)+xz3(3,:)+2*xz4(1,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','ForegroundColor',[0.7 0 0]);
Handels.hcheck_text_v_y = uicontrol('Style','text','String','Step Y (mm)','Position',[xz1(1,:)+xz2(2,:)+xz3(5,:)+2*xz4(1,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','ForegroundColor',[0 0 1]);
Handels.hcheck_text_v_yp = uicontrol('Style','text','String','Step dY/ds (mrad)','Position',[xz1(1,:)+xz2(2,:)+xz3(3,:)+2*xz4(1,:)-[2.5*dx dz]/2 dx*2.5 dz],'HandleVisibility','on','ForegroundColor',[0 0 1]);

% Pushbuttons
Handels.CalculateStep_h= uicontrol('Style','pushbutton','String','Calculate X','Position',[xz1(1,:)+xz2(1,:)+xz3(8,:)-[2.5*dx 4*dz]/2 dx*2.5 4*dz],'Callback',{@ClacX_Callback},'BackgroundColor','r','ForegroundColor','w');
Handels.CalculateStep_v= uicontrol('Style','pushbutton','String','Calculate Y','Position',[xz1(1,:)+xz2(2,:)+xz3(8,:)-[2.5*dx 4*dz]/2 dx*2.5 4*dz],'Callback',{@ClacY_Callback},'BackgroundColor','b','ForegroundColor','w');
Handels.Step_h= uicontrol('Style','pushbutton','String','Step X','Position',[xz1(1,:)+xz2(1,:)+xz3(7,:)-[2.5*dx 4*dz]/2 dx*2.5 4*dz],'Callback',{@StepX_Callback},'BackgroundColor','r','ForegroundColor','w');
Handels.Step_v= uicontrol('Style','pushbutton','String','Step Y','Position',[xz1(1,:)+xz2(2,:)+xz3(7,:)-[2.5*dx 4*dz]/2 dx*2.5 4*dz],'Callback',{@StepY_Callback},'BackgroundColor','b','ForegroundColor','w');
Handels.Refresh= uicontrol('Style','pushbutton','String','Refresh','Position',[xz1(1,:)-[0 dz]-[3*dx 2*dz]/2 3*dx 2*dz],'Callback',{@Refresh_Callback});
Handels.Reset= uicontrol('Style','pushbutton','String','Reset','Position',[xz1(1,:)-[3*dx dz]-[3*dx 2*dz]/2 3*dx 2*dz],'Callback',{@Reset_Callback});
Handels.Save= uicontrol('Style','pushbutton','String','Save','Position',[xz1(1,:)-[-3*dx dz]-[3*dx 2*dz]/2 3*dx 2*dz],'Callback',{@Save_Callback});

%Plots
[X Y Cx Cy]=MeasureAll(Params);
Measures.X=X;
Measures.Y=Y;
Measures.Cx=Cx;
Measures.Cy=Cy;
Params.Measures=Measures;
Handels.ax1 = axes('Units','pixels','Position',[xz1(1,:)+xz2(3,:)+xz5(1,:)-[dfx dfz]/2 dfx dfz]);
hold on;
stem(Params.ss(Params.indx.BPM),X,'r');stem(Params.ss(Params.indx.BPM),Y,'b');
box on;
set(Handels.ax1,'xtick',[]);
set(Handels.ax1,'Tag','ax1','Color','none');
ylabel(Handels.ax1,'BPMs (mm)','Color','k');
set(Handels.ax1,'Units','normalized','YColor',[0 0 0],'XColor',[0 0 0]);
yl=ylim;
ylim([-max(abs(yl)) max(abs(yl))]);


Handels.ax2 = axes('Position',get(Handels.ax1,'Position'),'Tag','ax2','XAxisLocation','top','YAxisLocation','right','Color','none');
hold on;
h=bar([Params.ss(Params.indx.HCM) Params.ss(Params.indx.SEINJ) Params.ss(Params.indx.KIINJ)],Cx,'r');set(h,'BarWidth',0.1);
h=bar(Params.ss(Params.indx.VCM),Cy,'b');set(h,'BarWidth',0.05);
ylabel(Handels.ax2,'Corr. Strength (%)');
set(Handels.ax2,'xtick',[]);
yl=ylim;
ylim([-max(abs(yl)) max(abs(yl))]);

Handels.axd= axes('Units','pixels','Position',[xz1(1,:)+xz2(3,:)+xz5(2,:)-[dfx dfz/5]/2 dfx dfz/5]);
set(Handels.axd,'Tag','ax2','Color','none');
set(Handels.axd,'ytick',[]);
hold on;
drawlattice;
ylim([-1 1]);
box on;

xlabel('s (m)');
linkaxes([Handels.ax1 Handels.ax2 Handels.axd],'x');

% Normalizing and centering
getnames = fieldnames(Handels);
list=zeros(1,numel(getnames)-1);
for ii=1:(numel(getnames)-1)
    list(ii)=Handels.(getnames{ii+1});
end
set(list,'Units','normalized');
movegui(Handels.f,'center');

%% Call back functions


    function popup_h_BPM_Callback(source,eventdata)
        val = get(source,'Value');
        if val<=5
            Params.indx_BPMx=Params.indx.BPM(val);
            Params.cell_BPMx={THERING{Params.indx_BPMx}.FamName,[1 val]};
        elseif val==6
            Params.indx_BPMx=Params.indx.IP;
            Params.cell_BPMx={'IP'};
        elseif val==7
            Params.indx_BPMx=Params.indx.IP2;
        end
    end
    function popup_v_BPM_Callback(source,eventdata)
        val = get(source,'Value');
        if val<=5
            Params.indx_BPMy=Params.indx.BPM(val);
            Params.cell_BPMy={THERING{Params.indx_BPMx}.FamName,[1 val]};
        elseif val==6
            Params.indx_BPMy=Params.indx.IP;
            Params.cell_BPMy={'IP'};
        elseif val==7
            Params.indx_BPMy=Params.indx.IP2;
        end
        
        
    end
    function popup_v_M1_Callback(source,eventdata)
        val = get(source,'Value');
        Params.indx_v_M1=Params.indx.VCM(val);
        Params.cell_v_M1={'VCM',[1 val]};
    end
    function popup_v_M2_Callback(source,eventdata)
        val = get(source,'Value');
        Params.indx_v_M2=Params.indx.VCM(val);
        Params.cell_v_M2={'VCM',[1 val]};
    end
    function popup_h_M1_Callback(source,eventdata)
        val = get(source,'Value');
        if val<=4
            Params.indx_h_M1=Params.indx.HCM(val);
            Params.cell_h_M1={'HCM',[1 val]};
        elseif val==5
            Params.indx_h_M1=Params.indx.SEINJ;
            Params.cell_h_M1={'SEINJ'};
        elseif val==6
            Params.indx_h_M1=Params.indx.KIINJ;
            Params.cell_h_M1={'KIINJ'};
        end
    end
    function popup_h_M2_Callback(source,eventdata)
        val = get(source,'Value');
        if val<=4
            Params.indx_h_M2=Params.indx.HCM(val);
            Params.cell_h_M2={'HCM',[1 val]};
        elseif val==5
            Params.indx_h_M2=Params.indx.SEINJ;
            Params.cell_h_M2={'SEINJ'};
        elseif val==6
            Params.indx_h_M2=Params.indx.KIINJ;
            Params.cell_h_M2={'KIINJ'};
        end
    end
    function ClacX_Callback(source,eventdata)
        Params.corry=[0;0];
        TestLogic(Params);
        fact=1e3;
        Params.dx=str2double(get(Handels.hcheck_edit_h_x,'String'))/fact;
        Params.dxp=str2double(get(Handels.hcheck_edit_h_xp,'String'))/fact;
        Params.dy=str2double(get(Handels.hcheck_edit_v_y,'String'))/fact;
        Params.dyp=str2double(get(Handels.hcheck_edit_v_yp,'String'))/fact;
        if strcmp(Params.mode,'Online')
            [X_online Y_online Cx_online Cy_online]=MeasureAll(Params);
        end
        
        switch2sim;
        Orbit0=getorbit;
        Params.Orbit0=Orbit0;
        M=getRespOrbit_x(Params);
        corr=M\[Params.dx; Params.dxp];
        Params.corrx=corr;
        dev1=Params.cell_h_M1{1};
        dev2=Params.cell_h_M2{1};
        
        if strcmp(dev1,'HCM')
            steppv(dev1,corr(1),Params.cell_h_M1{2});
        elseif strcmp(dev1,'SEINJ')
            Polyb0=THERING{Params.indx.(dev1)}.PolynomB;
            L=THERING{Params.indx.(dev1)}.Length;
            C=Params.magnetcoefficients.SeptumInjection;
            br=Params.brho;
            Polyb1=Polyb0+[(C(end-1)*corr(1))*L/br Polyb0(2:end)];
            THERING{Params.indx.(dev1)}.PolynomB=Polyb1;
        elseif strcmp(dev1,'KIINJ')
            Polyb0=THERING{Params.indx.(dev1)}.PolynomB;
            L=THERING{Params.indx.(dev1)}.Length;
            C=Params.magnetcoefficients.KickerInjection;
            br=Params.brho;
            Polyb1=Polyb0+[(C(end-1)*corr(1))*L/br Polyb0(2:end)];
            THERING{Params.indx.(dev1)}.PolynomB=Polyb1;
        end
        
        if strcmp(dev2,'HCM')
            steppv(dev2,corr(2),Params.cell_h_M2{2});
        elseif strcmp(dev2,'SEINJ')
            Polyb0_2=THERING{Params.indx.(dev2)}.PolynomB;
            L=THERING{Params.indx.(dev2)}.Length;
            C=Params.magnetcoefficients.SeptumInjection;
            br=Params.brho;
            Polyb1=Polyb0_2+[(C(end-1)*corr(2))*L/br Polyb0_2(2:end)];
            THERING{Params.indx.(dev2)}.PolynomB=Polyb1;
        elseif strcmp(dev2,'KIINJ')
            Polyb0_2=THERING{Params.indx.(dev2)}.PolynomB;
            L=THERING{Params.indx.(dev2)}.Length;
            C=Params.magnetcoefficients.KickerInjection;
            br=Params.brho;
            Polyb1=Polyb0_2+[(C(end-1)*corr(2))*L/br Polyb0_2(2:end)];
            THERING{Params.indx.(dev2)}.PolynomB=Polyb1;
        end
        
        
        
        
        Orbit1=getorbit;
        Params.Orbit1=Orbit1;
        [X Y Cx Cy]=MeasureAll(Params);
        if strcmp(Params.mode,'Online')
            Measures.Cx=Cx+Cx_online;
            Measures.Cy=Cy+Cy_online;
        else
            Measures.Cx=Cx;
            Measures.Cy=Cy;
        end
        Params.Measures=Measures;
        plotdata(Params,Handels)
        Params.getmachineconfig=getmachineconfig;
        
        if strcmp(dev1,'HCM')
            steppv(dev1,-corr(1),Params.cell_h_M1{2});
        elseif strcmp(dev1,'SEINJ')
            THERING{Params.indx.(dev1)}.PolynomB=Polyb0;
        elseif strcmp(dev1,'KIINJ')
            THERING{Params.indx.(dev1)}.PolynomB=Polyb0;
        end
        
        if strcmp(dev2,'HCM')
            steppv(dev2,-corr(2),Params.cell_h_M2{2});
        elseif strcmp(dev2,'SEINJ')
            THERING{Params.indx.(dev2)}.PolynomB=Polyb0_2;
        elseif strcmp(dev2,'KIINJ')
            THERING{Params.indx.(dev2)}.PolynomB=Polyb0_2;
        end
        
        if strcmp(Params.mode,'Online')
            switch2online;
        end
    end
    function ClacY_Callback(source,eventdata)
        Params.corrx=[0;0];
        TestLogic(Params);
        fact=1e3;
        Params.dx=str2double(get(Handels.hcheck_edit_h_x,'String'))/fact;
        Params.dxp=str2double(get(Handels.hcheck_edit_h_xp,'String'))/fact;
        Params.dy=str2double(get(Handels.hcheck_edit_v_y,'String'))/fact;
        Params.dyp=str2double(get(Handels.hcheck_edit_v_yp,'String'))/fact;
                
        if strcmp(Params.mode,'Online')
            [X_online Y_online Cx_online Cy_online]=MeasureAll(Params);
        end        
        switch2sim;
        Orbit0=getorbit;
        Params.Orbit0=Orbit0;
        M=getRespOrbit_y(Params);
        corr=M\[Params.dy; Params.dyp];
        Params.corry=corr;
        dev1=Params.cell_v_M1{1};
        dev2=Params.cell_v_M2{1};
        steppv(dev1,corr(1),Params.cell_v_M1{2});
        steppv(dev2,corr(2),Params.cell_v_M2{2});
        Orbit1=getorbit;
        Params.Orbit1=Orbit1;
        [X Y Cx Cy]=MeasureAll(Params);
        if strcmp(Params.mode,'Online')
            Measures.Cx=Cx+Cx_online;
            Measures.Cy=Cy+Cy_online;
        else
            Measures.Cx=Cx;
            Measures.Cy=Cy;
        end
        Params.Measures=Measures;
        plotdata(Params,Handels);
        Params.getmachineconfig=getmachineconfig;
        steppv(dev1,-corr(1),Params.cell_v_M1{2});
        steppv(dev2,-corr(2),Params.cell_v_M2{2});
        if strcmp(Params.mode,'Online')
            switch2online;
        end
    end
    function StepX_Callback(source,eventdata)
        TestLogic(Params);
        corr=Params.corrx;
        dev1=Params.cell_h_M1{1};
        dev2=Params.cell_h_M2{1};
        
        if strcmp(dev1,'HCM')
            steppv(dev1,corr(1),Params.cell_h_M1{2});
        elseif strcmp(dev1,'SEINJ')
            %in the machine
            steppv(dev1,corr(1));
            % in the model
            Polyb0=THERING{Params.indx.(dev1)}.PolynomB;
            L=THERING{Params.indx.(dev1)}.Length;
            C=Params.magnetcoefficients.SeptumInjection;
            br=Params.brho;
            Polyb1=Polyb0+[(C(end-1)*corr(1))*L/br Polyb0(2:end)];
            THERING{Params.indx.(dev1)}.PolynomB=Polyb1;
        elseif strcmp(dev1,'KIINJ')
            %in the machine
            steppv(dev1,corr(1));
            % in the model
            Polyb0=THERING{Params.indx.(dev1)}.PolynomB;
            L=THERING{Params.indx.(dev1)}.Length;
            C=Params.magnetcoefficients.KickerInjection;
            br=Params.brho;
            Polyb1=Polyb0+[(C(end-1)*corr(1))*L/br Polyb0(2:end)];
            THERING{Params.indx.(dev1)}.PolynomB=Polyb1;
        end
        
        if strcmp(dev2,'HCM')
            steppv(dev2,corr(2),Params.cell_h_M2{2});
        elseif strcmp(dev2,'SEINJ')
            %in the machine
            steppv(dev2,corr(2));
            % in the model
            Polyb0_2=THERING{Params.indx.(dev2)}.PolynomB;
            L=THERING{Params.indx.(dev2)}.Length;
            C=Params.magnetcoefficients.SeptumInjection;
            br=Params.brho;
            Polyb1=Polyb0_2+[(C(end-1)*corr(2))*L/br Polyb0_2(2:end)];
            THERING{Params.indx.(dev2)}.PolynomB=Polyb1;
        elseif strcmp(dev2,'KIINJ')
            %in the machine
            steppv(dev2,corr(2));
            % in the model
            Polyb0_2=THERING{Params.indx.(dev2)}.PolynomB;
            L=THERING{Params.indx.(dev2)}.Length;
            C=Params.magnetcoefficients.KickerInjection;
            br=Params.brho;
            Polyb1=Polyb0_2+[(C(end-1)*corr(2))*L/br Polyb0_2(2:end)];
            THERING{Params.indx.(dev2)}.PolynomB=Polyb1;
        end
        
        [X Y Cx Cy]=MeasureAll(Params);
        Measures.X=X;
        Measures.Y=Y;
        Measures.Cx=Cx;
        Measures.Cy=Cy;
        Params.Measures=Measures;
        plotdata(Params,Handels);
    end
    function StepY_Callback(source,eventdata)
        TestLogic(Params);
        corr=Params.corry;
        dev1=Params.cell_v_M1{1};
        dev2=Params.cell_v_M2{1};
        steppv(dev1,corr(1),Params.cell_v_M1{2});
        steppv(dev2,corr(2),Params.cell_v_M2{2});
        
        [X Y Cx Cy]=MeasureAll(Params);
        Measures.X=X;
        Measures.Y=Y;
        Measures.Cx=Cx;
        Measures.Cy=Cy;
        Params.Measures=Measures;
        plotdata(Params,Handels);
    end
    function Refresh_Callback(source,eventdata)
        [X Y Cx Cy]=MeasureAll(Params);
        Measures.X=X;
        Measures.Y=Y;
        Measures.Cx=Cx;
        Measures.Cy=Cy;
        Params.Measures=Measures;
        plotdata(Params,Handels);
    end
    function Reset_Callback(source,eventdata)
        setpv('HCM',Params.Corr_0.HCM);
        setpv('VCM',Params.Corr_0.VCM);
        setpv('SEINJ',Params.Corr_0.SEINJ);
        THERING{Params.indx.SEINJ}.PolynomB=Params.PolyB_0.SEINJ;
        setpv('KIINJ',Params.Corr_0.KIINJ);
        THERING{Params.indx.KIINJ}.PolynomB=Params.PolyB_0.KIINJ;
        [X Y Cx Cy]=MeasureAll(Params);
        Measures.X=X;
        Measures.Y=Y;
        Measures.Cx=Cx;
        Measures.Cy=Cy;
        Params.Measures=Measures;
        plotdata(Params,Handels);
    end
    function Save_Callback(source,eventdata)
        MConf=getmachineconfig;
        cd(Params.daydir);
        time_name=datestr(now,'yyyymmdd-HHMMSS');
        save(['MachineConfig_' time_name '.mat'],'MConf');
        print('-dpng',['Injection Panel_' time_name '.png']);
    end


set(Handels.f,'HandleVisibility','callback');

end

function TestLogic(Params)

indx_x_1=Params.indx_h_M1;
indx_x_2=Params.indx_h_M2;
indx_x_3=Params.indx_BPMx;
indx_y_1=Params.indx_v_M1;
indx_y_2=Params.indx_v_M2;
indx_y_3=Params.indx_BPMy;

if indx_x_1>indx_x_2
    error('First horizontal magnet after the second one...');
elseif indx_x_1>indx_x_3
    error('First horizontal magnet after the BPM x...');
elseif indx_x_2>indx_x_3
    error('Second horizontal magnet after the BPM x...');
elseif indx_x_1==indx_x_2
    error('Select two diferent horizontal magnets...');
elseif indx_y_1>indx_y_2
    error('First vertical magnet after the second one...');
elseif indx_y_1>indx_y_3
    error('First vertical magnet after the BPM x...');
elseif indx_y_2>indx_y_3
    error('Second vertical magnet after the BPM x...');
elseif indx_y_1==indx_y_2
    error('Select two diferent vertical magnets...');
end

end
function [X Y Cx Cy]=MeasureAll(Params)
global THERING;

My=Params.My;
Mx0=Params.Mx0;
Mxs=Params.Mxs;
Mxk=Params.Mxk;
Cy=100*getpv('VCM')./My;
Cx0=100*getpv('HCM')./Mx0;


localmode=getmode('HCM');
if strcmp(localmode,'Simulator')
    br=Params.brho;
    L=THERING{Params.indx.SEINJ}.Length;
    C=Params.magnetcoefficients.SeptumInjection;
    Cxs=(THERING{Params.indx.SEINJ}.PolynomB(1))*br/C(end-1)/L;
    L=THERING{Params.indx.KIINJ}.Length;
    C=Params.magnetcoefficients.KickerInjection;
    Cxk=(THERING{Params.indx.KIINJ}.PolynomB(1))*br/C(end-1)/L;
elseif strcmp(localmode,'Online')
    Cxs=getpv('SEINJ')-Params.SeptumSettingRef;
    Cxk=getpv('KIINJ')-Params.KickerSettingRef;
end

Cx=[Cx0; 100*Cxs/Mxs;100*Cxk/Mxk];
X=getx;
Y=gety;

end
function Orbit=getorbit
global THERING;
TwissData = getpvmodel('TwissData');
x0 = [TwissData.ClosedOrbit(:); TwissData.dP; TwissData.dL];
Orbit = linepass(THERING, x0, 1:(numel(THERING)+1));
end
function M=getRespOrbit_x(Params)

global THERING;
Orbit0=getorbit;
dev1=Params.cell_h_M1{1};
dev2=Params.cell_h_M2{1};


if strcmp(dev1,'HCM')
    damp1=0.01;
    steppv(dev1,damp1,Params.cell_h_M1{2});
elseif strcmp(dev1,'SEINJ')
    damp1=0.1;
    Polyb0=THERING{Params.indx.(dev1)}.PolynomB;
    L=THERING{Params.indx.(dev1)}.Length;
    C=Params.magnetcoefficients.SeptumInjection;
    br=Params.brho;
    Polyb1=Polyb0+[(C(end-1)*damp1)*L/br Polyb0(2:end)];
    THERING{Params.indx.(dev1)}.PolynomB=Polyb1;
    
elseif strcmp(dev1,'KIINJ')
    damp1=1;
    Polyb0=THERING{Params.indx.(dev1)}.PolynomB;
    L=THERING{Params.indx.(dev1)}.Length;
    C=Params.magnetcoefficients.KickerInjection;
    br=Params.brho;
    Polyb1=Polyb0+[(C(end-1)*damp1)*L/br Polyb0(2:end)];
    THERING{Params.indx.(dev1)}.PolynomB=Polyb1;
end

Orbit1=getorbit;

if strcmp(dev1,'HCM')
    damp1=0.01;
    steppv(dev1,-damp1,Params.cell_h_M1{2});
elseif strcmp(dev1,'SEINJ')
    damp1=0.1;
    THERING{Params.indx.(dev1)}.PolynomB=Polyb0;
elseif strcmp(dev1,'KIINJ')
    damp1=1;
    THERING{Params.indx.(dev1)}.PolynomB=Polyb0;
end

if strcmp(dev2,'HCM')
    damp2=0.01;
    steppv(dev2,damp2,Params.cell_h_M2{2});
elseif strcmp(dev2,'SEINJ')
    damp2=0.1;
    Polyb0_2=THERING{Params.indx.(dev2)}.PolynomB;
    L=THERING{Params.indx.(dev2)}.Length;
    C=Params.magnetcoefficients.SeptumInjection;
    br=Params.brho;
    Polyb1=Polyb0_2+[(C(end-1)*damp2)*L/br Polyb0_2(2:end)];
    THERING{Params.indx.(dev2)}.PolynomB=Polyb1;
elseif strcmp(dev2,'KIINJ')
    damp2=1;
    Polyb0_2=THERING{Params.indx.(dev2)}.PolynomB;
    L=THERING{Params.indx.(dev2)}.Length;
    C=Params.magnetcoefficients.KickerInjection;
    br=Params.brho;
    Polyb1=Polyb0_2+[(C(end-1)*damp2)*L/br Polyb0_2(2:end)];
    THERING{Params.indx.(dev2)}.PolynomB=Polyb1;
end

Orbit2=getorbit;

if strcmp(dev2,'HCM')
    damp2=0.01;
    steppv(dev2,-damp2,Params.cell_h_M2{2});
elseif strcmp(dev2,'SEINJ')
    THERING{Params.indx.(dev2)}.PolynomB=Polyb0_2;
elseif strcmp(dev2,'KIINJ')
    THERING{Params.indx.(dev2)}.PolynomB=Polyb0_2;
end

Dx2=Orbit2(1,Params.indx_BPMx)-Orbit0(1,Params.indx_BPMx);
Dx1=Orbit1(1,Params.indx_BPMx)-Orbit0(1,Params.indx_BPMx);
Dxp2=Orbit2(2,Params.indx_BPMx)-Orbit0(2,Params.indx_BPMx);
Dxp1=Orbit1(2,Params.indx_BPMx)-Orbit0(2,Params.indx_BPMx);
M=[Dx1/damp1 Dx2/damp2;Dxp1/damp1 Dxp2/damp2];

end
function M=getRespOrbit_y(Params)

Orbit0=getorbit;
dev1=Params.cell_v_M1{1};
dev2=Params.cell_v_M2{1};



damp1=0.01;
steppv(dev1,damp1,Params.cell_v_M1{2});
Orbit1=getorbit;
steppv(dev1,-damp1,Params.cell_v_M1{2});


damp2=0.01;
steppv(dev2,damp2,Params.cell_v_M2{2});
Orbit2=getorbit;
steppv(dev2,-damp2,Params.cell_v_M2{2});


Dx2=Orbit2(3,Params.indx_BPMy)-Orbit0(3,Params.indx_BPMy);
Dx1=Orbit1(3,Params.indx_BPMy)-Orbit0(3,Params.indx_BPMy);
Dxp2=Orbit2(4,Params.indx_BPMy)-Orbit0(4,Params.indx_BPMy);
Dxp1=Orbit1(4,Params.indx_BPMy)-Orbit0(4,Params.indx_BPMy);
M=[Dx1/damp1 Dx2/damp2;Dxp1/damp1 Dxp2/damp2];

end
function plotdata(Params,Handels)
Measures=Params.Measures;
X=Measures.X;
Y=Measures.Y;
Cx=Measures.Cx;
Cy=Measures.Cy;

axes(Handels.ax1);
cla;
hold on;
ssBPM=Params.ss(Params.indx.BPM);
stem(ssBPM,X,'r');stem(ssBPM,Y,'b');
box on;
set(Handels.ax1,'xtick',[]);
set(Handels.ax1,'Tag','ax1','Color','none');
ylabel(Handels.ax1,'BPMs (mm)','Color','k');
set(Handels.ax1,'Units','normalized','YColor',[0 0 0],'XColor',[0 0 0]);
xx=1000*(Params.Orbit1(1,:)-Params.Orbit0(1,:));
yy=1000*(Params.Orbit1(3,:)-Params.Orbit0(3,:));
mx=max(abs([X' Y' xx yy]));
plot(Params.ss,xx,'--r');
plot(Params.ss,yy,'--b');
if mx==0
    yl=ylim;
    ylim([-max(abs(yl)) max(abs(yl))]);
else
    ylim([-mx mx]);
end
axes(Handels.ax2);
cla;
hold on;
h=bar([Params.ss(Params.indx.HCM) Params.ss(Params.indx.SEINJ) Params.ss(Params.indx.KIINJ)],Cx,'r');set(h,'BarWidth',0.1);
h=bar(Params.ss(Params.indx.VCM),Cy,'b');set(h,'BarWidth',0.05);
ylabel(Handels.ax2,'Corr. Strength (%)');
set(Handels.ax2,'xtick',[]);
mx=max(abs([Cx; Cy]));
if mx==0
    yl=ylim;
    ylim([-max(abs(yl)) max(abs(yl))]);
else
    ylim([-max(abs([Cx; Cy])) max(abs([Cx;Cy]))]);
end
end
