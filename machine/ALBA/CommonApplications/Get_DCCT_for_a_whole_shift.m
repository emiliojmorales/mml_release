

%**************************************************%
nn0=8;
name=['DCCT_for_a_while_whole_shift' num2str(nn0) '.mat'];

%**************************************************%


p0=get_ccg_presures;
ao=getao;
id_num=ao.BPMx.GroupId;
ii=0;
tic;
t=0;
Data_str=struct;
while(true)
    ii=ii+1;
    t1=toc;
    res0=tango_read_attribute('sr/di/dcct','AverageCurrent');
    I=res0.value;
    pf=get_ccg_presures;
    res=tango_group_read_attribute(id_num,'SumSA');
    t2=toc;
    blm=getpv('BLM');
    res2=tango_group_read_attribute(id_num,'Gain');
    res3=tango_read_attribute('motor/sr_ipapfshscrapers_ctrl/3','Position');
    [vs pows]=ReadVoltageRFs;
    Gaps=ReadGapsIds;
    nbpm=numel(res.replies);
    bpm_sumsa=zeros(nbpm,1);
    bpm_gain=zeros(nbpm,1);
    for kk=1:nbpm
        if isempty(res.replies(kk).error)
            bpm_sumsa(kk)=res.replies(kk).value;
            bpm_gain(kk)=res2.replies(kk).value(1);
        else
            bpm_sumsa(kk)=nan;
            bpm_gain(kk)=nan;
        end
    end
    t=(t1+t2)/2;
    Data_str.scr(ii)=res3.value(1);
    Data_str.t(ii)=t;
    Data_str.blm(:,ii)=blm;
    Data_str.presure(:,ii)=pf';
    Data_str.SumSA(:,ii)=bpm_sumsa;
    Data_str.Gain(:,ii)=bpm_gain;
    Data_str.I(ii)=I;
    Data_str.VRF(:,ii)=vs';
    Data_str.PowRF(:,ii)=pows';
    Data_str.Gaps(:,ii)=Gaps;
    pause(1);
    if (floor(ii/60)-ii/60)<eps
        save(name,'Data_str');
        c=clock;
    end
    if c(4)==22
        nn0=nn0+1;
        name=['DCCT_for_a_while_whole_shift' num2str(nn0) '.mat'];
        pause(9*3600);
        ii=0;
        tic;
        t=0;
        Data_str=struct;
    end
end



