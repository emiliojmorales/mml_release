function [devlist status]=ReadBadBPMs()
% removes bad bpms from the ao.
% reads them from [ad.Directory.OpsData '/bpmstat.txt']
% also creates a new tango group with the new bpm list
devlist=[];
status=[];
ad=getad;
[header, data] = hdrload([ad.Directory.OpsData '/bpmstat.txt']);
nbpms=size(data,1);
if nbpms,
    status=data(:,3);
    devlist=data(:,1:2);
    setfamilydata(status, 'BPMx','Status', devlist);
    setfamilydata(status, 'BPMy','Status', devlist);
    %% now we have to destroy the old group and create the new one
    [statuss WHO] = system('whoami');
    if strncmp(WHO, 'operator',8),
        ControlRoomFlag = 1;
        Mode = 'Online';
    else
        ControlRoomFlag = 0;
        Mode = 'Simulator';
    end
%     ao=getao;
%     if ControlRoomFlag
%         tango_group_kill(ao.BPMx.GroupId);
%         tango_group_kill(ao.BPMy.GroupId);
%         ao.BPMx.GroupId = tango_group_create2('BPMx');
%         tango_group_add(ao.BPMx.GroupId,ao.BPMx.DeviceName(logical(ao.BPMx.Status))');
%         ao.BPMy.GroupId = tango_group_create2('BPMy');
%         tango_group_add(ao.BPMy.GroupId,ao.BPMy.DeviceName(logical(ao.BPMy.Status))');
%     end
%     setao(ao);
end