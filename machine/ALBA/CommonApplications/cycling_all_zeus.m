
function cycling_all_zeus
% 2011 Created by Z.Marti, 


[s,v] = listdlg('PromptString','Select Fimilies to cycle:','SelectionMode','single','ListString',{'QUAD','SEXT','BEND','QUAD+SEXT','All'});

if s==1
    fam=findmemberof('QUAD');
    min=num2cell(20*ones(numel(fam),1));
    max=num2cell(200*ones(numel(fam),1));
elseif s==2
    fam=findmemberof('SEXT');
    min=num2cell(30*ones(numel(fam),1));
    max=num2cell(215*ones(numel(fam),1));
elseif s==3
    fam={'BEND'};
    min=num2cell(35*ones(numel(fam),1));
    max=num2cell(600*ones(numel(fam),1));
elseif s==4
    q=findmemberof('QUAD');
    s=findmemberof('SEXT');
    minq=num2cell(20*ones(numel(q),1));
    maxq=num2cell(200*ones(numel(q),1));
    mins=num2cell(30*ones(numel(s),1));
    maxs=num2cell(215*ones(numel(s),1));
    min={minq{:},mins{:}}';
    max={maxq{:},maxs{:}}';
    fam={q{:},s{:}}';
elseif s==5
    q=findmemberof('QUAD');
    s=findmemberof('SEXT');
    minq=num2cell(20*ones(numel(q),1));
    maxq=num2cell(200*ones(numel(q),1));
    mins=num2cell(30*ones(numel(s),1));
    maxs=num2cell(215*ones(numel(s),1));
    minb={35};
    maxb={600};
    min={minq{:},mins{:},minb{:}}';
    max={maxq{:},maxs{:},maxb{:}}';
    fam={q{:},s{:},'BEND'}';
end

cycling_2(fam,min,max);


end
