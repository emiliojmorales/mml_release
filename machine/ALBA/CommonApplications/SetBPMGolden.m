function SetBPMGolden(varargin)
% SetBPMGolden(varargin)
% Optional Flags:
% 'File' - Get it from file
%  example: [x y] =
%   SetBPMGolden('File','....
%   /data/MML/Release/measdata/ALBA/StorageRing/BPM/BPM_Golden_2011-05-23_16-57-26.mat')
% 'Measure' - Get it from the current orbit
% 'Zero'    - Set it to 0
% 'Golden' - from the file GoldenBPMOrbit in OpsData

if nargin==0,
    help SetBPMGolden
    return
else
    if strcmp(varargin(1),'Measure'),
        [BPMx, BPMy] = getbpm('Struct','NoArchive');
        Data=BPMx.Data;
        FamilyName='BPMx';
        DeviceList=BPMx.DeviceList;
        setfamilydata(Data, FamilyName,'Monitor','Golden', DeviceList);
        Data=BPMy.Data;
        FamilyName='BPMy';
        DeviceList=BPMy.DeviceList;
        setfamilydata(Data, FamilyName,'Monitor','Golden', DeviceList);
    elseif strcmp(varargin(1),'Zero'),
        [BPMx, BPMy] = getbpm('Struct','NoArchive');
        Data=0*BPMx.Data;
        FamilyName='BPMx';
        DeviceList=BPMx.DeviceList;
        setfamilydata(Data, FamilyName,'Monitor','Golden', DeviceList);
        Data=0*BPMy.Data;
        FamilyName='BPMy';
        DeviceList=BPMy.DeviceList;
        setfamilydata(Data, FamilyName,'Monitor','Golden', DeviceList);
    elseif strcmp(varargin(1),'Golden'),
        FileName = [getfamilydata('OpsData','BPMGoldenFile'),'.mat'];
        DirectoryName = getfamilydata('Directory','OpsData');
        data=load([DirectoryName FileName]);
        Data=data.Xgolden.Data;
        FamilyName='BPMx';
        DeviceList=data.Xgolden.DeviceList;
        setfamilydata(Data, FamilyName,'Monitor','Golden', DeviceList);
        Data=data.Ygolden.Data;
        FamilyName='BPMy';
        DeviceList=data.Ygolden.DeviceList;
        setfamilydata(Data, FamilyName,'Monitor','Golden', DeviceList);
    elseif strcmp(varargin(1),'File')
        if  length(varargin) > 1
            golden_file = varargin(2);
        else
            ad=getad;
            [filename, pathname, filterindex] = uigetfile('BPM_Golden*.mat', 'Pick the save GoldenBPMOrbit',ad.Directory.BPMData);
            golden_file = [pathname filename];
        end
        data=load(golden_file);
        Data=data.Xgolden.Data;
        FamilyName='BPMx';
        DeviceList=data.Xgolden.DeviceList;
        setfamilydata(Data, FamilyName,'Monitor','Golden', DeviceList);
        Data=data.Ygolden.Data;
        FamilyName='BPMy';
        DeviceList=data.Ygolden.DeviceList;
        setfamilydata(Data, FamilyName,'Monitor','Golden', DeviceList);
    else
        fprintf('Usage:\n')
        help SetBPMGolden
    end
end
end
