
function [Inew I0]=LocalOrbitCorrection(CorrCell,Atribute,x0,y0,iter,time_wait,percentatge)

global DI_corr ;
% Initial setings
ncorr=numel(CorrCell);
I0=zeros(ncorr,1);
b=zeros(ncorr,1);
R=zeros(ncorr);
id=eye(ncorr);

for ii=1:ncorr
    str=tango_read_attribute2(CorrCell{ii},Atribute);
    I0(ii)=str.value(1);
    if abs(I0(ii))>0.1
        tango_write_correctors(CorrCell,Atribute,zeros(ncorr,1));
        pause(time_wait);
        str=tango_read_attribute2(CorrCell{ii},Atribute);
        I0(ii)=str.value(1);
        if abs(I0(ii))>0.1
            tango_write_correctors(CorrCell,Atribute,zeros(ncorr,1));
            pause(time_wait);
            str=tango_read_attribute2(CorrCell{ii},Atribute);
            I0(ii)=str.value(1);
        end
    end
end
x=getx;
y=gety;
D_corr=mean(abs([x0-x;y0-y]));
fprintf('Iteration %d: Orbit error= %f mm\n',0,D_corr);
try
    % settings for the correction
    %loop
    ii=0;
    while ii<iter
        ii=ii+1;
        pause(time_wait/2);
        
        [I C]=MatrixOrbitCorrection(CorrCell,Atribute,x,y,x0,y0,time_wait,ii);
        pause(time_wait);
        x=getx;
        y=gety;
        if mean(abs([x0-x;y0-y]))>D_corr
            fprintf('No efficient correction found...\n');
            tango_write_correctors(CorrCell,Atribute,I);
            pause(time_wait);
            x=getx;
            y=gety;
            DI_corr=DI_corr/2;
        else
            D_corr=mean(abs([x0-x;y0-y]));
        end
        fprintf('Iteration %d: Orbit error= %f mm\n',ii,mean(abs([x0-x;y0-y])));
    end
    Inew=I+C;
catch msserr
    disp(msserr)
    tango_write_correctors(CorrCell,Atribute,I0);
end

end

function tango_write_correctors(CorrCell,Atribute,I)
ncorr=numel(CorrCell);
for ii=1:ncorr
    tango_write_attribute2(CorrCell{ii},Atribute,I(ii));
end
end


function [I0 C]=MatrixOrbitCorrection(CorrCell,Atribute,x,y,x0,y0,time_wait,nn)
global DI_corr;
ncorr=numel(CorrCell);
I0=zeros(ncorr,1);
for ii=1:ncorr
    str=tango_read_attribute2(CorrCell{ii},Atribute);
    I0(ii)=str.value(1);
end
DI=DI_corr/nn;

nbpm=numel(x);
M=zeros(2*nbpm,ncorr);
for ii=1:ncorr
    tango_write_attribute2(CorrCell{ii},Atribute,I0(ii)+DI);
    pause(time_wait);
    M(:,ii)=[getx-x;gety-y]/DI;
    tango_write_attribute2(CorrCell{ii},Atribute,I0(ii));
    pause(time_wait);
end
[U,S,V] = svd(M);
ns=min(size(S));
s=diag(S);
alpha=s(floor(ns/2));
%iM=invSVD(U,S,V,alpha);
iM=invSVD(U,S,V,3);
% line optimization
C=1.5*iM*[x0-x;y0-y];
n_steps_lin=11;
cc=zeros(ncorr,n_steps_lin);
cost=zeros(n_steps_lin,1);
for ii=1:n_steps_lin
    cc(:,ii)=C*(ii-1)/(n_steps_lin-1);
    for jj=1:ncorr
        tango_write_attribute2(CorrCell{jj},Atribute,I0(jj)+cc(jj,ii));
    end
    pause(time_wait);
    cost(ii)=norm([getx-x0;gety-y0]);
end
[m imin]=min(cost);
C=cc(:,imin);
for jj=1:ncorr
    tango_write_attribute2(CorrCell{jj},Atribute,I0(jj)+C(jj));
end

end


function inv_R=invSVD(U,S,V,varargin)
% Perform an SVD inversion with n singular values
nargin=numel(varargin);
if nargin==1
    v=varargin{1};
    if floor(v)==v
        n=v;
        alpha=0;
    else
        n=size(S,1);
        alpha=v;
    end
else
    error('wrong number of variables');
end
small=1e-16;
sd=diag(S);
in_sd0=find(abs(sd)<small);
d=diag(S)./(diag(S).^2+alpha^2);
%plot(d,'-r.');hold on;plot(1./diag(S),'-k.');
d(in_sd0)=zeros(length(in_sd0),1);
ndiag=length(d);
d((n+1):ndiag)=zeros(ndiag-n,1);
ind_S_0=diag(d);
ind_S=S';
ind_S(1:ndiag,1:ndiag)=ind_S_0;
inv_R=V*ind_S*(U');
end