function [ x, y, I ] = OrbitatTurn( file, turn )
% returns the co orbit at the given turn for a saved file with all the BPM
% DD data (zeus format)
%ORBITATTURN Summary of this function goes here
%   Detailed explanation goes here
data=load(file);
turns=1000;
x=[];
y=[];
I=[];
if (turn+turns)  < length(data.M_x(:,1))
    x=mean(data.M_x(turn+(1:turns),:));
    y=mean(data.M_y(turn+(1:turns),:));
    I=mean(data.M_I(turn+(1:turns),:));
else
    disp 'No enought turns in the data file'
end
end

