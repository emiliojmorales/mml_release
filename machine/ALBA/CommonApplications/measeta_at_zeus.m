function disp=measeta_at_zeus(index_elm)

global THERING;
indx = 1:(length(THERING)+1);
if nargin == 1
    indx = index_elm;
end


dp=0.0000001;
Nd=10;
Ne=length(indx);
d=zeros(Nd,1);
x=zeros(Ne,Nd);
disp=zeros(Ne,4);

for ii=1:Nd
    d(ii)=dp*(2*(ii-1)-(Nd-1))/(Nd-1);
    cod=findorbit4(THERING,d(ii),indx);
    x(:,ii)=cod(1,:)';
    px(:,ii)=cod(2,:)';
    y(:,ii)=cod(3,:)';
    py(:,ii)=cod(4,:)';
end

for ii=1:Ne
    p=polyfit(d,x(ii,:)',1);
    p2=polyfit(d,px(ii,:)',1);
    p3=polyfit(d,y(ii,:)',1);
    p4=polyfit(d,py(ii,:)',1);
    disp(ii,:)=[p(1) p2(1) p3(1) p4(1)];
end


end