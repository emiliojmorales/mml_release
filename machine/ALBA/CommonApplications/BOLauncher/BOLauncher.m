function f = BOLauncher
% f = BOLauncher
% author: M. Munoz, 15 Jul 2009.
% close already running versions
oldfig = findobj(allchild(0),'tag','BOLauncher');
if ~isempty(oldfig), delete(oldfig); end
%create the figure, and the panels
f = figure('menubar','none', 'Position', [0 0 1200 600],'Resize', 'Off','Tag','ltb_optic',...
    'Interruptible', 'on', ...
    'HandleVisibility','off', ...
    'Name', 'Booster Launcher', ...
    'NumberTitle','off','tag','BOLauncher');
handles = guihandles(f);
handles.f = f;
handles.mainpanel = uipanel('Parent', f);
columns=5;
loop=1;
handles.app{loop}=[{'Plot Family'},{'plotfamily'},{'Launch PlotFamily'}];
loop=loop+1;
handles.app{loop}=[{'Measure Dispersion'},{'measdisp'},{'Launch PlotFamily'}];
loop=loop+1;
handles.app{loop}=[{'Tune Single Turn'},{'TuneSingleTurnGUI'},{'launch TuneSingleTurnGUI'}];
loop=loop+1;
handles.app{loop}=[{'Tune Ramp Average'},{'TuneRampAveraging'},{'launch TuneRampAveraging'}];
loop=loop+1;
handles.app{loop}=[{'Tune Ramp'},{'TuneRampSingleMeasure'},{'launch TuneRampSingleMeasure'}];
loop=loop+1;
handles.app{loop}=[{'Orbit Correction (CO)'},{'setorbitgui'},{'Launch setorbitgui'}];
loop=loop+1;
handles.app{loop}=[{'Bump Application'},{'setorbitbumpgui'},{'setorbitbumpgui'}];
loop=loop+1;
% handles.app{loop}=[{'Orbit Correction (Few Turns)'},{'BoosterFewTurnsOrbitCorrectionGUI'},{'launch BoosterFewTurnsOrbitCorrectionGUI'}];
% loop=loop+1;
handles.app{loop}=[{'Measure LOCO Data'},{'measlocodata'},{'launch measlocodata'}];
loop=loop+1;
handles.app{loop}=[{'Scale Magnets'},{'ScaleMagnets'},{'launch ScaleMagnets'}];
loop=loop+1;
handles.app{loop}=[{'Tune Move Panel'},{'TuneMovePanel'},{'launch TuneMovePanel'}];
loop=loop+1;
handles.app{loop}=[{'Save Machine Configuration'},{'savemachineconfig'},{'launch savemachineconfig'}];
loop=loop+1;
handles.app{loop}=[{sprintf('Orbit during ramp \n(all ramp BPMs buffers)')},{'getBPMs_Not_freeze'},{'launch getBPMs_Not_freeze'}];
loop=loop+1;
handles.app{loop}=[{sprintf('Orbit during ramp \n(changing BPMs offsets)')},{'getOrbitRamping_group'},{'launch getOrbitRamping_group'}];

nrows=ceil(length(handles.app)/columns);
y0=0.85;
dx=0.04;
dy=dx;
bwidth=(1-dx*(columns+1))/columns;
bheight=0.1;
nbutton=1;
for loop=1: nrows-1;
    x0=dx;
    for loop2=1:columns,
    handles.button(nbutton)=uibutton('Style','pushbutton', 'Parent', handles.mainpanel,'String', handles.app{nbutton}{1}, ...
        'ForegroundColor','b', 'CallBack', @button_Callback,'Units','normalized','Position',[x0 y0 bwidth bheight], ...
        'UserData', handles.app{nbutton}{2},'TooltipString', handles.app{nbutton}{3});
    x0=x0+dx+bwidth;
    nbutton=nbutton+1;
    end
    y0=y0-dy-bheight;
end
x0=dx;
for loop=1:(length(handles.app)-columns*(nrows-1)),
  
   handles.button(nbutton)=uibutton('Style','pushbutton', 'Parent', handles.mainpanel,'String', handles.app{nbutton}{1}, ...
        'ForegroundColor','b', 'CallBack', @button_Callback,'Units','normalized','Position',[x0 y0 bwidth bheight], ....
        'UserData', handles.app{nbutton}{2},'TooltipString', handles.app{nbutton}{3});
    x0=x0+dx+bwidth;
    nbutton=nbutton+1;
end
guidata(f, handles);

function button_Callback(hObject, eventdata, handles)
eval(get(hObject,'UserData'));
