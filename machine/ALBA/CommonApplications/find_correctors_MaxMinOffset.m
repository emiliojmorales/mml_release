
T=0.320;
n_t_points=7501;

MC=getmachineconfig();
AO=getao();
hcm_names=AO.HCM.DeviceName;
hcm_common_names=AO.HCM.CommonNames;
hcm_status=AO.HCM.Status;
hcm_names=hcm_names(hcm_status==1);
hcm_common_names=hcm_common_names(hcm_status==1,:);
n_hcm=length(hcm_names);

vcm_names=AO.VCM.DeviceName;
vcm_common_names=AO.VCM.CommonNames;
vcm_status=AO.VCM.Status;
vcm_names=vcm_names(vcm_status==1);
vcm_common_names=vcm_common_names(vcm_status==1,:);
n_vcm=length(vcm_names);

hcm_I=zeros(n_hcm,1);
vcm_I=zeros(n_vcm,1);
hcm_Imax=zeros(n_hcm,1);
vcm_Imax=zeros(n_vcm,1);

for ii=1:n_hcm
    str_bpm=tango_read_attribute2(hcm_names{ii},'Current');
    hcm_I(ii)=str_bpm.value(1);
end

for ii=1:n_vcm
    str_bpm=tango_read_attribute2(vcm_names{ii},'Current');
    vcm_I(ii)=str_bpm.value(1);
end

%find maximums
time=[0 0.02  T/2 T-0.02 T];
hcm_Imax=30*hcm_I/12;
vcm_Imax=30*vcm_I/11;

hcm_IRamp=[hcm_I hcm_I hcm_Imax hcm_I hcm_I];
vcm_IRamp=[vcm_I vcm_I vcm_Imax vcm_I vcm_I];
ts=(0:n_t_points-1)*T/n_t_points;
daydir=['/data/BOOSTER/' datestr(now,'yyyymmdd') '/CorrectorsWaveForm'];
mkdir(daydir);
cd(daydir);
phi0=2*pi*(13.7e-3)/T;
figure(44);


% first pair
ii=1;
%hcm_Imin=hcm_I(ii)/2; % /2 comes from injecting at I=2*Imin in the bending
hcm_Imin=(2*hcm_I(ii)-hcm_Imax(ii)*(1-cos(phi0)))/(1+cos(phi0));
hcm_I_single=(hcm_Imax(ii)-hcm_Imin).*(1-cos(2*pi*(ts)/T+phi0))/2+hcm_Imin;
filename=['RamCorr_Triangle_' hcm_common_names(ii,:) '.dat'];
fid=fopen(filename,'w+');
fprintf(fid,'# DATASET= "RamCorr_Triangle" \n');
fprintf(fid,'# SNAPSHOT_TIME= ---- \n');
fprintf(fid, '%3.6f %3.6f \n', [ts ; hcm_I_single]);
fclose(fid);
plot(ts,hcm_I_single,'-r');hold on;
%vcm_Imin=vcm_I(ii)/2;   % /2 comes from injecting at I=2*Imin in the bending
vcm_Imin=(2*vcm_I(ii)-vcm_Imax(ii)*(1-cos(phi0)))/(1+cos(phi0));
vcm_I_single=(vcm_Imax(ii)-vcm_Imin).*(1-cos(2*pi*(ts)/T+phi0))/2+vcm_Imin;
filename=['RamCorr_Triangle_' vcm_common_names(ii,:) '.dat'];
fid=fopen(filename,'w+');
fprintf(fid,'# DATASET= "RamCorr_Triangle" \n');
fprintf(fid,'# SNAPSHOT_TIME= ---- \n');
fprintf(fid, '%3.6f %3.6f \n', [ts ; vcm_I_single]);
fclose(fid);
plot(ts,vcm_I_single,'-b');hold on;
legend('Hor. Corr.','Ver. Corr.');
xlabel('time (s)');
ylabel('Curr (A)');



for ii=2:n_hcm
%hcm_I_single = interp1(time',hcm_IRamp(ii,:)',ts','linear','extrap'); 
%hcm_Imin=hcm_I(ii)/2; % /2 comes from injecting at I=2*Imin in the bending
hcm_Imin=(2*hcm_I(ii)-hcm_Imax(ii)*(1-cos(phi0)))/(1+cos(phi0));
hcm_I_single=(hcm_Imax(ii)-hcm_Imin).*(1-cos(2*pi*(ts)/T+phi0))/2+hcm_Imin;
filename=['RamCorr_Triangle_' hcm_common_names(ii,:) '.dat'];
fid=fopen(filename,'w+');
fprintf(fid,'# DATASET= "RamCorr_Triangle" \n');
fprintf(fid,'# SNAPSHOT_TIME= ---- \n');
fprintf(fid, '%3.6f %3.6f \n', [ts ; hcm_I_single]);
fclose(fid);
plot(ts,hcm_I_single,'-r');hold on;
end

for ii=2:n_vcm
%vcm_I_single = interp1(time',vcm_IRamp(ii,:)',ts','linear','extrap'); 
%vcm_Imin=vcm_I(ii)/2;   % /2 comes from injecting at I=2*Imin in the bending
vcm_Imin=(2*vcm_I(ii)-vcm_Imax(ii)*(1-cos(phi0)))/(1+cos(phi0));
vcm_I_single=(vcm_Imax(ii)-vcm_Imin).*(1-cos(2*pi*(ts)/T+phi0))/2+vcm_Imin;
filename=['RamCorr_Triangle_' vcm_common_names(ii,:) '.dat'];
fid=fopen(filename,'w+');
fprintf(fid,'# DATASET= "RamCorr_Triangle" \n');
fprintf(fid,'# SNAPSHOT_TIME= ---- \n');
fprintf(fid, '%3.6f %3.6f \n', [ts ; vcm_I_single]);
fclose(fid);
plot(ts,vcm_I_single,'-b');hold on;
end

hold off;
filename=['Correctors_setpoints_ramp.dat'];
fid=fopen(filename,'w+');
fprintf(fid,'# Name               State    Setpoint Ramp  WaveOffset  Waveform\n');

for ii=1:n_hcm
filenamei=['RamCorr_Triangle_' hcm_common_names(ii,:) '.dat'];
fprintf(fid,'%s \t',lower(hcm_names{ii}));
fprintf(fid,'on \t');
fprintf(fid,'0.0 \t');
fprintf(fid,'on \t');
fprintf(fid,'0.0 \t');
fprintf(fid, '%s \n', filenamei);
end

for ii=1:n_vcm
filenamei=['RamCorr_Triangle_' vcm_common_names(ii,:) '.dat'];
fprintf(fid,'%s \t',lower(vcm_names{ii}));
fprintf(fid,'on \t');
fprintf(fid,'0.0 \t');
fprintf(fid,'on \t');
fprintf(fid,'0.0 \t');
fprintf(fid, '%s \n', filenamei);
end

fclose(fid);



