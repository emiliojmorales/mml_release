function data = RecoverDataArchiver( device, DateStart, DateEnd )
% data = RecoverDataArchiver( device, DateStart, DateEnd )
startt=datestr(datenum(DateStart),'yyyy-mm-dd HH:MM');
endd=datestr(datenum(DateEnd),'yyyy-mm-dd HH:MM');
filename=[strrep(device,'/','_') '.csv'];
system(['archiving2csv  sr/di/dcct/averagecurrent ' device ' "',startt,'" "',endd,'"  ./' filename]);


data=LoadArchFile(filename);



end

