/* Function find_Twiss.c*/

#include <string.h>
#include <math.h>
#include "mex.h"
#include "matrix.h"
#include "nrutil.h"
#include "nrutil.c"
#include "funcions_zeus.c"




/*Definitions*/

/*
 *  [b a g d dp]=find_Twiss(ang,L,K,e1,e2,b0,a0,g0,d0,dp0);
 *
 *
 *
 * b[nelm]: beta function at the end of every element
 * a[nelm]: alpha function at the end of every element
 * g[nelm]: gamma function at the end of every element
 * d[nelm]: dispersion function at the end of every element
 * dp[nelm]: derivate of the dispersion function at the end of every element
 *
 * ang[nelm]: defelecting angle of each element
 * L[nelm]: length of each element
 * e1[nelm]: entrance angle of each element
 * e2[nelm]: exit angle of each element
 * K[nelm]: quadrupole of each element
 * b0: beta function at the start of the line
 * a0: alpha function at the start of the line
 * g0: gamma function at the start of the line
 * d0: dispersion function at the start of the line
 * dp0: derivate of the dispersion function at the start of the line
 *
 *
 */


/************************************************************************/
/************************************************************************/
/*************************** Functions **********************************/
/************************************************************************/
/************************************************************************/


double S_functions(double ang, double L, double K, double **Mt, double **Md) {
    double S, C, Sp, Cp, D, Dp, k, rho,kn,csi,akn;
    
    if(ang==0) {
        if(K==0) {
            S=L;
            C=1;
            Cp=0;
            Sp=1;
            D=0;
            Dp=0;
        }
        else {
            if (K<0) {
                k=sqrt(fabs(K));
                S=sinh(k*L)/k;
                C=cosh(k*L);
                Sp=cosh(k*L);
                Cp=k*sinh(k*L);
                D=0;
                Dp=0;
            }
            else if(K>=0) {
                k=sqrt(K);
                S=sin(k*L)/k;
                C=cos(k*L);
                Sp=cos(k*L);
                Cp=-k*sin(k*L);
                D=0;
                Dp=0;
            }
        }
    }
    else {
        rho=L/ang;
        if(K==0) {
            S=rho*sin(ang);
            C=cos(ang);
            Cp=-sin(ang)/rho;
            Sp=sin(ang);
            D=rho*(1-cos(ang));
            Dp=sin(ang);
        }
        else {
            kn=K+1/(rho*rho);
            csi=sqrt(fabs(kn))*L;
            if (kn<0) {
                akn=sqrt(fabs(kn));
                S=sinh(csi)/akn;
                C=cosh(csi);
                Sp=cosh(csi);
                Cp=akn*sinh(csi);
                D=(cosh(csi)-1)/rho/fabs(kn);
                Dp=sinh(csi)/akn/rho;
            }
            else if(kn>=0) {
                akn=sqrt(kn);
                S=sin(csi)/akn;
                C=cos(csi);
                Sp=cos(csi);
                Cp=-akn*sin(csi);
                D=(cos(csi)-1)/rho/kn;
                Dp=sin(csi)/akn/rho;
            }
        }
        
    }
    
    Mt[1][1]=C*C;
    Mt[1][2]=-2*S*C;
    Mt[1][3]=S*S;
    Mt[2][1]=-C*Cp;
    Mt[2][2]=Sp*C+S*Cp;
    Mt[2][3]=-S*Sp;
    Mt[3][1]=Cp*Cp;
    Mt[3][2]=-2*Sp*Cp;
    Mt[3][3]=Sp*Sp;
    
    Md[1][1]=C;
    Md[1][2]=S;
    Md[1][3]=D;
    Md[2][1]=Cp;
    Md[2][2]=Sp;
    Md[2][3]=Dp;
    Md[3][1]=0;
    Md[3][2]=0;
    Md[3][3]=1;
}


/*************************************************************************/
/*************************************************************************/
/******************************     Main    ******************************/
/*************************************************************************/
/*************************************************************************/



void mexFunction(
        int nlhs, mxArray *plhs[],
        int nrhs, const mxArray *prhs[]) {
    double **Mt,**Md,*vin,*vout,*din,*dout, *ang, *L, *K, *e1, *e2, a0, b0, g0, d0, dp0, *a, *b, *g, *d, *dp;
    double S, C, Sp, Cp, D, Dp,da;
    int ii, jj, kk, nelm, n, n1, n2;
    size_t bytes_to_copy;
    const mwSize dims[]={3, 3, mxGetM(prhs[0])};
    
    
    
    ang=mxGetPr(prhs[0]);
    L=mxGetPr(prhs[1]);
    K=mxGetPr(prhs[2]);
    e1=mxGetPr(prhs[3]);
    e2=mxGetPr(prhs[4]);
    b0=mxGetScalar(prhs[5]);
    a0=mxGetScalar(prhs[6]);
    g0=mxGetScalar(prhs[7]);
    d0=mxGetScalar(prhs[8]);
    dp0=mxGetScalar(prhs[9]);
    
    n = mxGetM(prhs[0]);
    n1 = mxGetM(prhs[1]);
    n2 = mxGetM(prhs[2]);
    
    /* Check for proper number of arguments */
    if (nrhs != 10) {
        mexErrMsgTxt("Ten input arguments required.");
    } else if (nlhs != 5) {
        mexErrMsgTxt("Must have 5 output arguments.");
    }
    /* Check the dimensions  */
    /*printf("%d %d %d %d\n", nx_all, nz_all, Nx_all, Nz_all);*/
    if ((n1 != n2) || (n2 != n)) {
        mexErrMsgTxt("Arguments must have compatible sizes.");
    }

    
    
    /****Initializations *******************/
    Mt=matrix(1, 3, 1, 3);
    Md=matrix(1, 3, 1, 3);
    vin=vector(1, 3);
    vout=vector(1, 3);
    din=vector(1, 3);
    dout=vector(1, 3);
    a=vector(1, n);
    b=vector(1, n);
    g=vector(1, n);
    d=vector(1, n);
    dp=vector(1, n);

    plhs[0]=mxCreateDoubleMatrix(n, 1, mxREAL);
    plhs[1]=mxCreateDoubleMatrix(n, 1, mxREAL);
    plhs[2]=mxCreateDoubleMatrix(n, 1, mxREAL);
    plhs[3]=mxCreateDoubleMatrix(n, 1, mxREAL);
    plhs[4]=mxCreateDoubleMatrix(n, 1, mxREAL);


    
    /***************Do the loop************/
    vin[1]=b0;
    vin[2]=a0;
    vin[3]=g0;
    din[1]=d0;
    din[2]=dp0;
    din[3]=1;
    
    for(ii=1;ii<=n;ii++){
        if (L[ii]>0){
            da=vin[1]*ang[ii]*tan(e1[ii])/L[ii];
            vin[3]+=(da*da+2*da*vin[2])/vin[1];
            vin[2]-=da;
            din[2]-=din[1]*ang[ii]*tan(e1[ii])/L[ii];
        }
        
        S_functions(ang[ii], L[ii], K[ii], Mt, Md);
        MatrixVectorProduct(Mt, vin, vout, 3, 3);
        MatrixVectorProduct(Md, din, dout, 3, 3);       
        if (L[ii]>0){
            da=vout[1]*ang[ii]*tan(e2[ii])/L[ii];
            vout[3]+=(da*da+2*da*vout[2])/vout[1];
            vout[2]-=da;
            dout[2]-=dout[1]*ang[ii]*tan(e2[ii])/L[ii];
        }
        
        b[ii]=vout[1];
        a[ii]=vout[2];
        g[ii]=vout[3];
        d[ii]=dout[1];
        dp[ii]=dout[2];
        
        vin[1]=vout[1];
        vin[2]=vout[2];
        vin[3]=vout[3];
        din[1]=dout[1];
        din[2]=dout[2];
        din[3]=dout[3];
    }
    
    /* populate the real part of the created array */
            
    for (ii=0;ii<n;ii++){
        mxGetPr(plhs[0])[ii]=b[1+ii];  
        mxGetPr(plhs[1])[ii]=a[1+ii];
        mxGetPr(plhs[2])[ii]=g[1+ii];
        mxGetPr(plhs[3])[ii]=d[1+ii];
        mxGetPr(plhs[4])[ii]=dp[1+ii];
    }
    
    
    /****Finishing*******************/
    
    free_matrix(Mt, 1, 3, 1, 3);
    free_matrix(Md, 1, 3, 1, 3);
    free_vector(a, 1, n);
    free_vector(b, 1, n);
    free_vector(g, 1, n);
    free_vector(d, 1, n);
    free_vector(dp, 1, n);
}


