function getOrbitRamping(varargin)
%% Parameters of the calculation
% DDNSamples for the ACQs and DDBufferSize for non-ACQs
%TotalNumberTurns=180000;
% For checking the Intensity.
%NumberRampPoints=9;
% For checking the Intensity.
%TurnTest=500;
% intensity threshold for the chek
%Imin=0.0E7;
% step turns vector



if length(varargin)==4
    TotalNumberTurns = varargin{1};
    NumberRampPoints= varargin{2};
    Imin = varargin{3};
else
    TotalNumberTurns=[];
    NumberRampPoints=[];
    Imin=[];
end
if isempty(TotalNumberTurns)
    answer = inputdlg({'Range of turns ', 'Number of analisis points','Intensity threshold'},'BPMs during Ramp',1,{'180000','9','0.3e7'});
    if isempty(answer)
        return
    end
    TotalNumberTurns = str2num(answer{1});
    NumberRampPoints= str2num(answer{2});
    Imin = str2num(answer{3});
end
TurnsInitial=5:floor(TotalNumberTurns/NumberRampPoints):TotalNumberTurns;


tic;
%% Reading the BPMS


MC=getmachineconfig();
s=getspos('BPMx');
AO=getao();
bpm_names=AO.BPMx.DeviceName;
bpm_status=AO.BPMx.Status;
bpm_names=bpm_names(bpm_status==1);
bpm_names_noACQ=bpm_names;
n_bpm=length(bpm_names);
res=findstr('ACQ',bpm_names{1});
pos_=findstr('-',bpm_names{1});
if isempty(res)
    fprintf('The BPMs are not ACQ\n');
    for ii=1:n_bpm
        str_bpm=bpm_names_noACQ{ii};
        bpm_names{ii}=[str_bpm(1:pos_(1)) 'ACQ-' str_bpm((pos_(1)+1):end)];
    end
else
    fprintf('The BPMs are ACQ\n');
    for ii=1:n_bpm
        str_bpm=bpm_names{ii};
        bpm_names_noACQ{ii}=[str_bpm(1:pos_(1)) str_bpm((pos_(1)+5):end)];
    end
end

ny=floor(sqrt(NumberRampPoints));
nx=ceil(NumberRampPoints/ny);

M_x=zeros(n_bpm,NumberRampPoints);
M_y=zeros(n_bpm,NumberRampPoints);

try
    for jj=1:NumberRampPoints
        ChangeBoosterBPMsDDoffset(TurnsInitial(jj));
        
        pause(1);
        for ii=1:n_bpm
            valid=false;
            bpm=bpm_names{ii};
            tango_command_inout2(bpm,'DDAcquire',uint8(0));
            
            while(valid==false)
                %fprintf('Reading BPM num %d\n',ii);
                
                
                try
                    XTango=tango_read_attributes2(bpm,{'XPosDDMean','ZPosDDMean','VaDDMean','VbDDMean','VcDDMean','VdDDMean'});
                    Xstr=XTango(1);
                    Ystr=XTango(2);
                    Vastr=XTango(3);
                    Vbstr=XTango(3);
                    Vcstr=XTango(3);
                    Vdstr=XTango(3);
                    M_x(ii,jj)=Xstr.value;
                    M_y(ii,jj)=Ystr.value;
                    Itest=Vastr.value+Vbstr.value+Vcstr.value+Vdstr.value;
                catch ME1
                    fprintf('Efective buffer size is: %d\n',numel(X));
                    X=[];
                    fprintf('Error: %s\n',ME1.message);
                    Itest=-1;
                end
                
                if (Itest >= Imin),
                    valid=true;
                    fprintf('Itest=%f, Adquired BPM nn %d\n',Itest,ii)
                else
                    fprintf('Itest=%f, Bad shot, BPM nn %d\n',Itest,ii)
                    pause(1);
                end
                
            end
            %tango_command_inout2(bpm,'DDStop');
            pause(0.1);
        end
        figure(101);
        subplot(ny,nx,jj);
        plot(s, M_x(:,jj),'r'); hold on; plot(s, M_y(:,jj),'b'); drawlattice(-5,1); hold off
        title(['Turn ' num2str(TurnsInitial(jj))])
        yaxis([-6 6]);
        figure(102);
        plot(s, M_x(:,jj),'Color',[0.5*(1+(jj-1)/(NumberRampPoints-1)) 0 0]); hold on; drawlattice(-5,1);
        figure(103);
        plot(s, M_y(:,jj),'Color',[0 0 0.5*(1+(jj-1)/(NumberRampPoints-1))]); hold on; drawlattice(-5,1);
    end
    figure(101);
    subplot(ny,nx,1);
    legend('Horizontal', 'Vertical');
    figure(102);
    ylabel('x(mm)');
    xlabel('s(m)');
    figure(103);
    ylabel('y(mm)');
    xlabel('s(m)');

    ChangeBoosterBPMsDDoffset(0);
    
    daydir=['/data/BOOSTER/',datestr(now,'yyyymmdd'),'/OrbitRamp'];
    mkdir(daydir);
    cd(daydir);
    name0=['OrbitRamp_' datestr(now, 'yyyymmddTHHMMSS') ] ;
    name=[name0 '.mat'];
    save(name,'M_x','M_y','MC');
    
    
catch ME
    ChangeBoosterBPMsDDoffset(0);
    fprintf('Error: %s',ME.message);
end

toc;
end