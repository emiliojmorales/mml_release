function [coefs Inew L]=Fit_Current(t,I,varargin)
% coefs=Fit_Current(t,I,n_peacks,n_deeps)

global n_deeps n_peacks poly_order II tt;

narg=numel(varargin);

n_deeps=0; % number of deeps to fit, each peack has 4 coeficients: t_a,I_a,alpha_a,peta_a
n_peacks=0; % number of peack to fit, each peack has 3 coeficients: t_b,A_b,c_b
Initial=[];
poly_order=2;
II=I;
tt=t;

if narg==1
    n_peacks=varargin{1};
elseif narg==2
    n_peacks=varargin{1};
    n_deeps=varargin{2};
elseif narg==3
    n_peacks=varargin{1};
    n_deeps=varargin{2};
    Initial=varargin{3};
elseif narg>=4
    error('Too much imputs\n');
end

Nparameters=poly_order+1+4*n_deeps+3*n_peacks;

%% Initial coeficients
coef_p=polyfit(t,I,poly_order);
Koef=zeros(Nparameters,1);
Koef(1:(poly_order+1))=coef_p;
tM=max(t);
tm=min(t);
if isempty(Initial)
    for ii=1:n_deeps
        ind=1+poly_order+(ii-1)+(1:4);
        t_a=tm+(tM-tm)*rand;
        I_a=mean(I)/10;
        alpha_a=rand;
        beta_a=rand;
        Koef(ind)=[t_a I_a alpha_a beta_a]';
    end
    
    for ii=1:n_peacks
        ind=1+poly_order+n_deeps*4+(ii-1)*n_peacks+(1:3);
        t_a=tm+(tM-tm)*rand;
        I_a=mean(I)/10;
        gamma_a=10/(tM-tm);
        Koef(ind)=[t_a I_a gamma_a]';
    end
else
      Koef((poly_order+2):end)=Initial(:);
end

%% Optimization
I0=CostF(Koef);
fprintf('Cost function: %1.2f\n ',norm(I0));
for ii=1:10
    [Knew Inew]=Find_next_step(Koef,I0);
    Koef=Knew;
    I0=Inew;
fprintf('Cost function: %1.2f\n ',norm(I0));
end

%% Solution
coefs=Koef;
[Inew L]=Lifetime_coefs(Koef);
end


function [Knew I]=Find_next_step(Koef,I0)
global n_deeps n_peacks;

nk=numel(Koef);
n=numel(I0);
M=getMatrix(Koef,I0);

[U,S,V] = svd(M);

iM=invSVD(U,S,V,nk-n_deeps);

dk=-iM*I0;

steps=20;
Iii=zeros(n,steps);
Cii=zeros(steps,1);
Kii=Koef*ones(1,steps)+dk*((1:steps)-1)/(steps-1);
Iii(:,1)=I0;
Cii(1)=norm(I0)^2;
for ii=2:steps
    Iii(:,ii)=CostF(Kii(:,ii));
    Cii(ii)=norm(Iii(:,ii))^2;
end

[m iimin]=min(Cii);
Knew=Kii(:,iimin);
I=Iii(:,iimin);
end

function M=getMatrix(Koef,I0)


n=numel(I0);
nk=numel(Koef);
M=zeros(n,nk);
dK=1e-6;

for ii=1:nk
    Kii=Koef;
    Kii(ii)=Kii(ii)+dK;
    Iii=CostF(Kii);
    M(:,ii)=(Iii-I0)/dK;
end

end

function ITotal=CostF(Koef)
global n_deeps n_peacks poly_order II tt;

n=numel(tt);
Ipoly=polyval(Koef(1:(1+poly_order)),tt);
Ideeps=zeros(n,1);
Ipeacks=zeros(n,1);

for ii=1:n_deeps
    c_ii=Koef(1+poly_order+(ii-1)+1:4);
    t_a=c_ii(1);
    I_a=c_ii(2);
    alpha_a=c_ii(3);
    beta_a=c_ii(4);
    Ideeps(tt<t_a)=Ideeps(tt<t_a)+I_a+alpha_a*(tt(tt<t_a)-t_a);
    Ideeps(tt>=t_a)=Ideeps(tt>=t_a)+I_a+beta_a*(tt(tt>=t_a)-t_a);
end

for ii=1:n_peacks
    c_ii=Koef(1+poly_order+n_deeps*4+(ii-1)*n_peacks+(1:3));
    t_a=c_ii(1);
    I_a=c_ii(2);
    gamma_a=c_ii(3);
    Ipeacks=Ipeacks+I_a*exp(gamma_a*(tt-t_a))./(1+exp(gamma_a*(tt-t_a)));
end

ITotal=II-Ideeps-Ipeacks-Ipoly;

end


function [ITotal L]=Lifetime_coefs(Koef)
global n_deeps n_peacks poly_order II tt;

n=numel(tt);
Ipoly=polyval(Koef(1:(1+poly_order)),tt);
Ideeps=zeros(n,1);
Ipeacks=zeros(n,1);

for ii=1:n_deeps
    c_ii=Koef(1+poly_order+(ii-1)+1:4);
    t_a=c_ii(1);
    I_a=c_ii(2);
    alpha_a=c_ii(3);
    beta_a=c_ii(4);
    Ideeps(tt<t_a)=Ideeps(tt<t_a)+I_a+alpha_a*(tt(tt<t_a)-t_a);
    Ideeps(tt>=t_a)=Ideeps(tt>=t_a)+I_a+beta_a*(tt(tt>=t_a)-t_a);
end

for ii=1:n_peacks
    c_ii=Koef(1+poly_order+n_deeps*4+(ii-1)*n_peacks+(1:3));
    t_a=c_ii(1);
    I_a=c_ii(2);
    gamma_a=c_ii(3);
    Ipeacks=Ipeacks+I_a*exp(gamma_a*(tt-t_a))./(1+exp(gamma_a*(tt-t_a)));
end

ITotal=Ideeps+Ipeacks+Ipoly;

% derivate

dIpoly=polyval(Koef(1:poly_order).*(poly_order:(-1):1)',tt);
dIdeeps=zeros(n,1);
dIpeacks=zeros(n,1);

for ii=1:n_deeps
    c_ii=Koef(1+poly_order+(ii-1)+1:4);
    t_a=c_ii(1);
    alpha_a=c_ii(3);
    beta_a=c_ii(4);
    dIdeeps(tt<t_a)=dIdeeps(tt<t_a)+alpha_a;
    dIdeeps(tt>=t_a)=dIdeeps(tt>=t_a)+beta_a;
end

for ii=1:n_peacks
    c_ii=Koef(1+poly_order+n_deeps*4+(ii-1)*n_peacks+(1:3));
    t_a=c_ii(1);
    I_a=c_ii(2);
    gamma_a=c_ii(3);
    dIpeacks=dIpeacks+I_a*gamma_a*exp(gamma_a*(tt-t_a))./(1+exp(gamma_a*(tt-t_a))).^2;
end

dI=dIpoly+dIdeeps+dIpeacks;
L=-ITotal./dI/3600;
end