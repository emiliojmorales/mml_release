function Gaps= ReadGapsIds()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

IDs={'SR/ID/SCW01'; 'motor/id11ivu_icepap/1';'motor/id13ivu_icepap/1'; 'motor/idmpw_icepap/1'; ...
    'pm/ideu62_pseudomotor_z/1';'pm/ideu62_pseudomotor_z/1'; 'pm/ideu62_pseudomotor_y/1'; 'pm/ideu62_pseudomotor_y/1'};
attrv={'MagneticField','DialPosition','DialPosition','DialPosition',...
    'Position','Position','Position','Position'};

Gaps=zeros(numel(IDs),1);
for loop=1:numel(IDs)
    hh=tango_read_attribute(IDs{loop},attrv{loop});
    if isstruct(hh)
        Gaps(loop)=hh.value(1);
    else
        Gaps(loop)=nan;
    end
end

end

