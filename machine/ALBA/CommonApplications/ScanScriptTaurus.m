function Data=ScanScriptTaurus(file)

%cd /home/zeus/ZeusApps/Datas/Trends;
fid=fopen(file,'r');


tline = fgetl(fid);
tline = fgetl(fid);
tline = fgetl(fid);
count=1;
out=sscanf(tline,'%d-%d-%d_%d:%d:%f\t%f');

while all(tline~=-1)&&not(any(isempty(out)))
    time(count)=datenum(out(1),out(2),out(3),out(4),out(5),out(6));
    value(count)=out(7);
    tline = fgetl(fid);
    count=count+1;
    out=sscanf(tline,'%d-%d-%d_%d:%d:%f\t%f');
end
Data=[time(:) value(:)];

fclose(fid);
end