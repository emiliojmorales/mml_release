function [ h ] = plottune( order, range, nonsys, q0 )
% plottune( order, range, sys, q0 )
% Plot the measured tune (from gettune) and the resonance diagram
% all parameters are optional, but the order matter!
% order: max order to plot (5 by def)
% range: range of the winodw (0.2 by def)
% nonsys: plot(1) or not(0) the nnon systematic resonance (1 by def)
% q0: center of the plot eg [18.2, 8.4]. (gettune+integer part by def)
%
% Created 16-Dec-2011 by M. Munoz
q=gettune+[18;8];
if nargin<4,
    q0=q;
end
period=4;
if nargin<3,
    nonsys=1;
end
if nonsys,
    period=1;
end
if nargin < 2,
    range=0.2;
end
if nargin<1,
    order=5;
end
window=[q0(1)-range/2 q0(1)+range/2 q0(2)-range/2 q0(2)+range/2];
figure
webplot( 1, order, window, period )
hold on
plot(q(1), q(2),'+','MarkerSize',6)
plot(q(1), q(2),'o')
xlabel('Q_x')
ylabel('Q_y')
title( [datestr(now) ', Tune: (' num2str(q(1)) ','  num2str(q(2)) ')'] )
box on;
h=gcf;
end

