function loco2pinhole
%% GB 2012
DirStart = [getfamilydata('Directory', 'DataRoot'), 'LOCO', filesep];

[filename, LocoDirectory, filterindex] = uigetfile('*.mat','Select a locodata file',DirStart);

load([LocoDirectory filename], 'RINGData');

%%
ATI = atindex(RINGData.Lattice);

THERINGloco = RINGData.Lattice(1:ATI.BEND(end-1));
THERINGloco(ATI.BEND(end)+4:length(RINGData.Lattice)+3) = RINGData.Lattice(ATI.BEND(end)+1:end);

THERINGloco(ATI.BEND(end)) = RINGData.Lattice(ATI.BEND(end));
THERINGloco(ATI.BEND(end)+1) = RINGData.Lattice(ATI.BEND(end));
THERINGloco(ATI.BEND(end)+2) = RINGData.Lattice(ATI.BEND(end));
THERINGloco(ATI.BEND(end)+3) = RINGData.Lattice(ATI.BEND(end));

blength = RINGData.Lattice{ATI.BEND(end)}.Length;

THERINGloco{ATI.BEND(end)}.Length = 0.098-0.007;
THERINGloco{ATI.BEND(end)}.BendingAngle = (0.098-0.007)/blength*RINGData.Lattice{ATI.BEND(end)}.BendingAngle;
THERINGloco{ATI.BEND(end)}.ExitAngle = 0;
THERINGloco{ATI.BEND(end)}.FringeInt2 = 0;

THERINGloco{ATI.BEND(end)+1}.Length = 0.007;
THERINGloco{ATI.BEND(end)+1}.BendingAngle = 0.007/blength*RINGData.Lattice{ATI.BEND(end)}.BendingAngle;
THERINGloco{ATI.BEND(end)+1}.ExitAngle = 0;
THERINGloco{ATI.BEND(end)+1}.FringeInt2 = 0;

THERINGloco{ATI.BEND(end)+2}.Length = 0.007;
THERINGloco{ATI.BEND(end)+2}.BendingAngle = 0.007/blength*RINGData.Lattice{ATI.BEND(end)}.BendingAngle;
THERINGloco{ATI.BEND(end)+2}.ExitAngle = 0;
THERINGloco{ATI.BEND(end)+2}.FringeInt2 = 0;

THERINGloco{ATI.BEND(end)+3}.Length = blength-0.098-0.007;
THERINGloco{ATI.BEND(end)+3}.BendingAngle = (blength-0.098-0.007)/blength*RINGData.Lattice{ATI.BEND(end)}.BendingAngle;

%%
ATIloco = atindex(THERINGloco);
twiss = gettwiss(THERINGloco,0);

s=twiss.s(ATIloco.BEND(end)-1);
ds=0.5*abs(twiss.s(ATIloco.BEND(end))-twiss.s(ATIloco.BEND(end)-2));

bx=twiss.betax(ATIloco.BEND(end)-1);
dbx=0.5*abs(twiss.betax(ATIloco.BEND(end))-twiss.betax(ATIloco.BEND(end)-2));

by=twiss.betay(ATIloco.BEND(end)-1);
dby=0.5*abs(twiss.betay(ATIloco.BEND(end))-twiss.betay(ATIloco.BEND(end)-2));

dx=twiss.etax(ATIloco.BEND(end)-1);
ddx=0.5*abs(twiss.etax(ATIloco.BEND(end))-twiss.etax(ATIloco.BEND(end)-2));

dy=twiss.etay(ATIloco.BEND(end)-1);
ddy=0.5*abs(twiss.etay(ATIloco.BEND(end))-twiss.etay(ATIloco.BEND(end)-2));

%%
disp(' ')
disp('Parameters at pinhole source point:')
disp(['s: ',num2str(s),' +\- ', num2str(ds), ' m'])
disp(['betax: ',num2str(bx),' +\- ', num2str(dbx), ' m'])
disp(['betay: ',num2str(by),' +\- ', num2str(dby), ' m'])
disp(['etax: ',num2str(dx),' +\- ', num2str(ddx), ' m'])
disp(['etay: ',num2str(dy),' +\- ', num2str(ddy), ' m'])
disp(' ')

system(['emittance ',num2str(bx+dbx),' ',num2str(by),' ',num2str(dx)]);