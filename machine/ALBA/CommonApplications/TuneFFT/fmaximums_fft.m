function varargout=fmaximums_fft(nmax,f)
% [nummax Amax mumax fft0]=fmaximumsNAFF(nmax,f)
%finds nmax relative maximums of the fft values of the vector f
%
%INPUTS
%
%  nmax: maximum number of peacks to be analized
%  f: Array with the tracking data to be annalized
%
%OUTPUTS
%
%  nummax: Array with unitary frequencies of the nmax harmonics
%  Amax: Array with the fft back transformes amplitudes of the nmax harmonics
%  mumax: Array with phases of the nmax harmonics
%
% Z.Marti  1/12/2008

numargout = nargout;

n=length(f);
Big=1;
ampliant=1e100;
f0=f*Big;
numax=zeros(nmax,1);
Amax=zeros(nmax,1);
mumax=zeros(nmax,1);
jj=0;
surt=0;
while(jj<nmax &&surt==0)
    jj=jj+1;
    f0=(f0-mean(f0));
    ff=(fft(f0.*sin(pi*(0:(n-1))/n).*sin(pi*(0:(n-1))/n)));  %Hanning filter
    ffm=abs(ff);

    %figure;
%     semilogy((0:(n-1))/(n-1),ffm/Big);
%     hold on;


         
    [fmax imax]=max(ffm);
    fnmax=ffm(imax+1);
    inmax=imax+1;
    if imax>1
        if ffm(imax-1)>ffm(imax+1)
            fnmax=ffm(imax);
            inmax=imax;
            fmax=ffm(imax-1);
            imax=imax-1;
        end
        b=exp(2*1i*pi/n);
        Q=ff(imax)/ff(inmax);
        p1=b*Q-1;
        p2=(b*b-1)*(1+Q);
        p3=b*(b-Q);
        a=(-p2-sqrt(p2*p2-4*p1*p3))/(2*p1);
        %fprintf('ab %f %f\n',imag(a),imag(b));
        nu=(imax-1)/(n)+log(a)/(2*pi*1i);
        nu=abs(real(nu));
        nu=find_nu(f0,n,nu);
        a=exp(2*pi*1i*(nu-(imax-1)/(n)));
        A=8*(fmax/abs(a^n-1))*1i*(1-a)*(a-b)*(b-conj(a))/((1+a)*(b-1)*(b-1));
        A=abs(A);
        mu=0;
        comp=A*sin(mu+nu*(0:(n-1))*2*pi);
        ffcomp=(fft(comp.*sin(pi*(0:(n-1))/n).*sin(pi*(0:(n-1))/n)));
        mui=angle(ff.*conj(ffcomp));
        mu=mui(imax);
        comp=A*sin(mu+nu*(0:(n-1))*2*pi);
        f0=f0-comp;  
        ampli=norm(f0);
    end
    if imax==1
        A=sum(f0)/n;
        comp=A;
        nu=0;
        ffcomp=(fft(comp.*sin(pi*(0:(n-1))/n).*sin(pi*(0:(n-1))/n)));
        mui=angle(ff.*conj(ffcomp));
        mu=mui(imax);
        comp=A*cos(mu);
        f0=f0-comp;
        ampli=norm(f0);
    end
    %disp('values');
    %disp(imax);
    %disp(A);
    %disp(nu);
    %disp(mu);
    if ampli>ampliant
        surt=1;
    else
        Amax(jj)=A/Big;
        numax(jj)=nu;
        mumax(jj)=mu;
    end
    ampliant=ampli;
end

if numargout==1
    varargout{1} = numax;
elseif numargout==2
    varargout{1} = numax;
    varargout{2} = Amax;
elseif numargout==3
    varargout{1} = numax;
    varargout{2} = Amax;
    varargout{3} = mumax;
elseif numargout==4
    varargout{1} = numax;
    varargout{2} = Amax;
    varargout{3} = mumax;
    varargout{4} = ffm;
end

end

function nu_min=find_nu(f0,n,nu_0)
warning off;
%figure;

d_nu=0.3/n;
N_nu=11;
N_it=6;
cost=zeros(N_nu,1);
nu=zeros(N_nu,1);
nu_or=nu_0;
for jj=1:N_it
    for ii=1:N_nu
        nu(ii)=mod(nu_or+d_nu/2*(2*ii-1-N_nu)/(N_nu-1),1);
        [A mu]=Get_Amp_Phase_BPM(f0,n,nu(ii));
        comp=A*sin(mu+nu(ii)*(0:(n-1))*2*pi);
        fE=f0-comp;
        cost(ii)=norm(fE)/norm(f0);
    end
    %plot(nu,cost);
    %hold on;
    [mc ior]=min(cost);
    nu_or=nu(ior);
    if (mc<100)&&(ior~=1)&&(ior~=N_nu)
        d_nu=d_nu/10;
    elseif (ior==1)||(ior==N_nu)
        N_it=N_it+1;
    elseif (mc>100)
        d_nu=2*d_nu;
        N_it=N_it+1;
    end
end
cost_new=(cost-mean(cost));
cost_new=cost_new/abs(eps+mean(cost_new));
p = polyfit(1:N_nu,cost_new',2);
if p(1)>0
    i_min=1+mod(-p(2)/2/p(1),N_nu-1);
else
    [mc i_min]=min(cost_new);
end

nu_min = interp1(1:N_nu,nu,i_min);


% plot(1:N_nu,cost_new,'-b.',1:N_nu,polyval(p,1:N_nu),'r');
warning on;
end
function [A mu]=Get_Amp_Phase_BPM(f0,n,nu)


ff=(fft(f0.*sin(pi*(0:(n-1))/n).*sin(pi*(0:(n-1))/n)));
ffm=abs(ff);

[fmax imax]=max(ffm);
if imax>1
    a=exp(2*pi*1i*(nu-(imax-1)/(n)));
    b=exp(2*pi*1i/n);
    A=8*(fmax/abs(a^(n)-1))*1i*(1-a)*(a-b)*(b-conj(a))/((1+a)*(b-1)*(b-1));
    A=abs(A);
    mu=0;
    comp=A*sin(mu+nu*(0:(n-1))*2*pi);
    ffcomp=(fft(comp.*sin(pi*(0:(n-1))/n).*sin(pi*(0:(n-1))/n)));
    mui=angle(ff.*conj(ffcomp));
    mu=mui(imax);
end
if imax==1
    A=sum(f0)/n;
    comp=A;
    nu=0;
    ffcomp=(fft(comp.*sin(pi*(0:(n-1))/n).*sin(pi*(0:(n-1))/n)));
    mui=angle(ff.*conj(ffcomp));
    mu=mui(imax);
end

end