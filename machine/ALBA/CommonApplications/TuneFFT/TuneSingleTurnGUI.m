function varargout = TuneSingleTurnGUI(varargin)
% TUNESINGLETURNGUI M-file for TuneSingleTurnGUI.fig
%      TUNESINGLETURNGUI, by itself, creates a new TUNESINGLETURNGUI or raises the existing
%      singleton*.
%
%      H = TUNESINGLETURNGUI returns the handle to a new TUNESINGLETURNGUI or the handle to
%      the existing singleton*.
%
%      TUNESINGLETURNGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TUNESINGLETURNGUI.M with the given input arguments.
%
%      TUNESINGLETURNGUI('Property','Value',...) creates a new TUNESINGLETURNGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs
%      are
%      applied to the GUI before TuneSingleTurnGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TuneSingleTurnGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TuneSingleTurnGUI

% Last Modified by GUIDE v2.5 04-Oct-2010 16:48:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TuneSingleTurnGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @TuneSingleTurnGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TuneSingleTurnGUI is made visible.
function TuneSingleTurnGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TuneSingleTurnGUI (see VARARGIN)

% Choose default command line output for TuneSingleTurnGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
ad=getad;
if strcmp(ad.SubMachine,'StorageRing'),
    set(handles.BPMMenu,'String',{'SR03/DI/DBPM-01'; 'SR02/DI/BPM-06';'SR03/DI/BPM-08'})  
end

% UIWAIT makes TuneSingleTurnGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = TuneSingleTurnGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function DocMenu_Callback(hObject, eventdata, handles)
% hObject    handle to DocMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function SaveMenu_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp 'Saving the data'
ad=getad;
name_=[ad.Directory.TuneData 'Tune_' datestr(now,'yyyy_mm_dd-HH_MM_SS')];
save(name_,'-struct', 'handles', 'result')
h=msgbox(['File ' name_ ' saved']);


% --------------------------------------------------------------------
function QuitMenu_Callback(hObject, eventdata, handles)
% hObject    handle to QuitMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp 'Closing the app'
choice = questdlg('Quit the application?', ...
	'Quit?', ...
	'Yes','No','No');
if (isequal(choice,'Yes')),
    close(get(get(hObject,'Parent'),'Parent'))
end

% --- Executes on button press in GetDataButton.
function GetDataButton_Callback(hObject, eventdata, handles)
% hObject    handle to GetDataButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp 'Getting the data'
a=get(handles.BPMMenu,'String'); b=get(handles.BPMMenu,'Value');
bpm=a(b,:);
handles.result.BPMName=cell2mat(bpm);
tango_write_attribute2(cell2mat(bpm),'DDBufferSize',...
    int32(str2double(get(handles.TurnField,'String')) + str2double(get(handles.StartField,'String')) ));
disp 'Change buffer size and reading BPM, be patient ..'
pause(2.0)
thedata=tango_read_attributes2(cell2mat(bpm), {'SumDD','XPosDD','ZPosDD'});
disp 'Setting buffer size back to 1024'
tango_write_attribute2(cell2mat(bpm),'DDBufferSize',int32(1024));

handles.I=thedata(1);
handles.X=thedata(2);
handles.Y=thedata(3);
plot(handles.IRawAxe ,1:length(handles.I.value),handles.I.value,'g+-');
%set(handles.IRawAxe,'ButtonDownFcn', @MouseClick);
xlabel(handles.IRawAxe,'Turn');
ylabel(handles.IRawAxe,'Intensity [au]');
plot(handles.XRawAxe ,1:length(handles.X.value),handles.X.value,'r+-');
%set(handles.XRawAxe,'ButtonDownFcn', @MouseClick);
xlabel(handles.XRawAxe,'Turn');
ylabel(handles.XRawAxe,'Xpos [mm]');
plot(handles.YRawAxe ,1:length(handles.Y.value),handles.Y.value,'b+-');
%set(handles.YRawAxe,'ButtonDownFcn', @MouseClick);
xlabel(handles.YRawAxe,'Turn');
ylabel(handles.YRawAxe,'Ypos [mm]');
guidata(hObject, handles);
AnalyzeDataButton_Callback(hObject, eventdata, handles)

% --- Executes on button press in AnalyzeDataButton.
function AnalyzeDataButton_Callback(hObject, eventdata, handles)
% hObject    handle to AnalyzeDataButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp 'Analyzing the Data'
start=str2double(get(handles.StartField,'String'));
turns=str2double(get(handles.TurnField,'String'));
handles.result.start=start;
handles.result.turns=turns;
method=get(handles.MethodMenu,'Value')-1;
xwindow=[0 0.5];
if (get(handles.LimitQxBox,'Value')==1),
    xwindow=[str2double(get(handles.LowerQxField,'String')) ...
        str2double(get(handles.UpperQxField,'String'))];
end
ywindow=[0 0.5];
if (get(handles.LimitQyBox,'Value')==1),
    ywindow=[str2double(get(handles.LowerQyField,'String')) ...
        str2double(get(handles.UpperQyField,'String'))];
end
% get the data and calculate the tunes
try
    datax=handles.X.value(start:(turns+start-1));
    resultx=calc_tune(datax,method, xwindow);
    datay=handles.Y.value(start:(turns+start-1));
    resulty=calc_tune(datay,method, ywindow);
    % write the tunes
    set(handles.QxField, 'String', num2str(resultx.peak,'%0.4f'));
    set(handles.QyField, 'String', num2str(resulty.peak,'%0.4f'));
    tango_write_attribute('BO/BD/BD', 'TuneX',resultx.peak);
    tango_write_attribute('BO/BD/BD', 'TuneY',resulty.peak);
    handles.result.x=resultx;
    handles.result.y=resulty;
    % plot the fft
    limit=([0 0.5]);
    tune=resultx.fft_tune;
    amp=resultx.fft_amp;
    if (get(handles.plotqx,'Value')==1),
        limit=([str2num(get(handles.plotqxlow,'String')) str2num(get(handles.plotqxhigh,'String'))]);
        tune=tune(floor(limit(1)*turns):ceil(limit(2)*turns));
        amp=amp(floor(limit(1)*turns):ceil(limit(2)*turns));
    end
    hx=plot(handles.XFFTAxe, tune, amp,'r-');
    xaxis(limit,handles.XFFTAxe);
    limit=([0 0.5]);
    tune=resulty.fft_tune;
    amp=resulty.fft_amp;
    if (get(handles.plotqy,'Value')==1),
        limit=([str2num(get(handles.plotqylow,'String')) str2num(get(handles.plotqyhigh,'String'))]);
        tune=tune(floor(limit(1)*turns):ceil(limit(2)*turns));
        amp=amp(floor(limit(1)*turns):ceil(limit(2)*turns));
    end
    hy=plot(handles.YFFTAxe, tune, amp,'b-');
    xaxis(limit,handles.YFFTAxe);
    ylabel(handles.XFFTAxe, 'FFT(x)');
    %set(handles.YFFTAxe,'ButtonDownFcn', @MouseClick);
    ylabel(handles.YFFTAxe, 'FFT(y)');
    % plot the peaks as stem
    hold(handles.XFFTAxe);
    stem(handles.XFFTAxe,resultx.peaks.nu, resultx.peaks.ampl,'m','MarkerSize',1)
    hold(handles.XFFTAxe);
    hold(handles.YFFTAxe);
    stem(handles.YFFTAxe,resulty.peaks.nu, resulty.peaks.ampl,'c','MarkerSize',1)
    hold(handles.YFFTAxe);
    % plot the data used
    plot(handles.XUsedAxe, 1:turns, datax,'r');
    %set(handles.XUsedAxe,'ButtonDownFcn', @MouseClick);
    ylabel(handles.XUsedAxe,'X [mm]');
    xaxis([0 turns], handles.XUsedAxe);
    plot(handles.YUsedAxe, 1:turns, datay,'b');
    %set(handles.XUsedAxe,'ButtonDownFcn', @MouseClick);
    ylabel(handles.YUsedAxe,'X [mm]');
    xaxis([0 turns], handles.YUsedAxe);
catch
end
guidata(hObject, handles);



% --- Executes on button press in RunModeButton.
function RunModeButton_Callback(hObject, eventdata, handles)
% hObject    handle to RunModeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.RunModeButton,'Visible','off');
set(handles.GetDataButton,'Enable','off');
set(handles.AnalyzeDataButton,'Enable','off');
set(handles.Stop,'Value',0);
set(handles.Stop,'Visible','on');
set(handles.Stop,'String','Stop!', 'ForegroundColor',[1 0 0])
while(get(handles.Stop,'Value')==0),
    GetDataButton_Callback(hObject, eventdata, handles);
    pause(str2double(get(handles.Pause,'String')));
end
set(handles.Stop,'Value',0);
set(handles.Stop,'Visible','off');
set(handles.RunModeButton,'Visible','on');
set(handles.GetDataButton,'Enable','on');
set(handles.AnalyzeDataButton,'Enable','on');


% --- Executes on selection change in BPMMenu.
function BPMMenu_Callback(hObject, eventdata, handles)
% hObject    handle to BPMMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns BPMMenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from BPMMenu


% --- Executes during object creation, after setting all properties.
function BPMMenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BPMMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in MethodMenu.
function MethodMenu_Callback(hObject, eventdata, handles)
% hObject    handle to MethodMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns MethodMenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from MethodMenu


% --- Executes during object creation, after setting all properties.
function MethodMenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MethodMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TurnField_Callback(hObject, eventdata, handles)
% hObject    handle to TurnField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TurnField as text
%        str2double(get(hObject,'String')) returns contents of TurnField as a double


% --- Executes during object creation, after setting all properties.
function TurnField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TurnField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function StartField_Callback(hObject, eventdata, handles)
% hObject    handle to StartField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of StartField as text
%        str2double(get(hObject,'String')) returns contents of StartField as a double


% --- Executes during object creation, after setting all properties.
function StartField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to StartField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LowerQxField_Callback(hObject, eventdata, handles)
% hObject    handle to LowerQxField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LowerQxField as text
%        str2double(get(hObject,'String')) returns contents of LowerQxField as a double


% --- Executes during object creation, after setting all properties.
function LowerQxField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LowerQxField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function UpperQxField_Callback(hObject, eventdata, handles)
% hObject    handle to UpperQxField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of UpperQxField as text
%        str2double(get(hObject,'String')) returns contents of UpperQxField as a double


% --- Executes during object creation, after setting all properties.
function UpperQxField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UpperQxField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LowerQyField_Callback(hObject, eventdata, handles)
% hObject    handle to LowerQyField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LowerQyField as text
%        str2double(get(hObject,'String')) returns contents of LowerQyField as a double


% --- Executes during object creation, after setting all properties.
function LowerQyField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LowerQyField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function UpperQyField_Callback(hObject, eventdata, handles)
% hObject    handle to UpperQyField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of UpperQyField as text
%        str2double(get(hObject,'String')) returns contents of UpperQyField as a double


% --- Executes during object creation, after setting all properties.
function UpperQyField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UpperQyField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in LimitQxBox.
function LimitQxBox_Callback(hObject, eventdata, handles)
% hObject    handle to LimitQxBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of LimitQxBox


% --- Executes on button press in LimitQyBox.
function LimitQyBox_Callback(hObject, eventdata, handles)
% hObject    handle to LimitQyBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of LimitQyBox


% --------------------------------------------------------------------
function MouseMenu_Callback(hObject, eventdata, handles)
% hObject    handle to MouseMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_3_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function PopFFTMenu_Callback(hObject, eventdata, handles)
% hObject    handle to PopFFTMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp 'Pop-up plot'
hfig=figure;
caxe=copyobj(handles.XFFTAxe, hfig)
set(caxe,'Units', 'normalized')
set(caxe,'Position', [0.1 0.1 0.8 0.8])
hfig=figure;
caxe=copyobj(handles.YFFTAxe, hfig)
set(caxe,'Units', 'normalized')
set(caxe,'Position', [0.1 0.1 0.8 0.8])
hfig=figure
caxe=copyobj(handles.XFFTAxe, hfig)
set(caxe,'Units', 'normalized')
set(caxe,'Position', [0.1 0.1 0.8 0.3])
caxe=copyobj(handles.YFFTAxe, hfig)
set(caxe,'Units', 'normalized')
set(caxe,'Position', [0.1 0.6 0.8 0.3])



% --------------------------------------------------------------------
function PopDataMenu_Callback(hObject, eventdata, handles)
% hObject    handle to PopDataMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp 'Pop-up plot'
hfig=figure;
caxe=copyobj(handles.XUsedAxe, hfig)
set(caxe,'Units', 'normalized')
set(caxe,'Position', [0.1 0.1 0.8 0.8])
hfig=figure;
caxe=copyobj(handles.YUsedAxe, hfig)
set(caxe,'Units', 'normalized')
set(caxe,'Position', [0.1 0.1 0.8 0.8])
hfig=figure
caxe=copyobj(handles.XUsedAxe, hfig)
set(caxe,'Units', 'normalized')
set(caxe,'Position', [0.1 0.1 0.8 0.3])
caxe=copyobj(handles.YUsedAxe, hfig)
set(caxe,'Units', 'normalized')
set(caxe,'Position', [0.1 0.6 0.8 0.3])

% --------------------------------------------------------------------
function ArrowModeMenu_Callback(hObject, eventdata, handles)
% hObject    handle to ArrowModeMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom off
datalabel off

% --------------------------------------------------------------------
function ZoomModeMenu_Callback(hObject, eventdata, handles)
% hObject    handle to ZoomModeMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom on
datalabel off

% --------------------------------------------------------------------
function LabelModeMenu_Callback(hObject, eventdata, handles)
% hObject    handle to LabelModeMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom off
datalabel on

% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --------------------------------------------------------------------
function Untitled_4_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function SetBufferSize_Callback(hObject, eventdata, handles)
% hObject    handle to SetBufferSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
a=get(handles.BPMMenu,'String'); b=get(handles.BPMMenu,'Value');
bpm=a(b,:);
SetBPMDDLength('UserData',cell2mat(bpm));

% --- Executes on mouse press over axes background.
function MouseClick(src, event)
% hObject    handle to QxAxe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
type=get(get(get(src,'Parent'),'Parent'),'SelectionType');
if isequal(type,'alt')
    inspect(src);
end


% --- Executes on button press in Stop.
function Stop_Callback(hObject, eventdata, handles)
% hObject    handle to Stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Stop



function Pause_Callback(hObject, eventdata, handles)
% hObject    handle to Pause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Pause as text
%        str2double(get(hObject,'String')) returns contents of Pause as a double


% --- Executes during object creation, after setting all properties.
function Pause_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function plotqxlow_Callback(hObject, eventdata, handles)
% hObject    handle to plotqxlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of plotqxlow as text
%        str2double(get(hObject,'String')) returns contents of plotqxlow as a double


% --- Executes during object creation, after setting all properties.
function plotqxlow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plotqxlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function plotqxhigh_Callback(hObject, eventdata, handles)
% hObject    handle to plotqxhigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of plotqxhigh as text
%        str2double(get(hObject,'String')) returns contents of plotqxhigh as a double


% --- Executes during object creation, after setting all properties.
function plotqxhigh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plotqxhigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function plotqylow_Callback(hObject, eventdata, handles)
% hObject    handle to plotqylow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of plotqylow as text
%        str2double(get(hObject,'String')) returns contents of plotqylow as a double


% --- Executes during object creation, after setting all properties.
function plotqylow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plotqylow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function plotqyhigh_Callback(hObject, eventdata, handles)
% hObject    handle to plotqyhigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of plotqyhigh as text
%        str2double(get(hObject,'String')) returns contents of plotqyhigh as a double


% --- Executes during object creation, after setting all properties.
function plotqyhigh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plotqyhigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plotqx.
function plotqx_Callback(hObject, eventdata, handles)
% hObject    handle to plotqx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotqx


% --- Executes on button press in plotqy.
function plotqy_Callback(hObject, eventdata, handles)
% hObject    handle to plotqy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotqy
