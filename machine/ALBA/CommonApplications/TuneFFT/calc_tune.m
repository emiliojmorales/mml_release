function [ result ] = calc_tune( varargin)
%CALC_TUNE Evaluates tunes from tracking data.
%   result = calc_tune( data, [ method, guess_range])
% input arguments
% data : vector with the values (compulsory)
% method : :
%          0 : rectangular window, no fit
%          1 : von Hann like window of order 2 with exact fit: sin^2(pi*n/N)
%          2 : Zeus method
%          3 : NAFF method, requires length(data) to a multiple of 6
% guess_range : window where to look for the data
%
% M. Munoz, July 2010
% v 0.5
%
result.time=datestr(now);
method = 0;
guess_range = [0 0.5];

if length(varargin) < 1,
    result.control=-1;
    return
end
data=varargin{1};
result.data=data;
ndata=length(data);
if length(varargin) == 2,
    method=varargin{2};
elseif length(varargin) ==3,
    method=varargin{2};
    guess_range=varargin{3};
end
result.method_used=method;
result.guess_range=guess_range;
% remove the average value from the data
data=data-mean(data);
result.fft_amp=abs(fft(data))/ndata;
result.fft_tune=(0:(ndata-1))/ndata;
lowerlimit=floor(ndata*guess_range(1))+1;
upperlimit=ceil(ndata*guess_range(2));
[amp, ind]=max(result.fft_amp(lowerlimit:upperlimit));
ind=ind+lowerlimit-1;
result.peakfft=(ind-1)/ndata;
result.ampfft=amp;
result.peaks.nu=[];
result.peaks.ampl=[];
if (ind == 0 ||  ind==(ndata-1)),
    disp 'The tune is too close to the integer, something is wrong'
end
% create the window w
w=ones(ndata,1);
if method==1,
    w=hann(ndata);
end
norm=abs(ndata/sum(w));
turns=(0:(ndata-1));
data1=data.*w';
fft1=abs(fft(data1))/ndata;
fft1=fft1*norm;
[amp, ind]=max(fft1(lowerlimit:upperlimit));
ind=ind+lowerlimit-1;
;
%    [amp, ind]=max(fft1(1:ndata/2));
% find the peak with interpolation
switch method,
    case 0 % 1/N
        peak=(ind-1)/ndata;
        % now reconstruc the signal with the right amplitude
        aux=exp(2*pi*1i*peak*turns);
        amplitude=2*dot(data,aux)/ndata; % the factor 2 is to take in account the two peaks
        
        result.amplitude=abs(amplitude);
        result.peak=peak;
        result.peaks.nu=peak;
        result.peaks.ampl=abs(amplitude);
    case 1 % exact formula for second order window
        peak=0;
        a=fft1(ind);
        b=fft1(ind+1);
        c=cos(2*pi/ndata);
        Delta=c^2*(a+b)^2-2*a*b*(2*c^2-c-1)*a^2+b^2+2*a*b*c;
        Psi=(-(a+b*c)*(a-b)+b*sqrt(Delta))/(a^2+b^2+2*a*b*c);
        peak=(ind-1)/ndata+asin(Psi*sin(2*pi/ndata))/(2*pi);
        % now reconstruc the signal with the right amplitude
        aux=exp(2*pi*1i*peak*turns);
        amplitude=2*dot(data,aux)/ndata; % the factor 2 is to take in account the two peaks
        result.amplitude=abs(amplitude);
        result.peak=peak;
        result.peaks.nu=peak;
        result.peaks.ampl=abs(amplitude);
    case 2 % Fitting using the Zeus method
        peak=0;
        find=0;
        [nu ampl mumax ffm]=fmaximums_fft(10, data);
        result.peaks.nu=nu;
        result.peaks.ampl=ampl/2;
        for loop=1:length(nu),
            if (nu(loop) >= guess_range(1)) &&  (nu(loop) <= guess_range(2)),
                peak = abs(nu(loop));
                amplitude=abs(ampl(loop));
                find=1;
                break
            end
        end
        if find==0,
            disp 'Zeus method could not find a peak, falling back to 1/N'
            peak=(ind-1)/ndata;
            % now reconstruc the signal with the right amplitude
            aux=exp(2*pi*1i*peak*turns);
            amplitude=2*dot(data,aux)/ndata; % the factor 2 is to take in account the two peaks
            result.method_used=0;
        end
    case 3 % naff method
        peak=0;
        find=0;
        if ndata < 67,
            disp 'NAFF needs at least 67 points, falling back to 1/N'
            peak=(ind-1)/ndata;
            % now reconstruc the signal with the right amplitude
            aux=exp(2*pi*1i*peak*turns);
            amplitude=2*dot(data,aux)/ndata; % the factor 2 is to take in account the two peaks
        else
            if (mod(ndata,6)~=0), % naff needs a multiple of 6, padding with 0
                tmp=length(data);
                data((tmp+1):(tmp+6-mod(tmp,6)))=0;
                result.method_used=0;
            end
            [nu ampl phase]=calcnaff(data,0*data,1,10);
            result.peaks.nu=nu/2/pi;
            result.peaks.ampl=ampl;
            result.peaks.phase=phase;
            nu=abs(nu/2/pi);
            for loop=1:length(nu),
                if (nu(loop) >= guess_range(1)) &&  (nu(loop) <= guess_range(2)),
                    peak = abs(nu(loop));
                    amplitude=2*abs(ampl(loop));
                    find=1;
                    break
                end
            end
            if find==0,
                disp 'NAFF method could not find a peak, falling back to 1/N'
                peak=(ind-1)/ndata;
                % now reconstruc the signal with the right amplitude
                aux=exp(2*pi*1i*peak*turns);
                amplitude=2*dot(data(1:ndata),aux)/ndata; % the factor 2 is to take in account the two peaks
                
            end
        end
    otherwise
        disp 'method not implemented, falling back to 1/N'
        peak=(ind-1)/ndata;
        % now reconstruc the signal with the right amplitude
        aux=exp(2*pi*1i*peak*turns);
        amplitude=2*dot(data,aux)/ndata; % the factor 2 is to take in account the two peaks
        
        
end
result.amplitude=abs(amplitude);
result.peak=peak;
result.method=method;
return