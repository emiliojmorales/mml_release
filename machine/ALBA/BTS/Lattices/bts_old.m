function bts_old
%ALBA_LTB lattice definition file
% Created by G. Benedetti 18/08/08
%

global FAMLIST THERING GLOBVAL

GLOBVAL.E0 = 3.0e+9;
GLOBVAL.LatticeFile = mfilename;
FAMLIST = cell(0);

disp(['**   Loading ALBA BTS lattice ', mfilename]);


DRT01   =    drift('DRT01',1.2106, 'DriftPass'); 
DRT02   =    drift('DRT02',0.1500, 'DriftPass');
DRT03a   =    drift('DRT03a',0.3727, 'DriftPass');
DRT03b   =    drift('DRT03b',0.0920, 'DriftPass');
DRT04a   =    drift('DRT04a',1.3507, 'DriftPass');    
DRT04b   =    drift('DRT04b',0.0850, 'DriftPass');    
DRT05   =    drift('DRT05',3.7400, 'DriftPass');
DRT06a   =    drift('DRT06a',0.0720, 'DriftPass');
DRT06b   =    drift('DRT06b',0.3633, 'DriftPass');
DRT07   =    drift('DRT07',0.4353, 'DriftPass');
DRT08   =    drift('DRT08',0.1050, 'DriftPass');
DRT09   =    drift('DRT09',0.1500, 'DriftPass');
DRT10a   =    drift('DRT10a',0.589, 'DriftPass');   
DRT10b   =    drift('DRT10b',0.1101, 'DriftPass');   
DRT11a   =    drift('DRT11a',0.0720, 'DriftPass');
DRT11b   =    drift('DRT11b',0.3633, 'DriftPass');
DRT12   =    drift('DRT12',0.4353, 'DriftPass');
DRT13   =    drift('DRT13',0.5450, 'DriftPass');
DRT14   =    drift('DRT14',0.1050, 'DriftPass');
DRT15   =    drift('DRT15',0.1500, 'DriftPass');
DRT16   =    drift('DRT16',2.7570, 'DriftPass');
DRT17   =    drift('DRT17',0.1500, 'DriftPass');
DRT18a   =    drift('DRT18a',0.5040, 'DriftPass'); 
DRT18b   =    drift('DRT18b',0.0749, 'DriftPass'); 
DRT18c   =    drift('DRT18b',0.1011, 'DriftPass'); 

CORH01=corrector('HCM', 0.0, [0 0],'CorrectorPass');
CORV01=corrector('VCM', 0.0, [0 0],'CorrectorPass');
CORH02=corrector('HCM', 0.0, [0 0],'CorrectorPass');
CORV02=corrector('VCM', 0.0, [0 0],'CorrectorPass');
CORH03=corrector('HCM', 0.0, [0 0],'CorrectorPass');
CORV03=corrector('VCM', 0.0, [0 0],'CorrectorPass');
CORH04=corrector('HCM', 0.0, [0 0],'CorrectorPass');
CORV04=corrector('VCM', 0.0, [0 0],'CorrectorPass');

% Ideal Matching
QT1=  quadrupole('QT1',0.36, 0.908387015294, 'StrMPoleSymplectic4Pass');
QT2=  quadrupole('QT2',0.36, -0.77247744003, 'StrMPoleSymplectic4Pass');
QT3=  quadrupole('QT3',0.36, 0.955078906198, 'StrMPoleSymplectic4Pass');
QT4=  quadrupole('QT4',0.36, -1.468803125726, 'StrMPoleSymplectic4Pass');
QT5=  quadrupole('QT5',0.36, 1.719939158929, 'StrMPoleSymplectic4Pass');
QT6=  quadrupole('QT6',0.36, -1.441554550001, 'StrMPoleSymplectic4Pass');
QT7=  quadrupole('QT7',0.36, 1.804294690347, 'StrMPoleSymplectic4Pass');

% Optimal Matching
% QT1=  quadrupole('QT1',0.36, 0.901323963484, 'StrMPoleSymplectic4Pass');
% QT2=  quadrupole('QT2',0.36, -0.638791770777, 'StrMPoleSymplectic4Pass');
% QT3=  quadrupole('QT3',0.36, 1.021985434089, 'StrMPoleSymplectic4Pass');
% QT4=  quadrupole('QT4',0.36, -1.589700575724, 'StrMPoleSymplectic4Pass');
% QT5=  quadrupole('QT5',0.36, 1.974097246219, 'StrMPoleSymplectic4Pass');
% QT6=  quadrupole('QT6',0.36, -1.476717615832, 'StrMPoleSymplectic4Pass');
% QT7=  quadrupole('QT7',0.36, 1.933485536914, 'StrMPoleSymplectic4Pass');

DL = sbend('BEND',1.3319,10.675*pi/180,10.675/2*pi/180,10.675/2*pi/180,0.0, 'BndMPoleSymplectic4Pass');
EXTSEPTUM = sbend('EXTSEPTUM',1.0231,-4.92*pi/180,-0.43*pi/180,-5.35*pi/180,0.0, 'BndMPoleSymplectic4Pass');
INJSEPTUM = sbend('INJSEPTUM',1.7514,9*pi/180,9*pi/180,0.0,0.0, 'BndMPoleSymplectic4Pass');


BPM = marker('BPM','IdentityPass');
FSOTR = marker('FSOTR','IdentityPass');

%Pice of Booster lattice
bangle10 = 10*pi/180;
QB1=quadrupole('QB1',0.36, 1.58, 'StrMPoleSymplectic4Pass');
DRB01=    drift('DRB01',0.1500, 'DriftPass'); 
DRB02=    drift('DRB02',0.7400, 'DriftPass'); 
DRB03=    drift('DRB03',0.5980, 'DriftPass'); 
DRB04=    drift('DRB04',0.07019*cos(0.43*pi/180), 'DriftPass'); 
%RD1 = sbendC('RD1', 2.000, bangle10, bangle10/2, bangle10/2,-0.229, -1.95/2, 'BndMPoleSymplectic4Pass');
RD1 = sbendC('RD1', 2.000, 10*pi/180, 1.1*pi/180, 1.1*pi/180,-0.2291, -1.95/2, 'BndMPoleSymplectic4Pass');

EXTKIC=corrector('EXTKIC', 1.0, [3.35e-3 0],'CorrectorPass');
ElemData=FAMLIST{EXTSEPTUM}.ElemData;
ElemData.T1=[-0.01862 -0.43*pi/180 0 0 0 0];
FAMLIST{EXTSEPTUM}.ElemData=ElemData;


% Piece of Storage Ring

LKICK_SR = 0.796;
LI0=0.6144+0.006;
LI3= 0.4958;
LI4= 0.6965;
kik=0.006700107347782; % equivalent to 10 mm
L_I0     = drift('L_I0', LI0,'DriftPass');
L_I3     = drift('L_I3', LI3,'DriftPass');
L_I4     = drift('L_I4', LI4,'DriftPass');
KICKER_SR_1    = corrector('IK' ,LKICK_SR, [kik 0],'CorrectorPass');
KICKER_SR_2    = corrector('IK' ,LKICK_SR, [-kik 0],'CorrectorPass');
QD1 =    quadrupole('QD1',  0.23,-1.773389,'StrMPoleSymplectic4Pass');
ElemData=FAMLIST{INJSEPTUM}.ElemData;
ElemData.T2=[-0.01862 0 0 0 0 0];
FAMLIST{INJSEPTUM}.ElemData=ElemData;


% Begin Lattice
%----- Booster to Storage Transfer Line: BTS --------------------------!;
BTS = [QB1 DRB01 EXTKIC DRB02 RD1 DRB03 DRB04 EXTSEPTUM ...
    DRT01 CORH01 DRT02 CORV01 DRT03a BPM DRT03b QT1 DRT04a FSOTR DRT04b QT2 DRT05 QT3 DRT06a BPM DRT06b DL...
    DRT07 QT4 DRT08 CORH02 DRT09 CORV02 DRT10a FSOTR DRT10b QT5 DRT11a BPM DRT11b DL...
    DRT12 QT6 DRT13 QT7 DRT14 CORH03 DRT15 CORV03 DRT16 CORH04 DRT17 CORV04 DRT18a BPM DRT18b FSOTR DRT18c INJSEPTUM...
    L_I3 KICKER_SR_1 L_I4 KICKER_SR_2 L_I0 BPM QD1];

    
% INIT_BTS: Beta0

% Betx =4.462831602; 
% Alfx =-1.756025185;
% Bety =5.869663417;
% Alfy =1.557796849;
% Dx   = 0.2542293004; 
% Dpx  =  0.1213182565;
% phix=0;
% phiy=0;
% twissdatain.ElemIndex=1;
% twissdatain.SPos=0;
% twissdatain.ClosedOrbit=zeros(4,1);
% twissdatain.Dispersion=[Dx Dpx 0 0]';
% twissdatain.M44=eye(4);
% twissdatain.beta= [Betx Bety];
% twissdatain.alpha= [Alfx Alfy];
% twissdatain.mu= 2*pi*[phix phiy];



buildlat(BTS);

% set all magnets to the same energy
THERING = setcellstruct(THERING,'Energy',1:length(THERING),GLOBVAL.E0);

%evalin('caller','global THERING FAMLIST GLOBVAL');

%atsummary;

if nargout
    varargout{1} = THERING;
end

% Compute total length and RF frequency
L0_tot=0;
for i=1:length(THERING)
    L0_tot=L0_tot+THERING{i}.Length;
end
fprintf('   Model BTS length is %.6f meters\n', L0_tot)
%fprintf('   Model RF frequency is %.6f MHz\n', HarmNumber*C0/L0_tot/1e6)

clear global FAMLIST
%clear global GLOBVAL when GWig... has been changed.

%evalin('caller','global THERING FAMLIST GLOBVAL');
