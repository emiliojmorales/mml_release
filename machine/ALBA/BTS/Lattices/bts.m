function bts
%ALBA_LTB lattice definition file
% Created by G. Benedetti 18/08/08
%

global FAMLIST THERING GLOBVAL

GLOBVAL.E0 = 3.0e+9;


GLOBVAL.LatticeFile = mfilename;
FAMLIST = cell(0);

disp(['**   Loading ALBA BTS lattice ', mfilename]);

Ld_0=1.3319;
Ld_1=1.330565257;
Ld_2=1.331956937;

APLim_VC = aperture('APLimV', [-0.0155 0.0155 -0.0155 0.0155],'AperturePass');
APLim_SR_Septum_str = aperture('APLimS', [-0.007 0.007 -0.0055 0.0055],'AperturePass');
APLim_SR_Septum = aperture('APLimS', [-0.007-0.01862 0.007-0.01862 -0.0055 0.0055],'AperturePass');
%APLim_VC =  marker('APLim' ,'IdentityPass');
%APLim_SR_Septum =  marker('APLim' ,'IdentityPass');

DRT01   =    drift('DRT01',1.19704, 'DriftPass'); 
DRT02   =    drift('DRT02',0.1500, 'DriftPass');
DRT03a   =    drift('DRT03a',0.3727, 'DriftPass');
DRT03b   =    drift('DRT03b',0.0920, 'DriftPass');
DRT04a   =    drift('DRT04a',1.3507, 'DriftPass');    
DRT04b   =    drift('DRT04b',0.0850, 'DriftPass');    
DRT05   =    drift('DRT05',3.7400, 'DriftPass');
DRT06a   =    drift('DRT06a',0.0720, 'DriftPass');
DRT06b   =    drift('DRT06b',0.3633-(Ld_1-Ld_0)/2, 'DriftPass');
DRT07   =    drift('DRT07',0.4353-(Ld_1-Ld_0)/2, 'DriftPass');
DRT08   =    drift('DRT08',0.1050, 'DriftPass');
DRT09   =    drift('DRT09',0.1500, 'DriftPass');
DRT10a   =    drift('DRT10a',0.589, 'DriftPass');   
DRT10b   =    drift('DRT10b',0.1101, 'DriftPass');   
DRT11a   =    drift('DRT11a',0.0720, 'DriftPass');
DRT11b   =    drift('DRT11b',0.3633-(Ld_2-Ld_0)/2, 'DriftPass');
DRT12   =    drift('DRT12',0.4353-(Ld_2-Ld_0)/2, 'DriftPass');
DRT13   =    drift('DRT13',0.5450, 'DriftPass');
DRT14   =    drift('DRT14',0.1050, 'DriftPass');
DRT15   =    drift('DRT15',0.1500, 'DriftPass');
DRT16   =    drift('DRT16',2.7570, 'DriftPass');
DRT17   =    drift('DRT17',0.1500, 'DriftPass');
DRT18a   =    drift('DRT18a',0.069, 'DriftPass'); 
DRT18b   =    drift('DRT18b',0.0749, 'DriftPass'); 
DRT18c   =    drift('DRT18b',0.57218, 'DriftPass'); 

CORH01=corrector('HCM', 0.0, [0.0002135 0],'CorrectorPass');
CORV01=corrector('VCM', 0.0, [0 0],'CorrectorPass');
CORH02=corrector('HCM', 0.0, [0 0],'CorrectorPass');
CORV02=corrector('VCM', 0.0, [0 0],'CorrectorPass');
CORH03=corrector('HCM', 0.0, [0 0],'CorrectorPass');
CORV03=corrector('VCM', 0.0, [0 0],'CorrectorPass');
CORH04=corrector('HCM', 0.0, [0 0],'CorrectorPass');
CORV04=corrector('VCM', 0.0, [0 0],'CorrectorPass');

% Actual setting 

QT1=quadrupole('Q', 0.36,1.3902,'StrMPoleSymplectic4Pass');
QT2=quadrupole('Q', 0.36,-1.9876,'StrMPoleSymplectic4Pass');
QT3=quadrupole('Q', 0.36,1.2587,'StrMPoleSymplectic4Pass');
QT4=quadrupole('Q', 0.36,-1.7274,'StrMPoleSymplectic4Pass');
QT5=quadrupole('Q', 0.36,2.1998,'StrMPoleSymplectic4Pass');
QT6=quadrupole('Q', 0.36,-1.8399,'StrMPoleSymplectic4Pass');
QT7=quadrupole('Q', 0.36,2.0567,'StrMPoleSymplectic4Pass');

% Gabriele Ideal Matching
% QT1=quadrupole('Q', 0.36,0.9083870153,'StrMPoleSymplectic4Pass');
% QT2=quadrupole('Q', 0.36,-0.7724774400,'StrMPoleSymplectic4Pass');
% QT3=quadrupole('Q', 0.36,0.9550789062,'StrMPoleSymplectic4Pass');
% QT4=quadrupole('Q', 0.36,-1.4688031257,'StrMPoleSymplectic4Pass');
% QT5=quadrupole('Q', 0.36,1.7199391589,'StrMPoleSymplectic4Pass');
% QT6=quadrupole('Q', 0.36,-1.4415545500,'StrMPoleSymplectic4Pass');
% QT7=quadrupole('Q', 0.36,1.8042946903,'StrMPoleSymplectic4Pass');

% Optimal Matching
% QT1=  quadrupole('QT1',0.36, 0.901323963484, 'StrMPoleSymplectic4Pass');
% QT2=  quadrupole('QT2',0.36, -0.638791770777, 'StrMPoleSymplectic4Pass');
% QT3=  quadrupole('QT3',0.36, 1.021985434089, 'StrMPoleSymplectic4Pass');
% QT4=  quadrupole('QT4',0.36, -1.589700575724, 'StrMPoleSymplectic4Pass');
% QT5=  quadrupole('QT5',0.36, 1.974097246219, 'StrMPoleSymplectic4Pass');
% QT6=  quadrupole('QT6',0.36, -1.476717615832, 'StrMPoleSymplectic4Pass');
% QT7=  quadrupole('QT7',0.36, 1.933485536914, 'StrMPoleSymplectic4Pass');

phi1_a=4.559893458*pi/180;
phi1_b=4.650498694*pi/180;
phi2_a=4.8528210*pi/180;	
phi2_b=4.7671250*pi/180;


DL_1 = sbend('BEND',Ld_1,10.675*pi/180,phi1_a,phi1_b,0.0, 'BndMPoleSymplectic4Pass');
DL_2 = sbend('BEND',Ld_2,10.675*pi/180,phi2_a,phi2_b,0.0, 'BndMPoleSymplectic4Pass');
EXTSEPTUM = sbend('SEEXT',1.0231,-4.92*pi/180,0.43*pi/180,-5.35*pi/180,0.0, 'BndMPoleSymplectic4Pass');
INJSEPTUM = sbend('SEINJ',1.7514,9*pi/180,9*pi/180,0.0,0.0, 'BndMPoleSymplectic4Pass');


BPM = marker('BPM','IdentityPass');
FSOTR = marker('FSOTR','IdentityPass');
FSH = marker('FSOTR','IdentityPass');

%Pice of Booster lattice
bangle10 = 10*pi/180;
%QB1=quadrupole('QB1',0.36, 1.5626, 'StrMPoleSymplectic4Pass'); % QH02 actual settings 02/2011
QB1=quadrupole('QB1',0.36, 1.5997, 'StrMPoleSymplectic4Pass'); % QH02 actual settings 04/2016

DRB01=    drift('DRB01',0.1500, 'DriftPass'); 
DRB02=    drift('DRB02',0.690, 'DriftPass'); 
DRB03=    drift('DRB03',0.5980, 'DriftPass'); 
DRB04=    drift('DRB04',0.07019*cos(0.43*pi/180), 'DriftPass'); 

%RD1 = sbendC('RD1', 2.000, 10*pi/180, 1.1*pi/180, 1.1*pi/180,-0.2291, -1.95/2, 'BndMPoleSymplectic4Pass');
RD1a = sbendC('RD1', 1.000, 5*pi/180, 1.1*pi/180, 0,-0.2291, -1.95/2, 'BndMPoleSymplectic4Pass');
RD1b = sbendC('RD1', 1.000, 5*pi/180, 0, 1.1*pi/180,-0.2291, -1.95/2, 'BndMPoleSymplectic4Pass');

EXTKIC=corrector('KIEXT', 1.0, [0.1925*pi/180 0],'CorrectorPass');
%Nominally EXTKIC=corrector('KIEXT', 1.0, [0.2*pi/1800],'CorrectorPass');

ElemData=FAMLIST{EXTSEPTUM}.ElemData;
ElemData.T1=[-0.01862 -0.43*pi/180 0 0 0 0];
FAMLIST{EXTSEPTUM}.ElemData=ElemData;


% Piece of Storage Ring

LKICK_SR = 0.796;
LI0=0.6144+0.006;
LI3= 0.4958;
LI4= 0.6965;
kik=0.006700107347782; % equivalent to 10 mm
L_I0     = drift('L_I0', LI0,'DriftPass');
L_I3     = drift('L_I3', LI3,'DriftPass');
L_I4     = drift('L_I4', LI4,'DriftPass');
KICKER_SR_1    = corrector('KIINJ' ,LKICK_SR, [kik 0],'CorrectorPass');
KICKER_SR_2    = corrector('KIINJ' ,LKICK_SR, [-kik 0],'CorrectorPass');
QD1 =    quadrupole('QD1',  0.23,-1.773389,'StrMPoleSymplectic4Pass');
ElemData=FAMLIST{INJSEPTUM}.ElemData;
ElemData.T2=[-0.01862 0 0 0 0 0];
FAMLIST{INJSEPTUM}.ElemData=ElemData;


% Begin Lattice
%----- Booster to Storage Transfer Line: BTS --------------------------!;
BTS = [QB1 DRB01 EXTKIC DRB02 RD1a RD1b DRB03 FSH DRB04 EXTSEPTUM ...
    APLim_VC DRT01 APLim_VC CORH01 APLim_VC DRT02 APLim_VC CORV01 APLim_VC DRT03a BPM APLim_VC DRT03b ...
    APLim_VC QT1 APLim_VC DRT04a APLim_VC FSOTR APLim_VC DRT04b APLim_VC QT2 DRT05 APLim_VC QT3 APLim_VC DRT06a BPM APLim_VC DRT06b DL_1...
    APLim_VC DRT07 QT4 DRT08 CORH02 DRT09 CORV02 DRT10a ...
    APLim_VC FSOTR APLim_VC DRT10b APLim_VC QT5 DRT11a BPM DRT11b DL_2...
    APLim_VC DRT12 APLim_VC QT6 DRT13 APLim_VC QT7 DRT14 CORH03 DRT15 CORV03 ...
    APLim_VC DRT16 APLim_VC CORH04 DRT17 CORV04 APLim_VC DRT18a BPM DRT18b APLim_VC FSOTR DRT18c APLim_SR_Septum_str INJSEPTUM APLim_SR_Septum...
    L_I3 KICKER_SR_1 L_I4 KICKER_SR_2 L_I0 BPM QD1];

    
% INIT_BTS: Beta0

% Betx =4.462831602; 
% Alfx =-1.756025185;
% Bety =5.869663417;
% Alfy =1.557796849;
% Dx   = 0.2542293004; 
% Dpx  =  0.1213182565;
% phix=0;
% phiy=0;
% twissdatain.ElemIndex=1;
% twissdatain.SPos=0;
% twissdatain.ClosedOrbit=zeros(4,1);
% twissdatain.Dispersion=[Dx Dpx 0 0]';
% twissdatain.M44=eye(4);
% twissdatain.beta= [Betx Bety];
% twissdatain.alpha= [Alfx Alfy];
% twissdatain.mu= 2*pi*[phix phiy];



buildlat(BTS);

% set all magnets to the same energy
THERING = setcellstruct(THERING,'Energy',1:length(THERING),GLOBVAL.E0);

%evalin('caller','global THERING FAMLIST GLOBVAL');

%atsummary;

if nargout
    varargout{1} = THERING;
end

% Compute total length and RF frequency
L0_tot=0;
for i=1:length(THERING)
    L0_tot=L0_tot+THERING{i}.Length;
end
fprintf('   Model BTS length is %.6f meters\n', L0_tot)
%fprintf('   Model RF frequency is %.6f MHz\n', HarmNumber*C0/L0_tot/1e6)

clear global FAMLIST
%clear global GLOBVAL when GWig... has been changed.

%evalin('caller','global THERING FAMLIST GLOBVAL');
