function btsinit(OperationalMode)
% Initialize parameters for ALBA BTS control in MATLAB

% Created by G.Benedetti - 18 August 2008
% Modified by GB - 13 July 2009
% Modified by Z.Martí - 24 July 2009

if nargin < 1
    OperationalMode = 1;
end

%==============================
% load AcceleratorData structure
%==============================

Mode             = 'Simulator';
setad([]);       %clear AcceleratorData memory

%%%%%%%%%%%%%%%%%%%%
% ACCELERATOR OBJECT
%%%%%%%%%%%%%%%%%%%%

setao([]);   %clear previous AcceleratorObjects

%=============================================
%% BPMx data: status field designates if BPM in use
%=============================================
iFam = 'BPMx';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'BPM';
AO.(iFam).MemberOf                 = {'PlotFamily'; 'HBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';

bpm={
     1,	'BT/DI/BPM-ACQ-01', 1, [1  1], '01BPM01'
     2,	'BT/DI/BPM-ACQ-02', 1, [1  2], '01BPM02'
     3,	'BT/DI/BPM-ACQ-03', 1, [1  3], '01BPM03'
     4,	'BT/DI/BPM-ACQ-04', 1, [1  4], '01BPM04'
     5,	'SR01/DI/BPM-ACQ-01', 1, [1 5], '01BPM05'
    };

% Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/XposDDPeak');
    AO.(iFam).Status(ii,:)             = bpm{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';

%=============================================
%% BPMy data: status field designates if BPM in use
%=============================================

iFam = 'BPMy';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'BPM';
AO.(iFam).MemberOf                 = {'PlotFamily'; 'VBPM'; 'BPM'; 'Diagnostics'};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'mm';
AO.(iFam).Monitor.PhysicsUnits     = 'meter';

bpm={
     1,	'BT/DI/BPM-ACQ-01', 1, [1  1], '01BPM01'
     2,	'BT/DI/BPM-ACQ-02', 1, [1  2], '01BPM02'
     3,	'BT/DI/BPM-ACQ-03', 1, [1  3], '01BPM03'
     4,	'BT/DI/BPM-ACQ-04', 1, [1  4], '01BPM04'
     5,	'SR01/DI/BPM-ACQ-01', 1, [1 5], '01BPM05'
    };

%Load fields from data block
for ii=1:size(bpm,1)
    AO.(iFam).ElementList(ii,:)        = bpm{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bpm(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bpm(ii,2), '/ZposDDPeak');
    AO.(iFam).Status(ii,:)             = bpm{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bpm{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bpm{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams(ii,:) = 1e-3;
    AO.(iFam).Monitor.Physics2HWParams(ii,:) = 1e3;
end

% Scalar channel method
AO.(iFam).Monitor.DataType = 'Scalar';

%===========================================================
%% HCM
%===========================================================

iFam ='HCM';
AO.(iFam).FamilyName               = iFam;
AO.(iFam).MemberOf                 = {'COR'; 'MCOR'; 'HCM'; 'Magnet'; 'CORH'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;

cor={
     1,	'BT/PC/CORH-01', 1, [1  1], '01CORH01'
     2,	'BT/PC/CORH-02', 1, [1  2], '01CORH02'
     3,	'BT/PC/CORH-03', 1, [1  3], '01CORH03'
     4,	'BT/PC/CORH-04', 1, [1  4], '01CORH04'
    };

%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients('HCM');

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-6 6]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-4;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%% CORV

iFam ='VCM';

AO.(iFam).FamilyName               = iFam;
AO.(iFam).FamilyType               = 'COR';
AO.(iFam).MemberOf                 = {'COR'; 'MCOR'; 'VCM'; 'Magnet'; 'CORV'};

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode             = Mode;
AO.(iFam).Monitor.DataType         = 'Scalar';
AO.(iFam).Monitor.Units            = 'Hardware';
AO.(iFam).Monitor.HWUnits          = 'A';
AO.(iFam).Monitor.PhysicsUnits     = 'radian';
AO.(iFam).Monitor.HW2PhysicsFcn = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn = @k2amp;

AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode            = Mode;
AO.(iFam).Setpoint.DataType        = 'Scalar';
AO.(iFam).Setpoint.Units           = 'Hardware';
AO.(iFam).Setpoint.HWUnits         = 'A';
AO.(iFam).Setpoint.PhysicsUnits    = 'radian';
AO.(iFam).Setpoint.HW2PhysicsFcn = @amp2k;
AO.(iFam).Setpoint.Physics2HWFcn = @k2amp;


cor={
     1,	'BT/PC/CORV-01', 1, [1  1], '01CORV01'
     2,	'BT/PC/CORV-02', 1, [1  2], '01CORV02'
     3,	'BT/PC/CORV-03', 1, [1  3], '01CORV03'
     4,	'BT/PC/CORV-04', 1, [1  4], '01CORV04'
    };

% Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
[C, Leff, MagnetType, coefficients] = magnetcoefficients('VCM');

for ii=1:size(cor,1)
    AO.(iFam).ElementList(ii,:)        = cor{ii,1};
    AO.(iFam).DeviceName(ii,:)         = cor(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(cor(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = cor{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = cor{ii,4};
    AO.(iFam).CommonNames(ii,:)        = cor{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = coefficients;
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(cor(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [-6 6]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 1E-4;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 1;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = coefficients;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = coefficients;
    AO.(iFam).Monitor.Handles(ii,1)    = NaN;
    AO.(iFam).Setpoint.Handles(ii,1)   = NaN;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%=============================
%        MAIN MAGNETS
%=============================

%===========
% Dipole data
%===========
%% *** BEND ***
iFam = 'BEND';

AO.(iFam).FamilyName                 = iFam;
AO.(iFam).FamilyType               = 'BEND';
AO.(iFam).MemberOf                   = {'BEND'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('BEND');
Physics2HWParams                    = HW2PhysicsParams;
AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';
AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Setpoint.DataType           = 'Scalar';
AO.(iFam).Setpoint.Units              = 'Hardware';
AO.(iFam).Setpoint.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Setpoint.Physics2HWFcn      = @gev2bend;
AO.(iFam).Setpoint.HWUnits            = 'A';
AO.(iFam).Setpoint.PhysicsUnits       = 'rad';
AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.Mode               = Mode;

%A0.(iFam).DataType = @single;
%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
bend={
     1,	'BT/PC/BEND-01', 1, [1 1], '01BEND01'
     2,	'BT/PC/BEND-02', 1, [1 2], '01BEND02'
    };
for ii=1:size(bend,1)
    AO.(iFam).ElementList(ii,:)        = bend{ii,1};
    AO.(iFam).DeviceName(ii,:)         = bend(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(bend(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = bend{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = bend{ii,4};
    AO.(iFam).CommonNames(ii,:)        = bend{ii,5};
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = Physics2HWParams;
    AO.(iFam).Monitor.Range(ii,:)        = [0 180]; 
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(bend(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 180]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 0.0005;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.2;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = Physics2HWParams;
end



%% *** Extraction kicker ***
iFam = 'KIEXT';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).FamilyType           = 'SEPTUM';
AO.(iFam).MemberOf                   = {'KICKER'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients(iFam);
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'V';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';

AO.(iFam).DeviceName(:,:) = {'BO/PC/KIEXT'};
AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/VoltageSetpoint');

AO.(iFam).DeviceList(:,:) = [1 1];
AO.(iFam).ElementList(:,:)= 1;
AO.(iFam).Status          = 1;

val = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(iFam).Monitor.Range(:,:) = [0 32200];

AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/VoltageSetpoint');
AO.(iFam).Setpoint.Tolerance(:,:) = 1.0e-3;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 1.0;


%% *** Extraction septum ***
iFam = 'SEEXT';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).FamilyType           = 'SEPTUM';
AO.(iFam).MemberOf                   = {'SEPTUM'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('SEEXT');
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'V';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';

AO.(iFam).DeviceName(:,:) = {'BO/PC/SEEXT'};
AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/VoltageSetpoint');

AO.(iFam).DeviceList(:,:) = [1 1];
AO.(iFam).ElementList(:,:)= 1;
AO.(iFam).Status          = 1;

val = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(iFam).Monitor.Range(:,:) = [0 500]; 
AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf        = {'MachineConfig'; 'PlotFamily';};
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/VoltageSetpoint');
AO.(iFam).Setpoint.Tolerance(:,:) = 0.001;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.5;






%% *** injection septum ***
iFam = 'SEINJ';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).FamilyType           = 'SEPTUM';
AO.(iFam).MemberOf                   = {'SEPTUM'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('SEINJ');
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'V';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';
AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf           = {'MachineConfig'; 'PlotFamily';};


AO.(iFam).DeviceName(:,:) = {'sr/pc/seinj'};
val = 1;
AO.(iFam).Monitor.TangoNames(:,:) = strcat(AO.(iFam).DeviceName(:,:),'/VoltageSetpoint');
AO.(iFam).DeviceList(:,:) = [1 1];
AO.(iFam).ElementList(:,:)= 1;
AO.(iFam).Status          = 1;
AO.(iFam).Monitor.HW2PhysicsParams{1}(:,:) = HW2PhysicsParams;
AO.(iFam).Monitor.HW2PhysicsParams{2}(:,:) = val;
AO.(iFam).Monitor.Physics2HWParams{1}(:,:) = Physics2HWParams;
AO.(iFam).Monitor.Physics2HWParams{2}(:,:) = val;
AO.(iFam).Monitor.Range(:,:) = [0 700]; 
AO.(iFam).Setpoint.TangoNames(:,:)  = strcat(AO.(iFam).DeviceName,'/VoltageSetpoint');
AO.(iFam).Setpoint.Tolerance(:,:) = 0.001;
AO.(iFam).Setpoint.DeltaRespMat(:,:) = 0.5;


%% *** Injection kicker ***
iFam = 'KIINJ';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).FamilyType           = 'SEPTUM';
AO.(iFam).MemberOf                   = {'KICKER'; 'Magnet';};
HW2PhysicsParams                    = magnetcoefficients('KIINJ');
Physics2HWParams                    = HW2PhysicsParams;

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @bend2gev;
AO.(iFam).Monitor.Physics2HWFcn      = @gev2bend;
AO.(iFam).Monitor.HWUnits            = 'V';
AO.(iFam).Monitor.PhysicsUnits       = 'rad';
AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf           = {'MachineConfig'; 'PlotFamily';};

kick={1,	'sr/pc/kiinj-03', 1, [1 1], 'KIINJ3',1
     2,	'sr/pc/kiinj-04', 1, [1 2], 'KIINJ4',1
    };

val=1;
for ii=1:size(kick,1)
    AO.(iFam).ElementList(ii,:)        = kick{ii,1};
    AO.(iFam).DeviceName(ii,:)         = kick(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(kick(ii,2), '/VoltageSetpoint');
    AO.(iFam).Status(ii,:)             = kick{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = kick{ii,4};
    AO.(iFam).CommonNames(ii,:)        = kick{ii,5};
    AO.(iFam).Polarity(ii,:)             = kick{ii,6};  
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = kick{ii,6}*HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = kick{ii,6}*Physics2HWParams;
    AO.(iFam).Monitor.HW2PhysicsParams{2}(ii,:) = val;
    AO.(iFam).Monitor.Physics2HWParams{2}(ii,:) = val;
    AO.(iFam).Monitor.Range(ii,:)        = [0 8500]; 
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(kick(ii,2), '/VoltageSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 8500]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 0.001;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.5;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = kick{ii,6}*HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = kick{ii,6}*Physics2HWParams;
    AO.(iFam).Setpoint.HW2PhysicsParams{2}(ii,:) = val;
    AO.(iFam).Setpoint.Physics2HWParams{2}(ii,:) = val;
end
AO.(iFam).Status = AO.(iFam).Status(:);


%============
% QUADRUPOLES
%============
%% *** QUADS ***
iFam = 'Q';
AO.(iFam).FamilyType               = 'QUAD';
AO.(iFam).FamilyName                 = iFam;
AO.(iFam).MemberOf                   = {'QUAD'; 'Magnet';};

HW2PhysicsParams  = magnetcoefficients(AO.(iFam).FamilyName);
Physics2HWParams  = magnetcoefficients(AO.(iFam).FamilyName);

AO.(iFam).Monitor.MemberOf           = {'PlotFamily';};
AO.(iFam).Monitor.Mode               = Mode;
AO.(iFam).Monitor.DataType           = 'Scalar';
AO.(iFam).Monitor.Units              = 'Hardware';
AO.(iFam).Monitor.HW2PhysicsFcn      = @amp2k;
AO.(iFam).Monitor.Physics2HWFcn      = @k2amp;
AO.(iFam).Monitor.HWUnits            = 'A';
AO.(iFam).Monitor.PhysicsUnits       = 'meter^-2';
AO.(iFam).Setpoint = AO.(iFam).Monitor;
AO.(iFam).Setpoint.MemberOf           = {'MachineConfig'; 'PlotFamily';};
%A0.(iFam).DataType = @single;

%                                                                                                               delta-k
%common             monitor                setpoint           stat devlist  elem        range   tol  respkick
quad={
     1,	'BT/PC/Q-01', 1, [1 1], '01Q01',1
     2,	'BT/PC/Q-02', 1, [1 2], '01Q02',-1
     3,	'BT/PC/Q-03', 1, [1 3], '01Q03',1
     4,	'BT/PC/Q-04', 1, [1 4], '01Q04',-1
     5,	'BT/PC/Q-05', 1, [1 5], '01Q05',1
     6,	'BT/PC/Q-06', 1, [1 6], '01Q06',-1
     7,	'BT/PC/Q-07', 1, [1 7], '01Q07',1
    };



for ii=1:size(quad,1)
    AO.(iFam).ElementList(ii,:)        = quad{ii,1};
    AO.(iFam).DeviceName(ii,:)         = quad(ii,2);
    AO.(iFam).Monitor.TangoNames(ii,:) = strcat(quad(ii,2), '/Current');
    AO.(iFam).Status(ii,:)             = quad{ii,3};  
    AO.(iFam).DeviceList(ii,:)         = quad{ii,4};
    AO.(iFam).CommonNames(ii,:)        = quad{ii,5};
    AO.(iFam).Polarity(ii,:)             = quad{ii,6};  
    AO.(iFam).Monitor.HW2PhysicsParams{1}(ii,:)  = quad{ii,6}*HW2PhysicsParams;
    AO.(iFam).Monitor.Physics2HWParams{1}(ii,:)  = quad{ii,6}*Physics2HWParams;
    AO.(iFam).Monitor.Range(ii,:)        = [0 170]; 
    AO.(iFam).Setpoint.TangoNames(ii,:) = strcat(quad(ii,2), '/CurrentSetpoint');
    AO.(iFam).Setpoint.Range(ii,:)        = [0 170]; 
    AO.(iFam).Setpoint.Tolerance(ii,1)    = 0.0005;
    AO.(iFam).Setpoint.DeltaRespMat(ii,1) = 0.2;
    AO.(iFam).Setpoint.HW2PhysicsParams{1}(ii,:) = quad{ii,6}*HW2PhysicsParams;
    AO.(iFam).Setpoint.Physics2HWParams{1}(ii,:) = quad{ii,6}*Physics2HWParams;
end

AO.(iFam).Status = AO.(iFam).Status(:);

%%%%%%%%%%%%%%%%%%
%%% Diagnostics
%%%%%%%%%%%%%%%%%%

% Fast current Transformers - 
%% *** FCTs ***

% To be included (use the corresponding osciloscope device tango names)





%%
% The operational mode sets the path, filenames, and other important parameters
% Run setoperationalmode after most of the AO is built so that the Units and Mode fields
% can be set in setoperationalmode
setao(AO);
setoperationalmode(OperationalMode);
AO = getao;

%======================================================================
%% Append Accelerator Toolbox information
%======================================================================
%======================================================================
% disp('** Initializing Accelerator Toolbox information');
% 
% AO = getao;
% 
% ATindx = atindex(THERING);  %structure with fields containing indices
% 
% s = findspos(THERING,1:length(THERING)+1)';
% 
% %% Horizontal BPMs
% iFam = ('BPMx');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BPM(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% Vertical BPMs
% iFam = ('BPMy');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.BPM(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);  
% 
% %% HORIZONTAL CORRECTORS
% iFam = ('HCM');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% VERTICAL CORRECTORS
% iFam = ('VCM');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).AT.ATIndex = AO.(iFam).AT.ATIndex(AO.(iFam).ElementList);   %not all correctors used
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %% BENDING magnets
% iFam = ('BEND');
% AO.(iFam).AT.ATType  = iFam;
% AO.(iFam).AT.ATIndex = ATindx.(iFam)(:);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% % One group of all dipoles
% %AO.(iFam).Position   = reshape(s(AO.(iFam).AT.ATIndex),1,40);
% %AT.(iFam).AT.ATParamGroup = mkparamgroup(THERING,AT.(iFam).AT.ATIndex,'K2');
% 
% %% QUADRUPOLES
% iFam = ('Q');
% AO.(iFam).AT.ATType  = 'QUAD';
% AO.(iFam).AT.ATIndex = eval(['ATindx.' iFam '(:)']);
% AO.(iFam).Position   = s(AO.(iFam).AT.ATIndex);
% 
% %%
% 
% AO.HCM.Setpoint.DeltaRespMat = physics2hw('HCM','Setpoint', 1e-4, AO.HCM.DeviceList);
% AO.VCM.Setpoint.DeltaRespMat = physics2hw('VCM','Setpoint', 1e-4, AO.VCM.DeviceList);
% 
% AO.Q.Setpoint.DeltaRespMat  = physics2hw('Q', 'Setpoint', AO.Q.Setpoint.DeltaRespMat,  AO.Q.DeviceList);

setao(AO);

% reference values
phix=0;
phiy=0;
twissdatain.ElemIndex=1;
twissdatain.SPos=0;
twissdatain.ClosedOrbit=zeros(4,1);
twissdatain.M44=eye(4);
twissdatain.Dispersion=[0.419238147753613   0.117903618573629 0 0]';
twissdatain.beta= [10.020560375827804 2.978621075087383];
twissdatain.alpha= [-2.873496217610703 0.864521303154206];
twissdatain.mu= 2*pi*[phix phiy];

global THERING;
TD=twissline(THERING,0.0,twissdatain,1:length(THERING),'chrom');
ETA = cat(2,TD.Dispersion)';
ALFA = cat(1,TD.alpha);
BETA = cat(1,TD.beta);
MU = cat(1,TD.mu);
S = cat(1,TD.SPos);
XX=cat(2,TD.ClosedOrbit)';
ndata=length(BETA(:,1));
phix=MU(:,1);
phiy=MU(:,2);
betax=BETA(:,1);
betay=BETA(:,2);
alfax=ALFA(:,1);
alfay=ALFA(:,2);
etax=ETA(:,1);
etapx=ETA(:,2);
etay=ETA(:,3);
etapy=ETA(:,4);
x=XX(:,1);
px=XX(:,2);
y=XX(:,3);
py=XX(:,4);

twiss.s=S;
twiss.phix=phix;
twiss.phiy=phiy;
twiss.betax=betax;
twiss.betay=betay;
twiss.alfax=alfax;
twiss.alfay=alfay;
twiss.etax=etax;
twiss.etapx=etapx;
twiss.etay=etay;
twiss.etapy=etapy;
twiss.x=x;
twiss.px=px;
twiss.y=y;
twiss.py=py;

global RefOptics;
%disp '    Reference optics, tunes and AO stored in RefOptics'
RefOptics.AO=getao();
RefOptics.twiss=twiss;
RefOptics.tune= [phix(ndata);phiy(ndata)];
