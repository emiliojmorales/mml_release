
function updateatindex
%UPDATEATINDEX - Updates the AT indices in the MiddleLayer with the present AT lattice (THERING)

global THERING

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Append Accelerator Toolbox information %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Since changes in the AT model could change the AT indexes, etc,
% It's best to regenerate all the model indices whenever a model is loaded

% Sort by family first (findcells is linear and slow)
% Written for ALBA by G. Benedetti 18/8/08
% modified for ALBA BTS by Z.Marti 27/07/09

Indices = atindex(THERING);

AO = getao;

try
    AO.BPMx.AT.ATType = 'X';
    AO.BPMx.AT.ATIndex = Indices.BPM(:); % findcells(THERING,'FamName','BPM')';
    AO.BPMx.Position = findspos(THERING, AO.BPMx.AT.ATIndex)';

    AO.BPMy.AT.ATType = 'Y';
    AO.BPMy.AT.ATIndex = Indices.BPM(:); % findcells(THERING,'FamName','BPM')';
    AO.BPMy.Position = findspos(THERING, AO.BPMy.AT.ATIndex)';
catch
    fprintf('   BPM family not found in the model.\n');
end

try
    % Horizontal correctors
    AO.HCM.AT.ATType  = 'HCM';
    AO.HCM.AT.ATIndex = buildatindex(AO.HCM.FamilyName, Indices.HCM);
    AO.HCM.Position = findspos(THERING, AO.HCM.AT.ATIndex)';
    
    % Vertical correctors
    AO.VCM.AT.ATType = 'VCM';
    AO.VCM.AT.ATIndex = buildatindex(AO.VCM.FamilyName, Indices.VCM);
    AO.VCM.Position = findspos(THERING, AO.VCM.AT.ATIndex)';
catch
    fprintf('   COR family not found in the model.\n');
end

try
    AO.Q.AT.ATType = 'QUAD';
    ATIndex = [Indices.Q(:)];
    AO.Q.AT.ATIndex  = sort(ATIndex);
    %AO.Q.AT.ATIndex = buildatindex(AO.QT1.FamilyName, Indices.QT1);
    AO.Q.Position = findspos(THERING, AO.Q.AT.ATIndex(:,1))';
catch
    fprintf('   Q family not found in the model.\n');
end

try
    AO.SEINJ.AT.ATType = 'SEPTUM';
    ATIndex = [Indices.SEINJ(:)];
    AO.SEINJ.AT.ATIndex  = sort(ATIndex);
    %AO.SEINJ.AT.ATIndex = buildatindex(AO.SEINJ.FamilyName, Indices.QT1);
    AO.SEINJ.Position = findspos(THERING, AO.SEINJ.AT.ATIndex(:,1))';
catch
    fprintf('   SEINJ family not found in the model.\n');
end

try
    AO.SEEXT.AT.ATType = 'SEPTUM';
    ATIndex = [Indices.SEEXT(:)];
    AO.SEEXT.AT.ATIndex  = sort(ATIndex);
    %AO.SEEXT.AT.ATIndex = buildatindex(AO.SEEXT.FamilyName, Indices.QT1);
    AO.SEEXT.Position = findspos(THERING, AO.SEEXT.AT.ATIndex(:,1))';
catch
    fprintf('   SEEXT family not found in the model.\n');
end

try
    AO.KIINJ.AT.ATType = 'SEPTUM';
    ATIndex = [Indices.KIINJ(:)];
    AO.KIINJ.AT.ATIndex  = sort(ATIndex);
    %AO.KIINJ.AT.ATIndex = buildatindex(AO.KIINJ.FamilyName, Indices.QT1);
    AO.KIINJ.Position = findspos(THERING, AO.KIINJ.AT.ATIndex(:,1))';
catch
    fprintf('   KIINJ family not found in the model.\n');
end

try
    AO.KIEXT.AT.ATType = 'SEPTUM';
    ATIndex = [Indices.KIEXT(:)];
    AO.KIEXT.AT.ATIndex  = sort(ATIndex);
    %AO.KIEXT.AT.ATIndex = buildatindex(AO.KIEXT.FamilyName, Indices.QT1);
    AO.KIEXT.Position = findspos(THERING, AO.KIEXT.AT.ATIndex(:,1))';
catch
    fprintf('   KIEXT family not found in the model.\n');
end

try
    AO.BEND.AT.ATType = 'BEND';
    AO.BEND.AT.ATIndex = buildatindex(AO.BEND.FamilyName, Indices.BEND);
    AO.BEND.Position = findspos(THERING, AO.BEND.AT.ATIndex)';
catch
    fprintf('   BEND family not found in the model.\n');
end

setao(AO);
