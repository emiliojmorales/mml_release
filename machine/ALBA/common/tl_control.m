function f = tl_control()
% f = tl_display
% Displays the theoreticals optical functions and beam sizes
% in a transfer line.
% The input values are enter in the leftmost pane.
% The right pane provides tabs with the plots and the beam sizes
% at the position of the FS/OTR.
% author: M. Munoz, 27 November 2009.

% close already running versions
oldfig = findobj(allchild(0),'tag','ltb_optic'); 
if ~isempty(oldfig), delete(oldfig); end
%create the figure, and the panels
f = figure('menubar','none', 'Position', [0 0 1200 600],'Resize', 'Off','Tag','ltb_optic',...
    'Interruptible', 'on', ...   
    'HandleVisibility','off', ...
    'Name', 'Transfer line optics', ...
    'NumberTitle','off','CloseRequestFcn',@closeME);

%% 1st some shared information is created 

% the information required by the different components of the gui is stored
% in handles.
handles = guihandles(f);
handles.f = f;
handles.npoints=180;
global THERING;
handles.ati=atindex(THERING);
handles.n_fsotr=0;

% Creates the tabs names
if isfield(handles.ati,'FSOTR'),
    tab_names = cell(3+length(handles.ati.FSOTR));
    tab_names{1}='Optics';
    tab_names{2}='Beam Size';
    handles.n_fsotr=length(handles.ati.FSOTR);
    for loop=1:handles.n_fsotr;
        tab_names{2+loop}=['FS/OTR ' num2str(loop)];
    end
    tab_names{3+handles.n_fsotr}='Results'; 
else
    tab_names = cell(3);
    tab_names{1}='Optics';
    tab_names{2}='Beam Size';
    tab_names{3}='Results';
end

% Get the quads names and indexes and store them in
% qnames and qindex
handles.spos=findspos(THERING, 1:length(THERING)+1);
qfamilies=findmemberof('QUAD');
qnames=[];
nquads=1;
for loop1=1:length(qfamilies),
    tmp_index=cell2mat(family2atindex(qfamilies(loop1)));
    for loop2=1:length(tmp_index),
       a=[cell2mat(qfamilies(loop1)) '-' num2str(loop2)];
       qnames{nquads}=a;
       qindex(nquads)=tmp_index(loop2);
       nquads=nquads+1;
    end
end
handles.qnames=qnames;
handles.qindex=qindex;

%% Prepare the Gui

% The Gui is compossed of 3 main panels
mainpanel = uipanel('Parent', f);
% For input of the data
inputpanel = uipanel('Parent', mainpanel,'Units','normalized','Position',[0 0 0.18 0.95],'Title','Input Parameters');
% For a table with the quads current and strength
quadpanel = uipanel('Parent', mainpanel,'Units','normalized','Position', [0.18 0 0.3 0.95],'Title', 'Quad Values');
% And for the tabs with betas, sizes and information
betapanel = uipanel('Parent', mainpanel,'Units','normalized','Position', [0.48 0 0.52 0.95],'Title', 'Model Predictions');
% Creating the tabpanel
handles.tabpanel=uitabpanel(...
  'Parent',betapanel,...
  'TabPosition','lefttop',...
  'Position',[0,0,1,1],...
  'Margins',{[0,-1,1,0],'pixels'},...
  'PanelBorderType','line',...
  'Title',tab_names,'CreateFcn',@CreateTab);


%% Laying the components in the input panel
x0=70;
dx=40;
y0=500;
dy=7;
stepy=35;
% we create pairs of label and input, and change the y position eache time
handles.quadlabel=uibutton('Style','text', 'Parent', inputpanel,'String', 'Quad:','Position', [x0 y0 40 20], ...
    'HorizontalAlignment', 'Right');
handles.quadpopup=uicontrol('Parent',f, 'Position', [x0+1.2*dx y0+dy 80 20], 'Style','popupmenu', 'String', handles.qnames,...
    'ToolTip','Quadrupole where the optical functions are know');
y0=y0-stepy;

handles.betaxlabel=uibutton('Style','text', 'Parent', inputpanel,'String', '\beta_x [m]=','Position', [x0 y0 40 20], ...
    'HorizontalAlignment', 'Right');
handles.betaxinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '5'           ,'Position', [x0+dx y0+dy 50 20], ...
    'BackgroundColor','w');
y0=y0-stepy;

handles.alphaxlabel=uibutton('Style','text', 'Parent', inputpanel,'String', '\alpha_x =','Position', [x0 y0 40 20], 'HorizontalAlignment', 'Right');
handles.alphaxinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '-1'           ,'Position', [x0+dx y0+dy 50 20], ...
    'BackgroundColor','w' );
y0=y0-stepy;

handles.betaylabel=uibutton('Style','text', 'Parent', inputpanel,'String', '\beta_y [m]=','Position', [x0 y0 40 20], 'HorizontalAlignment', 'Right');
handles.betayinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '5'           ,'Position', [x0+dx y0+dy 50 20] , ...
    'BackgroundColor','w');
y0=y0-stepy;

handles.alphaylabel=uibutton('Style','text', 'Parent', inputpanel,'String', '\alpha_y =','Position', [x0 y0 40 20], 'HorizontalAlignment', 'Right');
handles.alphayinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '-1'           ,'Position', [x0+dx y0+dy 50 20], ...
    'BackgroundColor','w' );
y0=y0-stepy;

handles.dxlabel=uibutton('Style','text', 'Parent', inputpanel,'String', 'D_x [m]=','Position', [x0 y0 40 20], 'HorizontalAlignment', 'Right');
handles.dxinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '0'           ,'Position', [x0+dx y0+dy 50 20], ...
    'BackgroundColor','w' );
y0=y0-stepy;

handles.dpxlabel=uibutton('Style','text', 'Parent', inputpanel,'String', 'Dp_x =','Position', [x0 y0 40 20], 'HorizontalAlignment', 'Right');
handles.dpxinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '0'           ,'Position', [x0+dx y0+dy 50 20], ...
    'BackgroundColor','w' );
y0=y0-stepy;

handles.exnlabel=uibutton('Style','text', 'Parent', inputpanel,'String', '\epsilon^n_x [mm\timesmrad]=','Position', [x0 y0 40 20], 'HorizontalAlignment', 'Right');
handles.exninput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '30'           ,'Position', [x0+dx y0+dy 50 20], ...
    'BackgroundColor','w','ToolTip','Normalized Horizontal Emittance' );
y0=y0-stepy;

handles.eynlabel=uibutton('Style','text', 'Parent', inputpanel,'String', '\epsilon^n_y [mm\timesmrad]=','Position', [x0 y0 40 20], 'HorizontalAlignment', 'Right');
handles.eyninput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '30'           ,'Position', [x0+dx y0+dy 50 20], ...
    'BackgroundColor','w','ToolTip','Normalized Vertical Emittance' );
y0=y0-stepy;

handles.energylabel=uibutton('Style','text', 'Parent', inputpanel,'String', 'E [MeV]=','Position', [x0 y0 40 20], 'HorizontalAlignment', 'Right');
handles.energyinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '100'           ,'Position', [x0+dx y0+dy 50 20], ...
    'BackgroundColor','w' );
y0=y0-stepy;

handles.dElabel=uibutton('Style','text', 'Parent', inputpanel,'String', '$\frac{\Delta E}{E}\ [\%]$ =','Position', [x0 y0 40 20], ...,
    'HorizontalAlignment', 'Right','Interpreter','latex');
handles.dEinput=uicontrol('Style','edit', 'Parent', inputpanel,'String', '0.5'           ,'Position', [x0+dx y0+dy 50 20], ...
    'BackgroundColor','w' );

%finally the button to restore the default values.

handles.restorebutton=uibutton('Style','pushbutton', 'Parent', inputpanel,'String', 'Restore\newline values', ...
   'CallBack', @restorebutton_Callback,'Units','normalized','Position',[0.1 0.1 0.8 0.1]);

%% Quad panel
handles.quadtable=uitable('Parent', quadpanel,'ColumnName',{ 'Current' , 'K'}, 'Units','normalized','Position',[0.05 0.25 0.9 0.7] );
handles.getdatabutton=uibutton('Style','pushbutton', 'Parent', quadpanel,'String', 'Get data\newline from machine', ...
    'ForegroundColor','b', 'CallBack', @getdatabutton_Callback,'Units','normalized','Position',[0.1 0.1 0.8 0.1]);

%% Tabpanels
% beta and size 
htab = getappdata(handles.tabpanel,'panels');
handles.opticframe=uipanel('Parent', htab(1), 'Position',[ 0 0 1 1]);
handles.opticaxe=axes('Parent', handles.opticframe, 'Position', [0.1 0.3 0.8 0.68]);
handles.latticeaxe=axes('Parent', handles.opticframe, 'Position', [0.1 0.075 0.8 0.15]);
handles.lattice=drawlatticeINFO(0,1, handles.latticeaxe);
handles.betax   =	line('parent',handles.opticaxe,'XData',handles.spos,'YData',0*handles.spos,'Color','r','Marker','+');
handles.betay   =	line('parent',handles.opticaxe,'XData',handles.spos,'YData',0.*handles.spos,'Color','b','Marker','s');
handles.dx      =	line('parent',handles.opticaxe,'XData',handles.spos,'YData',0.*handles.spos,'Color','g');
legend(handles.opticaxe,'\beta_x','\beta_y','10\timesD_x','Location','NorthOutside','Orientation','Horizontal')
set(handles.latticeaxe, 'YTick', []);
xlim= get(handles.latticeaxe,'XLim');
set(handles.opticaxe,'XLim', xlim);
set(handles.opticaxe, 'XTick',[]);
set(get(handles.opticaxe,'YLabel'),'String','[m]')
set(get(handles.latticeaxe,'XLabel'),'String','[m]')


handles.sizeframe=uipanel('Parent', htab(2), 'Position',[ 0 0 1 1]);
handles.envelopeaxe=axes('Parent', handles.sizeframe, 'Position', [0.1 0.3 0.8 0.68]);
handles.latticeaxe2=axes('Parent', handles.sizeframe, 'Position', [0.1 0.075 0.8 0.15]);
handles.lattice2=drawlatticeINFO(0,1, handles.latticeaxe2);
handles.sigmax   =	line('parent',handles.envelopeaxe,'XData',handles.spos,'YData',0*handles.spos,'Color','r','Marker','+');
handles.sigmay   =	line('parent',handles.envelopeaxe,'XData',handles.spos,'YData',0.*handles.spos,'Color','b','Marker','s');
legend(handles.envelopeaxe,'\sigma_x','\sigma_y','Location','NorthOutside','Orientation','Horizontal')
set(handles.latticeaxe2, 'YTick', []);
xlim= get(handles.latticeaxe2,'XLim');
set(handles.envelopeaxe,'XLim', xlim);
set(handles.envelopeaxe, 'XTick',[]);
set(get(handles.envelopeaxe,'YLabel'),'String','[mm]')
set(get(handles.latticeaxe2,'XLabel'),'String','[m]')


for loop=1:length(handles.lattice),
    itemstr1=['<HTML><Font color="red">&;&beta;<sub>x</sub>= ' num2str(0,'%f') 'm </Font><br />'];
    itemstr2=['<Font color="blue">&beta;<sub>y</sub>= ' num2str(0,'%f') ' m</Font><br />'];
    itemstr3=['<Font color="green">D<sub>x</sub>= ' num2str(0,'%f') ' m</Font></HTML>'];
    itemstr=[itemstr1 itemstr2 itemstr3];
    a=uimenu('Parent', get(handles.lattice(loop),'UIContextMenu'),'Separator','On','Label',itemstr);%'<HTML><b>&;&beta;<sub>x</sub> [m]</b></HTML>');
    itemstr1=['<HTML><Font color="red">&;&sigma;<sub>x</sub>= ' num2str(0,'%f') 'm </Font><br />'];
    itemstr2=['<Font color="blue">&sigma;<sub>y</sub>= ' num2str(0,'%f') ' m</Font></HTML>'];
    itemstr=[itemstr1 itemstr2];
    a=uimenu('Parent', get(handles.lattice2(loop), 'UIContextMenu'),'Separator','On','Label',itemstr);
end


% FS/OTR panels

for loop=1:handles.n_fsotr;
   handles.fsortframe(loop)= uipanel('Parent', htab(2+loop), 'Position',[ 0 0 1 1]);
   handles.fsotraxe(loop)=axes('Parent', handles.fsortframe(loop), 'Position', [0.1 0.1 0.8 0.8]);
   set(get(handles.fsotraxe(loop),'YLabel'),'String',' y [mm]');
   set(get(handles.fsotraxe(loop),'XLabel'),'String',' x [mm]');
   set(get(handles.fsotraxe(loop),'Title'),'String',['1 \sigma envelope at FS/OTR ' num2str(loop)]);
   handles.fsotrline(loop) = line('parent',handles.fsotraxe(loop),'XData',0*(1:handles.npoints+1),'YData',0*(1:handles.npoints+1),'Color','g');
end
handles.resultframe=uipanel('Parent', htab(handles.n_fsotr+3), 'Position',[ 0 0 1 1]);
handles.resulttable=uitable('Parent', handles.resultframe,'ColumnName',{ 'rms(x) [mm]', 'rms(y) [mm]'}, 'Units','normalized','Position',[0.05 0.05 0.9 0.9] );

%% Finally, a small menu bar
m1=uimenu('Parent',f,'Label','File');
m11=uimenu('Parent',m1,'Label','Create print','Callback', @print_menu);
m12=uimenu('Parent',m1,'Label','Exit','Separator','on','Callback', @exit_menu);

% save the handles
guidata(f,handles);

% center the gui in the screen
movegui(f, 'center');

function print_menu(hObject, eventdata, handles)
handles=guidata(hObject);
nf=figure;
h = subplot(2,1,1);
p = get(h, 'Position');
delete(h);
b = copyobj(handles.opticframe, nf);
set(b, 'Position', p);
h = subplot(2,1,2);
p = get(h, 'Position');
delete(h);
b = copyobj(handles.sizeframe, nf);
set(b, 'Position', p);




function exit_menu(hObject, eventdata, handles)
    closeME(hObject, eventdata);
    
function closeME(hObject, eventdata)    
    disp 'Closing the figure and children'
    handles=guidata(hObject);
    delete(findall(0,'Tag','DeleteOnClose'))
    delete(handles.f);
    

function restorebutton_Callback(hObject, eventdata, handles)
    handles=guidata(hObject);
    set(handles.betaxinput,'String',2.17);    
    set(handles.alphaxinput,'String',-0.88);
    set(handles.betayinput,'String',15.35);
    set(handles.alphayinput,'String',2.79);
    set(handles.dxinput,'String',0);
    set(handles.dpxinput,'String',0);
    set(handles.energyinput,'String','107')
    set(handles.dEinput,'String','0.4')
    set(handles.exninput,'String','37')
    set(handles.eyninput,'String','37')
    getdatabutton_Callback(hObject, eventdata, handles)
    
function CreateTab(htab,evdt,hpanel,hstatus)



    
    
        