function getdatabutton_Callback(hObject, eventdata, handles)
    handles=guidata(hObject);
    [name, valueI, valueK]=get_quads_machine(hObject);
    global THERING;
    tw0=twissring(THERING, 0,1,'Chrom');
    tw0.beta=[str2num(get(handles.betaxinput,'String')) str2num(get(handles.betayinput,'String'))];
    tw0.alpha=[str2num(get(handles.alphaxinput,'String')) str2num(get(handles.alphayinput,'String'))];
    tw0.Dispersion(1)=str2num(get(handles.dxinput,'String'));
    tw0.Dispersion(2)=str2num(get(handles.dpxinput,'String'));
    dE=str2num(get(handles.dEinput,'String'));
    selectedquad_atindex=handles.qindex( get(handles.quadpopup,'Value'));
    tmp_index=0;
    %BACKRING=[];
    for loop=(selectedquad_atindex-1):-1:1;
        tmp_index=tmp_index+1;
        BACKRING{tmp_index}=THERING{loop};
    end
    %FORWARDRING=[];
    tmp_index=0;
    for loop=selectedquad_atindex:length(THERING);
        tmp_index=tmp_index+1;
        FORWARDRING{tmp_index}=THERING{loop};
    end
    twb=tw0;
    twb.alpha=-tw0.alpha;
    twiss_backward=twissline(BACKRING, 0, twb, 1:length(BACKRING)+1,'Chrom');
    twiss_forward=twissline(FORWARDRING, 0, tw0, 1:length(FORWARDRING)+1,'Chrom');
    BETAB_org = cat(1,twiss_backward.beta);
    BETAB=BETAB_org((end):-1:2,:);
    DxB =  0*cat(2,twiss_backward.Dispersion)';
    BETAF = cat(1,twiss_forward.beta);
    DxF =  cat(2,twiss_forward.Dispersion)';
    BETA= cat(1,BETAB, BETAF);
    Dx=     cat(1,DxB(end:-1:2,1), DxF(:,1));
    set(handles.betax,'YData',BETA(:,1));
    set(handles.betay,'YData',BETA(:,2));
    set(handles.dx,'YData',10*Dx(:,1));
    % beam size
    gamma=str2num(get(handles.energyinput,'String'))*1.957;
    ex=1e-6*str2num(get(handles.exninput,'String'))/(gamma*(sqrt(1-1/(gamma^2))));
    ey=1e-6*str2num(get(handles.eyninput,'String'))/(gamma*(sqrt(1-1/(gamma^2))));
    dE=str2num(get(handles.dEinput,'String'))/100;
    sx=1e3*sqrt(ex*BETA(:,1)+dE^2* Dx(:,1).*Dx(:,1)); 
    sy=1e3*sqrt(ey*BETA(:,2)); 
    set(handles.sigmax,'YData',sx);
    set(handles.sigmay,'YData',sy);
    % Table
    for loop=1:length(name), 
        n(loop,:)=cell2mat(name{loop}); 
        data(loop,:)=[valueI(loop), valueK(loop)] ;
    end;
    set(handles.quadtable,'Rowname', n,'data', data);
    for loop=1:handles.n_fsotr;
        xo=sx(handles.ati.FSOTR(loop));
        yo=sy(handles.ati.FSOTR(loop));
        set(handles.fsotrline(loop), 'XData',xo*cos(2*pi*(1:handles.npoints+1)/handles.npoints),...
            'YData', yo*sin(2*pi*(1:handles.npoints+1)/handles.npoints));
    end
    clear name data;
    name(1,:)={'Start'};
    data(1,:)=[sx(1),  sy(1)];
    for loop=1:handles.n_fsotr,
        name(1+loop,:)={['FS/OTR ' num2str(loop)]};
        data(1+loop,:)=[sx(handles.ati.FSOTR(loop)),  sy(handles.ati.FSOTR(loop))];
    end
    name(end+1,:)={'End'};
    data(end+1,:)=[sx(end),  sy(end)];
    
    set(handles.resulttable,'Rowname', name,'data', data);
    handles.sx=sx;
    handles.sy=sy;
    
    % Put the values in the context menus of the lattice.
   for loop=1:length(handles.lattice),
    m1=get(get(handles.lattice(loop),'UiContextMenu'),'Children');
    m2=get(get(handles.lattice2(loop),'UiContextMenu'),'Children');
    index=get(handles.lattice(loop),'Userdata');
    itemstr1=['<HTML><Font color="red">&;&beta;<sub>x</sub>= ' num2str(BETA(index,1),'%.3f') 'm </Font><br />'];
    itemstr2=['<Font color="blue">&beta;<sub>y</sub>= ' num2str(BETA(index,2),'%.3f') ' m</Font><br />'];
    itemstr3=['<Font color="green">D<sub>x</sub>= ' num2str(Dx(index,1),'%.3f') ' m</Font></HTML>'];
    itemstr=[itemstr1 itemstr2 itemstr3];
    set(m1(1),'Label', itemstr);
    itemstr1=['<HTML><Font color="red">&;&sigma;<sub>x</sub>= ' num2str(sx(index),'%.1f') ' mm</Font><br />'];
    itemstr2=['<Font color="blue">&sigma;<sub>y</sub>= ' num2str(sy(index),'%.1f') ' mm</Font></HTML>'];
    itemstr=[itemstr1 itemstr2];
    set(m2(1),'Label', itemstr);
   end
    
    
    
    
    guidata(handles.f,handles);
    
    function  [name, valueI, valueK]=get_quads_machine(hObject)
    handles=guidata(hObject);
    ao=getao;
    Energy=str2num(get(handles.energyinput,'String'))/1000;
    Q=findmemberof('QUAD');
    nquads=1;
    name=[];
    valueI=[];
    valueK=[];
    for loop=1:length(Q),
        vq=getsp(Q{loop}, 'Hardware');
        commonnames=(ao.(Q{loop}).CommonNames);
        for loop2=1:length(vq),
            valueI(nquads)=vq(loop2);
            valueK(nquads)=hw2physics(Q{loop},'Monitor',valueI(nquads), 1, Energy);
            name{nquads}=(ao.(Q{loop}).DeviceName(loop2));
            setsp(commonnames(loop2,:),valueK(nquads),'Physics','Model');
            nquads=nquads+1;
        end
    end