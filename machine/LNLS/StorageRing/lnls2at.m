function k = tls2at(Family, Field, Amps, DeviceList, Energy)
%TLS2AT - Converts amperes to simulator values
%  K = mls2at(Family, Field, Amps, DeviceList, Energy)
%
%  INPUTS
%  1. Family - Family name
%  2. Field - Sub-field (like 'Setpoint')
%  3. Amps - Ampere
%  4. DeviceList - Device list (Amps and DeviceList must have the same number of rows)
%  5. Energy - Energy in GeV {Default: getenergy}
%              If Energy is a vector, each output column will correspond to that energy.
%              Energy can be anything getenergy accepts, like 'Model' or 'Online'.
%              (Note: If Energy is a vector, then Amps can only have 1 column)
%
%  OUTPUTS
%  1. K - "K-value" (AT convention)
%     For dipole:      K = B / Brho
%     For quadrupole:  K = B'/ Brho
%     For sextupole:   K = B"/ Brho / 2
%
%  See also at2tls, hw2physics, physics2hw


if nargin < 3
    error('At least 3 input required');
end

if isempty(Field)
    Field = 'Setpoint';
end

if nargin < 4
    DeviceList = [];
end
if isempty(DeviceList)
    DeviceList = family2dev(Family);
end

if nargin < 5
    Energy = [];
end
if isempty(Energy)
    Energy = getenergy;
elseif ischar(Energy)
    Energy = getenergy(Energy);
end


if size(Amps,1) == 1 && length(DeviceList) > 1
    Amps = ones(size(DeviceList,1),1) * Amps;
elseif size(Amps,1) ~= size(DeviceList,1)
    error('Rows in Amps must equal rows in DeviceList or be a scalar');
end


if all(isnan(Amps))
    k = Amps;
    return
end


% Force Energy and Amps to have the same number of columns
if all(size(Energy) > 1)
    error('Energy can only be a scalar or vector');
end
Energy = Energy(:)';

if length(Energy) > 1
    if size(Amps,2) == size(Energy,2)
        % OK
    elseif size(Amps,2) > 1
        error('If Energy is a vector, then Amps can only have 1 column.');
    else
        % Amps has one column, expand to the size of Energy
        Amps = Amps * ones(1,size(Energy,2));
    end
else
    Energy = Energy * ones(1,size(Amps,2));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setpoint and Monitor Fields %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if any(strcmpi(Field, {'Setpoint','Monitor'}))
    Brho = getbrho(Energy);
    if strcmpi(Family, 'HCM')
        % HCM
        % Rad = BLeff / Brho
        BLeffPerI = 7e5 / 1e4 / 100;  % T*m/Amp 7e5 G.cm/Amp  @ 1.37 GeV
        k = BLeffPerI .* Amps ./ Brho;
    elseif strcmpi(Family, 'VCM')
        % VCM
        % Rad = BLeff / Brho
        BLeffPerI = 7e5 / 1e4 / 100;  % T*m/Amp 7e5 G.cm/Amp  @ 1.37 GeV
        k = BLeffPerI .* Amps ./ Brho;
    elseif any(strcmpi(Family, {'QF','QD','QFC'}))
        Brho1p37 = 4.571532295839962;
        k =  0.016 * Amps * (Brho / Brho1p37);  %  [K/I] or [(1/m2)/Amp] @ 1.37 GeV
    else
        k = Amps;
    end
    return
end


% If you made it to here, I don't know how to convert it
k = Amps;
return

