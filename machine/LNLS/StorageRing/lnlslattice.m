function lnls_v10

global FAMLIST THERING

Energy = 1.37e9;
FAMLIST = cell(0);

disp(['   Loading LNLS VUV Lattice: ', mfilename]);

L0 = 93.21204;    % design length [m]
C0 = 299792458;   % speed of light [m/s]
HarmNumber = 148;
CAV	= rfcavity('RF' , 0 , 0.5e+6 , HarmNumber*C0/L0, HarmNumber ,'CavityPass');  


li_len  = 2.39867;
lsq_len = 0.31;
sxt_len = 0.10;
qdp_len = 0.25;
lb_len  = 0.832 - sxt_len/2;
l1_len  = 0.39;

li1_len  = 2.07267;
li2_len  = li_len - li1_len;
lb1_len  = 0.12;
lb2_len  = lb_len - lb1_len;
l11_len  = 0.185; 
lsq1_len = 0.145;

li21_len = 0.148;
li22_len = li2_len - li21_len;

b      = rbend('BEND', 1.432, 2*pi/12,(2*pi/12)/2, (2*pi/12)/2, 0,'BndMPoleSymplectic4Pass');
hsf    = sextupole('SF', sxt_len/2, 0, 'StrMPoleSymplectic4Pass');
sd     = sextupole('SD', sxt_len,   0, 'StrMPoleSymplectic4Pass');

% --- normal mode ---

%mode     = 'normal mode';
%qf_str   = 2.724986956698;
%qd_str   = -2.495002737845;
%qfw_str  = 2.724986956698;
%qdw_str  = -2.495002737845;
%qfc_str  = 2.11326453924;

% --- low betaz at ss 01 and 11 ---

mode     = 'low vertical beta in ss 01 and 11';
qf_str   = 2.675926636139;
qd_str   = -2.351671730885;
qfw_str  = 2.923399071069;
qdw_str  = -3.066023816313;
qfc_str  = 2.11326453924;

% --- common elements ---

qfc    = quadrupole('QFC',  qdp_len, qfc_str, 'StrMPoleSymplectic4Pass');

lb   = drift('lb',  0.832 - sxt_len/2, 'DriftPass');
%l1   = drift('l1',  l1_len,    'DriftPass');
%lsq  = drift('lsq', 0.31,    'DriftPass');
%li   = drift('li',  li_len,  'DriftPass');
%li2   = drift('li2',   li2_len, 'DriftPass');


la   = drift('la',  1.125 - lsq_len - sxt_len, 'DriftPass');
l2   = drift('l2',  0.84,    'DriftPass');
li1   = drift('li1',   li1_len, 'DriftPass');
lb1   = drift('lb1',   lb1_len, 'DriftPass');
lb2   = drift('lb2',   lb2_len, 'DriftPass');
l11   = drift('l11',   l11_len, 'DriftPass');
l12   = drift('l12',   l1_len - l11_len, 'DriftPass');
li21  = drift('li21',  li21_len, 'DriftPass');
li22  = drift('li22',  li22_len, 'DriftPass');
lsq1  = drift('lsq1',  lsq1_len, 'DriftPass');
lsq2  = drift('lsq2',  lsq_len - lsq1_len, 'DriftPass');

% --- straight section 02 ---

qfb   = quadrupole('QF', qdp_len, qfw_str,  'StrMPoleSymplectic4Pass');
qdb   = quadrupole('QD', qdp_len, qdw_str,  'StrMPoleSymplectic4Pass');
qfa   = quadrupole('QF', qdp_len, qf_str,  'StrMPoleSymplectic4Pass');
qda   = quadrupole('QD', qdp_len, qd_str,  'StrMPoleSymplectic4Pass');

achbl  = corrector ('HCM', 0, [0 0],  'CorrectorPass');
ampbl  = marker    ('BPM', 'IdentityPass');
acvbl  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampas  = marker    ('BPM', 'IdentityPass');
achbs  = corrector ('HCM',  0, [0 0],  'CorrectorPass');
ampbs  = marker    ('BPM', 'IdentityPass');
acval  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampal  = marker    ('BPM', 'IdentityPass');
achal  = corrector ('HCM', 0, [0 0],  'CorrectorPass');

ssecb = [li1 achbl li21 ampbl li22 qfb l11 acvbl l12 qdb  l2];
dispa = [la sd lsq2 ampas lsq1 qfc lb hsf];
dispb = [hsf lb1 achbs lb2 qfc lsq1 ampbs lsq2 sd la];
sseca = [l2 qda l12 acval l11 qfa li22 ampal li21 achal li1];
sup02 = [ssecb b dispa dispb b sseca];

% --- straight section 04 ---

qfb   = quadrupole('QF', qdp_len, qf_str,  'StrMPoleSymplectic4Pass');
qdb   = quadrupole('QD', qdp_len, qd_str,  'StrMPoleSymplectic4Pass');
qfa   = quadrupole('QF', qdp_len, qf_str,  'StrMPoleSymplectic4Pass');
qda   = quadrupole('QD', qdp_len, qd_str,  'StrMPoleSymplectic4Pass');

achbl  = corrector ('HCM', 0, [0 0],  'CorrectorPass');
ampbl  = marker    ('BPM', 'IdentityPass');
acvbl  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampas  = marker    ('BPM', 'IdentityPass');
achbs  = corrector ('HCM',  0, [0 0],  'CorrectorPass');
ampbs  = marker    ('BPM', 'IdentityPass');
acval  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampal  = marker    ('BPM', 'IdentityPass');
achal  = corrector ('HCM', 0, [0 0],  'CorrectorPass');

ssecb = [li1 achbl li21 ampbl li22 qfb l11 acvbl l12 qdb  l2];
dispa = [la sd lsq2 ampas lsq1 qfc lb hsf];
dispb = [hsf lb1 achbs lb2 qfc lsq1 ampbs lsq2 sd la];
sseca = [l2 qda l12 acval l11 qfa li22 ampal li21 achal li1];
sup04 = [ssecb b dispa dispb b sseca];

% --- straight section 06 ---

qfb   = quadrupole('QF', qdp_len, qf_str,  'StrMPoleSymplectic4Pass');
qdb   = quadrupole('QD', qdp_len, qd_str,  'StrMPoleSymplectic4Pass');
qfa   = quadrupole('QF', qdp_len, qf_str,  'StrMPoleSymplectic4Pass');
qda   = quadrupole('QD', qdp_len, qd_str,  'StrMPoleSymplectic4Pass');

achbl  = corrector ('HCM', 0, [0 0],  'CorrectorPass');
ampbl  = marker    ('BPM', 'IdentityPass');
acvbl  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampas  = marker    ('BPM', 'IdentityPass');
achbs  = corrector ('HCM',  0, [0 0],  'CorrectorPass');
ampbs  = marker    ('BPM', 'IdentityPass');
acval  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampal  = marker    ('BPM', 'IdentityPass');
achal  = corrector ('HCM', 0, [0 0],  'CorrectorPass');

ssecb = [li1 achbl li21 ampbl li22 qfb l11 acvbl l12 qdb  l2];
dispa = [la sd lsq2 ampas lsq1 qfc lb hsf];
dispb = [hsf lb1 achbs lb2 qfc lsq1 ampbs lsq2 sd la];
sseca = [l2 qda l12 acval l11 qfa li22 ampal li21 achal li1];
sup06 = [ssecb b dispa dispb b sseca];


% --- straight section 08 ---

qfb   = quadrupole('QF', qdp_len, qf_str,  'StrMPoleSymplectic4Pass');
qdb   = quadrupole('QD', qdp_len, qd_str,  'StrMPoleSymplectic4Pass');
qfa   = quadrupole('QF', qdp_len, qf_str,  'StrMPoleSymplectic4Pass');
qda   = quadrupole('QD', qdp_len, qd_str,  'StrMPoleSymplectic4Pass');

achbl  = corrector ('HCM', 0, [0 0],  'CorrectorPass');
ampbl  = marker    ('BPM', 'IdentityPass');
acvbl  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampas  = marker    ('BPM', 'IdentityPass');
achbs  = corrector ('HCM',  0, [0 0],  'CorrectorPass');
ampbs  = marker    ('BPM', 'IdentityPass');
acval  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampal  = marker    ('BPM', 'IdentityPass');
achal  = corrector ('HCM', 0, [0 0],  'CorrectorPass');

ssecb = [li1 achbl li21 ampbl li22 qfb l11 acvbl l12 qdb  l2];
dispa = [la sd lsq2 ampas lsq1 qfc lb hsf];
dispb = [hsf lb1 achbs lb2 qfc lsq1 ampbs lsq2 sd la];
sseca = [l2 qda l12 acval l11 qfa li22 ampal li21 achal li1];
sup08 = [ssecb b dispa dispb b sseca];

% --- straight section 10 ---

qfb   = quadrupole('QF', qdp_len, qf_str,  'StrMPoleSymplectic4Pass');
qdb   = quadrupole('QD', qdp_len, qd_str,  'StrMPoleSymplectic4Pass');
qfa   = quadrupole('QF', qdp_len, qfw_str,  'StrMPoleSymplectic4Pass');
qda   = quadrupole('QD', qdp_len, qdw_str,  'StrMPoleSymplectic4Pass');

achbl  = corrector ('HCM', 0, [0 0],  'CorrectorPass');
ampbl  = marker    ('BPM', 'IdentityPass');
acvbl  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampas  = marker    ('BPM', 'IdentityPass');
achbs  = corrector ('HCM',  0, [0 0],  'CorrectorPass');
ampbs  = marker    ('BPM', 'IdentityPass');
acval  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampal  = marker    ('BPM', 'IdentityPass');
achal  = corrector ('HCM', 0, [0 0],  'CorrectorPass');

ssecb = [li1 achbl li21 ampbl li22 qfb l11 acvbl l12 qdb  l2];
dispa = [la sd lsq2 ampas lsq1 qfc lb hsf];
dispb = [hsf lb1 achbs lb2 qfc lsq1 ampbs lsq2 sd la];
sseca = [l2 qda l12 acval l11 qfa li22 ampal li21 achal li1];
sup10 = [ssecb b dispa dispb b sseca];

% --- straight section 12 ---

qfb   = quadrupole('QF', qdp_len, qfw_str,  'StrMPoleSymplectic4Pass');
qdb   = quadrupole('QD', qdp_len, qdw_str,  'StrMPoleSymplectic4Pass');
qfa   = quadrupole('QF', qdp_len, qfw_str,  'StrMPoleSymplectic4Pass');
qda   = quadrupole('QD', qdp_len, qdw_str,  'StrMPoleSymplectic4Pass');

achbl  = corrector ('HCM', 0, [0 0],  'CorrectorPass');
ampbl  = marker    ('BPM', 'IdentityPass');
acvbl  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampas  = marker    ('BPM', 'IdentityPass');
achbs  = corrector ('HCM',  0, [0 0],  'CorrectorPass');
ampbs  = marker    ('BPM', 'IdentityPass');
acval  = corrector ('VCM', 0, [0 0],  'CorrectorPass');
ampal  = marker    ('BPM', 'IdentityPass');
achal  = corrector ('HCM', 0, [0 0],  'CorrectorPass');

ssecb = [li1 achbl li21 ampbl li22 qfb l11 acvbl l12 qdb  l2];
dispa = [la sd lsq2 ampas lsq1 qfc lb hsf];
dispb = [hsf lb1 achbs lb2 qfc lsq1 ampbs lsq2 sd la];
sseca = [l2 qda l12 acval l11 qfa li22 ampal li21 achal li1];
sup12 = [ssecb b dispa dispb b sseca];


FIN =  marker('FIN', 'IdentityPass');

% --- ring ---

elist = [CAV sup02 sup04 sup06 sup08 sup10 sup12 FIN];
buildlat(elist);


% Newer AT versions requires 'Energy' to be an AT field
THERING = setcellstruct(THERING, 'Energy', 1:length(THERING), Energy);

% Compute total length and RF frequency
L0_tot = findspos(THERING, length(THERING)+1);
fprintf('   L0 = %.6f m   (design length 93.212 m)\n', L0_tot);
fprintf('   RF = %.6f MHz\n', HarmNumber*C0/L0_tot/1e6);

%radiationoff;
%cavityoff;

% Compute initial tunes before loading errors
[InitialTunes, InitialChro]= tunechrom(THERING,0,'chrom','coupling');
% fprintf('   Tunes and chromaticities might be slightly incorrect if synchrotron radiation and cavity are on\n');
fprintf('   Tunes: nu_x=%g, nu_y=%g\n',InitialTunes(1),InitialTunes(2));
fprintf('   Chromaticities: xi_x=%g, xi_y=%g\n',InitialChro(1),InitialChro(2));

clear global FAMLIST GLOBVAL
evalin('base','clear LOSSFLAG');
