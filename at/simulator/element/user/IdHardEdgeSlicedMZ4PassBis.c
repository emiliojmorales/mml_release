
/*<<<<<<<<<<<<<<<<<<<  IdHardEdgeSlicedMultZeus4PassBis  >>>>>>>>>>>>>>>>>>>>>
 *----------------------------------------------------------------------------
 * Modification Log:
 * -----------------
 * .01  2008-01-12      Z.Martí: Implementing a sliced "hard edge" Id model :
 * 
 * A series of dipoles with multipole thin lenses in the edges!
 *
 *
 *
Calculate multipole kick in a curved elemrnt (bending magnet)
The reference coordinate system  has the curvature given by the inverse 
(design) radius irho.
IMPORTANT !!!
The magnetic field Bo that provides this curvature MUST NOT be included in the dipole term
PolynomB[1](MATLAB notation)(C: B[0] in this function) of the By field expansion

The kick is given by

           e L      L delta      L x
theta  = - --- B  + -------  -  -----  , 
     x     p    y     rho           2
            0                    rho

         e L
theta  = --- B
     y    p   x
           0


Note: in the US convention the transverse multipole field is written as:

                         max_order+1
                           ----
                           \                       n-1
	   (B + iB  )/ B rho  =  >   (ia  + b ) (x + iy)
         y    x            /       n    n
	                       ----
                          n=1
	is a polynomial in (x,y) with the highest order = MaxOrder
	

	Using different index notation 
   
                         max_order
                           ----
                           \                       n
	   (B + iB  )/ B rho  =  >   (iA  + B ) (x + iy)
         y    x            /       n    n
	                       ----
                          n=0

	A,B: i=0 ... max_order
   [0] - dipole, [1] - quadrupole, [2] - sextupole ...
   units for A,B[i] = 1/[m]^(i+1)
	Coeficients are stroed in the PolynomA, PolynomB field of the element
	structure in MATLAB

	A[i] (C++,C) =  PolynomA(i+1) (MATLAB) 
	B[i] (C++,C) =  PolynomB(i+1) (MATLAB) 
	i = 0 .. MaxOrder

******************************************************************************/


#include "mex.h"
#include<stdio.h>
#include<math.h>
#include "elempass.h"
#include "atlalib.c"
#include "atphyslib.c"

#define DRIFT1    0.6756035959798286638
#define DRIFT2   -0.1756035959798286639
#define KICK1     1.351207191959657328
#define KICK2    -1.702414383919314656
#define small    0.0000000000000000000000001


#define SQR(X) ((X)*(X))


/******************************************************************************/
/* PHYSICS SECTION ************************************************************/

/* referenced to the orbit*/

void bndthinkick(double* r, double* A, double* B, double L, int max_order)
{  int i;
	double ReSum = B[max_order];
 	double ImSum = A[max_order];
    double irho_x=B[0],irho_y=-A[0];
    double f=(irho_x*r[0]+irho_y*r[2]);
	double ReSumTemp;
    
	for(i=max_order-1;i>=0;i--)
		{	ReSumTemp = ReSum*r[0] - ImSum*r[2] +B[i];
			ImSum = ImSum*r[0] +  ReSum*r[2] + A[i];
			ReSum = ReSumTemp;
		}
	r[1] +=  L*(irho_x*(1.0+r[4])-ReSum*(1.0+f));
	r[3] +=  L*(irho_y*(1.0+r[4])+ImSum*(1.0+f));
	r[5] +=  L*f;
}
void Edgethinkick(double* r, double* A, double* B, int max_order, double sing)
{  int i;
   double ReSum = B[max_order]/(max_order*1.0e0+1.0e0);
   double ImSum = A[max_order]/(max_order*1.0e0+1.0e0);

/*
   double irho_x=B[0],irho_y=-A[0];
   double f=(irho_x*r[0]+irho_y*r[2]);
*/
   double ReSumTemp;
   for(i=max_order-1;i>=0;i--) 
   {
       ReSumTemp = ReSum*r[0] - ImSum*r[2] +B[i]/(i*1.0e0+1.0e0);
       ImSum = ImSum*r[0] +  ReSum*r[2] +A[i]/(i*1.0e0+1.0e0);
       ReSum = ReSumTemp;
   }
   ReSumTemp = ReSum*r[0] - ImSum*r[2];
   ImSum = ImSum*r[0] +  ReSum*r[2];
   ReSum = ReSumTemp;

   
/*
   r[1] +=sing*(r[1]*f/(2.0e0)+(r[1]*ImSum+r[3]*ReSum)/(1.0e0+r[4])/4.0e0);
   r[3] +=sing*(r[3]*f/(2.0e0)+(r[1]*ReSum-r[3]*ImSum)/(1.0e0+r[4])/4.0e0);
*/
   
   r[1] +=sing*(r[1]*ReSum/(1.0e0+r[4])/2.0e0);
   r[3] +=sing*(r[3]*ReSum/(1.0e0+r[4])/2.0e0);
}

/*referenced to the devidce axis*/
/*
void bndthinkick(double* r, double* A, double* B, double L, int max_order)
{  int i;
   double ReSum = B[max_order];
   double ImSum = A[max_order];
   double ReSumTemp;
   for(i=max_order-1;i>=0;i--)
   {   
       ReSumTemp = ReSum*r[0] - ImSum*r[2] + B[i];
       ImSum = ImSum*r[0] +  ReSum*r[2] + A[i];
       ReSum = ReSumTemp;
   }
   
   r[1] -=  L*ReSum;
   r[3] +=  L*ImSum;
}

void Edgethinkick(double* r, double* A, double* B, int max_order, double sing)
{  int i;
   double ReSum = B[max_order]/(max_order*1.0e0+1.0e0);
   double ImSum = A[max_order]/(max_order*1.0e0+1.0e0);
   double ReSumTemp;
   for(i=max_order-1;i>=0;i--) 
   {
       ReSumTemp = ReSum*r[0] - ImSum*r[2] + B[i]/(i*1.0e0+1.0e0);
       ImSum = ImSum*r[0] +  ReSum*r[2] + A[i]/(i*1.0e0+1.0e0);
       ReSum = ReSumTemp;
   }
   ReSumTemp = ReSum*r[0] - ImSum*r[2];
   ImSum = ImSum*r[0] +  ReSum*r[2];
   ReSum = ReSumTemp;
   
   r[1] +=sing*(r[1]*ImSum+r[3]*ReSum)/(1.0e0+r[4])/4.0e0;
   r[3] +=sing*(r[1]*ReSum-r[3]*ImSum)/(1.0e0+r[4])/4.0e0;
}

*/

void IdHardEdgeSliced4(double *r, double le, double Lslices, int Nslices,int subNslices, 
                    double *A, double *B, int max_order,int Nmeth,double *T1, double *T2,	
					double *R1, double *R2, int num_particles)
{	int j,j1,j2,c,m;
	double norm, NormL1, NormL2;	
	double *r6,*A_array,*B_array;
    double theta,irho,SL0,entrance_angle,exit_angle,SL, L1, L2, K1, K2;
	bool useT1, useT2, useR1, useR2;
	

    SL0 = Lslices/(subNslices*1.0e00);          
    L1 = SL0*DRIFT1;    
    L2 = SL0*DRIFT2;    
    K1 = SL0*KICK1;
    K2 = SL0*KICK2;
    FILE *arxiu;
    
    
    /*arxiu=fopen("trackInside.dat","w");*/
     
	if(T1==NULL)
	    useT1=false;
	else 
	    useT1=true;  
	    
    if(T2==NULL)
	    useT2=false; 
	else 
	    useT2=true;  
	
	if(R1==NULL)
	    useR1=false; 
	else 
	    useR1=true;  
	    
    if(R2==NULL)
	    useR2=false;
	else 
	    useR2=true;
    
    for(c = 0;c<num_particles;c++)	/*Loop over particles  */
			
    {	
        r6 = r+c*6;	
        if(!mxIsNaN(r6[0]))
        {   
/*  misalignment at entrance  */	
            if(useT1)   
                ATaddvv(r6,T1); 
            if(useR1)      
                ATmultmv(r6,R1);

            /*  integrator  */
            for(j=0; j < Nslices; j++)
            {
                A_array=A+j*(max_order+1);
                B_array=B+j*(max_order+1);
                
                
                theta=2.0e0*asin(SL0*B_array[0]/2.0e0);
                SL=theta/(B_array[0]+small);
                irho = B_array[0];
                /*fprintf(arxiu,"%e %e %e %e %e %e\n",r6[0],r6[1],r6[2],r6[3],r6[4],r6[5]);*/
              
                
                L1 = SL*DRIFT1/(1.0e0+r6[4]);
                L2 = SL*DRIFT2/(1.0e0+r6[4]);
                K1 = SL*KICK1/(1.0e0+r6[4]);
                K2 = SL*KICK2/(1.0e0+r6[4]);
                
                /*fprintf(arxiu,"%e %e %e %e %e %e\n",B_array[0],A_array[0],L1,K1,L2,K2);*/
              
                Edgethinkick(r6, A_array, B_array, max_order,1.0e0);
                for(m=0; m < subNslices; m++) /* Loop over sub slices*/			
                {				
                    ATdrift6(r6,L1);  
                    bndthinkick(r6, A_array, B_array, K1,max_order);	
                    ATdrift6(r6,L2); 
                    bndthinkick(r6, A_array, B_array, K2,max_order);	
                    ATdrift6(r6,L2);	
                    bndthinkick(r6, A_array, B_array,  K1,max_order);	
                    ATdrift6(r6,L1);		
                }  
                Edgethinkick(r6, A_array, B_array, max_order,-1.0e0);
            }		

            /* Misalignment at exit */	      
            if(useR2)     
                ATmultmv(r6,R2);   
            if(useT2)           
                ATaddvv(r6,T2);	    
        }	
    }
       
    /*fclose(arxiu);*/
}




/********** END PHYSICS SECTION ***********************************************/
/******************************************************************************/



ExportMode int* passFunction(const mxArray *ElemData, int *FieldNumbers,
								double *r_in, int num_particles, int mode)

#define NUM_FIELDS_2_REMEMBER 12


{	double *A , *B;
	double  *pr1, *pr2, *pt1, *pt2;   

	int max_order, Nmeth, Nslices, subNslices;
	double le,Lslices;
	int *returnptr;
	int *NewFieldNumbers, fnum;

	
	switch(mode)
		{   case MAKE_LOCAL_COPY: 	/* Find field numbers first
										Save a list of field number in an array
										and make returnptr point to that array
									*/
				{	
					/* Allocate memory for integer array of 
					    field numbers for faster future reference
					*/
		
					NewFieldNumbers = (int*)mxCalloc(NUM_FIELDS_2_REMEMBER,sizeof(int));

					/* Populate */
					
					
					
					fnum = mxGetFieldNumber(ElemData,"PolynomA");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'PolynomA' was not found in the element data structure"); 
					NewFieldNumbers[0] = fnum;
					A = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					
					
					fnum = mxGetFieldNumber(ElemData,"PolynomB");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'PolynomB' was not found in the element data structure"); 
					NewFieldNumbers[1] = fnum;
					B = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));				
					
					fnum = mxGetFieldNumber(ElemData,"MaxOrder");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'MaxOrder' was not found in the element data structure"); 
					NewFieldNumbers[2] = fnum;
					max_order = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));
					
					fnum = mxGetFieldNumber(ElemData,"Nmeth");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'Nmeth' was not found in the element data structure"); 
					NewFieldNumbers[3] = fnum;
					Nmeth = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));
					
					
					fnum = mxGetFieldNumber(ElemData,"Length");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'Length' was not found in the element data structure"); 
					NewFieldNumbers[4] = fnum;
					le = mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));
					
					fnum = mxGetFieldNumber(ElemData,"Lslices");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'Lslices' was not found in the element data structure"); 
					NewFieldNumbers[5] = fnum;
					Lslices = mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));
                    
					fnum = mxGetFieldNumber(ElemData,"Nslices");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'Nslices' was not found in the element data structure"); 
					NewFieldNumbers[6] = fnum;
					Nslices = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));
					fnum = mxGetFieldNumber(ElemData,"subNslices");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'subNslices' was not found in the element data structure"); 
					NewFieldNumbers[7] = fnum;
					subNslices = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));                    
                    
                    

                    
				
                    fnum = mxGetFieldNumber(ElemData,"R1");
					NewFieldNumbers[8] = fnum;
					if(fnum<0)
					    pr1 = NULL;
					else
					    pr1 = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					

					fnum = mxGetFieldNumber(ElemData,"R2");
					NewFieldNumbers[9] = fnum;
					if(fnum<0)
					    pr2 = NULL;
					else
					    pr2 = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					
					
                    fnum = mxGetFieldNumber(ElemData,"T1");
	                NewFieldNumbers[10] = fnum;
					if(fnum<0)
					    pt1 = NULL;
					else
					    pt1 = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					
	                
	                fnum = mxGetFieldNumber(ElemData,"T2");
	                NewFieldNumbers[11] = fnum;
					if(fnum<0)
					    pt2 = NULL;
					else
					    pt2 = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					
				
					returnptr = NewFieldNumbers;

				}	break;

			case	USE_LOCAL_COPY:	/* Get fields from MATLAB using field numbers
									    The second argument ponter to the array of field 
									    numbers is previously created with 
										QuadLinPass( ..., MAKE_LOCAL_COPY)
									*/	
				{	A = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[0]));
					B = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[1]));
					max_order = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[2]));
					Nmeth = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[3]));
					le = mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[4]));
                    Lslices = mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[5]));
                    Nslices = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[6]));
                    subNslices = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[7]));
					

					
					/* Optional fields */
					if(FieldNumbers[8]<0)
					    pr1 = NULL;
					else
					    pr1 = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[8]));
					
					if(FieldNumbers[9]<0)
					    pr2 = NULL;
					else
					    pr2 = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[9]));
					
					    
					if(FieldNumbers[10]<0)
					    pt1 = NULL;
					else    
					    pt1 = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[10]));
					    
					if(FieldNumbers[11]<0)
					    pt2 = NULL;
					else 
					    pt2 = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[11]));
					
			
					
					returnptr = FieldNumbers;
				}	break;
			default:
				{	mexErrMsgTxt("No match for calling mode in function IdHardEdgeSliced4Pass\n");
				}
		}


	IdHardEdgeSliced4(r_in, le, Lslices,Nslices,subNslices, A, B, max_order, Nmeth,pt1, pt2, pr1, pr2, num_particles);
	

	
	return(returnptr);

}


 



void mexFunction(	int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{	int m,n;
	double *r_in;
	double *A, *B;  
	int max_order, Nmeth, Nslices,subNslices;
	double  le,*pr1, *pr2, *pt1, *pt2, Lslices;  
    mxArray *tmpmxptr;

    if(nrhs)
    {
    /* ALLOCATE memory for the output array of the same size as the input */
	m = mxGetM(prhs[1]);
	n = mxGetN(prhs[1]);
	if(m!=6) 
		mexErrMsgTxt("Second argument must be a 6 x N matrix");
	
	
	
    tmpmxptr =mxGetField(prhs[0],0,"PolynomA");
	if(tmpmxptr)
		A = mxGetPr(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'PolynomA' was not found in the element data structure"); 
				    
	tmpmxptr =mxGetField(prhs[0],0,"PolynomB");
	if(tmpmxptr)   
		B = mxGetPr(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'PolynomB' was not found in the element data structure");
				
   
	tmpmxptr = mxGetField(prhs[0],0,"MaxOrder");
	if(tmpmxptr)
		max_order = (int)mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'MaxOrder' was not found in the element data structure");
				        
	tmpmxptr = mxGetField(prhs[0],0,"Nmeth");
	if(tmpmxptr)   
		Nmeth = (int)mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'Nmeth' was not found in the element data structure");    
				    
	tmpmxptr = mxGetField(prhs[0],0,"Length");
	if(tmpmxptr)
	    le = mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'Length' was not found in the element data structure");    
					    
	tmpmxptr = mxGetField(prhs[0],0,"Lslices");
	if(tmpmxptr)
		Lslices = mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'Lslices' was not found in the element data structure"); 
				
				
	tmpmxptr = mxGetField(prhs[0],0,"Nslices");
	if(tmpmxptr)   
		Nslices = (int)mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'Nslices' was not found in the element data structure");   

    
    tmpmxptr = mxGetField(prhs[0],0,"subNslices");
	if(tmpmxptr)   
		subNslices = (int)mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'subNslices' was not found in the element data structure");  
	    
	    tmpmxptr = mxGetField(prhs[0],0,"R1");
	    if(tmpmxptr)
	        pr1 = mxGetPr(tmpmxptr);
	    else
	        pr1=NULL; 
	    
	    tmpmxptr = mxGetField(prhs[0],0,"R2");
	    if(tmpmxptr)
	        pr2 = mxGetPr(tmpmxptr);
	    else
	        pr2=NULL; 
	    
	    
	    tmpmxptr = mxGetField(prhs[0],0,"T1");
	    
	    
	    if(tmpmxptr)
	        pt1=mxGetPr(tmpmxptr);
	    else
	        pt1=NULL;
	    
	    tmpmxptr = mxGetField(prhs[0],0,"T2");
	    if(tmpmxptr)
	        pt2=mxGetPr(tmpmxptr);
	    else
	        pt2=NULL;  
		
		
    plhs[0] = mxDuplicateArray(prhs[1]);
	r_in = mxGetPr(plhs[0]);
    IdHardEdgeSliced4(r_in, le, Lslices,Nslices,subNslices, A, B, max_order, Nmeth,pt1, pt2, pr1, pr2, n);
	}
	else
	{   /* return list of required fields */
	    plhs[0] = mxCreateCellMatrix(8,1);
	    
	    mxSetCell(plhs[0],0,mxCreateString("Length"));
	    mxSetCell(plhs[0],1,mxCreateString("Lslices"));
	    mxSetCell(plhs[0],2,mxCreateString("Nslices"));
        mxSetCell(plhs[0],3,mxCreateString("subNslices"));
        mxSetCell(plhs[0],4,mxCreateString("PolynomA"));
	    mxSetCell(plhs[0],5,mxCreateString("PolynomB"));
	    mxSetCell(plhs[0],6,mxCreateString("MaxOrder"));
	    mxSetCell(plhs[0],7,mxCreateString("Nmeth"));	 	    
	    
	    if(nlhs>1) /* Required and optional fields */ 
	    {   plhs[1] = mxCreateCellMatrix(4,1);
	        mxSetCell(plhs[1],0,mxCreateString("T1"));
	        mxSetCell(plhs[1],1,mxCreateString("T2"));
	        mxSetCell(plhs[1],2,mxCreateString("R1"));
	        mxSetCell(plhs[1],3,mxCreateString("R2"));
	    }
	}



}



