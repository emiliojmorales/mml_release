/*
 *----------------------------------------------------------------------------
 * Modification Log:
 * -----------------
 * .01  2008-01-12      Z.Martí: Implementing a sliced "soft edge" Id model 
 *
 *                         max_order
 *                          ----
 *                          \                                      n
 *	   (B + iB  )/ B rho  =  >   (ia  + b +(ialp + bet )*z) (x + iy)
 *       y    x             /       n    n      n     n  
 *	                        ----
 *                         n=0    
 *
 * The potental vector (and Eta, its integrated (in x or y) derivates with z):
 *
 *
 *                           _                                         _
 *                          | max_order                                 |   
 *                          |   ----                                    |
 *                          |   \                      n+1              |
 *                  A = -Re |    >    (ia + b ) (x + iy)/(n+1)          |
 *                   z      |   /       n    n                          |  
 *	                        |  ----                                     | 
 *                          |_ n=0                                     _| 
 *
 *
 *  A* =(A - iA  )/ B rho 
 *        x    y
 *
 *  A*=G+H*      
 *
 * Where
 *
 *                         max_order
 *         _                 ----
 *        /                  \                                     n
 *     G=/ B dz/ B rho =      >   (ialp + bet )*(z(z-L)/2) (x + iy)
 *     _/                    /         n     n    
 *	                        ----
 *                         n=0    
 *
 * and:
 *
 *                           _                                             _
 *                          | max_order                                     |   
 *         _                |   ----                                        |
 *        /                 |   \                          n+2              |
 * Im[H]=/ B dx/ B rho = Im |    >    (ialp + bet ) (x + iy)/(2(n+1)(n+2))  |
 *     _/   z               |   /          n     n                          |  
 *	                        |  ----                                         | 
 *                          |_ n=0                                         _| 
 * 
 *  Traking based on: "Explicit symplectic integrator for s-dependent static  magnetic field", Y.K.Wu           
 *----------------------------------------------------------------------------
 *  Accelerator Physics Group, CELLS-ALBA Spain  
 */


#include "mex.h"
#include<stdio.h>
#include<math.h>
#include "elempass.h"
#include "atlalib.c"
#include "atphyslib.c"

#define second 2
#define fourth 4


#define SQR(X) ((X)*(X))


/******************************************************************************/
/* PHYSICS SECTION ************************************************************/

void B_function(double *r3,double* A, double* B,int max_order,double *B_real,double *B_imag)
{
        
    int i;
	double ReSumXi = B[max_order];
 	double ImSumXi = A[max_order];
    double ReSumXiTemp;
    
    for(i=max_order-1;i>=0;i--)
    { 
        ReSumXiTemp = ReSumXi*r3[0] - ImSumXi*r3[1] +B[i];
        ImSumXi = ImSumXi*r3[0] +  ReSumXi*r3[1] +A[i];  
        ReSumXi = ReSumXiTemp;  
    }
  *B_real=-ReSumXi;  /*the minus is for electron machines*/
  *B_imag=-ImSumXi;
}

void G_function(double *r3, double* alpha, double* beta, double L,int max_order,double *G_real,double *G_imag)
{
        
    int i;
	double ReSumXi = beta[max_order];
 	double ImSumXi = alpha[max_order];
    double ReSumXiTemp;
    
    for(i=max_order-1;i>=0;i--)
    { 
        ReSumXiTemp = ReSumXi*r3[0] - ImSumXi*r3[1] +beta[i];
        ImSumXi = ImSumXi*r3[0] +  ReSumXi*r3[1] +alpha[i];  
        ReSumXi = ReSumXiTemp;  
    }
  *G_real=-ReSumXi*(r3[2]*r3[2]-r3[2]*L)/2.0e0;/*the minus is for electron machines*/
  *G_imag=-ImSumXi*(r3[2]*r3[2]-r3[2]*L)/2.0e0;
}

void G2_function(double *r3,double* A, double* B, double* alpha, double* beta, double L,int max_order,double *G_real,double *G_imag)
{/* used insted of G when the gauge is Az=0*/
        
    int i;
	double ReSumXi = B[max_order]+beta[max_order]*(r3[2]-L)/2.0e0;
 	double ImSumXi = A[max_order]+alpha[max_order]*(r3[2]-L)/2.0e0;
    double ReSumXiTemp;
    
    for(i=max_order-1;i>=0;i--)
    { 
        ReSumXiTemp = ReSumXi*r3[0] - ImSumXi*r3[1] +B[i]+beta[i]*(r3[2]-L)/2.0e0;
        ImSumXi = ImSumXi*r3[0] +  ReSumXi*r3[1] +A[i]+alpha[i]*(r3[2]-L)/2.0e0;  
        ReSumXi = ReSumXiTemp;  
    }
  *G_real=-ReSumXi*r3[2];     /*the minus is for electron machines*/
  *G_imag=-ImSumXi*r3[2];
}

void H_function(double *r3, double* alpha, double* beta,int max_order,double *H_real,double *H_imag)
{
        
    int i;
	double ReSumXi = alpha[max_order]*r3[1]*r3[0]/((max_order+1.0e0)*(max_order+2.0e0))-beta[max_order]*(r3[0]*r3[0]-r3[1]*r3[1])/(2.0e0*(max_order+1.0e0)*(max_order+2.0e0));
 	double ImSumXi = -beta[max_order]*r3[1]*r3[0]/((max_order+1.0e0)*(max_order+2.0e0))-alpha[max_order]*(r3[0]*r3[0]-r3[1]*r3[1])/(2.0e0*(max_order+1.0e0)*(max_order+2.0e0));
    double ReSumXiTemp;
    
    for(i=max_order-1;i>=0;i--)
    { 
        ReSumXiTemp = ReSumXi*r3[0] - ImSumXi*r3[1] +alpha[i]*r3[1]*r3[0]/((i+1.0e0)*(i+2.0e0))-beta[i]*(r3[0]*r3[0]-r3[1]*r3[1])/(2.0e0*(i+1.0e0)*(i+2.0e0));
        ImSumXi = ImSumXi*r3[0] +  ReSumXi*r3[1] -beta[i]*r3[1]*r3[0]/((i+1.0e0)*(i+2.0e0))-alpha[i]*(r3[0]*r3[0]-r3[1]*r3[1])/(2.0e0*(i+1.0e0)*(i+2.0e0));  
        ReSumXi = ReSumXiTemp;  
    }
  *H_real=-ReSumXi;/*the minus is for electron machines*/
  *H_imag=-ImSumXi;
}


void SliceIdPass_2nd(double* r8, double* A, double* B, double* alpha, double* beta, double L, int max_order)
{
    int i;
    double *r8_0,*r3,dld,dl2,dl2d,x,y,z,px,py,x0,y0,z0;
    double ReG_1,ReG_2,ReG_3,ReG_4,ReH_1,ReH_2,ReH_3,ReH_4;
    double ImG_1,ImG_2,ImG_3,ImG_4,ImH_1,ImH_2,ImH_3,ImH_4;
    double BX_1,BY_1,BX_L2,BY_L2,BX_4,BY_4;
    
            
            

    r8_0=(double*)mxCalloc(8,sizeof(double));
    r3=(double*)mxCalloc(3,sizeof(double));
    
    for(i=0;i<=7;i++) r8_0[i]=r8[i];
  
    dld  = L/(1.0e0 + r8[4]);
    dl2  = 0.5e0 * L;
    dl2d = dl2/(1.0e0 + r8[4]);

    x=r8[0];
    y=r8[2];        
    z=r8[6];
    px=r8[1];
    py=r8[3];        
    x0=0.0e0;
    y0=0.0e0;        
    z0=0.0e0;
  
    /* Gauge Az=0*/
    
    /*Auxiliary steps
    
    r3[0]=x;r3[1]=y;r3[2]=dl2+z;
    G2_function(r3,A,B,alpha,beta,L,max_order, &ReG_1,&ImG_1);
    H_function(r3,alpha,beta,max_order, &ReH_1,&ImH_1);

    
        
    r3[0]=x;r3[1]=y+dl2d*(py-BX_1+ImG_1-ImH_1);r3[2]=dl2+z;
    G2_function(r3,A,B,alpha,beta,L,max_order, &ReG_2,&ImG_2);
    H_function(r3,alpha,beta,max_order, &ReH_2,&ImH_2); 
    
    r3[0]=x+dld*(px+BY_1*dl2+ReG_1-2*ReG_2-ReH_1);r3[1]=y+dl2d*(py-BX_1*dl2+ImG_1-ImH_1);r3[2]=dl2+z;
    G2_function(r3,A,B,alpha,beta,L,max_order, &ReG_3,&ImG_3);
    H_function(r3,alpha,beta,max_order, &ReH_3,&ImH_3);

        
    r3[0]=x+dld*(px+BY_1*dl2+ReG_1-2*ReG_2-ReH_1);r3[1]=y+dld*(-BX_1*dl2+(py+ImG_1-ImH_1+ImH_2-ImH_3));r3[2]=dl2+z;
    G2_function(r3,A,B,alpha,beta,L,max_order, &ReG_4,&ImG_4);
    H_function(r3,alpha,beta,max_order, &ReH_4,&ImH_4);

    
       
    /* Multipole aproach Gauge d(Az)/dz=0*/
    /*Auxiliary steps */
     
    r3[0]=x;r3[1]=y;r3[2]=dl2+z;
    G_function(r3,alpha,beta,L,max_order, &ReG_1,&ImG_1);
    H_function(r3,alpha,beta,max_order, &ReH_1,&ImH_1);
    B_function(r3,A,B,max_order, &BY_1,&BX_1);
    BX_L2=L*L*BX_1;
    BY_L2=L*L*BY_1;
    
        
    r3[0]=x;r3[1]=y+dl2d*(py-BX_1+ImG_1-ImH_1);r3[2]=dl2+z;
    G_function(r3,alpha,beta,L,max_order, &ReG_2,&ImG_2);
    H_function(r3,alpha,beta,max_order, &ReH_2,&ImH_2); 
    
    r3[0]=x+dld*(px+BY_1*dl2+ReG_1-2*ReG_2-ReH_1);r3[1]=y+dl2d*(py-BX_1*dl2+ImG_1-ImH_1);r3[2]=dl2+z;
    G_function(r3,alpha,beta,L,max_order, &ReG_3,&ImG_3);
    H_function(r3,alpha,beta,max_order, &ReH_3,&ImH_3);

        
    r3[0]=x+dld*(px+BY_1*dl2+ReG_1-2*ReG_2-ReH_1);r3[1]=y+dld*(-BX_1*dl2+(py+ImG_1-ImH_1+ImH_2-ImH_3));r3[2]=dl2+z;
    G_function(r3,alpha,beta,L,max_order, &ReG_4,&ImG_4);
    H_function(r3,alpha,beta,max_order, &ReH_4,&ImH_4);
    B_function(r3,A,B,max_order, &BY_4,&BX_4);


    /*Appling kicks and drifts*/


    r8[0]+=dld*(px+BY_1*dl2+ReG_1-2*ReG_2-ReH_1);
    r8[1]+=BY_1*dl2+dl2*BY_4+ReG_1-2*ReG_2+2*ReG_3-ReG_4-ReH_1+ReH_4;  
    r8[2]+=dld*(py-BX_1*dl2+ImG_1-ImH_1+ImH_2-ImH_3);
    r8[3]+=-BX_1*dl2-dl2*BX_4+ImG_1-ImG_4-ImH_1+2*ImH_2-2*ImH_3+ImH_4;
    r8[5]+=dl2d*(pow(py-BX_1*dl2+ImG_1-ImH_1,2)+pow(py-BX_1*dl2+ImG_1-ImH_1+2*ImH_2-2*ImH_3,2)+2*pow(px+BY_1*dl2+ReG_1-2*ReG_2-ReH_1,2))/(2.0e0*(1+r8[4]));
    r8[6]+=L;
    
        
    /* Free memory*/
    
    mxFree(r8_0);
    mxFree(r3);
}


void SliceIdPass_4th(double* r8, double* A, double* B, double* alpha, double* beta, double L, int max_order)
{
  double dl1, dl0;
  const double t1=1.3512071919596576340476878089715e0;
  const double t0=-1.7024143839193152680953756179429e0;
  
  dl1 = t1*L;
  dl0 = t0*L;
  
  SliceIdPass_2nd(r8,A,B,alpha,beta,dl1,max_order);
  /*mexPrintf("%e %e %e %e %e %e\n",r8[0],r8[1],r8[2],r8[3],r8[4],r8[5]);*/
  SliceIdPass_2nd(r8,A,B,alpha,beta,dl0,max_order);
  /*mexPrintf("%f %f %f %f %f %f %f %f\n",r8[0],r8[1],r8[2],r8[3],r8[4],r8[5],r8[6],r8[7]);*/
  SliceIdPass_2nd(r8,A,B,alpha,beta,dl1,max_order);
}

void SliceIdPass(double* r, double* A, double* B, double* alpha, double* beta, double L, int subNslices, int max_order, int Nmeth)
{	
    int i;
    double *r8,subL=L/(1.0e0*subNslices);
    
    /*mexPrintf("%e %e %e %e %e %e %e\n",B[0],r[0],r[1],r[2],r[3],r[4],r[5]);
    /*Reshape phase space to include z and Pz*/
    r8=(double*)mxCalloc(8,sizeof(double));
    for(i=0;i<=5;i++)
    {
        r8[i]=r[i];
    }     
    r8[6]=0.0;    /*Start from the beginning of the slice: CAREFULL!! The multipoles are calculated in the centre of the slice! */
    r8[7]=0.0;
    
    /*Choose integrator*/
    switch (Nmeth) 
    {
        case second :
            for(i=1;i<=subNslices;i++) SliceIdPass_2nd(r8,A,B,alpha,beta,subL,max_order);
            break;
        case fourth:
            for(i=1;i<=subNslices;i++) SliceIdPass_4th(r8,A,B,alpha,beta,subL,max_order);
            break;
        default:
            printf("Invalid method ...\n");
            break;
    }
    for(i=0;i<=5;i++)
    {
        r[i]=r8[i];
    }     
    mxFree(r8);
}



void IdSoftEdgeSliced4(double *r, double le, double Lslices, int Nslices,int subNslices, 
                    double *A, double *B, double* alpha, double* beta,
					int max_order,int Nmeth,double *T1, double *T2,	
					double *R1, double *R2, int num_particles)
{	int j,c,m;
	double norm, NormL1, NormL2;	
	double *r6,*A_array,*B_array,*alpha_array,*beta_array;
	bool useT1, useT2, useR1, useR2;
    
    FILE *arxiu;
    
    arxiu=fopen("trackInside.dat","w");
    
	if(T1==NULL)
	    useT1=false;
	else 
	    useT1=true;  
	    
    if(T2==NULL)
	    useT2=false; 
	else 
	    useT2=true;  
	
	if(R1==NULL)
	    useR1=false; 
	else 
	    useR1=true;  
	    
    if(R2==NULL)
	    useR2=false;
	else 
	    useR2=true;
    
    for(c = 0;c<num_particles;c++)	/*Loop over particles  */
			
    {	
        r6 = r+c*6;	
        if(!mxIsNaN(r6[0]))
        {   
/*  misalignment at entrance  */	
            if(useT1)   
                ATaddvv(r6,T1); 
            if(useR1)      
                ATmultmv(r6,R1);
/*  integrator  */
            for(j=0; j < Nslices; j++)
            {
                A_array=A+j*(max_order+1);
                B_array=B+j*(max_order+1);
                alpha_array=alpha+j*(max_order+1);
                beta_array=beta+j*(max_order+1);
                fprintf(arxiu,"%e %e %e %e %e %e %e \n",B_array[0],r6[0],r6[1],r6[2],r6[3],r6[4],r6[5]);
                SliceIdPass(r6,A_array,B_array,alpha_array,beta_array,Lslices,subNslices,max_order,Nmeth);
            }		
/* Misalignment at exit */	      
            if(useR2)     
                ATmultmv(r6,R2);   
            if(useT2)           
                ATaddvv(r6,T2);	    
        }	
    }
    
    fclose(arxiu);
}




/********** END PHYSICS SECTION ***********************************************/
/******************************************************************************/



ExportMode int* passFunction(const mxArray *ElemData, int *FieldNumbers,
								double *r_in, int num_particles, int mode)

#define NUM_FIELDS_2_REMEMBER 14


{	double *A , *B, *alpha , *beta;
	double  *pr1, *pr2, *pt1, *pt2;   

	int max_order, Nmeth, Nslices, subNslices;
	double le,Lslices;
	int *returnptr;
	int *NewFieldNumbers, fnum;

	
	switch(mode)
		{   case MAKE_LOCAL_COPY: 	/* Find field numbers first
										Save a list of field number in an array
										and make returnptr point to that array
									*/
				{	
					/* Allocate memory for integer array of 
					    field numbers for faster future reference
					*/
		
					NewFieldNumbers = (int*)mxCalloc(NUM_FIELDS_2_REMEMBER,sizeof(int));

					/* Populate */
					
					
					
					fnum = mxGetFieldNumber(ElemData,"PolynomA");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'PolynomA' was not found in the element data structure"); 
					NewFieldNumbers[0] = fnum;
					A = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					
					
					fnum = mxGetFieldNumber(ElemData,"PolynomB");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'PolynomB' was not found in the element data structure"); 
					NewFieldNumbers[1] = fnum;
					B = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
                    
					fnum = mxGetFieldNumber(ElemData,"PolynomAlpha");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'PolynomAlpha' was not found in the element data structure"); 
					NewFieldNumbers[2] = fnum;
					alpha = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					
					
					fnum = mxGetFieldNumber(ElemData,"PolynomBeta");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'PolynomBeta' was not found in the element data structure"); 
					NewFieldNumbers[3] = fnum;
					beta = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));					
					
					
					fnum = mxGetFieldNumber(ElemData,"MaxOrder");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'MaxOrder' was not found in the element data structure"); 
					NewFieldNumbers[4] = fnum;
					max_order = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));
					
					fnum = mxGetFieldNumber(ElemData,"Nmeth");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'Nmeth' was not found in the element data structure"); 
					NewFieldNumbers[5] = fnum;
					Nmeth = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));
					
					
					fnum = mxGetFieldNumber(ElemData,"Length");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'Length' was not found in the element data structure"); 
					NewFieldNumbers[6] = fnum;
					le = mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));
					
					fnum = mxGetFieldNumber(ElemData,"Lslices");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'Lslices' was not found in the element data structure"); 
					NewFieldNumbers[7] = fnum;
					Lslices = mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));
                    
					fnum = mxGetFieldNumber(ElemData,"Nslices");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'Nslices' was not found in the element data structure"); 
					NewFieldNumbers[8] = fnum;
					Nslices = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));
					fnum = mxGetFieldNumber(ElemData,"subNslices");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'subNslices' was not found in the element data structure"); 
					NewFieldNumbers[9] = fnum;
					subNslices = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,fnum));                    
                    
                    

                    
				
                    fnum = mxGetFieldNumber(ElemData,"R1");
					NewFieldNumbers[10] = fnum;
					if(fnum<0)
					    pr1 = NULL;
					else
					    pr1 = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					

					fnum = mxGetFieldNumber(ElemData,"R2");
					NewFieldNumbers[11] = fnum;
					if(fnum<0)
					    pr2 = NULL;
					else
					    pr2 = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					
					
                    fnum = mxGetFieldNumber(ElemData,"T1");
	                NewFieldNumbers[12] = fnum;
					if(fnum<0)
					    pt1 = NULL;
					else
					    pt1 = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					
	                
	                fnum = mxGetFieldNumber(ElemData,"T2");
	                NewFieldNumbers[13] = fnum;
					if(fnum<0)
					    pt2 = NULL;
					else
					    pt2 = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					
				
					returnptr = NewFieldNumbers;

				}	break;

			case	USE_LOCAL_COPY:	/* Get fields from MATLAB using field numbers
									    The second argument ponter to the array of field 
									    numbers is previously created with 
										QuadLinPass( ..., MAKE_LOCAL_COPY)
									*/	
				{	A = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[0]));
					B = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[1]));
                    alpha = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[2]));
					beta = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[3]));
					max_order = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[4]));
					Nmeth = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[5]));
					le = mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[6]));
                    Lslices = mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[7]));
                    Nslices = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[8]));
                    subNslices = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[9]));
					

					
					/* Optional fields */
					if(FieldNumbers[10]<0)
					    pr1 = NULL;
					else
					    pr1 = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[10]));
					
					if(FieldNumbers[11]<0)
					    pr2 = NULL;
					else
					    pr2 = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[11]));
					
					    
					if(FieldNumbers[12]<0)
					    pt1 = NULL;
					else    
					    pt1 = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[12]));
					    
					if(FieldNumbers[13]<0)
					    pt2 = NULL;
					else 
					    pt2 = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[13]));
					
			
					
					returnptr = FieldNumbers;
				}	break;
			default:
				{	mexErrMsgTxt("No match for calling mode in function IdHardEdgeSliced4Pass\n");
				}
		}


	IdSoftEdgeSliced4(r_in, le, Lslices,Nslices,subNslices, A, B,alpha,beta, max_order, Nmeth,pt1, pt2, pr1, pr2, num_particles);
	

	
	return(returnptr);

}


 



void mexFunction(	int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{	int m,n;
	double *r_in;
	double *A, *B, *alpha , *beta;  
	int max_order, Nmeth, Nslices,subNslices;
	double  le,*pr1, *pr2, *pt1, *pt2, Lslices;  
    mxArray *tmpmxptr;

    if(nrhs)
    {
    /* ALLOCATE memory for the output array of the same size as the input */
	m = mxGetM(prhs[1]);
	n = mxGetN(prhs[1]);
	if(m!=6) 
		mexErrMsgTxt("Second argument must be a 6 x N matrix");
	
	
	
    tmpmxptr =mxGetField(prhs[0],0,"PolynomA");
	if(tmpmxptr)
		A = mxGetPr(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'PolynomA' was not found in the element data structure"); 
				    
	tmpmxptr =mxGetField(prhs[0],0,"PolynomB");
	if(tmpmxptr)   
		B = mxGetPr(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'PolynomB' was not found in the element data structure");
		
        tmpmxptr =mxGetField(prhs[0],0,"PolynomAlpha");
	if(tmpmxptr)
		alpha = mxGetPr(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'PolynomAlpha' was not found in the element data structure"); 
				    
	tmpmxptr =mxGetField(prhs[0],0,"PolynomBeta");
	if(tmpmxptr)   
		beta = mxGetPr(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'PolynomBeta' was not found in the element data structure");
				
   
	tmpmxptr = mxGetField(prhs[0],0,"MaxOrder");
	if(tmpmxptr)
		max_order = (int)mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'MaxOrder' was not found in the element data structure");
				        
	tmpmxptr = mxGetField(prhs[0],0,"Nmeth");
	if(tmpmxptr)   
		Nmeth = (int)mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'Nmeth' was not found in the element data structure");    
				    
	tmpmxptr = mxGetField(prhs[0],0,"Length");
	if(tmpmxptr)
	    le = mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'Length' was not found in the element data structure");    
					    
	tmpmxptr = mxGetField(prhs[0],0,"Lslices");
	if(tmpmxptr)
		Lslices = mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'Lslices' was not found in the element data structure"); 
				
				
	tmpmxptr = mxGetField(prhs[0],0,"Nslices");
	if(tmpmxptr)   
		Nslices = (int)mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'Nslices' was not found in the element data structure");   

    
    tmpmxptr = mxGetField(prhs[0],0,"subNslices");
	if(tmpmxptr)   
		subNslices = (int)mxGetScalar(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'subNslices' was not found in the element data structure");  
	    
	    tmpmxptr = mxGetField(prhs[0],0,"R1");
	    if(tmpmxptr)
	        pr1 = mxGetPr(tmpmxptr);
	    else
	        pr1=NULL; 
	    
	    tmpmxptr = mxGetField(prhs[0],0,"R2");
	    if(tmpmxptr)
	        pr2 = mxGetPr(tmpmxptr);
	    else
	        pr2=NULL; 
	    
	    
	    tmpmxptr = mxGetField(prhs[0],0,"T1");
	    
	    
	    if(tmpmxptr)
	        pt1=mxGetPr(tmpmxptr);
	    else
	        pt1=NULL;
	    
	    tmpmxptr = mxGetField(prhs[0],0,"T2");
	    if(tmpmxptr)
	        pt2=mxGetPr(tmpmxptr);
	    else
	        pt2=NULL;  
		
		
    plhs[0] = mxDuplicateArray(prhs[1]);
	r_in = mxGetPr(plhs[0]);
    IdSoftEdgeSliced4(r_in, le, Lslices,Nslices,subNslices, A, B,alpha,beta, max_order, Nmeth,pt1, pt2, pr1, pr2, n);
	}
	else
	{   /* return list of required fields */
	    plhs[0] = mxCreateCellMatrix(10,1);
	    
	    mxSetCell(plhs[0],0,mxCreateString("Length"));
	    mxSetCell(plhs[0],1,mxCreateString("Lslices"));
	    mxSetCell(plhs[0],2,mxCreateString("Nslices"));
        mxSetCell(plhs[0],3,mxCreateString("subNslices"));
        mxSetCell(plhs[0],4,mxCreateString("PolynomA"));
	    mxSetCell(plhs[0],5,mxCreateString("PolynomB"));
        mxSetCell(plhs[0],6,mxCreateString("PolynomAlpha"));
	    mxSetCell(plhs[0],7,mxCreateString("PolynomBeta"));
	    mxSetCell(plhs[0],8,mxCreateString("MaxOrder"));
	    mxSetCell(plhs[0],9,mxCreateString("Nmeth"));	 	    
	    
	    if(nlhs>1) /* Required and optional fields */ 
	    {   plhs[1] = mxCreateCellMatrix(4,1);
	        mxSetCell(plhs[1],0,mxCreateString("T1"));
	        mxSetCell(plhs[1],1,mxCreateString("T2"));
	        mxSetCell(plhs[1],2,mxCreateString("R1"));
	        mxSetCell(plhs[1],3,mxCreateString("R2"));
	    }
	}



}



