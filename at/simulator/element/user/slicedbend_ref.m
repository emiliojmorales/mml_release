function z=slicedbend_ref(fname,filename,Ldip,Nbends,sextindipole,part,method)
% 
%
% z=slicedbend_ref(fname,filename,Ldip,Nbends,sextindipole,part,method)
%	creates a new family in the FAMLIST
%  
%   INPUTS:
%		FamName             family name
%		filename         	file with the name of the magnetic field along
%                           the reference dipole (avaraged) 
%		Ldip            	Length of the diople in the model
%		Nbends          	number of bendings in the model
%       sextindipole        >0 if de multipolar components are to be included
%       part                can be 'all', 'half_left' or 'half_right' 
%		PassMethod          shold be BndMPole_sliced_Symplectic4Pass or
%                           BndMPole_sliced_Symplectic4RadPass

%  OUTPUTS:
%   z                   returns assigned address in the FAMLIST that is uniquely identifies
%                       the family
global GLOBVAL;
Energy=(GLOBVAL.E0)/1e9;

melectron = 9.10938188*10^-31;
clight = 2.99792458*10^8;
qelectron =-1.602176462*10^-19;
mc2 =abs(melectron*(clight^2/qelectron)/10^9);
QM = qelectron/(Energy/mc2*melectron*clight);
Br = 1/QM;


F0=load(filename);
NF0=length(F0(:,1));

if NF0<50  % few slices, in this case usually values are integrals
    j1=2;
    j2=4;
    F0p=F0(:,2:5);
    ll=0;
    for jj=j1:j2
        F0p(:,jj)=F0p(:,jj)./F0p(:,1)/Br/factorial(ll);
        ll=ll+1;
    end
else         %many slices, in this case usually values are fields
    j1=5;
    j2=8;
    F0p=F0(1:(NF0-1),:);
    F0p(:,1)=F0(2:NF0,1)-F0(1:(NF0-1),1);
    ll=0;
    for jj=j1:j2
        F0p(:,jj)=(F0(2:NF0,jj)+F0(1:(NF0-1),jj))/2/Br/factorial(ll);
        ll=ll+1;
    end
end
F0=F0p;
NF0=length(F0(:,1));

% concentrating final slices to keep the field out of the quads

Ltot=sum(F0(:,1));
DLside=(Ltot-Ldip)/2;
leng=0;
for i=1:NF0
    leng=leng+F0(i,1);
    if leng>DLside
        i1=i;
        break;
    end
end
leng=0;
for i=1:NF0
    leng=leng+F0(NF0-(i-1),1);
    if leng>DLside
        i2=NF0-(i-1);
        break;
    end
end
F=F0(i1:i2,:);
NF=length(F(:,1));

F(1,1)=sum(F0(1:i1,1))-DLside;
F(1,j1:j2)=sum(F0(1:i1,j1:j2).*(F0(1:i1,1)*ones(1,j2-j1+1)),1)/F(1,1);
F(NF,1)=sum(F0(i2:NF0,1))-DLside;
F(NF,j1:j2)=sum(F0(i2:NF0,j1:j2).*(F0(i2:NF0,1)*ones(1,j2-j1+1)),1)/F(NF,1);



if strcmp('all',part)||strcmp('All',part)||strcmp('ALL',part)||strcmp('aLL',part)
    iin=1;
    ifi=NF;
elseif strcmp('half_left',part)||strcmp('Half_left',part)||strcmp('Half_Left',part)||strcmp('half left',part)||strcmp('Half left',part)||strcmp('Half Left',part)
    iin=1;
    [mi Imi]=min(abs(cumsum(F(:,1))-sum(F(:,1))/2));
    ifi=Imi;
elseif strcmp('half_right',part)||strcmp('Half_right',part)||strcmp('Half_Right',part)||strcmp('half right',part)||strcmp('Half right',part)||strcmp('Half Right',part)
    
    [mi Imi]=min(abs(cumsum(F(:,1))-sum(F(:,1))/2));
    iin=Imi+1;
    ifi=NF;
end
NFT=ifi-iin+1;

% Translate to multipoles 
Le=F(iin:ifi,1);
MaxOrder=j2-j1;
PolynomB=[zeros(NFT,1) F(iin:ifi,(j1+1)) zeros(NFT,MaxOrder-1)];
if sextindipole==1
    PolynomB=[zeros(NFT,1) F(iin:ifi,(j1+1):j2)];
end
PolynomA=zeros(NFT,MaxOrder+1);
A=2*pi/Nbends*F(iin:ifi,j1).*F(iin:ifi,1)/sum(F(1:NF,j1).*F(1:NF,1));
A1=0*A/2;
A2=0*A/2; 
NumIntSteps=1+floor(10*max(Le)/Ldip);


% This fields have a diferent value for each one of the Nslices
ElemData.BendingAngle  	= A;
ElemData.EntranceAngle 	= A1;
ElemData.ExitAngle     	= A2;
ElemData.PolynomA		= PolynomA';	 
ElemData.PolynomB		= PolynomB'; 


% this fields have identical meaning than without slicing
ElemData.FamName = fname;  % add check for identical family names
ElemData.Length			= sum(Le);
ElemData.Lengths			= Le;
ElemData.MaxOrder			= MaxOrder;
ElemData.NumIntSteps 	=NumIntSteps;
ElemData.Nslices  	= NFT;
ElemData.R1 = diag(ones(6,1));
ElemData.R2 = diag(ones(6,1));
ElemData.T1 = zeros(1,6);
ElemData.T2 = zeros(1,6);
ElemData.PassMethod 		= method;

global FAMLIST
z = length(FAMLIST)+1; % number of declare families including this one
FAMLIST{z}.FamName = fname;
FAMLIST{z}.NumKids = 0;
FAMLIST{z}.KidsList= [];
FAMLIST{z}.ElemData= ElemData;

