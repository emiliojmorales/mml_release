function TRACK_Full_Hamiltonian

a25_noout;
updateatindex;

setcavity('off');
setradiation('off');
thintrack=0;
exact=0;
turns=1000;
delta=0.03;
x0=[0.0241 0.020 0.015];
y0=0*[0.01 0.005 0.001];
r0=[[x0(1) 0.0 y0(1) 0.0 0.03 0.0]' [x0(2) 0.0 y0(2) 0.0 0.03 0.0]' [x0(3) 0.0 y0(3) 0.0 0.03 0.0]'];
r1=[[x0(1) 0.0 y0(1) 0.0 0.03 0.0]' [x0(2) 0.0 y0(2) 0.0 0.03 0.0]' [x0(3) 0.0 y0(3) 0.0 0.03 0.0]'];
r2=[[x0(1) 0.0 y0(1) 0.0 0.03 0.0]' [x0(2) 0.0 y0(2) 0.0 0.03 0.0]' [x0(3) 0.0 y0(3) 0.0 0.03 0.0]'];
ringpass_nolimit(THERING,r0,1);
[T0, loss]=ringpass_nolimit(THERING,r0,turns,'reuse');
cr=modelchro;
SetHamiltonianAndFringe('full');
cr1=modelchro;
ringpass_nolimit(THERING,r1,1);
[T1, loss]=ringpass_nolimit(THERING,r1,turns,'reuse');
SetHamiltonianAndFringe('fringe');
cr2=modelchro;
ringpass_nolimit(THERING,r2,1);
[T2, loss]=ringpass_nolimit(THERING,r2,turns,'reuse');
f_u=1e3;
figure;
plot(f_u*T0(1,:),f_u*T0(2,:),'r.',f_u*T1(1,:),f_u*T1(2,:),'b.',f_u*T2(1,:),f_u*T2(2,:),'g.');
xlabel('x(mm)');
ylabel('p_x (mrad)');
legend(sprintf('Approx H, \\xi_{x}=%1.3f, \\xi_{y}=%1.3f',cr(1),cr(2)),sprintf('Full H, \\xi_{x}=%1.3f, \\xi_{y}=%1.3f',cr1(1),cr1(2)),sprintf('Full H + Fringe, \\xi_{x}=%1.3f, \\xi_{y}=%1.3f',cr2(1),cr2(2)),'Location','Best')
title(sprintf('track at x=%1.3f mm y=%1.3f mm \\delta = %1.1f %%',f_u*x0(1),f_u*y0(1),100*delta));

figure;
plot(f_u*T0(3,:),f_u*T0(4,:),'r.',f_u*T1(3,:),f_u*T1(4,:),'b.',f_u*T2(3,:),f_u*T2(4,:),'g.');
xlabel('y(mm)');
ylabel('p_y (mrad)');
legend(sprintf('Approx H, \\xi_{x}=%1.3f, \\xi_{y}=%1.3f',cr(1),cr(2)),sprintf('Full H, \\xi_{x}=%1.3f, \\xi_{y}=%1.3f',cr1(1),cr1(2)),sprintf('Full H + Fringe, \\xi_{x}=%1.3f, \\xi_{y}=%1.3f',cr2(1),cr2(2)),'Location','Best')
title(sprintf('track at x=%1.3f mm y=%1.3f mm \\delta = %1.1f %%',f_u*x0(1),f_u*y0(1),100*delta));

end