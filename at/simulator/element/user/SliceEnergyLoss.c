
#include <stdio.h>
#include "nrutil.h"
#include "nrutil.c"
#include "ran1.c"
#include "bessik.c"
#include "qromo.c"
#include "IntBess.c"

#define pi 3.1415926535897931159979634685


int factorial (int num)
{
 if (num==1||num<=0) return 1;
 return factorial(num-1)*num; 
}

void c_poi(double lb, int *k,int npoi, double *p) {
    double pant;
    int ii;
    for(ii=1;ii<=npoi;ii++)
    {
        p[ii]=exp(-lb)*pow(lb,(k[ii]))/factorial(k[ii]);
    }
    pant=0;
    for(ii=1;ii<=npoi;ii++)
    {
        p[ii]+=pant;
        pant=p[ii];
    }
}     
  
double K_5_3(double x)
{
    double xnu, ri, rk, rip,rkp;
    xnu=5.0/3.0;
    bessik(x, xnu, &ri, &rk, &rip, &rkp);
    return rk;
}
void calcdN(double C, double Ec,  double *E, double *dE, double *dN, int N) {
    double infi, ans, *df, *x, *dx;
    int ii;
    /*FILE *fid;
    fid = fopen("bessik.dat", "w");
    infi=1; 
    for (ii=2;ii<N;ii++) {
        fprintf(fid,"%lf %lf\n",E[ii]/Ec,K_5_3(E[ii]/Ec));
        ans=qromo(K_5_3, E[ii]/Ec, infi, midexp);
        dN[ii]=ans*C*dE[ii];
    }
     *fclose(fid);
     */
    df=vector(1,N);
    dx=vector(1,N);
    x=vector(1,N);
    SetVectordN(x,dx,df);
        
    for (ii=1;ii<=N;ii++) {
        dE[ii]=Ec*dx[ii];
        dN[ii]=C*Ec*df[ii];
        E[ii]=Ec*x[ii];
    }
    
    free_vector(df,1,N);
    free_vector(x,1,N);
    free_vector(dx,1,N);
}


double SliceEnergyLoss(double E0, double L, double B2P, long* idum) {
    double Eloss,me, a, c, m, hb, e, Brho, BP, gamma, Ec, C,Emax,*E,*dE,*dN,rand_num,*p,dr,N3;
    int nfot_tot,N,Npoi,jj,ii,kk,nfot,*nfotv,*k;

    /*FILE *fid;
    fid = fopen("rand_radia.dat", "w");
    
    /*  Random Radiation Energy loss in a slice 
     *
     * E0: energy of the electrons in GeV.
     * L: Length of the readiative slice in m.
     * B2P: square of the perpendicular magnetic field in (1/m)^2
     *
     */

    me=0.510998910;
    a=7.2973525376e-3;
    c=299792458.0;
    m=9.10938188e-31;
    hb=6.62606896e-34/2.0/pi;
    e=1.60217646e-19;
    Brho=sqrt(pow(E0,2.0)-pow(me*1e6,2.0))/c;
    BP=Brho*sqrt(B2P);
    gamma=E0/1.0e6/me;
    Ec=3.0*hb*pow(gamma,2.0)*e*BP/2.0/m;
    C=fabs(L)*sqrt(3.0)*a*e*BP/(Ec*2.0*pi*m*c);
    Emax=10.0*Ec;
    N=100;
    N3=pow(1.0*N,3);
    Npoi=7;
    /*printf("%e %e %e %e %e %e %e %e %e\n",L,BP,C,Ec,E0,gamma,Brho,hb,m);*/
    
    
    E=vector(1,N);
    dE=vector(1,N);
    dN=vector(1,N);
    p=vector(1,Npoi);
    k=ivector(1,Npoi);
    nfotv=ivector(1,N);
    Eloss=0.0;
    for (ii=1;ii<=Npoi;ii++) {
        k[ii]=ii-1;
    }
    /*---------------- Main calculation------------------*/
    calcdN(C,Ec,E,dE,dN, N);
    /*---------------------------------------------------*/

   


    /*printf("idum %ld CLOCKS_PER_SEC %ld",idum,CLOCKS_PER_SEC);*/
    nfot_tot=0;
    for (ii=1;ii<=N;ii++) {
        c_poi(dN[ii],k,Npoi,p);
        rand_num=ran1(idum);
        
        for (kk=1;kk<=Npoi;kk++) {
                dr=rand_num-p[kk];
                if (dr<0) break;
            }      
         
        nfot=kk-1; 
        nfotv[ii]=nfot;
        Eloss+=(nfot*1.0)*E[ii];
        nfot_tot+=nfot;
        /*fprintf(fid,"%lf %1.20lf %1.20lf %d %lf\n",rand_num,p[1],p[2],nfot,Eloss);*/
    }
    Eloss/=e*E0;
    if(L<0.0){
        Eloss=-fabs(Eloss);
    }       

    /*printf("%d ",nfot_tot);*/
    free_vector(E,1,N);
    free_vector(dE,1,N);
    free_vector(dN,1,N);
    free_vector(p,1,Npoi);
    free_ivector(k,1,Npoi);
    free_ivector(nfotv,1,Npoi);
    /*fclose(fid);*/
    
    return Eloss;
}

#undef pi
