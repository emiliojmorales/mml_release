#include "mex.h"
#include <math.h>
#include "elempass.h"
#include "atlalib.c"

#define SQR(X) ((X)*(X))

void set2zero(double *r6)
{	int i;
	
    for(i=1;i<6;i++)
		r6[i] = 0;
}

void markaslost(double *r6)
{	int i;
    
    r6[0] = mxGetNaN();
	
    for(i=1;i<6;i++)
		r6[i] =0;
}

void AperturePass(double *r_in, double *limitsptr, int num_particles,double *T1, double *T2)
{   /*  Checks X and Y of each input 6-vector and marks the corresponding element in 
    lossflag array with 0 if X,Y are exceed the limits given by limitsptr array
	limitsptr has 4 elements: (MinX, MaxX, MinY, MaxY) */
	 
	int i, c, c6;
    bool useT1, useT2;
    
    	
    if(T1==NULL)
	    useT1=false;
	else 
	    useT1=true;  
	    
    if(T2==NULL)
	    useT2=false; 
	else 
	    useT2=true;  
    
    
    for(c = 0;c<num_particles;c++)
    {   c6 = c*6;
        if(!mxIsNaN(r_in[c6])) /*  check if this particle is already marked as lost			*/ {
            /*  misalignment at entrance  */
            if(useT1)
                ATaddvv(r_in+c6, T1);
            /* check limits for X position */
            if(r_in[c6+0]<limitsptr[0] || r_in[c6+0]>limitsptr[1] || r_in[c6+2]<limitsptr[2] || r_in[c6+2]>limitsptr[3])
                markaslost(r_in+c6);
            else
                for(i=0;i<6;i++)
                {    if(!mxIsFinite(r_in[c6+i]))
                     {   markaslost(r_in+c6);
                         break;
                     }
                }
            /* Misalignment at exit */
            if(useT2)
                ATaddvv(r_in+c6, T2);
        }
    }
}


ExportMode int* passFunction(const mxArray *ElemData,int *FieldNumbers,
				double *r_in, int num_particles, int mode)


#define NUM_FIELDS_2_REMEMBER 3

{	int *returnptr;
	int *NewFieldNumbers, fnum;

	double * limitsptr, *pt1, *pt2;  

	switch(mode)
		{	case NO_LOCAL_COPY:	/* OBSOLETE ON at 1.3  */
		
				{	
				}	break;	
			
			case MAKE_LOCAL_COPY: 	/* Find field numbers first
									   Save a list of field number in an array
									   and make returnptr point to that array
									*/
				{	
					NewFieldNumbers = (int*)mxCalloc(NUM_FIELDS_2_REMEMBER,sizeof(int));
					fnum = mxGetFieldNumber(ElemData,"Limits");
					if(fnum<0) 
					    mexErrMsgTxt("Required field 'Limits' was not found in the element data structure"); 
					NewFieldNumbers[0] = fnum;
					limitsptr = mxGetPr(mxGetFieldByNumber(ElemData,0,NewFieldNumbers[0]));

                                        
                    fnum = mxGetFieldNumber(ElemData,"T1");
	                NewFieldNumbers[8] = fnum;
					if(fnum<0)
					    pt1 = NULL;
					else
					    pt1 = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
					
	                
	                fnum = mxGetFieldNumber(ElemData,"T2");
	                NewFieldNumbers[9] = fnum;
					if(fnum<0)
					    pt2 = NULL;
					else
					    pt2 = mxGetPr(mxGetFieldByNumber(ElemData,0,fnum));
                    
                    
					returnptr = NewFieldNumbers;
				}	break;

			case	USE_LOCAL_COPY:	/* Get fields from MATLAB using field numbers
										The second argument ponter to the array of field 
										numbers is previously created with 
										QuadLinPass( ..., MAKE_LOCAL_COPY)
									*/	
				{	limitsptr = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[0]));
                    
                    
                    					
                    if(FieldNumbers[1]<0)
					    pt1 = NULL;
					else    
					    pt1 = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[1]));
					    
					if(FieldNumbers[2]<0)
					    pt2 = NULL;
					else 
					    pt2 = mxGetPr(mxGetFieldByNumber(ElemData,0,FieldNumbers[2]));
                    
                    
					returnptr = FieldNumbers;
				}	break;

			default:
				{	mexErrMsgTxt("No match for calling mode in function AperturePass\n");
				}
	}


	AperturePass(r_in,limitsptr,num_particles,pt1, pt2);

	return(returnptr);
}













void mexFunction(	int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{ 	
    double *r_in, *LIMITS,*pt1, *pt2; 
  	int m,n;
	mxArray *tmpmxptr;

	if(nrhs)
	{
    /* ALLOCATE memory for the output array of the same size as the input  */
	r_in = mxGetPr(prhs[1]);
	m = mxGetM(prhs[1]);
	n = mxGetN(prhs[1]);
	if(m!=6) 
		{mexErrMsgTxt("Second argument must be a 6 x N matrix");}	
	
		


	tmpmxptr = mxGetField(prhs[0],0,"Limits");
	if(tmpmxptr)
	    LIMITS = mxGetPr(tmpmxptr);
	else
		mexErrMsgTxt("Required field 'Limits' was not found in the element data structure"); 

    	tmpmxptr=mxGetField(prhs[0],0,"T1");
	if(tmpmxptr)
        pt1 = mxGetPr(tmpmxptr);
    else
		pt1 = NULL; 

	tmpmxptr=mxGetField(prhs[0],0,"T2");
	if(tmpmxptr)
        pt2 = mxGetPr(tmpmxptr);
    else
		pt2 = NULL;
    
    
    plhs[0] = mxDuplicateArray(prhs[1]);
	r_in = mxGetPr(plhs[0]);		
		
	AperturePass(r_in,LIMITS,n,pt1, pt2);

	}
	else
	{   /* return list of required fields */
	    plhs[0] = mxCreateCellMatrix(1,1);
	    mxSetCell(plhs[0],0,mxCreateString("Limits"));
	    if(nlhs>1) /* Required and optional fields */ 
	    {   plhs[1] = mxCreateCellMatrix(2,1); /* No optional fields */
            mxSetCell(plhs[1],0,mxCreateString("T1"));
	        mxSetCell(plhs[1],1,mxCreateString("T2"));
	    }
	}

}
