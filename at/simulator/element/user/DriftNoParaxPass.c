/* DriftPass.c
 * Accelerator Toolbox
 * Revision 6/26/00
 * A.Terebilo terebilo@ssrl.slac.stanford.edu
 */
#include <math.h>
#include "mex.h"
#include "elempass.h"
#include <stdio.h>


void DriftPass(double *r_in, double le, int num_particles, double Eid)
/* le - physical length
 * r_in - 6-by-N matrix of initial conditions reshaped into
 * 1-d array of 6*N elements
 */
{	int i, i6;
    double mc2=0.511e6, c=299792458;
    double gamma, E, t, vx, vy, vs, coef, vmod, vmod_id, gamma_id;
    
    for(i = 0;i<num_particles;i++)
    {	i6 = i*6;
        if(!mxIsNaN(r_in[i6])) {
            
            E=Eid*(1+r_in[i6+4]);
            gamma=E/mc2;
            gamma_id=Eid/mc2;
            vmod=c*sqrt(1-1/(gamma*gamma));
            vmod_id=c*sqrt(1-1/(gamma_id*gamma_id)); 
            coef=(c*sqrt(Eid*Eid-mc2*mc2))/(mc2*gamma);
            vx=r_in[i6+1]*coef;
            vy=r_in[i6+3]*coef;
            vs=sqrt(vmod*vmod-vx*vx-vy*vy);
            t=le/vs; 
            r_in[i6+0]+= t*vx;
            r_in[i6+2]+= t*vy;
            r_in[i6+5]-= c*(le-t*vmod_id)/vmod_id;
            /*r_in[i6+5]-= c*le*(1/vmod_id-1/vs);*/
        }
        
    }
}




ExportMode int* passFunction(const mxArray *ElemData, int *FieldNumbers,
        double *r_in, int num_particles, int mode)
        
        
#define NUM_FIELDS_2_REMEMBER 2
        
{	double le,Eid;
    int *returnptr;
    int fnum, *NewFieldNumbers;
    switch(mode)
    {	case NO_LOCAL_COPY:	/* Not used in AT1.3 Get fields by names from MATLAB workspace  */ {
        }	break;
        
        case MAKE_LOCAL_COPY: 	/* Find field numbers first
         * Save a list of field number in an array
         * and make returnptr point to that array
         */
        {
            NewFieldNumbers = (int*)mxCalloc(NUM_FIELDS_2_REMEMBER, sizeof(int));
            fnum = mxGetFieldNumber(ElemData, "Length");
            if(fnum<0)
                mexErrMsgTxt("Required field 'Length' was not found in the element data structure");
            else
            {   NewFieldNumbers[0] = fnum;
                le = mxGetScalar(mxGetFieldByNumber(ElemData, 0, NewFieldNumbers[0]));
                returnptr = NewFieldNumbers;
            }
            fnum = mxGetFieldNumber(ElemData, "Energy");
            if(fnum<0)
                mexErrMsgTxt("Required field 'Energy' was not found in the element data structure");
            else
            {   NewFieldNumbers[1] = fnum;
                Eid = mxGetScalar(mxGetFieldByNumber(ElemData, 0, NewFieldNumbers[1]));
                returnptr = NewFieldNumbers;
            }
        }	break;
        
        case	USE_LOCAL_COPY:	/* Get fields from MATLAB using field numbers
         * The second argument ponter to the array of field
         * numbers is previously created with
         * QuadLinPass( ..., MAKE_LOCAL_COPY)
         */
            
        {	le = mxGetScalar(mxGetFieldByNumber(ElemData, 0, FieldNumbers[0]));
            Eid = mxGetScalar(mxGetFieldByNumber(ElemData, 0, FieldNumbers[1]));
            returnptr = FieldNumbers;
        }	break;
        default:
        {	mexErrMsgTxt("No match for calling mode in function DriftPass\n");
        }
    }
    DriftPass(r_in, le, num_particles, Eid);
    return(returnptr);
}









void mexFunction(	int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{ 	double le, Eid;
    int m, n;
    double *r_in;
    mxArray *tmpmxptr;
    if(nrhs) {
        /* ALLOCATE memory for the output array of the same size as the input  */
        m = mxGetM(prhs[1]);
        n = mxGetN(prhs[1]);
        if(m!=6)
            mexErrMsgTxt("Second argument must be a 6 x N matrix");
        
        tmpmxptr=mxGetField(prhs[0], 0, "Length");
        if(tmpmxptr)
            le = mxGetScalar(tmpmxptr);
        else
            mexErrMsgTxt("Required field 'Length' was not found in the element data structure");
        
        tmpmxptr=mxGetField(prhs[0], 0, "Energy");
        if(tmpmxptr)
            Eid = mxGetScalar(tmpmxptr);
        else
            mexErrMsgTxt("Required field 'Energy' was not found in the element data structure");
        
        plhs[0] = mxDuplicateArray(prhs[1]);
        r_in = mxGetPr(plhs[0]);
        DriftPass(r_in, le, n,Eid);
    }
    else {   /* return list of required fields */
        plhs[0] = mxCreateCellMatrix(1, 1);
        mxSetCell(plhs[0], 0, mxCreateString("Length"));
        if(nlhs>1) /* Required and optional fields */
        {   plhs[1] = mxCreateCellMatrix(0, 0); /* No optional fields */
        }
    }
    
}

