%FINDMPOLERADDIFFMATIRX calculates radiation diffusion matrix of a multipole element
% for use in Ohmi's beam envelope formalism [1]
% See also OHMIENVELOPE
% [1] K.Ohmi et al. Phys.Rev.E. Vol.49. (1994)
% Z.Martí Modification 2/2/2010
