/* findmpoleraddifmatrix.c

   mex-function to calculate radiation diffusion matrix B defined in [2] 
   for multipole elements in MATLAB Accelerator Toolbox
   A.Terebilo 8/14/00

   References
   [1] M.Sands 'The Physics of Electron Storage Rings
   [2] Ohmi, Kirata, Oide 'From the beam-envelope matrix to synchrotron
   radiation integrals', Phys.Rev.E  Vol.49 p.751 (1994)
*/

#include "mex.h"
#include "matrix.h"
#include "atlalib.c"
#include <math.h>
#include "gwig.h"


/* Fourth order-symplectic integrator constants */

#define x1     1.351207191959657328
#define x0    -1.702414383919314656

/* Physical constants used in the calculations */

#define TWOPI	   6.28318530717959
#define CGAMMA 	   8.846056192e-05 			/* [m]/[GeV^3] Ref[1] (4.1)      */
#define M0C2       5.10999060e5				/* Electron rest mass [eV]       */
#define LAMBDABAR  3.86159323e-13			/* Compton wavelength/2pi [m]    */
#define CER   	   2.81794092e-15			/* Classical electron radius [m] */
#define CU         1.323094366892892		/* 55/(24*sqrt(3)) factor        */

#define SQR(X) ((X)*(X))





double B2perp(double bx, double by, double bz, double *X, double E0)    
{   /*Calculates sqr(|e x B|) , where e is a unit vector in the direction of velocity*/
    double coef,vx,vy,vz,gamma,E,vmod;
    double c=299792458;        /* m s-1 */
    E=E0*(1+X[4]);
    gamma=E/M0C2;
    vmod=c*sqrt(1-1/(gamma*gamma));
    coef=(c*sqrt(E0*E0-M0C2*M0C2))/(M0C2*gamma);
    vx=X[1]*coef;
    vy=X[3]*coef;
    vz=sqrt(vmod*vmod-vx*vx-vy*vy);
	return((SQR(bz*vy-by*vz) + SQR(bx*vz-bz*vx) + SQR(bx*vy-by*vx))/SQR(vmod)) ;
}



double sinc(double x)
{   /* Expand sinc(x) = sin(x)/x to x^8 */
    double x2, result;
    x2 = x*x;
    result = 1e0 - x2/6e0*(1e0 - x2/20e0 *(1e0 - x2/42e0*(1e0-x2/72e0) ) );
    return result;
}
    
    
    
    
/*----------------------------------------MAGNETIC FIELD--------------------------------------------*/
    
    

void GWigBx(struct gwig *pWig, double *Xvec, double *pbx) 
{
  int    i;
  double x, y, z, Cmn;
  double kx, ky, kz, tz;
  double sx, shy, cz;
  double chx, cy;
  double bx, BMAX;

  x = Xvec[0];
  y = Xvec[2];
  z = pWig->Zw;
  BMAX = pWig->PB0;
  bx   = 0e0;

  /* Horizontal Wiggler: note that one potentially could have: kx=0 */
  for (i = 0; i < pWig->NHharm; i++) {
    Cmn= pWig->HCw_raw[i];
    kx = pWig->Hkx[i];
    ky = pWig->Hky[i];
    kz = pWig->Hkz[i];
    tz = pWig->Htz[i];

    sx  = sin(kx * x);
    shy = sinh(ky * y);
    cz  = cos(kz * z + tz);
    bx  = bx + BMAX*(Cmn*kx/ky)*sx*shy*cz;
  }
  /* Vertical Wiggler: note that one potentially could have: ky=0 */
  for (i = 0; i < pWig->NVharm; i++ ) {
    Cmn= pWig->VCw_raw[i];
    kx = pWig->Vkx[i];
    ky = pWig->Vky[i];
    kz = pWig->Vkz[i];
    tz = pWig->Vtz[i];

    chx = cosh(kx * x);
    cy  = cos(ky * y);
    cz  = cos(kz * z + tz);
    bx  = bx + BMAX*Cmn*chx*cy*cz;  
  }
  *pbx   = bx;
}





void GWigBy(struct gwig *pWig, double *Xvec, double *pby) 
{
  int    i;
  double x, y, z, Cmn;
  double kx, ky, kz, tz;
  double cx, chy, cz;
  double shx, sy;
  double by, BMAX;

  x = Xvec[0];
  y = Xvec[2];
  z = pWig->Zw;
  BMAX = pWig->PB0;
  by   = 0e0;

  /* Horizontal Wiggler: note that one potentially could have: kx=0 */
  for (i = 0; i < pWig->NHharm; i++) {
    Cmn= pWig->HCw_raw[i];
    kx = pWig->Hkx[i];
    ky = pWig->Hky[i];
    kz = pWig->Hkz[i];
    tz = pWig->Htz[i];

    cx  = cos(kx * x);
    chy = cosh(ky * y);
    cz  = cos(kz * z + tz);
    by  = by - BMAX*Cmn*cx*chy*cz;
  }
  /* Vertical Wiggler: note that one potentially could have: ky=0 */
  for (i = 0; i < pWig->NVharm; i++ ) {
    Cmn= pWig->VCw_raw[i];
    kx = pWig->Vkx[i];
    ky = pWig->Vky[i];
    kz = pWig->Vkz[i];
    tz = pWig->Vtz[i];

    shx = sinh(kx * x);
    sy  = sin(ky * y);
    cz  = cos(kz * z + tz);
    by  = by - BMAX*(Cmn*ky/kx)*shx*sy*cz;  
  }
  *pby   = by;
}





void GWigBz(struct gwig *pWig, double *Xvec, double *pbz) 
{
  int    i;
  double x, y, z, Cmn;
  double kx, ky, kz, tz;
  double cx, shy, sz;
  double shx, cy;
  double bz, BMAX;

  x = Xvec[0];
  y = Xvec[2];
  z = pWig->Zw;
  BMAX = pWig->PB0;
  bz   = 0e0;

  /* Horizontal Wiggler: note that one potentially could have: kx=0 */
  for (i = 0; i < pWig->NHharm; i++) {
    Cmn= pWig->HCw_raw[i];
    kx = pWig->Hkx[i];
    ky = pWig->Hky[i];
    kz = pWig->Hkz[i];
    tz = pWig->Htz[i];

    cx  = cos(kx * x);
    shy = sinh(ky * y);
    sz  = sin(kz * z + tz);
    bz  = bz + BMAX*(Cmn*kz/ky)*cx*shy*sz;
  }
  /* Vertical Wiggler: note that one potentially could have: ky=0 */
  for (i = 0; i < pWig->NVharm; i++ ) {
    Cmn= pWig->VCw_raw[i];
    kx = pWig->Vkx[i];
    ky = pWig->Vky[i];
    kz = pWig->Vkz[i];
    tz = pWig->Vtz[i];

    shx = sinh(kx * x);
    cy  = cos(ky * y);
    sz  = sin(kz * z + tz);
    bz  = bz - BMAX*(Cmn*kz/kx)*shx*cy*sz;
  }
  *pbz   = bz;
}

/*--------------------------------------------------------------------------------------------------*/
  
/*----------------------------------DERIVATION MAGNETIC FIELDS--------------------------------------*/
  
void GWigdBxdy(struct gwig *pWig, double *Xvec, double *dpbx) 
{
  int    i;
  double x, y, z, Cmn;
  double kx, ky, kz, tz;
  double sx, chy, cz;
  double chx, sy;
  double dbx, BMAX;

  x = Xvec[0];
  y = Xvec[2];
  z = pWig->Zw;
  BMAX = pWig->PB0;
  dbx   = 0e0;

  /* Horizontal Wiggler: note that one potentially could have: kx=0 */
  for (i = 0; i < pWig->NHharm; i++) {
    Cmn= pWig->HCw_raw[i];
    kx = pWig->Hkx[i];
    ky = pWig->Hky[i];
    kz = pWig->Hkz[i];
    tz = pWig->Htz[i];

    sx  = sin(kx * x);
    chy = cosh(ky * y);
    cz  = cos(kz * z + tz);
    dbx = dbx + BMAX*(Cmn*kx)*sx*chy*cz;
  }
  /* Vertical Wiggler: note that one potentially could have: ky=0 */
  for (i = 0; i < pWig->NVharm; i++ ) {
    Cmn= pWig->VCw_raw[i];
    kx = pWig->Vkx[i];
    ky = pWig->Vky[i];
    kz = pWig->Vkz[i];
    tz = pWig->Vtz[i];

    chx = cosh(kx * x);
    sy  = sin(ky * y);
    cz  = cos(kz * z + tz);
    dbx = dbx - BMAX*(Cmn*ky)*chx*sy*cz;  
  }
  *dpbx   = dbx;
}





void GWigdBydx(struct gwig *pWig, double *Xvec, double *dpby) 
{
  int    i;
  double x, y, z, Cmn;
  double kx, ky, kz, tz;
  double sx, chy, cz;
  double chx, sy;
  double dby, BMAX;

  x = Xvec[0];
  y = Xvec[2];
  z = pWig->Zw;
  BMAX = pWig->PB0;
  dby   = 0e0;

  /* Horizontal Wiggler: note that one potentially could have: kx=0 */
  for (i = 0; i < pWig->NHharm; i++) {
    Cmn= pWig->HCw_raw[i];
    kx = pWig->Hkx[i];
    ky = pWig->Hky[i];
    kz = pWig->Hkz[i];
    tz = pWig->Htz[i];

    sx  = sin(kx * x);
    chy = cosh(ky * y);
    cz  = cos(kz * z + tz);
    dby = dby + BMAX*(Cmn*kx)*sx*chy*cz;
  }
  /* Vertical Wiggler: note that one potentially could have: ky=0 */
  for (i = 0; i < pWig->NVharm; i++ ) {
    Cmn= pWig->VCw_raw[i];
    kx = pWig->Vkx[i];
    ky = pWig->Vky[i];
    kz = pWig->Vkz[i];
    tz = pWig->Vtz[i];

    chx = cosh(kx * x);
    sy  = sin(ky * y);
    cz  = cos(kz * z + tz);
    dby = dby - BMAX*(Cmn*ky)*chx*sy*cz;  
  }
  *dpby  = dby;
}


/*-------------------------------------------------------------------------------------------------*/

/*------------------------------------------VECTOR POTENTIAL----------------------------------------*/



void GWigAx(struct gwig *pWig, double *Xvec, double *pax, double *paxpy) 
{
  int    i;
  double x, y, z;
  double kx, ky, kz, tz, kw;
  double cx, sxkx, chx, shx;
  double cy, sy, chy, shy, sz;
  double gamma0, beta0;
  double ax, axpy;

  x = Xvec[0];
  y = Xvec[2];
  z = pWig->Zw;
  
  kw   = 2e0*PI/(pWig->Lw);
  ax   = 0e0;
  axpy = 0e0;

  gamma0   = pWig->E0/XMC2;
  beta0    = sqrt(1e0 - 1e0/(gamma0*gamma0));
  pWig->Aw = (q_e/m_e/clight)/(2e0*PI) * (pWig->Lw) * (pWig->PB0);

  /* Horizontal Wiggler: note that one potentially could have: kx=0 */
  for (i = 0; i < pWig->NHharm; i++) {
    pWig->HCw[i] = pWig->HCw_raw[i]*(pWig->Aw)/(gamma0*beta0);
    kx = pWig->Hkx[i];
    ky = pWig->Hky[i];
    kz = pWig->Hkz[i];
    tz = pWig->Htz[i];

    cx  = cos(kx*x);
    chy = cosh(ky * y);
    sz  = sin(kz * z + tz);
    ax  = ax + (pWig->HCw[i])*(kw/kz)*cx*chy*sz;

    shy = sinh(ky * y);
    if ( abs(kx/kw) > GWIG_EPS ) {
      sxkx = sin(kx * x)/kx;	
    } else {
      sxkx = x*sinc(kx*x);
    }

    axpy = axpy + pWig->HCw[i]*(kw/kz)*ky*sxkx*shy*sz;
  }


  /* Vertical Wiggler: note that one potentially could have: ky=0 */
  for (i = 0; i < pWig->NVharm; i++ ) {
    pWig->VCw[i] = pWig->VCw_raw[i]*(pWig->Aw)/(gamma0*beta0);
    kx = pWig->Vkx[i];
    ky = pWig->Vky[i];
    kz = pWig->Vkz[i];
    tz = pWig->Vtz[i];

    shx = sinh(kx * x);
    sy  = sin(ky * y);
    sz  = sin(kz * z + tz);
    ax  = ax + pWig->VCw[i]*(kw/kz)*(ky/kx)*shx*sy*sz;

    chx = cosh(kx * x);
    cy  = cos(ky * y);
    axpy = axpy + pWig->VCw[i]*(kw/kz)* pow(ky/kx,2) *chx*cy*sz;      
  }

  *pax   = ax;
  *paxpy = axpy;
}






void GWigAy(struct gwig *pWig, double *Xvec, double *pay, double *paypx)
{
  int    i;
  double x, y, z;
  double kx, ky, kz, tz, kw;
  double cx, sx, chx, shx;
  double cy, syky, chy, shy, sz;
  double gamma0, beta0;
  double ay, aypx;

  x = Xvec[0];
  y = Xvec[2];
  z = pWig->Zw;
    
  kw   = 2e0*PI/(pWig->Lw);
  ay   = 0e0;
  aypx = 0e0;

  gamma0  = pWig->E0/XMC2;
  beta0   = sqrt(1e0 - 1e0/(gamma0*gamma0));
  pWig->Aw = (q_e/m_e/clight)/(2e0*PI) * (pWig->Lw) * (pWig->PB0);
     
  /* Horizontal Wiggler: note that one potentially could have: kx=0 */
  for ( i = 0; i < pWig->NHharm; i++ ){
    pWig->HCw[i] = (pWig->HCw_raw[i])*(pWig->Aw)/(gamma0*beta0);
    kx = pWig->Hkx[i];
    ky = pWig->Hky[i];
    kz = pWig->Hkz[i];
    tz = pWig->Htz[i];
  
    sx = sin(kx * x);
    shy = sinh(ky * y);
    sz  = sin(kz * z + tz);
    ay  = ay + (pWig->HCw[i])*(kw/kz)*(kx/ky)*sx*shy*sz;
  
    cx  = cos(kx * x);
    chy = cosh(ky * y);
    
    aypx = aypx + (pWig->HCw[i])*(kw/kz)*pow(kx/ky,2) * cx*chy*sz;
  }

  /* Vertical Wiggler: note that one potentially could have: ky=0 */
  for (i = 0; i < pWig->NVharm; i++) {
    pWig->VCw[i] = (pWig->VCw_raw[i])*(pWig->Aw)/(gamma0*beta0);       
    kx = pWig->Vkx[i];
    ky = pWig->Vky[i];
    kz = pWig->Vkz[i];
    tz = pWig->Vtz[i];

    chx = cosh(kx * x);
    cy  = cos(ky * y);
    sz  = sin(kz * z + tz);
    ay  = ay + (pWig->VCw[i])*(kw/kz)*chx*cy*sz;

    shx = sinh(kx * x);
    if (abs(ky/kw) > GWIG_EPS) {
      syky  = sin(ky * y)/ky;
    } else {
      syky = y * sinc(ky * y);
      aypx = aypx + (pWig->VCw[i])*(kw/kz)* kx*shx*syky*sz;
    }
  }
  
  *pay = ay;
  *paypx = aypx;
}


/*--------------------------------------------------------------------------------------------*/




void GWigInit(struct gwig *Wig, double Ltot, double Lw, double Bmax,
	      int Nstep, int Nmeth, int NHharm, int NVharm,
	      double *pBy, double *pBx, double E0, double *T1, double *T2, 
	      double *R1, double *R2)
{
  double *tmppr;
  double design_energy;
  int    i;
  double kw;
  
  design_energy=E0*1e-9;  /* convert to GeV */
    
  Wig->E0 = design_energy;
  Wig->Pmethod = Nmeth;
  Wig->PN = Nstep;
  Wig->Nw = (int)((Ltot + GWIG_EPS)/ Lw);
  Wig->NHharm = NHharm;
  Wig->NVharm = NVharm;
  Wig->PB0 = Bmax;
  Wig->Lw  = Lw;


  kw = 2.0e0*PI/(Wig->Lw);
  Wig->Zw = 0.0;
  Wig->Aw = 0.0;
  tmppr = pBy;
  for (i = 0; i < NHharm; i++){
    tmppr++;
    Wig->HCw[i] = 0.0;
    Wig->HCw_raw[i] = *tmppr;

    tmppr++;
    Wig->Hkx[i]     = (*tmppr) * kw;

    tmppr++;
    Wig->Hky[i]     = (*tmppr) * kw;

    tmppr++;
    Wig->Hkz[i]     = (*tmppr) * kw;

    tmppr++;
    Wig->Htz[i]     =  *tmppr;

    tmppr++;
  }

  tmppr = pBx;
  for (i = 0; i < NVharm; i++){
    tmppr++;
    Wig->VCw[i] = 0.0;
    Wig->VCw_raw[i] = *tmppr;

    tmppr++;
    Wig->Vkx[i]     = (*tmppr) * kw;

    tmppr++;
    Wig->Vky[i]     = (*tmppr) * kw;

    tmppr++;
    Wig->Vkz[i]     = (*tmppr) * kw;

    tmppr++;
    Wig->Vtz[i]     =  *tmppr;

    tmppr++;
  }
  
  for (i = NHharm ; i< WHmax; i++) {
    Wig->HCw[i] = 0.0;
    Wig->HCw_raw[i] = 0.0;
    Wig->Hkx[i] = 0.0;
    Wig->Hky[i] = 0.0;
    Wig->Hkz[i] = 0.0;
    Wig->Htz[i] = 0.0;
  }
  for (i = NVharm ; i< WHmax; i++) {
    Wig->VCw[i] = 0.0;
    Wig->VCw_raw[i] = 0.0;
    Wig->Vkx[i] = 0.0;
    Wig->Vky[i] = 0.0;
    Wig->Vkz[i] = 0.0;
    Wig->Vtz[i] = 0.0;
  }
}





void GWigGauge(struct gwig *pWig, double *X, int flag)
{
  double ax, ay, axpy, aypx;

  GWigAx(pWig, X, &ax, &axpy);
  GWigAy(pWig, X, &ay, &aypx);
  
  if (flag == Elem_Entrance) {
    /* At the entrance of the wiggler */
    X[1] = X[1] + ax;
    X[3] = X[3] + ay;
  } else if (flag == Elem_Exit) {
    /* At the exit of the wiggler */
    X[1] = X[1] - ax;
    X[3] = X[3] - ay;
  } else {
    printf("  GWigGauge: Unknown flag = %i\n", flag);
  }
}




void rad_loss(double *X, double B2P, double L, double irho, double E0)
{   /*This subroutine determines the radiation loss of the wiggler*/
    double CRAD = CGAMMA*E0*E0*E0/(TWOPI*1e27);   /* m */
    double c = 299792458;   /* m s-1 */
    double e = 1;
    double dp_0, dDelta, Brho;
       
	dp_0 = X[4];
    Brho = E0/(e*c);
    dDelta = - CRAD*SQR(1+X[4])*B2P*(1 + X[0]*irho + 0.5e0*(SQR(X[1])+SQR(X[3]))/SQR(1+X[4]))*L/SQR(Brho);
	X[4] = X[4] + dDelta;
	X[1] = X[1]*((1+X[4])/(1+dp_0));
	X[3] = X[3]*((1+X[4])/(1+dp_0));
}




void wigg_radloss(struct gwig *pWig, double *X, double L)
{     /*This subroutine applies the radiation loss*/
      double B2P,bx,by,bz,ax,ay,axpy,aypx,irho;
      double E0 = pWig->E0*1e9;    /*  eV  */
      double c = 299792458;     /*  m s-1  */
      double e = 1;
      GWigBx(pWig, X, &bx);
      GWigBy(pWig, X, &by);
      GWigBz(pWig, X, &bz);
      GWigAx(pWig, X, &ax, &axpy);
      GWigAy(pWig, X, &ay, &aypx);
      B2P=B2perp(bx, by, bz, X, E0);
      irho=e*c*sqrt(B2P)/(E0*(1+X[4]));
      X[1] -= ax;
      X[3] -= ay;
      rad_loss(X, B2P, L, irho, E0);
      X[1] += ax;
      X[3] += ay;
}





void GWigMap_2nd(struct gwig *pWig, double *X, double dl) 
{

  double dld, dl2, dl2d;
  double ax, ay, axpy, aypx;
  
  dld  = dl/(1.0e0 + X[4]);
  dl2  = 0.5e0 * dl;
  dl2d = dl2/(1.0e0 + X[4]);

  /* Step1: increase a half step in z */
      wigg_radloss(pWig, X, dl2);
  pWig->Zw = pWig->Zw + dl2;

  /* Step2: a half drift in y */ 
  GWigAy(pWig, X, &ay, &aypx);
  X[1] = X[1] - aypx;
  X[3] = X[3] - ay;

  X[2] = X[2] + dl2d*X[3];
  X[5] = X[5] + 0.5e0*dl2d*(X[3]*X[3])/(1.0e0+X[4]);
   
  GWigAy(pWig, X, &ay, &aypx);
  X[1] = X[1] + aypx;
  X[3] = X[3] + ay;

  /* Step3: a full drift in x */
  GWigAx(pWig, X, &ax, &axpy);
  X[1] = X[1] - ax;
  X[3] = X[3] - axpy;

  X[0] = X[0] + dld*X[1];
  X[5] = X[5] + 0.5e0*dld*(X[1]*X[1])/(1.0e0+X[4]);
  
  GWigAx(pWig, X, &ax, &axpy);
  X[1] = X[1] + ax;
  X[3] = X[3] + axpy;

  /* Step4: a half drift in y */
  GWigAy(pWig, X, &ay, &aypx);
  X[1] = X[1] - aypx;
  X[3] = X[3] - ay;

  X[2] = X[2] + dl2d*X[3];
  X[5] = X[5] + 0.5e0*dl2d*(X[3]*X[3])/(1.0e0+X[4]);
   
  GWigAy(pWig, X, &ay, &aypx);
  X[1] = X[1] + aypx;
  X[3] = X[3] + ay;

  /* Step5: increase a half step in z */
      wigg_radloss(pWig, X, dl2);
  pWig->Zw = pWig->Zw + dl2;
  
}





void wigglerM(struct gwig *pWig, double* orbit_in, double L, double E0, 
				double *M66)
/* Calculate the symplectic (no radiation) transfer matrix of a 
   thin multipole kick near the entrance point orbit_in
   For elements with straight coordinate system irho = 0
   For curved elements the B polynomial (PolynomB in MATLAB) 
   MUST NOT include the guide field  By0 = irho * E0 /(c*e)

   M is a (*double) pointer to a preallocated 1-dimentional array 
   of 36 elements of matrix M arranged column-by-column 
*/
{  int m,n;
   double ImSumN, ReSumN;
   double c = 299792458;   /* m s-1 */
   double e = 1;
   double bx,by,bz, Brho,irho;
   double dbxdy,dbydx;
   double B2P;

   /* Calculate the derivatives
	   ReSumN = (irho/B0)*Re(d(By + iBx)/dx)
	   ImSumN = (irho/B0)*Im(d(By + iBx)/dy)
    */
   
    Brho = E0/(e*c);    
    GWigdBxdy(pWig, orbit_in, &dbxdy);
    GWigdBydx(pWig, orbit_in, &dbydx); 
    ReSumN = dbydx/Brho;
    ImSumN = dbxdy/Brho;
               

	/* Initialize M66 to a 6-by-6 identity matrix */
	for(m=0;m<36;m++)
		M66[m]= 0;
	/* Set diagonal elements to 1 */
	for(m=0;m<6;m++)
		M66[m*7] = 1;
	
	/* The relationship between indexes when a 6-by-6 matrix is 
	   represented in MATLAB as one-dimentional array containing
	   36 elements arranged column-by-column is
	   [i][j] <---> [i+6*j] 
	*/
    
    GWigBx(pWig, orbit_in, &bx);
    GWigBy(pWig, orbit_in, &by);
    GWigBz(pWig, orbit_in, &bz);
    B2P=B2perp(bx, by, bz, orbit_in, E0);
    irho=e*c*sqrt(B2P)/(E0*(1+orbit_in[4]));
    /*printf("ReSumN = %e, ImSum = %e, L = %e \n",ReSumN,ImSumN,L);*/
    /*irho=0;               /*---------------------------IMPORTANTE--------------------------------*/

	M66[1]   = -L*ReSumN;				/* [1][0] */
	M66[13]  =  L*ImSumN;				/* [1][2] */
	M66[3]   =  L*ImSumN;				/* [3][0] */
	M66[15]  =  L*ReSumN;				/* [3][2] */
	M66[25]  =  L*irho;					/* [1][4] */
	M66[1]  += -L*irho*irho;			/* [1][0] */
	M66[5]   =  L*irho;					/* [5][0] */

    /*printf("M66[1]= %e,M66[3]= %e,M66[5]= %e,M66[13]= %e,M66[15]= %e,M66[25]= %e \n",M66[1],M66[3],M66[5],M66[13],M66[15],M66[25]);*/
}





void wigglerB(struct gwig *pWig, double* orbit_in, double L, 
				 double E0, double *B66)

/* Calculate Ohmi's diffusion matrix of a thin multipole  element 
   For elements with straight coordinate system irho = 0
   For curved elements the B polynomial (PolynomB in MATLAB) 
   MUST NOT include the guide field  By0 = irho * E0 /(c*e)
   The result is stored in a preallocated 1-dimentional array B66
   of 36 elements of matrix B arranged column-by-column
*/

{	double BB,B2P,B3P;
	int i;
	double p_norm = 1/(1+orbit_in[4]);
	double p_norm2 = SQR(p_norm);
    double bx,by,bz,Brho,irho;
    double c = 299792458;   /* m s-1 */
    double e = 1;
	
  	/* calculate the local transverse magnetic field */

      Brho = E0/(e*c);
      GWigBx(pWig, orbit_in, &bx);
      GWigBy(pWig, orbit_in, &by);
      GWigBz(pWig, orbit_in, &bz);
              	
	    
    /* determine the magnetic field in T for determining irho */
              
    B2P=B2perp(bx, by, bz, orbit_in, E0);
    irho=e*c*sqrt(B2P)/(E0*(1+orbit_in[4]));
    
    /* calculate |B x n|^3 - the third power of the B field component 
	   orthogonal to the normalized velocity vector n
    */
    
    bx=bx/Brho;
    by=by/Brho;
    bz=bz/Brho;
    B2P=B2perp(bx, by, bz, orbit_in, E0); 
	B3P = B2P*sqrt(B2P);
    
	BB = CU * CER * LAMBDABAR *  pow(E0/M0C2,5) * L * B3P * SQR(SQR(1+orbit_in[4]))*
				(1+orbit_in[0]*irho + (SQR(orbit_in[1])+SQR(orbit_in[3]))*p_norm2/2);

	
	/* When a 6-by-6 matrix is represented in MATLAB as one-dimentional 
	   array containing 36 elements arranged column-by-column,
	   the relationship between indexes  is
	   [i][j] <---> [i+6*j] 

	*/
	
	/* initialize B66 to 0 */
	for(i=0;i<36;i++)
		B66[i] = 0;
	
	/* Populate B66 */
	B66[7]      = BB*SQR(orbit_in[1])*p_norm2;
	B66[19]     = BB*orbit_in[1]*orbit_in[3]*p_norm2;
	B66[9]      = B66[19];
	B66[21]     = BB*SQR(orbit_in[3])*p_norm2;
	B66[10]     = BB*orbit_in[1]*p_norm;
	B66[25]     = B66[10];
	B66[22]     = BB*orbit_in[3]*p_norm;
	B66[27]     = B66[22];
	B66[28]     = BB;
}


     


void FindElemB(double *orbit_in, double le, double Lw, double Bmax,
               int Nstep, int Nmeth, int NHharm, int NVharm,
               double *pBy, double *pBx, double E0, double *pt1,
               double *pt2, double *PR1, double *PR2, double *BDIFF)

{	/* Find Ohmi's diffusion matrix BDIFF of a thick multipole
	   BDIFF - cumulative Ohmi's diffusion is initialized to 0
	   BDIFF is preallocated 1 dimensional array to store 6-by-6 matrix 
	   columnwise
	*/
	
	int m;	
	double  *MKICK, *BKICK;

	/* 4-th order symplectic integrator constants */
	double dl1,dl0;
    int Niter = Nstep*(le/Lw);
    double SL = Lw/Nstep;
    struct gwig pWig;
    double ax,ay,axpy,aypx;       /*###########################################*/
    double orbit_in2[6], orbit_in3[6];             /*###########################################*/
    int i,j;             /*###########################################*/
    int flag;
    double *a;
       
    
	dl1 = x1*SL;
    dl0 = x0*SL;
	
	/* Allocate memory for thin kick matrix MKICK
	   and a diffusion matrix BKICK
	*/
 	MKICK = (double*)mxCalloc(36,sizeof(double));
	BKICK = (double*)mxCalloc(36,sizeof(double));
	for(m=0; m < 6; m++)
		{	MKICK[m] = 0;
			BKICK[m] = 0;
		}
	
	/* Transform orbit to a local coordinate system of an element
       BDIFF stays zero	*/
    if(pt1)
        ATaddvv(orbit_in,pt1);	
    if(PR1)
        ATmultmv(orbit_in,PR1);	

   
    /*Generate the wiggler*/ 
    
    GWigInit(&pWig, le, Lw, Bmax, Nstep, Nmeth,NHharm,NVharm,pBy,pBx,E0,pt1,pt2,PR1,PR2);
    
	/* Propagate orbit_in and BDIFF through a 4-th orderintegrator */
    
    flag=1;
    GWigGauge(&pWig, orbit_in, flag);
    
    FILE * fp;                                       /*#########################################*/
    fp = fopen ("file.csv", "w+");                   /*#########################################*/
    
    FILE * fp2;                                       /*#########################################*/
    fp2 = fopen ("file2.csv", "w+");                   /*#########################################*/
       
    /*orbit_in[0]=0;
    orbit_in[1]=0;
    orbit_in[2]=0;
    orbit_in[3]=0;
    orbit_in[4]=0;
    orbit_in[5]=0;*/
    
    
    /*int *a,*b,c;
a=&c;
b=a;*/
    
    

    
    GWigAx(&pWig, orbit_in, &ax, &axpy);                      /*######################################*/
    GWigAy(&pWig, orbit_in, &ay, &aypx);                      /*####################################*/
    fprintf(fp,"%e , %e \n", orbit_in[0], orbit_in[1]-ax);    /*#####################################*/    
    /*printf("x[0]=%e ,x[1]=%e ,x[2]=%e ,x[3]=%e ,x[4]=%e ,x[5]=%e \n",orbit_in[0],orbit_in[1],orbit_in[2],orbit_in[3],orbit_in[4],orbit_in[5]);*/
        
    
    for (m = 1; m <= Niter; m++ )   /* Loop over slices	*/
		{	          
               wigglerM(&pWig, orbit_in, dl1, E0, MKICK);
			   wigglerB(&pWig, orbit_in, dl1, E0, BKICK);
			   ATsandwichmmt(MKICK,BDIFF);
			   ATaddmm(BKICK,BDIFF);
               
               
               if (m==1){
                   for (i = 0; i <= 5; i++){
                       orbit_in2[i]=orbit_in[i];
                       orbit_in3[i]=orbit_in[i];
                   }
                   fprintf(fp2, "%e , %e , %e , %e , %e , %e \n", orbit_in[0], orbit_in[1], orbit_in[2], orbit_in[3], orbit_in[4], orbit_in[5]);
                   printf("%e , %e , %e , %e , %e , %e \n", orbit_in[0], orbit_in[1], orbit_in[2], orbit_in[3], orbit_in[4], orbit_in[5]);
                   printf("%e , %e , %e , %e , %e , %e \n", orbit_in2[0], orbit_in2[1], orbit_in2[2], orbit_in2[3], orbit_in2[4], orbit_in2[5]);
               }
               
               GWigMap_2nd(&pWig, orbit_in, dl1);
               
               if (m==1){
                   fprintf(fp2,"%e , %e , %e , %e , %e , %e \n", orbit_in[0], orbit_in[1],orbit_in[2],orbit_in[3],orbit_in[4],orbit_in[5]);
                   for (i = 0; i <= 5; i++){
                       orbit_in2[i] +=1e-6;
                       fprintf(fp2,"%e , %e , %e , %e , %e , %e \n", orbit_in2[0], orbit_in2[1],orbit_in2[2],orbit_in2[3],orbit_in2[4],orbit_in2[5]);
                       pWig.Zw = pWig.Zw - dl1;
                       GWigMap_2nd(&pWig, orbit_in2, dl1);
                       fprintf(fp2,"%e , %e , %e , %e , %e , %e \n", orbit_in2[0], orbit_in2[1],orbit_in2[2],orbit_in2[3],orbit_in2[4],orbit_in2[5]);
                       for (j = 0; j <= 5; j++){
                           orbit_in2[j]=orbit_in3[j];
                           printf("%d \n",i);
                           printf("%e , %e , %e , %e , %e , %e \n", orbit_in2[0], orbit_in2[1],orbit_in2[2],orbit_in2[3],orbit_in2[4],orbit_in2[5]);
                       }
                   }
                   for (i = 0; i <= 5; i++){
                       for (j = 0; j <= 4; j++){
                           fprintf(fp2,"%e , ", MKICK[i+j*6]);
                       }
                       fprintf(fp2,"%e \n", MKICK[i+5*6]);
                   }
               }

               wigglerM(&pWig, orbit_in, dl0, E0, MKICK);
			   wigglerB(&pWig, orbit_in, dl0, E0, BKICK);
			   ATsandwichmmt(MKICK,BDIFF);
			   ATaddmm(BKICK,BDIFF);
               GWigMap_2nd(&pWig, orbit_in, dl0);
               
               wigglerM(&pWig, orbit_in, dl1, E0, MKICK);
			   wigglerB(&pWig, orbit_in, dl1, E0, BKICK);
			   ATsandwichmmt(MKICK,BDIFF);
			   ATaddmm(BKICK,BDIFF);
               GWigMap_2nd(&pWig, orbit_in, dl1);
               
               GWigAx(&pWig, orbit_in, &ax, &axpy);                    /*####################################*/
               GWigAy(&pWig, orbit_in, &ay, &aypx);                    /*####################################*/
               fprintf(fp,"%e , %e \n", orbit_in[0], orbit_in[1]-ax);        /*####################################*/
		} 
    
        fclose(fp);                                       /*#########################################*/
        fclose(fp2);                                       /*#########################################*/
        
        
        flag=-1;
        GWigGauge(&pWig, orbit_in, flag);
        
		
        if(PR2)
            ATmultmv(orbit_in,PR2);	
        if(pt2)
            ATaddvv(orbit_in,pt2);	
        
        
		mxFree(MKICK);
		mxFree(BKICK);
}





void mexFunction(	int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
/* The calling syntax of this mex-function from MATLAB is
   FindMPoleRadDiffMatrix(ELEMENT, ORBIT)
   ELEMENT is the element structure with field names consistent with 
           a multipole transverse field model.
   ORBIT is a 6-by-1 vector of the closed orbit at the entrance (calculated elsewhere)
*/
{	int m,n;  
	double E0;
    double Ltot, Lw, Bmax; 
    double *pBy, *pBx;
	double *BDIFF;
    int Nstep, Nmeth;
    int NHharm, NVharm;
    mxArray *tmpmxptr;

	double *orb, *orb0;
	double *pt1, *pt2, *PR1, *PR2;
    


	m = mxGetM(prhs[1]);
	n = mxGetN(prhs[1]);
	if(!(m==6 && n==1))
		mexErrMsgTxt("Second argument must be a 6-by-1 column vector");
    
	/* ALLOCATE memory for the output array */
	plhs[0] = mxCreateDoubleMatrix(6,6,mxREAL);
	BDIFF = mxGetPr(plhs[0]);

    

	orb0 = mxGetPr(prhs[1]);
	/* make local copy of the input closed orbit vector */
	orb = (double*)mxCalloc(6,sizeof(double));
	for(m=0;m<6;m++)
		orb[m] = orb0[m];
    
	/* Retrieve element information */
    
     
  tmpmxptr = mxGetField(prhs[0],0,"Length");
  if(tmpmxptr)
    Ltot = mxGetScalar(tmpmxptr);
  else
    mexErrMsgTxt("Required field 'Length' was not found in the element data structure"); 
	
  tmpmxptr = mxGetField(prhs[0],0,"Lw");
  if(tmpmxptr)
    Lw = mxGetScalar(tmpmxptr);
  else
    mexErrMsgTxt("Required field 'Lw' was not found in the element data structure"); 
  
  tmpmxptr = mxGetField(prhs[0],0,"Bmax");
  if(tmpmxptr)
    Bmax = mxGetScalar(tmpmxptr);
  else
    mexErrMsgTxt("Required field 'Bmax' was not found in the element data structure"); 
  
  tmpmxptr = mxGetField(prhs[0],0,"Nstep");
  if(tmpmxptr)
    Nstep = (int)mxGetScalar(tmpmxptr);
  else
    mexErrMsgTxt("Required field 'Nstep' was not found in the element data structure"); 
  
  tmpmxptr = mxGetField(prhs[0],0,"Nmeth");
  if(tmpmxptr)
    Nmeth = (int)mxGetScalar(tmpmxptr);
  else
    mexErrMsgTxt("Required field 'Nmeth' was not found in the element data structure"); 

  tmpmxptr = mxGetField(prhs[0],0,"NHharm");
  if(tmpmxptr)
    NHharm = (int)mxGetScalar(tmpmxptr);
  else
    mexErrMsgTxt("Required field 'NHharm' was not found in the element data structure"); 
  
  tmpmxptr = mxGetField(prhs[0],0,"NVharm");
  if(tmpmxptr)
    NVharm = (int)mxGetScalar(tmpmxptr);
  else
    mexErrMsgTxt("Required field 'NVharm' was not found in the element data structure"); 
  
  tmpmxptr = mxGetField(prhs[0],0,"By");
  if(tmpmxptr)
    pBy = mxGetPr(tmpmxptr);
  else
    mexErrMsgTxt("Required field 'By' was not found in the element data structure"); 

  tmpmxptr = mxGetField(prhs[0],0,"Bx");
  if(tmpmxptr)
    pBx = mxGetPr(tmpmxptr);
  else
    mexErrMsgTxt("Required field 'Bx' was not found in the element data structure"); 

  tmpmxptr = mxGetField(prhs[0],0,"Energy");
  if(tmpmxptr)
    E0 = mxGetScalar(tmpmxptr);
  else
    mexErrMsgTxt("Required field 'Energy' was not found in the element data structure"); 
  
  tmpmxptr = mxGetField(prhs[0],0,"R1");
  if(tmpmxptr)
    PR1 = mxGetPr(tmpmxptr);
  else
    PR1 = NULL; 
  
  tmpmxptr = mxGetField(prhs[0],0,"R2");
  if(tmpmxptr)
    PR2 = mxGetPr(tmpmxptr);
  else
    PR2 = NULL; 
  
  tmpmxptr = mxGetField(prhs[0],0,"T1");
  if(tmpmxptr)
    pt1 = mxGetPr(tmpmxptr);
  else
    pt1 = NULL; 
  
  tmpmxptr = mxGetField(prhs[0],0,"T2");
  if(tmpmxptr)
    pt2 = mxGetPr(tmpmxptr);
  else
    pt2 = NULL;
    
    
           
    

	FindElemB(orb, Ltot, Lw, Bmax, Nstep, Nmeth, NHharm, NVharm,
                pBy, pBx, E0, pt1, pt2, PR1, PR2, BDIFF);
}
