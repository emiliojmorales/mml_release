/* SelfFieldPass.c
 * Accelerator Toolbox
 * Revision 24/05/17
 * Z.Marti
 */

#include "mex.h"
#include "elempass.h"
#include <math.h>


void SelfFieldPass(double *r_in, double le, double coef, int LinearField, int num_particles)
/* le - physical length
 * nv - peak voltage (V) normalized to the design enegy (eV)
 * r is a 6-by-N matrix of initial conditions reshaped into
 * 1-d array of 6*N elements
 */
{	int c, c6;
    double s_l , centre;
    centre=0;
    s_l=0;
    for(c = 0;c<num_particles;c++)
    {	c6 = c*6;
        if(!mxIsNaN(r_in[c6]))
            centre += r_in[c6+5];
    }
    centre/=(1.0*num_particles);
    for(c = 0;c<num_particles;c++)
    {	c6 = c*6;
        if(!mxIsNaN(r_in[c6]))
            s_l += (r_in[c6+5]-centre)*(r_in[c6+5]-centre);
    }
    s_l/=(1.0*num_particles);
    s_l=sqrt(s_l);
    for(c = 0;c<num_particles;c++)
    {	c6 = c*6;
        if(!mxIsNaN(r_in[c6])){
            if (LinearField==1)
            {
                r_in[c6+4] += coef*(r_in[c6+5]-centre);
            }
            else{
                r_in[c6+4] += coef*(r_in[c6+5]-centre)*exp(-(r_in[c6+5]-centre)*(r_in[c6+5]-centre)/2/s_l/s_l);
            }
        }
    }
}




ExportMode int* passFunction(const mxArray *ElemData, int *FieldNumbers,
        double *r_in, int num_particles, int mode)
        
        
#define NUM_FIELDS_2_REMEMBER 5
        
{	double le, energy, coef;
    int *returnptr,LinearField;
    int *NewFieldNumbers, fnum;
    
    
    switch(mode)
    {	case NO_LOCAL_COPY:	/* Obsolete in AT1.3  */ {
            
        }	break;
        
        case MAKE_LOCAL_COPY: 	/* Find field numbers first
         * Save a list of field number in an array
         * and make returnptr point to that array
         */
        {
            NewFieldNumbers = (int*)mxCalloc(NUM_FIELDS_2_REMEMBER, sizeof(int));
            
            fnum = mxGetFieldNumber(ElemData, "Length");
            if(fnum<0)
                mexErrMsgTxt("Required field 'Length' was not found in the element data structure");
            NewFieldNumbers[0] = fnum;
            
            fnum = mxGetFieldNumber(ElemData, "ChargeCoeficient");
            if(fnum<0)
                mexErrMsgTxt("Required field 'ChargeCoeficient' was not found in the element data structure");
            NewFieldNumbers[1] = fnum;
            
            fnum = mxGetFieldNumber(ElemData, "Energy");
            if(fnum<0)
                mexErrMsgTxt("Required field 'Energy' was not found in the element data structure");
            NewFieldNumbers[2] = fnum;
            
            fnum = mxGetFieldNumber(ElemData, "LinearField");
            if(fnum<0)
                mexErrMsgTxt("Required field 'LinearField' was not found in the element data structure");
            NewFieldNumbers[3] = fnum;
            
            
            le = mxGetScalar(mxGetFieldByNumber(ElemData, 0, NewFieldNumbers[0]));
            coef = mxGetScalar(mxGetFieldByNumber(ElemData, 0, NewFieldNumbers[1]));
            energy = mxGetScalar(mxGetFieldByNumber(ElemData, 0, NewFieldNumbers[2]));
            LinearField = (int)mxGetScalar(mxGetFieldByNumber(ElemData, 0, NewFieldNumbers[3]));
            
            returnptr = NewFieldNumbers;
        }	break;
        
        case	USE_LOCAL_COPY:	/* Get fields from MATLAB using field numbers
         * The second argument ponter to the array of field
         * numbers is previously created with
         * QuadLinPass( ..., MAKE_LOCAL_COPY)
         */
            
        {	le = mxGetScalar(mxGetFieldByNumber(ElemData, 0, FieldNumbers[0]));
            coef = mxGetScalar(mxGetFieldByNumber(ElemData, 0, FieldNumbers[1]));
            energy = mxGetScalar(mxGetFieldByNumber(ElemData, 0, FieldNumbers[2]));
            LinearField = (int)mxGetScalar(mxGetFieldByNumber(ElemData,0,FieldNumbers[3]));
            
            returnptr = FieldNumbers;
        }	break;
        
        default:
        {	mexErrMsgTxt("No match found for calling mode in function CavityPass\n");
        }
    }
    SelfFieldPass(r_in, le, coef, LinearField,num_particles);
    return(returnptr);
}




void mexFunction(	int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{ 	double coef, energy;
    int m, n,LinearField;
    double *r_in, le;
    mxArray *tmpmxptr;
    
    if(nrhs) {
        
        /* ALLOCATE memory for the output array of the same size as the input */
        m = mxGetM(prhs[1]);
        n = mxGetN(prhs[1]);
        if(m!=6)
            mexErrMsgTxt("Second argument must be a 6 x N matrix");
        
        
        tmpmxptr=mxGetField(prhs[0], 0, "Length");
        if(tmpmxptr)
            le = mxGetScalar(tmpmxptr);
        else
            mexErrMsgTxt("Required field 'Length' was not found in the element data structure");
        
        
        tmpmxptr=mxGetField(prhs[0], 0, "ChargeCoeficient");
        if(tmpmxptr)
            coef = mxGetScalar(tmpmxptr);
        else
            mexErrMsgTxt("Required field 'Voltage' was not found in the element data structure");
        
        tmpmxptr=mxGetField(prhs[0], 0, "Energy");
        if(tmpmxptr)
            energy = mxGetScalar(tmpmxptr);
        else
            mexErrMsgTxt("Required field 'Energy' was not found in the element data structure");
        
        tmpmxptr = mxGetField(prhs[0], 0, "LinearField");
        if(tmpmxptr)
            LinearField = (int)mxGetScalar(tmpmxptr);
        else
            mexErrMsgTxt("Required field 'LinearField' was not found in the element data structure");
        
        
        plhs[0] = mxDuplicateArray(prhs[1]);
        r_in = mxGetPr(plhs[0]);
        SelfFieldPass(r_in, le, coef, LinearField,n);
    }
    else {   /* return list of required fields */
        plhs[0] = mxCreateCellMatrix(4, 1);
        mxSetCell(plhs[0], 0, mxCreateString("Length"));
        mxSetCell(plhs[0], 1, mxCreateString("ChargeCoeficient"));
        mxSetCell(plhs[0], 2, mxCreateString("Energy"));
        mxSetCell(plhs[0],3,mxCreateString("LinearField"));
    }
    
}
