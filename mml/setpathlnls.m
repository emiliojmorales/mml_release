function [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathlnls(LinkFlag)
%SETPATHLNLS - Initializes the Matlab Middle Layer (MML) for Brazilian Source (LNLS)
%  [MachineName, SubMachineName, OnlineLinkMethod, MMLROOT] = setpathlnls(OnlineLinkMethod)
%
%  INPUTS
%  1. OnlineLinkMethod - 'MCA', 'LabCA' {Default}, SCA

%  Written by Greg Portmann


Machine = 'LNLS';


% Input parsing
if nargin < 1
    LinkFlag = '';
end
if isempty(LinkFlag)
    if strncmp(computer,'PC',2)
        LinkFlag = 'labca';
    elseif isunix
        LinkFlag = 'labca';
    else
        LinkFlag = 'labca';
    end
end

[MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'StorageRing', 'StorageRing', LinkFlag);

