function machineconfig(varargin)
%MACHINECONFIG - GUI for setmachineconfig
%
%  See also plotfamily, viewfamily

% Written by Greg Portmann


% To-Do:
% 1. Extra column to remove an item from the machine set


% Run initialization if it has not been run (like standalone)
checkforao;


FileName = 'Golden';
for i = length(varargin):-1:1
    if isstruct(varargin{i})
        % Remove structures
        varargin(i) = [];
    elseif iscell(varargin{i})
        % Remove cells
        varargin(i) = [];
    elseif ischar(varargin{i})
        % Look for keywords
        if strcmpi(varargin{i},'struct')
            % Remove
            varargin(i) = [];
        elseif strcmpi(varargin{i},'numeric')
            % Remove
            varargin(i) = [];
        elseif strcmpi(varargin{i},'Golden')
            % Golden file
            FileName = 'Golden';
            varargin(i) = [];
        elseif strcmpi(varargin{i},'simulator') || strcmpi(varargin{i},'model')
            ModeFlag = varargin{i};
            varargin(i) = [];
        elseif strcmpi(varargin{i},'Online')
            ModeFlag = 'Online';
            varargin(i) = [];
        elseif strcmpi(varargin{i},'Manual')
            ModeFlag = 'Manual';
            varargin(i) = [];
        elseif strcmpi(varargin{i},'physics')
            UnitsFlag = 'Physics';
            varargin(i) = [];
        elseif strcmpi(varargin{i},'hardware')
            UnitsFlag = 'Hardware';
            varargin(i) = [];
        elseif strcmpi(varargin{i},'Display')
            DisplayFlag = 1;
            varargin(i) = [];
        elseif strcmpi(varargin{i},'NoDisplay')
            DisplayFlag = 0;
            varargin(i) = [];
        end
    end
end


% Check for a lattice file input
if length(varargin) >= 1
    if ischar(varargin{1})
        FileName = varargin{1};
        varargin(1) = [];
    end
end

if isempty(FileName)
    if any(strcmpi(getfamilydata('Machine'),{'spear3','spear'}))
        DirectoryName = getfamilydata('Directory', 'GoldenConfigFiles');
    else
        DirectoryName = getfamilydata('Directory', 'ConfigData');
    end
    [FileName, DirectoryName] = uigetfile('*.mat', 'Select a configuration file to restore', DirectoryName);
    if FileName == 0
        fprintf('   No change to lattice (machineconfig)\n');
        return
    else
        FileName = fullfile(DirectoryName, FileName);
    end
end


if strcmpi(FileName, 'Golden') || strcmpi(FileName, 'Production')
    FigureTitle = sprintf('Stored values come from golden file: %s', getfamilydata('OpsData','LatticeFile'));
elseif strcmpi(FileName, 'Injection')
    FigureTitle = sprintf('Stored values come from injection file: %s', getfamilydata('OpsData','InjectionFile'));
else
    [Directory, FileNameTmp, Ext] = fileparts(FileName);
    FigureTitle = sprintf('Stored values come from file: %s', FileNameTmp);
end


% Create a figure that will have a uitable, axes and checkboxes
h_fig = figure( ...
    'Name', FigureTitle,    ... % Title figure
    'NumberTitle', 'off', ... % Do not show figure number
    'MenuBar', 'none',    ... % Hide standard menu bar menus
    'Units', 'pixels');
%'Units', 'characters', ...
%'Position', [100, 300, 500, 460], ...


% Make the figure longer
ExtraHeight = 350;
a = get(h_fig, 'Position');
a(2) = a(2) - ExtraHeight;
a(4) = a(4) + ExtraHeight;
set(h_fig, 'Position', a);


% Could add pushtool for save/restore etc.
%ht = uitoolbar(h_fig);
%htt = uipushtool(ht,'TooltipString','Hello');


Table = MachineConfig_buildtable(FileName);


% Define parameters for a uitable (col headers are fictional)
ColNames = {'Common Name', 'Stored', 'Present', 'Difference', 'Channel Name'};

% Columns contains
colfmt = {'char', 'numeric', 'numeric', 'numeric', 'char'};

% Editing or not
coledit = [false false false false false];

% Set columns width
colwdt = {160 90 90 90 180};

a = get(h_fig, 'Position');

TableWidth = sum(cell2mat(colwdt))+60;

% Create table
htable = uitable(...
    'Units', 'pixels', ...
    'Position', [4 4 TableWidth a(4)-40], ...
    'Data',  Table, ...
    'ColumnName', ColNames, ...
    'ColumnFormat', colfmt, ...
    'ColumnWidth', colwdt, ...
    'ColumnEditable', coledit, ...
    'FontWeight', 'Bold', ...
    'RearrangeableColumns', 'off', ...
    'ToolTipString', '', ...
    'UserData', FileName, ...
    'CellEditCallback',{@MachineConfig_Table_Callback});
% CellSelectionCallback

% Button to update everything
a = get(h_fig, 'Position');
hupdate = uicontrol(...
    'Style', 'pushbutton', ...
    'Units', 'pixels', ...
    'String', sprintf('Update (%s)', datestr(now,14)), ...
    'Position', [4 a(4)-30 190 25], ...
    'ToolTipString', 'Update the stored and present setpoints.', ...
    'UserData', htable, ...
    'Callback', @MachineConfig_Update_Callback);

% Button to set the Golden to the machine
a = get(h_fig, 'Position');
hset = uicontrol(...
    'Style', 'pushbutton', ...
    'Units', 'pixels', ...
    'String', 'Restore Setpoints', ...
    'Position', [195 a(4)-30 180 25], ...
    'ToolTipString', 'Set the stored setpoints to the machine.', ...
    'UserData', FileName, ...
    'Callback', @MachineConfig_Set_Callback);

% Edit button
a = get(h_fig, 'Position');
hedit = uicontrol(...
    'Style', 'togglebutton', ...
    'Units', 'pixels', ...
    'String', 'Edit Mode is Off', ...
    'Position', [377 a(4)-30 150 25], ...
    'ToolTipString', 'Allow changes to the present setpoint, or not.', ...
    'UserData', htable, ...
    'Callback', @MachineConfig_Edit_Callback);


a(3) = TableWidth + 10;
set(h_fig, 'Position', a);

set(h_fig,   'Units', 'Normalized');
set(htable,  'Units', 'Normalized');
set(hset,    'Units', 'Normalized');
set(hupdate, 'Units', 'Normalized');
set(hedit,   'Units', 'Normalized');

set(h_fig, 'HandleVisibility','Callback');



function MachineConfig_Table_Callback(varargin)

h = varargin{1};
Index = varargin{2}.Indices;
Table = get(h, 'Data');

if Index(1,2) == 3
    % Column 3 - Make setpoint change
    ChannelName = deblank(Table{Index(1),5});
    if isspace(ChannelName(1))
        ChannelName(1) = []; % Space was added for readability
    end
    [Family, Field, Device] = channel2family(ChannelName);
    NewSP = Table{Index(1),3};
    
    fprintf('   Changing %s (%s) to %f\n', Table{Index(1),1}, ChannelName, NewSP);
    
    % Using Family, Field, Device will catch special functions and can be used in simulate mode!
    setpv(Family, Field, NewSP, Device);
    
    ColNames = get(h, 'ColumnName');
    ColNames{3} = datestr(now,14);
    set(h, 'ColumnName', ColNames);
end


Table(:,4) = num2cell(cell2mat(Table(:,2)) - cell2mat(Table(:,3)));

set(h, 'Data', Table);



function MachineConfig_Update_Callback(varargin)

% Table handle
h = get(varargin{1}, 'UserData');

% Old table
%Table = get(h, 'Data');

% New table
FileName = get(h, 'UserData');
Table = MachineConfig_buildtable(FileName);

%ColNames = get(h, 'ColumnName');
%ColNames{3} = datestr(now,14);
%set(h, 'ColumnName', ColNames);

set(varargin{1},'String', sprintf('Update (%s)', datestr(now,14)));

set(h, 'Data', Table);



function MachineConfig_Set_Callback(varargin)
StartFlag = questdlg({'Change All Setpoints to the ','Values in the Stored Column?'},'','Yes','No','No');
drawnow;

if ~strcmp(StartFlag,'Yes')
    fprintf('   No changes made.\n');
    return
end


FileName = get(varargin{1}, 'UserData');
setmachineconfig(FileName, 0);



function MachineConfig_Edit_Callback(varargin)

% Table handle
h = get(varargin{1}, 'UserData');
ColNames = get(h, 'ColumnName');

if get(varargin{1}, 'Value') == 1
    ColNames{3} = 'Present (edit)';
    coledit = [false false true false false];
    set(varargin{1}, 'String', 'Edit Mode is On');
else
    ColNames{3} = 'Present';
    coledit = [false false false false false];
    set(varargin{1}, 'String', 'Edit Mode is Off');
end

set(h, 'ColumnEditable', coledit);
set(h, 'ColumnName', ColNames);




function Table = MachineConfig_buildtable(FileName)


if strcmpi(FileName, 'Golden') || strcmpi(FileName, 'Production')
    ConfigSetpoint = getproductionlattice;
elseif strcmpi(FileName, 'Injection')
    ConfigSetpoint = getinjectionlattice;
else
    load(FileName);
end


Families = fields(ConfigSetpoint);


n = 0;
for i = 1:length(Families)
    Fields = fields(ConfigSetpoint.(Families{i}));
    
    for j = 1:length(Fields)
        ValueSaved = ConfigSetpoint.(Families{i}).(Fields{j}).Data;
        DeviceList = ConfigSetpoint.(Families{i}).(Fields{j}).DeviceList;
        CommonName = family2common(Families{i}, DeviceList);
        ChannelName = family2channel(Families{i}, Fields{j}, DeviceList);
        ValueNow = getpv(Families{i}, Fields{j}, DeviceList);

        % iNaN = isnan(ValueNow);  % Base removal on present values
        iNaN = isnan(ValueSaved);  % Base removal on the saved values
        
        CommonName(iNaN,:) = [];
        ChannelName(iNaN,:) = [];
        ValueSaved(iNaN,:) = [];
        ValueNow(iNaN,:) = [];
        
        for k = 1:size(ChannelName,1)
            n = n + 1;
            if length(Fields) > 1
                Table{n,1} = [deblank(CommonName(k,:)) ' (', deblank(Fields{j}), ')'];
            else
                Table{n,1} = deblank(CommonName(k,:));
            end
            Table{n,2} = ValueSaved(k);
            Table{n,3} = ValueNow(k);
            Table{n,4} = ValueSaved(k) - ValueNow(k);
            Table{n,5} = [' ', deblank(ChannelName(k,:))];
        end
    end
end
