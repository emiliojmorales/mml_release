function [Chro, Tune] = modelchro_NO_COD(varargin)
%MODELCHRO - Returns the AT model chromaticity
%  [Chromaticity, Tune] = modelchro(DeltaRF)
%
%  INPUTS
%  1. DeltaRF - Change in RF used to compute the chromaticity [Hz]  {Default: .1 Hz}
%  2. 'Hardware' returns chromaticity in hardware units (Tune/MHz (usually))
%     'Physics'  returns chromaticity in physics  units (Tune/(dp/p))
%
%  OUTPUTS
%  1. Chromaticity vector (2x1)
%  1. Tune vector (2x1)
%
%  NOTES  
%  1. This function is orbit dependent (unlike modeltwiss)
%
%  See also getnusympmat, modelbeta, modeltune, modeldisp, modeltwiss

%  Written by Greg Portmann


UnitsFlag = 'Physics';
MCF = [];

% Look if 'struct' or 'numeric' in on the input line
for i = length(varargin):-1:1
    if strcmpi(varargin{i},'Physics')
        UnitsFlag = 'Physics';
        varargin(i) = [];
        %if length(varargin) >= i
        %    if isnumeric(varargin{i})
        %        MCF = varargin{i};
        %        varargin(i) = [];
        %    end
        %end
    elseif strcmpi(varargin{i},'Hardware')
        UnitsFlag = 'Hardware';
        varargin(i) = [];
        %if length(varargin) >= i
        %    if isnumeric(varargin{i})
        %        MCF = varargin{i};
        %        varargin(i) = [];
        %    end
        %end
    end        
end


DeltaRF = [];
if length(varargin) >= 1
    if isnumeric(varargin{1})
        DeltaRF = varargin{1};  % Hz
    end
end
if  isempty(DeltaRF)   
    DeltaRF = 1;  % Hz
end


global THERING
if isempty(THERING)
    error('Simulator variable is not setup properly.');
end

MCF = mcf(THERING);
gt=gettwiss;
[CavityState, PassMethod, iCavity] = getcavity;
if isempty(CavityState)
    % tunechrom does not take into account the present RF frequency
    [Tune, Chro] = tunechrom(THERING, 0, 'chrom');
    Tune = Tune(:);
    Chro = Chro(:);
    UnitsFlag = 'Physics';
    if strcmpi(UnitsFlag,'Hardware')
        fprintf('   Chromaticity units changed to physics\n');
    end
    return
    %error('The model does not have a cavity, hence the chromaticity cannot be determined.');
else
    if any(strcmpi(CavityState(1,:),'Off'))
        % Turn on the cavities
        setcavity('On');
    end
end


% If the integer tune in needed
%[TD, Tune] = twissring(THERING,0,1:length(THERING)+1);
%IntTune = fix(Tune(:));


% Get tune (Johan's method)
M66 = findm66(THERING);
FracTune0 = getnusympmat(M66);
%FracTune0 =getnu_track;
%FracTune0 =modeltune;
%Tune = fix(IntTune) + FracTune0;


% Small change in the RF and remeasure the tune
RF0 = THERING{iCavity(1)}.Frequency;
for i = 1:length(iCavity)
    THERING{iCavity(i)}.Frequency = RF0 + DeltaRF;
end

%% correct orbit here
[ss0 Cx0 Cy0]=get_local_chro(DeltaRF,RF0,MCF);
ss=findspos(THERING,1:(numel(THERING)+1));
x = getx('struct');
y = gety('struct');
orb=findorbit6(THERING,1:(numel(THERING)+1));
hold on;
hcm = getsp('HCM','struct');
setorbit('Model', x, hcm,5,88);
% corrected!
[ss1 Cx1 Cy1]=get_local_chro(DeltaRF,RF0,MCF);
orb2=findorbit6(THERING,1:(numel(THERING)+1));
hcm2 = getsp('HCM','struct');
ind=atindex(THERING);
C0 =   299792458; 	% speed of light [m/s]
HarmNumber = 448;
thetas=getpv('HCM','Physics');
deltii=-gt.etax(ind.COR).*thetas*(RF0 + DeltaRF)/MCF/HarmNumber/C0;



save('/home/zeus/ZeusApps/Studies/Crom_Corrected_Orbit/thetas.mat','thetas');

a1=sqrt(gt.betax(ind.COR)*gt.betax');
a2=thetas*ones(1,numel(gt.betax));
b1=ones(numel(ind.COR),1)*gt.phix';
b2=gt.phix(ind.COR)*ones(1,numel(gt.betax));
a3=cos((2*pi*abs(b1-b2)-pi*gt.phix(end)));

orbit_th=sum(a1.*a2.*a3)/2/sin(pi*gt.phix(end));
orbit_mod=orb2(1,:)-orb(1,:);
[orb(5,1) orb2(5,1)-orb(5,1) sum(deltii)]


plot(ss,1e3*orb(1,:)','-b.');
plot(ss,1e3*orb2(1,:)','-c.');
[AX,H1,H2]=plotyy(ss,1e3*orb(1,:)',getspos('HCM'),hcm2.Data);
set(AX(2),'Xlim',[0 max(ss)/8]);
set(AX(2),'Ylim',[min(hcm2.Data)-0.1*abs(min(hcm2.Data)) max(hcm2.Data)+0.1*abs(max(hcm2.Data))]);
set(get(AX(2),'YLabel'),'String','HCM [A]');
set(H2,'Marker','o','LineStyle','--','MarkerFaceColor','g');
yl=ylim;
drawlattice(0,(yl(2)-yl(1))/10);
plot(ss,1e3*orb(1,:)','-b.');
plot(ss,1e3*orb2(1,:)','-c.');
xlim([0 max(ss)/8]);
xlabel('s [m]');
ylabel('COD [mm]');
box on;
grid on;

legend('RF changed orbit',sprintf('Corrected in %d BPMs',numel(x.Data)),'Location','South');
legend(AX(2),'HCM')
SetPlotSize(15,15);


%%
% Get tune (Johan's method)
M66 = findm66(THERING);
FracTune1 = getnusympmat(M66);
%FracTune1 =getnu_track;
%FracTune1 =modeltune;
% Reset the RF
for i = 1:length(iCavity)
    THERING{iCavity(i)}.Frequency = RF0;
end
setpv(hcm);

% Chromaticity in hardware units (DeltaTune / DeltaRF)
DeltaRF = physics2hw('RF', 'Setpoint', DeltaRF, [1 1], 'Model');
Chro = (FracTune1 - FracTune0) / DeltaRF;


if strcmpi(UnitsFlag,'Physics')
    % Change to Physics units
    
%    RF0 = RF0;% *1e-6;  % MHz
    Chro = (-MCF * RF0) * Chro;
end
%%
title(sprintf('Orbit correction after a %1.0f Hz change,\\xi=[%1.1f,%1.1f]',DeltaRF,Chro(1),Chro(2)));
print(sprintf('/home/zeus/ZeusApps/Studies/Crom_Corrected_Orbit/COD_CORR_%1.0f_Hz_change.eps',DeltaRF),'-depsc2','-r300');
print(sprintf('/home/zeus/ZeusApps/Studies/Crom_Corrected_Orbit/COD_CORR_%1.0f_Hz_change.png',DeltaRF),'-dpng','-r300');

% if strcmpi(UnitsFlag,'Hardware')
%     % Change to Hardware units
%     MCF = mcf(THERING);
%     CavityFrequency  = THERING{iCavity(1)}.Frequency;
%     Chro = Chro / (-MCF * RF0);
% end



% Reset cavity PassMethod
setcavity(PassMethod);
figure;
subplot(2,1,1);

stem(ss0,Cx0,'b.');hold on;stem(ss0,Cx1,'rx');
legend('RF change','RF change + orbit correct');legend('boxoff');
drawlattice(0,0.1);xlim([0 max(ss0)/8]);
title('Horizontal partial chromaticities contributions');
xlabel('s [m]');
ylabel(' \xi_{x,i} ');
subplot(2,1,2);
stem(ss0,Cy0,'b.');hold on;stem(ss0,Cy1,'rx');
%legend('RF change','RF change + orbit correct');
drawlattice(0,0.1);xlim([0 max(ss0)/8]);
title('Vertical partial chromaticities contributions');
xlabel('s [m]');
ylabel(' \xi_{y,i} ');
SetPlotSize(20,15);
print(sprintf('/home/zeus/ZeusApps/Studies/Crom_Corrected_Orbit/LocCrho_CORR_%1.0f_Hz_change.eps',DeltaRF),'-depsc2','-r300');
print(sprintf('/home/zeus/ZeusApps/Studies/Crom_Corrected_Orbit/LocCrho_CORR_%1.0f_Hz_change.png',DeltaRF),'-dpng','-r300');
end



