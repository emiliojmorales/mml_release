function sum = atsummary_tl
%ATSUMMARY - Prints out the paramters of the current AT lattice
%  The parameters that come after the Synchrotron Integrals are
%  parameters that depend on the Integrals themselves. The equations to
%  calculate them were taken from [1].
%
%  [1] Alexander Wu Chao and Maury Tigner, Handbook of Accelerator Physics
%  and Engineering (World Scientific, Singapore, 1998), pp. 183-187. (or
%  187-190 in ed. 2)
%
%  See also ringpara

%  Written by Eugene Tan
%  Revised by Laurent S. Nadolski

global THERING

% Structure to store info
sum.e0 = getenergy('Model');
sum.length = findspos(THERING, length(THERING)+1);
sum.gamma = sum.e0 / 0.51099906e-3;
sum.beta = sqrt(1 - 1/sum.gamma);

betaxin = 5.0;
betayin = 5.0;
alphaxin = 0.0;
alphayin = 0.0;
dxin = 0.0;
dpxin = 0.0;

tw0=twissring(THERING, 0,1,'Chrom');
    tw0.beta=[betaxin, betayin];
    tw0.alpha=[alphaxin alphayin];
    tw0.Dispersion(1)=dxin;
    tw0.Dispersion(2)=dpxin;

TD = twissline(THERING, 0, tw0, 1:length(THERING)+1,'Chrom');

% For calculating the synchrotron integrals
temp  = cat(2,TD.Dispersion);
Dx   = temp(1,:)';
Dpx  = temp(2,:)';
beta  = cat(1, TD.beta);
alpha = cat(1, TD.alpha);
gamma = (1 + alpha.^2) ./ beta;

% sum.naturalEnergySpread = sqrt(3.8319e-13*sum.gamma.^2*sum.integrals(3)/(2*sum.integrals(2) + sum.integrals(4)));
% sum.naturalEmittance = 3.8319e-13*(sum.e0*1e3/0.510999).^2*sum.integrals(5)/(sum.damping(1)*sum.integrals(2));

if nargout == 0
    fprintf('\n');
    %fprintf('   ******** Summary for ''%s'' ********\n', GLOBVAL.LatticeFile);
    fprintf('   ******** AT Lattice Summary ********\n');
    fprintf('   Energy: \t\t\t%4.5f [GeV]\n', sum.e0);
    fprintf('   Gamma: \t\t\t%4.5f \n', sum.gamma);
    fprintf('   Length: \t\t%4.5f [m]\n', sum.length);
    fprintf('   Energy Spread: \t%4.5e\n', sum.naturalEnergySpread);
    fprintf('   Emittance: \t\t%4.5e [mrad]\n', sum.naturalEmittance);
end