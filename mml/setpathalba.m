function [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathalba(varargin)
%SETPATHALBA - Initializes the Matlab Middle Layer (MML) for ALBA
%  [MachineName, SubMachineName, OnlineLinkMethod, MMLROOT] = setpathalba

%  Written by Greg Portmann
%  Modified by Gabriele Benedetti - 31 July 2009



Machine = 'ALBA';

%%%%%%%%%%%%%%%%%
% Input Parsing %
%%%%%%%%%%%%%%%%%

% First strip-out the link method
LinkFlag = '';
for i = length(varargin):-1:1
    if ~ischar(varargin{i})
        % Ignor input
    elseif strcmpi(varargin{i},'LabCA')
        LinkFlag = 'LabCA';
        varargin(i) = [];
    elseif strcmpi(varargin{i},'MCA')
        LinkFlag = 'MCA';
        varargin(i) = [];
    elseif strcmpi(varargin{i},'SCA')
        LinkFlag = 'SCA';
        varargin(i) = [];
    elseif strcmpi(varargin{i},'SLC')
        LinkFlag = 'SLC';
        varargin(i) = [];
    elseif strcmpi(varargin{i},'Tango')
        LinkFlag = 'Tango';
        varargin(i) = [];
    elseif strcmpi(varargin{i},'UCODE')
        LinkFlag = 'UCODE';
        varargin(i) = [];
    end
end

MatlabVersion = ver('Matlab');
MatlabVersion = str2num(MatlabVersion.Version);

% Get the submachine name
if length(varargin) >= 1
    SubMachineName = varargin{1};
else
    SubMachineName = 'StorageRing';
end

if isempty(SubMachineName)
    SubMachineNameCell = {'Storage Ring', 'Storage Ring 14Q' , 'SR Low Alpha', 'SR all skews', 'Booster', 'LTB', 'BTS', 'Lidia','LTB_BOOSTER'};
    [i, ok] = listdlg('PromptString', 'Select an accelerator:',...
        'SelectionMode', 'Single',...
        'Name', Machine, ...
        'ListString', SubMachineNameCell);
    if ok
        SubMachineName = SubMachineNameCell{i};
    else
        fprintf('Initialization cancelled (no path changed).\n');
        return;
    end
end

atrrot=getenv('ATROOT');
filesep_ind=findstr(atrrot,filesep);
str_atfolder=atrrot((filesep_ind(end)+1):end);
ListColors={[1,1,1],[1,1,1],[189 236 182]/255,[132 195 190]/255,[0.49*255 255 0.83*255]/255,[248 243 053]/255};
%ListColors={[1,1,1],[189 236 182]/255,[132 195 190]/255,[096 111 140]/255,[248 243 053]/255};
jDesktop = com.mathworks.mde.desk.MLDesktop.getInstance;
jDesktop.getMainFrame.setTitle(['-- ' SubMachineName ' -- ' str_atfolder '--MML']);
cmdWinDoc = com.mathworks.mde.cmdwin.CmdWinDocument.getInstance;
listeners = cmdWinDoc.getDocumentListeners;
vers=version('-release');
if str2num(vers(1:4))<2012
    set(listeners(3),'Background',ListColors{i});
    set(listeners(4),'Background',ListColors{i});
end


if any(strcmpi(SubMachineName, {'Storage Ring','Ring'}))
    SubMachineName = 'StorageRing';
end

% Common path at ALBA
   try
       MMLROOT = getmmlroot;
       addpath(fullfile(MMLROOT, 'machine', Machine, 'common'));
%        addpath(fullfile(MMLROOT, 'machine', Machine, 'common', 'naff', 'naffutils'));
%        addpath(fullfile(MMLROOT, 'machine', Machine, 'common', 'naff', 'naffutils', 'touscheklifetime'));
%        addpath(fullfile(MMLROOT, 'machine', Machine, 'common', 'naff', 'nafflib'));
       addpath(fullfile(MMLROOT, 'machine', Machine, 'common', 'database'));
%        addpath(fullfile(MMLROOT, 'mml', 'plotfamily')); % greg version
%        addpath(fullfile(MMLROOT, 'machine', Machine, 'common', 'plotfamily'));
       addpath(fullfile(MMLROOT, 'machine', Machine, 'common', 'configurations'));
%        addpath(fullfile(MMLROOT, 'applications', 'mmlviewer'));  
   catch
       disp('Path loading failed');
   end

if strcmpi(SubMachineName,'StorageRing')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'StorageRing', 'StorageRing', LinkFlag);
elseif strcmpi(SubMachineName,'Storage Ring 14Q')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'StorageRing_14Q',     'StorageRing_14Q',     LinkFlag);
elseif strcmpi(SubMachineName,'SR Low Alpha')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'StorageRing_LowAlpha',     'StorageRing_LowAlpha',     LinkFlag);
elseif strcmpi(SubMachineName,'SR all skews')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'StorageRing_SQall',     'StorageRing_SQall',     LinkFlag);
elseif strcmpi(SubMachineName,'Booster')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'Booster',     'Booster',     LinkFlag);
elseif strcmpi(SubMachineName,'LTB')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'LTB',         'Transport',   LinkFlag);
elseif strcmpi(SubMachineName,'BTS')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'BTS',         'Transport',   LinkFlag);
elseif strcmpi(SubMachineName,'Lidia')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'Lidia',         'Transport',   LinkFlag);
elseif strcmpi(SubMachineName,'LTB_BOOSTER')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'LTB_BOOSTER',         'Transport',   LinkFlag);
else
    error('SubMachineName %s unknown', SubMachineName);
end


addpath('/home/zeus/ZeusApps/MMLapps');
