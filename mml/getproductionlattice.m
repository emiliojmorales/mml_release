function [ConfigSetpoint, ConfigMonitor, FileName] = getproductionlattice(varargin)
%GETPRODUCTIONLATTICE - Get data from the production (golden) lattice file
%  [ConfigSetpoint, ConfigMonitor, FileName] = getproductionlattice(Field1, Field2, ...)
%
%  INPUTS
%  1. Family - Selected families (Default: All)
%
%  OUTPUTS
%  1. ConfigSetpoint - Setpoint structure
%  2. ConfigMonitor  - Monitor  structure
%  3. FileName       - Name of the file where the data was retrived
%
%  See also getinjectionlattice, getmachineconfig, setmachineconfig

%  Written by Greg Portmann


% Get the production file name (full path)
% AD.OpsData.LatticeFile could have the full path else default to AD.Directory.OpsData
FileName = getfamilydata('OpsData','LatticeFile');
[DirectoryName, FileName, Ext, VerNumber] = fileparts(FileName);
if isempty(DirectoryName)
    DirectoryName = getfamilydata('Directory', 'OpsData');
end
FileName = fullfile(DirectoryName,[FileName, '.mat']);


% Load the lattice 
load(FileName);


% Loop for keeping only asked family set values
if nargin > 0
    for i = 1:length(varargin)
        if isfield(ConfigSetpoint, varargin{i})
            ConfigSetpoint = ConfigSetpoint.(varargin{i});
        end
    end
end

% Loop for keeping only asked family set values
if nargout >= 2
if nargin > 0
    for i = 1:length(varargin)
        if isfield(ConfigMonitor, varargin{i})
            ConfigMonitor = ConfigMonitor.(varargin{i});
        end
    end
end
end

